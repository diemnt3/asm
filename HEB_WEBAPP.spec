%define        __spec_install_post %{nil}
%define          debug_package %{nil}
%define        __os_install_post %{_dbpath}/brp-compress
%define        _sysconfdir /appl

Summary: Deploy WEB Applications within Tomcat Server.
Name: @packageName@
Version: @packageVersion@
Release: @packageRelease@
Source0: %{_topdir}/SOURCES/%{name}-%{version}.tar.gz
Group: Applications/Internet
License: Copyright H.E. Butt Grocery Co, 2013
Requires: HEB_tomcat
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch

%description
This package copies application code and database connection configuration to a custom H-E-B Tomcat install and deploys them.

%prep

%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT

install -m 0755 -d  $RPM_BUILD_ROOT%{_sysconfdir}/apache-tomcat/webapps
cd  %{_topdir}/BUILD/%{name}-%{version}
find .| cpio -pvdum $RPM_BUILD_ROOT

%pre
/sbin/service tomcat7 stop

DIRECTORY=%{_sysconfdir}/apache-tomcat/webapps/@warName@
FILE=%{_sysconfdir}/apache-tomcat/webapps/@warName@.war

if [ -d "$DIRECTORY" ]; then
  rm -rf %{_sysconfdir}/apache-tomcat/webapps/@warName@
fi

if [ -f "$FILE" ]; then
  rm -rf %{_sysconfdir}/apache-tomcat/webapps/@warName@.war
fi

%post
/sbin/service tomcat7 start

%preun

%postun
if [ $1 = 0 ]; then
  DIRECTORY=%{_sysconfdir}/apache-tomcat/webapps/@warName@
  FILE=%{_sysconfdir}/apache-tomcat/webapps/@warName@.war

  if [ -d "$DIRECTORY" ]; then
    rm -rf %{_sysconfdir}/apache-tomcat/webapps/@warName@
  fi

  if [ -f $FILE ]; then
    rm -rf %{_sysconfdir}/apache-tomcat/webapps/@warName@.war
  fi
  /sbin/service tomcat7 restart
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(770,svct_tc,javaapp)
%{_sysconfdir}/apache-tomcat/webapps/@warName@.war

