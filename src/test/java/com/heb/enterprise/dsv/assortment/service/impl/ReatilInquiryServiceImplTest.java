package com.heb.enterprise.dsv.assortment.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.assortment.dao.RetailInquiryDAO;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;

@RunWith(MockitoJUnitRunner.class)
public class ReatilInquiryServiceImplTest {

	@InjectMocks
	ReatilInquiryServiceImpl reatilInquiryServiceImpl = new ReatilInquiryServiceImpl();

	@Mock
	private RetailInquiryDAO retailInquiryDAO;

	/**
	 * Test Search Products With Retail Inquiry 1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author dinhphan
	 */
	@Test
	public void testSearchProductsWithRetailInquiry1() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(3);
		Mockito.when(this.retailInquiryDAO.searchProductsWithRetailInquiry(productSearchCriteriaVO)).thenReturn(page);
		int actual = this.reatilInquiryServiceImpl.searchProductsWithRetailInquiry(productSearchCriteriaVO).getRowCount();
		assertEquals(3, actual);
	}

	/**
	 * Test Search Products With Retail Inquiry 2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author dinhphan
	 */
	@Test
	public void testSearchProductsWithRetailInquiry2() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		Mockito.when(this.retailInquiryDAO.searchProductsWithRetailInquiry(productSearchCriteriaVO)).thenReturn(null);
		Page<ProductRiewVO> actual = this.reatilInquiryServiceImpl.searchProductsWithRetailInquiry(productSearchCriteriaVO);
		assertNull(actual);
	}

	/**
	 * Test Export Excel Retail Inquiry.
	 * @throws DSVException
	 *             the DSV exception
	 * @author dinhphan
	 */
	@Test
	public void testExportExcelRetailInquiry() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		for (int i = 0; i < 3; i++) {
			ProductRiewVO productRiewVO = new ProductRiewVO();
			productRiewVO.setUpc("Upc");
			productRiewVO.setDescription("TEST");
			productRiewVO.setSize("Max");
			productRiewVO.setMap("Map");
			productRiewVO.setPrePrice("40");
			productRiewVO.setMsrp("msr");
			productRiewVO.setSrp("srp");
			productRiewVO.setCost("30");
			productRiewVO.setGpPercent("20");
			productRiewVO.setPennyProfit("PennyProfit");
			productRiewVO.setImageUri("ImageUri");
			productRiewVO.setFailedReason("Fail");
			productRiewVO.setTotalRow(1);
			productRiewVO.setReviewStatus("Y");
			productRiewVOs.add(productRiewVO);
		}

		Mockito.when(this.retailInquiryDAO.exportExcelRetailInquiry(productSearchCriteriaVO)).thenReturn(productRiewVOs);
		int actual = this.reatilInquiryServiceImpl.exportExcelRetailInquiry(productSearchCriteriaVO).size();
		assertEquals(3, actual);

	}

}
