package com.heb.enterprise.dsv.assortment.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Test;

import com.heb.enterprise.dsv.vo.AssortmentRulesVO;

/**
 * The Class DAOHelperTest.
 * @author quang.phan
 */
public class DAOHelperTest {

	/**
	 * Test convert colection rule to map1.
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testConvertColectionRuleToMap1() throws JsonGenerationException, JsonMappingException, IOException {
		List<AssortmentRulesVO> lstRuleVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setSeqNbr(199);
		assortmentRulesVO.setValue("999");
		lstRuleVOs.add(assortmentRulesVO);
		// Map<String, AssortmentRulesVO> actual = DAOHelper.convertColectionRuleToMap(lstRuleVOs);
		// ObjectMapper mapper = new ObjectMapper();
		// String expected =
		// "{\"123\":{\"id\":\"123\",\"name\":null,\"desc\":null,\"priority\":0,\"value\":\"999\",\"values\":null,\"activeSw\":null,\"type\":null,\"lvlId\":0,\"seqNbr\":199,\"lstSeqNbrs\":null,\"hebComdId\":0,\"hebSubComdId\":0,\"subDeptId\":null,\"deptId\":null,\"actionCd\":null,\"vendorId\":null,\"valType\":null,\"pctSW\":null,\"itmCls\":0,\"actTypCd\":null}}";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test convert colection rule to map2.
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testConvertColectionRuleToMap2() throws JsonGenerationException, JsonMappingException, IOException {
		List<AssortmentRulesVO> lstRuleVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO1 = new AssortmentRulesVO();
		assortmentRulesVO1.setId("123");
		assortmentRulesVO1.setSeqNbr(199);
		assortmentRulesVO1.setValue("999");
		List<String> values1 = new ArrayList<String>();
		values1.add("15");
		values1.add("39");
		assortmentRulesVO1.setValues(values1);
		lstRuleVOs.add(assortmentRulesVO1);
		AssortmentRulesVO assortmentRulesVO2 = new AssortmentRulesVO();
		assortmentRulesVO2.setId("123");
		assortmentRulesVO2.setSeqNbr(88);
		assortmentRulesVO2.setValue("789");
		List<String> values2 = new ArrayList<String>();
		values2.add("15");
		values2.add("39");
		assortmentRulesVO2.setValues(values2);
		lstRuleVOs.add(assortmentRulesVO2);
		// Map<String, AssortmentRulesVO> actual = DAOHelper.convertColectionRuleToMap(lstRuleVOs);
		// ObjectMapper mapper = new ObjectMapper();
		// String expected =
		// "{\"123\":{\"id\":\"123\",\"name\":null,\"desc\":null,\"priority\":0,\"value\":\"999\",\"values\":[\"15\",\"39\",\"789\"],\"activeSw\":null,\"type\":null,\"lvlId\":0,\"seqNbr\":199,\"lstSeqNbrs\":[199,88],\"hebComdId\":0,\"hebSubComdId\":0,\"subDeptId\":null,\"deptId\":null,\"actionCd\":null,\"vendorId\":null,\"valType\":null,\"pctSW\":null,\"itmCls\":0,\"actTypCd\":null}}";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test convert colection rule to map3.
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testConvertColectionRuleToMap3() throws JsonGenerationException, JsonMappingException, IOException {
		List<AssortmentRulesVO> lstRuleVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO1 = new AssortmentRulesVO();
		assortmentRulesVO1.setId("123");
		assortmentRulesVO1.setSeqNbr(88);
		assortmentRulesVO1.setValue("999");
		List<String> values1 = new ArrayList<String>();
		values1.add("15");
		values1.add("39");
		assortmentRulesVO1.setValues(values1);
		lstRuleVOs.add(assortmentRulesVO1);
		AssortmentRulesVO assortmentRulesVO2 = new AssortmentRulesVO();
		assortmentRulesVO2.setId("123");
		assortmentRulesVO2.setSeqNbr(88);
		assortmentRulesVO2.setValue("789");
		List<String> values2 = new ArrayList<String>();
		values2.add("15");
		values2.add("39");
		assortmentRulesVO2.setValues(values2);
		lstRuleVOs.add(assortmentRulesVO2);
		// Map<String, AssortmentRulesVO> actual = DAOHelper.convertColectionRuleToMap(lstRuleVOs);
		// ObjectMapper mapper = new ObjectMapper();
		// String expected =
		// "{\"123\":{\"id\":\"123\",\"name\":null,\"desc\":null,\"priority\":0,\"value\":\"999\",\"values\":[\"15\",\"39\"],\"activeSw\":null,\"type\":null,\"lvlId\":0,\"seqNbr\":88,\"lstSeqNbrs\":null,\"hebComdId\":0,\"hebSubComdId\":0,\"subDeptId\":null,\"deptId\":null,\"actionCd\":null,\"vendorId\":null,\"valType\":null,\"pctSW\":null,\"itmCls\":0,\"actTypCd\":null}}";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}
}
