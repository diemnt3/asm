package com.heb.enterprise.dsv.assortment.service.impl;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.assortment.dao.ProductsDAO;
import com.heb.enterprise.dsv.assortment.webservice.ProductHierarchyWS;
import com.heb.enterprise.dsv.common.service.DMDService;
import com.heb.enterprise.dsv.common.webservice.VendorWsClient;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;

/**
 * The Class CacheServiceImplTest.
 * @author quang.phan
 */
@RunWith(MockitoJUnitRunner.class)
public class CacheServiceImplTest {

	@InjectMocks
	private CacheServiceImpl cacheServiceImpl = new CacheServiceImpl();

	@Mock
	private ProductHierarchyWS productHierachyWsService;

	@Mock
	private VendorWsClient vendorWsClient;

	@Mock
	private DMDService dmdService;

	@Mock
	private ProductsDAO productsDAO;

	/**
	 * Test refresh caches.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	// @Test
	// public void testRefreshCaches() throws DSVException {
	// this.cacheServiceImpl.refreshCaches();
	// }

	/**
	 * Test get hierachy for global rules from cache.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetHierachyForGlobalRulesFromCache() throws DSVException {
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(null);
		List<DepartmentVO> actual = this.cacheServiceImpl.getHierachyForGlobalRulesFromCache();
		assertEquals(null, actual);
	}

	/**
	 * Test refresh hierachy for global from cache.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testRefreshHierachyForGlobalFromCache() throws DSVException {
		this.cacheServiceImpl.refreshHierachyForGlobalFromCache();
	}

	/**
	 * Test get commodity list1.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetCommodityList1() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.getCommodityList();
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"commodityCd\":0,\"commodityDesc\":null,\"bdmCd\":null,\"subCommodityVOs\":null,\"rulesCustomize\":null,\"parentCd\":null,\"markMapped\":0}]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test get commodity list2.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetCommodityList2() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.getCommodityList();
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test refresh vendor cache.
	 * @author quang.phan
	 */
	@Test
	public void testRefreshVendorCache() {
		this.cacheServiceImpl.refreshVendorCache();
	}

	/**
	 * Test get commodities from cache by commodity.
	 * @author quang.phan
	 */
	@Test
	public void testGetCommoditiesFromCacheByCommodity() {
		String productName = "Cocacola";
		this.cacheServiceImpl.getCommoditiesFromCacheByCommodity(productName);
	}

	@Test
	public void testRefreshCommoditiesFromCache() {
	}

	/**
	 * Test get brands from cache.
	 * @author quang.phan
	 */
	@Test
	public void testGetBrandsFromCache() {
		this.cacheServiceImpl.getBrandsFromCache();
	}

	@Test
	public void testRefreshBrandsFromCache() {
	}

	/**
	 * Test get brands from cache by brand.
	 * @author quang.phan
	 */
	@Test
	public void testGetBrandsFromCacheByBrand() {
		String brandName = "BR";
		this.cacheServiceImpl.getBrandsFromCacheByBrand(brandName);
	}

	/**
	 * Test get all brand from cache1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetAllBrandFromCache1() throws DSVException {
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setId("10");
		baseVO.setName("ABC");
		baseVOs.add(baseVO);
		Mockito.when(this.dmdService.getBrandLists()).thenReturn(baseVOs);
		this.cacheServiceImpl.getAllBrandFromCache();
	}

	/**
	 * Test get all brand from cache2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetAllBrandFromCache2() throws DSVException {
		Mockito.when(this.dmdService.getBrandLists()).thenThrow(new DSVException("Error"));
		List<BaseVO> actual = this.cacheServiceImpl.getAllBrandFromCache();
		assertEquals(null, actual);
	}

	/**
	 * Test refresh brand cache.
	 * @author quang.phan
	 */
	@Test
	public void testRefreshBrandCache() {
		this.cacheServiceImpl.refreshBrandCache();
	}

	/**
	 * Test get sub comm by commodity id1.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetSubCommByCommodityId1() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String commodityId = "123";
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(150);
		subCommodityVO.setSubCommodityDesc("AAA");
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<SubCommodityVO> actual = this.cacheServiceImpl.getSubCommByCommodityId(commodityId);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"subCommodityCd\":150,\"subCommodityDesc\":\"AAA\",\"parentCd\":null,\"sort\":0}]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test get sub comm by commodity id2.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetSubCommByCommodityId2() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String commodityId = "111";
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(150);
		subCommodityVO.setSubCommodityDesc("AAA");
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<SubCommodityVO> actual = this.cacheServiceImpl.getSubCommByCommodityId(commodityId);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "null";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test get sub commodity list1.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetSubCommodityList1() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		Map<String, SubCommodityVO> actual = this.cacheServiceImpl.getSubCommodityList();
		ObjectMapper mapper = new ObjectMapper();
		String expected = "{}";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test get sub commodity list2.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetSubCommodityList2() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(150);
		subCommodityVO.setSubCommodityDesc("AAA");
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		Map<String, SubCommodityVO> actual = this.cacheServiceImpl.getSubCommodityList();
		ObjectMapper mapper = new ObjectMapper();
		String expected = "{\"150\":{\"subCommodityCd\":150,\"subCommodityDesc\":\"AAA\",\"parentCd\":null,\"sort\":0}}";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test get commodity detail list.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetCommodityDetailMap() throws DSVException {
		Mockito.when(this.productHierachyWsService.getCommodityDetailList()).thenReturn(null);
		Map<Integer, CommodityVO> actual = this.cacheServiceImpl.getCommodityDetailMap();
		assertEquals(null, actual);
	}

	/**
	 * Test get commodity detail list1.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetCommodityDetailList1() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		Map<Integer, CommodityVO> mapCommodityVOs = new HashMap<Integer, CommodityVO>();
		mapCommodityVOs.put(112, new CommodityVO());
		Mockito.when(this.productHierachyWsService.getCommodityDetailList()).thenReturn(mapCommodityVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.getCommodityDetailList();
		ObjectMapper mapper = new ObjectMapper();
		assertEquals("[]", mapper.writeValueAsString(actual));
	}

	/**
	 * Test get commodity detail list2.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetCommodityDetailList2() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		Map<Integer, CommodityVO> mapCommodityVOs = new HashMap<Integer, CommodityVO>();
		mapCommodityVOs.put(123, new CommodityVO());
		Mockito.when(this.productHierachyWsService.getCommodityDetailList()).thenReturn(mapCommodityVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.getCommodityDetailList();
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"commodityCd\":0,\"commodityDesc\":null,\"bdmCd\":null,\"subCommodityVOs\":null,\"rulesCustomize\":null,\"parentCd\":null,\"markMapped\":0}]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test searchbrand1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchbrand1() throws DSVException {
		String query = "ABC";
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setId("10");
		baseVO.setName("ABC");
		baseVOs.add(baseVO);
		Mockito.when(this.dmdService.getBrandLists()).thenReturn(baseVOs);
		List<BaseVO> actual = this.cacheServiceImpl.searchbrand(query);
		assertEquals(1, actual.size());
		assertEquals("10", actual.get(0).getId());
		assertEquals("ABC", actual.get(0).getName());
	}

	/**
	 * Test searchbrand2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchbrand2() throws DSVException {
		String query = "";
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setId("10");
		baseVO.setName("ABC");
		baseVOs.add(baseVO);
		Mockito.when(this.dmdService.getBrandLists()).thenReturn(baseVOs);
		List<BaseVO> actual = this.cacheServiceImpl.searchbrand(query);
		assertEquals(1, actual.size());
		assertEquals("10", actual.get(0).getId());
		assertEquals("ABC", actual.get(0).getName());
	}

	/**
	 * Test searchbrand3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchbrand3() throws DSVException {
		String query = "ABC";
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setId("10");
		baseVO.setName("");
		baseVOs.add(baseVO);
		Mockito.when(this.dmdService.getBrandLists()).thenReturn(baseVOs);
		List<BaseVO> actual = this.cacheServiceImpl.searchbrand(query);
		assertEquals(0, actual.size());
	}

	/**
	 * Test searchbrand4.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchbrand4() throws DSVException {
		String query = "abc";
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setId("");
		baseVO.setName("");
		baseVOs.add(baseVO);
		Mockito.when(this.dmdService.getBrandLists()).thenReturn(baseVOs);
		List<BaseVO> actual = this.cacheServiceImpl.searchbrand(query);
		assertEquals(0, actual.size());
	}

	/**
	 * Test searchbrand5.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchbrand5() throws DSVException {
		String query = "abc";
		Mockito.when(this.dmdService.getBrandLists()).thenThrow(new DSVException("Error"));
		List<BaseVO> actual = this.cacheServiceImpl.searchbrand(query);
		assertEquals(0, actual.size());
	}

	/**
	 * Test searchbrand6.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchbrand6() throws DSVException {
		String query = "abc";
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setId("abc");
		baseVO.setName("ABC");
		baseVOs.add(baseVO);
		Mockito.when(this.dmdService.getBrandLists()).thenReturn(baseVOs);
		List<BaseVO> actual = this.cacheServiceImpl.searchbrand(query);
		assertEquals(1, actual.size());
	}

	/**
	 * Test search commodity list1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchCommodityList1() throws DSVException {
		String query = "COUNT";
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setCommodityDesc("TEST");
		commodityVOs.add(commodityVO);
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.searchCommodityList(query);
		assertEquals(0, actual.size());
	}

	/**
	 * Test search commodity list2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testSearchCommodityList2() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String query = "";
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setCommodityDesc("TEST");
		commodityVOs.add(commodityVO);
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.searchCommodityList(query);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"commodityCd\":123,\"commodityDesc\":\"TEST\",\"bdmCd\":null,\"subCommodityVOs\":null,\"rulesCustomize\":null,\"parentCd\":null,\"markMapped\":0}]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test search commodity list3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchCommodityList3() throws DSVException {
		String query = "XYZ";
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setCommodityDesc("");
		commodityVOs.add(commodityVO);
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.searchCommodityList(query);
		assertEquals(0, actual.size());
	}

	/**
	 * Test search commodity list4.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchCommodityList4() throws DSVException {
		String query = "XYZ";
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenThrow(new DSVException("Error"));
		List<CommodityVO> actual = this.cacheServiceImpl.searchCommodityList(query);
		assertEquals(0, actual.size());
	}

	/**
	 * Test search commodity list5.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testSearchCommodityList5() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String query = "XYZ";
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setCommodityDesc("XYZ");
		commodityVOs.add(commodityVO);
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.searchCommodityList(query);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"commodityCd\":123,\"commodityDesc\":\"XYZ\",\"bdmCd\":null,\"subCommodityVOs\":null,\"rulesCustomize\":null,\"parentCd\":null,\"markMapped\":0}]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test search commodity list6.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testSearchCommodityList6() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String query = "123";
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setCommodityDesc("123");
		commodityVOs.add(commodityVO);
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.productHierachyWsService.getMerchandisingHierarchyDetails()).thenReturn(departmentVOs);
		List<CommodityVO> actual = this.cacheServiceImpl.searchCommodityList(query);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"commodityCd\":123,\"commodityDesc\":\"123\",\"bdmCd\":null,\"subCommodityVOs\":null,\"rulesCustomize\":null,\"parentCd\":null,\"markMapped\":0}]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test search Product Desc.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchProductDesc_1() throws DSVException {
		String query = "ABC";
		List<ProductRiewVO> lstProductDes = new ArrayList<ProductRiewVO>();
		Mockito.when(this.productsDAO.getAllProductInfo()).thenReturn(lstProductDes);
		List<String> actual = this.cacheServiceImpl.searchProductDesc(query);
		assertEquals(0, actual.size());
	}

	/**
	 * Test search Product Desc.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchProductDesc_2() throws DSVException {
		String query = "ABC";
		List<ProductRiewVO> lstProductDes = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setDescription("ABC");
		lstProductDes.add(productRiewVO);
		Mockito.when(this.productsDAO.getAllProductInfo()).thenReturn(lstProductDes);
		List<String> actual = this.cacheServiceImpl.searchProductDesc(query);
		assertEquals("[ABC]", actual.toString());
	}
}
