package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.assortment.dao.CostReviewDAO;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.RetailCostSearchVO;
import com.heb.enterprise.dsv.vo.RetailCostVO;

/**
 * The Class RetailCostServiceImplTest.
 * @author quang.phan
 */
@RunWith(MockitoJUnitRunner.class)
public class RetailCostServiceImplTest {

	@InjectMocks
	private CostReviewServiceImpl retailCostServiceImpl = new CostReviewServiceImpl();

	@Mock
	private CostReviewDAO retailDao;

	/**
	 * Test get product with cost changes.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	// @Test
	// public void testGetProductWithCostChanges() throws DSVException {
	// List<RetailCostVO> lstResult = new ArrayList<RetailCostVO>();
	// RetailCostVO costVO = new RetailCostVO();
	// costVO.setProdDesc("prodDesc");
	// costVO.setPackSize("S");
	// costVO.setCurrentCost(Double.valueOf("1"));
	// costVO.setCurrentRetail(Double.valueOf("1.2"));
	// costVO.setCurrentMap("N");
	// costVO.setCurrentComp("Y");
	// costVO.setCurrentGP(Double.valueOf("20"));
	// costVO.setNextCost(Double.valueOf("1"));
	// costVO.setNextRetail(Double.valueOf("1.1"));
	// costVO.setNextMap("N");
	// costVO.setNextGP(Double.valueOf("10"));
	// costVO.setNextEffectiveDate(new Date());
	// costVO.setStatus("FAIL");
	// costVO.setFutureCostRetail("N");
	// lstResult.add(costVO);
	// RetailCostSearchVO model = new RetailCostSearchVO();
	// String tabActive = "tabA";
	// Mockito.when(this.retailDao.getProductWithCostChanges(model, tabActive)).thenReturn(lstResult);
	// this.retailCostServiceImpl.getProductWithCostChanges(model, tabActive);
	// }

	/**
	 * Test total of result for each status.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testTotalOfResultForEachStatus() throws DSVException {
		RetailCostSearchVO model = new RetailCostSearchVO();
		String tabActive = "tabA";
		// Mockito.when(this.retailDao.totalOfResultForEachStatus(model, tabActive)).thenReturn(null);
		// Map<String, Integer> actual = this.retailCostServiceImpl.totalOfResultForEachStatus(model, tabActive);
		// assertEquals(null, actual);
	}

	/**
	 * Test reject products.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testRejectProducts() throws DSVException {
		List<RetailCostVO> retailCostVOs = new ArrayList<RetailCostVO>();
		// Mockito.when(this.retailDao.rejectProducts(retailCostVOs)).thenReturn(null);
		// String actual = this.retailCostServiceImpl.rejectProducts(retailCostVOs);
		// assertEquals(null, actual);
	}

	/**
	 * Test remove products.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testRemoveProducts() throws DSVException {
		List<RetailCostVO> retailCostVOs = new ArrayList<RetailCostVO>();
		// Mockito.when(this.retailDao.removeProducts(retailCostVOs)).thenReturn(null);
		// String actual = this.retailCostServiceImpl.removeProducts(retailCostVOs);
		// assertEquals(null, actual);
	}

	/**
	 * Test save products.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSaveProducts() throws DSVException {
		List<RetailCostVO> retailCostVOs = new ArrayList<RetailCostVO>();
		// Mockito.when(this.retailDao.saveProducts(retailCostVOs)).thenReturn(null);
		// String actual = this.retailCostServiceImpl.saveProducts(retailCostVOs);
		// assertEquals(null, actual);
	}

	/**
	 * Test get competitor list.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetCompetitorList() throws DSVException {
		this.retailCostServiceImpl.getCompetitorList();
	}

	/**
	 * Test get future segments.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetFutureSegments() throws DSVException {
		this.retailCostServiceImpl.getFutureSegments();
	}
}
