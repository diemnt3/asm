/*
 * $Id.: $VendorManagementControllerTest.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ModelMap;

import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.VendorManagementService;
import com.heb.enterprise.dsv.assortment.webservice.ProductHierarchyWS;
import com.heb.enterprise.dsv.common.webservice.BDMWsClient;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.BDMDetailVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * VendorManagementControllerTest.
 * @author anhtran
 */
@RunWith(MockitoJUnitRunner.class)
public class VendorManagementControllerTest {

	private static final String VENDOR_TILE = "vendor.tile";

	private static final Logger LOG = Logger.getLogger(VendorManagementControllerTest.class);

	@Mock
	private CacheService cacheService;

	@Mock
	private VendorManagementService vendorManagementService;

	@Mock
	private BDMWsClient bdmServiceWS;

	@Mock
	private ModelMap model;

	@Mock
	private ProductHierarchyWS productHierarchyWS;

	@Mock
	private CommonService commonService;

	@Mock
	private HttpSession session;

	@InjectMocks
	private VendorManagementController vendorManagementController = new VendorManagementController();

	/**
	 * setUpBeforeClass.
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
	}

	/**
	 * tearDownAfterClass.
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() {
	}

	/**
	 * setUp.
	 * @throws Exception
	 */
	@Before
	public void setUp() {
	}

	/**
	 * tearDown.
	 * @throws Exception
	 */
	@After
	public void tearDown() {
	}

	/**
	 * testShowPricingRulesScreen.
	 */
	@Test
	@Ignore
	public void testShowPricingRulesScreen() {
		try {
			Assert.assertEquals("error message", VENDOR_TILE, this.vendorManagementController.showVendorManagementScreen(model));
		} catch (DSVException e) {
			LOG.info(e.getMessage());
		}
	}

	/**
	 * testGetAllVendorsList.
	 * @author anhtran.
	 */
	@Test
	public void testGetAllVendorsListTc01() throws DSVException {
		List<VendorInfoVO> infoVOs = new ArrayList<VendorInfoVO>();
		VendorInfoVO infoVO = new VendorInfoVO();
		infoVO.setVendorId("1");
		infoVO.setVendorName("Kehe");
		infoVOs.add(infoVO);
		Mockito.when(this.commonService.getAllListVendors()).thenReturn(infoVOs);
		this.vendorManagementController.getAllVendorsList();
		Mockito.verify(this.commonService).getAllListVendors();
	}

	/**
	 * testGetVendorHierarchyMappingByVendor.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testGetVendorHierarchyMappingByVendorTc01() {
		LOG.info("testGetVendorHierarchyMappingByVendorTc01");
		String vendorId = "12765";
		Map<String, SubCommodityVO> subCommodityMap = new HashMap<String, SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subCommodityVO.setSubCommodityDesc("DESC");
		subCommodityMap.put("123", subCommodityVO);
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setVendorId("VD28");
		hierarchyMapVO.setEntyIdSubCommodity("EN28");
		hierarchyMapVO.setCategoryAbb1("ABB1");
		hierarchyMapVO.setCategory1("C1");
		hierarchyMapVO.setCategoryAbb2("ABB2");
		hierarchyMapVO.setCategory2("C2");
		hierarchyMapVO.setCategoryAbb3("ABB3");
		hierarchyMapVO.setCategory3("C3");
		hierarchyMapVO.setCategoryAbb4("ABB4");
		hierarchyMapVO.setCategory4("C4");
		hierarchyMapVO.setSubCommodity("SUB");
		hierarchyMapVO.setLowestCatId("123");
		hierarchyMapVO.setStatusNew("N");
		hierarchyMapVO.setStatusChange("C");

		hierarchyMapVO.setSubCommodityCd("123");
		hierarchyMapVOs.add(hierarchyMapVO);
		try {
			Mockito.when(this.cacheService.getSubCommodityList()).thenReturn(subCommodityMap);
			Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);
		} catch (DSVException e) {
			LOG.info(e.getMessage());
		}
		String actual = this.vendorManagementController.getVendorHierarchyMappingByVendor(vendorId, this.model);
		String expected = "{\"errorMessage\":\"\",\"data\":[{\"category4Input\":\"ABB4\",\"entyIdSubCommodity\":\"EN28\",\"category1Input\":\"ABB1\",\"lowestCatId\":\"123\",\"category2Input\":\"ABB2\",\"statusChange\":\"C\",\"statusNew\":\"N\",\"category1\":\"ABB1\",\"subCommodity\":\"DESC\",\"category3Input\":\"ABB3\",\"vendorId\":\"VD28\",\"category3\":\"ABB3\",\"subCommodityCd\":\"123\",\"category2\":\"ABB2\",\"category4\":\"ABB4\",\"categoryId3\":\"C3\",\"categoryId4\":\"C4\",\"categoryId1\":\"C1\",\"categoryId2\":\"C2\",\"categoryIds\":\"C1>C2>C3>C4>EN28\",\"DT_RowId\":1}]}";
		Assert.assertEquals("Error result not same ", expected, actual);
	}

	/**
	 * testSaveVendorHierarchyMapping.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testSaveVendorHierarchyMappingTc01() {
		String vendorId = "12765";
		JSONObject jsonObject = new JSONObject();
		this.vendorManagementController.saveVendorHierarchyMapping(vendorId, jsonObject.toString());
	}

	/**
	 * testSaveVendorHierarchyMapping.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testSaveVendorHierarchyMappingTc02() {
		String vendorId = "12765";
		String jsonst = "[{\"DT_RowId\":4,\"category1\":\"input\",\"category2\":\"input\",\"category3\":\"input\"," +
				"\"category4\":\"input\",\"statusChange\":\"Y\",\"statusNew\":\"Y\",\"searchSubCommodity\":\"\"," +
				"\"subCommodity\":\"CANNED VEG - GREEN BEANS\",\"vendorId\":\"\",\"category1Input\":\"CAT1\"," +
				"\"category2Input\":\"CAT2\",\"category3Input\":\"\",\"category4Input\":\"\",\"lowestCatId\":\"\"," +
				"\"entyIdSubCommodity\":\"\",\"subCommodityCd\":\"130\",\"rowId\":4,\"cats\":\"CAT1>CAT2>CANNED VEG - GREEN BEANS\"}]";
		String actual = this.vendorManagementController.saveVendorHierarchyMapping(vendorId, jsonst);
		String expected = "{\"errorMessage\":\"\"}";
		Assert.assertEquals("Error result not same ", expected, actual);
	}

	/**
	 * testSaveVendorHierarchyMapping.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testSaveVendorHierarchyMappingTc03() {
		String vendorId = "12765";
		this.vendorManagementController.saveVendorHierarchyMapping(vendorId, "");
	}

	/**
	 * testGetHEBHierarchyRoot.
	 * @author mrh.diemnguyen
	 * @throws DSVException
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testGetHEBHierarchyRootTc01() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String commodityCd = "2";
		List<CommodityVO> lst = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setCommodityDesc("DESC");
		commodityVO.setSubCommodityVOs(new ArrayList<SubCommodityVO>());
		List<SubCommodityVO> subCommodityList = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subCommodityList.add(subCommodityVO);
		Mockito.when(this.cacheService.getCommodityList()).thenReturn(lst);
		Mockito.when(this.cacheService.getSubCommByCommodityId(commodityCd)).thenReturn(subCommodityList);
		List<JSONObject> actual = this.vendorManagementController.getHEBHierarchyRoot(commodityCd, this.session);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"id\":123,\"text\":\"<span class='subCommodityCd-cls'>123</span> <span class='subCommodityDesc-cls'>null</span>\",\"li_attr\":{},\"a_attr\":{}}]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * testGetHEBHierarchyRoot.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testGetHEBHierarchyRootTc02() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		List<CommodityVO> lst = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setCommodityDesc("DESC");
		List<SubCommodityVO> lstSub = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		lstSub.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(lstSub);
		lst.add(commodityVO);
		Map<Integer, CommodityVO> mapCommodityVO = new HashMap<Integer, CommodityVO>();
		Map<String, BDMDetailVO> bdmDetailMap = new HashMap<String, BDMDetailVO>();
		Mockito.when(this.cacheService.getCommodityList()).thenReturn(lst);
		Mockito.when(this.cacheService.getCommodityDetailMap()).thenReturn(mapCommodityVO);
		Mockito.when(this.bdmServiceWS.getBDMDetailList()).thenReturn(bdmDetailMap);
		List<JSONObject> actual = this.vendorManagementController.getHEBHierarchyRoot("#", this.session);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test get heb hierarchy root tc03.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetHEBHierarchyRootTc03() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String commodityCd = "#";
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVOs.add(subCommodityVO);
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO1 = new CommodityVO();
		commodityVO1.setCommodityCd(123);
		commodityVO1.setCommodityDesc("TEST");
		commodityVO1.setBdmCd("a106478");
		commodityVO1.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO1);
		CommodityVO commodityVO2 = new CommodityVO();
		commodityVO2.setBdmCd("a125");
		commodityVO2.setCommodityCd(145);
		commodityVO2.setCommodityDesc("APPLY");
		commodityVO2.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO2);
		Mockito.when(this.cacheService.getCommodityDetailList()).thenReturn(commodityVOs);

		BDMDetailVO bdmDetailVO1 = new BDMDetailVO();
		bdmDetailVO1.setBdmFullNm("Amonica");
		BDMDetailVO bdmDetailVO2 = new BDMDetailVO();
		bdmDetailVO2.setBdmFullNm("Rob Hardt");

		Map<String, BDMDetailVO> bdmDetailMap = new HashMap<String, BDMDetailVO>();
		bdmDetailMap.put("a125", bdmDetailVO1);
		bdmDetailMap.put("a106478", bdmDetailVO2);
		Mockito.when(this.bdmServiceWS.getBDMDetailList()).thenReturn(bdmDetailMap);
		List<JSONObject> actual = this.vendorManagementController.getHEBHierarchyRoot(commodityCd, this.session);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"id\":\"123_com\",\"text\":\"123 - TEST - Rob Hardt\",\"li_attr\":{\"class\":\"\"},\"state\":{\"disabled\":true},\"children\":true,\"a_attr\":{\"class\":\"font-bold commodity-a\"}},{\"id\":\"145_com\",\"text\":\"145 - APPLY - Amonica\",\"li_attr\":{\"class\":\"\"},\"state\":{\"disabled\":true},\"children\":true,\"a_attr\":{\"class\":\"font-bold commodity-a\"}}]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * testDeleteEntyRlshp.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testDeleteEntyRlshp_1() {
		this.vendorManagementController.deleteEntyRlshp("1", "12765");
	}

	/**
	 * testDeleteEntyRlshp.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testDeleteEntyRlshp_2() {
		String jsonst = "[{\"DT_RowId\":4,\"category1\":\"input\",\"category2\":\"input\",\"category3\":\"input\"," +
				"\"category4\":\"input\",\"statusChange\":\"Y\",\"statusNew\":\"Y\",\"searchSubCommodity\":\"\"," +
				"\"subCommodity\":\"CANNED VEG - GREEN BEANS\",\"vendorId\":\"\",\"category1Input\":\"CAT1\"," +
				"\"category2Input\":\"CAT2\",\"category3Input\":\"\",\"category4Input\":\"\",\"lowestCatId\":\"\"," +
				"\"entyIdSubCommodity\":\"\",\"subCommodityCd\":\"130\",\"rowId\":4,\"cats\":\"CAT1>CAT2>CANNED VEG - GREEN BEANS\"}]";

		this.vendorManagementController.deleteEntyRlshp("1", jsonst);
	}

	/**
	 * testDeleteEntyRlshp.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testDeleteEntyRlshp_3() {
		this.vendorManagementController.deleteEntyRlshp("1", "");
	}

	/**
	 * searchHebHierarchyTc01.
	 * @author mrh.diemnguyen
	 * @throws DSVException
	 */
	@Test
	public void searchHebHierarchyTc01() throws DSVException {
		String bdmcd = "MWORG";
		String str = "asian";
		Map<Integer, CommodityVO> mapCommodityVO = new HashMap<Integer, CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setBdmCd(bdmcd);
		List<SubCommodityVO> subCommodityList = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subCommodityVO.setSubCommodityDesc(str);
		subCommodityList.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityList);

		mapCommodityVO.put(1, commodityVO);

		Map<String, BDMDetailVO> bdmDetailMap = new HashMap<String, BDMDetailVO>();
		BDMDetailVO bdmDetailVO = new BDMDetailVO();
		bdmDetailVO.setBdmCd(bdmcd);
		bdmDetailVO.setBdmFullNm("ABC");
		bdmDetailMap.put(bdmcd, bdmDetailVO);

		Mockito.when(this.cacheService.getCommodityDetailMap()).thenReturn(mapCommodityVO);
		Mockito.when(this.bdmServiceWS.getBDMDetailList()).thenReturn(bdmDetailMap);
		this.vendorManagementController.searchHebHierarchy(str);
	}

	/**
	 * searchHebHierarchyTc01.
	 * @author mrh.diemnguyen
	 * @throws DSVException
	 */
	@Test
	public void searchHebHierarchyTc02() throws DSVException {
		String bdmcd = "MWORG";
		String str = "asian";
		Map<Integer, CommodityVO> mapCommodityVO = new HashMap<Integer, CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setBdmCd(bdmcd);
		List<SubCommodityVO> subCommodityList = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subCommodityVO.setSubCommodityDesc("BCD");
		subCommodityList.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityList);

		mapCommodityVO.put(1, commodityVO);

		Map<String, BDMDetailVO> bdmDetailMap = new HashMap<String, BDMDetailVO>();
		BDMDetailVO bdmDetailVO = new BDMDetailVO();
		bdmDetailVO.setBdmCd(bdmcd);
		bdmDetailVO.setBdmFullNm("ABC");
		bdmDetailMap.put(bdmcd, bdmDetailVO);

		Mockito.when(this.cacheService.getCommodityDetailMap()).thenReturn(mapCommodityVO);
		Mockito.when(this.bdmServiceWS.getBDMDetailList()).thenReturn(bdmDetailMap);
		this.vendorManagementController.searchHebHierarchy(str);
	}

	/**
	 * searchHebHierarchyTc01.
	 * @author mrh.diemnguyen
	 * @throws DSVException
	 */
	@Test
	public void searchHebHierarchyTc03() throws DSVException {
		String bdmcd = "MWORG";
		String str = "asian";
		Map<Integer, CommodityVO> mapCommodityVO = new HashMap<Integer, CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setBdmCd(bdmcd);
		commodityVO.setCommodityDesc(str);
		List<SubCommodityVO> subCommodityList = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subCommodityVO.setSubCommodityDesc("BCD");
		subCommodityList.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityList);

		mapCommodityVO.put(1, commodityVO);

		Map<String, BDMDetailVO> bdmDetailMap = new HashMap<String, BDMDetailVO>();
		BDMDetailVO bdmDetailVO = new BDMDetailVO();
		bdmDetailVO.setBdmCd(bdmcd);
		bdmDetailVO.setBdmFullNm("ABC");
		bdmDetailMap.put(bdmcd, bdmDetailVO);

		Mockito.when(this.cacheService.getCommodityDetailMap()).thenReturn(mapCommodityVO);
		Mockito.when(this.bdmServiceWS.getBDMDetailList()).thenReturn(bdmDetailMap);
		this.vendorManagementController.searchHebHierarchy(str);
	}

	/**
	 * Search heb hierarchy tc04.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void searchHebHierarchyTc04() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String str = "ABC";
		Mockito.when(this.bdmServiceWS.getBDMDetailList()).thenThrow(new DSVException("Error"));
		List<String> actual = this.vendorManagementController.searchHebHierarchy(str);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[\"error\",\"java.lang.Exception: Error\"]";
		assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Search heb hierarchy tc05.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void searchHebHierarchyTc05() throws DSVException {
		String bdmcd = "MWORG";
		String str = "asian";
		Map<Integer, CommodityVO> mapCommodityVO = new HashMap<Integer, CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setBdmCd(bdmcd);
		List<SubCommodityVO> subCommodityList = new ArrayList<SubCommodityVO>();
		commodityVO.setSubCommodityVOs(subCommodityList);
		mapCommodityVO.put(1, commodityVO);
		Map<String, BDMDetailVO> bdmDetailMap = new HashMap<String, BDMDetailVO>();
		BDMDetailVO bdmDetailVO = new BDMDetailVO();
		bdmDetailVO.setBdmCd(bdmcd);
		bdmDetailVO.setBdmFullNm("ABC");
		bdmDetailMap.put(bdmcd, bdmDetailVO);
		Mockito.when(this.cacheService.getCommodityDetailMap()).thenReturn(mapCommodityVO);
		Mockito.when(this.bdmServiceWS.getBDMDetailList()).thenReturn(bdmDetailMap);
		List<String> actual = this.vendorManagementController.searchHebHierarchy(str);
		assertEquals(0, actual.size());
	}
}
