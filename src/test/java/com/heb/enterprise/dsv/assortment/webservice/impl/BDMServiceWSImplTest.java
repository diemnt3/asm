/*
 * $Id.: $BDMServiceWSImplTest.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * BDMServiceWSImplTest.
 * @author anhtran
 */
public class BDMServiceWSImplTest {

	/**
	 * setUpBeforeClass.
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
	}

	/**
	 * tearDownAfterClass.
	 */
	@AfterClass
	public static void tearDownAfterClass() {
	}

	/**
	 * setUp.
	 * @throws Exception
	 */
	@Before
	public void setUp() {
	}

	/**
	 * tearDown.
	 * @throws Exception
	 */
	@After
	public void tearDown() {
	}

	/**
	 * testGetBDMDetailList.
	 */
	@Test
	public void testGetBDMDetailList() {
	}

	/**
	 * testGetBDMDetails.
	 */
	@Test
	public void testGetBDMDetails() {
	}

}
