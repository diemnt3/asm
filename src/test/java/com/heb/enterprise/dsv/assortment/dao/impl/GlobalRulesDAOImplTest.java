package com.heb.enterprise.dsv.assortment.dao.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

//@RunWith(SpringJUnit4ClassRunner.class)
public class GlobalRulesDAOImplTest {// extends BaseDSVContextTest {

	// @Autowired
	// private GlobalRulesDAO globalRulesDAO;

	/**
	 * Perform pre-test initialization.
	 * @throws Exception
	 *             if the initialization fails for some reason
	 * @generatedBy CodePro at 2/25/15 4:58 PM
	 */
	@Before
	public void setUp() throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 * @throws Exception
	 *             if the clean-up fails for some reason
	 * @generatedBy CodePro at 2/25/15 4:58 PM
	 */
	@After
	public void tearDown() throws Exception {
		// Add additional tear down code here
	}

	@Test
	public void testGetGlobalRuleSet() {

	}

	@Test
	public void testGetGlobalRulebyCommodity() {

	}

	@Test
	public void testUpdateRuleValuesToDfns() {

	}

	@Test
	public void testUpdateLvl() {

	}

	@Test
	public void testInsertRulesToDfns() {

	}

	@Test
	public void testInsertlvl() {

	}

	@Test
	public void testGetGlobalCommodityOverride() {

	}

	@Test
	public void testInsertlvlActivated() {

	}

	@Test
	public void testRemoveRule() {

	}

	@Test
	public void testRemoveLvl() {

	}

	@Test
	public void testGetLvlIdByCommodity() {

	}

	@Test
	public void testRemoveRules() {

	}

	@Test
	public void testRemoveLvls() {
		// List<Long> lstLvlIds = new ArrayList<Long>();
		// lstLvlIds.add(30l);
		// System.out.println("Hello");
		// boolean actual = this.globalRulesDAO.removeLvls(lstLvlIds);
		// assertEquals(true, actual);
	}

	// @Test
	// public void testGetMaxLVLID() {
	// long actual = this.globalRulesDAO.getMaxLVLID();
	// assertEquals(63l, actual);
	// }

	// public static void main(String[] args) {
	// new org.junit.runner.JUnitCore().run(GlobalRulesDAOImplTest.class);
	// }
}
