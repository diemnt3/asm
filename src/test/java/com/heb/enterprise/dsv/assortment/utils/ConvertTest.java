package com.heb.enterprise.dsv.assortment.utils;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;

/**
 * The Class ConvertTest.
 * @author mrh.diemnguyen
 */
@RunWith(MockitoJUnitRunner.class)
public class ConvertTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test convert assortment rules vo.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testConvertAssortmentRulesVO_1() {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("A123");
		assortmentRulesVO.setLvlId(123L);
		List<String> lstValue = new ArrayList<String>();
		lstValue.add("Start-Start");
		lstValue.add("End-End");
		assortmentRulesVO.setValues(lstValue);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		RuleDefinationVO ruleDefinationVO = new RuleDefinationVO();
		lstRuleDefinationVOs.add(ruleDefinationVO);

		// Convert.convertAssortmentRulesVO(assortmentRulesVO, lstRuleDefinationVOs);
	}

	/**
	 * Test convert assortment rules vo.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testConvertAssortmentRulesVO_2() {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("A123");
		assortmentRulesVO.setLvlId(123L);
		List<String> lstValue = new ArrayList<String>();
		lstValue.add("Start-Start");
		lstValue.add("End-End");
		assortmentRulesVO.setValues(lstValue);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		RuleDefinationVO ruleDefinationVO = new RuleDefinationVO();
		lstRuleDefinationVOs.add(ruleDefinationVO);

		Convert.convertAssortmentRulesVO(new AssortmentRulesVO(), lstRuleDefinationVOs);
	}

	/**
	 * Test convert assortment rules v o_3.
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testConvertAssortmentRulesVO_3() throws JsonGenerationException, JsonMappingException, IOException {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("10");
		assortmentRulesVO.setLvlId(123L);
		assortmentRulesVO.setActiveSw("Y");
		List<String> lstValue = new ArrayList<String>();
		lstValue.add("Start-Start");
		assortmentRulesVO.setValues(lstValue);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		RuleDefinationVO ruleDefinationVO = new RuleDefinationVO();
		lstRuleDefinationVOs.add(ruleDefinationVO);
		Convert.convertAssortmentRulesVO(assortmentRulesVO, lstRuleDefinationVOs);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"ruleId\":0,\"seq_nbr\":0,\"lvlId\":0,\"actSW\":null,\"val\":null,\"valType\":null,\"pctSW\":null,\"activeType\":null,\"logicOprtrId\":0,\"mathOprtrId\":0,\"ruleCat1Id\":0,\"ruleCat2Id\":0},{\"ruleId\":10,\"seq_nbr\":1,\"lvlId\":123,\"actSW\":\"Y\",\"val\":\"1\",\"valType\":null,\"pctSW\":null,\"activeType\":\"\",\"logicOprtrId\":1,\"mathOprtrId\":5,\"ruleCat1Id\":1,\"ruleCat2Id\":2}]";
		assertEquals(expected, mapper.writeValueAsString(lstRuleDefinationVOs));
	}

	/**
	 * Test convert assortment rules v o_4.
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testConvertAssortmentRulesVO_4() throws JsonGenerationException, JsonMappingException, IOException {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("1");
		assortmentRulesVO.setLvlId(123L);
		assortmentRulesVO.setActiveSw("Y");
		List<String> lstValue = new ArrayList<String>();
		lstValue.add("Start-Start");
		assortmentRulesVO.setValues(lstValue);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		RuleDefinationVO ruleDefinationVO = new RuleDefinationVO();
		lstRuleDefinationVOs.add(ruleDefinationVO);
		Convert.convertAssortmentRulesVO(assortmentRulesVO, lstRuleDefinationVOs);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"ruleId\":0,\"seq_nbr\":0,\"lvlId\":0,\"actSW\":null,\"val\":null,\"valType\":null,\"pctSW\":null,\"activeType\":null,\"logicOprtrId\":0,\"mathOprtrId\":0,\"ruleCat1Id\":0,\"ruleCat2Id\":0},{\"ruleId\":1,\"seq_nbr\":1,\"lvlId\":123,\"actSW\":\"Y\",\"val\":\"1\",\"valType\":null,\"pctSW\":null,\"activeType\":\"\",\"logicOprtrId\":1,\"mathOprtrId\":5,\"ruleCat1Id\":1,\"ruleCat2Id\":2}]";
		assertEquals(expected, mapper.writeValueAsString(lstRuleDefinationVOs));
	}

	/**
	 * Test convert assortment rules v o_5.
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testConvertAssortmentRulesVO_5() throws JsonGenerationException, JsonMappingException, IOException {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("1");
		assortmentRulesVO.setLvlId(123L);
		assortmentRulesVO.setActiveSw("N");
		List<String> lstValue = new ArrayList<String>();
		lstValue.add("Start-Start");
		assortmentRulesVO.setValues(lstValue);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		RuleDefinationVO ruleDefinationVO = new RuleDefinationVO();
		lstRuleDefinationVOs.add(ruleDefinationVO);
		Convert.convertAssortmentRulesVO(assortmentRulesVO, lstRuleDefinationVOs);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"ruleId\":0,\"seq_nbr\":0,\"lvlId\":0,\"actSW\":null,\"val\":null,\"valType\":null,\"pctSW\":null,\"activeType\":null,\"logicOprtrId\":0,\"mathOprtrId\":0,\"ruleCat1Id\":0,\"ruleCat2Id\":0},{\"ruleId\":1,\"seq_nbr\":1,\"lvlId\":123,\"actSW\":\"N\",\"val\":\"0\",\"valType\":null,\"pctSW\":null,\"activeType\":\"\",\"logicOprtrId\":1,\"mathOprtrId\":5,\"ruleCat1Id\":1,\"ruleCat2Id\":2}]";
		assertEquals(expected, mapper.writeValueAsString(lstRuleDefinationVOs));
	}

	/**
	 * Test get hierachy.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testGetHierachy_1() {
		String subdept = "123";
		String commodityId = "123";
		String subComId = "123";

		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("DPID");

		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();

		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(BigDecimal.ONE);

		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();

		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subDepartmentVO.setSubDeptDesc("DESC");

		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);
		AssortmentRulesVO actual = Convert.getHierachy(lstDepartmentVOs, subdept, commodityId, subComId);
		Assert.assertNotNull(actual);

	}

	/**
	 * Test get hierachy.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testGetHierachy_2() {
		String subdept = "123";
		String commodityId = "123";
		String subComId = "";
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("DPID");

		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();

		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(BigDecimal.ONE);

		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);

		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subDepartmentVO.setSubDeptDesc("DESC");

		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);
		AssortmentRulesVO actual = Convert.getHierachy(lstDepartmentVOs, subdept, commodityId, subComId);
		Assert.assertNotNull(actual);

	}

	/**
	 * Test get hierachy.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testGetHierachy_3() {
		String subdept = "123";
		String commodityId = "";
		String subComId = "";
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("DPID");

		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptId(subdept);
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(BigDecimal.ONE);

		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();

		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subDepartmentVO.setSubDeptDesc("DESC");

		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);
		AssortmentRulesVO actual = Convert.getHierachy(lstDepartmentVOs, subdept, commodityId, subComId);
		Assert.assertNotNull(actual);

	}

	/**
	 * Test get path sub comm.
	 */
	@Test
	public void testGetPathSubComm() {
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("DPID");

		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptId("123");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(BigDecimal.ONE);

		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();

		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subDepartmentVO.setSubDeptDesc("DESC");

		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);

		List<String> lstsubComs = new ArrayList<String>();
		lstsubComs.add("123");

		Convert.getPathSubComm(lstDepartmentVOs, lstsubComs);
	}

	/**
	 * Test convert pricing rules v o1.
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testConvertPricingRulesVO1() throws JsonGenerationException, JsonMappingException, IOException {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActTypCd("Cd1");
		assortmentRulesVO.setValType("type1");
		assortmentRulesVO.setLvlId(1l);
		assortmentRulesVO.setPctSW("ptc 5");
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Convert.convertPricingRulesVO(assortmentRulesVO, lstRuleDefinationVOs);
		ObjectMapper mapper = new ObjectMapper();
		// String expected =
		// "[{\"ruleId\":123,\"seq_nbr\":1,\"lvlId\":1,\"actSW\":\"Y\",\"val\":null,\"valType\":\"type1\",\"pctSW\":\"ptc 5\",\"activeType\":\"Cd1\",\"logicOprtrId\":1,\"mathOprtrId\":5,\"ruleCat1Id\":1,\"ruleCat2Id\":2}]";
		// assertEquals(expected, mapper.writeValueAsString(lstRuleDefinationVOs));
	}

	/**
	 * Test convert pricing rules v o2.
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testConvertPricingRulesVO2() throws JsonGenerationException, JsonMappingException, IOException {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActTypCd("Cd1");
		assortmentRulesVO.setValType("type1");
		assortmentRulesVO.setLvlId(1l);
		assortmentRulesVO.setPctSW("Y");
		List<String> values = new ArrayList<String>();
		values.add("Val1");
		assortmentRulesVO.setValues(values);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Convert.convertPricingRulesVO(assortmentRulesVO, lstRuleDefinationVOs);
		ObjectMapper mapper = new ObjectMapper();
		// String expected =
		// "[{\"ruleId\":123,\"seq_nbr\":1,\"lvlId\":1,\"actSW\":\"Y\",\"val\":\"Val1\",\"valType\":\"type1\",\"pctSW\":\"Y\",\"activeType\":\"Cd1\",\"logicOprtrId\":1,\"mathOprtrId\":5,\"ruleCat1Id\":1,\"ruleCat2Id\":2}]";
		// assertEquals(expected, mapper.writeValueAsString(lstRuleDefinationVOs));
	}

	/**
	 * Test convert pricing rules v o3.
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testConvertPricingRulesVO3() throws JsonGenerationException, JsonMappingException, IOException {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActTypCd("Cd1");
		assortmentRulesVO.setValType("type1");
		assortmentRulesVO.setLvlId(1l);
		assortmentRulesVO.setPctSW("Y");
		List<String> values = new ArrayList<String>();
		values.add("Start");
		values.add("");
		assortmentRulesVO.setValues(values);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Convert.convertPricingRulesVO(assortmentRulesVO, lstRuleDefinationVOs);
		ObjectMapper mapper = new ObjectMapper();
		// String expected =
		// "[{\"ruleId\":123,\"seq_nbr\":1,\"lvlId\":1,\"actSW\":\"Y\",\"val\":\"Start\",\"valType\":\"type1\",\"pctSW\":\"N\",\"activeType\":\"Cd1\",\"logicOprtrId\":1,\"mathOprtrId\":1,\"ruleCat1Id\":1,\"ruleCat2Id\":2},{\"ruleId\":123,\"seq_nbr\":2,\"lvlId\":1,\"actSW\":\"Y\",\"val\":\"0\",\"valType\":\"type1\",\"pctSW\":\"Y\",\"activeType\":\"Cd1\",\"logicOprtrId\":1,\"mathOprtrId\":2,\"ruleCat1Id\":1,\"ruleCat2Id\":2}]";
		// assertEquals(expected, mapper.writeValueAsString(lstRuleDefinationVOs));
	}

	/**
	 * Test convert pricing rules v o4.
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testConvertPricingRulesVO4() throws JsonGenerationException, JsonMappingException, IOException {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActTypCd("Cd1");
		assortmentRulesVO.setValType("type1");
		assortmentRulesVO.setLvlId(1l);
		assortmentRulesVO.setPctSW("N");
		List<String> values = new ArrayList<String>();
		values.add("Start");
		values.add("");
		assortmentRulesVO.setValues(values);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Convert.convertPricingRulesVO(assortmentRulesVO, lstRuleDefinationVOs);
		ObjectMapper mapper = new ObjectMapper();
		// String expected =
		// "[{\"ruleId\":123,\"seq_nbr\":1,\"lvlId\":1,\"actSW\":\"Y\",\"val\":\"Start\",\"valType\":\"type1\",\"pctSW\":\"N\",\"activeType\":\"Cd1\",\"logicOprtrId\":1,\"mathOprtrId\":1,\"ruleCat1Id\":1,\"ruleCat2Id\":2},{\"ruleId\":123,\"seq_nbr\":2,\"lvlId\":1,\"actSW\":\"Y\",\"val\":\"0\",\"valType\":\"type1\",\"pctSW\":\"N\",\"activeType\":\"Cd1\",\"logicOprtrId\":1,\"mathOprtrId\":2,\"ruleCat1Id\":1,\"ruleCat2Id\":2}]";
		// assertEquals(expected, mapper.writeValueAsString(lstRuleDefinationVOs));
	}
}
