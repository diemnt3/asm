package com.heb.enterprise.dsv.assortment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.ui.ModelMap;

import com.heb.enterprise.dsv.assortment.service.CostReviewService;

/**
 * The Class RetailCostControllerTest.
 * @author quang.phan
 */
// @RunWith(MockitoJUnitRunner.class)
public class RetailCostControllerTest {

	@InjectMocks
	private CostReviewController retailCostController = new CostReviewController();

	@Mock
	private CostReviewService retailCostService;

	@Mock
	private HttpSession session;

	@Mock
	private HttpServletRequest request;

	@Mock
	private ModelMap model;

	/**
	 * Test get product with cost changes1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	// @Test
	// public void testGetProductWithCostChanges1() throws DSVException {
	// RetailCostSearchVO costSearchVO = new RetailCostSearchVO();
	// costSearchVO.setDraw("XYZ");
	// String tabActive = "tabA";
	// Mockito.when(this.retailCostService.getProductWithCostChanges(costSearchVO, tabActive)).thenReturn(null);
	// Mockito.when(this.session.getAttribute("passed")).thenReturn(null);
	// Mockito.when(this.session.getAttribute("failed")).thenReturn(null);
	// Map<String, Integer> totalResultForEachStatus = new HashMap<String, Integer>();
	// totalResultForEachStatus.put("tabB", 123);
	// Mockito.when(this.retailCostService.totalOfResultForEachStatus(costSearchVO, null)).thenReturn(totalResultForEachStatus);
	// String actual = this.retailCostController.getProductWithCostChanges(costSearchVO, tabActive, this.request, this.session, this.model);
	// String expected = "{\"draw\":\"XYZ\",\"data\":[],\"tabB\":123}";
	// assertEquals(expected, actual);
	// }
	//
	// /**
	// * Test get product with cost changes2.
	// * @throws DSVException
	// * the DSV exception
	// * @author quang.phan
	// */
	// @Test
	// public void testGetProductWithCostChanges2() throws DSVException {
	// RetailCostSearchVO costSearchVO = new RetailCostSearchVO();
	// costSearchVO.setDraw("XYZ");
	// String tabActive = "tabA";
	// Mockito.when(this.retailCostService.getProductWithCostChanges(costSearchVO, tabActive)).thenReturn(null);
	// Mockito.when(this.session.getAttribute("passed")).thenReturn(null);
	// Mockito.when(this.session.getAttribute("failed")).thenReturn(null);
	// Map<String, Integer> totalResultForEachStatus = new HashMap<String, Integer>();
	// totalResultForEachStatus.put("tabA", 123);
	// Mockito.when(this.retailCostService.totalOfResultForEachStatus(costSearchVO, null)).thenReturn(totalResultForEachStatus);
	// String actual = this.retailCostController.getProductWithCostChanges(costSearchVO, tabActive, this.request, this.session, this.model);
	// String expected = "{\"draw\":\"XYZ\",\"recordsFiltered\":30,\"data\":[],\"recordsTotal\":30,\"tabA\":123}";
	// assertEquals(expected, actual);
	// }
	//
	// /**
	// * Test get product with cost changes3.
	// * @throws DSVException
	// * the DSV exception
	// * @author quang.phan
	// */
	// @Test
	// public void testGetProductWithCostChanges3() throws DSVException {
	// RetailCostSearchVO costSearchVO = new RetailCostSearchVO();
	// costSearchVO.setDraw("XYZ");
	// String tabActive = "tabA";
	// Mockito.when(this.retailCostService.getProductWithCostChanges(costSearchVO, tabActive)).thenReturn(null);
	// Mockito.when(this.session.getAttribute("passed")).thenReturn(0);
	// Mockito.when(this.session.getAttribute("failed")).thenReturn(1);
	// Map<String, Integer> totalResultForEachStatus = new HashMap<String, Integer>();
	// totalResultForEachStatus.put("tabA", 123);
	// Mockito.when(this.retailCostService.totalOfResultForEachStatus(costSearchVO, tabActive)).thenReturn(totalResultForEachStatus);
	// String actual = this.retailCostController.getProductWithCostChanges(costSearchVO, tabActive, this.request, this.session, this.model);
	// String expected = "{\"draw\":\"XYZ\",\"recordsFiltered\":123,\"passed\":0,\"failed\":1,\"data\":[],\"recordsTotal\":123}";
	// assertEquals(expected, actual);
	// }
	//
	// /**
	// * Test get product with cost changes4.
	// * @throws DSVException
	// * the DSV exception
	// * @author quang.phan
	// */
	// @Test
	// public void testGetProductWithCostChanges4() throws DSVException {
	// RetailCostSearchVO costSearchVO = new RetailCostSearchVO();
	// costSearchVO.setDraw("XYZ");
	// String tabActive = "tabA";
	// Mockito.when(this.retailCostService.getProductWithCostChanges(costSearchVO, tabActive)).thenReturn(null);
	// Mockito.when(this.session.getAttribute("passed")).thenReturn(0);
	// Mockito.when(this.session.getAttribute("failed")).thenReturn(1);
	// Map<String, Integer> totalResultForEachStatus = new HashMap<String, Integer>();
	// totalResultForEachStatus.put("passed", 0);
	// totalResultForEachStatus.put("failed", 1);
	// Mockito.when(this.retailCostService.totalOfResultForEachStatus(costSearchVO, tabActive)).thenReturn(totalResultForEachStatus);
	// String actual = this.retailCostController.getProductWithCostChanges(costSearchVO, tabActive, this.request, this.session, this.model);
	// System.out.println("Actual: " + actual);
	// String expected = "{\"draw\":\"XYZ\",\"recordsFiltered\":1,\"passed\":0,\"failed\":1,\"data\":[],\"recordsTotal\":1}";
	// assertEquals(expected, actual);
	// }
	//
	// /**
	// * Test get product with cost changes5.
	// * @throws DSVException
	// * the DSV exception
	// * @author quang.phan
	// */
	// @Test
	// public void testGetProductWithCostChanges5() throws DSVException {
	// RetailCostSearchVO costSearchVO = new RetailCostSearchVO();
	// costSearchVO.setDraw("XYZ");
	// String tabActive = "tabA";
	// List<RetailCostVO> lstResult = new ArrayList<RetailCostVO>();
	// RetailCostVO retailCostVO = new RetailCostVO();
	// retailCostVO.setProductID(123);
	// retailCostVO.setProdDesc("Cocacola");
	// retailCostVO.setPackSize("10");
	// retailCostVO.setCurrentCost(100.0);
	// retailCostVO.setCurrentRetail(50.0);
	// retailCostVO.setCurrentMap("ABC");
	// retailCostVO.setCurrentComp("COMP");
	// retailCostVO.setCurrentGP(19.00);
	// retailCostVO.setNextCost(5.0);
	// retailCostVO.setNextRetail(10.0);
	// retailCostVO.setNextGP(10.0);
	// retailCostVO.setNextMap("Map");
	// retailCostVO.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails1 = new CompetitiveRetails();
	// competitiveRetails1.setCompetitor("Walmart");
	// CompetitiveRetails competitiveRetails2 = new CompetitiveRetails();
	// competitiveRetails2.setCompetitor("Amazon");
	// lstCompetitiveRetials.add(competitiveRetails1);
	// lstCompetitiveRetials.add(competitiveRetails2);
	// retailCostVO.setLstCompetitor(lstCompetitiveRetials);
	// lstResult.add(retailCostVO);
	//
	// Mockito.when(this.request.getParameter("order[0][dir]")).thenReturn("desc");
	// Mockito.when(this.request.getParameter("order[0][column]")).thenReturn("desc");
	// Mockito.when(this.retailCostService.getProductWithCostChanges(costSearchVO, tabActive)).thenReturn(lstResult);
	// Mockito.when(this.session.getAttribute("passed")).thenReturn(null);
	// Mockito.when(this.session.getAttribute("failed")).thenReturn(null);
	//
	// Map<String, Integer> totalResultForEachStatus = new HashMap<String, Integer>();
	// totalResultForEachStatus.put("tabA", 123);
	// Mockito.when(this.retailCostService.totalOfResultForEachStatus(costSearchVO, null)).thenReturn(totalResultForEachStatus);
	// String actual = this.retailCostController.getProductWithCostChanges(costSearchVO, tabActive, this.request, this.session, this.model);
	// String expected =
	// "{\"draw\":\"XYZ\",\"recordsFiltered\":30,\"data\":[{\"desc\":\"Cocacola\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"Walmart\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null},{\"competitor\":\"Amazon\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":51.0,\"nextRetail\":13.0,\"nextCost\":5.0,\"DT_RowId\":123,\"currentGP\":19.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":102.0,\"nextGP\":\"10.00\"}],\"recordsTotal\":30,\"tabA\":123}";
	// // assertEquals(expected, actual);
	// }
	//
	// /**
	// * Test get product with cost changes6.
	// * @throws DSVException
	// * the DSV exception
	// * @author quang.phan
	// */
	// @Test
	// public void testGetProductWithCostChanges6() throws DSVException {
	// RetailCostSearchVO costSearchVO = new RetailCostSearchVO();
	// costSearchVO.setDraw("XYZ");
	// String tabActive = "tabA";
	// List<RetailCostVO> lstResult = new ArrayList<RetailCostVO>();
	//
	// RetailCostVO retailCostVO1 = new RetailCostVO();
	// retailCostVO1.setProductID(123);
	// retailCostVO1.setProdDesc("Cocacola");
	// retailCostVO1.setPackSize("10");
	// retailCostVO1.setCurrentCost(100.0);
	// retailCostVO1.setCurrentRetail(50.0);
	// retailCostVO1.setCurrentMap("ABC");
	// retailCostVO1.setCurrentComp("COMP");
	// retailCostVO1.setCurrentGP(19.00);
	// retailCostVO1.setNextCost(5.0);
	// retailCostVO1.setNextRetail(10.0);
	// retailCostVO1.setNextGP(10.0);
	// retailCostVO1.setNextMap("Map");
	// retailCostVO1.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials1 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails1 = new CompetitiveRetails();
	// competitiveRetails1.setCompetitor("Walmart");
	// lstCompetitiveRetials1.add(competitiveRetails1);
	// retailCostVO1.setLstCompetitor(lstCompetitiveRetials1);
	// lstResult.add(retailCostVO1);
	//
	// RetailCostVO retailCostVO2 = new RetailCostVO();
	// retailCostVO2.setProductID(124);
	// retailCostVO2.setProdDesc("Pepsi");
	// retailCostVO2.setPackSize("10");
	// retailCostVO2.setCurrentCost(100.0);
	// retailCostVO2.setCurrentRetail(50.0);
	// retailCostVO2.setCurrentMap("ABC");
	// retailCostVO2.setCurrentComp("COMP");
	// retailCostVO2.setCurrentGP(19.00);
	// retailCostVO2.setNextCost(5.0);
	// retailCostVO2.setNextRetail(10.0);
	// retailCostVO2.setNextGP(10.0);
	// retailCostVO2.setNextMap("Map");
	// retailCostVO2.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials2 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails2 = new CompetitiveRetails();
	// competitiveRetails2.setCompetitor("Amazon");
	// lstCompetitiveRetials2.add(competitiveRetails2);
	// retailCostVO2.setLstCompetitor(lstCompetitiveRetials2);
	// lstResult.add(retailCostVO2);
	//
	// RetailCostVO retailCostVO3 = new RetailCostVO();
	// retailCostVO3.setProductID(125);
	// retailCostVO3.setProdDesc("Fanta");
	// retailCostVO3.setPackSize("10");
	// retailCostVO3.setCurrentCost(100.0);
	// retailCostVO3.setCurrentRetail(50.0);
	// retailCostVO3.setCurrentMap("ABC");
	// retailCostVO3.setCurrentComp("COMP");
	// retailCostVO3.setCurrentGP(19.00);
	// retailCostVO3.setNextCost(5.0);
	// retailCostVO3.setNextRetail(10.0);
	// retailCostVO3.setNextGP(10.0);
	// retailCostVO3.setNextMap("Map");
	// retailCostVO3.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials3 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails3 = new CompetitiveRetails();
	// competitiveRetails3.setCompetitor("BigC");
	// lstCompetitiveRetials3.add(competitiveRetails3);
	// retailCostVO3.setLstCompetitor(lstCompetitiveRetials3);
	// lstResult.add(retailCostVO3);
	//
	// RetailCostVO retailCostVO4 = new RetailCostVO();
	// retailCostVO4.setProductID(126);
	// retailCostVO4.setProdDesc("String");
	// retailCostVO4.setPackSize("10");
	// retailCostVO4.setCurrentCost(100.0);
	// retailCostVO4.setCurrentRetail(50.0);
	// retailCostVO4.setCurrentMap("ABC");
	// retailCostVO4.setCurrentComp("COMP");
	// retailCostVO4.setCurrentGP(19.00);
	// retailCostVO4.setNextCost(5.0);
	// retailCostVO4.setNextRetail(10.0);
	// retailCostVO4.setNextGP(10.0);
	// retailCostVO4.setNextMap("Map");
	// retailCostVO4.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials4 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails4 = new CompetitiveRetails();
	// competitiveRetails2.setCompetitor("Metro");
	// lstCompetitiveRetials4.add(competitiveRetails4);
	// retailCostVO4.setLstCompetitor(lstCompetitiveRetials4);
	// lstResult.add(retailCostVO4);
	//
	// RetailCostVO retailCostVO5 = new RetailCostVO();
	// retailCostVO5.setProductID(127);
	// retailCostVO5.setProdDesc("Big cola");
	// retailCostVO5.setPackSize("10");
	// retailCostVO5.setCurrentCost(100.0);
	// retailCostVO5.setCurrentRetail(50.0);
	// retailCostVO5.setCurrentMap("ABC");
	// retailCostVO5.setCurrentComp("COMP");
	// retailCostVO5.setCurrentGP(19.00);
	// retailCostVO5.setNextCost(5.0);
	// retailCostVO5.setNextRetail(10.0);
	// retailCostVO5.setNextGP(10.0);
	// retailCostVO5.setNextMap("Map");
	// retailCostVO5.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials5 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails5 = new CompetitiveRetails();
	// competitiveRetails5.setCompetitor("eBay");
	// lstCompetitiveRetials5.add(competitiveRetails5);
	// retailCostVO5.setLstCompetitor(lstCompetitiveRetials5);
	// lstResult.add(retailCostVO5);
	//
	// RetailCostVO retailCostVO6 = new RetailCostVO();
	// retailCostVO6.setProductID(124);
	// retailCostVO6.setProdDesc("Pepsi");
	// retailCostVO6.setPackSize("10");
	// retailCostVO6.setCurrentCost(100.0);
	// retailCostVO6.setCurrentRetail(50.0);
	// retailCostVO6.setCurrentMap("ABC");
	// retailCostVO6.setCurrentComp("COMP");
	// retailCostVO6.setCurrentGP(19.00);
	// retailCostVO6.setNextCost(5.0);
	// retailCostVO6.setNextRetail(10.0);
	// retailCostVO6.setNextGP(10.0);
	// retailCostVO6.setNextMap("Map");
	// retailCostVO6.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials6 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails6 = new CompetitiveRetails();
	// competitiveRetails6.setCompetitor("Amazon");
	// lstCompetitiveRetials6.add(competitiveRetails6);
	// retailCostVO6.setLstCompetitor(lstCompetitiveRetials6);
	// lstResult.add(retailCostVO6);
	//
	// Mockito.when(this.request.getParameter("order[0][dir]")).thenReturn("desc");
	// Mockito.when(this.request.getParameter("order[0][column]")).thenReturn("desc");
	// Mockito.when(this.retailCostService.getProductWithCostChanges(costSearchVO, tabActive)).thenReturn(lstResult);
	// Mockito.when(this.session.getAttribute("passed")).thenReturn(null);
	// Mockito.when(this.session.getAttribute("failed")).thenReturn(null);
	//
	// Map<String, Integer> totalResultForEachStatus = new HashMap<String, Integer>();
	// totalResultForEachStatus.put("tabA", 123);
	// Mockito.when(this.retailCostService.totalOfResultForEachStatus(costSearchVO, null)).thenReturn(totalResultForEachStatus);
	// String actual = this.retailCostController.getProductWithCostChanges(costSearchVO, tabActive, this.request, this.session, this.model);
	// String expected =
	// "{\"draw\":\"XYZ\",\"recordsFiltered\":30,\"data\":[{\"desc\":\"Cocacola\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"Walmart\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":51.0,\"nextRetail\":13.0,\"nextCost\":5.0,\"DT_RowId\":123,\"currentGP\":19.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":102.0,\"nextGP\":\"10.00\"},{\"desc\":\"Pepsi\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"Metro\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":52.0,\"nextRetail\":14.0,\"nextCost\":6.0,\"DT_RowId\":124,\"currentGP\":20.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":103.0,\"nextGP\":\"10.00\"},{\"desc\":\"Fanta\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"BigC\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":53.0,\"nextRetail\":15.0,\"nextCost\":7.0,\"DT_RowId\":125,\"currentGP\":21.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":104.0,\"nextGP\":\"10.00\"},{\"desc\":\"String\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":null,\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":54.0,\"nextRetail\":16.0,\"nextCost\":8.0,\"DT_RowId\":126,\"currentGP\":22.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":105.0,\"nextGP\":\"10.00\"},{\"desc\":\"Big cola\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"eBay\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":55.0,\"nextRetail\":17.0,\"nextCost\":9.0,\"DT_RowId\":127,\"currentGP\":23.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":106.0,\"nextGP\":\"10.00\"},{\"desc\":\"Pepsi\",\"sizePack\":\"10\",\"futureCostRetail\":null,\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"Amazon\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":50.0,\"nextRetail\":10.0,\"nextCost\":5.0,\"DT_RowId\":124,\"currentGP\":19.0,\"currentComp\":\"COMP\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":100.0,\"nextGP\":\"10.00\"}],\"recordsTotal\":30,\"tabA\":123}";
	// // assertEquals(expected, actual);
	// }
	//
	// /**
	// * Test get product with cost changes7.
	// * @throws DSVException
	// * the DSV exception
	// * @author quang.phan
	// */
	// @Test
	// public void testGetProductWithCostChanges7() throws DSVException {
	// RetailCostSearchVO costSearchVO = new RetailCostSearchVO();
	// costSearchVO.setDraw("XYZ");
	// String tabActive = "tabA";
	// List<RetailCostVO> lstResult = new ArrayList<RetailCostVO>();
	//
	// RetailCostVO retailCostVO1 = new RetailCostVO();
	// retailCostVO1.setProductID(123);
	// retailCostVO1.setProdDesc("Cocacola");
	// retailCostVO1.setPackSize("10");
	// retailCostVO1.setCurrentCost(100.0);
	// retailCostVO1.setCurrentRetail(50.0);
	// retailCostVO1.setCurrentMap("ABC");
	// retailCostVO1.setCurrentComp("COMP");
	// retailCostVO1.setCurrentGP(19.00);
	// retailCostVO1.setNextCost(5.0);
	// retailCostVO1.setNextRetail(10.0);
	// retailCostVO1.setNextGP(10.0);
	// retailCostVO1.setNextMap("Map");
	// retailCostVO1.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials1 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails1 = new CompetitiveRetails();
	// competitiveRetails1.setCompetitor("Walmart");
	// lstCompetitiveRetials1.add(competitiveRetails1);
	// retailCostVO1.setLstCompetitor(lstCompetitiveRetials1);
	// lstResult.add(retailCostVO1);
	//
	// RetailCostVO retailCostVO2 = new RetailCostVO();
	// retailCostVO2.setProductID(124);
	// retailCostVO2.setProdDesc("Pepsi");
	// retailCostVO2.setPackSize("10");
	// retailCostVO2.setCurrentCost(100.0);
	// retailCostVO2.setCurrentRetail(50.0);
	// retailCostVO2.setCurrentMap("ABC");
	// retailCostVO2.setCurrentComp("COMP");
	// retailCostVO2.setCurrentGP(19.00);
	// retailCostVO2.setNextCost(5.0);
	// retailCostVO2.setNextRetail(10.0);
	// retailCostVO2.setNextGP(10.0);
	// retailCostVO2.setNextMap("Map");
	// retailCostVO2.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials2 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails2 = new CompetitiveRetails();
	// competitiveRetails2.setCompetitor("Amazon");
	// lstCompetitiveRetials2.add(competitiveRetails2);
	// retailCostVO2.setLstCompetitor(lstCompetitiveRetials2);
	// lstResult.add(retailCostVO2);
	//
	// RetailCostVO retailCostVO3 = new RetailCostVO();
	// retailCostVO3.setProductID(125);
	// retailCostVO3.setProdDesc("Fanta");
	// retailCostVO3.setPackSize("10");
	// retailCostVO3.setCurrentCost(100.0);
	// retailCostVO3.setCurrentRetail(50.0);
	// retailCostVO3.setCurrentMap("ABC");
	// retailCostVO3.setCurrentComp("COMP");
	// retailCostVO3.setCurrentGP(19.00);
	// retailCostVO3.setNextCost(5.0);
	// retailCostVO3.setNextRetail(10.0);
	// retailCostVO3.setNextGP(10.0);
	// retailCostVO3.setNextMap("Map");
	// retailCostVO3.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials3 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails3 = new CompetitiveRetails();
	// competitiveRetails3.setCompetitor("BigC");
	// lstCompetitiveRetials3.add(competitiveRetails3);
	// retailCostVO3.setLstCompetitor(lstCompetitiveRetials3);
	// lstResult.add(retailCostVO3);
	//
	// RetailCostVO retailCostVO4 = new RetailCostVO();
	// retailCostVO4.setProductID(126);
	// retailCostVO4.setProdDesc("String");
	// retailCostVO4.setPackSize("10");
	// retailCostVO4.setCurrentCost(100.0);
	// retailCostVO4.setCurrentRetail(50.0);
	// retailCostVO4.setCurrentMap("ABC");
	// retailCostVO4.setCurrentComp("COMP");
	// retailCostVO4.setCurrentGP(19.00);
	// retailCostVO4.setNextCost(5.0);
	// retailCostVO4.setNextRetail(10.0);
	// retailCostVO4.setNextGP(10.0);
	// retailCostVO4.setNextMap("Map");
	// retailCostVO4.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials4 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails4 = new CompetitiveRetails();
	// competitiveRetails2.setCompetitor("Metro");
	// lstCompetitiveRetials4.add(competitiveRetails4);
	// retailCostVO4.setLstCompetitor(lstCompetitiveRetials4);
	// lstResult.add(retailCostVO4);
	//
	// RetailCostVO retailCostVO5 = new RetailCostVO();
	// retailCostVO5.setProductID(127);
	// retailCostVO5.setProdDesc("Big cola");
	// retailCostVO5.setPackSize("10");
	// retailCostVO5.setCurrentCost(100.0);
	// retailCostVO5.setCurrentRetail(50.0);
	// retailCostVO5.setCurrentMap("ABC");
	// retailCostVO5.setCurrentComp("COMP");
	// retailCostVO5.setCurrentGP(19.00);
	// retailCostVO5.setNextCost(5.0);
	// retailCostVO5.setNextRetail(10.0);
	// retailCostVO5.setNextGP(10.0);
	// retailCostVO5.setNextMap("Map");
	// retailCostVO5.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials5 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails5 = new CompetitiveRetails();
	// competitiveRetails5.setCompetitor("eBay");
	// lstCompetitiveRetials5.add(competitiveRetails5);
	// retailCostVO5.setLstCompetitor(lstCompetitiveRetials5);
	// lstResult.add(retailCostVO5);
	//
	// RetailCostVO retailCostVO6 = new RetailCostVO();
	// retailCostVO6.setProductID(124);
	// retailCostVO6.setProdDesc("Pepsi");
	// retailCostVO6.setPackSize("10");
	// retailCostVO6.setCurrentCost(100.0);
	// retailCostVO6.setCurrentRetail(50.0);
	// retailCostVO6.setCurrentMap("ABC");
	// retailCostVO6.setCurrentComp("COMP");
	// retailCostVO6.setCurrentGP(19.00);
	// retailCostVO6.setNextCost(5.0);
	// retailCostVO6.setNextRetail(10.0);
	// retailCostVO6.setNextGP(10.0);
	// retailCostVO6.setNextMap("Map");
	// retailCostVO6.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials6 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails6 = new CompetitiveRetails();
	// competitiveRetails6.setCompetitor("Apple");
	// lstCompetitiveRetials6.add(competitiveRetails6);
	// retailCostVO6.setLstCompetitor(lstCompetitiveRetials6);
	// lstResult.add(retailCostVO6);
	//
	// RetailCostVO retailCostVO7 = new RetailCostVO();
	// retailCostVO7.setProductID(124);
	// retailCostVO7.setProdDesc("Pepsi");
	// retailCostVO7.setPackSize("10");
	// retailCostVO7.setCurrentCost(100.0);
	// retailCostVO7.setCurrentRetail(50.0);
	// retailCostVO7.setCurrentMap("ABC");
	// retailCostVO7.setCurrentComp("COMP");
	// retailCostVO7.setCurrentGP(19.00);
	// retailCostVO7.setNextCost(5.0);
	// retailCostVO7.setNextRetail(10.0);
	// retailCostVO7.setNextGP(10.0);
	// retailCostVO7.setNextMap("Map");
	// retailCostVO7.setNextEffectiveDate(new Date());
	// List<CompetitiveRetails> lstCompetitiveRetials7 = new ArrayList<CompetitiveRetails>();
	// CompetitiveRetails competitiveRetails7 = new CompetitiveRetails();
	// competitiveRetails7.setCompetitor("Lotte Mart");
	// lstCompetitiveRetials7.add(competitiveRetails7);
	// retailCostVO7.setLstCompetitor(lstCompetitiveRetials7);
	// lstResult.add(retailCostVO7);
	//
	// Mockito.when(this.request.getParameter("order[0][dir]")).thenReturn("desc");
	// Mockito.when(this.request.getParameter("order[0][column]")).thenReturn("desc");
	// Mockito.when(this.retailCostService.getProductWithCostChanges(costSearchVO, tabActive)).thenReturn(lstResult);
	// Mockito.when(this.session.getAttribute("passed")).thenReturn(null);
	// Mockito.when(this.session.getAttribute("failed")).thenReturn(null);
	//
	// Map<String, Integer> totalResultForEachStatus = new HashMap<String, Integer>();
	// totalResultForEachStatus.put("tabA", 123);
	// Mockito.when(this.retailCostService.totalOfResultForEachStatus(costSearchVO, null)).thenReturn(totalResultForEachStatus);
	// String actual = this.retailCostController.getProductWithCostChanges(costSearchVO, tabActive, this.request, this.session, this.model);
	// String expected =
	// "{\"draw\":\"XYZ\",\"recordsFiltered\":30,\"data\":[{\"desc\":\"Cocacola\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"Walmart\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":51.0,\"nextRetail\":13.0,\"nextCost\":5.0,\"DT_RowId\":123,\"currentGP\":19.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":102.0,\"nextGP\":\"10.00\"},{\"desc\":\"Pepsi\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"Metro\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":52.0,\"nextRetail\":14.0,\"nextCost\":6.0,\"DT_RowId\":124,\"currentGP\":20.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":103.0,\"nextGP\":\"10.00\"},{\"desc\":\"Fanta\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"BigC\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":53.0,\"nextRetail\":15.0,\"nextCost\":7.0,\"DT_RowId\":125,\"currentGP\":21.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":104.0,\"nextGP\":\"10.00\"},{\"desc\":\"String\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":null,\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":54.0,\"nextRetail\":16.0,\"nextCost\":8.0,\"DT_RowId\":126,\"currentGP\":22.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":105.0,\"nextGP\":\"10.00\"},{\"desc\":\"Big cola\",\"sizePack\":\"10\",\"futureCostRetail\":\"Y\",\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"eBay\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":55.0,\"nextRetail\":17.0,\"nextCost\":9.0,\"DT_RowId\":127,\"currentGP\":23.0,\"currentComp\":\"Y\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":106.0,\"nextGP\":\"10.00\"},{\"desc\":\"Pepsi\",\"sizePack\":\"10\",\"futureCostRetail\":null,\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"Apple\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"N\",\"currentRetail\":50.0,\"nextRetail\":10.0,\"nextCost\":5.0,\"DT_RowId\":124,\"currentGP\":19.0,\"currentComp\":\"COMP\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":100.0,\"nextGP\":\"10.00\"},{\"desc\":\"Pepsi\",\"sizePack\":\"10\",\"futureCostRetail\":null,\"effectDate\":\"03/05/2015\",\"no\":\"\",\"competitors\":[{\"competitor\":\"Lotte Mart\",\"oldRetail\":null,\"newRetail\":null,\"oldDate\":null,\"newDate\":null}],\"nextMap\":\"Map\",\"currentMap\":\"ABC\",\"currentRetail\":59.0,\"nextRetail\":17.0,\"nextCost\":13.0,\"DT_RowId\":124,\"currentGP\":19.0,\"currentComp\":\"N\",\"informations\":\"Violate the Rule 'Only allow products where retail is between 1 and 10'\",\"currentCost\":106.0,\"nextGP\":\"16.00\"}],\"recordsTotal\":30,\"tabA\":123}";
	// // assertEquals(expected, actual);
	// }
	//
	// /**
	// * Test get future segments.
	// * @author quang.phan
	// */
	// @Test
	// public void testGetFutureSegments() throws DSVException {
	// this.retailCostController.getFutureSegments();
	// }
	//
	// /**
	// * Test save product.
	// * @author quang.phan
	// */
	// @Test
	// public void testSaveProduct() throws DSVException {
	// String jsonObject = "[{\"data\": 10}]";
	// this.retailCostController.saveProduct(jsonObject);
	// }
	//
	// /**
	// * Test remove product.
	// * @author quang.phan
	// */
	// @Test
	// public void testRemoveProduct() throws DSVException {
	// String jsonObject = "[{\"data\": 10}]";
	// this.retailCostController.removeProduct(jsonObject);
	// }
	//
	// /**
	// * Test reject products.
	// * @author quang.phan
	// */
	// @Test
	// public void testRejectProducts() throws DSVException {
	// String jsonObject = "[{\"data\": 10}]";
	// this.retailCostController.rejectProducts(jsonObject);
	// }
	//
	// /**
	// * Test show pricing rules screen.
	// * @author quang.phan
	// */
	// @Test
	// public void testShowPricingRulesScreen() throws DSVException {
	// this.retailCostController.showPricingRulesScreen();
	// }
}
