package com.heb.enterprise.dsv.assortment.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.assortment.dao.GlobalRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.RulesDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.AttrAndRuleVendorVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;
import com.heb.enterprise.dsv.vo.VendorAttributeVO;

/**
 * The class <code>VendorRulesServiceImplTest</code> contains tests for the class <code>{@link VendorRulesServiceImpl}</code>.
 * @author thangdang
 * @author thangdang
 * @version $Revision: 1.14 $
 */
@RunWith(MockitoJUnitRunner.class)
public class VendorRulesServiceImplTest {
	@InjectMocks
	private VendorRulesServiceImpl vendorRulesServiceImpl = new VendorRulesServiceImpl();
	@Mock
	private CacheService cacheService;
	/**
	 * inject RulesDao.
	 */
	@Mock
	private RulesDAO ruleDao;
	/**
	 * inject RulesDao.
	 */
	@Mock
	private GlobalRulesDAO globalDao;
	private String vendorId = "52765";

	@Test
	public void testAjaxGetCommodityandSub() throws DSVException {
		this.vendorRulesServiceImpl.ajaxGetCommodityandSub();
	}

	/**
	 * Run the VendorRulesServiceImpl() constructor test.
	 * @author thangdang
	 */
	@Test
	public void testVendorRulesServiceImpl_1() throws DSVException {
		assertNotNull(vendorRulesServiceImpl);

	}

	/**
	 * Run the List<Integer> getActiveSw(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetActiveSw_1() throws DSVException {
		Mockito.when(this.ruleDao.getActiveSw(vendorId)).thenReturn(null);
		List<Integer> result = this.vendorRulesServiceImpl.getActiveSw(vendorId);
		org.junit.Assert.assertEquals("Error result not same", null, result);
	}

	/**
	 * Run the List<Integer> getActiveSw(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetActiveSw_2() throws DSVException {
		List<Integer> lstActual = new ArrayList<Integer>();
		Mockito.when(this.ruleDao.getActiveSw(vendorId)).thenReturn(lstActual);
		List<Integer> result = this.vendorRulesServiceImpl.getActiveSw(vendorId);
		org.junit.Assert.assertEquals("Error result not same", lstActual, result);
	}

	/**
	 * Run the AttrAndRuleVendorVO getRuleSettingsByVendor(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetRuleSettingsByVendor_1() throws DSVException {
		Mockito.when(this.ruleDao.getRuleSettingsByVendor(vendorId)).thenReturn(null);
		AttrAndRuleVendorVO result = this.vendorRulesServiceImpl.getRuleSettingsByVendor(vendorId);
		org.junit.Assert.assertEquals("Error result not same", null, result);
	}

	/**
	 * Run the AttrAndRuleVendorVO getRuleSettingsByVendor(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetRuleSettingsByVendor_2() throws DSVException {
		AttrAndRuleVendorVO attrAndRuleVendorVO = new AttrAndRuleVendorVO();
		Mockito.when(this.ruleDao.getRuleSettingsByVendor(vendorId)).thenReturn(attrAndRuleVendorVO);
		AttrAndRuleVendorVO result = this.vendorRulesServiceImpl.getRuleSettingsByVendor(vendorId);
		org.junit.Assert.assertEquals("Error result not same", attrAndRuleVendorVO, result);
	}

	/**
	 * Run the Map<String, AssortmentRulesVO> getRuleSettingsForVendor(String,String,String,String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetRuleSettingsForVendor_1() throws DSVException {
		String commodityId = "23";
		String subCommodityId = "4";
		String subDept = "07A";
		String deptId = "07";
		String subDeptId = "A";
		Mockito.when(this.ruleDao.getRuleSettings(vendorId, commodityId, subCommodityId, deptId, subDeptId)).thenReturn(null);
		// Map<String, AssortmentRulesVO> result = this.vendorRulesServiceImpl.getRuleSettingsForVendor(vendorId, commodityId, subCommodityId, subDept);
		// org.junit.Assert.assertEquals("Error result not same", null, result);
	}

	/**
	 * Run the Map<String, AssortmentRulesVO> getRuleSettingsForVendor(String,String,String,String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetRuleSettingsForVendor_2() throws DSVException {
		String commodityId = "23";
		String subCommodityId = "4";
		String subDept = "07A";
		String deptId = "07";
		String subDeptId = "A";
		Map<String, AssortmentRulesVO> resultActual = new HashMap<String, AssortmentRulesVO>();
		Mockito.when(this.ruleDao.getRuleSettings(vendorId, commodityId, subCommodityId, deptId, subDeptId)).thenReturn(resultActual);
		// Map<String, AssortmentRulesVO> result = this.vendorRulesServiceImpl.getRuleSettingsForVendor(vendorId, commodityId, subCommodityId, subDept);
		// org.junit.Assert.assertEquals("Error result not same", resultActual, result);
	}

	/**
	 * Test test get rule settings for vendor.
	 * @author quang.phan
	 * @throws DSVException
	 *             the DSV exception
	 */
	@Test
	public void testTestGetRuleSettingsForVendor() throws DSVException {
		String subCommodityId = "123";
		String vendorId = "a1234";
		String commodityId = "999";
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("897");
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptIdOri("999");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		BigDecimal classCode = new BigDecimal("10");
		classVO.setClassCode(classCode);
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(25);
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(123);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(departmentVOs);
		Map<String, AssortmentRulesVO> actual = this.vendorRulesServiceImpl.getRuleSettingsForVendor(vendorId, commodityId, subCommodityId);
		assertEquals(0, actual.size());
	}

	/**
	 * Run the List<VendorAttributeVO> getVendorAttr() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetVendorAttr_1() throws DSVException {
		Mockito.when(this.ruleDao.getVendorAttr()).thenReturn(null);
		List<VendorAttributeVO> result = this.vendorRulesServiceImpl.getVendorAttr();
		org.junit.Assert.assertEquals("Error result not same", null, result);
	}

	/**
	 * Run the List<VendorAttributeVO> getVendorAttr() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetVendorAttr_2() throws DSVException {
		List<VendorAttributeVO> lstActual = new ArrayList<VendorAttributeVO>();
		Mockito.when(this.ruleDao.getVendorAttr()).thenReturn(lstActual);
		List<VendorAttributeVO> result = this.vendorRulesServiceImpl.getVendorAttr();
		org.junit.Assert.assertEquals("Error result not same", lstActual, result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String,String,String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_1() throws DSVException {
		String commodityId = "23";
		String subCommodityId = "4";
		long lvlId = 0;
		Mockito.when(this.ruleDao.checkLVL(vendorId, commodityId, subCommodityId)).thenReturn(lvlId);
		boolean result = this.vendorRulesServiceImpl.resetRulesToHigherLevel(vendorId, commodityId, subCommodityId);
		assertTrue(result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String,String,String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_2() throws DSVException {
		String commodityId = "23";
		String subCommodityId = "4";
		long lvlId = 1;
		Mockito.when(this.ruleDao.checkLVL(vendorId, commodityId, subCommodityId)).thenReturn(lvlId);
		Mockito.when(this.globalDao.removeRule(lvlId)).thenReturn(true);
		Mockito.when(this.globalDao.removeLvl(lvlId)).thenReturn(true);
		boolean result = this.vendorRulesServiceImpl.resetRulesToHigherLevel(vendorId, commodityId, subCommodityId);
		assertTrue(result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String,String,String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_3() throws DSVException {
		String commodityId = "23";
		String subCommodityId = "4";
		Mockito.when(this.ruleDao.checkLVL(vendorId, commodityId, subCommodityId)).thenThrow(new DSVException("ERROR"));
		boolean result = this.vendorRulesServiceImpl.resetRulesToHigherLevel(vendorId, commodityId, subCommodityId);
		assertTrue(!result);
	}

	/**
	 * Run the boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_1() throws DSVException {
		AttrAndRuleVendorVO vendorAsmVo = new AttrAndRuleVendorVO();
		List<AssortmentRulesVO> lstVendorRule = new ArrayList<AssortmentRulesVO>();
		vendorAsmVo.setVendorId("52765");
		vendorAsmVo.setUserId("");
		long lvlId = 1;

		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setLvlId(lvlId);
		lstVendorRule.add(assortmentRulesVO);
		vendorAsmVo.setLstVendorRules(lstVendorRule);
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		List<String> lstHebSubComActivated = new ArrayList<String>();
		List<String> lstSubCommodityDeActivated = new ArrayList<String>();
		lstSubCommodityDeActivated.add(AntiMagicNumber.STRING_ONE);
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		lstAssortmentRulesVOs.add(assortmentRulesVO);

		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		lstHebSubComActivated.add(AntiMagicNumber.STRING_ONE);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.ruleDao.getSubCommodityActivated(vendorAsmVo.getVendorId())).thenReturn(lstHebSubComActivated);
		Mockito.when(this.ruleDao.removeSubCommodityActivated(vendorAsmVo.getVendorId(), lstSubCommodityDeActivated)).thenReturn(true);
		Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvlActivated(vendorAsmVo.getUserId(), lstAssortmentRulesVOs, vendorAsmVo.getVendorId(), lvlId)).thenReturn("");
		// Mockito.when(this.ruleDao.checkLVL(vendorAsmVo.getVendorId(), vendorAsmVo.getCommodityId(), vendorAsmVo.getSubCommdId())).thenReturn(lvlId);
		// Mockito.when(this.ruleDao.updateRuleValuesToDfns(lstRuleDefinationVOs, lvlId, vendorAsmVo.getUserId())).thenReturn("");
		// boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(vendorAsmVo);
		// assertTrue(result);
	}

	/**
	 * Run the boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_2() throws DSVException {
		AttrAndRuleVendorVO vendorAsmVo = new AttrAndRuleVendorVO();
		List<AssortmentRulesVO> lstVendorRule = new ArrayList<AssortmentRulesVO>();
		long lvlId = 1;
		vendorAsmVo.setVendorId("52765");
		vendorAsmVo.setUserId("");
		vendorAsmVo.setCommodityId("3");
		vendorAsmVo.setSubCommdId("3");

		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setLvlId(lvlId);
		lstVendorRule.add(assortmentRulesVO);
		vendorAsmVo.setLstVendorRules(lstVendorRule);
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		List<String> lstHebSubComActivated = new ArrayList<String>();
		List<String> lstSubCommodityDeActivated = new ArrayList<String>();
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		lstHebSubComActivated.add(AntiMagicNumber.STRING_ONE);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.ruleDao.getSubCommodityActivated(vendorAsmVo.getVendorId())).thenReturn(lstHebSubComActivated);
		Mockito.when(this.ruleDao.removeSubCommodityActivated(vendorAsmVo.getVendorId(), lstSubCommodityDeActivated)).thenReturn(true);
		Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvlActivated(vendorAsmVo.getUserId(), lstAssortmentRulesVOs, vendorAsmVo.getVendorId(), lvlId)).thenReturn("");
		// Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvl(lvlId, vendorAsmVo.getUserId(), vendorAsmVo.getCommodityId(), null, null, 0, vendorId, vendorAsmVo.getSubCommdId(), null)).thenReturn(lvlId);
		// boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(vendorAsmVo);
		// assertTrue(result);
	}

	/**
	 * Run the boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_3() throws DSVException {
		AttrAndRuleVendorVO vendorAsmVo = new AttrAndRuleVendorVO();
		List<AssortmentRulesVO> lstVendorRule = new ArrayList<AssortmentRulesVO>();
		vendorAsmVo.setVendorId("52765");
		vendorAsmVo.setUserId("");
		vendorAsmVo.setSubCommdId("3");
		long lvlId = 1;

		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setLvlId(lvlId);
		lstVendorRule.add(assortmentRulesVO);
		vendorAsmVo.setLstVendorRules(lstVendorRule);
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		List<String> lstHebSubComActivated = new ArrayList<String>();
		List<String> lstSubCommodityDeActivated = new ArrayList<String>();
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		lstHebSubComActivated.add(AntiMagicNumber.STRING_ONE);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.ruleDao.getSubCommodityActivated(vendorAsmVo.getVendorId())).thenReturn(lstHebSubComActivated);
		Mockito.when(this.ruleDao.removeSubCommodityActivated(vendorAsmVo.getVendorId(), lstSubCommodityDeActivated)).thenReturn(true);
		Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvlActivated(vendorAsmVo.getUserId(), lstAssortmentRulesVOs, vendorAsmVo.getVendorId(), lvlId)).thenReturn("");
		// Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvl(lvlId, vendorAsmVo.getUserId(), vendorAsmVo.getCommodityId(), null, null, 0, vendorId, vendorAsmVo.getSubCommdId(), null)).thenReturn(lvlId);
		// boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(vendorAsmVo);
		// assertTrue(result);
	}

	/**
	 * Run the boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_4() throws DSVException {
		AttrAndRuleVendorVO vendorAsmVo = new AttrAndRuleVendorVO();
		List<AssortmentRulesVO> lstVendorRule = new ArrayList<AssortmentRulesVO>();
		vendorAsmVo.setVendorId("52765");
		vendorAsmVo.setUserId("");
		long lvlId = 1;
		vendorAsmVo.setCommodityId("3");

		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setLvlId(lvlId);
		lstVendorRule.add(assortmentRulesVO);
		vendorAsmVo.setLstVendorRules(lstVendorRule);
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		List<String> lstHebSubComActivated = new ArrayList<String>();
		List<String> lstSubCommodityDeActivated = new ArrayList<String>();
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		lstHebSubComActivated.add(AntiMagicNumber.STRING_ONE);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.ruleDao.getSubCommodityActivated(vendorAsmVo.getVendorId())).thenReturn(lstHebSubComActivated);
		Mockito.when(this.ruleDao.removeSubCommodityActivated(vendorAsmVo.getVendorId(), lstSubCommodityDeActivated)).thenReturn(true);
		Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvlActivated(vendorAsmVo.getUserId(), lstAssortmentRulesVOs, vendorAsmVo.getVendorId(), lvlId)).thenReturn("");
		// Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvl(lvlId, vendorAsmVo.getUserId(), vendorAsmVo.getCommodityId(), null, null, 0, vendorId, vendorAsmVo.getSubCommdId(), null)).thenReturn(lvlId);
		// boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(vendorAsmVo);
		// assertTrue(result);
	}

	/**
	 * Run the boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_5() throws DSVException {
		AttrAndRuleVendorVO vendorAsmVo = new AttrAndRuleVendorVO();
		List<AssortmentRulesVO> lstVendorRule = new ArrayList<AssortmentRulesVO>();
		vendorAsmVo.setVendorId("52765");
		vendorAsmVo.setUserId("");
		long lvlId = 1;
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setLvlId(lvlId);
		lstVendorRule.add(assortmentRulesVO);
		vendorAsmVo.setLstVendorRules(lstVendorRule);
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		List<String> lstHebSubComActivated = new ArrayList<String>();
		List<String> lstSubCommodityDeActivated = new ArrayList<String>();
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		lstHebSubComActivated.add(AntiMagicNumber.STRING_ONE);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.ruleDao.getSubCommodityActivated(vendorAsmVo.getVendorId())).thenReturn(lstHebSubComActivated);
		Mockito.when(this.ruleDao.removeSubCommodityActivated(vendorAsmVo.getVendorId(), lstSubCommodityDeActivated)).thenReturn(true);
		Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvlActivated(vendorAsmVo.getUserId(), lstAssortmentRulesVOs, vendorAsmVo.getVendorId(), lvlId)).thenReturn("");
		// Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvl(lvlId, vendorAsmVo.getUserId(), vendorAsmVo.getCommodityId(), null, null, 0, vendorId, vendorAsmVo.getSubCommdId(), null)).thenReturn(lvlId);
		// boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(vendorAsmVo);
		// assertTrue(result);
	}

	/**
	 * Run the boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_6() throws DSVException {
		AttrAndRuleVendorVO vendorAsmVo = new AttrAndRuleVendorVO();
		List<AssortmentRulesVO> lstVendorRule = new ArrayList<AssortmentRulesVO>();
		vendorAsmVo.setVendorId("52765");
		vendorAsmVo.setUserId("");
		long lvlId = 1;

		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setLvlId(lvlId);
		lstVendorRule.add(assortmentRulesVO);
		vendorAsmVo.setLstVendorRules(lstVendorRule);
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		List<String> lstHebSubComActivated = new ArrayList<String>();
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.ruleDao.getSubCommodityActivated(vendorAsmVo.getVendorId())).thenReturn(lstHebSubComActivated);
		Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvlActivated(vendorAsmVo.getUserId(), lstAssortmentRulesVOs, vendorAsmVo.getVendorId(), lvlId)).thenReturn("");
		// Mockito.when(this.ruleDao.checkLVL(vendorAsmVo.getVendorId(), vendorAsmVo.getCommodityId(), vendorAsmVo.getSubCommdId())).thenReturn(lvlId);
		// Mockito.when(this.ruleDao.updateRuleValuesToDfns(lstRuleDefinationVOs, lvlId, vendorAsmVo.getUserId())).thenReturn("");
		// boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(vendorAsmVo);
		// assertTrue(result);
	}

	/**
	 * Run the boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_7() throws DSVException {
		boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(null);
		assertTrue(!result);
	}

	/**
	 * Run the boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO) method test.
	 * @throws DSVException
	 * @author quang.phan
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_8() throws DSVException {
		AttrAndRuleVendorVO vendorAsmVo = new AttrAndRuleVendorVO();
		List<AssortmentRulesVO> lstVendorRule = new ArrayList<AssortmentRulesVO>();
		vendorAsmVo.setVendorId("52765");
		vendorAsmVo.setUserId("");
		long lvlId = 1;

		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setLvlId(lvlId);
		lstVendorRule.add(assortmentRulesVO);
		vendorAsmVo.setLstVendorRules(lstVendorRule);
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		List<String> lstHebSubComActivated = new ArrayList<String>();
		List<String> lstSubCommodityDeActivated = new ArrayList<String>();
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		lstHebSubComActivated.add(AntiMagicNumber.STRING_ONE);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.ruleDao.getSubCommodityActivated(vendorAsmVo.getVendorId())).thenReturn(lstHebSubComActivated);
		Mockito.when(this.ruleDao.removeSubCommodityActivated(vendorAsmVo.getVendorId(), lstSubCommodityDeActivated)).thenReturn(true);
		Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvlActivated(vendorAsmVo.getUserId(), lstAssortmentRulesVOs, vendorAsmVo.getVendorId(), lvlId)).thenReturn("");
		// Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvl(lvlId, vendorAsmVo.getUserId(), vendorAsmVo.getCommodityId(), null, null, 0, vendorId, vendorAsmVo.getSubCommdId(), null)).thenReturn(lvlId);
		// boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(vendorAsmVo);
		// assertTrue(result);
	}

	/**
	 * Test update rule settings for vendor_9.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_9() throws DSVException {
		AttrAndRuleVendorVO attrAndRuleVendorVO = new AttrAndRuleVendorVO();
		attrAndRuleVendorVO.setVendorId("123");
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenThrow(new DSVException("Error"));
		boolean actual = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(attrAndRuleVendorVO);
		assertTrue(!actual);
	}

	/**
	 * Test update rule settings for vendor_10.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdateRuleSettingsForVendor_10() throws DSVException {
		AttrAndRuleVendorVO vendorAsmVo = new AttrAndRuleVendorVO();
		List<AssortmentRulesVO> lstVendorRule = new ArrayList<AssortmentRulesVO>();
		vendorAsmVo.setVendorId("52765");
		vendorAsmVo.setUserId("");
		long lvlId = 1;

		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setLvlId(lvlId);
		lstVendorRule.add(assortmentRulesVO);
		vendorAsmVo.setLstVendorRules(lstVendorRule);
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		List<String> lstHebSubComActivated = new ArrayList<String>();
		List<String> lstSubCommodityDeActivated = new ArrayList<String>();
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		lstHebSubComActivated.add(AntiMagicNumber.STRING_ONE);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.ruleDao.getSubCommodityActivated(vendorAsmVo.getVendorId())).thenReturn(lstHebSubComActivated);
		Mockito.when(this.ruleDao.removeSubCommodityActivated(vendorAsmVo.getVendorId(), lstSubCommodityDeActivated)).thenReturn(true);
		Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvlActivated(vendorAsmVo.getUserId(), lstAssortmentRulesVOs, vendorAsmVo.getVendorId(), lvlId)).thenReturn("");
		// Mockito.when(this.globalDao.getMaxLVLID()).thenReturn(lvlId);
		// Mockito.when(this.globalDao.insertlvl(lvlId, vendorAsmVo.getUserId(), vendorAsmVo.getCommodityId(), null, null, 0, vendorId, vendorAsmVo.getSubCommdId(), null)).thenReturn(lvlId);
		// boolean result = this.vendorRulesServiceImpl.updateRuleSettingsForVendor(vendorAsmVo);
		// assertTrue(result);
	}

	/**
	 * Perform pre-test initialization.
	 * @throws DSVException
	 *             if the initialization fails for some reason
	 * @author thangdang
	 */
	@Before
	public void setUp() throws DSVException {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 * @throws DSVException
	 *             if the clean-up fails for some reason
	 * @author thangdang
	 */
	@After
	public void tearDown() throws DSVException {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 * @param args
	 *            the command line arguments
	 * @author thangdang
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(VendorRulesServiceImplTest.class);
	}
}