package com.heb.enterprise.dsv.assortment.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.heb.enterprise.dsv.assortment.service.RetailInquiryService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;

@RunWith(MockitoJUnitRunner.class)
public class RetailInquiryControllerTest {

	@InjectMocks
	RetailInquiryController retailInquiryController = new RetailInquiryController();

	@Mock
	RetailInquiryService retailInquiryService;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test search for product1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author dinhphan
	 */
	@Test
	public void testSearchForProduct1() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("Upc");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("PennyProfit");
		productRiewVO.setImageUri("ImageUri");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		Mockito.when(this.retailInquiryService.searchProductsWithRetailInquiry(productSearchCriteriaVO)).thenReturn(page);
		String actual = this.retailInquiryController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"recordsFiltered\":1,\"data\":[{\"hebRetail\":null,\"msrp\":\"msr\",\"description\":\"TEST\",\"image\":\"ImageUri\",\"gppercent\":\"20\",\"totalRow\":1,\"cost\":\"30\",\"upc\":\"Upc\",\"pennyProfit\":\"PennyProfit\",\"size\":\"Max\"}],\"recordsTotal\":1}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product 2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author dinhphan
	 */
	@Test
	public void testSearchForProduct2() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);

		Mockito.when(this.retailInquiryService.searchProductsWithRetailInquiry(productSearchCriteriaVO)).thenReturn(page);
		String actual = this.retailInquiryController.searchForProduct(productSearchCriteriaVO, request, response);
		System.out.println(actual);
		String expected = "{\"recordsFiltered\":1,\"data\":[],\"recordsTotal\":1}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product not found.
	 * @throws DSVException
	 *             the DSV exception
	 * @author dinhphan
	 */
	@Test
	public void testSearchForProduct3_notFound() throws DSVException {
		Mockito.when(this.retailInquiryService.searchProductsWithRetailInquiry(Mockito.any(ProductSearchCriteriaVO.class))).thenAnswer(new Answer<Page<ProductRiewVO>>() {
			public Page<ProductRiewVO> answer(InvocationOnMock invocation) throws Throwable {
				throw new Exception("NotFound");
			}
		});
		try {
			ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
			productSearchCriteriaVO.setStart("1");
			productSearchCriteriaVO.setMarginBegin("10");
			productSearchCriteriaVO.setMarginEnd("20");
			productSearchCriteriaVO.setDateBegin("20/11/2015");
			productSearchCriteriaVO.setDateEnd("25/11/2015");
			productSearchCriteriaVO.setiSortColumn("order[0][column]");
			String actual = this.retailInquiryController.searchForProduct(productSearchCriteriaVO, request, response);
			Assert.assertNull(actual);
		} catch (Exception e) {

			Assert.assertEquals("NotFound", e.getMessage());
		}
	}

	/**
	 * Test Export to Excel.
	 * @throws DSVException
	 *             the DSV exception
	 * @author dinhphan
	 */
	@Test
	public void testExportToExcel() throws DSVException {
		List<ProductRiewVO> lstPrdsToExport = new ArrayList<ProductRiewVO>();
		ProductSearchCriteriaVO prodSearchModel = new ProductSearchCriteriaVO();
		Mockito.when(this.retailInquiryService.exportExcelRetailInquiry(prodSearchModel)).thenReturn(lstPrdsToExport);
		this.retailInquiryController.exportToExcel(request, response, prodSearchModel);
	}

}
