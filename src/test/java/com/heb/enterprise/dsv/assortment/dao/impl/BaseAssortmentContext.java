package com.heb.enterprise.dsv.assortment.dao.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

/**
 * BaseAssortmentContext.
 * @author anhtran.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {
		"classpath*:common-beans-test.xml",
		"classpath*:spring-core-test.xml",
		"classpath*:spring-beans-test.xml",
		"classpath*:spring-web-test.xml",
		"classpath*:security-core-test.xml",
		"classpath*:spring-security-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class BaseAssortmentContext {
	@Autowired
	protected WebApplicationContext wac;

	/**
	 * testBaseDSVContextTest.
	 */
	@Test
	public void testBaseDSVContextTest() {
		assertNotNull(this.wac);
	}
}
