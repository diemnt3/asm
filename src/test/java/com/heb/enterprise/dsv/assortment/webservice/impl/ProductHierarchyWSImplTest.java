/*
 * $Id.: $ProductHierarchyWSImplTest.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * ProductHierarchyWSImplTest.
 * @author anhtran.
 */
public class ProductHierarchyWSImplTest {

	/**
	 * setUpBeforeClass.
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
	}

	/**
	 * tearDownAfterClass.
	 */
	@AfterClass
	public static void tearDownAfterClass() {
	}

	/**
	 * setUp.
	 */
	@Before
	public void setUp() {
	}

	/**
	 * tearDown.
	 */
	@After
	public void tearDown() {
	}

	/**
	 * testGetCommodityDetailList.
	 */
	@Test
	public void testGetCommodityDetailList() {
	}

	/**
	 * testGetMerchandisingHierarchyDetails.
	 */
	@Test
	public void testGetMerchandisingHierarchyDetails() {
	}

	/**
	 * testGetHierachyForGlobalRules.
	 */
	@Test
	public void testGetHierachyForGlobalRules() {
	}

}
