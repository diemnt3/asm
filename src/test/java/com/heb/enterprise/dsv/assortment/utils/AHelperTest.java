package com.heb.enterprise.dsv.assortment.utils;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;

/**
 * The Class AHelperTest.
 * @author quang.phan
 */
@RunWith(MockitoJUnitRunner.class)
public class AHelperTest {

	/**
	 * Test parse json object to list hierarchy map vo.
	 * @author quang.phan
	 */
	@Test
	public void testPasreJsonObjectToListHierarchyMapVO() {
		String orderJsonId = "{name: Quang}";
		// AHelper.parseJsonObjectToListHierarchyMapVO(orderJsonId);
	}

	/**
	 * Test parse list commodity vo to json str.
	 * @author quang.phan
	 */
	@Test
	public void testPasreListCommodityVOToJsonStr() {
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		// AHelper.parseListCommodityVOToJsonStr(commodityVOs);
	}

	/**
	 * Test parse list hierarchy map vo to json str.
	 * @author quang.phan
	 */
	@Test
	public void testPasreListHierarchyMapVOToJsonStr() {
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		// AHelper.parseListHierarchyMapVOToJsonStr(hierarchyMapVOs);
	}

	/**
	 * Test parse list vendors vo to json str1.
	 * @author quang.phan
	 */
	@Test
	public void testPasreListVendorsVOToJsonStr1() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("property1", "abc");
		jsonObject.put("property2", "123");
		jsonObject.put("property3", "fff");
		jsonObject.put("property4", "ddd");
		jsonObject.put("property5", "zzz");
		// String actual = AHelper.parseListVendorsVOToJsonStr(jsonObject);
		// String expected = "{\"property4\":\"ddd\",\"property3\":\"fff\",\"property2\":\"123\",\"property5\":\"zzz\",\"property1\":\"abc\"}";
		// assertEquals(expected, actual);
	}

	/**
	 * Test parse list vendors vo to json str2.
	 * @author quang.phan
	 */
	@Test
	public void testPasreListVendorsVOToJsonStr2() {
		JSONObject jsonObject = new JSONObject();
		List<String> lstString = new ArrayList<String>();
		lstString.add("ABC");
		lstString.add("XYZ");
		jsonObject.put("item", lstString);
		// String actual = AHelper.parseListVendorsVOToJsonStr(jsonObject);
		// String expected = "{\"item\":[\"ABC\",\"XYZ\"]}";
		// assertEquals(expected, actual);
	}

	/**
	 * Test parse file to list hierarchy map vo.
	 * @author quang.phan
	 */
	@Test
	public void testPasreFileToListHierarchyMapVO() {
		File file = new File("C:\\");
		// AHelper.parseFileToListHierarchyMapVO(file);
	}

	/**
	 * Test parse json object to list retail cost vo.
	 * @author quang.phan
	 */
	@Test
	public void testPasreJsonObjectToListRetailCostVO() {
		String orderJsonId = "{name: Quang}";
		// AHelper.parseJsonObjectToListRetailCostVO(orderJsonId);
	}

	/**
	 * Test parse json object to json str1.
	 * @author quang.phan
	 */
	@Test
	public void testPasreJsonObjectToJsonStr1() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("property1", "abc");
		jsonObject.put("property2", "123");
		jsonObject.put("property3", "fff");
		jsonObject.put("property4", "ddd");
		jsonObject.put("property5", "zzz");
		// String actual = AHelper.parseJsonObjectToJsonStr(jsonObject);
		// String expected = "{\"property4\":\"ddd\",\"property3\":\"fff\",\"property2\":\"123\",\"property5\":\"zzz\",\"property1\":\"abc\"}";
		// assertEquals(expected, actual);
	}

	/**
	 * Test parse json object to json str2.
	 * @author quang.phan
	 */
	@Test
	public void testPasreJsonObjectToJsonStr2() {
		JSONObject jsonObject = new JSONObject();
		List<String> lstString = new ArrayList<String>();
		lstString.add("ABC");
		lstString.add("XYZ");
		jsonObject.put("item", lstString);
		// String actual = AHelper.parseJsonObjectToJsonStr(jsonObject);
		// String expected = "{\"item\":[\"ABC\",\"XYZ\"]}";
		// assertEquals(expected, actual);
	}

	/**
	 * Test convert list to string.
	 * @author quang.phan
	 */
	@Test
	public void testConvertListToString1() {
		List<String> lstParams = new ArrayList<String>();
		lstParams.add("HEB");
		lstParams.add("Texas");
		String actual = AHelper.convertListToString(lstParams);
		String expected = "HEB,Texas";
		assertEquals(expected, actual);
	}

	/**
	 * Test convert list to string2.
	 * @author quang.phan
	 */
	@Test
	public void testConvertListToString2() {
		List<String> lstParams = new ArrayList<String>();
		String actual = AHelper.convertListToString(lstParams);
		assertEquals("", actual);
	}

	/**
	 * Test append criteria string1.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaString1() {
		String fieldName = "DCM_CD";
		String fieldValue = "a10678";
		String actual = AHelper.appendCriteriaString(fieldName, fieldValue);
		String expected = "DCM_CD='a10678'";
		assertEquals(expected, actual);
	}

	/**
	 * Test append criteria string2.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaString2() {
		String fieldName = "DCM_CD";
		String fieldValue = "";
		String actual = AHelper.appendCriteriaString(fieldName, fieldValue);
		assertEquals("", actual);
	}

	/**
	 * Test append criteria string3.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaString3() {
		String fieldName = "DCM_CD";
		String fieldValue = "%";
		String actual = AHelper.appendCriteriaString(fieldName, fieldValue);
		assertEquals("", actual);
	}

	/**
	 * Test append criteria string4.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaString4() {
		String fieldName = "PROD_DES";
		String fieldValue = "Big ' and black";
		String actual = AHelper.appendCriteriaString(fieldName, fieldValue);
		assertEquals("PROD_DES='Big '' and black'", actual);
	}

	/**
	 * Test append criteria string in1.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaStringIn1() {
		String fieldName = "DCM_CD";
		String fieldValue = "a10678";
		String actual = AHelper.appendCriteriaStringIn(fieldName, fieldValue);
		String expected = "DCM_CD='a10678'";
		assertEquals(expected, actual);
	}

	/**
	 * Test append criteria string in2.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaStringIn2() {
		String fieldName = "DCM_CD";
		String fieldValue = "";
		String actual = AHelper.appendCriteriaStringIn(fieldName, fieldValue);
		assertEquals("", actual);
	}

	/**
	 * Test append criteria string in3.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaStringIn3() {
		String fieldName = "DCM_CD";
		String fieldValue = "%";
		String actual = AHelper.appendCriteriaStringIn(fieldName, fieldValue);
		assertEquals("", actual);
	}

	/**
	 * Test append criteria string in4.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaStringIn4() {
		String fieldName = "PROD_DES";
		String fieldValue = "Big ' and black";
		String actual = AHelper.appendCriteriaStringIn(fieldName, fieldValue);
		assertEquals("PROD_DES='Big '' and black'", actual);
	}

	/**
	 * Test append criteria like1.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaLike1() {
		String fieldName = "DCM_CD";
		String fieldValue = "a10678";
		String actual = AHelper.appendCriteriaLike(fieldName, fieldValue);
		String expected = "UPPER(DCM_CD) like '%A10678%'";
		assertEquals(expected, actual.trim());
	}

	/**
	 * Test append criteria like2.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaLike2() {
		String fieldName = "DCM_CD";
		String fieldValue = "";
		String actual = AHelper.appendCriteriaLike(fieldName, fieldValue);
		assertEquals("", actual);
	}

	/**
	 * Test append criteria like3.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaLike3() {
		String fieldName = "DCM_CD";
		String fieldValue = "%";
		String actual = AHelper.appendCriteriaLike(fieldName, fieldValue);
		assertEquals("", actual);
	}

	/**
	 * Test append criteria like4.
	 * @author quang.phan
	 */
	@Test
	public void testAppendCriteriaLike4() {
		String fieldName = "PROD_DES";
		String fieldValue = "Big ' and black";
		String actual = AHelper.appendCriteriaLike(fieldName, fieldValue);
		String expected = "UPPER(PROD_DES) like '%BIG '' AND BLACK%'";
		assertEquals(expected, actual.trim());
	}

	/**
	 * Test search date criteria1.
	 * @author quang.phan
	 */
	@Test
	public void testSearchDateCriteria1() {
		String fieldName = "PROC_STRT_TS";
		String fieldValue = "20/11/2015";
		String actual = AHelper.searchDateCriteria(fieldName, fieldValue, ">");
		String expected = "TRUNC(PROC_STRT_TS) > TO_DATE('20/11/2015', 'mm/dd/YYYY')";
		assertEquals(expected, actual);
	}

	/**
	 * Test search date criteria2.
	 * @author quang.phan
	 */
	@Test
	public void testSearchDateCriteria2() {
		String fieldName = "PROC_STRT_TS";
		String fieldValue = "";
		String actual = AHelper.searchDateCriteria(fieldName, fieldValue, ">");
		assertEquals("", actual);
	}

	/**
	 * Test get search condition bdm review1.
	 * @author quang.phan
	 */
	@Test
	public void testGetSearchConditionBdmReview1() {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		String actual = AHelper.getSearchConditionBdmReview(productSearchCriteriaVO);
		// assertEquals(" AND  PRC.WEB_STAT_CD IN ('CPS','PM')  AND  (PRC.REV_STAT_CD IS NULL OR PRC.REV_STAT_CD ='RJECT' OR PRC.REV_STAT_CD ='APPRV')", actual.trim());
	}

	/**
	 * Test get search condition bdm review2.
	 * @author quang.phan
	 */
	@Test
	public void testGetSearchConditionBdmReview2() {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setFailReason("Fail");
		productSearchCriteriaVO.setCommodittyId("123");
		productSearchCriteriaVO.setBrand("Brand");
		productSearchCriteriaVO.setVendorId("a1234");
		productSearchCriteriaVO.setProdDes("It has black color");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		productSearchCriteriaVO.setUpc("UPC 1");
		productSearchCriteriaVO.setMarginBegin("20");
		productSearchCriteriaVO.setMarginEnd("10");
		productSearchCriteriaVO.setAllMapProduct(true);
		productSearchCriteriaVO.setStatus("0");
		productSearchCriteriaVO.setOnlyPrePriceProduct(true);
		productSearchCriteriaVO.setUpcIdFilter("upc-1");
		productSearchCriteriaVO.setProdDescFilter("Desc");
		productSearchCriteriaVO.setProdSizeFilter("Max");
		productSearchCriteriaVO.setMapFilter("uber");
		productSearchCriteriaVO.setPrePriceFilter("50");
		productSearchCriteriaVO.setMsrpFilter("filter");
		productSearchCriteriaVO.setSrpFilter("srp");
		productSearchCriteriaVO.setCostFilter("10");
		productSearchCriteriaVO.setPennyProfitFilter("10");
		productSearchCriteriaVO.setPerGpFilter("10");
		productSearchCriteriaVO.setFailReasonFilter("10");
		productSearchCriteriaVO.setToEditedTSFilter("10/2/2014");
		productSearchCriteriaVO.setFromEditedTSFilter("10/2/2014");
		productSearchCriteriaVO.setToReceivedTSFilter("10/2/2014");
		productSearchCriteriaVO.setFromReceivedTSFilter("10/2/2014");
		String actual = AHelper.getSearchConditionBdmReview(productSearchCriteriaVO);
		// String expected =
		// "AND  PRC.WEB_STAT_CD IN ('CPS','PM') INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD  AND PRS.ERR_TYP_CD='Fail' AND PRC.HEB_COM_ID=123 AND  UPPER(PRC.BRND_NM) like '%BRAND%' AND PRC.VEND_ID=a1234 AND ( UPPER(PRC.PROD_DES) like '%IT HAS BLACK COLOR%' OR PRC.PROD_SCN_CD=UPC 1) AND TRUNC(PRC.CRE8_TS) >= TO_DATE('20/11/2015', 'mm/dd/YYYY') AND TRUNC(PRC.CRE8_TS) <= TO_DATE('25/11/2015', 'mm/dd/YYYY') AND RC.MARGIN>=20 AND RC.MARGIN<=10 AND RC.MAP_SW='Y' AND RC.PRE_PRC_AMT>0 AND  (PRC.REV_STAT_CD IS NULL OR PRC.REV_STAT_CD ='RJECT' OR PRC.REV_STAT_CD ='APPRV') AND PRC.PROD_SCN_CD=upc-1 AND  UPPER(PRC.PROD_DES) like '%DESC%' AND PRC.SELL_UNT_SZ=Max AND RC.MAP_AMT=uber AND RC.PRE_PRC_AMT=50 AND RC.MSRP_AMT=filter AND RC.SRP_AMT=srp AND RC.HEB_CST_AMT=10 AND  UPPER(PRC.IMG_URL_TXT) like '%IMG%'";
		// assertEquals(expected, actual.trim());
	}

	/**
	 * Test get search condition bdm review3.
	 * @author quang.phan
	 */
	@Test
	public void testGetSearchConditionBdmReview3() {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setFailReason("Fail");
		productSearchCriteriaVO.setStatus("1");
		String actual = AHelper.getSearchConditionBdmReview(productSearchCriteriaVO);
		// String expected =
		// "INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD AND PRC.VEND_ID = PRS.VEND_ID INNER JOIN DSV_ASSRT. RULE R ON PRS.RULE_ID = R.RULE_ID AND PRS.RULE_ID=Fail AND PRC.HEB_COM_ID=123 AND  UPPER(PRC.BRND_NM) like '%BRAND%' AND PRC.VEND_ID=a1234 AND  UPPER(PRC.PROD_DES) like '%IT HAS BLACK COLOR%' AND PRC.PROD_SCN_CD=UPC 1 AND RC.MAP_SW='Y' AND RC.PRE_PRC_AMT>0 AND PRC.REV_STAT_CD IS NULL AND PRC.PROD_SCN_CD NOT IN (SELECT DISTINCT PS.PROD_SCN_CD FROM DSV_ASSRT.PROD_RULE_STAT PS WHERE PS.RULE_ID IN (1,2,3,4,5,6,7,8,9,10)) AND PRC.PROD_SCN_CD=upc-1 AND  UPPER(PRC.PROD_DES) like '%DESC%' AND PRC.SELL_UNT_SZ=Max AND RC.MAP_AMT=uber AND RC.PRE_PRC_AMT=50 AND RC.MSRP_AMT=filter AND RC.SRP_AMT=srp AND RC.HEB_CST_AMT=10 AND  UPPER(PRC.IMG_URL_TXT) like '%IMG%'";
		// assertEquals(expected, actual.trim());
	}

	/**
	 * Test get search condition bdm review4.
	 * @author quang.phan
	 */
	@Test
	public void testGetSearchConditionBdmReview4() {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setFailReason("Fail");
		productSearchCriteriaVO.setStatus("2");
		String actual = AHelper.getSearchConditionBdmReview(productSearchCriteriaVO);
		// String expected =
		// "INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD AND PRC.VEND_ID = PRS.VEND_ID INNER JOIN DSV_ASSRT. RULE R ON PRS.RULE_ID = R.RULE_ID AND PRS.RULE_ID=Fail AND PRC.HEB_COM_ID=123 AND PRC.BRND_NM='Brand' AND PRC.VEND_ID=a1234 AND  UPPER(PRC.PROD_DES) like '%IT HAS BLACK COLOR%' AND TRUNC(PRC.REV_TS) >= TO_DATE('20/11/2015', 'mm/dd/YYYY') AND TRUNC(PRC.REV_TS) <= TO_DATE('25/11/2015', 'mm/dd/YYYY') AND PRC.PROD_SCN_CD=UPC 1 AND RC.MSRP_AMT>=20 AND RC.MSRP_AMT<=10 AND RC.MAP_SW='Y' AND RC.PRE_PRC_AMT>0 AND PRC.REV_STAT_CD IS NULL AND PRC.PROD_SCN_CD IN (SELECT DISTINCT PS.PROD_SCN_CD FROM DSV_ASSRT.PROD_RULE_STAT PS WHERE PS.RULE_ID IN (1,2,3,4,5,6,7,8,9,10)) AND PRC.PROD_SCN_CD=upc-1 AND  UPPER(PRC.PROD_DES) like '%DESC%' AND PRC.SELL_UNT_SZ=Max AND RC.MAP_AMT=uber AND RC.PRE_PRC_AMT=50 AND RC.MSRP_AMT=filter AND RC.SRP_AMT=srp AND RC.HEB_CST_AMT=10 AND  UPPER(PRC.IMG_URL_TXT) like '%IMG%'";
		// assertEquals(expected, actual.trim());
	}

	/**
	 * Test get search condition bdm review5.
	 * @author quang.phan
	 */
	@Test
	public void testGetSearchConditionBdmReview5() {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setFailReason("Fail");
		productSearchCriteriaVO.setStatus("3");
		productSearchCriteriaVO.setOnlyPrePriceProduct(true);
		String actual = AHelper.getSearchConditionBdmReview(productSearchCriteriaVO);
		// String expected =
		// "INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD AND PRC.VEND_ID = PRS.VEND_ID INNER JOIN DSV_ASSRT. RULE R ON PRS.RULE_ID = R.RULE_ID AND PRS.RULE_ID=Fail AND PRC.HEB_COM_ID=123 AND PRC.BRND_NM='Brand' AND PRC.VEND_ID=a1234 AND  UPPER(PRC.PROD_DES) like '%IT HAS BLACK COLOR%' AND TRUNC(PRC.REV_TS) >= TO_DATE('20/11/2015', 'mm/dd/YYYY') AND TRUNC(PRC.REV_TS) <= TO_DATE('25/11/2015', 'mm/dd/YYYY') AND PRC.PROD_SCN_CD=UPC 1 AND RC.MSRP_AMT>=20 AND RC.MSRP_AMT<=10 AND RC.MAP_SW='Y' AND RC.PRE_PRC_AMT>0 AND PRC.REV_STAT_CD='RJECT'";
		// assertEquals(expected, actual);
	}

	/**
	 * Test get search condition bdm review1.
	 * @author quang.phan
	 */
	@Test
	public void testGetSearchConditionBdmReviewCount() {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setFailReason("Fail");
		productSearchCriteriaVO.setCommodittyId("123");
		productSearchCriteriaVO.setBrand("Brand");
		productSearchCriteriaVO.setVendorId("a1234");
		productSearchCriteriaVO.setProdDes("It has black color");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		productSearchCriteriaVO.setUpc("UPC 1");
		productSearchCriteriaVO.setMarginBegin("20");
		productSearchCriteriaVO.setMarginEnd("10");
		productSearchCriteriaVO.setAllMapProduct(true);
		productSearchCriteriaVO.setStatus("0");
		productSearchCriteriaVO.setOnlyPrePriceProduct(true);
		productSearchCriteriaVO.setUpcIdFilter("upc-1");
		productSearchCriteriaVO.setProdDescFilter("Desc");
		productSearchCriteriaVO.setProdSizeFilter("Max");
		productSearchCriteriaVO.setMapFilter("uber");
		productSearchCriteriaVO.setPrePriceFilter("50");
		productSearchCriteriaVO.setMsrpFilter("filter");
		productSearchCriteriaVO.setSrpFilter("srp");
		productSearchCriteriaVO.setCostFilter("10");
		productSearchCriteriaVO.setPennyProfitFilter("10");
		productSearchCriteriaVO.setPerGpFilter("10");
		productSearchCriteriaVO.setFailReasonFilter("10");
		productSearchCriteriaVO.setToEditedTSFilter("10/2/2014");
		productSearchCriteriaVO.setFromEditedTSFilter("10/2/2014");
		productSearchCriteriaVO.setToReceivedTSFilter("10/2/2014");
		productSearchCriteriaVO.setFromReceivedTSFilter("10/2/2014");
		String actual = AHelper.getSearchConditionBdmReviewCount(productSearchCriteriaVO);
	}

	/**
	 * Test get search condition bdm review1.
	 * @author quang.phan
	 */
	@Test
	public void testGetSearchConditionBdmReviewCountFail() {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setFailReason("Fail");
		productSearchCriteriaVO.setCommodittyId("123");
		productSearchCriteriaVO.setBrand("Brand");
		productSearchCriteriaVO.setVendorId("a1234");
		productSearchCriteriaVO.setProdDes("It has black color");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		productSearchCriteriaVO.setUpc("UPC 1");
		productSearchCriteriaVO.setMarginBegin("20");
		productSearchCriteriaVO.setMarginEnd("10");
		productSearchCriteriaVO.setAllMapProduct(true);
		productSearchCriteriaVO.setStatus("0");
		productSearchCriteriaVO.setOnlyPrePriceProduct(true);
		productSearchCriteriaVO.setUpcIdFilter("upc-1");
		productSearchCriteriaVO.setProdDescFilter("Desc");
		productSearchCriteriaVO.setProdSizeFilter("Max");
		productSearchCriteriaVO.setMapFilter("uber");
		productSearchCriteriaVO.setPrePriceFilter("50");
		productSearchCriteriaVO.setMsrpFilter("filter");
		productSearchCriteriaVO.setSrpFilter("srp");
		productSearchCriteriaVO.setCostFilter("10");
		productSearchCriteriaVO.setPennyProfitFilter("10");
		productSearchCriteriaVO.setPerGpFilter("10");
		productSearchCriteriaVO.setFailReasonFilter("10");
		productSearchCriteriaVO.setToEditedTSFilter("10/2/2014");
		productSearchCriteriaVO.setFromEditedTSFilter("10/2/2014");
		productSearchCriteriaVO.setToReceivedTSFilter("10/2/2014");
		productSearchCriteriaVO.setFromReceivedTSFilter("10/2/2014");
		String actual = AHelper.getSearchConditionBdmReviewCountFail(productSearchCriteriaVO);
	}

	/**
	 * Test get search condition bdm review1.
	 * @author quang.phan
	 */
	@Test
	public void testGetSearchConditionRetailInquiry() {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setFailReason("Fail");
		productSearchCriteriaVO.setCommodittyId("123");
		productSearchCriteriaVO.setBrand("Brand");
		productSearchCriteriaVO.setVendorId("a1234");
		productSearchCriteriaVO.setProdDes("It has black color");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		productSearchCriteriaVO.setUpc("UPC 1");
		productSearchCriteriaVO.setMarginBegin("20");
		productSearchCriteriaVO.setMarginEnd("10");
		productSearchCriteriaVO.setAllMapProduct(true);
		productSearchCriteriaVO.setStatus("0");
		productSearchCriteriaVO.setOnlyPrePriceProduct(true);
		productSearchCriteriaVO.setUpcIdFilter("upc-1");
		productSearchCriteriaVO.setProdDescFilter("Desc");
		productSearchCriteriaVO.setProdSizeFilter("Max");
		productSearchCriteriaVO.setMapFilter("uber");
		productSearchCriteriaVO.setPrePriceFilter("50");
		productSearchCriteriaVO.setMsrpFilter("filter");
		productSearchCriteriaVO.setSrpFilter("srp");
		productSearchCriteriaVO.setCostFilter("10");
		productSearchCriteriaVO.setPennyProfitFilter("10");
		productSearchCriteriaVO.setPerGpFilter("10");
		productSearchCriteriaVO.setFailReasonFilter("10");
		productSearchCriteriaVO.setToEditedTSFilter("10/2/2014");
		productSearchCriteriaVO.setFromEditedTSFilter("10/2/2014");
		productSearchCriteriaVO.setToReceivedTSFilter("10/2/2014");
		productSearchCriteriaVO.setFromReceivedTSFilter("10/2/2014");
		String actual = AHelper.getSearchConditionRetailInquiry(productSearchCriteriaVO);
	}
}
