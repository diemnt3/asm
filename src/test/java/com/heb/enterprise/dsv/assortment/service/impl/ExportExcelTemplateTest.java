package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;
import com.heb.enterprise.dsv.vo.RetailCostVO;

/**
 * @author mrh.diemnguyen
 */
@RunWith(MockitoJUnitRunner.class)
public class ExportExcelTemplateTest {
	@InjectMocks
	private ExportExcelTemplate exportExcelTemplate = new ExportExcelTemplate();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetExcelProduct() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		this.exportExcelTemplate.getExcelProduct(productRiewVOs);

	}

	@Test
	public void testGetExcelCostReview() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		Page<RetailCostVO> page = new Page<RetailCostVO>();
		page.setRowCount(1);
		List<RetailCostVO> productRiewVOs = new ArrayList<RetailCostVO>();
		RetailCostVO productRiewVO = new RetailCostVO();
		productRiewVO.setProdScnCd(111L);
		productRiewVO.setProdDesc("TEST");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		this.exportExcelTemplate.getExcelCostReview(productRiewVOs);
	}

	@Test
	public void testGetExcelRetailInquiry() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		this.exportExcelTemplate.getExcelRetailInquiry(productRiewVOs);
	}

}
