package com.heb.enterprise.dsv.assortment.service.impl;

import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.assortment.dao.VendorManagementDAO;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;

/**
 * The class <code>VendorManagementServiceImplTest</code> contains tests for the class <code>{@link VendorManagementServiceImpl}</code>.
 * @author mrh.diemnguyen
 * @version $Revision: 1.6 $
 */
@RunWith(MockitoJUnitRunner.class)
public class VendorManagementServiceImplTest {

	@InjectMocks
	private VendorManagementServiceImpl vendorManagementServiceImpl = new VendorManagementServiceImpl();

	@Mock
	private VendorManagementDAO vendorManagementDAO;

	/**
	 * Run the VendorManagementServiceImpl() constructor test.
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testVendorManagementServiceImpl_1() throws DSVException {
		assertNotNull(vendorManagementServiceImpl);
	}

	/**
	 * Run the List<HierarchyMapVO> getVendorHierarchyMappingByVendor(String vendorId) method test.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testGetVendorHierarchyMappingByVendor_1() throws DSVException {
		String vendorId = "DN2801";
		Mockito.when(this.vendorManagementDAO.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(null);
		List<HierarchyMapVO> resultActual = this.vendorManagementServiceImpl.getVendorHierarchyMappingByVendor(vendorId);
		Assert.assertEquals("Error result not same", null, resultActual);
	}

	/**
	 * Run the List<HierarchyMapVO> getVendorHierarchyMappingByVendor(String vendorId) method test.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testGetVendorHierarchyMappingByVendor_2() throws DSVException {
		String vendorId = "DN2801";
		List<HierarchyMapVO> lstReturn = new ArrayList<HierarchyMapVO>();
		lstReturn.add(new HierarchyMapVO());
		Mockito.when(this.vendorManagementDAO.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(lstReturn);
		List<HierarchyMapVO> resultActual = this.vendorManagementServiceImpl.getVendorHierarchyMappingByVendor(vendorId);
		Assert.assertEquals("Error result not same", lstReturn, resultActual);
	}

	/**
	 * Run the String saveVendorHierarchyMapping(List<HierarchyMapVO> hierarchyMapVOs, String vendorId) method test.
	 * @throws SQLException
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testSaveVendorHierarchyMapping_1() throws SQLException {
		String vendorId = "DN2801";
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		this.vendorManagementServiceImpl.saveVendorHierarchyMapping(hierarchyMapVOs, vendorId);
	}

	/**
	 * Run the String getHEBHierarchy() method test.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	// @Test
	// public void testGetHEBHierarchy_1() throws DSVException {
	// List<CommodityVO> lstReturn = new ArrayList<CommodityVO>();
	// Mockito.when(this.vendorManagementDAO.getHEBHierarchy()).thenReturn(lstReturn);
	// String resultActual = this.vendorManagementServiceImpl.getHEBHierarchy();
	// String resultExpected = "";
	// Assert.assertEquals("Error result not same", resultExpected, resultActual);
	// }

	/**
	 * Run the String getHEBHierarchy() method test.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	// @Test
	// public void testGetHEBHierarchy_2() throws DSVException {
	// List<CommodityVO> lstReturn = new ArrayList<CommodityVO>();
	// CommodityVO commodityVO = new CommodityVO();
	// commodityVO.setCommodityCd(2801);
	// commodityVO.setCommodityDesc("DESC");
	// commodityVO.setBdmCd("BDMCD");
	// commodityVO.setSubCommodityVOs(new ArrayList<SubCommodityVO>());
	// commodityVO.setRulesCustomize("RULE");
	// commodityVO.setParentCd("2802");
	// lstReturn.add(commodityVO);
	// Mockito.when(this.vendorManagementDAO.getHEBHierarchy()).thenReturn(lstReturn);
	// String resultActual = this.vendorManagementServiceImpl.getHEBHierarchy();
	// String resultExpected = "";
	// Assert.assertEquals("Error result not same", resultExpected, resultActual);
	// }

	/**
	 * Run the int deleteEntyRlshp(String parntEntyId, String childEntyId, String hierCntxtCd) method test.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 * @throws SQLException
	 */
	@Test
	public void testDeleteEntyRlshp_1() throws DSVException, SQLException {
		String vendorId = "123";
		String categoryIds = "ABC";
		// this.vendorManagementServiceImpl.deleteEntyRlshp(vendorId, categoryIds);
	}
}
