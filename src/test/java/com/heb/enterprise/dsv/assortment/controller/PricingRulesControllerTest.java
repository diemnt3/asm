package com.heb.enterprise.dsv.assortment.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.ui.ModelMap;

import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.PricingRulesService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.PricingRuleVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;

/**
 * The Class PricingRulesControllerTest.
 * @author quang.phan
 */
@RunWith(MockitoJUnitRunner.class)
public class PricingRulesControllerTest {

	@InjectMocks
	private PricingRulesController pricingRulesController = new PricingRulesController();

	@Mock
	private PricingRulesService pricingRulesService;

	@Mock
	private CommonService commonService;

	@Mock
	private ModelMap model;

	/**
	 * Test ajax get sub commodity by commodity id1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetSubCommodityByCommodityId1() throws DSVException {
		String commodityId = "";
		List<SubCommodityVO> lstCommodities = new ArrayList<SubCommodityVO>();
		SubCommodityVO vo = new SubCommodityVO();
		vo.setSubCommodityCd(1702);
		vo.setSubCommodityDesc("ABC");
		lstCommodities.add(vo);
		Mockito.when(this.pricingRulesService.getListSubCommByCommodityId(commodityId)).thenReturn(lstCommodities);
		String actual = this.pricingRulesController.ajaxGetSubCommodityByCommodityId(commodityId);
		String expected = "{\"data\":[{\"name\":\"\",\"value\":\"\",\"label\":\"ALL\"},{\"name\":\"ABC\",\"value\":1702,\"label\":\"1702 - ABC\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get sub commodity by commodity id2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetSubCommodityByCommodityId2() throws DSVException {
		String commodityId = "";
		List<SubCommodityVO> lstCommodities = new ArrayList<SubCommodityVO>();
		SubCommodityVO vo = new SubCommodityVO();
		lstCommodities.add(vo);
		Mockito.when(this.pricingRulesService.getListSubCommByCommodityId(commodityId)).thenReturn(lstCommodities);
		String actual = this.pricingRulesController.ajaxGetSubCommodityByCommodityId(commodityId);
		String expected = "{\"data\":[{\"name\":\"\",\"value\":\"\",\"label\":\"ALL\"},{\"name\":null,\"value\":0,\"label\":\"0 - null\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get sub commodity by commodity id3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetSubCommodityByCommodityId3() throws DSVException {
		String commodityId = "";
		List<SubCommodityVO> lstCommodities = new ArrayList<SubCommodityVO>();
		Mockito.when(this.pricingRulesService.getListSubCommByCommodityId(commodityId)).thenReturn(lstCommodities);
		String actual = this.pricingRulesController.ajaxGetSubCommodityByCommodityId(commodityId);
		String expected = "{\"data\":[]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get commodity1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodity1() throws DSVException {
		List<CommodityVO> lstCommodities = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(1702);
		commodityVO.setCommodityDesc("XYZ");
		lstCommodities.add(commodityVO);
		Mockito.when(this.pricingRulesService.getListCommodities()).thenReturn(lstCommodities);
		String actual = this.pricingRulesController.ajaxGetCommodity();
		String expected = "{\"data\":[{\"name\":\"XYZ\",\"value\":1702,\"label\":\"1702 - XYZ\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get commodity2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodity2() throws DSVException {
		List<CommodityVO> lstCommodities = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		lstCommodities.add(commodityVO);
		Mockito.when(this.pricingRulesService.getListCommodities()).thenReturn(lstCommodities);
		String actual = this.pricingRulesController.ajaxGetCommodity();
		String expected = "{\"data\":[{\"name\":null,\"value\":0,\"label\":\"0 - null\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get commodity3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodity3() throws DSVException {
		List<CommodityVO> lstCommodities = new ArrayList<CommodityVO>();
		Mockito.when(this.pricingRulesService.getListCommodities()).thenReturn(lstCommodities);
		String actual = this.pricingRulesController.ajaxGetCommodity();
		String expected = "{\"data\":[]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get online competitors1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetOnlineCompetitors1() throws DSVException {
		List<BaseVO> lstCompetitors = new ArrayList<BaseVO>();
		BaseVO competitiveRetails = new BaseVO();
		competitiveRetails.setId("2");
		competitiveRetails.setName("Walmart");
		lstCompetitors.add(competitiveRetails);
		Mockito.when(this.pricingRulesService.getOnlineCompetitors()).thenReturn(lstCompetitors);
		String actual = this.pricingRulesController.ajaxGetOnlineCompetitors();
		String expected = "{\"id\":\"0_root\",\"text\":\"Select All\",\"state\":\"open\",\"children\":[{\"id\":\"2\",\"text\":\"Walmart\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get online competitor2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetOnlineCompetitor2() throws DSVException {
		List<BaseVO> lstCompetitors = new ArrayList<BaseVO>();
		BaseVO competitiveRetails = new BaseVO();
		lstCompetitors.add(competitiveRetails);
		Mockito.when(this.pricingRulesService.getOnlineCompetitors()).thenReturn(lstCompetitors);
		String actual = this.pricingRulesController.ajaxGetOnlineCompetitors();
		String expected = "{\"id\":\"0_root\",\"text\":\"Select All\",\"state\":\"open\",\"children\":[{\"id\":null,\"text\":null}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test get pricing setting rules.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetPricingSettingRules() throws DSVException {
		String commodityId = "123";
		String subCommodityId = "C";
		String vendorId = "vn1234";
		Mockito.when(this.pricingRulesService.getPricingRules(vendorId, commodityId, subCommodityId)).thenReturn(null);
		PricingRuleVO actual = this.pricingRulesController.getPricingSettingRules(vendorId, commodityId, subCommodityId);
		assertNull(actual);
	}

	/**
	 * Test update pricing setting rules.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingSettingRules() throws DSVException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", "Quang Phan");
		jsonObject.put("age", 25);
		HebUserDetails user = new HebUserDetails("vn44178", "user1234", true, true, true, true, new ArrayList<GrantedAuthority>());
		Mockito.when(this.commonService.getUserLogin()).thenReturn(user);
		this.pricingRulesController.updatePricingSettingRules(jsonObject.toJSONString());
	}

	/**
	 * Test show pricing rules screen.
	 * @author quang.phan
	 */
	@Test
	public void testShowPricingRulesScreen() throws DSVException {
		this.pricingRulesController.showPricingRulesScreen(this.model);
	}

}
