/*
 * $Id.: $VendorManagementDAOImplTest.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.heb.enterprise.dsv.assortment.dao.VendorManagementDAO;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;

/**
 * VendorManagementDAOImplTest.
 * @author anhtran.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class VendorManagementDAOImplTest extends BaseAssortmentContext {

	@Mock
	private CommonService commonService;

	@Mock
	private HebUserDetails hebUserDetails;

	//
	// @Autowired
	// private VendorRulesService vendorRulesService;

	@InjectMocks
	@Autowired
	private VendorManagementDAO vendorManagementDAO;

	/**
	 * setUpBeforeClass.
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
	}

	/**
	 * tearDownAfterClass.
	 */
	@AfterClass
	public static void tearDownAfterClass() {
	}

	/**
	 * setUp.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(this.commonService.getUserLogin()).thenReturn(this.hebUserDetails);
		Mockito.when(this.hebUserDetails.getUsername()).thenReturn("vn75430");
		this.vendorManagementDAO.setCommonService(this.commonService);
	}

	/**
	 * tearDown.
	 * @throws Exception
	 */
	@After
	public void tearDown() {
	}

	/**
	 * testGetVendorHierarchyMappingByVendor.
	 */
	@Test
	public void testGetVendorHierarchyMappingByVendor() {
		try {
			this.vendorManagementDAO.getVendorHierarchyMappingByVendor("12765");
		} catch (DSVException e) {
			e.printStackTrace();
		}
	}

	/**
	 * testSaveVendorHierarchyMapping.
	 */
	@Test
	public void testSaveVendorHierarchyMappingTc01() {
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setCats("cat1>ASIAN-BAKING");
		hierarchyMapVO.setStatusNew(Constants.YES);
		hierarchyMapVO.setVendorId("12765");
		hierarchyMapVO.setEntyIdSubCommodity("1");
		hierarchyMapVO.setSubCommodity("ASIAN-BAKING");
		hierarchyMapVO.setSubCommodityCd("16");
		hierarchyMapVOs.add(hierarchyMapVO);
		try {
			this.vendorManagementDAO.saveVendorHierarchyMapping(hierarchyMapVOs, "12765");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * testSaveVendorHierarchyMapping.
	 */
	@Test
	public void testSaveVendorHierarchyMappingTc02() {
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setCats("cat1>ASIAN-BAKING");
		hierarchyMapVO.setStatusNew(Constants.NO);
		hierarchyMapVO.setVendorId("12765");
		hierarchyMapVO.setEntyIdSubCommodity("1");
		hierarchyMapVO.setSubCommodity("ASIAN-BAKING");
		hierarchyMapVO.setSubCommodityCd("16");
		hierarchyMapVO.setLowestCatId("3");
		hierarchyMapVO.setRowId(1);
		hierarchyMapVOs.add(hierarchyMapVO);
		// try {
		// this.vendorManagementDAO.saveVendorHierarchyMapping(hierarchyMapVOs, "12765");
		// } catch (SQLException e) {
		// e.printStackTrace();
		// }
	}

	/**
	 * testInsertEnty.
	 */
	@Test
	public void testInsertEnty() {
	}

	/**
	 * testGetHEBHierarchy.
	 */
	@Test
	public void testGetHEBHierarchy() {
	}

	/**
	 * testDeleteEntyRlshp.
	 */
	@Test
	public void testDeleteEntyRlshp() {
		try {
			this.vendorManagementDAO.deleteEntyRlshp("12765", "10>16", "16");
		} catch (DSVException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
