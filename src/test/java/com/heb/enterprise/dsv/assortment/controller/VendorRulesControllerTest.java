package com.heb.enterprise.dsv.assortment.controller;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;

import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.VendorManagementService;
import com.heb.enterprise.dsv.assortment.service.VendorRulesService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.AttrAndRuleVendorVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;
import com.heb.enterprise.dsv.vo.VendorAttributeVO;

/**
 * The Class VendorRulesControllerTest.
 * @author quang.phan
 */
@RunWith(MockitoJUnitRunner.class)
public class VendorRulesControllerTest {

	@InjectMocks
	private VendorRulesController vendorRulesController = new VendorRulesController();

	@Mock
	private VendorRulesService vendorRuleService;

	@Mock
	private HttpServletRequest request;

	@Mock
	private CommonService commonService;

	@Mock
	private CacheService cacheService;

	@Mock
	private VendorManagementService vendorManagementService;

	/**
	 * Test get rule settings for vendor1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetRuleSettingsForVendor1() throws DSVException {
		String vendorId = "v1234";
		AttrAndRuleVendorVO settings = new AttrAndRuleVendorVO();
		List<VendorAttributeVO> attributeVOs = new ArrayList<VendorAttributeVO>();
		VendorAttributeVO attributeVO = new VendorAttributeVO();
		attributeVOs.add(attributeVO);
		settings.setLstVendorAttrs(attributeVOs);
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVOs.add(assortmentRulesVO);
		settings.setLstVendorRules(assortmentRulesVOs);
		Mockito.when(this.vendorRuleService.getRuleSettingsByVendor(vendorId)).thenReturn(settings);
		// String actual = this.vendorRulesController.getRuleSettingsForVendor(vendorId, null, null);
		// String expected =
		// "{\"onlyVend\":true,\"vendorRules\":[{\"id\":null,\"name\":null,\"desc\":null,\"priority\":0,\"value\":null,\"values\":null,\"activeSw\":null,\"type\":null,\"lvlId\":0,\"seqNbr\":0,\"lstSeqNbrs\":null,\"hebComdId\":0,\"hebSubComdId\":0,\"subDeptId\":null,\"deptId\":null,\"actionCd\":null,\"vendorId\":null,\"valType\":null,\"pctSW\":null,\"itmCls\":0,\"actTypCd\":null}],\"vendorAttrs\":[{\"id\":0,\"value\":null}]}";
		// assertEquals(expected, actual);
	}

	/**
	 * Test get rule settings for vendor2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetRuleSettingsForVendor2() throws DSVException {
		String vendorId = "v1234";
		AttrAndRuleVendorVO settings = new AttrAndRuleVendorVO();
		List<VendorAttributeVO> attributeVOs = new ArrayList<VendorAttributeVO>();
		VendorAttributeVO attributeVO = new VendorAttributeVO();
		attributeVOs.add(attributeVO);
		settings.setLstVendorAttrs(attributeVOs);
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVOs.add(assortmentRulesVO);
		settings.setLstVendorRules(assortmentRulesVOs);
		Mockito.when(this.vendorRuleService.getRuleSettingsByVendor(vendorId)).thenReturn(settings);
		// String actual = this.vendorRulesController.getRuleSettingsForVendor(vendorId, "", "");
		// String expected =
		// "{\"onlyVend\":true,\"vendorRules\":[{\"id\":null,\"name\":null,\"desc\":null,\"priority\":0,\"value\":null,\"values\":null,\"activeSw\":null,\"type\":null,\"lvlId\":0,\"seqNbr\":0,\"lstSeqNbrs\":null,\"hebComdId\":0,\"hebSubComdId\":0,\"subDeptId\":null,\"deptId\":null,\"actionCd\":null,\"vendorId\":null,\"valType\":null,\"pctSW\":null,\"itmCls\":0,\"actTypCd\":null}],\"vendorAttrs\":[{\"id\":0,\"value\":null}]}";
		// assertEquals(expected, actual);
	}

	/**
	 * Test update rule settings1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdateRuleSettings1() throws DSVException {
		AttrAndRuleVendorVO vendorRule = new AttrAndRuleVendorVO();
		Mockito.when(this.commonService.getUserLogin()).thenReturn(null);
		Mockito.when(this.vendorRuleService.updateRuleSettingsForVendor(vendorRule)).thenReturn(true);
		String actual = this.vendorRulesController.updateRuleSettings(vendorRule, this.request);
		String expected = "{\"rs\":true}";
		assertEquals(expected, actual);
	}

	/**
	 * Test update rule settings2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdateRuleSettings2() throws DSVException {
		AttrAndRuleVendorVO vendorRule = new AttrAndRuleVendorVO();
		HebUserDetails user = new HebUserDetails("vn44178", "user1234", true, true, true, true, new ArrayList<GrantedAuthority>());
		Mockito.when(this.commonService.getUserLogin()).thenReturn(user);
		Mockito.when(this.vendorRuleService.updateRuleSettingsForVendor(vendorRule)).thenReturn(true);
		String actual = this.vendorRulesController.updateRuleSettings(vendorRule, this.request);
		String expected = "{\"rs\":true}";
		assertEquals(expected, actual);
	}

	public void testAjaxGetCommodityAndSub() throws DSVException {

	}

	/**
	 * Test ajax get commodity and sub1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodityAndSub1() throws DSVException {
		String vendorId = "v1234";
		String commodityCd = "123";
		List<Integer> lstHebsubComd = new ArrayList<Integer>();
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);

		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(12);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(departmentVOs);

		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("11");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);

		List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		int expected = 0;
		assertEquals(expected, actual.size());
	}

	/**
	 * Test ajax get commodity and sub2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testAjaxGetCommodityAndSub2() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		String commodityCd = "12";
		List<Integer> lstHebsubComd = new ArrayList<Integer>();
		lstHebsubComd.add(10);
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);
		List<Integer> asmLvlModified = new ArrayList<Integer>();
		asmLvlModified.add(10);
		Mockito.when(this.vendorRuleService.getAsmLVLModified("sub", vendorId)).thenReturn(asmLvlModified);
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(10);
		subCommodityVO.setSubCommodityDesc("XYZ");
		subCommodityVO.setSort(1);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setCommodityCd(96);
		commodityVO.setCommodityDesc("TEST");
		commodityVO.setBdmCd("1947");
		commodityVO.setRulesCustomize("rule 1");
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptId("1021");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(departmentVOs);
		Mockito.when(this.cacheService.getSubCommByCommodityId(commodityCd)).thenReturn(subCommodityVOs);

		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("12");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);
		List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"id\":\"id10\",\"text\":\"10 - XYZ\",\"a_attr\":{\"class\":\" markMapped jstree-checked\"}}]";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test ajax get commodity and sub3.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodityAndSub3() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		String commodityCd = "12";
		List<Integer> lstHebsubComd = new ArrayList<Integer>();
		lstHebsubComd.add(10);
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);
		List<Integer> asmLvlModified = new ArrayList<Integer>();
		asmLvlModified.add(10);
		Mockito.when(this.vendorRuleService.getAsmLVLModified("sub", vendorId)).thenReturn(asmLvlModified);
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(10);
		subCommodityVO.setSubCommodityDesc("XYZ");
		subCommodityVO.setSort(0);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setCommodityCd(96);
		commodityVO.setCommodityDesc("TEST");
		commodityVO.setBdmCd("1947");
		commodityVO.setRulesCustomize("rule 1");
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptId("1021");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(departmentVOs);
		Mockito.when(this.cacheService.getSubCommByCommodityId(commodityCd)).thenReturn(subCommodityVOs);

		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("12");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);
		List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"id\":\"id10\",\"text\":\"10 - XYZ\",\"a_attr\":{\"class\":\" jstree-checked\"}}]";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test ajax get commodity and sub3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testAjaxGetCommodityAndSub4() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		String commodityCd = "12";
		List<Integer> lstHebsubComd = null;
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);
		List<Integer> asmLvlModified = new ArrayList<Integer>();
		asmLvlModified.add(10);
		Mockito.when(this.vendorRuleService.getAsmLVLModified("sub", vendorId)).thenReturn(asmLvlModified);
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(10);
		subCommodityVO.setSubCommodityDesc("XYZ");
		subCommodityVO.setSort(0);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setCommodityCd(96);
		commodityVO.setCommodityDesc("TEST");
		commodityVO.setBdmCd("1947");
		commodityVO.setRulesCustomize("rule 1");
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptId("1021");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(departmentVOs);
		Mockito.when(this.cacheService.getSubCommByCommodityId(commodityCd)).thenReturn(subCommodityVOs);

		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("12");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);
		List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"id\":\"id10\",\"text\":\"10 - XYZ\",\"a_attr\":{\"class\":\"\"}}]";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test ajax get commodity and sub4.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Test
	public void testAjaxGetCommodityAndSub5() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		String commodityCd = "12";
		List<Integer> lstHebsubComd = new ArrayList<Integer>();
		lstHebsubComd.add(10);
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);
		List<Integer> asmLvlModified = new ArrayList<Integer>();
		asmLvlModified.add(10);
		Mockito.when(this.vendorRuleService.getAsmLVLModified("sub", vendorId)).thenReturn(asmLvlModified);
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(15);
		subCommodityVO.setSubCommodityDesc("XYZ");
		subCommodityVO.setSort(0);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setCommodityCd(96);
		commodityVO.setCommodityDesc("TEST");
		commodityVO.setBdmCd("1947");
		commodityVO.setRulesCustomize("rule 1");
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptId("1021");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(departmentVOs);
		Mockito.when(this.cacheService.getSubCommByCommodityId(commodityCd)).thenReturn(subCommodityVOs);
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("12");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);
		// List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		// ObjectMapper mapper = new ObjectMapper();
		// String expected = "[{\"id\":\"id15\",\"text\":\"15 - XYZ\",\"a_attr\":{\"class\":\"\"}}]";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test ajax get commodity and sub6.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodityAndSub6() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		String commodityCd = "#";
		List<Integer> lstHebsubComd = new ArrayList<Integer>();
		lstHebsubComd.add(10);
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);
		List<Integer> asmLvlModified = new ArrayList<Integer>();
		asmLvlModified.add(10);
		Mockito.when(this.vendorRuleService.getAsmLVLModified("comd", vendorId)).thenReturn(asmLvlModified);

		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("10");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);

		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(10);
		subCommodityVO.setSubCommodityDesc("XYZ");
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setCommodityCd(10);
		commodityVO.setCommodityDesc("TEST");
		commodityVO.setBdmCd("1947");
		commodityVO.setRulesCustomize("rule 1");
		commodityVO.setMarkMapped(1);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptId("1021");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getCommodityDetailList()).thenReturn(commodityVOs);
		List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		ObjectMapper mapper = new ObjectMapper();
		String expected = "[{\"id\":10,\"text\":\"10 - TEST\",\"a_attr\":{\"class\":\" markMapped\"},\"children\":true}]";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test ajax get commodity and sub7.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodityAndSub7() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		String commodityCd = "#";
		List<Integer> lstHebsubComd = new ArrayList<Integer>();
		lstHebsubComd.add(10);
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);
		List<Integer> asmLvlModified = new ArrayList<Integer>();
		asmLvlModified.add(15);
		Mockito.when(this.vendorRuleService.getAsmLVLModified("comd", vendorId)).thenReturn(asmLvlModified);

		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("10");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);

		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		commodityVO.setCommodityCd(12);
		commodityVO.setCommodityDesc("TEST");
		commodityVO.setBdmCd("1947");
		commodityVO.setRulesCustomize("rule 1");
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptId("1021");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getCommodityDetailList()).thenReturn(commodityVOs);
		// List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		// ObjectMapper mapper = new ObjectMapper();
		// String expected = "[{\"id\":12,\"text\":\"12 - TEST\",\"a_attr\":{\"class\":\"\"},\"children\":false}]";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test ajax get commodity and sub8.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodityAndSub8() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		String commodityCd = "#";
		List<Integer> lstHebsubComd = new ArrayList<Integer>();
		lstHebsubComd.add(10);
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);
		List<Integer> asmLvlModified = null;
		Mockito.when(this.vendorRuleService.getAsmLVLModified("comd", vendorId)).thenReturn(asmLvlModified);

		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("10");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);

		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		commodityVO.setCommodityCd(12);
		commodityVO.setCommodityDesc("TEST");
		commodityVO.setBdmCd("1947");
		commodityVO.setRulesCustomize("rule 1");
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptId("1021");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getCommodityDetailList()).thenReturn(commodityVOs);
		// List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		// ObjectMapper mapper = new ObjectMapper();
		// String expected = "[{\"id\":12,\"text\":\"12 - TEST\",\"a_attr\":{\"class\":\"\"},\"children\":false}]";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test ajax get commodity and sub9.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetCommodityAndSub9() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		String commodityCd = "12";
		List<Integer> lstHebsubComd = new ArrayList<Integer>();
		lstHebsubComd.add(10);
		Mockito.when(this.vendorRuleService.getActiveSw(vendorId)).thenReturn(lstHebsubComd);
		List<Integer> asmLvlModified = null;
		Mockito.when(this.vendorRuleService.getAsmLVLModified("sub", vendorId)).thenReturn(asmLvlModified);
		List<DepartmentVO> departmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(15);
		subCommodityVO.setSubCommodityDesc("XYZ");
		subCommodityVO.setSort(0);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setCommodityCd(96);
		commodityVO.setCommodityDesc("TEST");
		commodityVO.setBdmCd("1947");
		commodityVO.setRulesCustomize("rule 1");
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptId("1021");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		departmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(departmentVOs);
		Mockito.when(this.cacheService.getSubCommByCommodityId(commodityCd)).thenReturn(subCommodityVOs);
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
		hierarchyMapVO.setSubCommodityCd("12");
		hierarchyMapVO.setSubCommodity("C");
		hierarchyMapVOs.add(hierarchyMapVO);
		Mockito.when(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId)).thenReturn(hierarchyMapVOs);
		// List<JSONObject> actual = this.vendorRulesController.ajaxGetCommodityAndSub(vendorId, commodityCd);
		// ObjectMapper mapper = new ObjectMapper();
		// String expected = "[{\"id\":\"id15\",\"text\":\"15 - XYZ\",\"a_attr\":{\"class\":\"\"}}]";
		// assertEquals(expected, mapper.writeValueAsString(actual));
	}

	/**
	 * Test reset rules to higher level1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testResetRulesToHigherLevel1() throws DSVException {
		String vendorId = "v1234";
		String commodityId = "1702";
		String subCommodityId = "12C";
		Mockito.when(this.vendorRuleService.resetRulesToHigherLevel(vendorId, commodityId, subCommodityId)).thenReturn(true);
		String actual = this.vendorRulesController.resetRulesToHigherLevel(vendorId, commodityId, subCommodityId);
		String expected = "true";
		assertEquals(expected, actual);
	}

	/**
	 * Test reset rules to higher level2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testResetRulesToHigherLevel2() throws DSVException {
		String vendorId = "v1234";
		String commodityId = "1702";
		String subCommodityId = "12C";
		Mockito.when(this.vendorRuleService.resetRulesToHigherLevel(vendorId, commodityId, subCommodityId)).thenReturn(false);
		String actual = this.vendorRulesController.resetRulesToHigherLevel(vendorId, commodityId, subCommodityId);
		String expected = "false";
		assertEquals(expected, actual);
	}

	/**
	 * Test get rule settings for vendor3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetRuleSettingsForVendor3() throws DSVException {
		String vendorId = "v1234";
		String commodityId = "1702";
		String subCommodityId = "12C";
		Map<String, AssortmentRulesVO> lstRuleRs = new HashMap<String, AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setName("ABC");
		List<String> lstValue = new ArrayList<String>();
		lstValue.add("10");
		lstValue.add("20");
		assortmentRulesVO.setValues(lstValue);
		assortmentRulesVO.setActiveSw("Y");
		assortmentRulesVO.setHebComdId(1772);
		assortmentRulesVO.setHebSubComdId(111212);
		assortmentRulesVO.setSubDeptId("12C");
		assortmentRulesVO.setLvlId(111L);
		List<Integer> lstInt = new ArrayList<Integer>();
		lstInt.add(1);
		lstInt.add(2);
		assortmentRulesVO.setLstSeqNbrs(lstInt);
		lstRuleRs.put("1", assortmentRulesVO);
		Mockito.when(this.vendorRuleService.getRuleSettingsForVendor(vendorId, commodityId, subCommodityId)).thenReturn(lstRuleRs);
		String actual = this.vendorRulesController.getRuleSettingsForVendor(vendorId, commodityId, subCommodityId);
		String expected = "{\"onlyVend\":false,\"lstRules\":[{\"subDeptId\":\"12C\",\"id\":\"123\",\"seqNbr\":[1,2],\"lvlId\":111,\"HebComdId\":1772,\"values\":[\"10\",\"20\"],\"lstUpdateTs\":null,\"subComdId\":111212,\"name\":\"ABC\",\"lstUpdateUid\":null,\"activeSw\":\"Y\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test get rule settings for vendor4.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetRuleSettingsForVendor4() throws DSVException {
		String vendorId = "v1234";
		String commodityId = "1702";
		String subCommodityId = "12C";
		Map<String, AssortmentRulesVO> lstRuleRs = new HashMap<String, AssortmentRulesVO>();
		lstRuleRs.put("1", null);
		Mockito.when(this.vendorRuleService.getRuleSettingsForVendor(vendorId, commodityId, subCommodityId)).thenReturn(lstRuleRs);
		String actual = this.vendorRulesController.getRuleSettingsForVendor(vendorId, commodityId, subCommodityId);
		String expected = "{\"onlyVend\":false,\"lstRules\":[]}";
		assertEquals(expected, actual);
	}
}
