/*
 * 
 */
package com.heb.enterprise.dsv.assortment.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;

import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.ReviewNewProductsService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

@RunWith(MockitoJUnitRunner.class)
public class BDMReviewControllerTest {

	@InjectMocks
	private BDMReviewController bdmReviewController = new BDMReviewController();

	@Mock
	private ReviewNewProductsService productForReviewService;

	@Mock
	private MessageSource messageSource;

	@Mock
	private CacheService cacheService;

	@Mock
	private CommonService commonService;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Test
	public void testShowNewProductForReviewScreen() {
	}

	/**
	 * Test search for product1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct1() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("20/11/2015");
		productSearchCriteriaVO.setDateEnd("25/11/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("Y");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":0,\"data\":{\"Receive Date End\":null,\"Receive Date Begin\":null},\"totalpassed\":0,\"recordsTotal\":0,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct2() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("11/25/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("Y");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":1,\"data\":[{\"firstReceivedTS\":null,\"hebRetail\":null,\"status\":null,\"lastEditedTS\":null,\"no\":2,\"reviewStatus\":\"Y\",\"image\":\"img\",\"failedReason\":\"Fail\",\"gppercent\":\"20\",\"upc\":\"upc1\",\"cost\":\"30\",\"webStatusCd\":null,\"size\":\"Max\",\"srp\":\"srp\",\"pennyProfit\":\"penny\",\"msrp\":\"msr\",\"description\":\"TEST\",\"map\":\"Map\",\"totalRow\":1,\"failedReasonCode\":null,\"prePrice\":\"40\"}],\"totalpassed\":0,\"recordsTotal\":1,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct3() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("11/25/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("Y");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(null);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":1,\"data\":[],\"totalpassed\":0,\"recordsTotal\":1,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product4.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct4() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("11/25/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":1,\"data\":[],\"totalpassed\":0,\"recordsTotal\":1,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product5.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct5() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("zz");
		productSearchCriteriaVO.setMarginEnd("");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("P");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":0,\"data\":{\"Margin begin\":null,\"Receive Date Range\":null},\"totalpassed\":0,\"recordsTotal\":0,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product6.
	 * @author quang.phan
	 * @throws DSVException
	 *             the DSV exception
	 */
	@Test
	public void testSearchForProduct6() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("11/25/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("F");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":1,\"data\":[{\"firstReceivedTS\":null,\"hebRetail\":null,\"status\":null,\"lastEditedTS\":null,\"no\":2,\"reviewStatus\":\"Y\",\"image\":\"img\",\"failedReason\":\"Fail\",\"gppercent\":\"20\",\"upc\":\"upc1\",\"cost\":\"30\",\"webStatusCd\":null,\"size\":\"Max\",\"srp\":\"srp\",\"pennyProfit\":\"penny\",\"msrp\":\"msr\",\"description\":\"TEST\",\"map\":\"Map\",\"totalRow\":1,\"failedReasonCode\":null,\"prePrice\":\"40\"}],\"totalpassed\":0,\"recordsTotal\":1,\"totalfailed\":2}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product7.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct7() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("11/25/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("R");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":1,\"data\":[{\"firstReceivedTS\":null,\"hebRetail\":null,\"status\":null,\"lastEditedTS\":null,\"no\":2,\"reviewStatus\":\"Y\",\"image\":\"img\",\"failedReason\":\"Fail\",\"gppercent\":\"20\",\"upc\":\"upc1\",\"cost\":\"30\",\"webStatusCd\":null,\"size\":\"Max\",\"srp\":\"srp\",\"pennyProfit\":\"penny\",\"msrp\":\"msr\",\"description\":\"TEST\",\"map\":\"Map\",\"totalRow\":1,\"failedReasonCode\":null,\"prePrice\":\"40\"}],\"totalpassed\":0,\"recordsTotal\":1,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product8.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct8() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("11/25/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("P");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, this.request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":1,\"data\":[{\"firstReceivedTS\":null,\"hebRetail\":null,\"status\":null,\"lastEditedTS\":null,\"no\":2,\"reviewStatus\":\"Y\",\"image\":\"img\",\"failedReason\":\"Fail\",\"gppercent\":\"20\",\"upc\":\"upc1\",\"cost\":\"30\",\"webStatusCd\":null,\"size\":\"Max\",\"srp\":\"srp\",\"pennyProfit\":\"penny\",\"msrp\":\"msr\",\"description\":\"TEST\",\"map\":\"Map\",\"totalRow\":1,\"failedReasonCode\":null,\"prePrice\":\"40\"}],\"totalpassed\":2,\"recordsTotal\":1,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product9.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct9() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("11/25/2015");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("RJECT");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, this.request, response);
		String expected = "{\"totalrejected\":2,\"recordsFiltered\":1,\"data\":[{\"firstReceivedTS\":null,\"hebRetail\":null,\"status\":null,\"lastEditedTS\":null,\"no\":2,\"reviewStatus\":\"Y\",\"image\":\"img\",\"failedReason\":\"Fail\",\"gppercent\":\"20\",\"upc\":\"upc1\",\"cost\":\"30\",\"webStatusCd\":null,\"size\":\"Max\",\"srp\":\"srp\",\"pennyProfit\":\"penny\",\"msrp\":\"msr\",\"description\":\"TEST\",\"map\":\"Map\",\"totalRow\":1,\"failedReasonCode\":null,\"prePrice\":\"40\"}],\"totalpassed\":0,\"recordsTotal\":1,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test search for product10.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProduct10() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		productSearchCriteriaVO.setStart("1");
		productSearchCriteriaVO.setMarginBegin("10");
		productSearchCriteriaVO.setMarginEnd("20");
		productSearchCriteriaVO.setDateBegin("11/20/2015");
		productSearchCriteriaVO.setDateEnd("11/25/2015");
		productSearchCriteriaVO.setProdDes("12345645455");
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		page.setRowCount(1);
		List<ProductRiewVO> productRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = new ProductRiewVO();
		productRiewVO.setUpc("upc1");
		productRiewVO.setDescription("TEST");
		productRiewVO.setSize("Max");
		productRiewVO.setMap("Map");
		productRiewVO.setPrePrice("40");
		productRiewVO.setMsrp("msr");
		productRiewVO.setSrp("srp");
		productRiewVO.setCost("30");
		productRiewVO.setGpPercent("20");
		productRiewVO.setPennyProfit("penny");
		productRiewVO.setImageUri("img");
		productRiewVO.setFailedReason("Fail");
		productRiewVO.setTotalRow(1);
		productRiewVO.setReviewStatus("Y");
		productRiewVOs.add(productRiewVO);
		page.setPageItems(productRiewVOs);
		List<ProductRiewVO> lstCountProductRiewVO = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO2 = new ProductRiewVO();
		productRiewVO2.setReviewStatus("Y");
		productRiewVO2.setTotalRow(2);
		lstCountProductRiewVO.add(productRiewVO2);
		Mockito.when(this.productForReviewService.searchForProducts(productSearchCriteriaVO)).thenReturn(page);
		Mockito.when(this.productForReviewService.countProudctRev(productSearchCriteriaVO)).thenReturn(lstCountProductRiewVO);
		String actual = this.bdmReviewController.searchForProduct(productSearchCriteriaVO, request, response);
		String expected = "{\"totalrejected\":0,\"recordsFiltered\":1,\"data\":[{\"firstReceivedTS\":null,\"hebRetail\":null,\"status\":null,\"lastEditedTS\":null,\"no\":2,\"reviewStatus\":\"Y\",\"image\":\"img\",\"failedReason\":\"Fail\",\"gppercent\":\"20\",\"upc\":\"upc1\",\"cost\":\"30\",\"webStatusCd\":null,\"size\":\"Max\",\"srp\":\"srp\",\"pennyProfit\":\"penny\",\"msrp\":\"msr\",\"description\":\"TEST\",\"map\":\"Map\",\"totalRow\":1,\"failedReasonCode\":null,\"prePrice\":\"40\"}],\"totalpassed\":0,\"recordsTotal\":1,\"totalfailed\":0}";
		assertEquals(expected, actual);
	}

	/**
	 * Test Approved Products.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testApproveProducts1() throws DSVException {
		List<ProductRiewVO> lstProds = new ArrayList<ProductRiewVO>();
		ProductRiewVO prodR = new ProductRiewVO();
		prodR.setUpc("16291442443");
		prodR.setHebRetail("12345");
		lstProds.add(prodR);
		List<String> listProducts = new ArrayList<String>();
		listProducts.add("16291442443");
		Mockito.when(this.commonService.getUserLogin()).thenReturn(null);
		Mockito.when(this.productForReviewService.checkApproveReject(listProducts)).thenReturn(true);
		Mockito.when(this.productForReviewService.updateProducts(lstProds, "VN002")).thenReturn("SUCCESS");
		String actual = this.bdmReviewController.approveProducts(lstProds, request);
		assertEquals("{}", actual);
	}

	/**
	 * Test approve products2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testApproveProducts2() throws DSVException {
		List<ProductRiewVO> lstProds = new ArrayList<ProductRiewVO>();
		ProductRiewVO prodR = new ProductRiewVO();
		prodR.setUpc("16291442443");
		prodR.setHebRetail("12345");
		lstProds.add(prodR);
		List<String> listProducts = new ArrayList<String>();
		listProducts.add("16291442443");
		HebUserDetails user = new HebUserDetails("vn44178", "user1234", true, true, true, true, new ArrayList<GrantedAuthority>());
		Mockito.when(this.commonService.getUserLogin()).thenReturn(user);
		Mockito.when(this.productForReviewService.checkApproveReject(listProducts)).thenReturn(true);
		Mockito.when(this.productForReviewService.updateProducts(lstProds, "VN002")).thenReturn("SUCCESS");
		String actual = this.bdmReviewController.approveProducts(lstProds, request);
		assertEquals("{}", actual);
	}

	/**
	 * Test approve products3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testApproveProducts3() throws DSVException {
		List<ProductRiewVO> lstProds = new ArrayList<ProductRiewVO>();
		List<String> listProducts = new ArrayList<String>();
		listProducts.add("16291442443");
		Mockito.when(this.commonService.getUserLogin()).thenReturn(null);
		Mockito.when(this.productForReviewService.checkApproveReject(listProducts)).thenReturn(true);
		Mockito.when(this.productForReviewService.updateProducts(lstProds, "VN002")).thenReturn("SUCCESS");
		String actual = this.bdmReviewController.approveProducts(lstProds, request);
		assertEquals("{\"errors\":null}", actual);
	}

	/**
	 * Test Update Heb Retail.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testUpdateProducts1() throws DSVException {
		List<ProductRiewVO> lstProds = new ArrayList<ProductRiewVO>();
		ProductRiewVO prodR = new ProductRiewVO();
		prodR.setUpc("16291442443");
		prodR.setHebRetail("12345");
		lstProds.add(prodR);
		HebUserDetails user = new HebUserDetails("vn44178", "user1234", true, true, true, true, new ArrayList<GrantedAuthority>());
		Mockito.when(this.commonService.getUserLogin()).thenReturn(user);
		Mockito.when(this.productForReviewService.updateProducts(lstProds, "VN001")).thenReturn("SUCCESS");
		String actual = this.bdmReviewController.updateProducts(lstProds, request);
		String expected = "{}";
		assertEquals(expected, actual);
	}

	/**
	 * Test update products2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdateProducts2() throws DSVException {
		List<ProductRiewVO> lstProds = new ArrayList<ProductRiewVO>();
		ProductRiewVO prodR = new ProductRiewVO();
		prodR.setUpc("16291442443");
		prodR.setHebRetail("12345");
		lstProds.add(prodR);
		Mockito.when(this.productForReviewService.updateProducts(lstProds, "VN001")).thenReturn("SUCCESS");
		String actual = this.bdmReviewController.updateProducts(lstProds, request);
		String expected = "{}";
		assertEquals(expected, actual);
	}

	/**
	 * Test reject products.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testRejectProduct1() throws DSVException {
		List<String> listProducts = new ArrayList<String>();
		listProducts.add("16291442443");
		Mockito.when(this.commonService.getUserLogin()).thenReturn(null);
		Mockito.when(this.productForReviewService.checkApproveReject(listProducts)).thenReturn(true);
		String actual = this.bdmReviewController.rejectProducts(listProducts, request);
		assertEquals("{}", actual);
	}

	/**
	 * Test reject product2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testRejectProduct2() throws DSVException {
		List<String> listProducts = new ArrayList<String>();
		listProducts.add("16291442443");
		HebUserDetails user = new HebUserDetails("vn44178", "user1234", true, true, true, true, new ArrayList<GrantedAuthority>());
		Mockito.when(this.commonService.getUserLogin()).thenReturn(user);
		Mockito.when(this.productForReviewService.checkApproveReject(listProducts)).thenReturn(true);
		String actual = this.bdmReviewController.rejectProducts(listProducts, request);
		assertEquals("{}", actual);
	}

	/**
	 * Test reject product3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testRejectProduct3() throws DSVException {
		List<String> listProducts = new ArrayList<String>();
		HebUserDetails user = new HebUserDetails("vn44178", "user1234", true, true, true, true, new ArrayList<GrantedAuthority>());
		Mockito.when(this.commonService.getUserLogin()).thenReturn(user);
		Mockito.when(this.productForReviewService.checkApproveReject(listProducts)).thenReturn(false);
		String actual = this.bdmReviewController.rejectProducts(listProducts, request);
		assertEquals("{\"errors\":null}", actual);
	}

	/**
	 * Test un-reject products.
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@Test
	public void testUnRejectProduct1() throws DSVException {
		List<String> listProducts = new ArrayList<String>();
		listProducts.add("16291442443");
		Mockito.when(this.commonService.getUserLogin()).thenReturn(null);
		Mockito.when(this.productForReviewService.checkUnReject(listProducts)).thenReturn(true);
		String actual = this.bdmReviewController.unrejectProducts(listProducts, request);
		assertEquals("{}", actual);
	}

	/**
	 * Test un reject product2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUnRejectProduct2() throws DSVException {
		List<String> listProducts = new ArrayList<String>();
		listProducts.add("16291442443");
		HebUserDetails user = new HebUserDetails("vn44178", "user1234", true, true, true, true, new ArrayList<GrantedAuthority>());
		Mockito.when(this.commonService.getUserLogin()).thenReturn(user);
		Mockito.when(this.productForReviewService.checkUnReject(listProducts)).thenReturn(true);
		String actual = this.bdmReviewController.unrejectProducts(listProducts, request);
		assertEquals("{}", actual);
	}

	/**
	 * Test un reject product3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUnRejectProduct3() throws DSVException {
		List<String> listProducts = new ArrayList<String>();
		Mockito.when(this.commonService.getUserLogin()).thenReturn(null);
		Mockito.when(this.productForReviewService.checkUnReject(listProducts)).thenReturn(false);
		String actual = this.bdmReviewController.unrejectProducts(listProducts, request);
		assertEquals("{\"errors\":null}", actual);
	}

	/**
	 * Test ajax get data commodity1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataCommodity1() throws DSVException {
		List<CommodityVO> lstCommodities = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVO.setCommodityDesc("TEST");
		lstCommodities.add(commodityVO);
		Mockito.when(this.cacheService.getCommodityList()).thenReturn(lstCommodities);
		String actual = this.bdmReviewController.ajaxGetDataCommodity();
		String expected = "{\"data\":[{\"name\":\"TEST\",\"value\":123,\"label\":\"123-TEST\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data commodity2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataCommodity2() throws DSVException {
		Mockito.when(this.cacheService.getCommodityList()).thenReturn(null);
		String actual = this.bdmReviewController.ajaxGetDataCommodity();
		String expected = "{\"data\":[]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data brand1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataBrand1() throws DSVException {
		String termSearch = "asc";
		List<BaseVO> lstBrand = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setId("123");
		baseVO.setName("TEST");
		lstBrand.add(baseVO);
		Mockito.when(this.cacheService.searchbrand(termSearch)).thenReturn(lstBrand);
		String actual = this.bdmReviewController.ajaxGetDataBrand(termSearch);
		String expected = "{\"data\":[{\"name\":\"TEST\",\"value\":\"123\",\"label\":\"123-TEST\"}]}";
		// assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data brand2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataBrand2() throws DSVException {
		String termSearch = "asc";
		Mockito.when(this.cacheService.searchbrand(termSearch)).thenReturn(null);
		String actual = this.bdmReviewController.ajaxGetDataBrand(termSearch);
		String expected = "{\"data\":[]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data failed reason1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataFailedReason1() throws DSVException {
		List<BaseVO> lstFailedReason = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setId("R0001");
		baseVO.setName("ABC");
		lstFailedReason.add(baseVO);
		baseVO.setId("R0002");
		baseVO.setName("ABC");
		lstFailedReason.add(baseVO);
		baseVO.setId("R0003");
		baseVO.setName("ABC");
		lstFailedReason.add(baseVO);
		baseVO.setId("R0004");
		baseVO.setName("ABC");
		lstFailedReason.add(baseVO);
		baseVO.setId("R0005");
		baseVO.setName("ABC");
		lstFailedReason.add(baseVO);
		baseVO.setId("R0006");
		baseVO.setName("ABC");
		lstFailedReason.add(baseVO);
		Mockito.when(this.productForReviewService.getListFailedReasons()).thenReturn(lstFailedReason);
		String actual = this.bdmReviewController.ajaxGetDataFailedReason();
		// String expected = "{\"data\":[{\"name\":\"ABC\",\"value\":\"123\",\"label\":\"123-ABC\"}]}";
		// assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data failed reason2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataFailedReason2() throws DSVException {
		List<BaseVO> lstFailedReason = null;
		Mockito.when(this.commonService.getAllAssortmentRule()).thenReturn(lstFailedReason);
		String actual = this.bdmReviewController.ajaxGetDataFailedReason();
		String expected = "{\"data\":[]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data products autocomplete.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataProduct_1() throws DSVException {
		List<String> lstProduct = null;
		String searchKey = "ABC";
		Mockito.when(this.cacheService.searchProductDesc(searchKey)).thenReturn(lstProduct);
		String actual = this.bdmReviewController.ajaxGetDataProduct("ABC");
		String expected = "{\"data\":[]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data products autocomplete.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataProduct_2() throws DSVException {
		List<String> lstProduct = new ArrayList<String>();
		lstProduct.add("PROD 1");
		String searchKey = "ABC";
		Mockito.when(this.cacheService.searchProductDesc(searchKey)).thenReturn(lstProduct);
		String actual = this.bdmReviewController.ajaxGetDataProduct("ABC");
		String expected = "{\"data\":[{\"label\":\"PROD 1\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data products autocomplete.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testExportToExcel() throws DSVException {
		List<ProductRiewVO> lstPrdsToExport = new ArrayList<ProductRiewVO>();
		ProductSearchCriteriaVO prodSearchModel = new ProductSearchCriteriaVO();
		Mockito.when(this.productForReviewService.exportExcelForProducts(prodSearchModel)).thenReturn(lstPrdsToExport);
		this.bdmReviewController.exportToExcel(request, response, prodSearchModel);
	}

	/**
	 * Test ajax get data vendor1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataVendor1() throws DSVException {
		List<VendorInfoVO> vendorInfoVOs = new ArrayList<VendorInfoVO>();
		VendorInfoVO vendorInfoVO = new VendorInfoVO();
		vendorInfoVO.setVendorId("999999");
		vendorInfoVO.setVendorName("TEMP");
		vendorInfoVOs.add(vendorInfoVO);
		Mockito.when(this.commonService.getAllListVendors()).thenReturn(vendorInfoVOs);
		String actual = this.bdmReviewController.ajaxGetDataVendor();
		String expected = "{\"data\":[{\"name\":\"TEMP\",\"value\":\"999999\",\"label\":\"999999-TEMP\"}]}";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get data vendor2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetDataVendor2() throws DSVException {
		List<VendorInfoVO> vendorInfoVOs = new ArrayList<VendorInfoVO>();
		Mockito.when(this.commonService.getAllListVendors()).thenReturn(vendorInfoVOs);
		String actual = this.bdmReviewController.ajaxGetDataVendor();
		assertEquals("{\"data\":[]}", actual);
	}
}
