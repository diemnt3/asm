package com.heb.enterprise.dsv.assortment.service.impl;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.assortment.dao.GlobalRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.PricingRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.RulesDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.PricingRuleVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;

/**
 * The Class PricingRulesServiceImplTest.
 * @author quang.phan
 */
@RunWith(MockitoJUnitRunner.class)
public class PricingRulesServiceImplTest {

	@InjectMocks
	private PricingRulesServiceImpl pricingRulesServiceImpl = new PricingRulesServiceImpl();

	@Mock
	private PricingRulesDAO pricingRulesDAO;

	@Mock
	private CacheService cacheService;

	@Mock
	private RulesDAO rulesDAO;

	@Mock
	private GlobalRulesDAO globalRulesDAO;

	/**
	 * Test get list commodities.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetListCommodities() throws DSVException {
		Mockito.when(this.cacheService.getCommodityDetailList()).thenReturn(null);
		List<CommodityVO> actual = this.pricingRulesServiceImpl.getListCommodities();
		assertEquals(actual, null);
	}

	/**
	 * Test get list sub comm by commodity id.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetListSubCommByCommodityId() throws DSVException {
		String commodityId = "123";
		Mockito.when(this.cacheService.getSubCommByCommodityId(commodityId)).thenReturn(null);
		List<SubCommodityVO> actual = this.pricingRulesServiceImpl.getListSubCommByCommodityId(commodityId);
		assertEquals(actual, null);
	}

	/**
	 * Test get online competitors.
	 * @author quang.phan
	 */
	@Test
	public void testGetOnlineCompetitors() {
		this.pricingRulesServiceImpl.getOnlineCompetitors();
	}

	/**
	 * Test get act typ.
	 * @author quang.phan
	 */
	@Test
	public void testGetActTyp() {
		this.pricingRulesServiceImpl.getActTyp();
	}

	/**
	 * Test get pricing rules1.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetPricingRules1() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		PricingRuleVO actual = this.pricingRulesServiceImpl.getPricingRules("", "", "");
		assertEquals(mapper.writeValueAsString(actual.getLstAssortmentRulesVODisplays()), "null");
		assertEquals(mapper.writeValueAsString(actual.getLstCompetitors()), "[]");
	}

	/**
	 * Test get pricing rules2.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetPricingRules2() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String vendorId = "v1234";
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setSeqNbr(100);
		assortmentRulesVO.setValue("TEST");
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		List<String> lstCompetitors = new ArrayList<String>();
		lstCompetitors.add("Walmart");
		Mockito.when(this.pricingRulesDAO.getPricingRuleByVendor(vendorId)).thenReturn(lstAssortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getPricingCompetitorByVendor(vendorId)).thenReturn(lstCompetitors);
		PricingRuleVO actual = this.pricingRulesServiceImpl.getPricingRules(vendorId, "", "");
		ObjectMapper mapper = new ObjectMapper();
		String expected1 = "{\"123\":{\"id\":\"123\",\"name\":null,\"desc\":null,\"priority\":0,\"value\":\"TEST\",\"values\":null,\"activeSws\":[],\"activeSw\":null,\"type\":null,\"lvlId\":0,\"seqNbr\":100,\"lstSeqNbrs\":null,\"hebComdId\":0,\"hebSubComdId\":0,\"subDeptId\":null,\"deptId\":null,\"actionCd\":null,\"vendorId\":null,\"valType\":null,\"pctSW\":null,\"itmCls\":0,\"actTypCd\":null,\"lstUpdateUid\":null,\"lstUpdateTs\":null,\"root\":true}}";
		String expected2 = "[\"Walmart\"]";
		assertEquals(expected1, mapper.writeValueAsString(actual.getLstAssortmentRulesVODisplays()));
		assertEquals(expected2, mapper.writeValueAsString(actual.getLstCompetitors()));
	}

	/**
	 * Test get pricing rules3.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetPricingRules3() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String commodityId = "1704";
		String subCommodityId = "12C";
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setSeqNbr(100);
		assortmentRulesVO.setValue("TEST");
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		List<String> lstCompetitors = new ArrayList<String>();
		lstCompetitors.add("Walmart");
		Mockito.when(this.pricingRulesDAO.getPricingRuleByCommoditySubCommodity(commodityId, subCommodityId)).thenReturn(lstAssortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getPricingCompetitorByCommoditySubCommodity(commodityId, subCommodityId)).thenReturn(null);
		Mockito.when(this.pricingRulesDAO.getPricingCompetitorByCommodity(commodityId)).thenReturn(lstCompetitors);
		PricingRuleVO actual = this.pricingRulesServiceImpl.getPricingRules("", commodityId, subCommodityId);
		ObjectMapper mapper = new ObjectMapper();
		String expected1 = "{\"123\":{\"id\":\"123\",\"name\":null,\"desc\":null,\"priority\":0,\"value\":\"TEST\",\"values\":null,\"activeSws\":[],\"activeSw\":null,\"type\":null,\"lvlId\":0,\"seqNbr\":100,\"lstSeqNbrs\":null,\"hebComdId\":0,\"hebSubComdId\":0,\"subDeptId\":null,\"deptId\":null,\"actionCd\":null,\"vendorId\":null,\"valType\":null,\"pctSW\":null,\"itmCls\":0,\"actTypCd\":null,\"lstUpdateUid\":null,\"lstUpdateTs\":null,\"root\":true}}";
		String expected2 = "[\"Walmart\"]";
		assertEquals(expected1, mapper.writeValueAsString(actual.getLstAssortmentRulesVODisplays()));
		assertEquals(expected2, mapper.writeValueAsString(actual.getLstCompetitors()));
	}

	/**
	 * Test get pricing rules4.
	 * @throws DSVException
	 *             the DSV exception
	 * @throws JsonGenerationException
	 *             the json generation exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @author quang.phan
	 */
	@Test
	public void testGetPricingRules4() throws DSVException, JsonGenerationException, JsonMappingException, IOException {
		String commodityId = "1704";
		String subCommodityId = "12C";
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setSeqNbr(100);
		assortmentRulesVO.setValue("TEST");
		lstAssortmentRulesVOs.add(assortmentRulesVO);
		List<String> lstCompetitors = new ArrayList<String>();
		lstCompetitors.add("Walmart");
		Mockito.when(this.pricingRulesDAO.getPricingRuleByCommoditySubCommodity(commodityId, subCommodityId)).thenReturn(lstAssortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getPricingCompetitorByCommoditySubCommodity(commodityId, subCommodityId)).thenReturn(lstCompetitors);
		Mockito.when(this.pricingRulesDAO.getPricingCompetitorByCommodity(commodityId)).thenReturn(lstCompetitors);
		PricingRuleVO actual = this.pricingRulesServiceImpl.getPricingRules("", commodityId, subCommodityId);
		ObjectMapper mapper = new ObjectMapper();
		String expected1 = "{\"123\":{\"id\":\"123\",\"name\":null,\"desc\":null,\"priority\":0,\"value\":\"TEST\",\"values\":null,\"activeSws\":[],\"activeSw\":null,\"type\":null,\"lvlId\":0,\"seqNbr\":100,\"lstSeqNbrs\":null,\"hebComdId\":0,\"hebSubComdId\":0,\"subDeptId\":null,\"deptId\":null,\"actionCd\":null,\"vendorId\":null,\"valType\":null,\"pctSW\":null,\"itmCls\":0,\"actTypCd\":null,\"lstUpdateUid\":null,\"lstUpdateTs\":null,\"root\":true}}";
		String expected2 = "[\"Walmart\"]";
		assertEquals(expected1, mapper.writeValueAsString(actual.getLstAssortmentRulesVODisplays()));
		assertEquals(expected2, mapper.writeValueAsString(actual.getLstCompetitors()));
	}

	/**
	 * Test update pricing rules1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingRules1() throws DSVException {
		String vendorId = "v1234";
		String userId = "vn44178";
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		pricingRuleVO.setVendorId(vendorId);
		List<String> lstCompetitor = new ArrayList<String>();
		lstCompetitor.add("Walmart");
		pricingRuleVO.setLstCompetitors(lstCompetitor);
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		pricingRuleVO.setLstAssortmentRulesVOs(assortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getLvlByVendor(vendorId)).thenReturn(5l);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Mockito.when(this.rulesDAO.updateRuleValuesToDfns(lstRuleDefinationVOs, 5l, userId)).thenReturn("success");
		Mockito.when(this.pricingRulesDAO.removeComparetorInfo(5l)).thenReturn("success");
		String actual = this.pricingRulesServiceImpl.updatePricingRules(userId, pricingRuleVO);
		String expected = "Successfully";
		assertEquals(expected, actual);
	}

	/**
	 * Test update pricing rules2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingRules2() throws DSVException {
		String vendorId = "v1234";
		String userId = "vn44178";
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		pricingRuleVO.setVendorId(vendorId);
		List<String> lstCompetitor = new ArrayList<String>();
		lstCompetitor.add("Walmart");
		pricingRuleVO.setLstCompetitors(lstCompetitor);
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		List<String> values = new ArrayList<String>();
		values.add("Start-1");
		assortmentRulesVO.setValues(values);
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActiveSw("A");
		assortmentRulesVO.setValType("number");
		assortmentRulesVO.setLvlId(5l);
		assortmentRulesVO.setPctSW("zzz");
		assortmentRulesVOs.add(assortmentRulesVO);
		pricingRuleVO.setLstAssortmentRulesVOs(assortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getLvlByVendor(vendorId)).thenReturn(5l);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Mockito.when(this.rulesDAO.updateRuleValuesToDfns(lstRuleDefinationVOs, 5l, userId)).thenReturn("success");
		Mockito.when(this.pricingRulesDAO.removeComparetorInfo(5l)).thenReturn("success");
		String actual = this.pricingRulesServiceImpl.updatePricingRules(userId, pricingRuleVO);
		String expected = "Successfully";
		assertEquals(expected, actual);
	}

	/**
	 * Test update pricing rules3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingRules3() throws DSVException {
		String vendorId = "v1234";
		String userId = "vn44178";

		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("999");
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptIdOri("AAA");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(new BigDecimal(1000));
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(12);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);

		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		pricingRuleVO.setVendorId(vendorId);
		List<String> lstCompetitor = new ArrayList<String>();
		lstCompetitor.add("Walmart");
		pricingRuleVO.setLstCompetitors(lstCompetitor);
		pricingRuleVO.setCommodityId("123");
		pricingRuleVO.setSubCommodityId("12");
		pricingRuleVO.setLvlType("D");

		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		List<String> values = new ArrayList<String>();
		values.add("Start-1");
		assortmentRulesVO.setValues(values);
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActiveSw("A");
		assortmentRulesVO.setValType("number");
		assortmentRulesVO.setLvlId(5l);
		assortmentRulesVO.setPctSW("zzz");
		assortmentRulesVOs.add(assortmentRulesVO);
		pricingRuleVO.setLstAssortmentRulesVOs(assortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getLvlByVendor(vendorId)).thenReturn(0l);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Mockito.when(this.rulesDAO.updateRuleValuesToDfns(lstRuleDefinationVOs, 0l, userId)).thenReturn("success");
		Mockito.when(this.pricingRulesDAO.removeComparetorInfo(0l)).thenReturn("success");
		Mockito.when(this.globalRulesDAO.getMaxLVLID()).thenReturn(5l);

		String actual = this.pricingRulesServiceImpl.updatePricingRules(userId, pricingRuleVO);
		String expected = "Successfully";
		assertEquals(expected, actual);
	}

	/**
	 * Test update pricing rules4.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingRules4() throws DSVException {
		String vendorId = "v1234";
		String userId = "vn44178";
		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		pricingRuleVO.setVendorId(vendorId);
		pricingRuleVO.setCommodityId("123");
		pricingRuleVO.setSubCommodityId("12");
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenThrow(new DSVException("error"));
		// String actual = this.pricingRulesServiceImpl.updatePricingRules(userId, pricingRuleVO);
		// String expected = "Successfully";
		// assertEquals(expected, actual);
	}

	/**
	 * Test update pricing rules5.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingRules5() throws DSVException {
		String vendorId = "v1234";
		String userId = "vn44178";
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		pricingRuleVO.setVendorId(vendorId);
		List<String> lstCompetitor = new ArrayList<String>();
		// lstCompetitor.add("Walmart");
		pricingRuleVO.setLstCompetitors(lstCompetitor);
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		List<String> values = new ArrayList<String>();
		values.add("Start-1");
		assortmentRulesVO.setValues(values);
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActiveSw("A");
		assortmentRulesVO.setValType("number");
		assortmentRulesVO.setLvlId(5l);
		assortmentRulesVO.setPctSW("zzz");
		assortmentRulesVOs.add(assortmentRulesVO);
		pricingRuleVO.setLstAssortmentRulesVOs(assortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getLvlByVendor(vendorId)).thenReturn(5l);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Mockito.when(this.rulesDAO.updateRuleValuesToDfns(lstRuleDefinationVOs, 5l, userId)).thenReturn("success");
		Mockito.when(this.pricingRulesDAO.removeComparetorInfo(5l)).thenReturn("success");
		String actual = this.pricingRulesServiceImpl.updatePricingRules(userId, pricingRuleVO);
		String expected = "Successfully";
		assertEquals(expected, actual);
	}

	/**
	 * Test update pricing rules6.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingRules6() throws DSVException {
		String vendorId = "v1234";
		String userId = "vn44178";

		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("999");
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptIdOri("AAA");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(new BigDecimal(1000));
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(12);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);

		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		pricingRuleVO.setVendorId(vendorId);
		List<String> lstCompetitor = new ArrayList<String>();
		lstCompetitor.add("Walmart");
		pricingRuleVO.setLstCompetitors(lstCompetitor);
		pricingRuleVO.setCommodityId("");
		pricingRuleVO.setSubCommodityId("12");
		pricingRuleVO.setLvlType("D");

		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		List<String> values = new ArrayList<String>();
		values.add("Start-1");
		assortmentRulesVO.setValues(values);
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActiveSw("A");
		assortmentRulesVO.setValType("number");
		assortmentRulesVO.setLvlId(5l);
		assortmentRulesVO.setPctSW("zzz");
		assortmentRulesVOs.add(assortmentRulesVO);
		pricingRuleVO.setLstAssortmentRulesVOs(assortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getLvlByVendor(vendorId)).thenReturn(0l);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Mockito.when(this.rulesDAO.updateRuleValuesToDfns(lstRuleDefinationVOs, 0l, userId)).thenReturn("success");
		Mockito.when(this.pricingRulesDAO.removeComparetorInfo(0l)).thenReturn("success");
		Mockito.when(this.globalRulesDAO.getMaxLVLID()).thenReturn(5l);

		String actual = this.pricingRulesServiceImpl.updatePricingRules(userId, pricingRuleVO);
		String expected = "Successfully";
		assertEquals(expected, actual);
	}

	/**
	 * Test update pricing rules7.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingRules7() throws DSVException {
		String vendorId = "v1234";
		String userId = "vn44178";

		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("999");
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptIdOri("AAA");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(new BigDecimal(1000));
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(12);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);

		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		pricingRuleVO.setVendorId(vendorId);
		List<String> lstCompetitor = new ArrayList<String>();
		lstCompetitor.add("Walmart");
		pricingRuleVO.setLstCompetitors(lstCompetitor);
		pricingRuleVO.setCommodityId("123");
		pricingRuleVO.setSubCommodityId("");
		pricingRuleVO.setLvlType("D");

		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		List<String> values = new ArrayList<String>();
		values.add("Start-1");
		assortmentRulesVO.setValues(values);
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActiveSw("A");
		assortmentRulesVO.setValType("number");
		assortmentRulesVO.setLvlId(5l);
		assortmentRulesVO.setPctSW("zzz");
		assortmentRulesVOs.add(assortmentRulesVO);
		pricingRuleVO.setLstAssortmentRulesVOs(assortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getLvlByVendor(vendorId)).thenReturn(0l);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Mockito.when(this.rulesDAO.updateRuleValuesToDfns(lstRuleDefinationVOs, 0l, userId)).thenReturn("success");
		Mockito.when(this.pricingRulesDAO.removeComparetorInfo(0l)).thenReturn("success");
		Mockito.when(this.globalRulesDAO.getMaxLVLID()).thenReturn(5l);

		String actual = this.pricingRulesServiceImpl.updatePricingRules(userId, pricingRuleVO);
		String expected = "Successfully";
		assertEquals(expected, actual);
	}

	/**
	 * Test update pricing rules8.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdatePricingRules8() throws DSVException {
		String vendorId = "v1234";
		String userId = "vn44178";

		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId("999");
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptIdOri("AAA");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(new BigDecimal(1000));
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(12);
		subCommodityVOs.add(subCommodityVO);
		commodityVO.setSubCommodityVOs(subCommodityVOs);
		commodityVOs.add(commodityVO);
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);

		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		pricingRuleVO.setVendorId(vendorId);
		List<String> lstCompetitor = new ArrayList<String>();
		pricingRuleVO.setLstCompetitors(lstCompetitor);
		pricingRuleVO.setCommodityId("");
		pricingRuleVO.setSubCommodityId("");
		pricingRuleVO.setLvlType("D");

		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		List<String> values = new ArrayList<String>();
		values.add("Start-1");
		assortmentRulesVO.setValues(values);
		assortmentRulesVO.setId("123");
		assortmentRulesVO.setActiveSw("A");
		assortmentRulesVO.setValType("number");
		assortmentRulesVO.setLvlId(5l);
		assortmentRulesVO.setPctSW("zzz");
		assortmentRulesVOs.add(assortmentRulesVO);
		pricingRuleVO.setLstAssortmentRulesVOs(assortmentRulesVOs);
		Mockito.when(this.pricingRulesDAO.getLvlByVendor(vendorId)).thenReturn(0l);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		Mockito.when(this.rulesDAO.updateRuleValuesToDfns(lstRuleDefinationVOs, 0l, userId)).thenReturn("success");
		Mockito.when(this.pricingRulesDAO.removeComparetorInfo(0l)).thenReturn("success");
		Mockito.when(this.globalRulesDAO.getMaxLVLID()).thenReturn(5l);

		String actual = this.pricingRulesServiceImpl.updatePricingRules(userId, pricingRuleVO);
		String expected = "Successfully";
		assertEquals(expected, actual);
	}

	/**
	 * Test get lvl id1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetLvlId1() throws DSVException {
		long lvlId = this.pricingRulesServiceImpl.getLvlId("", "", "");
		assertEquals(lvlId, 0);
	}

	/**
	 * Test get lvl id2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetLvlId2() throws DSVException {
		String commodity = "123";
		String subCommodity = "456";
		Mockito.when(this.pricingRulesDAO.getLvlByCommoditySubCommodity(commodity, subCommodity)).thenReturn(10l);
		long lvlId = this.pricingRulesServiceImpl.getLvlId("", commodity, subCommodity);
		assertEquals(lvlId, 10l);
	}

	/**
	 * Test get lvl id3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetLvlId3() throws DSVException {
		String commodity = "456";
		Mockito.when(this.pricingRulesDAO.getLvlByCommodity(commodity)).thenReturn(10l);
		long lvlId = this.pricingRulesServiceImpl.getLvlId("", commodity, "");
		assertEquals(lvlId, 10l);
	}

	/**
	 * Test get lvl id4.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetLvlId4() throws DSVException {
		String vendorId = "456";
		Mockito.when(this.pricingRulesDAO.getLvlByVendor(vendorId)).thenReturn(10l);
		long lvlId = this.pricingRulesServiceImpl.getLvlId(vendorId, "", "");
		assertEquals(lvlId, 10l);
	}
}
