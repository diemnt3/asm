/*
 * 
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.assortment.dao.ProductsDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;
import com.heb.enterprise.dsv.vo.RetailCostSearchVO;

/**
 * The Class ReviewNewProductsServiceImplTest.
 * @author quang.phan
 */
@RunWith(MockitoJUnitRunner.class)
public class ReviewNewProductsServiceImplTest {

	@InjectMocks
	private ReviewNewProductsServiceImpl reviewNewProductsServiceImpl = new ReviewNewProductsServiceImpl();

	@Mock
	private ProductsDAO productsDAO;

	@Mock
	private CacheService cacheService;

	/**
	 * Test get list brands.
	 * @author quang.phan
	 */
	@Test
	public void testGetListBrands() {
		Mockito.when(this.cacheService.getBrandsFromCache()).thenReturn(null);
		List<BaseVO> actual = this.reviewNewProductsServiceImpl.getListBrands();
		assertEquals(null, actual);
	}

	/**
	 * Test get list commodities.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetListCommodities() throws DSVException {
		Mockito.when(this.cacheService.getCommodityList()).thenReturn(null);
		List<CommodityVO> actual = this.reviewNewProductsServiceImpl.getListCommodities();
		assertEquals(null, actual);
	}

	/**
	 * Test search for products.
	 * @author quang.phan
	 */
	@Test
	public void testSearchForProducts() throws DSVException {
		ProductSearchCriteriaVO prodModel = new RetailCostSearchVO();
		Mockito.when(this.productsDAO.searchForProducts(prodModel)).thenReturn(null);
		Page<ProductRiewVO> actual = this.reviewNewProductsServiceImpl.searchForProducts(prodModel);
		assertEquals(null, actual);
	}

	/**
	 * Test reject products.
	 * @author quang.phan
	 */
	@Test
	public void testRejectProducts() {
		List<String> lstUpcs = new ArrayList<String>();
		lstUpcs.add("upc1");
		lstUpcs.add("upc2");
		String userId = "vn44178";
		Mockito.when(this.productsDAO.rejectProducts(lstUpcs, userId)).thenReturn("");
		String actual = this.reviewNewProductsServiceImpl.rejectProducts(lstUpcs, userId);
		assertEquals("", actual);
	}

	/**
	 * Test un reject products.
	 * @author quang.phan
	 */
	@Test
	public void testUnRejectProducts() {
		List<String> lstUpcs = new ArrayList<String>();
		lstUpcs.add("upc1");
		lstUpcs.add("upc2");
		String userId = "vn44178";
		Mockito.when(this.productsDAO.unRejectProducts(lstUpcs, userId)).thenReturn("");
		String actual = reviewNewProductsServiceImpl.unRejectProducts(lstUpcs, userId);
		assertEquals("", actual);
	}

	/**
	 * Test approve products.
	 * @author quang.phan
	 */
	@Test
	public void testApproveProducts() {
		List<String> lstUpcs = new ArrayList<String>();
		lstUpcs.add("upc1");
		lstUpcs.add("upc2");
		String userId = "vn44178";
		Mockito.when(this.productsDAO.approveProducts(lstUpcs, userId)).thenReturn("");
		String actual = this.reviewNewProductsServiceImpl.approveProducts(lstUpcs, userId);
		assertEquals("", actual);
	}

	/**
	 * Test check approve reject.
	 * @author quang.phan
	 */
	@Test
	public void testCheckApproveReject() {
		List<String> lstUpcs = new ArrayList<String>();
		lstUpcs.add("upc1");
		lstUpcs.add("upc2");
		Mockito.when(this.productsDAO.checkApproveReject(lstUpcs)).thenReturn(true);
		boolean actual = this.reviewNewProductsServiceImpl.checkApproveReject(lstUpcs);
		assertTrue(actual);
	}

	/**
	 * Test check un reject.
	 * @author quang.phan
	 */
	@Test
	public void testCheckUnReject() {
		List<String> lstUpcs = new ArrayList<String>();
		lstUpcs.add("upc1");
		lstUpcs.add("upc2");
		Mockito.when(this.productsDAO.checkUnReject(lstUpcs)).thenReturn(true);
		boolean actual = this.reviewNewProductsServiceImpl.checkUnReject(lstUpcs);
		assertTrue(actual);
	}

	/**
	 * Test count proudct rev1.
	 * @throws DSVException
	 *             the DSV exception
	 */
	@Test
	public void testCountProudctRev1() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		List<ProductRiewVO> lstProductRiewVOs = new ArrayList<ProductRiewVO>();
		Mockito.when(this.productsDAO.countProudctRev(productSearchCriteriaVO)).thenReturn(lstProductRiewVOs);
		List<ProductRiewVO> actual = this.reviewNewProductsServiceImpl.countProudctRev(productSearchCriteriaVO);
	}

	/**
	 * Test count proudct rev2.
	 * @throws DSVException
	 *             the DSV exception
	 */
	@Test
	public void testCountProudctRev2() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		List<ProductRiewVO> lstProductRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO prodR = new ProductRiewVO();
		prodR.setReviewStatus("ALL");
		prodR.setTotalRow(1);
		lstProductRiewVOs.add(prodR);
		Mockito.when(this.productsDAO.countProudctRev(productSearchCriteriaVO)).thenReturn(lstProductRiewVOs);
		List<ProductRiewVO> actual = this.reviewNewProductsServiceImpl.countProudctRev(productSearchCriteriaVO);
	}

	/**
	 * Test export excel for products.
	 * @throws DSVException
	 *             the DSV exception
	 */
	@Test
	public void testExportExcelForProducts() throws DSVException {
		ProductSearchCriteriaVO productSearchCriteriaVO = new ProductSearchCriteriaVO();
		List<ProductRiewVO> lstProductRiewVOs = new ArrayList<ProductRiewVO>();
		ProductRiewVO prodR = new ProductRiewVO();
		prodR.setReviewStatus("ALL");
		prodR.setTotalRow(1);
		lstProductRiewVOs.add(prodR);
		Mockito.when(this.productsDAO.exportExcelForProducts(productSearchCriteriaVO)).thenReturn(lstProductRiewVOs);
		this.reviewNewProductsServiceImpl.exportExcelForProducts(productSearchCriteriaVO);
	}

	/**
	 * Test get list failed reasons.
	 * @throws DSVException
	 *             the DSV exception
	 */
	@Test
	public void testGetListFailedReasons() throws DSVException {
		List<BaseVO> failedReasons = new ArrayList<BaseVO>();
		Mockito.when(this.productsDAO.getListFailedReasons()).thenReturn(failedReasons);
		this.reviewNewProductsServiceImpl.getListFailedReasons();
	}

}
