package com.heb.enterprise.dsv.assortment.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.ui.ModelMap;

import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.GlobalRulesService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;

/**
 * The Class GlobalRulesControllerTest.
 * @author quang.phan
 */
public class GlobalRulesControllerTest {

	@InjectMocks
	private GlobalRulesController globalRulesController = new GlobalRulesController();

	@Mock
	private GlobalRulesService globalRulesService;

	@Mock
	private CommonService commonService;

	@Mock
	private HttpSession session;

	@Mock
	private ModelMap model;

	@Mock
	private HttpServletRequest request;

	@Before
	public void initialize() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test ajax get hierachy1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetHierachy1() throws DSVException {
		List<DepartmentVO> lstDept = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> lstSubDept = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptDesc("ABC");
		subDepartmentVO.setSubDeptId("07");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO vo = new ClassVO();
		List<CommodityVO> lstCommodity = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(60);
		commodityVO.setCommodityDesc("XYZ");
		lstCommodity.add(commodityVO);
		vo.setCommodityVOs(lstCommodity);
		classVOs.add(vo);
		subDepartmentVO.setClassVOs(classVOs);
		lstSubDept.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(lstSubDept);
		lstDept.add(departmentVO);
		Map<String, LevelVO> globalCommodityMap = new HashMap<String, LevelVO>();
		LevelVO levelVO = new LevelVO();
		levelVO.setSubDeptId("07H");
		levelVO.setSubComdId(60);
		globalCommodityMap.put("07H60", levelVO);
		Mockito.when(this.globalRulesService.getHierachyForGlobalRules()).thenReturn(lstDept);
		Mockito.when(this.globalRulesService.getGlobalCommodityOverride()).thenReturn(globalCommodityMap);
		String actual = this.globalRulesController.ajaxGetHierachy();
		String expected = "[{\"id\":\"07\",\"text\":\"ABC\",\"children\":[{\"id\":60,\"text\":\"60 - XYZ\",\"rulesCustomize\":\"N\"}]}]";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get hierachy2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetHierachy2() throws DSVException {
		List<DepartmentVO> lstDept = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> lstSubDept = new ArrayList<SubDepartmentVO>();
		lstDept.add(departmentVO);
		departmentVO.setSubDepartmentVOs(lstSubDept);
		Map<String, LevelVO> globalCommodityMap = new HashMap<String, LevelVO>();
		Mockito.when(this.globalRulesService.getHierachyForGlobalRules()).thenReturn(lstDept);
		Mockito.when(this.globalRulesService.getGlobalCommodityOverride()).thenReturn(globalCommodityMap);
		String actual = this.globalRulesController.ajaxGetHierachy();
		String expected = "[]";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get hierachy3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetHierachy3() throws DSVException {
		List<DepartmentVO> lstDept = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> lstSubDept = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptDesc("ABC");
		subDepartmentVO.setSubDeptId("07H");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO vo = new ClassVO();
		List<CommodityVO> lstCommodity = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(60);
		commodityVO.setCommodityDesc("XYZ");
		lstCommodity.add(commodityVO);
		vo.setCommodityVOs(lstCommodity);
		classVOs.add(vo);
		subDepartmentVO.setClassVOs(classVOs);
		lstSubDept.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(lstSubDept);
		lstDept.add(departmentVO);
		Map<String, LevelVO> globalCommodityMap = new HashMap<String, LevelVO>();
		LevelVO levelVO = new LevelVO();
		levelVO.setSubDeptId("07H");
		levelVO.setSubComdId(60);
		globalCommodityMap.put("07H60", levelVO);
		Mockito.when(this.globalRulesService.getHierachyForGlobalRules()).thenReturn(lstDept);
		Mockito.when(this.globalRulesService.getGlobalCommodityOverride()).thenReturn(globalCommodityMap);
		String actual = this.globalRulesController.ajaxGetHierachy();
		String expected = "[{\"id\":\"07H\",\"text\":\"ABC\",\"children\":[{\"id\":60,\"text\":\"60 - XYZ\",\"rulesCustomize\":\"Y\",\"a_attr\":{\"class\":\"rule-flag\"}}]}]";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get hierachy4.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetHierachy4() throws DSVException {
		List<DepartmentVO> lstDept = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> lstSubDept = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptDesc("ABC");
		subDepartmentVO.setSubDeptId("07H");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		subDepartmentVO.setClassVOs(classVOs);
		lstSubDept.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(lstSubDept);
		lstDept.add(departmentVO);
		Map<String, LevelVO> globalCommodityMap = new HashMap<String, LevelVO>();
		LevelVO levelVO = new LevelVO();
		levelVO.setSubDeptId("07H");
		levelVO.setSubComdId(60);
		globalCommodityMap.put("07H60", levelVO);
		Mockito.when(this.globalRulesService.getHierachyForGlobalRules()).thenReturn(lstDept);
		Mockito.when(this.globalRulesService.getGlobalCommodityOverride()).thenReturn(globalCommodityMap);
		String actual = this.globalRulesController.ajaxGetHierachy();
		String expected = "[{\"id\":\"07H\",\"text\":\"ABC\",\"children\":[]}]";
		assertEquals(expected, actual);
	}

	/**
	 * Test ajax get hierachy5.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testAjaxGetHierachy5() throws DSVException {
		List<DepartmentVO> lstDept = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> lstSubDept = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptDesc("ABC");
		subDepartmentVO.setSubDeptId("07H");
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO vo = new ClassVO();
		List<CommodityVO> lstCommodity = new ArrayList<CommodityVO>();
		vo.setCommodityVOs(lstCommodity);
		classVOs.add(vo);
		subDepartmentVO.setClassVOs(classVOs);
		lstSubDept.add(subDepartmentVO);
		departmentVO.setSubDepartmentVOs(lstSubDept);
		lstDept.add(departmentVO);
		Map<String, LevelVO> globalCommodityMap = new HashMap<String, LevelVO>();
		LevelVO levelVO = new LevelVO();
		levelVO.setSubDeptId("07H");
		levelVO.setSubComdId(60);
		globalCommodityMap.put("07H60", levelVO);
		Mockito.when(this.globalRulesService.getHierachyForGlobalRules()).thenReturn(lstDept);
		Mockito.when(this.globalRulesService.getGlobalCommodityOverride()).thenReturn(globalCommodityMap);
		String actual = this.globalRulesController.ajaxGetHierachy();
		String expected = "[{\"id\":\"07H\",\"text\":\"ABC\",\"children\":[]}]";
		assertEquals(expected, actual);
	}

	/**
	 * Test get global rule set1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetGlobalRuleSet1() throws DSVException {
		String subDeptID = "12C";
		String commodityID = "8140";
		Map<String, AssortmentRulesVO> lstRules = new HashMap<String, AssortmentRulesVO>();
		Mockito.when(this.globalRulesService.getGlobalRuleSet(subDeptID, commodityID)).thenReturn(lstRules);
		String actual = this.globalRulesController.getGlobalRuleSet(subDeptID, commodityID, this.model, this.session);
		String expect = "{\"data\":[]}";
		assertEquals(actual, expect);
	}

	/**
	 * Test get global rule set2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetGlobalRuleSet2() throws DSVException {
		String subDeptID = "12A";
		String commodityID = "1037";
		Map<String, AssortmentRulesVO> lstRules = new HashMap<String, AssortmentRulesVO>();
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO1 = new AssortmentRulesVO();
		List<String> lstVal1 = new ArrayList<String>();
		assortmentRulesVO1.setId("1");
		assortmentRulesVO1.setName("Map Product Only");
		assortmentRulesVO1.setValue("1");
		lstVal1.add(assortmentRulesVO1.getValue());
		assortmentRulesVO1.setValues(lstVal1);
		assortmentRulesVO1.setSeqNbr(1);
		assortmentRulesVO1.setActiveSw("Y");
		assortmentRulesVO1.setHebComdId(0);
		assortmentRulesVO1.setDeptId("12");
		assortmentRulesVO1.setSubDeptId("A");
		assortmentRulesVO1.setLvlId(Long.parseLong("220"));
		assortmentRulesVOs.add(assortmentRulesVO1);
		AssortmentRulesVO assortmentRulesVO2 = new AssortmentRulesVO();
		List<String> lstVal2 = new ArrayList<String>();
		assortmentRulesVO2.setId("2");
		lstVal2.add(assortmentRulesVO2.getValue());
		assortmentRulesVO1.setValues(lstVal2);
		assortmentRulesVO2.setName("Allow Products with Retail Between");
		assortmentRulesVO2.setValue("15.7");
		assortmentRulesVO2.setSeqNbr(1);
		assortmentRulesVO2.setActiveSw("Y");
		assortmentRulesVO2.setDeptId("12");
		assortmentRulesVO2.setHebComdId(0);
		assortmentRulesVO2.setSubDeptId("A");
		assortmentRulesVO2.setLvlId(Long.parseLong("220"));
		assortmentRulesVOs.add(assortmentRulesVO2);
		lstRules = convertColectionRuleToMap(assortmentRulesVOs);
		Mockito.when(this.globalRulesService.getGlobalRuleSet(subDeptID, commodityID)).thenReturn(lstRules);
		String actual = this.globalRulesController.getGlobalRuleSet(subDeptID, commodityID, this.model, this.session);
		String expected = "{\"data\":[{\"subDeptId\":\"12A\",\"id\":\"2\",\"seqNbr\":null,\"lvlId\":0,\"HebComdId\":0,\"values\":null,\"lstUpdateTs\":null,\"name\":\"Allow Products with Retail Between\",\"lstUpdateUid\":null,\"activeSw\":\"Y\"},{\"subDeptId\":\"12A\",\"id\":\"1\",\"seqNbr\":null,\"lvlId\":0,\"HebComdId\":0,\"values\":[null],\"lstUpdateTs\":null,\"name\":\"Map Product Only\",\"lstUpdateUid\":null,\"activeSw\":\"Y\"}]}";
		assertEquals(actual, expected);
	}

	/**
	 * Test get global rule set3.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testGetGlobalRuleSet3() throws DSVException {
		String subDeptID = "12A";
		String commodityID = "";
		Map<String, AssortmentRulesVO> lstRules = new HashMap<String, AssortmentRulesVO>();
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO1 = new AssortmentRulesVO();
		List<String> lstVal1 = new ArrayList<String>();
		assortmentRulesVO1.setId("1");
		assortmentRulesVO1.setName("Map Product Only");
		assortmentRulesVO1.setValue("1");
		lstVal1.add(assortmentRulesVO1.getValue());
		assortmentRulesVO1.setValues(lstVal1);
		assortmentRulesVO1.setSeqNbr(1);
		assortmentRulesVO1.setActiveSw("Y");
		assortmentRulesVO1.setHebComdId(1);
		assortmentRulesVO1.setDeptId("12");
		assortmentRulesVO1.setSubDeptId("A");
		assortmentRulesVO1.setLvlId(Long.parseLong("220"));
		assortmentRulesVOs.add(assortmentRulesVO1);
		AssortmentRulesVO assortmentRulesVO2 = new AssortmentRulesVO();
		List<String> lstVal2 = new ArrayList<String>();
		assortmentRulesVO2.setId("2");
		lstVal2.add(assortmentRulesVO2.getValue());
		assortmentRulesVO1.setValues(lstVal2);
		assortmentRulesVO2.setName("Allow Products with Retail Between");
		assortmentRulesVO2.setValue("15.7");
		assortmentRulesVO2.setSeqNbr(1);
		assortmentRulesVO2.setActiveSw("Y");
		assortmentRulesVO2.setDeptId("12");
		assortmentRulesVO2.setHebComdId(1);
		assortmentRulesVO2.setSubDeptId("A");
		assortmentRulesVO2.setLvlId(Long.parseLong("220"));
		assortmentRulesVOs.add(assortmentRulesVO2);
		lstRules = convertColectionRuleToMap(assortmentRulesVOs);
		Mockito.when(this.globalRulesService.getGlobalRuleSet(subDeptID, commodityID)).thenReturn(lstRules);
		String actual = this.globalRulesController.getGlobalRuleSet(subDeptID, commodityID, this.model, this.session);
		String expected = "{\"data\":[{\"subDeptId\":\"12A\",\"id\":\"2\",\"seqNbr\":null,\"lvlId\":220,\"HebComdId\":1,\"values\":null,\"lstUpdateTs\":null,\"name\":\"Allow Products with Retail Between\",\"lstUpdateUid\":null,\"activeSw\":\"Y\"},{\"subDeptId\":\"12A\",\"id\":\"1\",\"seqNbr\":null,\"lvlId\":220,\"HebComdId\":1,\"values\":[null],\"lstUpdateTs\":null,\"name\":\"Map Product Only\",\"lstUpdateUid\":null,\"activeSw\":\"Y\"}]}";
		assertEquals(actual, expected);
	}

	/**
	 * Test update rule settings1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdateRuleSettings1() throws DSVException {
		List<AssortmentRulesVO> lstRules = new ArrayList<AssortmentRulesVO>();
		String subDept = "ABC";
		String commodity = "1027";
		String userId = "";
		Mockito.when(this.commonService.getUserLogin()).thenReturn(null);
		Mockito.when(this.session.getAttribute("RULE_ORI")).thenReturn(null);
		Mockito.when(this.globalRulesService.updateGlobalRulesSetting(userId, subDept, commodity, lstRules)).thenReturn(false);
		String actual = this.globalRulesController.updateRuleSettings(lstRules, request, session);
		String expected = "{\"rs\":false}";
		assertEquals(actual, expected);
	}

	/**
	 * Test update rule settings2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdateRuleSettings2() throws DSVException {
		List<AssortmentRulesVO> lstRules = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		List<String> lstVal1 = new ArrayList<String>();
		assortmentRulesVO.setId("1");
		assortmentRulesVO.setName("Map Product Only");
		assortmentRulesVO.setValue("1");
		lstVal1.add(assortmentRulesVO.getValue());
		assortmentRulesVO.setValues(lstVal1);
		assortmentRulesVO.setSeqNbr(1);
		assortmentRulesVO.setActiveSw("Y");
		assortmentRulesVO.setHebComdId(0);
		assortmentRulesVO.setDeptId("12");
		assortmentRulesVO.setSubDeptId("A");
		assortmentRulesVO.setLvlId(Long.parseLong("220"));
		lstRules.add(assortmentRulesVO);
		List<AssortmentRulesVO> lstAssortmentRulesVOOris = new ArrayList<AssortmentRulesVO>();
		String subDeptID = "ABC";
		String commodityID = "1027";
		Mockito.when(this.request.getParameter("subDeptId")).thenReturn(subDeptID);
		Mockito.when(this.request.getParameter("commodityID")).thenReturn(commodityID);
		HebUserDetails user = new HebUserDetails("vn44178", "user1234", true, true, true, true, new ArrayList<GrantedAuthority>());
		Mockito.when(this.commonService.getUserLogin()).thenReturn(user);
		Mockito.when(session.getAttribute("RULE_ORI")).thenReturn(lstAssortmentRulesVOOris);
		Map<String, AssortmentRulesVO> lstRuless = new HashMap<String, AssortmentRulesVO>();
		lstRuless.put("1", assortmentRulesVO);
		Mockito.when(this.globalRulesService.getGlobalRuleSet(subDeptID, commodityID)).thenReturn(lstRuless);
		Mockito.when(this.globalRulesService.updateGlobalRulesSetting("vn44178", subDeptID, commodityID, lstRules)).thenReturn(true);
		String actual = this.globalRulesController.updateRuleSettings(lstRules, this.request, this.session);
		String expected = "{\"data\":[{\"lstUpdateTs\":null,\"lstUpdateUid\":null}],\"rs\":true}";
		assertEquals(actual, expected);
	}

	/**
	 * Test reset rules to higher level1.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testResetRulesToHigherLevel1() throws DSVException {
		String commodityId = "123";
		String subDept = "07B";
		Mockito.when(this.globalRulesService.resetRulesToHigherLevel(subDept, commodityId)).thenReturn(true);
		String actual = this.globalRulesController.resetRulesToHigherLevel(subDept, commodityId);
		assertEquals("true", actual);
	}

	/**
	 * Test reset rules to higher level2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testResetRulesToHigherLevel2() throws DSVException {
		String commodityId = "123";
		String subDept = "07B";
		Mockito.when(this.globalRulesService.resetRulesToHigherLevel(subDept, commodityId)).thenReturn(false);
		String actual = this.globalRulesController.resetRulesToHigherLevel(subDept, commodityId);
		assertEquals("false", actual);
	}

	/**
	 * Test show global rules screen.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testShowGlobalRulesScreen1() throws DSVException {
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		Mockito.when(this.globalRulesService.getRulesByRuleGroup()).thenReturn(assortmentRulesVOs);
		this.globalRulesController.showGlobalRulesScreen(model);
	}

	/**
	 * Test show global rules screen2.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testShowGlobalRulesScreen2() throws DSVException {
		List<AssortmentRulesVO> assortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVOs.add(assortmentRulesVO);
		Mockito.when(this.globalRulesService.getRulesByRuleGroup()).thenReturn(assortmentRulesVOs);
		this.globalRulesController.showGlobalRulesScreen(model);
	}

	/**
	 * Convert colection rule to map.
	 * @param lstRuleVOs
	 *            the lst rule v os
	 * @return the map
	 * @author quang.phan
	 */
	public Map<String, AssortmentRulesVO> convertColectionRuleToMap(List<AssortmentRulesVO> lstRuleVOs) {
		Map<String, AssortmentRulesVO> ruleAsHashMap = new HashMap<String, AssortmentRulesVO>();
		List<String> lstRuleNames = new ArrayList<String>();
		AssortmentRulesVO ruleVO;
		String key = Constants.EMPTY_STRING;
		List<Integer> lstSq = null;
		for (AssortmentRulesVO ruleVOTmp : lstRuleVOs) {
			key = ruleVOTmp.getId();
			if (!lstRuleNames.contains(key)) {
				lstSq = new ArrayList<Integer>();
				lstSq.add(ruleVOTmp.getSeqNbr());
				ruleVO = ruleVOTmp;
				lstRuleNames.add(key);
				ruleAsHashMap.put(ruleVOTmp.getId(), ruleVO);
			} else {
				if (!lstSq.contains(ruleVOTmp.getSeqNbr())) {
					lstSq.add(ruleVOTmp.getSeqNbr());
					ruleAsHashMap.get(ruleVOTmp.getId()).getValues().add(ruleVOTmp.getValue());
					ruleAsHashMap.get(ruleVOTmp.getId()).setLstSeqNbrs(lstSq);
				}
			}
		}
		return ruleAsHashMap;
	}
}
