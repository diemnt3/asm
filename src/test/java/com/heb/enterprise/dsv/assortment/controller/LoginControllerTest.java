package com.heb.enterprise.dsv.assortment.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.ui.ModelMap;

/**
 * The Class LoginControllerTest.
 * @author mrh.diemnguyen
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {

	@InjectMocks
	private LoginController loginController = new LoginController();

	@Mock
	private StandardEnvironment environment;

	private static final String LOGIN_TILE = "login.tile";
	private static final String ACCESS_DENIED = "403.tile";

	@Test
	public void testShowLogin() {
		String[] profs = new String[10];
		profs[0] = "ABC";

		Mockito.when(this.environment.getActiveProfiles()).thenReturn(profs);
		String actual = this.loginController.showLogin(true, false, new ModelMap());
		Assert.assertEquals(LOGIN_TILE, actual);

	}

	@Test
	public void testShowAccessDeniedScreen() {
		String actual = this.loginController.showAccessDeniedScreen();
		Assert.assertEquals(ACCESS_DENIED, actual);
	}

}
