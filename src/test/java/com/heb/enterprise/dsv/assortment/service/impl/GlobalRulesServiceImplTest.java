package com.heb.enterprise.dsv.assortment.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.heb.enterprise.dsv.assortment.dao.CommonDAO;
import com.heb.enterprise.dsv.assortment.dao.GlobalRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.RulesDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;

/**
 * The class <code>GlobalRulesServiceImplTest</code> contains tests for the class <code>{@link GlobalRulesServiceImpl}</code>.
 * @author thangdang
 * @version $Revision: 1.11 $
 */
@RunWith(MockitoJUnitRunner.class)
public class GlobalRulesServiceImplTest {
	@InjectMocks
	private GlobalRulesServiceImpl globalRulesServiceImpl = new GlobalRulesServiceImpl();
	@Mock
	private GlobalRulesDAO globalRulesDAO;
	/**
	 * Inject CacheService.
	 */
	@Mock
	private CacheService cacheService;
	/**
	 * Inject CommonService.
	 */
	@Mock
	private CommonDAO commonDao;
	@Mock
	private RulesDAO rulesDAO;

	/**
	 * Run the GlobalRulesServiceImpl() constructor test.
	 * @author thangdang
	 */
	@Test
	public void testGlobalRulesServiceImpl_1() throws DSVException {
		assertNotNull(globalRulesServiceImpl);
	}

	/**
	 * Run the Map<String, LevelVO> getGlobalCommodityOverride() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetGlobalCommodityOverride_1() throws DSVException {
		Map<String, LevelVO> lstReturn = new HashMap<String, LevelVO>();
		Mockito.when(this.globalRulesDAO.getGlobalCommodityOverride()).thenReturn(lstReturn);
		Map<String, LevelVO> resultActual = this.globalRulesServiceImpl.getGlobalCommodityOverride();
		Assert.assertEquals("Error result not same", lstReturn, resultActual);
	}

	/**
	 * Run the Map<String, AssortmentRulesVO> getGlobalRuleSet(String,String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetGlobalRuleSet_1() throws DSVException {
		String subDeptID = "07A";
		String commodityID = "3343";
		Map<String, AssortmentRulesVO> lstReturn = null;
		Mockito.when(this.globalRulesDAO.getGlobalRuleSet(subDeptID, commodityID)).thenReturn(lstReturn);
		Map<String, AssortmentRulesVO> resultActual = this.globalRulesServiceImpl.getGlobalRuleSet(subDeptID, commodityID);
		Assert.assertEquals("Error result not same", lstReturn, resultActual);
	}

	/**
	 * Run the Map<String, AssortmentRulesVO> getGlobalRuleSet(String,String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetGlobalRuleSet_2() throws DSVException {
		String subDeptID = "07A";
		String commodityID = "3";
		Map<String, AssortmentRulesVO> lstReturn = new HashMap<String, AssortmentRulesVO>();
		lstReturn.put(AntiMagicNumber.STRING_ONE, new AssortmentRulesVO());
		Mockito.when(this.globalRulesDAO.getGlobalRuleSet(subDeptID, commodityID)).thenReturn(lstReturn);
		Map<String, AssortmentRulesVO> resultActual = this.globalRulesServiceImpl.getGlobalRuleSet(subDeptID, commodityID);
		Assert.assertEquals("Error result not same", AntiMagicNumber.ONE, resultActual.size());
	}

	/**
	 * Run the List<DepartmentVO> getHierachyForGlobalRules() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetHierachyForGlobalRules_1() throws DSVException {
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(null);
		List<DepartmentVO> result = this.globalRulesServiceImpl.getHierachyForGlobalRules();
		Assert.assertEquals("Error result not same", null, result);
	}

	/**
	 * Run the List<DepartmentVO> getHierachyForGlobalRules() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetHierachyForGlobalRules_2() throws DSVException {
		List<DepartmentVO> lstReturn = new ArrayList<DepartmentVO>();
		lstReturn.add(new DepartmentVO());
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstReturn);
		List<DepartmentVO> result = this.globalRulesServiceImpl.getHierachyForGlobalRules();
		Assert.assertEquals("Error result not same", lstReturn, result);
	}

	/**
	 * Run the BaseVO getLogicRules() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetLogicRules_1() throws DSVException {
		Mockito.when(this.commonDao.findAllRuleLogicOprtr()).thenReturn(null);
		BaseVO result = this.globalRulesServiceImpl.getLogicRules();
		System.out.println(result);
		Assert.assertNotNull(result);
	}

	/**
	 * Run the BaseVO getLogicRules() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetLogicRules_2() throws DSVException {
		List<BaseVO> lstResult = new ArrayList<BaseVO>();
		lstResult.add(new BaseVO());
		Mockito.when(this.commonDao.findAllRuleLogicOprtr()).thenReturn(lstResult);
		BaseVO result = this.globalRulesServiceImpl.getLogicRules();
		Assert.assertEquals("Error result not same", lstResult.get(0).getId(), result.getId());
	}

	/**
	 * Run the BaseVO getLogicRules() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetLogicRules_3() throws DSVException {
		List<BaseVO> lstResult = new ArrayList<BaseVO>();
		BaseVO baseVO = new BaseVO();
		baseVO.setName("AND");
		lstResult.add(baseVO);
		Mockito.when(this.commonDao.findAllRuleLogicOprtr()).thenReturn(lstResult);
		BaseVO result = this.globalRulesServiceImpl.getLogicRules();
		Assert.assertEquals("Error result not same", lstResult.get(0).getId(), result.getId());
	}

	/**
	 * Run the BaseVO getLogicRules() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetLogicRules_4() throws DSVException {
		DSVException dsvException = new DSVException("error");
		Mockito.when(this.commonDao.findAllRuleLogicOprtr()).thenThrow(dsvException);
		BaseVO result = null;
		try {
			result = this.globalRulesServiceImpl.getLogicRules();
		} catch (DSVException e) {

		}
		Assert.assertEquals("Error result not same", null, result);
	}

	/**
	 * Run the List<AssortmentRulesVO> getRulesByRuleGroup() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetRulesByRuleGroup_1() throws DSVException {
		Mockito.when(this.commonDao.getRulesByRuleGroup()).thenReturn(null);
		List<AssortmentRulesVO> result = this.globalRulesServiceImpl.getRulesByRuleGroup();
		Assert.assertEquals("Error result not same", null, result);
	}

	/**
	 * Run the List<AssortmentRulesVO> getRulesByRuleGroup() method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testGetRulesByRuleGroup_2() throws DSVException {
		List<AssortmentRulesVO> lstReturn = new ArrayList<AssortmentRulesVO>();
		Mockito.when(this.commonDao.getRulesByRuleGroup()).thenReturn(lstReturn);
		List<AssortmentRulesVO> result = this.globalRulesServiceImpl.getRulesByRuleGroup();
		Assert.assertEquals("Error result not same", lstReturn, result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_1() throws DSVException {
		String commodityId = "3";
		String deptId = "07";
		String subDeptId = "A";
		String subDept = "07A";
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.globalRulesDAO.getLvlIdByCommodity(commodityId, deptId, subDeptId)).thenReturn(new Long(0));
		boolean result = this.globalRulesServiceImpl.resetRulesToHigherLevel(subDept, commodityId);
		assertTrue(result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_2() throws DSVException {
		String commodityId = "1";
		String deptId = "07";
		String subDeptId = "B";
		long lvlId = 1;
		String subDept = "07B";
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.globalRulesDAO.getLvlIdByCommodity(commodityId, deptId, subDeptId)).thenReturn(lvlId);
		Mockito.when(this.globalRulesDAO.removeRule(lvlId)).thenReturn(true);
		Mockito.when(this.globalRulesDAO.removeLvl(lvlId)).thenReturn(true);
		boolean result = this.globalRulesServiceImpl.resetRulesToHigherLevel(subDept, commodityId);
		assertTrue(result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_3() throws DSVException {
		String commodityId = "1";
		String subDept = "07B";
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenThrow(new DSVException("error"));
		boolean result = false;
		try {
			result = this.globalRulesServiceImpl.resetRulesToHigherLevel(subDept, commodityId);
		} catch (DSVException e) {

		}
		assertTrue(!result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_4() throws DSVException {
		String commodityId = "1";
		String deptId = "07";
		String subDeptId = "B";
		String subDept = "07B";
		long lvlId = 1;
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.globalRulesDAO.getLvlIdByCommodity(commodityId, deptId, subDeptId)).thenReturn(lvlId);
		Mockito.when(this.globalRulesDAO.removeRule(lvlId)).thenReturn(false);
		Mockito.when(this.globalRulesDAO.removeLvl(lvlId)).thenReturn(true);
		boolean result = this.globalRulesServiceImpl.resetRulesToHigherLevel(subDept, commodityId);
		assertTrue(result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_5() throws DSVException {
		String commodityId = "1";
		String deptId = "07";
		String subDeptId = "B";
		String subDept = "07B";
		long lvlId = 1;
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.globalRulesDAO.getLvlIdByCommodity(commodityId, deptId, subDeptId)).thenReturn(lvlId);
		Mockito.when(this.globalRulesDAO.removeRule(lvlId)).thenReturn(false);
		Mockito.when(this.globalRulesDAO.removeLvl(lvlId)).thenReturn(false);
		boolean result = this.globalRulesServiceImpl.resetRulesToHigherLevel(subDept, commodityId);
		assertTrue(result);
	}

	/**
	 * Run the boolean resetRulesToHigherLevel(String) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testResetRulesToHigherLevel_6() throws DSVException {
		String commodityId = "1";
		String deptId = "07";
		String subDeptId = "B";
		String subDept = "07B";
		long lvlId = 1;
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.globalRulesDAO.getLvlIdByCommodity(commodityId, deptId, subDeptId)).thenReturn(lvlId);
		Mockito.when(this.globalRulesDAO.removeRule(lvlId)).thenReturn(true);
		Mockito.when(this.globalRulesDAO.removeLvl(lvlId)).thenReturn(false);
		boolean result = this.globalRulesServiceImpl.resetRulesToHigherLevel(subDept, commodityId);
		assertTrue(result);
	}

	/**
	 * Test reset rules to higher level_7.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testResetRulesToHigherLevel_7() throws DSVException {
		String commodityId = "123";
		String deptId = "442";
		String subDeptId = "B";
		String subDept = "343B";
		long lvlId = 1;
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		DepartmentVO departmentVO = new DepartmentVO();
		List<SubDepartmentVO> subDepartmentVOs = new ArrayList<SubDepartmentVO>();
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		ClassVO classVO = new ClassVO();
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(123);
		commodityVOs.add(commodityVO);
		classVO.setClassCode(new BigDecimal("1000"));
		classVO.setCommodityVOs(commodityVOs);
		classVOs.add(classVO);
		subDepartmentVO.setSubDeptIdOri("B");
		subDepartmentVO.setClassVOs(classVOs);
		subDepartmentVOs.add(subDepartmentVO);
		departmentVO.setDeptId("442");
		departmentVO.setSubDepartmentVOs(subDepartmentVOs);
		lstDepartmentVOs.add(departmentVO);

		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.globalRulesDAO.getLvlIdByCommodity(commodityId, deptId, subDeptId)).thenReturn(lvlId);
		Mockito.when(this.globalRulesDAO.removeRule(lvlId)).thenReturn(true);
		Mockito.when(this.globalRulesDAO.removeLvl(lvlId)).thenReturn(false);
		boolean result = this.globalRulesServiceImpl.resetRulesToHigherLevel(subDept, commodityId);
		assertTrue(result);
	}

	/**
	 * Test reset rules to higher level_8.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testResetRulesToHigherLevel_8() throws DSVException {
		String commodityId = "";
		String deptId = "07";
		String subDeptId = "B";
		String subDept = "07B";
		long lvlId = 1;
		List<DepartmentVO> lstDepartmentVOs = new ArrayList<DepartmentVO>();
		Mockito.when(this.cacheService.getHierachyForGlobalRulesFromCache()).thenReturn(lstDepartmentVOs);
		Mockito.when(this.globalRulesDAO.getLvlIdByCommodity(commodityId, deptId, subDeptId)).thenReturn(lvlId);
		Mockito.when(this.globalRulesDAO.removeRule(lvlId)).thenReturn(true);
		Mockito.when(this.globalRulesDAO.removeLvl(lvlId)).thenReturn(false);
		boolean result = this.globalRulesServiceImpl.resetRulesToHigherLevel(subDept, commodityId);
		assertTrue(result);
	}

	/**
	 * Run the boolean updateGlobalRulesSetting(String,String,String,List<AssortmentRulesVO>) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateGlobalRulesSetting_1() throws DSVException {
		String userId = "vn44178";
		String subDept = "07A";
		String commodityId = "2";
		long lvlID = 1;
		String vendorId = "1353";
		List<AssortmentRulesVO> lstRules = new ArrayList<AssortmentRulesVO>();
		List<RuleDefinationVO> lstAddRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setSeqNbr(1);
		assortmentRulesVO.setLvlId(lvlID);
		assortmentRulesVO.setHebComdId(Long.valueOf(commodityId));
		assortmentRulesVO.setActionCd(Constants.STRING_A);
		lstRules.add(assortmentRulesVO);
		Mockito.when(this.globalRulesDAO.getMaxLVLID()).thenReturn(lvlID);
		lvlID = lvlID + 1;
		Mockito.when(this.globalRulesDAO.insertlvl(lvlID, userId, commodityId, "", "", 0, vendorId, "", null)).thenReturn(new Long(0));
		Mockito.when(this.globalRulesDAO.insertRulesToDfns(lstAddRuleDefinationVOs, lvlID, userId)).thenReturn("");
		boolean result = this.globalRulesServiceImpl.updateGlobalRulesSetting(userId, subDept, commodityId, lstRules);

		assertTrue(result);
	}

	/**
	 * Run the boolean updateGlobalRulesSetting(String,String,String,List<AssortmentRulesVO>) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateGlobalRulesSetting_2() throws DSVException {
		String userId = "vn44178";
		String subDept = "07A";
		String commodityId = "2";
		long lvlID = 1;
		String vendorId = "1353";
		List<AssortmentRulesVO> lstRules = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setSeqNbr(1);
		assortmentRulesVO.setLvlId(lvlID);
		assortmentRulesVO.setHebComdId(Long.valueOf(commodityId));
		assortmentRulesVO.setActionCd(Constants.STRING_A);
		lstRules.add(assortmentRulesVO);
		Mockito.when(this.globalRulesDAO.getMaxLVLID()).thenReturn(lvlID);
		lvlID = lvlID + 1;
		Mockito.when(this.globalRulesDAO.insertlvl(lvlID, userId, commodityId, "", "", 0, vendorId, "", null)).thenThrow(new DSVException("error"));
		boolean result = this.globalRulesServiceImpl.updateGlobalRulesSetting(userId, subDept, commodityId, lstRules);
		assertTrue(result);
	}

	/**
	 * Run the boolean updateGlobalRulesSetting(String,String,String,List<AssortmentRulesVO>) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateGlobalRulesSetting_3() throws DSVException {
		String userId = "vn44178";
		String subDept = "07A";
		String commodityId = "2";
		long lvlID = 1;
		List<AssortmentRulesVO> lstRules = new ArrayList<AssortmentRulesVO>();
		List<RuleDefinationVO> lstEditRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		RuleDefinationVO ruleDefinationVO = new RuleDefinationVO();
		ruleDefinationVO.setLvlId(lvlID);
		lstEditRuleDefinationVOs.add(ruleDefinationVO);
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setSeqNbr(1);
		assortmentRulesVO.setLvlId(lvlID);
		assortmentRulesVO.setHebComdId(Long.valueOf(commodityId));
		assortmentRulesVO.setActionCd(Constants.STRING_U);
		lstRules.add(assortmentRulesVO);
		Mockito.when(this.globalRulesDAO.updateRuleValuesToDfns(lstEditRuleDefinationVOs, userId)).thenReturn("");
		boolean result = this.globalRulesServiceImpl.updateGlobalRulesSetting(userId, subDept, commodityId, lstRules);
		assertTrue(result);
	}

	/**
	 * Run the boolean updateGlobalRulesSetting(String,String,String,List<AssortmentRulesVO>) method test.
	 * @throws DSVException
	 * @author thangdang
	 */
	@Test
	public void testUpdateGlobalRulesSetting_4() throws DSVException {
		String userId = "vn44178";
		String subDept = "07A";
		String commodityId = "2";
		long lvlID = 1;
		List<AssortmentRulesVO> lstRules = new ArrayList<AssortmentRulesVO>();
		List<RuleDefinationVO> lstEditRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		RuleDefinationVO ruleDefinationVO = new RuleDefinationVO();
		ruleDefinationVO.setLvlId(lvlID);
		lstEditRuleDefinationVOs.add(ruleDefinationVO);
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setSeqNbr(1);
		assortmentRulesVO.setLvlId(lvlID);
		assortmentRulesVO.setHebComdId(Long.valueOf(commodityId));
		assortmentRulesVO.setActionCd("Y");
		lstRules.add(assortmentRulesVO);
		Mockito.when(this.globalRulesDAO.updateRuleValuesToDfns(lstEditRuleDefinationVOs, userId)).thenReturn("");
		boolean result = this.globalRulesServiceImpl.updateGlobalRulesSetting(userId, subDept, commodityId, lstRules);
		assertTrue(result);
	}

	/**
	 * Test update global rules setting_5.
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	@Test
	public void testUpdateGlobalRulesSetting_5() throws DSVException {
		String userId = "vn44178";
		String subDept = "07A";
		String commodityId = "2";
		long lvlID = 1;
		List<AssortmentRulesVO> lstRules = new ArrayList<AssortmentRulesVO>();
		List<RuleDefinationVO> lstEditRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		RuleDefinationVO ruleDefinationVO = new RuleDefinationVO();
		ruleDefinationVO.setLvlId(lvlID);
		lstEditRuleDefinationVOs.add(ruleDefinationVO);
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setSeqNbr(1);
		assortmentRulesVO.setLvlId(lvlID);
		assortmentRulesVO.setHebComdId(Long.valueOf(commodityId));
		assortmentRulesVO.setActionCd(Constants.STRING_A);
		lstRules.add(assortmentRulesVO);
		Mockito.when(cacheService.getHierachyForGlobalRulesFromCache()).thenThrow(new DSVException("Error"));
		boolean result = this.globalRulesServiceImpl.updateGlobalRulesSetting(userId, subDept, commodityId, lstRules);
		assertTrue(!result);
	}

	/**
	 * Perform pre-test initialization.
	 * @throws DSVException
	 *             if the initialization fails for some reason
	 * @author thangdang
	 */
	@Before
	public void setUp() throws DSVException {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 * @throws DSVException
	 *             if the clean-up fails for some reason
	 * @author thangdang
	 */
	@After
	public void tearDown() throws DSVException {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 * @param args
	 *            the command line arguments
	 * @author thangdang
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(GlobalRulesServiceImplTest.class);
	}
}