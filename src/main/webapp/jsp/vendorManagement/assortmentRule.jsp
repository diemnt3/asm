<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<p id="pMsg" style="margin-bottom: 30px;">
	<span class="col-md-12">
		Please activate Commodities/Sub-commodities and setup Assortment Rules for <span class="title-style vendorTxt"><b>Unbeatable Sales</b></span>	
	</span>
</p>

	<div class="col-md-3">
		<div class="panel panel-default overflow-panel-body-commodity">
		  <div id="vendorRules" class="vendorTxt panel-heading panel-heading-style"></div>
		  <div class="panel-body pd0 panel-body-commodity">
		  	<div id="jstreeCommo">
				<!-- Load tree here -->
			</div>
		  </div>
		  <div  class="panel-heading panel-heading-style panel-bottom"><b></b></div>
		</div>
	</div>
	<div class="col-md-9 wrap-content-assment" id="contentColRight">
		<h4 class="page-heading">Please select the default rules for <span class="vendorTxt1"></span></h4>

		<form class="form-horizontal" role="form" id="formHie">
		<div id="vendorAttr">
			<h5 id="vendorAttrText">Vendor Attributes</h5>					
				<div class="form-group">
					<div class="col-sm-6 control-label text-left option-left">
						Please select if you want Vendor is fast tracked </div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<input id="attrCheckbox-9" type="checkbox" class="my-checkbox" name="attr-fastTrack" data-on-color="info"
								data-off-color="danger" data-on-text="YES" data-off-text="NO">
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						Vendor Lead time for cost changes </div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<input id="attr-10" type="text" name="attr-costChange" class="form-control input-sm" tabindex ="1">
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						Vendor Fulfilment Lead time  (in days) </div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<input id="attr-11" type="text" name="attr-leadTime" class="form-control input-sm" tabindex ="2">
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						Vendor Daily Order Processing Cut-off Time (HH:MM)  </div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<input id="attr-12" type="text" name="attr-processTime" class="form-control input-sm" tabindex ="3">
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						Vendor Buffer Time (in days) </div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<input  id="attr-13" type="text" name="attr-bufferTime" class="form-control input-sm" tabindex ="4">
						</div>
					</div>
				</div>
			</div>			
			<div id="vendorRule">
				<h5 id="vendorBufferDiv"></h5>
				<h4 class="page-heading" style="display:none" id="vendorBufferDivHeading">Please select the Assortment Rules for <span class="vendorTxt1"></span></h4>
				<h5 id="vendorAsmText">Vendor Assortment Rules</h5>
				
			  	<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						<input type="checkbox" id="turnOnAllRule"/> 
						<span class="turnAllRule">Apply for All Rules</span>
					</div>
					<div class="col-sm-6">
						<div class="pull-right select-option">
							
						</div>
					</div>
				</div>
			  	<hr class="margin-top0 margin-bottom5"/>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						<input type="checkbox" name="chk-1" data-left-check="0" class="checkboxRule">
						Only allow MAP products
					</div>
					<div class="col-sm-6">
						<div class="pull-right select-option">
							<!--<input type="checkbox" class="my-checkbox" name="my-checkbox"
								data-right-check="0" /> -->
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-7 control-label text-left option-left">
						<input type="checkbox" name="chk-2" class="checkboxRule" data-left-check="1"> Only allow
						products where retail is between
					</div>
					<div class="col-sm-5">
						<div class="pull-right">
							<div class="row">
								<!--<div class="col-xs-1 pull-right text-per">
									<img src="<s:url value="/image/icon-per.png"></s:url>"> 
									<h4><b></b></h4>
								</div>-->
								<div class="col-xs-5 pull-right from-percent" style="padding-left:0">
									<!-- <input id="retailEnd" type="text" class="input-between form-control input-sm text-align input-decimal" placeholder="" data-right-check="1" value="0" tabindex ="6"> -->
									<div class="input-group">
										<div class="input-group-addon">
											<i>$</i>
										</div>
										<input id="retailEnd" type="text" class="input-between form-control input-sm text-align input-decimal" placeholder="" data-right-check="1" value="0" tabindex ="6">
									</div>
								</div>
								<div class="col-xs-2 pull-right text-center text-and">
									<span>and</span>
								</div>
								<div class="col-xs-5 pull-right" style="padding-right:0">									
									<div class="input-group">
										<div class="input-group-addon">
											<i>$</i>
										</div>
										<input id="retailStart" type="text" class="input-between form-control to-percent input-sm text-align input-decimal"
											placeholder="" data-right-check="1" value="0" tabindex ="5">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>			
				
				<div class="form-group checkbox">					
					<div class="col-sm-7 control-label text-left option-left">
						<input type="checkbox" name="chk-3" class="checkboxRule" data-left-check="2"> Only allow
						products where margin is between
					</div>
					<div class="col-sm-5">
						<div class="pull-right">
							<div class="row">
								<!-- <div class="col-xs-1 pull-right text-per">									
									<h4><b>%</b></h4>
								</div>  -->
								<div class="col-xs-5 pull-right from-percent" style="padding-left:0">									
									<div class="input-group">
										<input id="maginEnd" type="text" class="input-between form-control input-sm text-align" placeholder="" data-right-check="2" value="0" tabindex ="8">
										<div class="input-group-addon">
											<i>%</i>
										</div>
									</div>
								</div>
								<div class="col-xs-2 pull-right text-center text-and">
									<span>And</span>
								</div>
								<div class="col-xs-5 pull-right" style="padding-right:0">									
									<div class="input-group">
										<input id="maginStart" type="text" class="input-between form-control input-sm text-align" placeholder="" data-right-check="2" value="0" tabindex ="7">
										<div class="input-group-addon">
											<i>%</i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	

				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						<input type="checkbox" name="chk-4" class="prd-with-img checkboxRule"
						data-left-check="3"> Only allow products with Image
					</div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<!--<input type="checkbox" class="my-checkbox" name="my-checkbox" data-right-check="3"
								/> -->
						</div>
					</div>
				</div>

				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						<input type="checkbox" name="chk-5" data-left-check="4" class="checkboxRule">
						Only allow products with Description

					</div>
					<div class="col-sm-6">
						<div class="pull-right select-option">
							<!--<input type="checkbox" class="my-checkbox" name="my-checkbox"
								data-right-check="4" /> -->
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						<input type="checkbox" name="chk-6" data-left-check="5" class="checkboxRule">
						Only allow products with Romance Copy

					</div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<!--<input type="checkbox" class="my-checkbox" name="my-checkbox" data-right-check="5"> -->
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						<input type="checkbox" name="chk-7" data-left-check="6" class="checkboxRule">
						Only allow products with Brand

					</div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<!--<input type="checkbox" class="my-checkbox" name="my-checkbox" data-right-check="6"> -->
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						<input type="checkbox" name="chk-8" data-on-color="info"
						data-off-color="danger" data-left-check="7" class="checkboxRule"> Only allow
						products with Size

					</div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<!--<input type="checkbox" class="my-checkbox" name="my-checkbox" data-right-check="7"/> -->
						</div>
					</div>
				</div>
				<div class="form-group checkbox">
					<div class="col-sm-6 control-label text-left option-left">
						<input type="checkbox" name="chk-9" data-left-check="8" class="checkboxRule">
						Only allow products with Cost

					</div>
					<div class="col-sm-6 select-option">
						<div class="pull-right">
							<!--<input type="checkbox" class="my-checkbox" name="my-checkbox" data-on-color="info"
								data-off-color="danger" data-right-check="8"> -->
						</div>
					</div>
				</div>				
				<!--<div class="form-group">
					<div class="col-sm-10 control-label text-left option-left">
						<input type="checkbox" id="chk-10" name="chk-10" data-left-check="10" class="checkboxRule">
						Allow Products Ranked in top X% of the Vendor Sales (if Vendor sales data is received)
					</div>
					<div class="col-sm-2 select-option">										
						<div class="pull-right">
							<div class="row">
								<div class="col-xs-2 pull-right text-per">
									<h4><b>%</b></h4>
								</div>
								<div class="col-xs-10 pull-right">
									<input id="rule10" type="text" class="form-control input-sm text-align" data-right-check="10" placeholder="" tabindex ="9"/>
								</div>												
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</form>
		<div class="form-group style-save-global">									
			<div class="col-sm-9">
			</div>
			<div class="col-sm-3 text-right padding-left0 padding-right0">
			<button id="reset-toDefault-btn" type="button" class="btn btn-default margin-right10" title="Reset Rules to Higher Level" tabindex ="10">
				<span class="glyphicon glyphicon-refresh" ></span>&nbsp;Reset
			</button>
			<button id="assortment-save-btn" type="button" class="btn btn-danger pull-right" title="Save" tabindex ="11"><span class="glyphicon glyphicon-floppy-disk" ></span>&nbsp;Save</button>
			</div>
		</div>
		<div id="showUserAndTimeUpdate" style="color: #22a1c8; display:none;padding-top: 50px;">			
			<div class="userAndTimeUpdateStyleDiv">
				<div class="row">
					<div class="col-sm-12" id="uidAndTsUpdate">								
					</div>
				</div>				
			</div>
		</div>
	</div>	



<div id="alert-msg" class="notifications top-right" style="display: none;">
	<div id="message" class="alert in fade alert-danger">There is no change.</div>
</div>