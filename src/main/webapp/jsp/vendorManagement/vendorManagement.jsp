<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container-fluid container-override">
   <div class="panel panel-primary">
      <div class="panel-heading"><s:message code='vendorManagement.panelHeading' /></div>
      <div class="panel-body style-panel-body">
         <div class="col-sm-6 margin-top10 hidden">
            <label class="control-label col-sm-2 style-header">Vendor:</label>
            <!-- <input type="text" class="form-control col-sm-8" id="vendor-input" -->
            <!-- value="Unbeatable Sales Inc."> -->
            <div class="input-group">
               <input id="vendor-search-txt" type="text" class="form-control col-sm-8 vendor-search txt-search-vendor-header input-sm">
               <div class="wrap-caret"></div>
               <span class="input-group-addon btn-search-vendor-header"><i
                  class="glyphicon glyphicon-search"></i></span>
            </div>
         </div>
<!--          Duyen add vendorCombobox  -->
         <div class="col-sm-4 margin-top10">
            <label class="control-label col-sm-2 style-header">Vendor:</label>            
            <select id="vendor-input" class="input-sm select-sm" tabindex="2">
               <option value="E"></option>
               <c:forEach var="vendor" items="${lstVendors}">
                   <option value="${vendor.vendorId}">${vendor.vendorId} - ${vendor.vendorName}</option>
               </c:forEach>
           </select>
         </div>

         <div id="vendor-alert-wp" class="col-md-12 margin-top10 hidden">
         	<div class="alert alert-danger margin-bottom10" role="alert">
		    	<span id="vendor-message-alert"></span>
		    </div>
         </div>
         
         <div class="col-sm-12 tabs-style hidden">
            <!-- Nav tabs -->
            <ul id="vendorTabs" class="nav nav-tabs style-nav-tabs" role="tablist">
               <li class="active"><a id="map-vendor-hierarchy-a" href="#map-vendor" role="tab"
                  data-toggle="tab">Map Vendor Hierarchy</a></li>
               <li><a id="assortment-rules-a" href="#assortment-rules" role="tab"
                  data-toggle="tab">Assortment Rules</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
               <div class="tab-pane active" id="map-vendor">
                  <div class="col-md-8 down-up-style text-left hidden">
                     <form id="form-upload" class="form-inline" method="POST" enctype="multipart/form-data" action="vendor-management/upload-hierarchy-map">
						<div class="form-group">
						  	<span class="glyphicon glyphicon-open icon-down-up-style"></span>
							<!--<a id="upload-Hierarchy-map">Upload Hierarchy Map</a> -->
						</div>
						<div class="form-group">
						  	<input id="upload-Hierarchy-map-input" type="file" name="file"/>
						  	<input id="vendorId" type="hidden" name="vendorId"/>
						  	<input type="submit" value="Press" style="display:none ;">
						</div>
					 </form>         
<!--                      <form id="form-upload" method="POST" enctype="multipart/form-data" action="upload-hierarchy-map"> -->
<!-- 					  	<input id="upload-Hierarchy-map-input" type="file" name="file"/> -->
<!-- 					 	<input type="submit" value="Press" style="display:none ;"> -->
<!-- 					 </form> -->
<!--                      <input id="upload-Hierarchy-map-input" type="file" name="file" style="display: none;"/> -->
                  </div>
                  <div class="col-md-4 down-up-style text-right hidden">
                     <span class="glyphicon glyphicon-save icon-down-up-style"></span><a id="download-template-btn">Download
                     template</a>
                  </div>
                  
                  <div id="alert-wp" class="col-md-12 margin-top10 hidden">
                  	<div class="alert alert-danger margin-bottom10" role="alert">
				      <span id="message-alert"></span>
				    </div>
                  </div>
                  
                  <div class="col-md-12 table-responsive">
		               <table id="vendor-hierarchy-mapping-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
		                  <colgroup>
		                     <col class="index-group">
		                     <col class="category1-group">
		                     <col class="category2-group">
		                     <col class="category3-group">
		                     <col class="category4-group">
		                     <col class="subcommodity-group">
		                     <col class="search-subcommodity-group">
		                     <col class="delete-group">
		                  </colgroup>
		                  <thead>
		                     <tr class="first-tr head-tr">
		                        <th>No.</th>
		                        <th>Category 1</th>
		                        <th>Category 2</th>
		                        <th>Category 3</th>
		                        <th>Category 4</th>
		                        <th>Sub Commodity</th>
		                        <th></th>
		                        <th></th>
		                     </tr>
		                  </thead>
		                  <tbody>
		                  </tbody>
		               </table>
		                <div class="col-md-offset-8 col-md-4 style-add-new-row hidden clear-fix text-right padding-right0">
		                  	<div class="pull-right">
		                  		<button id="add-new-row-id" type="button" class="btn btn-primary margin-right5">Add new row</button>
		                  		<button id="vendor-hierarchy-save-btn" type="button" class="btn btn-danger ">Save</button>
		                  	</div>	
		                </div>
		            </div>
                  
                 
<!--                   <div class="col-md-12 style-add-new-row hidden clear-fix"> -->
<!--                      <button id="add-new-row-id" type="button" class="btn btn-primary pull-right">Add new row</button> -->
<!--                   </div> -->
<!--                   <div class="clearfix visible-xs-block"></div> -->
<!--                   <div class="col-md-12 style-save"> -->
<!--                      <button id="vendor-hierarchy-save-btn" type="button" class="btn btn-danger pull-right">Save</button> -->
<!--                   </div> -->
               </div>
               <div class="tab-pane" id="assortment-rules">
                  <div id="contentApp">
                     <!-- ######################## -->
                     <%@ include file="assortmentRule.jsp" %>
                     <!-- ######################## -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal" id="subCommodityModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header modal-header-custom">
            <form role="form" action="#">
               <div class="form-group" role="form">
                  <h5 class="modal-title modal-title-custom" id="myModalLabel">Please select a Sub Commodity</h5>
                  <div class="input-group">
                     <input id="search-subCommodity-modal" type="text" class="form-control col-sm-8 vendor-search ui-autocomplete-input disabled" autocomplete="off">
                     <span class="input-group-addon" id="btn-search-commodity-modal"><i class="glyphicon glyphicon-search"></i></span>
                     <!--<div class="input-group-btn"> -->
                     <!--<button type="button" class="btn btn-default btn-ok-modal">OK</button> -->
                     <!--</div> -->
                  </div>
                  <!--<input type="text" class="form-control" id="search-subCommodity-modal" placeholder=""> -->
               </div>
            </form>
            <button type="button" class="close style-button-close"
               data-dismiss="modal">
            <span class="glyphicon glyphicon-remove-sign" style="color: white;"></span>
            </button>
         </div>
         <div class="modal-body">
         	<div id="alert-wp-jstree" class="col-md-12 margin-top10 hidden">
               	<div class="alert alert-danger margin-bottom10" role="alert">
	      			<span id="message-alert-jstree"></span>
	    		</div>
            </div>
            <div id="jstree">
            </div>
            <!--<button>demo button</button> -->
         </div>
         <div class="modal-footer">
            <!-- <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
            <span id="show-top50-jstree" class="pull-left hidden">Due to the large volume of data returned, only top 20 results are listed</span>
            <button type="button" class="btn btn-primary btn-ok-modal">OK</button>
         </div>
         <!--</div> -->
      </div>
   </div>
</div>

<!-- Category1 required modal -->
<div class="modal" id="category-required-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
		<h5>Category 1 is required</h5>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
<!-- 		<button id="ok-comfirm-btn" type="button" class="btn btn-primary" data-dismiss="modal">Ok</button> -->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
