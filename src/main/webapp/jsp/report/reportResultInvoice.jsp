<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
   uri="http://www.springframework.org/security/tags"%>
<style>
   .table-bordered > thead > tr > th, .table-bordered > thead > tr > td{
   border-bottom-width: 0px;
   }
</style>

<div class="container-fluid container-override">
   <div class="panel panel-primary">
      <div class="panel-heading" >
         Invoices Report 

      </div>
      <div class="panel-body style-panel-body">
         <div id="contentApp" class="padding-top0 margin-top10">

				<div class="row no-right-padding" id="searchResult">
                 <!-- <span class="text-bold">Search Results</span> -->
                 <div class="col-md-11">
                 	<label class="col-md-1 no-left-padding no-right-padding"> View page 1 of 1</label>
                    
	                 <div class="col-md-10 add-paging pagination">
				        <ul>
				          <li><a href="#" id="previous">«</a></li>
				          <li><a href="#" id="next">»</a></li>
				        </ul>
	                 </div>

                 </div>
                 <div class="col-md-1 no-right-padding">
                     <div class="col-md-4 no-right-padding no-left-padding left-align">
                        <span class="icon-pdf"> </span>
                     </div>
                     <div class="col-md-4 no-right-padding no-left-padding left-align">
                        <span class="icon-excel"> </span>
                     </div>
                     <div class="col-md-4 no-right-padding no-left-padding left-align">
                        <span class="icon-print"> </span>
                     </div>
                 </div>
            </div>

            <div class="report-info">
               <div class="form-group">
                  <div class="row center-align">
                 
                   <c:choose>
					    <c:when test="${reportType == 1}">
					        <h3>REPORT OF INVOICES WHICH HAVE BEEN RECONCILED</h3>
					    </c:when>
					    <c:when test="${reportType == 2}">
					       <h3>REPORT OF INVOICES WHICH HAVE ORDERS WITH NOT MATCHING HEB ORDER</h3>
					    </c:when>
					    <c:otherwise>
					       <h3>REPORT OF INVOICES WHICH HAS AN ORDER FOR WITH HEB HAS NOT RECIEVED AN ASN</h3>
					    </c:otherwise>
					</c:choose>
                  </div>
                  <div class="row">
                     <div class="col-md-4">
                     </div>
                     <div class="col-md-4">
                        <label class="col-md-5 no-right-padding">Vendor: </label>
                        <label class="col-md-7 add-info no-left-padding left-align"><c:out value="${vendorId}"/></label>
                     </div>
                     <div class="col-md-4">
                     </div>
                     
                  </div>
                  <div class="row">
                     <div class="col-md-4">
                     </div>
                     <div class="col-md-4">
                        <label class="col-md-5 no-right-padding">Invoice date: </label>
                        <label class="col-md-7 add-info no-left-padding left-align"><c:out value="${invoiceDateBegin}"/> – <c:out value="${invoiceDateEnd}"/></label>
                     </div>
                     <div class="col-md-4">
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-4 no-right-padding">
                     </div>
                     <div class="col-md-4">
                        <label class="col-md-5">ASN Received Date: </label>
                        <label class="col-md-7 add-info no-left-padding left-align"><c:out value="${invoiceDateBegin}"/> – <c:out value="${invoiceDateEnd}"/></label>
                     </div>
                     <div class="col-md-4">
                     </div>
                  </div>
                  
               </div>

               <!-- Tab panes -->
               <div class="tab-content">
			       <c:choose>
					    <c:when test="${reportType == 1}">
					       <div class="vendor"> 755504 - KeHE Distributors, LLC</div>
		                     <div class="table-responsive">
		                        <table id="report-result-1-table" class="table table-condensed common-table-style table-hover table-striped table-bordered add-place-id">
		                        	<thead class="">
		                        		<tr class="firs-tr head-tr addon">
		                        			<th rowspan="2" class="add-width">Order No</th>
			                                <th rowspan="2" class="add-width">Line Item</th>
					                        <th rowspan="2" class="add-width">Reconciled Date</th>
			                                <th colspan="4" >Vendor</th>
					                        <th colspan="4" >HEB</th>
					                        <th colspan="4" >Final(After Adjustment)</th>
					                        <th rowspan="2" class="add-width">Adjustment Reason</th>
					                     </tr>
					                     <tr class="second-tr head-tr">
			                                 <th class="add-width">Extended Cost</th>
			                                 <th class="add-width">Freight Cost</th>
			                                 <th class="add-width">Line Item Cost</th>
			                                 <th class="add-width">Line Item Qty</th>

			                                 <th class="add-width">Extended Cost</th>
			                                 <th class="add-width">Freight Cost</th>
			                                 <th class="add-width">Line Item Cost</th>
			                                 <th class="add-width">Line Item Qty</th>

			                                 <th class="add-width">Extended Cost</th>
			                                 <th class="add-width">Freight Cost</th>
			                                 <th class="add-width">Line Item Cost</th>
			                                 <th style="border-right-width: 1px" class="add-width">Line Item Qty</th>
					                     </tr> 
		                        	</thead>
		                        </table>
		                     </div>


		                    <!-- <div class="table-responsive">
		                        <table id="report-result-1-nestest-table" class="table table-condensed common-table-style table-hover table-striped table-bordered add-place-id">

		                           <thead>
				                     <tr class="firs-tr head-tr addon">
		                                <th colspan="4" style="border-bottom-width: 1px;">Vendor</th>
				                        <th colspan="4" style="border-bottom-width: 1px;">HEB</th>
				                        <th colspan="4" style="border-bottom-width: 1px;">Final(After Adjustment)</th>
				                        <th rowspan="2">Order No</th>
		                                <th rowspan="2">Line Item</th>
				                        <th style="border-right-width: 1px" rowspan="2">Adjustment Reason</th>
				                     </tr>
				                     <tr class="second-tr head-tr">

		                                 <th>Extended Cost</th>
		                                 <th>Freight Cost</th>
		                                 <th>Line Item Cost</th>
		                                 <th>Line Item Qty</th>

		                                 <th>Extended Cost</th>
		                                 <th>Freight Cost</th>
		                                 <th>Line Item Cost</th>
		                                 <th>Line Item Qty</th>

		                                 <th>Extended Cost</th>
		                                 <th>Freight Cost</th>
		                                 <th>Line Item Cost</th>
		                                 <th style="border-right-width: 1px" >Line Item Qty</th>
				                     </tr> 
				                  </thead>
		                        </table>
		                     </div> -->
					    </c:when>
					    <c:when test="${reportType == 2}">
					        <div class="table-responsive">
		                        <table id="report-result-2-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
		                           <thead>
		                              <tr class="firs-tr head-tr">
		                                 <th>Vendor Name</th>
		                                 <th>Vendor ID</th>
		                                 <th>Invoice ID</th>
		                                 <th>Date Range</th>
		                                 <th>Invoice Date</th>
		                                 <th>Amount</th>
		                                 <th>Order No</th>
		                                 <th>Line Item</th>
		                                 <th>Line Item Cost</th>
		                                 <th>Line Item Qty</th>
		                              </tr>
		                           </thead>
		                        </table>
		                     </div>
					    </c:when>
					    <c:otherwise>
					        <div class="table-responsive">
		                        <table id="report-result-3-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
		                           <thead>
		                              <tr class="firs-tr head-tr">
		                                 <th>Vendor Name</th>
		                                 <th>Vendor ID</th>
		                                 <th>Invoice ID</th>
		                                 <th>Date Range</th>
		                                 <th>Invoice Date</th>
		                                 <th>Amount</th>
		                                 <th>Order No</th>
		                                 <th>Line Item</th>
		                                 <th>Line Item Cost</th>
		                                 <th>Line Item Qty</th>
		                              </tr>
		                           </thead>
		                        </table>
		                     </div>
					    </c:otherwise>
					</c:choose>
                  
                  
               </div>
               
               <!-- end tab -->
            </div>
				
           

         </div>
      </div>
      <!-- end body -->
   </div>
</div>

<div id="hidden-to-search">
      <div id="report-type"><c:out value="${reportType}"/></div>
      <div id="vendor"><c:out value="${vendorId}"/></div>
</div>