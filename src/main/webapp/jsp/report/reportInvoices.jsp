<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container-fluid container-override">
   <div class="panel panel-primary">
      <div class="panel-heading">
         Invoices Report
      </div>
      <div class="panel-body style-panel-body">
         <div id="contentApp">
            <div class="legend">
            <span class="accordion-toggle-bdm" data-toggle="collapse" href="#invoice-list-header">Search Criteria</span>
         </div>
            <form id="search-report-form" target="_blank" action="/assortment/report-result-invoice">
               <div id="invoice-list-header" class="row collapse in">
               <div class="col-sm-6">
                  <div class="form-horizontal common-form-style">
                     <div class="form-group">
                        <label class="col-md-3">Report Type</label>
                        <div class="col-md-8">
                            <select class="form-control input-sm" id="report-type" name="reportType">
                              <option value="1">Invoices which have been reconciled</option>
                              <option value="2">Invoices which have orders with not matching HEB order</option>
                              <option value="3">Invoices which has an order for which HEB has not received an ASN</option>
                          </select>
                        </div>
                     </div>
                     
                     
                     <div class="form-group inv-date">
                        <label class="col-md-3">Invoice Date</label>
                        <div class="col-md-8">
                            <select class="form-control input-sm" id="inv-date" name="invDate">
                              <option value="1">Today</option>
                              <option value="2">This Week</option>
                              <option value="3">This Month</option>
                              <option value="4">This Year</option>
                              <option value="5">Custom</option>
                          </select>
                        </div>
                     </div>

                     <div class="form-group asn-date">
                        <label class="col-md-3">ASN Date</label>
                        <div class="col-md-8">
                            <select class="form-control input-sm" id="asn-date">
                              <option value="1">Today</option>
                              <option value="2">This Week</option>
                              <option value="3">This Month</option>
                              <option value="4">This Year</option>
                              <option value="5">Custom</option>
                          </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-3">Order By</label>
                        <div class="col-md-8">
                           <select class="form-control input-sm" id="order-by">
                                 <option value="1">Vendor Id</option>
                                 <option value="2">Vendor Name</option>
                                 <option value="3">Vendor Name</option> 
                                 <option value="3">Order Date</option>
                             </select>
                        </div>
                     </div>
                     
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-horizontal common-form-style">
                     <div class="form-group">
                        <label class="col-md-2">Vendor</label>
                        <div class="col-md-9">
                           <input id="vendor-input" type="text" class="form-control input-sm" name="vendorId">
                           <div class="wrap-caret"><span class="caret"></span></div>
                        </div>
                     </div>
                     <div class="form-group inv-date">
                        <label class="col-md-2">From</label>
                        <div class="col-md-4">
                            <div class="input-group">
                              <input class="form-control input-sm" type="text" placeholder=""
                                 id="dateBegin" name="invoiceDateBegin">
                              <div class="input-group-addon" id="dateBegin-icon-wp">
                                 <i class="fa fa-calendar"></i>
                              </div>
                           </div>
                        </div>
                        <label class="col-md-1 right-align">To</label>
                        <div class="col-md-4 no-left-padding">
                           <div class="input-group">
                              <input class="form-control input-sm" type="text" placeholder=""
                                 id="dateEnd" name="invoiceDateEnd">
                              <div class="input-group-addon" id="dateEnd-icon-wp">
                                 <i class="fa fa-calendar"></i>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="form-group asn-date">
                        <label class="col-md-2">From</label>
                        <div class="col-md-4">
                            <div class="input-group">
                              <input class="form-control input-sm" type="text" placeholder=""
                                 id="asn-date-from" name="asnDateBegin">
                              <div class="input-group-addon" id="asn-date-from-icon-wp">
                                 <i class="fa fa-calendar"></i>
                              </div>
                           </div>
                        </div>
                        <label class="col-md-1 right-align">To</label>
                        <div class="col-md-4 no-left-padding">
                           <div class="input-group">
                              <input class="form-control input-sm" type="text" placeholder=""
                                 id="asn-date-to" name="asnDateEnd">
                              <div class="input-group-addon" id="asn-date-to-icon-wp">
                                 <i class="fa fa-calendar"></i>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="form-group">
                        <label class="col-md-2"></label>
                        <div class="col-md-4">
                            
                        </div>
                        <label class="col-md-1 right-align"></label>
                        <div class="col-md-4 no-left-padding right-align">
                           <button id="search-invoice-list-btn" type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Generate Report</button>
                        </div>

                     </div>

                  </div>
               </div>
            </div>

            </form>
            
            
         </div>
      </div>
      <!-- end body -->
   </div>
</div>