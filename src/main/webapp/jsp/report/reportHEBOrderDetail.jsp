<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
   uri="http://www.springframework.org/security/tags"%>
<style>
   .table-bordered > thead > tr > th, .table-bordered > thead > tr > td{
   border-bottom-width: 0px;
   }
</style>
<div class="container-fluid container-override">
   <div class="panel panel-primary">
      <div class="panel-heading">
      	HEB Orders Report / Line Items List - 
         <span id="invoice-id" class="forID" value="<c:out value="${invoice}"/>">
            Order ID
                <c:out value="${invoice}"/>
                <c:out value="${aa}"/>
         </span>

      </div>
      <div class="panel-body style-panel-body">
         <div id="contentApp" class="padding-top0 margin-top10">
            <div class="col-md-8 down-up-style text-left padding-left0 ">
            	<a href="<s:url value="/report-order"></s:url>" id="back-invoice-list"><i class="fa fa-angle-double-left"></i>&nbsp;Back to Orders Report</a>	
            </div>
            <div class="col-md-4 down-up-style text-right padding-right0">
               <div id="export-invoice-details-btn" class="export-excel" onclick="exportListDetailsInvoiceToExcel('${invoice}')"></div>
            </div>

            <div id="invoice-list-header" class="row collapse in">
               <div class="col-sm-6">
                  <form class="form-horizontal common-form-style" role="form">
                     <div class="form-group">
                        <label class="col-md-3">Vendor</label>
                        <label class="col-md-9 value-content">
                           755504 - KeHE Distributors, LLC
                        </label>
                     </div>
                     <div class="form-group">
                        <label class="col-md-3">Order Date</label>
                        <label class="col-md-9 value-content">
                           12/12/2012
                        </label>
                     </div>
                     
                  </form>
               </div>
               <div class="col-sm-6">
                  <div class="form-horizontal common-form-style">
                    <div class="form-group">
                        <label class="col-md-2">Order Status</label>
                        <label class="col-md-4 value-content">
                               Mismatch
                          </label>
                          <label class="col-md-2">Order ID</label>
                          <label class="col-md-4 value-content">
                              <c:out value="${invoice}"/>
                          </label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2">Amount</label>
                        <label class="col-md-10 value-content">
                           $ 2.66
                        </label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-12 padding-left0 padding-right0 table-responsive">
               <table id="HEB-order-details-table" class="table table-condensed common-table-style table-hover table-striped table-bordered display" cellspacing="0" width="100%">
                  
                  <thead>
                     
                     <tr class="firs-tr head-tr">
                        <th>No</th>
                        <th>Order ID</th>
                        <th>Line Item ID</th>
                        <th>Line Item Type</th>
                        <th>Ship Date</th>
                        <th>Amount</th>
                        <th>Quantity</th>
                     </tr>
                  </thead>
                  <!-- <thead>
                     <tr class="second-tr head-tr second-tr-HEB-order-details-table">
                        <th><button id="clear-filtter-btn-HEB-order-detail" type="button" class="btn btn-default btn-sm filter-btn-style clear-btn" title="Clear the Filter"></button></th>
                        <th><input id="search-order-id-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                        <th><input id="search-line-item-id-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                        <th><input id="search-line-item-type-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                        <th><input id="search-ship-date-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                        <th><input id="search-amount-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                        <th><input id="search-qty-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                        
                     </tr>
                  </thead> -->
                  <tbody>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     <tr>
                        <th>1</th>
                        <th>ORB 001</th>
                        <th>1233</th>
                        <th>Line Item Type</th>
                        <th>10/10/2012</th>
                        <th>$ 555</th>
                        <th>10</th>
                     </tr>
                     
                  </tbody>
               </table>
            </div>

         </div>
      </div>
      <!-- end body -->
   </div>
</div>
