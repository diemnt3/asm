<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container-fluid container-override">
   <div class="panel panel-primary">
      <div class="panel-heading">
         HEB Orders Report
      </div>
      <div class="panel-body style-panel-body">
         <div id="contentApp">
         	<div class="legend">
				<span class="accordion-toggle-bdm" data-toggle="collapse" href="#invoice-list-header"><i id="fa-angle-double-down-style" class="fa fa-angle-double-down" style="width: 15px;"></i>Search Criteria</span>
			</div>
            <div id="invoice-list-header" class="row collapse in">
               <div class="col-sm-6">
                  <form class="form-horizontal common-form-style" role="form">
                     <div class="form-group">
                        <label class="col-md-3">Vendor</label>
                        <div class="col-md-8">
                           <input id="vendor-input" type="text" class="form-control input-sm">
                           <div class="wrap-caret"><span class="caret"></span></div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-3">Order Date Range</label>
                        <div class="col-md-8">
                           <div class="row">
                              <div class="col-md-6 no-left-padding">
                                 <div class="input-group">
                                    <input class="form-control input-sm" type="text" placeholder=""
                                       id="dateBegin">
                                    <div class="input-group-addon" id="dateBegin-icon-wp">
                                       <i class="fa fa-calendar"></i>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6 no-left-padding no-right-padding">
                                 <div class="input-group receive-date-bw-wp">
                                    <input class="form-control input-sm" type="text" placeholder=""
                                       id="dateEnd">
                                    <div class="input-group-addon" id="dateEnd-icon-wp">
                                       <i class="fa fa-calendar"></i>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                     
                  </form>
               </div>
               <div class="col-sm-6">
                  <div class="form-horizontal common-form-style">
	                 <div class="form-group">
	                 	<label class="col-md-3">Order Status</label>
	                    <div class="col-md-3">
	                         <select class="form-control input-sm" id="invoice-status">
	                         	<option></option>
		                        <option value="0">Match</option>
		                        <option value="1">Mismatch</option>
		                    </select>
	                    </div>
	                    <label class="col-md-2">Order ID</label>
	                    <div class="col-md-4">
	                        <input id="invoice-number" type="text" class="form-control input-sm">
	                    </div>
	                 </div>
                     
                     <div class="btn-group pull-right search-btn-wp">                          
						<button id="search-order-list-btn" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Search</button>
                     </div>
                  </div>
               </div>
            </div>
            
            <div class="row no-right-padding" id="searchResult">
                <div class="col-md-2">
                     <span class="text-bold">Search Results</span>
                 </div>
                 <div class="col-md-10 no-right-padding">
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-7 no-right-padding">
                       <label class="col-md-2 no-right-padding right-align">Export to</label>
                       <div class="col-md-3 export-dropdown add">
                         <div class="btn-group"> 
                               <a class="btn btn-default dropdown-toggle btn-select" data-toggle="dropdown" href="#">
                                  <span>Excel</span>
                                <span class="caret"></span></a> 
                               <ul class="dropdown-menu">
                                  <li><a href="#" class="export-to-choose">
                                     <span class="icon-csv"> CSV</span> </a></li>
                                  <li><a href="#" class="export-to-choose"><span class="icon-pdf"> PDF</span> </a></li>
                                  <li><a href="#" class="export-to-choose"><span class="icon-excel"> Excel</span> </a></li>
                               </ul>
                            </div>
                          </div>
                      <label class="col-md-1 no-right-padding right-align">With</label>
                       <div class="col-md-5">
                          <select class="form-control input-sm" id="export-option" >
                             <option value="1">Invoice Summary</option>
                             <option value="2">Invoice Details</option>
                             <option value="3">Item Details</option>
                         </select>
                      </div>
                      <div class="col-md-1 export-btn no-right-padding"></div>
                   </div>
                 </div>
					       
               
				</div>
				
            <div class="common-content-tab">
               <div class="table-responsive">
                  <table id="report-order-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
                     <thead>
	                     <tr class="firs-tr head-tr">
	                        <th>No</th>
	                        <th>Order ID</th>
	                        <th>Vendor</th>
	                        <th>Order Date</th>
	                        <!-- <th>Line Item Count</th> -->
                            <th>Status</th>
	                        <th>Amount</th>
	                     </tr>
	                  </thead>
                     <thead>
                        <tr class="second-tr head-tr second-tr-report-order-table">
                           <th><button id="clear-filtter-btn-retailInquiry" type="button" class="btn btn-default btn-sm filter-btn-style clear-btn" title="Clear the Filter"></button></th>
                           <th><input id="search-order-id-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                           <th><input id="search-vendor-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                           <th><input id="search-order-date-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                           <!-- <th><input id="search-line-item-count-btn" type="text" class="form-control input-sm text-right pd0"/></th> -->
                           <th><input id="search-total-qty-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                           <th><input id="search-amount-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                           
                        </tr>
                     </thead>
                     
                  </table>
               </div>
            </div>
				
            <!-- end tab -->
            
            
         </div>
      </div>
      <!-- end body -->
   </div>
</div>