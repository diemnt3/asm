<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
   uri="http://www.springframework.org/security/tags"%>
<style>
   .table-bordered > thead > tr > th, .table-bordered > thead > tr > td{
   border-bottom-width: 0px;
   }
</style>
<div class="container-fluid container-override">
   <div class="panel panel-primary">
      <div class="panel-heading">
      	<%-- <c:if test="${forPage == 'reportForOrder'}">
      		Orders Report / Line Items List - 
	         <span id="invoice-id" value="<c:out value="${invoice}"/>">
	            <c:out value="${invoice}"/>
	         </span>
      	</c:if> --%>
      	
      	<c:choose>
		      <c:when test="${forPage == 'reportForOrder'}">
		      	Orders Report / Line Items List - 
		         <span id="invoice-id" class="forID" value="<c:out value="${invoice}"/>">
		            Order ID
                  <c:out value="${invoice}"/>
		         </span>
		      </c:when>
		
		      <c:otherwise>
		      	Invoices Report / Orders List / Line Items List - 
		         <span id="invoice-id" class="forID" value="<c:out value="${invoice}"/>">
                  Order ID
		            <c:out value="${invoice}"/>
		         </span>
		      </c:otherwise>
		</c:choose>

      </div>
      <div class="panel-body style-panel-body">
         <div id="contentApp" class="padding-top0 margin-top10">
            <div class="col-md-8 down-up-style text-left padding-left0 ">
            	<c:choose>
				      <c:when test="${forPage == 'reportForOrder'}">
				      	<a href="<s:url value="/report-order"></s:url>" id="back-invoice-list"><i class="fa fa-angle-double-left"></i>&nbsp;Back to Orders Report</a>
				      </c:when>
				
				      <c:otherwise>
				      	<a class="link-order-a"><i class="fa fa-angle-double-left"></i>&nbsp;Back to Invoice Details Report </a>
				      </c:otherwise>
				</c:choose>		
               
               <input id="upload-Hierarchy-map-input" type="file" name="upload-Hierarchy-map-input" style="display: none;">
            </div>
            <div class="col-md-4 down-up-style text-right padding-right0">
               <div id="export-invoice-details-btn" class="export-excel" onclick="exportListDetailsInvoiceToExcel('${invoice}')"></div>
            </div>

            <div class="col-sm-12 padding-left0 padding-right0 table-responsive">
               <table id="order-details-table" class="table table-condensed common-table-style table-hover table-striped table-bordered display" cellspacing="0" width="100%">
                  
                  <thead>
                     <tr class="first-tr head-tr addon">
                        <th rowspan="2">No</th>
                        <th rowspan="2">Line Item ID</th>
                        <th rowspan="2">Matched Type</th>
                        <th rowspan="2">Status Code</th>
                        <th rowspan="2">Date</th>
                        <th colspan="2" style="border-bottom-width: 1px;">Vendor Invoice</th>
                        <th colspan="2" style="border-bottom-width: 1px;">Final(After Adjustment)</th>
                        
                        <th rowspan="2">Adjust Reason</th>
                     </tr>
                     <tr class="second-tr head-tr">
                        <th>Quantity</th>
                        <th>Amount</th>
                        <th>Quantity</th>
                        <th style="border-right-width: 1px">Amount</th>
                     </tr>
                  </thead>

                  <thead>
                     <tr class="second-tr second-tr-order-details-table">
                        <th><button id="clear-filtter-btn-order-detail" type="button" class="btn btn-default btn-sm filter-btn-style clear-btn" title="Clear the Filter"></button></th>
                        <th><input id="search-upc-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                        <th><input id="search-description-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                        <th><input id="search-size-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                        <th><input id="search-msrp-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                        <th><input id="search-retail-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                        <th><input id="search-cost-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                        <th><input id="search-gp-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                        <th><input id="search-penny-btn" type="text" class="form-control input-sm text-right pd0"/></th>
                        <th><input id="search-penny-btn" type="text" class="form-control input-sm text-left pd0"/></th>
                        
                     </tr>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
            </div>

         </div>
      </div>
      <!-- end body -->
   </div>
</div>
