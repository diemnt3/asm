<div class="container-fluid container-override">
	<div class="panel panel-primary">
		<div class="panel-heading">DSV Pricing Rules</div>
		<div class="panel-body style-panel-body">
			<div class="contentApp" id="contentApp">
				<!--row-->
				<div class="col-sm-12 padding-bottom">
					<div class="col-md-3">
						<label
							class="control-label col-sm-3 col-md-3 style-header custom-lable">Commodity</label>
						<div id="commodityInputWrap" class="col-sm-7 col-md-8">
							<input id="commodityInput" type="text" tabindex="1"
								class="form-control vendor-search txt-search-vendor-header input-sm">
							<div class="wrap-caret"></div>
						</div>
					</div>
					<div class="col-md-3">
						<label
							class="control-label col-sm-5 col-md-5 style-header custom-lable">Sub-Commodity</label>
						<div id="subCommodityInputWrap" class="col-sm-7 col-md-7">
							<input id="subCommodityInput" tabindex="2" type="text" class="form-control input-sm">
							<div class="wrap-caret"></div>
						</div>
					</div>
					<div class="col-md-3">
						<label
							class="control-label col-sm-3 col-md-2 style-header custom-lable">Vendor</label>
						<div id="vendorInputWrap" class="col-sm-7 col-md-8">
							<select id="vendorInput" tabindex="3" class="form-control vendor-search txt-search-vendor-header input-sm"></select>
						</div>
					</div>
					<div class="col-md-3">
						<label class="control-label col-sm-3 col-md-4 style-header custom-lable">Pricing Strategy</label>
						<div class="col-sm-7 col-md-7">
							<select id="pricing-strategy" class="form-control input-sm" tabindex="4">
								<option value="LEAD" checked="checked">LEAD</option>
								<option value="FOLOW">FOLLOW</option>
							</select>
						</div>
					</div>
				</div>
				<!--row-->
				<div class="col-md-12 rule-content">
					<div id="price-main-content">
						<div class="col-md-4 border-right sub-content arrow_box">
							<div class="price-sub-content">
								<h4 id="vendorBasedPricingLabel" class="page-heading">Vendor Based Pricing</h4>
								<div class="col-md-12 form-group">
									<div class="row">
										<div class="col-sm-7">
											<label class="col-sx-12 control-label custom-lable input-sm text-per" id="11-label">MAP/SRP pricing Index </label>
										</div>
										<div class="col-xs-12 col-sm-5 pull-right" style="padding-right: 0">
											<div id="wrap-11-percent" class="col-xs-2 text-per pull-right">
												<span class="percent-cls">%</span>
											</div>
											<div id="wrap-11-input" class="col-xs-10  pull-right">
												<input type="text" id="11-input" tabindex="5" class="form-control input-sm align-right" placeholder="" title="" maxLength="9">
											</div>
										</div>
									</div>
									<div class="row">
										<label class="col-sm-12 control-label text-left"><u>OR</u></label>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-sm-4">
												<label id="12-label" class="control-label custom-lable input-sm text-per" >Target GP </label>
											</div>
											<div class="col-sm-8">
												<div class="row">
													<div class="col-xs-1 pull-right text-per">
														<span class="percent-cls">%</span>
													</div>
													<div id="wrap-12-2-input" class="col-xs-4 pull-right">
														<input type="text" placeholder="" class="form-control input-sm align-right" tabindex="7" id="12-2-input" maxLength="9">
													</div>
													<div class="col-xs-2 pull-right text-center text-and">
														<span>or</span>
													</div>
													<div class="col-xs-1 pull-left text-per align-right">
														<span class="dollar-cls">$</span>
													</div>
													<div id="wrap-12-1-input"class="col-xs-4 pull-left">
														<input type="text" id="12-1-input" tabindex="6" class="form-control input-sm input-decimal" placeholder="" maxLength="10">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="price-sub-content">
								<h4 id="CompetitorPricingLabel" class="page-heading">Competitor Pricing</h4>
								<div class="page-content">
									<div style="margin: 5px;">Select one or more competitor for this Sub Commodity</div>
									<form class="form-horizontal customizedFormCls" role="form" _lpchecked="1">
										<div class="border-group jstree-scoll">
											<div id="competitor-tree" tabindex="8">
												<!-- Load Tree here -->
											</div>
										</div>
										<!--border-group -->
										<div class="form-group">
											<span id="13-label" class="col-sm-7 control-label">Competitor Index</span>
											<div class="col-xs-12 col-sm-5">
												<div id="wrap-13-input" class="row col-xs-11">
													<input type="text" placeholder="" class="form-control input-sm align-right" tabindex="9" id="13-input" maxlength="9">
												</div>
												<div class="col-xs-1 text-per pull-left">
													<span class="percent-cls">%</span>
												</div>
											</div>
										</div>
										<div class="form-group">
											<span class="col-sm-7 control-label text-right">On</span>
											<div class="col-sm-5">
												<select id="13-select" class="form-control input-sm"
													style="margin: 1px; padding: 0;" tabindex="10">
													<option value="" selected=""></option>
													<option value="High">Highest</option>
													<option value="Low">Lowest</option>
													<option value="Averg">Average</option>
													<option value="Mode">Mode</option>
												</select>
											</div>
										</div>

									</form>
								</div>
								<!--page-content-->
							</div>
						</div>
						<div
							class="col-md-4 border-right sub-content padding-bottom arrow_box">
							<h4 id="finishingRulesLabel" class="page-heading">Finishing Rules</h4>
							<div class="price-sub-content">
								<div class="col-md-12"><label id="" class="control-label custom-lable input-sm text-per">Recommended price should end with</label></div>
								<div class="col-md-12">
									<div id="roundDown" class="col-md-12">
										<div class="col-md-6">
											<input type="radio" name="rounding_rule" value="0" tabindex="11" checked />
											Rounding down to
										</div>
										<div class="col-md-1">
											<label class="control-label style-header">$0.</label>
										</div>
										<div class="col-md-3">
											<input type="text" id="14-1-input" class="form-control input-sm input-decimal" tabindex="12" placeholder="" maxLength="2" />
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<input type="radio" name="rounding_rule" value="1" tabindex="13" />
											Rounding up to
										</div>
										<div class="col-md-1">
											<label class="control-label style-header">$0.</label>
										</div>
										<div class="col-md-3">
											<input type="text" id="14-2-input" class="form-control input-sm input-decimal" placeholder="" tabindex="14" disabled maxLength="2" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 sub-content">
							<h4 id="boundaryRulesLabel" class="page-heading">Boundary Rules</h4>
								<div class="price-sub-content">
									<div class="col-md-12">
										<label id="15-label" class="control-label custom-lable input-sm text-per">Ignore price change if change is less than
										</label>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-12" style="padding: 0">
												<div id="wrap-15-dollar" class="col-md-1 text-per">
													<span class="dollar-cls">$</span>
												</div>
												<div id="wrap-15-1-input" class="col-md-3">
													<input id="15-1-input" type="text" class="form-control input-sm align-right" tabindex="15" maxLength="9" />
												</div>
												<div id="wrap-15-and" class="col-md-2 text-center text-per">
													<span>and</span>
												</div>
												<div id="wrap-15-2-input" class="col-md-3">
													<input id="15-2-input" type="text" class="form-control input-sm align-right" tabindex="16" maxLength="8" />
												</div>
												<div id="wrap-15-percent" class="col-md-1 text-per">
													<span class="percent-cls">%</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label id="16-label" class="control-label custom-lable input-sm text-per">Flag price change for review if change is greater than
										</label>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-12" style="padding: 0">
												<div id="wrap-16-dollar" class="col-md-1 text-per">
													<span class="dollar-cls">$</span>
												</div>
												<div id="wrap-16-1-input" class="col-md-3">
													<input id="16-1-input" type="text" class="form-control input-sm align-right" tabindex="17" maxLength="9" />
												</div>
												<div id="wrap-16-or" class="col-md-2 text-center text-per">
													<span>or</span>
												</div>
												<div id="wrap-16-2-input" class="col-md-3">
													<input id="16-2-input" type="text" class="form-control input-sm align-right" tabindex="18" maxLength="8" />
												</div>
												<div id="wrap-16-percent" class="col-md-1 text-per">
													<span class="percent-cls">%</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<label id="17-label" class="control-label custom-lable input-sm text-per">Flag price change for review if the new margin is not between
										</label>
									</div>
								<div class="col-md-12 padding-bottom">
									<div class="form-group">
										<div class="col-md-12" style="padding: 0">
											<div class="col-md-1 text-per" style="padding: 0; width: 15px; margin: 5px 0">
												<span class="hiddenSpace">space</span>
											</div>
											<div class="col-md-3" style="padding: 0; margin: 5px 0">
												<span style="display: block" class="text-center">Min</span>
												<input id="17-1-input" type="text" class="form-control input-sm align-right" tabindex="19" />
											</div>
											<div class="col-md-2 text-center text-per" style=" margin: 5px 0">
												<span class="hiddenSpace">space</span>
												<span>and</span>
											</div>
											<div class="col-md-3" style="padding: 0; margin: 5px 0">
												<span style="display: block" class="text-center">Max</span>
												<input id="17-2-input" type="text" class="form-control input-sm align-right" tabindex="20" />
											</div>
											<div class="col-md-1 text-per" style=" margin: 5px 0">
												<span class="hiddenSpace">space</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--row-->
				<div class="col-md-12 text-right border-top rule-content">
					<button id="pricing-save-btn" type="submit" tabindex="21" class="btn btn-danger">Save</button>
				</div>
				<div id="showUserAndTimeUpdate" class="panel-body col-md-12 rule-content" style="color: rgb(34, 161, 200);">
					<div class="userAndTimeUpdateStyleDiv" id="uidAndTsUpdate">Last Updated By <strong class="userAndTimeUpdateStyle">vn40577</strong> Last Updated On <strong class="userAndTimeUpdateStyle">03/31/2015 02:03</strong></div>				
				</div>
				<!--row-->
			</div>
			<!--contentApp-->
		</div>
	</div>
</div>