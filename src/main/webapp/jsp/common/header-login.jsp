<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<div id="header" class=container-fluid>
	<div class="container-fluid clearfix">
		<div class="navbar-header pull-left">
			<a class="navbar-brand" href="#"> <img src="<s:url value="/image/logo-Assortment.png"></s:url>"
				alt="DSV Invoice Match Tool" title="DSV Invoice Match Tool">
			</a>
		</div>
	</div>
</div>