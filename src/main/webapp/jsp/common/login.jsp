<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<div class="container-fluid">
<!-- 	<div id="wrapperLoginForm"> -->
		<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION.message}">
		    	<div class="alert alert-danger margin-top20">
			        <p>Your login attempt was unsuccessful. Please try again.</p>
			        <p>Cause: <c:out value='${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}' /></p>
			    </div>
		</c:if>
		<div class="contentLoginForm">
			<form name="f" action="/assortment/j_spring_security_check" method="POST"
				class="form-signin" role="form">
				<h3>Signin with your Onepass Id</h3>
				<div class="form-group">
					<input type="text" class="form-control" id="j_username"
						name="j_username" placeholder="Username" value="">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" id="password"
						name="j_password" placeholder="Password" value="">
						<c:set var="contextPath" scope="session" value="${pageContext.servletContext.contextPath}"/>
						<c:choose>
							<c:when test="${param.r eq contextPath}">
						       	<input type="hidden" name="spring-security-redirect" value="">
						    </c:when>
	
						    <c:otherwise>
						        <input type="hidden" name="spring-security-redirect" value="<c:out value='${param.r}' />">
						    </c:otherwise>
						</c:choose>		
				</div>
				<div class="clearfix gr-signing">
					<div class="checkbox pull-left">
						<label> <input type="checkbox"> Remember me
						</label>
					</div>
					<button id="loginBtn" type="submit" name="submit"
						class="btn btn-primary btn-md pull-right">
						Login
<%-- 						                	<span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp; --%>
<%-- 						               <i class="fa fa-sign-in"></i>&nbsp;&nbsp; 	 <span class="glyphicon glyphicon-log-in"></span> --%>
					</button>
				</div>
				<p class="text-center change-pw">
					<a href="https://onepass.heb.com/onepass/login" target="_blank">Change Password</a>
				</p>
			</form>
		</div>
<!-- 	</div> -->
</div>

