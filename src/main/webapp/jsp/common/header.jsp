<%@page import="com.heb.enterprise.dsv.jaf.security.HebUserDetails"%>
<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<%
org.springframework.security.core.Authentication auth = org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication(); 
HebUserDetails u = (HebUserDetails)auth.getPrincipal();
%>
<div id="wrapper">
	<div id="header">
		<div class="container-fluid clearfix">
			<div class="navbar-header pull-left">
				<a class="navbar-brand" href="<%=request.getContextPath()%>"> <img
					src="<s:url value="/image/logo-Assortment.png"></s:url>" alt="DSV Assortment Tool" title="DSV Assortment Tool">
				</a>
			</div>
			<div id="userAuthentication">
				<ul id="sectionLogin">
					<li>You are logged in as <b>
						<%
							if(u.getDisplayName() != null){
						%> 
							<sec:authentication property="principal.displayName" />
						<% 
							}else {%> 
							<sec:authentication property="principal.cn" />
						<% }
						%>		 
					</b></li>
					<li><a class="a-menu" href="<s:url value="/logout"></s:url>" title="Logout">Logout</a></li>
					<li><a class="a-menu" href="#" title="Help">Help</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="mainNav" class="container-fluid">
            <nav class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                	<div id="navbar-header-wp-button">
                		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#contentMainNav">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                  </button>
	                  <!-- <a class="navbar-brand" href="#">
	                      <img src="assets/image/logo-Assortment.png" alt="DSV Assortment Tool" title="DSV Assortment Tool">
	                  </a> -->	
                	</div>
                
                </div>

               <!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="contentMainNav">
				<ul class="nav navbar-nav width100">
					<li class="home-nav"><a class="a-menu" href="<s:url value="/"></s:url>" class="active"><img
							src="<s:url value="/image/icon-home.png"></s:url>"></a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Manage <span class="caret"></span></a>
						<ul class="dropdown-menu mgt8" role="menu">
							<li><a class="a-menu" href="<s:url value="/global-default"></s:url>">Global Defaults</a></li>
							<li><a class="a-menu" href="<s:url value="/vendor-management"></s:url>">Vendor Management</a></li>
							<li><a class="a-menu" href="<s:url value="/pricing-rules"></s:url>">Pricing Rules</a></li>
						</ul>
					</li>
					<li class="dropdown"><a class="a-menu" href="<s:url value="/product-review"></s:url>">Product Review</a></li>
					<!-- <li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Product Review <span class="caret"></span></a>
						<ul class="dropdown-menu mgt8" role="menu">
							<li><a class="a-menu" href="<s:url value="/new-products"></s:url>">New Products</a></li>
							<li><a class="a-menu" href="#">Product Changes</a></li>
							<li><a class="a-menu" href="<s:url value="/new-product-review"></s:url>">BDM Review</a></li>
						</ul>
					</li> -->
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Retail and Cost Review<span class="caret"></span></a>
						<ul class="dropdown-menu mgt8" role="menu">
							<li><a class="a-menu" href="<s:url value="/cost-review"></s:url>">New and Changes</a></li>
							<li><a class="a-menu" href="<s:url value="/retail-inquiry"></s:url>">Retail Inquiry</a></li>
						</ul>
					</li>
						
<!-- 						<li class="dropdown"><a href="#" class="dropdown-toggle" -->
<%-- 						data-toggle="dropdown">Invoice Match<span class="caret"></span></a> --%>
<!-- 						<ul class="dropdown-menu mgt8 multi-level" role="menu" aria-labelledby="dropdownMenu"> -->
<!-- 							<li class="dropdown-submenu"> -->
<!-- 				                <a tabindex="-1" href="#">Review and Adjust</a> -->
<!-- 				                <ul class="dropdown-menu"> -->
<%-- 				                  <li><a href="<s:url value="/vendor-rules"></s:url>">Manage Invoice Matching Rules</a></li> --%>
<%-- 				                  <li><a href="<s:url value="/invoice-match"></s:url>">Review Mismatches</a></li> --%>
<%-- 				                  <li><a href="<s:url value="/invoice-match/review-and-adjust/create-manual-invoice"></s:url>">Create Invoice</a></li>				                  --%>
<!-- 				                </ul> -->
<!-- 				             </li> -->
				            
<%-- 				             <li><a href="<s:url value="/user-access"></s:url>">User Access</a></li>						 --%>
<!-- 						</ul></li> -->
				</ul>
			</div>
			<!-- /.navbar-collapse -->
            </nav>
        </div>
</div>
<script>
	var contextPath = '<%=request.getContextPath()%>';
	var userNameApp = '<%=u.getUsername()%>';
	var isMakeChanges = false;
</script>