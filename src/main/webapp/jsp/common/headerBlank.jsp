<%@page import="com.heb.enterprise.dsv.jaf.security.HebUserDetails"%>
<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script>
	var contextPath = '<%=request.getContextPath()%>';
	var errorMessage = "";
</script>