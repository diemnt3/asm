<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <h2><s:message code="defaultErrorMessage" /></h2> --%>
<style type="text/css">
	*{margin: 0; padding: 0;}
	body {
		background-color: #fff;
	}
	#wrapper-error {
		width : 70%;
		margin: 2% auto;
	}
	#oops {
		display: block;
		font-size: 40px;
		font-weight: bold;
		margin: 0 auto;
	}
	span {
		display: block;
		font-size: 14px;
		color: #000;
		margin-top: 35px;
	}
	#viewError {
		margin-top: 15px;
		text-decoration: underline;
	}
	
</style>
<script>
	var contextPath = '<%=request.getContextPath()%>';
	errorMessage = "${errorMessage}";
</script>
<div id="wrapper-error">
	<p id="oops">OOps!</p>
	<span><s:message code="defaultErrorMessage" /></span>
	<a id="viewError">See more</a>
	<div id="message" class="hide" style="display:none"></div>
</div>
