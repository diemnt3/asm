<div class="container-fluid container-override">
<div class="panel panel-primary">
	<div class="panel-heading">Retail and Cost Review - Retail Inquiry</div>
	<div class="panel-body style-panel-body">
	
		<div id="contentApp" class="inquiry-retail">
			<div class="legend">
				<div class="accordion-toggle" data-toggle="collapse"  href="#filter">
							<span>&nbsp;&nbsp;&nbsp;&nbsp;Search Criteria</span>
				</div>
			</div>
			<div id="filter" class="row panel-collapse collapse in">
				<div class="col-sm-6">
					<div class="form-horizontal retail-form-style" role="form">
						<div class="form-group">
							<label class="col-md-3">Commodity</label>
	
							<div class="col-md-9">
								<input id="commodity-input" type="text" class="form-control input-sm" tabindex="1">
								<!-- /input-group -->
							</div>
						</div>
	
						<div class="form-group">
							<label class="col-md-3">Product</label>
	
							<div class="col-md-9">
								<input type="text" class="form-control input-sm" placeholder="" id="product-input" tabindex="3">
							</div>
						</div>
	
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-horizontal retail-form-style">
						<div class="form-group">
							<label class="col-md-3">Vendor</label>
	
							<div class="col-md-6">
								<select class="form-control input-sm" id="vendor-input" tabindex="2">
									<option></option>
								</select>
							</div>
						</div>
	
						<div class="btn-group pull-right  search-btn-wp">
							<button id="reset-retail-inquiry-btn" type="button" class="btn btn-default margin-right10"><span class="glyphicon glyphicon-refresh" tabindex="4"></span>&nbsp;&nbsp; Clear</button> 
							<button type="button" class="btn btn-primary" id="search-retail-inquiry-btn" tabindex="5"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Search</button>                    
	                    </div>
					</div>
				</div>
			</div>
			<div class="" id="searchResult">
				<span class="text-bold">Search Results</span>
			</div>
			<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist" id="tabResult">
					<li id="retail-inquiry-export-btn" class="export-excel" title="Export to Excel"></li>
				</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<!-- ALL TAB -->
				<div class="tab-pane common-content-tab active" id="all">
					<!-- table -->		
					<div class="table-responsive inquiry_detail">
						<table class="table table-condensed common-table-style table-hover table-striped table-bordered" id="inquiry_detail">
							<colgroup>
								<col class="no-group">
								<col class="upc-group">
								<col class="description-group">
								<col class="size-group">
								<col class="msrp-group">
								<col class="retail-group">
								<col class="cost-group">
								<col class="gp-per-group">
								<col class="gp-group">
								<col class="image-group">
							</colgroup>
							<thead>
								<tr class="firs-tr head-tr">
									<th>No</th>
									<th>UPC</th>
									<th>Description</th>
									<th>Size</th>
									<th>MSRP</th>
									<th>Retail</th>
									<th>Cost</th>
									<th>GP%</th>
									<th>GP$</th>
									<th>Image</th>
								</tr>
							</thead>
							
							<thead>
								<thead>
									<tr class="second-tr head-tr second-tr-retailInquiry">
										<th><button id="clear-filtter-btn-retailInquiry" type="button" class="btn btn-default btn-sm filter-btn-style" title="Clear the Filter"></button></th>
										<th><input id="search-upc-btn" type="text" class="form-control input-sm text-right pd0 input-number"/></th>
										<th><input id="search-description-btn" type="text" class="form-control input-sm text-left pd0"/></th>
										<th><input id="search-size-btn" type="text" class="form-control input-sm text-right pd0 input-decimal"/></th>
										<th><input id="search-msrp-btn" type="text" class="form-control input-sm text-right pd0 input-decimal"/></th>
										<th><input id="search-retail-btn" type="text" class="form-control input-sm text-right pd0 input-decimal"/></th>
										<th><input id="search-cost-btn" type="text" class="form-control input-sm text-right pd0 input-decimal"/></th>
										<th><input id="search-gp-btn" type="text" class="form-control input-sm text-right pd0 negative-number"/></th>
										<th><input id="search-penny-btn" type="text" class="form-control input-sm text-right pd0 negative-number"/></th>
										<th>
											<input id="search-image-btn" type="text" class="form-control input-sm text-right pd0 negative-number" disabled="true" />
											<!-- <select class="form-control input-sm" id="search-image-btn">
												<option>All</option>
												<option>Yes</option>
												<option>No</option>
											</select> -->
										</th>
										<th class="hidden"><input id="search-failed-btn-passed" type="text" class="form-control input-sm text-left pd0"/></th>
									</tr>
								</thead>
							<tbody class="body-table">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- end table -->
		</div>

	</div>
</div>
</div>