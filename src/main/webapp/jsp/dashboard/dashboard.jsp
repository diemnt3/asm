<%@page import="com.heb.enterprise.dsv.assortment.utils.ConstantsWeb"%>
<%@page import="com.heb.enterprise.dsv.assortment.utils.Convert"%>
<%@page import="com.heb.enterprise.dsv.assortment.utils.AConstants"%>
<%@page import="java.util.List"%>

<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="container-fluid container-override">
	<div class="panel panel-primary">
		<div class="panel-heading">
			DSV Global Defaults
		</div>
		<div id="chooseRulesDiv" class="panel-body" style="color: #22a1c8; display:none">
			<b>Please choose the rules below which will be applicable to all vendors unless otherwise changed explicitly for a vendor in the vendor rules management screen.</b>
		</div>
		<div class="panel-body style-panel-body">
				<div id="contentColLeft" class="col-sm-3 jstree-scoll">
					<div id="treeHie">
					  <!-- Tree data is displayed here -->
					</div>
				</div>
				<div id="contentColRight" class="col-sm-9" style="display: none">
					<div class="panel panel-default panel-border-none contentColRight-panel">
						<div class="panel-heading panel-border-none" style="border-bottom: 1px solid;">
					  		Please select the default for <label id="textRule">Sub Department</label> 
					  	</div>

					    <div class="panel-heading panel-border-none">
					  	 	<u> Assortment Rules </u>
					    </div>

					  <div class="panel-body padding-top0">
					    <form class="form-horizontal" role="form" id="formHie">
								<div class="form-group">
									<div class="col-sm-8 control-label text-left option-left">
										<input type="checkbox" id="turnOnAllRule"/>
					  					<span class="turnAllRule">Apply for All Rules</span>
									</div>
									<div class="col-sm-4 select-option">
										<div class="pull-right">
											<!--<input type="checkbox" class="my-checkbox" name="chk-${rule1.id}-value" data-right-check="0"> -->
										</div>
									</div>
								</div>
								<hr class="margin-top0 margin-bottom5"/>
								
								<div class="form-group">
									<div class="col-sm-8 control-label text-left option-left">
										
											<input type="checkbox" id="chk-${rule1.id}" name="chk-${rule1.id}" data-left-check="0"/> ${rule1.desc}
																												
									</div>
									<div class="col-sm-4 select-option">
										<div class="pull-right">
											<!--<input type="checkbox" class="my-checkbox" name="chk-${rule1.id}-value" data-right-check="0"> -->
										</div>
									</div>
								</div>
			
								<div class="form-group">
									<div class="col-sm-7 control-label text-left option-left">
										
										<input type="checkbox" id="chk-${rule2.id}" name="chk-${rule2.id}" data-left-check="7"/> ${rule2.desc}
										
									</div>
									<div class="col-sm-5">
										<div class="pull-right">
											<div class="row">
												<!-- <div class="col-xs-1 pull-right text-dollar">
														<h4><b></b></h4>
												
												</div> -->
												<div class="col-xs-5 pull-right" style="padding-left:0">
													<!-- <input id="retailEnd" type="text" class="form-control input-sm text-align input-decimal" value="0" placeholder="" data-right-check="7" tabindex="2"> -->
													<div class="input-group">
														<div class="input-group-addon">
															<i>$</i>
														</div>
														<input id="retailEnd" type="text" class="form-control input-sm text-align input-decimal" value="0" placeholder="" data-right-check="7" tabindex="2">
													</div>
												</div>
												<div class="col-xs-2 pull-right text-center text-and">
													<span><c:out value="and"></c:out></span>
													<input id="logicRule" type="hidden" value="AND">
												</div>
												<div class="col-xs-5 pull-right" style="padding-right:0">
													<!-- <input id="retailStart" type="text" class="form-control input-sm text-align input-decimal" value="0" placeholder="" data-right-check="7" tabindex="1"> -->
													<div class="input-group">
														<div class="input-group-addon">
															<i>$</i>
														</div>
														<input id="retailStart" type="text" class="form-control input-sm text-align input-decimal" value="0" placeholder="" data-right-check="7" tabindex="1">
													</div>
												</div>
			
											</div>
										</div>
									</div>
								</div>
			
								<div class="form-group">
									<div class="col-sm-7 control-label text-left option-left">
										<input type="checkbox" id="chk-${rule3.id}" name="chk-${rule3.id}" data-left-check="8"/> ${rule3.desc}
									</div>
									<div class="col-sm-5">
										<div class="pull-right">
											<div class="row">
												<!-- <div class="col-xs-1 pull-right text-per">
													<h4><b>%</b></h4>
												</div> -->
												<div class="col-xs-5 pull-right" style="padding-left:0">
													
													<div class="input-group">
														<input id="maginEnd" type="text" class="form-control input-sm text-align" value="0" placeholder="" data-right-check="8" tabindex="4">
														<div class="input-group-addon">
															<i>%</i>
														</div>														
													</div>
												</div>
												<div class="col-xs-2 pull-right text-center text-and">
													<span>and</span>
												</div>
												<div class="col-xs-5 pull-right" style="padding-right:0">
													<!-- <input id="maginStart" type="text" class="form-control input-sm text-align" value="0" placeholder="" data-right-check="8" tabindex="3"> -->
													<div class="input-group">
														<input id="maginStart" type="text" class="form-control input-sm text-align" value="0" placeholder="" data-right-check="8" tabindex="3">
														<div class="input-group-addon">
															<i>%</i>
														</div>														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
			
								<div class="form-group">
									<div class="col-sm-8 control-label text-left option-left">
										<input type="checkbox" id="chk-${rule4.id}" name="chk-${rule4.id}" data-left-check="1" /> ${rule4.desc}
									</div>
									<div class="col-sm-4 select-option">
										<div class="pull-right">
											<!--<input type="checkbox" class="my-checkbox" name="chk-${rule4.id}-value" data-right-check="1"/>-->
										</div>
									</div>
								</div>
			
								<div class="form-group">
									<div class="col-sm-8 control-label text-left option-left">
										<input type="checkbox" id="chk-${rule5.id}"  name="chk-${rule5.id}" data-left-check="2">
									${rule5.desc}
									</div>
									<div class="col-sm-4 select-option">
										<div class="pull-right">
											<!--<input type="checkbox" class="my-checkbox" data-right-check="2" name="chk-${rule5.id}-value"/> -->
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-8 control-label text-left option-left">
										<input type="checkbox" id="chk-${rule6.id}" name="chk-${rule6.id}" data-left-check="3">
								${rule6.desc}
									</div>
									<div class="col-sm-4 select-option">
										<div class="pull-right">
											<!--<input type="checkbox" class="my-checkbox" data-right-check="3" name="chk-${rule6.id}-value"> -->
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-8 control-label text-left option-left">
										<input type="checkbox" id="chk-${rule7.id}" name="chk-${rule7.id}" data-left-check="4">
								${rule7.desc}
									</div>
									<div class="col-sm-4 select-option">
										<div class="pull-right">
											<!--<input type="checkbox" class="my-checkbox" name="chk-${rule7.id}-value" data-right-check="4"> -->
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-8 control-label text-left option-left">
										<input type="checkbox" id="chk-${rule8.id}" name="chk-${rule8.id}" data-on-color="info"
								data-off-color="danger" data-left-check="5">${rule8.desc}
									</div>
									<div class="col-sm-4 select-option">
										<div class="pull-right">
											<!--<input type="checkbox" class="my-checkbox" data-right-check="5" name="chk-${rule8.id}-value"/> -->
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-8 control-label text-left option-left">
										<input type="checkbox" id="chk-${rule9.id}" name="chk-${rule9.id}" data-left-check="6">
											${rule9.desc}
									</div>
									<div class="col-sm-4 select-option">
										<div class="pull-right">
											<!--<input type="checkbox" class="my-checkbox" data-right-check="6" name="chk-${rule9.id}-value"/> -->
										</div>
									</div>
								</div>
								<!--<div class="form-group">
									<div class="col-sm-10 control-label text-left option-left">
										<input type="checkbox" id="chk-${rule10.id}" name="chk-${rule10.id}" data-left-check="10">
										${rule10.desc} 
										 
									</div>
									<div class="col-sm-2 select-option">										
										<div class="pull-right">
											<div class="row">
												<div class="col-xs-2 pull-right text-per">
													<h4><b>%</b></h4>
												</div>
												<div class="col-xs-10 pull-right">
													<input id="rule10" type="text" class="form-control input-sm text-align" data-right-check="10" value="0" placeholder="" tabindex="5"/>
												</div>												
											</div>
										</div>
									</div>
								</div> -->
			
								<div class="form-group style-save-global">									
									<div class="col-sm-9">
									</div>
									<div class="col-sm-3 text-right padding-left0 padding-right0">
										<button id="reset-toDefault-btn" type="button" class="btn btn-default margin-right10" title="Reset Rules to Higher Level" tabindex="6">
											<span class="glyphicon glyphicon-refresh" ></span>&nbsp;Reset
										</button> 
										<button id="global-save-btn" type="button" class="btn btn-danger" tabindex="7" title="Save">
											<span class="glyphicon glyphicon-floppy-disk"></span>
											&nbsp;Save
										</button>
									</div>
								</div>
							
						</form>
					  </div>
					</div>
				</div>

<!-- 			</div> -->
		</div>
		<div id="showUserAndTimeUpdate" class="panel-body" style="color: #22a1c8; display:none">
			<div class="col-sm-3" >								
			</div>
			<div class="col-sm-9 userAndTimeUpdateStyleDiv">
				<div class="row">
					<div class="col-sm-12 userAndTimeUpdateStyle" id="uidAndTsUpdate">								
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>


<div id="alert-msg" class="notifications top-right" style="display: none;">
	<div id="message" class="alert in fade alert-danger">There is no change.</div>
</div>