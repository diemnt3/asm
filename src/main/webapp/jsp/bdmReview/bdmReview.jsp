<div class="container-fluid container-override">
	<div class="panel panel-primary">
		<div class="panel-heading">
			Products for Review
		</div>
		
		<div class="panel-body style-panel-body">
				<div id="contentApp">
				<div class="legend">
					<span class="accordion-toggle-bdm" data-toggle="collapse" href="#bdm-header"><i id="fa-angle-double-down-style" class="fa fa-angle-double-down" style="width: 15px;"></i>Search Criteria</span>
				</div>
				<div id="alert-bdm-form" class="col-md-12 margin-top10 hidden">
	                  	<div class="alert alert-danger margin-bottom10" role="alert">
					      	<span id="message-bdm"></span>
					    </div>
				</div>
				<div id="bdm-header" class="row collapse in">
					<div class="col-sm-6">
						<div id="bdm-search-form" class="form-horizontal bdm-form-style" role="form">
							<div class="form-group">
								<label class="col-md-4">Commodity</label>
		
								<div class="col-md-8">
									<input id="commodities-input" type="text" class="form-control input-sm" tabindex="1">
									<!-- /input-group -->
								</div>
							</div>
		
							<div class="form-group">
								<label class="col-md-4">Brand</label>
		
								<div class="col-md-8">
									<input id="brand-input" type="text" class="form-control input-sm" placeholder="" tabindex="3">
								</div>
							</div>
		
							<div class="form-group">
								<label class="col-md-4">Receive Date Between</label>
		
								<div class="col-md-8">
									<div class="row text-center">
										<div class="col-md-5">
											<div class="input-group">
												<input class="form-control input-sm" type="text" placeholder=""
													id="dateBegin" tabindex="5">
												<div class="input-group-addon" id="dateBegin-icon-wp">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
										<label class="col-md-1  text-per">and</label>										
										<div class="col-md-5">
											<div class="input-group receive-date-bw-wp">
												<input class="form-control input-sm" type="text" placeholder=""
													id="dateEnd" tabindex="6">
												<div class="input-group-addon" id="dateEnd-icon-wp">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
		
							<div class="form-group">
								<label class="col-md-4">Margin Between</label>
		
								<div class="col-md-8">
									<div class="row text-center">
										<div class="col-md-4">
											<input id="marginBegin" type="text" class="form-control input-sm negative-number" tabindex="8">
										</div>
										<label class="col-md-1  text-per">and</label>
										<div class="col-md-4">
											<input id="marginEnd" type="text" class="form-control input-sm negative-number" tabindex="9">
										</div>
									</div>
								</div>
							</div>
		
							<div class="form-group">
								<label class="col-md-4">Failed Reason</label>
								<div class="col-md-6">
									<select class="form-control input-sm" id="failed-input" tabindex="11">
										<option></option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-horizontal  bdm-form-style">
							<div class="form-group">
								<label class="col-md-2">Vendor</label>
		
								<div class="col-md-6">
									<select class="form-control input-sm" id="vendor-input" tabindex="2">
										<option></option>
									</select>
									<!-- <input id="vendor-input" type="text" class="form-control input-sm" tabindex="2"> -->
								</div>
							</div>
		
							<div class="form-group">
								<label class="col-md-2">Product</label>
		
								<div class="col-md-9">
									<input id="product-input" type="text" class="form-control input-sm" placeholder="" tabindex="4" maxlength="255">
								</div>
							</div>
		
							<div class="form-group">
								<label class="col-md-2"></label>
								<div class="col-md-9">
									<label class="common-check"> <input id="show-all-map-check" type="checkbox" tabindex="7" class="input-check-box">
										Only MAP products
									</label>
								</div>
							</div>
		
							<div class="form-group">
								<label class="col-md-2"></label>
								<div class="col-md-9">
									<label class="common-check label-custom"> <input id="show-only-prePrice-check" type="checkbox" tabindex="10" class="input-check-box">
										Only pre-price products
									</label>
								</div>
							</div>
							<div class="btn-group pull-right search-btn-wp">
								<button id="reset-new-product-btn" type="button" class="btn btn-default margin-right10"><span class="glyphicon glyphicon-refresh" tabindex="12"></span>&nbsp;&nbsp; Clear</button>                          
									<button id="search-new-product-btn" type="button" class="btn btn-primary" tabindex="13"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Search</button>                          
<!-- 		                       <button type="button" class="btn btn-primary"> -->
<!-- 		                         <span class="glyphicon glyphicon-search"></span> -->
<!-- 		                       </button> -->
<!-- 		                       <button id="search-new-product-btn" type="button" class="btn btn-primary">Search</button> -->
		                     </div>
						</div>
					</div>
				</div>
				<div class="" id="searchResult">
					<span class="text-bold">Search Results</span>
				</div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist" id="tabResult">
					<li id="tab-all-li"  class="active" tabindex="14"><a id="tab-all-title" href="#all" role="tab" data-toggle="tab" value="all">All <span id="badge-all" class="badge-all-style">(0)</span></a></li>
					<li id="tab-passed-li" tabindex="15"><a id="tab-passed-title" href="#passed" role="tab" data-toggle="tab" value="passed">Passed <span id="badge-passed" class="badge-passed-style">(0)</span></a></li>
					<li id="tab-failed-li" tabindex="16"><a href="#failed" role="tab" data-toggle="tab" value="failed">Failed <span id="badge-failed" class="badge-failed-style">(0)</span></a></li>
					<li id="tab-rejected-li" tabindex="17"><a href="#rejected" role="tab" data-toggle="tab" value="rejected">Rejected <span id="badge-rejected" class="badge-rejected-style">(0)</span></a></li>
					<li id="product-review-export-btn" class="export-excel" title="Export to Excel"></li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<div id="alert-bdm" class="col-md-12 margin-top10 hidden">
	                  	<div class="alert alert-danger margin-bottom10" role="alert">
					      	<span id="message-bdm"></span>
					    </div>
				   </div>
				<!-- ALL TAB -->
					<div class="tab-pane common-content-tab active" id="all">				
						<div class="table-responsive">
							<table id="all-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
								<colgroup>
									<col class="check-all-group">
									<col class="no-group">
									<col class="upc-group">
									<col class="description-group">
									<col class="size-group">
									<col class="map-group">
									<col class="pre-price-group">
									<col class="msrp-group">
									<col class="srp-group">
									<col class="cost-group">
									<col class="gp-group">
									<col class="penny-profit-group">
									<col class="heb-retail-group">
									<col class="image-group">
									<col class="failed-reason-group">
									<col class="review-status-group hidden">
									<col class="status-group">
							    	<col class="first-received-group">
							    	<col class="last-edited-group ">
								</colgroup>
							 	
								<thead>
									<tr class="firs-tr head-tr">
		 								<th><input type="checkbox" class="check-all"></th>
										<th>S. No.</th>
										<th>UPC</th>
										<th>Description</th>
										<th>Size</th>
										<th>MAP</th>
										<th>Pre</br>Price</th>
										<th>MSRP</th>
										<th>SRP</th>
										<th>Cost</th>
										<th class="comfirm-filter">GP%</th>
										<th class="comfirm-filter">GP$</th>
										<th class="comfirm-filter">HEB</br>Retail</th>
										<th>Image</th>
										<th>Failed Reason</th>
										<th class="hidden">Review Status</th>
										<th>Status</th>
										<th>First</br>Received TS</th>
										<th>Last</br>Edited TS</th>
									</tr>
								</thead>
								
								<thead>
									<tr class="second-tr head-tr second-tr-all">
										<!--<i id="clear-filtter-icon" class="glyphicon glyphicon-remove clear-filtter-style"></i> -->
										<th><button id="clear-filtter-btn-all" type="button" class="btn btn-default btn-sm filter-btn-style" title="Clear the Filter"></button></th>
										<th><input id="search-no-btn-all" type="text" class="input-number form-control input-sm text-right pd0 disabled-input-all"/></th>
										<th><input id="search-upc-btn-all" type="text" class="input-number form-control input-sm text-right pd0"/></th>
										<th><input id="search-description-btn-all" type="text" class="form-control input-sm text-left pd0" maxlength="255"/></th>
										<th><input id="search-size-btn-all" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-map-btn-all" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-prePrice-btn-all" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-msrp-btn-all" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-srp-btn-all" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-cost-btn-all" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-gp-btn-all" type="text" class="form-control input-sm text-right pd0  confirm-filter negative-number" tmp=""/></th>
										<th><input id="search-penny-btn-all" type="text" class="form-control input-sm text-right pd0 confirm-filter negative-number" tmp=""/></th>
										<th><input id="search-heb-retail-btn-all" type="text" class="form-control input-sm text-right pd0 input-decimal confirm-filter" tmp=""/></th>
										<th><input id="search-image-btn-all" type="text" class="form-control input-sm text-center pd0 disabled-input-all"/></th>
										<th>
										 <select id="search-failed-btn-all" class="form-control input-sm text-left pd0"></select></th>
										<th><input id="search-status-btn-all" type="text" class="form-control input-sm text-right pd0 disabled-input-all"/></th>
										<th>
											<label id="search-first-received-btn-all" class="fa fa-calendar icon-calendar">
												<input  type="text" class="form-control input-sm text-right pd0 from-date-val hidden"/>
												<input type="text" class="form-control input-sm text-right pd0 to-date-val hidden"/>
											</label>
										</th>
										<th>
											<label id="search-last-edited-btn-all" class="fa fa-calendar icon-calendar">
												<input type="text" class="form-control input-sm text-right pd0 from-date-val hidden"/>
												<input type="text" class="form-control input-sm text-right pd0 to-date-val hidden"/>
											</label>
										</th>
									</tr>
								</thead>
							</table>
						</div>
					<!-- 	<div id = "grp-btn-all" class="clearfix padding-top20">
							<button id="save-btn-all" value="save" class="btn btn-danger btn-danger-save pull-right" disabled="true"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Save</button>
							<button id="un-reject-btn-all" value="un-rejected" class="btn btn-danger btn-danger-rejected pull-right " disabled="true">Un-reject</button>
							<button id="approve-btn-all" value="approved" class="btn btn-danger pull-right" disabled="true">Approve</button>
							<button id="reject-btn-all" value="rejected" class="btn btn-danger btn-danger-rejected pull-right" disabled="true">Reject</button>
						</div> -->
					</div>
					<!-- PASSED TAB -->
					<div class="tab-pane common-content-tab" id="passed">				
						<div class="table-responsive">
							<table id="passed-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
								<colgroup>
							    	<col class="check-all-group">
									<col class="no-group">
							    	<col class="upc-group">
							    	<col class="description-group">
							    	<col class="size-group">
							    	<col class="map-group">
							    	<col class="pre-price-group">
							    	<col class="msrp-group">
							    	<col class="srp-group">
							    	<col class="cost-group">
							    	<col class="gp-group">
							    	<col class="penny-profit-group">
							    	<col class="heb-retail-group">
							    	<col class="image-group">
							    	<col class="failed-reason-group hidden">
							    	<col class="review-status-group hidden">
							    	<col class="status-group">
							    	<col class="first-received-group">
							    	<col class="last-edited-group ">
							 	</colgroup>
							 	
								<thead>
									<tr class="firs-tr head-tr">
										<th><input type="checkbox" class="check-all"></th>
										<th>S. No.</th>
										<th>UPC</th>
										<th>Description</th>
										<th>Size</th>
										<th>MAP</th>
										<th>Pre</br>Price</th>
										<th>MSRP</th>
										<th>SRP</th>
										<th>Cost</th>
										<th class="comfirm-filter">GP%</th>
										<th class="comfirm-filter">GP$</th>
										<th class="comfirm-filter">HEB</br>Retail</th>
										<th>Image</th>
										<th class="hidden">Failed Reason</th>
										<th class="hidden">Review Status</th>
										<th>Status</th>
										<th>First</br>Received TS</th>
										<th>Last</br>Edited TS</th>
									</tr>
								</thead>
								
								<thead>
									<tr class="second-tr head-tr second-tr-passed">
										<!--<i id="clear-filtter-icon" class="glyphicon glyphicon-remove clear-filtter-style"></i> -->
										<th><button id="clear-filtter-btn-passed" type="button" class="btn btn-default btn-sm filter-btn-style" title="Clear the Filter"></button></th>
										<th><input id="search-no-btn-passed" type="text" class="input-number form-control input-sm text-right pd0 disabled-input-passed"/></th>
										<th><input id="search-upc-btn-passed" type="text" class="input-number form-control input-sm text-right pd0"/></th>
										<th><input id="search-description-btn-passed" type="text" class="form-control input-sm text-left pd0" maxlength="255"/></th>
										<th><input id="search-size-btn-passed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-map-btn-passed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-prePrice-btn-passed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-msrp-btn-passed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-srp-btn-passed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-cost-btn-passed" type="text" class="input-decimal form-control input-sm text-right pd0 "/></th>
										<th><input id="search-gp-btn-passed" type="text" class="form-control input-sm text-right pd0  confirm-filter negative-number" tmp=""/></th>
										<th><input id="search-penny-btn-passed" type="text" class="form-control input-sm text-right pd0 confirm-filter negative-number" tmp=""/></th>
										<th><input id="search-heb-retail-btn-passed" type="text" class="form-control input-sm text-right pd0 input-decimal confirm-filter" tmp=""/></th>
										<th><input id="search-image-btn-passed" type="text" class="form-control input-sm text-center pd0 disabled-input-passed"/></th>
										<th class="hidden"><!-- <input id="search-failed-btn-passed" type="text" class="form-control input-sm text-center pd0"/> -->
										 <select id="search-failed-btn-passed" class="form-control input-sm text-left pd0"></select></th>
										<th><input id="search-status-btn-passed" type="text" class="form-control input-sm text-right pd0 disabled-input-passed"/></th>
										<th>
											<label id="search-first-received-btn-passed" class="fa fa-calendar icon-calendar">
												<input  type="text" class="form-control input-sm text-right pd0 from-date-val hidden"/>
												<input type="text" class="form-control input-sm text-right pd0 to-date-val hidden"/>
											</label>
										</th>
										<th>
											<label id="search-last-edited-btn-passed" class="fa fa-calendar icon-calendar">
												<input type="text" class="form-control input-sm text-right pd0 from-date-val hidden"/>
												<input type="text" class="form-control input-sm text-right pd0 to-date-val hidden"/>
											</label>
										</th>
									</tr>
								</thead>
							</table>
						</div>
					<!-- 	<div id = "grp-btn-passed" class="clearfix padding-top20"> 
							<button id="save-btn-passed" value="save" class="btn btn-danger btn-danger-save pull-right" disabled="true"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Save</button>
							<button id="approve-btn-passed" value="approved" class="btn btn-danger pull-right " disabled="true">Approve</button>
							<button id="reject-btn-passed"  value="rejected" class="btn btn-danger btn-danger-rejected pull-right" disabled="true">Reject</button>
						</div> -->
					</div>
					<!-- FAILED TAB -->
					<div class="tab-pane common-content-tab" id="failed">				
						<div class="table-responsive">
							<table id="failed-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
								<colgroup>
									<col class="check-all-group">
									<col class="no-group">
									<col class="upc-group">
									<col class="description-group">
									<col class="size-group">
									<col class="map-group">
									<col class="pre-price-group">
									<col class="msrp-group">
									<col class="srp-group">
									<col class="cost-group">
									<col class="gp-group">
									<col class="penny-profit-group">
									<col class="heb-retail-group">
									<col class="image-group">
									<col class="failed-reason-group">
									<col class="review-status-group hidden">
									<col class="status-group">
							    	<col class="first-received-group">
							    	<col class="last-edited-group ">
								</colgroup>
								
								<thead>
									<tr class="firs-tr head-tr">
							 			<th><input type="checkbox" class="check-all"></th> 
										<th>S. No.</th>
										<th>UPC</th>
										<th>Description</th>
										<th>Size</th>
										<th>MAP</th>
										<th>Pre</br>Price</th>
										<th>MSRP</th>
										<th>SRP</th>
										<th>Cost</th>
										<th class="comfirm-filter">GP%</th>
										<th class="comfirm-filter">GP$</th>
										<th class="comfirm-filter">HEB</br>Retail</th>
										<th>Image</th>
										<th>Failed Reason</th>
										<th class="hidden">Review Status</th>
										<th>Status</th>
										<th>First</br>Received TS</th>
										<th>Last</br>Edited TS</th>
									</tr>
								</thead>
								
								<thead>
									<tr class="second-tr head-tr second-tr-failed">
										<!--<i id="clear-filtter-icon" class="glyphicon glyphicon-remove clear-filtter-style"></i> -->
										<th><button id="clear-filtter-btn-failed" type="button" class="btn btn-default btn-sm filter-btn-style" title="Clear the Filter"></button></th>
										<th><input id="search-no-btn-failed" type="text" class="input-number form-control input-sm text-right pd0 disabled-input-failed"/></th>
										<th><input id="search-upc-btn-failed" type="text" class="input-number form-control input-sm text-right pd0"/></th>
										<th><input id="search-description-btn-failed" type="text" class="form-control input-sm text-left pd0" maxlength="255"/></th>
										<th><input id="search-size-btn-failed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-map-btn-failed" type="text" class="input-decimal  form-control input-sm text-right pd0"/></th>
										<th><input id="search-prePrice-btn-failed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-msrp-btn-failed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-srp-btn-failed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-cost-btn-failed" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-gp-btn-failed" type="text" class="form-control input-sm text-right pd0 confirm-filter negative-number" tmp=""/></th>
										<th><input id="search-penny-btn-failed" type="text" class="form-control input-sm text-right pd0  confirm-filter negative-number" tmp=""/></th>
										<th><input id="search-heb-retail-btn-failed" type="text" class="form-control input-sm text-right input-decimal confirm-filter" tmp=""/></th>
										<th><input id="search-image-btn-failed" type="text" class="form-control input-sm text-center pd0 disabled-input-failed"/></th>
										<th><!-- <input id="search-failed-btn-failed" type="text" class="form-control input-sm text-center pd0"/> -->
										<select id="search-failed-btn-failed" class="form-control input-sm text-left pd0"></select> </th>
										<th><input id="search-status-btn-failed" type="text" class="form-control input-sm text-right pd0 disabled-input-failed"/></th>
										<th>
											<label id="search-first-received-btn-failed" class="fa fa-calendar icon-calendar">
												<input  type="text" class="form-control input-sm text-right pd0 from-date-val hidden"/>
												<input type="text" class="form-control input-sm text-right pd0 to-date-val hidden"/>
											</label>
										</th>
										<th>
											<label id="search-last-edited-btn-failed" class="fa fa-calendar icon-calendar">
												<input type="text" class="form-control input-sm text-right pd0 from-date-val hidden"/>
												<input type="text" class="form-control input-sm text-right pd0 to-date-val hidden"/>
											</label>
										</th>
									</tr>
								</thead>
							</table>
						</div>
						<!-- <div id = "grp-btn-failed" class="clearfix padding-top20">
							<button id="save-btn-failed" value="save" class="btn btn-danger btn-danger-save pull-right" disabled="true"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Save</button>
							<button id="approve-btn-failed" value="approved" class="btn btn-danger pull-right" disabled="true">Approve</button>
							<button id="reject-btn-failed" value="rejected" class="btn btn-danger btn-danger-rejected pull-right" disabled="true">Reject</button>
						</div> -->
					</div>
					
					<!-- INJECT TAB -->
					<div class="tab-pane common-content-tab" id="rejected">
						<div class="table-responsive">
							<table id="rejected-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
								<colgroup>
									<col class="check-all-group">
									<col class="no-group">
									<col class="upc-group">
									<col class="description-group">
									<col class="size-group">
									<col class="map-group">
									<col class="pre-price-group">
									<col class="msrp-group">
									<col class="srp-group">
									<col class="cost-group">
									<col class="gp-group">
									<col class="penny-profit-group">
									<col class="heb-retail-group">
									<col class="image-group">
									<col class="failed-reason-group">
									<col class="review-status-group hidden">
									<col class="status-group">
							    	<col class="first-received-group">
							    	<col class="last-edited-group ">
								</colgroup>
								
								<thead>
									<tr class="firs-tr head-tr">
		 								<th><input type="checkbox" class="check-all"></th> 
										<th>S. No.</th>
										<th>UPC</th>
										<th>Description</th>
										<th>Size</th>
										<th>MAP</th>
										<th>Pre</br>Price</th>
										<th>MSRP</th>
										<th>SRP</th>
										<th>Cost</th>
										<th class="comfirm-filter">GP%</th>
										<th class="comfirm-filter">GP$</th>
										<th class="comfirm-filter">HEB</br>Retail</th>
										<th>Image</th>
										<th>Failed Reason</th>
										<th class="hidden">Review Status</th>
										<th>Status</th>
										<th>First</br>Received TS</th>
										<th>Last</br>Edited TS</th>
									</tr>
								</thead>
								
								<thead>
									<tr class="second-tr head-tr second-tr-rejected">
										<!--<i id="clear-filtter-icon" class="glyphicon glyphicon-remove clear-filtter-style"></i> -->
										<th><button id="clear-filtter-btn-rejected" type="button" class="btn btn-default btn-sm filter-btn-style" title="Clear the Filter"></button></th>
										<th><input id="search-no-btn-rejected" type="text" class="input-number form-control input-sm text-right pd0 disabled-input-rejected"/></th>
										<th><input id="search-upc-btn-rejected" type="text" class="input-number form-control input-sm text-right pd0"/></th>
										<th><input id="search-description-btn-rejected" type="text" class="form-control input-sm text-left pd0" maxlength="255"/></th>
										<th><input id="search-size-btn-rejected" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-map-btn-rejected" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-prePrice-btn-rejected" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-msrp-btn-rejected" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-srp-btn-rejected" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-cost-btn-rejected" type="text" class="input-decimal form-control input-sm text-right pd0"/></th>
										<th><input id="search-gp-btn-rejected" type="text" class="form-control input-sm text-right pd0 confirm-filter negative-number" tmp=""/></th>
										<th><input id="search-penny-btn-rejected" type="text" class="form-control input-sm text-right pd0 confirm-filter negative-number" tmp=""/></th>
										<th><input id="search-heb-retail-btn-rejected" type="text" class="form-control input-sm text-right input-decimal confirm-filter" tmp=""/></th>
										<th><input id="search-image-btn-rejected" type="text" class="form-control input-sm text-center pd0 disabled-input-rejected"/></th>
										<th><!-- <input id="search-failed-btn-rejected" type="text" class="form-control input-sm text-center pd0"/> -->
										<select id="search-failed-btn-rejected" class="form-control input-sm text-left pd0"></select> </th>
										<th><input id="search-status-btn-rejected" type="text" class="form-control input-sm text-right pd0 disabled-input-rejected"/></th>
										<th>
											<label id="search-first-received-btn-rejected" class="fa fa-calendar icon-calendar">
												<input  type="text" class="form-control input-sm text-right pd0 from-date-val hidden"/>
												<input type="text" class="form-control input-sm text-right pd0 to-date-val hidden"/>
											</label>
										</th>
										<th>
											<label id="search-last-edited-btn-rejected" class="fa fa-calendar icon-calendar">
												<input type="text" class="form-control input-sm text-right pd0 from-date-val hidden"/>
												<input type="text" class="form-control input-sm text-right pd0 to-date-val hidden"/>
											</label>
										</th>
									</tr>
								</thead>
							</table>
						</div>
						<!-- <div id = "grp-btn-rejected" class="clearfix padding-top20">
							<button id="save-btn-rejected" value="save" class="btn btn-danger btn-danger-save pull-right" disabled="true"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Save</button>
							<button id="un-reject-btn-rejected" value="un-rejected" class="btn btn-danger btn-danger-rejected pull-right " disabled="true">Un-reject</button>
						</div> -->
					</div>

				</div>
				<div id = "grp-btn" class="clearfix">
					<button id="save-btn" value="save" class="btn btn-danger btn-danger-save pull-right margin-left10" disabled="true"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Save</button>
					<!-- <button id="reset-btn-all" value="reset" type="button" class="btn btn-default  pull-right" disabled="true"><span class="glyphicon glyphicon-refresh" tabindex="12"></span>&nbsp;&nbsp; Reset</button> -->
					<button id="un-reject-btn" value="un-rejected" class="btn btn-danger btn-danger-rejected pull-right margin-left10" disabled="true">Un-reject</button>
					<button id="approve-btn" value="approved" class="btn btn-danger pull-right margin-left10" disabled="true">Approve</button>
					<button id="reject-btn" value="rejected" class="btn btn-danger btn-danger-rejected pull-right" disabled="true">Reject</button>
				</div>
			</div>
		</div><!-- end body -->
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="datefilterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
        <div class="modal-content">
	        <div class="modal-header modal-header-custom">
	            <div class="form-group" role="form">
	            	<button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title modal-title-custom" id="myModalLabel">Filter Options</h4>
	            </div>
	        </div>
	        <div class="modal-body">
		        <form role="form" action="#">
		        	<div class="form-group">
		        		<label class="col-md-12">Filter by the time range the tasks start.<hr width="100%"></label>

		        	</div>
		         	<div class="form-group">
		         		<label class="col-md-3">From: </label>
		               	<div class="input-group col-md-6">
							<input class="form-control input-sm" type="text" placeholder=""
								id="from-date-modal">
							<div class="input-group-addon" id="from-date-modal-icon-wp">
								<i class="fa fa-calendar"></i>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">To: </label>
		               	<div class="input-group col-md-6">
							<input class="form-control input-sm" type="text" placeholder=""
								id="to-date-modal">
							<div class="input-group-addon" id="to-date-modal-icon-wp">
								<i class="fa fa-calendar"></i>
							</div>
						</div>
		            </div>
		        </form>
	        </div>
	        <div class="modal-footer">
	            <button type="button" class="btn-ok-modal">OK</button>
	            <button type="button" class="btn-cancel-modal">Cancel</button>
	        </div>
      	</div>
   </div>
</div>

<!--[if IE]>
	<style>
		#passed-table .text-ellipsis{
			width: 150px;
			padding: 0 !important;
		}
		#passed-table .text-ellipsis span{
			display: table-caption;
			word-break: break-all;
			overflow: hidden;
			text-overflow: ellipsis;
			margin: 0 !important;
			width: inherit;
		}
	</style>
<![endif]-->


<!-- <div class="container-fluid"> -->
<!-- 	<h4 class="common-title">New Products for Review</h4> -->
	
<!-- </div> -->