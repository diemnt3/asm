<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container-fluid">
	<div id="downloadAlertMessage" class="text-center">
		<br/>
		<h3 id="loadingMessage" style="display: none">
			<img src="/assortment/image/loading.gif">&nbsp;&nbsp;Please wait a moment for the system to generate the file...
		</h3>
		<h3 id="successMessage" style="display: none">
		
			<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Success! The file is ready to download.
			<br/><br/>
			<button id="download-btn" class="btn btn-default btn-lg">
				<span class="glyphicon glyphicon-save"></span>&nbsp;Download
			</button>
		</h3>
		<h3 id="errorMessage" style="display: none">
			<span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;&nbsp;Error! Cannot generate the file.
		</h3>
	</div>
</div>
<input id="nameProcess" type="hidden" value="${nameProcess}">
	<c:choose>
		<c:when test="${nameProcess == 'new-product-review-export-process'}">
			<form id="frmExcel" action="/assortment/new-products/create-excel-file" method="POST">
				<input type="hidden" name="start" value="${prodSearchModel.start}">
				<input type="hidden" name="length" value="${prodSearchModel.length}">
				<input type="hidden" name="commodittyID" value="${prodSearchModel.commodittyID}">
				<input type="hidden" name="brand" value="${prodSearchModel.brand}">
				<input type="hidden" name="dateBegin" value="${prodSearchModel.dateBegin}">
				<input type="hidden" name="dateEnd" value="${prodSearchModel.dateEnd}">
				<input type="hidden" name="marginBegin" value="${prodSearchModel.marginBegin}">
				<input type="hidden" name="marginEnd" value="${prodSearchModel.marginEnd}">
				<input type="hidden" name="failReason" value="${prodSearchModel.failReason}">
				<input type="hidden" name="vendorID" value="${prodSearchModel.vendorID}">
				<input type="hidden" name="prodId" value="${prodSearchModel.prodId}">
	<%-- 		<input type="hidden" name="showAllMapCheck" value="${prodSearchModel.showAllMapCheck}"> --%>
	<%-- 		<input type="hidden" name="showOnlyPrePriceCheck" value="${prodSearchModel.showOnlyPrePriceCheck}"> --%>
				<input type="hidden" name="upcIdFilter" value="${prodSearchModel.upcIdFilter}">
				<input type="hidden" name="prodDescFilter" value="${prodSearchModel.prodDescFilter}">
				<input type="hidden" name="prodSizeFilter" value="${prodSearchModel.prodSizeFilter}">
				<input type="hidden" name="mapFilter" value="${prodSearchModel.mapFilter}">
				<input type="hidden" name="msrpFilter" value="${prodSearchModel.msrpFilter}">
				<input type="hidden" name="srpFilter" value="${prodSearchModel.srpFilter}">
				<input type="hidden" name="costFilter" value="${prodSearchModel.costFilter}">
				<input type="hidden" name="gpFilter" value="${prodSearchModel.gpFilter}">
				<input type="hidden" name="pennyProfitFilter" value="${prodSearchModel.pennyProfitFilter}">
				<input type="hidden" name="imageFilter" value="${prodSearchModel.imageFilter}">
				<input type="hidden" name="prePriceFilter" value="${prodSearchModel.prePriceFilter}">
				<input type="hidden" name="failReasonFilter" value="${prodSearchModel.failReasonFilter}">
				<input type="hidden" name="tabActive" value="${tabActive}">
			</form>
		</c:when>
		
		<c:when test="${nameProcess == 'invoice-list-export-process'}">
			<form id="frmExcel" action="/assortment/invoice-match/create-excel-file" method="POST">
				<input type="hidden" name="start" value="${invoiceMl.start}">
				<input type="hidden" name="length" value="${invoiceMl.length}">
				<input type="hidden" name="vendor" value="${invoiceMl.vendor}">
				<input type="hidden" name="invoiceStatus" value="${invoiceMl.invoiceStatus}">
				<input type="hidden" name="invoiceDateBegin" value="${invoiceMl.invoiceDateBegin}">
				<input type="hidden" name="invoiceDateEnd" value="${invoiceMl.invoiceDateEnd}">
				<input type="hidden" name="po" value="${invoiceMl.po}">
				<input type="hidden" name="invoiceId" value="${invoiceMl.invoiceId}">
			</form>
		</c:when>
		
		<c:when test="${nameProcess == 'invoice-details-list-export-process'}">
			<form id="frmExcel" action="/assortment/invoice-details/create-excel-file" method="POST">
				<input type="hidden" name="invoiceId" value="${invoiceId}">
			</form>
		</c:when>
		
		<c:when test="${nameProcess == 'retail-inquiry-export-process'}">
			<form id="frmExcel" action="/assortment/retail-inquiry/create-excel-file" method="POST">
				<input type="hidden" name="start" value="${retailSearchModel.start}">
				<input type="hidden" name="length" value="${retailSearchModel.length}">
				<input type="hidden" name="commmodityId" value="${retailSearchModel.commmodityId}">
				<input type="hidden" name="vendorId" value="${retailSearchModel.vendorId}">
				<input type="hidden" name="prodId" value="${retailSearchModel.prodId}">
	<%-- 		<input type="hidden" name="showAllMapCheck" value="${prodSearchModel.showAllMapCheck}"> --%>
	<%-- 		<input type="hidden" name="showOnlyPrePriceCheck" value="${prodSearchModel.showOnlyPrePriceCheck}"> --%>
				<input type="hidden" name="upc" value="${retailSearchModel.upc}">
				<input type="hidden" name="prodDesc" value="${retailSearchModel.prodDesc}">
				<input type="hidden" name="prodSize" value="${retailSearchModel.prodSize}">
				<input type="hidden" name="msrp" value="${retailSearchModel.msrp}">
<%-- 			<input type="hidden" name="srpFilter" value="${prodSearchModel.retail}"> --%>
				<input type="hidden" name="cost" value="${retailSearchModel.cost}">
				<input type="hidden" name="gp" value="${retailSearchModel.gp}">
				<input type="hidden" name="pennyProfit" value="${retailSearchModel.pennyProfit}">
				<input type="hidden" name="image" value="${retailSearchModel.image}">
			</form>
		</c:when>
		
	</c:choose>

