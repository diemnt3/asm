<div class="container-fluid container-override">
<div class="panel panel-primary">
	<div class="panel-heading">Invoice Matching Rules</div>
	<div class="panel-body style-panel-body" id="vendor-rules">
	  
<!--      <form class="form-horizontal" role="form" id="formVendorSearch"> -->
	     <div class="row vendor-search form-group">
		     <div class="col-sm-6 margin-top10">
		        <label class="col-sm-2 style-header">Vendor</label>
		        <div class="col-sm-10 inputGroupContainer">
		            <div class="input-group">
		                <input id="vendor-input" type="text" name="vendor" class="form-control col-sm-8 vendor-search txt-search-vendor-header input-sm">
		                <div class="wrap-caret"><span class="caret addon"></span></div>
<!-- 		                <span class="input-group-addon btn-search-vendor-header"><i class="glyphicon glyphicon-search"></i></span> -->
		            </div>
		        </div>
		    </div>
	    </div>
    
<!--      </form> -->

     <form class="form-horizontal" role="form" id="formVendorRules">
	     
	     <div id="rules-result">
	     
	     	<h5>Vendor Attributes</h5>
	        <div id="vendor-radio">
	        	<div class="row">
		           	<div class="col-sm-7" >
		           		<div class="control-label text-left option-left">
			                <input type="radio" name="optradio" id="for-cogs"/> Does the vendor invoice contain only Cost of Goods?
			             </div>
		          	</div>
		           	<div class="col-sm-5 cogs-tolerance form-group" >
		           		<div class="padding-top" >
		                  <div class="col-xs-7 tolerance-label">
		                    <span>Invoice level tolerance</span>
		                  </div>
		                  <div class="col-xs-5 input-group">
		                  	<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
		                    <input type="text" name="cogs" class="form-control input-sm currency-format-ipt number-input text-right" placeholder="" id="for-cogs-show" disabled="disabled"  maxlength="15">
		                  </div>
		                </div>
		          	</div>
		        </div>
	        	<div class="row">
		           	<div class="col-sm-7" >
		           		<div class="control-label text-left option-left">
			                <input type="radio" name="optradio" id="for-freight"/> Does the vendor invoice contain  only Freight?
			              </div>
		          	</div>
		           	<div class="col-sm-5 freight-tolerance form-group" >
		           		<div class="padding-top" >
		                  <div class="col-xs-7 tolerance-label">
		                    <span>Invoice level tolerance</span>
		                  </div>
		                  <div class="col-xs-5 input-group">
		                  	<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
		                    <input type="text" name="freight" class="form-control input-sm currency-format-ipt number-input text-right" placeholder="" disabled="disabled" id="for-freight-show"  maxlength="15">
		                  </div>
		                </div>
		          	</div>
		        </div>
		        <div class="row">
		           <div class="col-sm-7" >
		              <div class="control-label text-left option-left">
		                <input type="radio" name="optradio" id="for-both"/> Does the vendor invoice contain  both Cost of Goods and Freight?
		              </div>
		          </div>
		           <div class="col-sm-5 both-tolerance form-group">
		                <div class="padding-top" >
		                  <div class="col-xs-7 tolerance-label">
		                    <span>Invoice level tolerance</span>
		                  </div>
		                  <div class="col-xs-5 input-group">
		                  	<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
		                    <input type="text"  name="both" class="form-control input-sm currency-format-ipt number-input text-right" placeholder="" disabled="disabled" id="for-both-show"  maxlength="15">
		                  </div>
		                </div>
		           </div>
		        </div>
	        </div>
	        <h5>Invoice Match Rules</h5>
	        
	        <div class="form-group">
	          <div class="col-sm-12 control-label text-left option-left">
	            <input type="checkbox" name="chk-approve" data-left-check="1" /> Automatically approve invoices if the invoice amount is within tolerance
	          </div>
	        </div>
	
	        <div class="form-group">
	          <div class="col-sm-12 control-label text-left option-left">
	            <input type="checkbox" name="chk-use-lower" data-left-check="2">
	          Automatically use lower of HEB and Vendor data and approve
	          </div>
	        </div>
	
	        <div class="col-md-12 style-save-btn style-save-assortment style-save-global">
		        <div class="vendor-attrs pull-right">
		        	<button id="assortment-reset-btn" type="button" class="btn btn-primary" disabled>
		        		<span class="glyphicon glyphicon glyphicon glyphicon-refresh"></span>Reset
		        	</button>
		           	<button id="assortment-save-btn" type="button" class="btn btn-danger" disabled>
		           		<span class="glyphicon glyphicon-floppy-disk"></span>Save
		           	</button>
		        </div>
	        </div>
	     
	     </div>
        
      </form>

   	</div>
</div>
</div>