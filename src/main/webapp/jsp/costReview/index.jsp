<!-- KhoiLe - layout for Retail and Cost Review page -->
<div class="container-fluid container-override">
	<div class="panel panel-primary">
		<div class="panel-heading">Retail and Cost Review - New and Changes</div>
<!-- 		<div class="panel-body style-panel-body">			 -->
			<!-- <div class="filterblock"><a href="javascript:void(0)" onclick="displayBlockCtr('areSearch')">Filters <span class="glyphicon glyphicon-chevron-down"</span></a></div> -->
			
			<div id="contentApp">	
			<div class="legend">
				<div class="accordion-toggle" data-toggle="collapse" href="#areSearch">
					<span>&nbsp;&nbsp;&nbsp;&nbsp;Search Criteria</span>
				</div>
			</div>			
				<div class="row panel-collapse collapse in" id="areSearch">
					<div class="col-sm-6">
						<form class="form-horizontal common-form-style" role="form">
							<div class="form-group">
								<label class="col-md-4">Commodities</label>

								<div class="col-md-8">
									<input id="commodities-input" type="text" class="form-control input-sm">
									<!-- /input-group -->
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4">Brand</label>

								<div class="col-md-8">
									<input type="text" class="form-control input-sm" placeholder="">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4">Receive Date Between</label>

								<div class="col-md-8">
									<div class="row">
										<div class="col-md-6">
											<div class="input-group">
												<input class="form-control input-sm" type="text" placeholder=""
													id="dateBegin">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<input class="form-control input-sm" type="text" placeholder=""
													id="dateEnd">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4">Margin between</label>

								<div class="col-md-8">
									<div class="row">
										<div class="col-md-4">
											<input type="text" class="form-control input-sm">
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control input-sm">
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4">Failed Reason</label>

								<div class="col-md-8">
									<div class="input-group">
										<input type="text" class="form-control input-sm">
										<div class="input-group-btn">
											<button type="button" class="btn btn-default dropdown-toggle"
												data-toggle="dropdown">
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu dropdown-menu-right" role="menu">
												<li><a href="#">Action</a></li>
											</ul>
										</div>
										<!-- /btn-group -->
									</div>
									<!-- /input-group -->
								</div>
							</div>
						</form>
					</div>
					<div class="col-sm-6">
						<div class="form-horizontal common-form-style">
							<div class="form-group">
								<label class="col-md-2">Vendor</label>

								<div class="col-md-9">
									<input id="vendor-input" type="text" class="form-control input-sm">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2">Product</label>

								<div class="col-md-9">
									<input type="text" class="form-control input-sm" placeholder="">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2"></label>
								<div class="col-md-9">
									<label class="common-check"> <input type="checkbox">
										Show all MAP product
									</label>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2"></label>
								<div class="col-md-9">
									<label class="common-check"> <input type="checkbox">
										Show only Pre price product
									</label>
								</div>
							</div>
							<div class="btn-group pull-right">                          
							   <button type="button" class="btn btn-primary">
								 <span class="glyphicon glyphicon-search"></span>
							   </button>
							   <button type="button" class="btn btn-primary">Search</button>
							 </div>
						</div>
					</div>
				</div>
				<div class="" id="searchResult">
					<span class="text-bold">Search Results</span>
				</div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist" id="tabResult">
					<li class="active"><a id="tab-passed-title" href="#pass" role="tab" data-toggle="tab">Passed
							<span class="badge-passed-style">(52)</span></a></li>
					<li><a href="#failed" role="tab" data-toggle="tab">Failed
							<span class="badge-passed-style">(11)</span></a></li>
					<a href="" class="export-excel"></a>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane active common-content-tab" id="pass">
						<form action="#">			
							<div class="table-responsive">				
								<table class="table table-condensed common-table-style table-hover">
									<thead>
										<tr class="firs-tr">
											<th><input type="checkbox" class="check-all"></th>
											<th>No</th>
											<th>Product Description</th>
											<th>Pack/Size</th>
											<th>Current Cost</th>
											<th>Current retail</th>
											<th>MAP</th>
											<th>COMP</th>
											<th>Current GP%</th>
											<th>Next Cost</th>
											<th>Next Retail</th>
											<th>MAP</th>
											<th>Next GP%</th>
											<th>Effective</th>
											<th>Future Cost/Retail</th>
										</tr>
									</thead>
									<tbody class="body-table">
										<tr class="inputData">
											<td></td>
											<td></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td></td>
										</tr>

										<tr>
											<td><input type="checkbox"></td>
											<td>1</td>
											<td>DSV Product A</td>
											<td>1/16 oz</td>
											<td class="table-align-right">$ 1</td>
											<td class="table-align-right can-change">$ 1.20</td>
											<td class="table-align-center">N</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-right">20.0%</td>
											<td class="table-align-right">$ 1</td>
											<td class="table-align-right can-change cost-decrease">$ 1.10</td>
											<td class="table-align-center">N</td>
											<td class="table-align-right cost-increase">10.0%</td>
											<td class="table-align-center">8/7/2014</td>
											<td class="table-align-center">N</td>
										</tr>
										<tr>
											<td><input type="checkbox"></td>
											<td>2</td>
											<td>DSV Product B</td>
											<td>12/ Large</td>
											<td class="table-align-right">$ 23</td>
											<td class="table-align-right can-change">$ 30</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-right">30.4%</td>
											<td class="table-align-right">$ 24</td>
											<td class="table-align-right can-change">$ 30</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-right cost-increase">25.0%</td>
											<td class="table-align-center">8/12/2014</td>
											<td class="table-align-center">
												<a type="button" href="#future-data-line1-row2" data-toggle="modal">Y</a>
												<div class="modal" id="future-data-line1-row2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-body">
															<table class="table table-condensed common-table-style table-hover">
																<thead>
																	<tr class="firs-tr">
																		<th>Product Description</th>
																		<th>Pack/Size</th>
																		<th>Current Cost</th>
																		<th>Current Retail</th>
																		<th>MAP</th>
																		<th>Current GP%</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>DSV Product B</td>
																		<td>12/ Large</td>
																		<td>$ 23.00</td>
																		<td>$ 30.00</td>
																		<td>Y</td>
																		<td>30.4%</td>
																	</tr>
																</tbody>
															</table>
															<table class="table table-condensed common-table-style table-hover magrintop30">
																<thead>
																	<tr class="firs-tr">
																		<th></th>
																		<th>Effective Date</th>
																		<th>Next Cost</th>
																		<th>Next Retail</th>
																		<th>MAP</th>
																		<th>Next GP%</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td><input type="checkbox"></td>
																		<td>8/12/2014</td>
																		<td>$ 24.00</td>
																		<td class="can-change">$ 30.00</td>
																		<td>Y</td>
																		<td>25.0%</td>
																	</tr>
																	<tr>
																		<td><input type="checkbox"></td>
																		<td>8/15/2014</td>
																		<td>$ 24.50</td>
																		<td class="can-change">$ 30.00</td>
																		<td>Y</td>
																		<td>22.4%</td>
																	</tr>
																	<tr>
																		<td><input type="checkbox"></td>
																		<td>8/18/2014</td>
																		<td>$ 25.00</td>
																		<td class="can-change">$ 30.00</td>
																		<td>Y</td>
																		<td>21.0%</td>
																	</tr>
																</tbody>
															</table>
															<a href="#" class="btn btn-primary">Accept</a>
															<a href="#" class="btn btn-primary">Reject</a>
															<a href="#" class="btn btn-primary">Remove</a>
														</div>
														<div class="modal-footer">												
															<a href="#" class="btn btn-primary">Save</a>
															<a href="#" class="btn btn-primary">Reset</a>
															<a href="#" class="btn btn-primary" data-dismiss="modal">Return</a>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td><input type="checkbox"></td>
											<td>3</td>
											<td>DSV Product C</td>
											<td>1 Each</td>
											<td class="table-align-right">$ 14.99</td>
											<td class="table-align-right can-change">$ 17.99</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-right">20.0%</td>
											<td class="table-align-right">$ 15.99</td>
											<td class="table-align-right can-change cost-increase">$ 18.99</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-right cost-increase">18.8%</td>
											<td class="table-align-center">8/12/2014</td>
											<td class="table-align-center">N</td>
										</tr>
									</tbody>
								</table>				
							</div>
							<div class="clearfix">
								<button class="btn btn-danger btn-danger-reject pull-right">Reject</button>
								<button class="btn btn-danger pull-right" style="margin-right: 10px;">Remove</button>
							</div>
						</form>
					</div>
					<div class="tab-pane common-content-tab" id="failed">
						<form action="#">
							<div class="table-responsive">				
								<table class="table table-condensed common-table-style table-hover">
									<thead>
										<tr class="firs-tr">
											<th><input type="checkbox" class="check-all"></th>
											<th>No</th>
											<th>Description</th>
											<th>Pack/Size</th>
											<th>Current Cost</th>
											<th>Current retail</th>
											<th>MAP</th>
											<th>COMP</th>
											<th>Current GP%</th>
											<th>Next Cost</th>
											<th>Next Retail</th>
											<th>MAP</th>
											<th>Next GP%</th>
											<th>Effective</th>
											<th>Future Cost/Retail</th>
											<th>Informations</th>
										</tr>
									</thead>
									<tbody class="body-table">
										<tr class="inputData">
											<td></td>
											<td></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td><input type="text" class="form-control input-sm"></td>
											<td></td>
										</tr>

										<tr>
											<td><input type="checkbox"></td>
											<td>1</td>
											<td>DSV Product D</td>
											<td>1/5 lbs</td>
											<td class="table-align-right">$ 12.98</td>
											<td class="table-align-right can-change">$ 15.99</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-center">N</td>
											<td class="table-align-right">23.2%</td>
											<td class="table-align-right">$ 12.98</td>
											<td class="table-align-right can-change cost-decrease">$ 13.99</td>
											<td class="table-align-center alert-block">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-right cost-increase">7.8%</td>
											<td class="table-align-center">8/14/2014</td>
											<td class="table-align-center">N</td>
											<td>										
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<p><b>Rules Broken:</b></p>
															<p>Minimun GP%: 15%</p>
												">
													<span class="glyphicon glyphicon-info-sign"></span>
												</a>
											</td>
										</tr>
										<tr>
											<td><input type="checkbox"></td>
											<td>2</td>
											<td>DSV Product E</td>
											<td>12/ Large</td>
											<td class="table-align-right">$ 23</td>
											<td class="table-align-right can-change">$ 30</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-right">30.4%</td>
											<td class="table-align-right">$ 24</td>
											<td class="table-align-right can-change">$ 30</td>
											<td class="table-align-center">
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<table class='table table-condensed common-table-style table-hover'>
														<thead>
															<tr class='firs-tr'>
																<th></th>
																<th>Old</th>
																<th>Date</th>
																<th>New</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class='table-align-left'>Wal-Mart:</td>
																<td>$1.20</td>
																<td>7/28/2014</td>
																<td>$1.10</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Amazon:</td>
																<td>$1.10</td>
																<td>7/28/2014</td>
																<td>$0.99</td>
																<td>8/1/2014</td>
															</tr>
															<tr>
																<td class='table-align-left'>Target:</td>
																<td>$1.30</td>
																<td>7/28/2014</td>
																<td>$1.20</td>
																<td>8/1/2014</td>
															</tr>
															<tr class='font-bold'>
																<td class='table-align-left'>Average:</td>
																<td>$1.20</td>
																<td></td>
																<td>$1.10</td>
																<td></td>
															</tr>
														</tbody>
													</table>
												">Y</a>
											</td>
											<td class="table-align-right cost-increase">25.0%</td>
											<td class="table-align-center">8/12/2014</td>
											<td class="table-align-center">N</td>
											<td>										
												<!-- Bootstrap Tooltip -->
												<a class="tooltip-class" href="#" data-html="true" data-toggle="tooltip" title="
													<p><b>Rules Broken:</b></p>
															<p>Minimun GP%: 15%</p>
												">
													<span class="glyphicon glyphicon-info-sign"></span>
												</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="clearfix">
								<button class="btn btn-danger btn-danger-reject pull-right">Reject</button>
								<button class="btn btn-danger pull-right" style="margin-right: 10px;">Remove</button>
								<button class="btn btn-danger pull-right" style="margin-right: 10px;">Accept</button>
							</div>
						</form>
					</div>
				</div>
			</div>
<!-- 		</div> -->
	</div>
</div>