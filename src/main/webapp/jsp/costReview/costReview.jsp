<div class="container-fluid container-override">
   <div class="panel panel-primary">
      <div class="panel-heading">
         Retail and Cost Review - New and Changes
      </div>
      <div class="panel-body style-panel-body">
         <div id="contentApp">
            <div class="legend">
               <span class="accordion-toggle-bdm" data-toggle="collapse" href="#new-and-changes-header"><i id="fa-angle-double-down-style" class="fa fa-angle-double-down" style="width: 15px;"></i>Search Criteria</span>
            </div>
            <div id="alert-wp-header" class="col-md-12 padding-left0 padding-right0 margin-bottom10 hidden">
                <div class="alert alert-danger margin-bottom10" role="alert">
					<span id="message-alert-header"></span>
				</div>
           	</div>
            <div id="new-and-changes-header" class="row collapse in">
               <div class="col-sm-6">
                  <form class="form-horizontal common-form-style" role="form">
                     <div class="form-group">
                        <label class="col-md-4">Commodity</label>
                        <div class="col-md-8">
                           <input id="commodities-input" type="text" class="form-control input-sm" commoditiesid="">
                           <!-- /input-group -->
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-4">Brand</label>
                        <div class="col-md-8">
                           <input id="brand-input" type="text" class="form-control input-sm" placeholder="" brandid="">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-4">Receive Date Between</label>
                        <div class="col-md-8">
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="input-group">
                                    <input class="form-control input-sm" type="text" placeholder=""
                                       id="dateBegin" dateBeginValue="">
                                    <div class="input-group-addon" id="dateBegin-icon-wp">
                                       <i class="fa fa-calendar"></i>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-2 and-label-wp">
                              	<span class="label label-default and-label" style="vertical-align: -webkit-baseline-middle;">and</span>
                              </div>
                              <div class="col-md-5">
                                 <div class="input-group receive-date-bw-wp">
                                    <input class="form-control input-sm" type="text" placeholder=""
                                       id="dateEnd" dateEndValue="">
                                    <div class="input-group-addon" id="dateEnd-icon-wp">
                                       <i class="fa fa-calendar"></i>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-4">Margin Between</label>
                        <div class="col-md-8">
                           <div class="row">
                              <div class="col-md-5">
                                 <input id="marginBegin" type="text" class="form-control input-sm text-right negative-number" marginBeginValue="">
                              </div>
                              <div class="col-md-2 and-label-wp">
                              	<span class="label label-default and-label" style="vertical-align: -webkit-baseline-middle;">and</span>
                              </div>
                              <div class="col-md-5">
                                 <input id="marginEnd" type="text" class="form-control input-sm text-right negative-number" marginEndValue="">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-4">Failed Reason</label>
                        <div class="col-md-8">
                           <select class="form-control input-sm" id="failed-input">
                              <option></option>
                           </select>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="col-sm-6">
                  <div class="form-horizontal common-form-style">
                     <div class="form-group">
                        <label class="col-md-2">Vendor</label>
                        <div class="col-md-9">
							<!--<input id="vendor-input" type="text" class="form-control input-sm" vendorid=""> -->
							<select class="form-control input-sm" id="vendor-input">
                              <option></option>
                           	</select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-2">Product</label>
                        <div class="col-md-9">
                           <input id="product-input" type="text" class="form-control input-sm" placeholder="" productid="">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-2"></label>
                        <div class="col-md-9">
                           <input id="show-all-map-check" class="text-ver-middle margin-top0 margin-right5 cursor-pointer" type="checkbox">
                           <label class="common-check"> 
<!--                            		text-ver-middle margin-top0 -->
                           		<span class="margin-top0">Only MAP products</span>
                           </label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-md-2"></label>
                        <div class="col-md-9">
                           <input id="show-only-prePrice-check" class="text-ver-middle margin-top0 margin-right5 cursor-pointer" type="checkbox">
                           <label class="common-check"> 
                           		<span class="">Only pre-price products</span>
                           </label>
                        </div>
                     </div>
                     <div class="btn-group pull-right search-btn-wp">
                     <button id="reset-cost-review-btn" type="button" class="btn btn-default margin-right10 border-radius-btn"><span class="glyphicon glyphicon-refresh" tabindex="12"></span>&nbsp;&nbsp; Clear</button>
                     <button id="search-cost-review-btn" type="button" class="btn btn-primary border-radius-btn margin-bottom5"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Search</button>                          
<!--                         <button type="button" class="btn btn-primary"> -->
<!--                         <span class="glyphicon glyphicon-search"></span> -->
<!--                         </button> -->
<!--                         <button id="search-new-product-btn" type="button" class="btn btn-primary">Search</button> -->
                     </div>
                  </div>
               </div>
            </div>
            
            <div class="" id="searchResult">
				<span class="text-bold">Search Results</span>
			</div>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist" id="tabResult">
				<li id="tab-passed-li" class="active"><a id="tab-all-title" href="#all" role="tab" data-toggle="tab" value="all">All <span id="badge-all" class="badge-all-style">(0)</span></a></li>
				<li id="tab-passed-li"><a id="tab-passed-title" href="#passed" role="tab" data-toggle="tab" value="passed">Passed <span id="badge-passed" class="badge-passed-style">(0)</span></a></li>
				<li id="tab-failed-li"><a id="tab-failed-title" href="#failed" role="tab" data-toggle="tab" value="failed">Failed <span id="badge-failed" class="badge-failed-style">(0)</span></a></li>
				<li id="cost-review-export-btn" class="export-excel" title="Export to Excel"></li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active common-content-tab" id="all">	
					<div id="alert-wp-all" class="col-md-12 margin-top10 padding-left0 padding-right0 min-width-alert-wp hidden">
                  		<div class="alert alert-danger margin-bottom10" role="alert">
				      		<span id="message-alert-all"></span>
				    	</div>
                  	</div>			
					<div class="col-sm-12 padding-left0 padding-right0 table-responsive table-responsive-min-width">
		               <table id="new-and-changes-all-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
		                  <colgroup>
		                  	 <col class="check-box-group">
		                     <col class="no-group">
		                     <col class="upc-group">
		                     <col class="product-desc-group">
		                     <col class="pack-size-group">
		                     <col class="current-cost-group">
		                     <col class="current-retail-group">
		                     <col class="current-map-yn-group">
		                     <col class="current-comp-yn-group">
		                     <col class="current-gp-group">   
		                     <col class="next-pack-size-group">
		                     <col class="next-cost-group">
		                     <col class="next-retail-group">
		                     <col class="next-map-yn-group">
		                     <col class="next-gp-group">
		                     <col class="status-group">
		                     <col class="effective-date-group">              
		                     <col class="change-reason-group">
		                  </colgroup>
		                 
		                  <thead>
		                  	<tr class="zero-tr head-tr">
		                        <th></th>
		                        <th></th>
		                        <th></th>
		                        <th></th>
		                        <th colspan="6">Current</th>	                        
		                        <th colspan="7">Next</th>		                        
		                        <th></th>
		                     </tr>
		                     <tr class="firs-tr head-tr">
		                        <th></th>
		                        <th>S. No.</th>
		                        <th>UPC</th>
		                        <th>Product Description</th>
		                        <th>Pack/Size</th>
		                        <th>Cost</th>
		                        <th>Retail</th>
		                        <th>MAP</th>
		                        <th>COMP</th>
		                        <th>GP%</th>	                        
		                        <th>Pack/Size</th>
		                        <th>Cost</th>
		                        <th>Retail</th>
		                        <th>MAP</th>
		                        <th>GP%</th>
		                        <th>Status</th>
		                        <th>Effective Date</th>		                        
		                        <th>Change Reason</th>
		                     </tr>
		                  </thead>
		                  <thead>
		                     <tr class="second-tr head-tr second-tr-all">
		                        <th>
									<button id="clear-filtter-btn-all" type="button" class="btn btn-default btn-sm filter-btn-style" title="Clear the Filter"></button>
		                        </th>
		                        <th><input id="no-all" type="text" class="form-control input-sm text-right pd0 input-filter input-number" disabled="disabled"/></th>
		                        <th><input id="search-upc-all" type="text" class="form-control input-sm text-right pd0 input-filter input-number"/></th>
		                        <th><input id="search-product-desc-all" type="text" class="form-control input-sm text-left pd0 input-filter"/></th>
		                        <th><input id="search-pack-size-all" type="text" class="form-control input-sm text-left pd0 input-filter"/></th>
		                        <th><input id="search-current-cost-all" type="text" class="form-control input-sm text-right pd0 input-filter input-decimal input-decimal"/></th>
		                        <th><input id="search-current-retail-all" type="text" class="form-control input-sm text-right pd0 input-filter input-decimal"/></th>         
		                        
		                        <th>
									<!--<input id="search-current-map-yn-all" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
		                        	<select id="search-current-map-yn-all" class="form-control select-sm pd0 input-filter">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
		                        </th>
		                        
		                        <th>
									<!--<input id="search-current-comp-yn-all" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
		                        	<select id="search-current-comp-yn-all" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
		                        </th>
		                        
		                        <th><input id="search-current-gp-all" type="text" class="form-control input-sm text-right pd0 input-filter negative-number"/></th>                        
		                        <th><input id="search-next-pack-size-all" type="text" class="form-control input-sm text-left pd0 input-filter"/></th>
		                        <th><input id="search-next-cost-all" type="text" class="form-control input-sm text-right pd0 input-filter input-decimal"/></th>
		                        <th><input id="search-next-retail-all" type="text" class="form-control input-sm text-right pd0 input-filter input-decimal"/></th>
		                        
		                        <th>
									<!--<input id="search-next-map-yn-all" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
		                        	<select id="search-next-map-yn-all" class="form-control select-sm pd0 input-filter">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
		                        </th>
		                        
		                        <th><input id="search-next-gp-all" type="text" class="form-control input-sm text-right pd0 input-filter negative-number"/></th>
		                        
		                        <th>
									<!--<input id="search-status-all" type="text" class="form-control input-sm text-left pd0 input-filter"/> -->
		                        	<select id="search-status-all" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="passed">Passed</option>
									  <option value="failed">Failed</option>
									</select>
		                        </th>
		                        
		                        <th><input id="search-effective-date-all" type="text" class="form-control input-sm text-right pd0 input-filter"/></th>                   
		                        
		                        <th>
									<!--<input id="search-change-reason-all" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
									<select id="search-change-reason-all" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="PACK">PACK</option>
									  <option value="COST">COST</option>
									  <option value="COMP">COMP</option>
									</select>
		                        </th> 
		                     </tr>
		                  </thead>
		               </table>
		               
		            </div>
					<div class="clearfix padding-top5 margin-bottom5">
						<div class="pull-right">
							<button id="approve-btn-all" class="btn btn-danger margin-right5 approve-btn-cls margin-top5">Approve</button>
							<button id="accept-btn-all" class="btn btn-danger margin-right5 accept-btn-cls margin-top5">Accept</button>
							<button id="reject-btn-all" class="btn btn-danger btn-danger-rejected margin-right5 reject-btn-cls margin-top5">Reject</button>
							<button id="remove-btn-all" class="btn btn-danger btn-danger-rejected margin-right5 remove-btn-cls margin-top5">Remove</button>
							<button id="save-btn-all" class="btn btn-danger btn-danger-rejected save-btn-cls margin-top5">Save</button>
						</div>
					</div>
				</div>
				<div class="tab-pane common-content-tab" id="passed">				
					<div id="alert-wp-passed" class="col-md-12 margin-top10 padding-left0 padding-right0 min-width-alert-wp hidden">
                  		<div class="alert alert-danger margin-bottom10" role="alert">
				      		<span id="message-alert-passed"></span>
				    	</div>
                  	</div>
					<div class="col-sm-12 padding-left0 padding-right0 table-responsive table-responsive-min-width">
		               <table id="new-and-changes-passed-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
		                  <colgroup>
		                  	 <col class="check-box-group">
		                     <col class="no-group">
		                     <col class="upc-group">
		                     <col class="product-desc-group">
		                     <col class="pack-size-group">
		                     <col class="current-cost-group">
		                     <col class="current-retail-group">
		                     <col class="current-map-yn-group">
		                     <col class="current-comp-yn-group">
		                     <col class="current-gp-group">		                     
		                     <col class="next-pack-size-group">
		                     <col class="next-cost-group">
		                     <col class="next-retail-group">
		                     <col class="next-map-yn-group">
		                     <col class="next-gp-group">
		                     <col class="status-group">
		                     <col class="effective-date-group">	                       
		                     <col class="change-reason-group"> 
		                  </colgroup>

		                  <thead>
		                   <tr class="zero-tr head-tr">
								<th></th>
		                        <th></th>
		                        <th></th>
		                        <th></th>
		                        <th colspan="6">Current</th>	                        
		                        <th colspan="7">Next</th>		                        
		                        <th></th>
		                     </tr>
		                     <tr class="firs-tr head-tr">
								<th></th>
		                        <th>S. No.</th>
		                        <th>UPC</th>
		                        <th>Product Description</th>
		                        <th>Pack/Size</th>
		                        <th>Cost</th>
		                        <th>Retail</th>
		                        <th>MAP</th>
		                        <th>COMP</th>
		                        <th>GP%</th>	                        
		                        <th>Pack/Size</th>
		                        <th>Cost</th>
		                        <th>Retail</th>
		                        <th>MAP</th>
		                        <th>GP%</th>
		                        <th>Status</th>
		                        <th>Effective Date</th>		                        
		                        <th>Change Reason</th>
		                     </tr>
		                  </thead>
		                  <thead>
		                     <tr class="second-tr head-tr second-tr-passed">
		                        <th>
									<button id="clear-filtter-btn-passed" type="button" class="btn btn-default btn-sm filter-btn-style" title="Clear the Filter"></button>
		                        </th>
		                        <th><input id="no-passed" type="text" class="form-control input-sm text-right pd0 input-filter input-number" disabled="disabled"/></th>
		                        <th><input id="search-upc-passed" type="text" class="form-control input-sm text-right pd0 input-filter input-number"/></th>
		                        <th><input id="search-product-desc-passed" type="text" class="form-control input-sm text-left input-filter pd0"/></th>
		                        <th><input id="search-pack-size-passed" type="text" class="form-control input-sm text-left input-filter pd0"/></th>
		                        <th><input id="search-current-cost-passed" type="text" class="form-control input-sm text-right pd0 input-filter input-decimal"/></th>
		                        <th><input id="search-current-retail-passed" type="text" class="form-control input-sm text-right pd0 input-filter input-decimal"/></th>         
		                        
		                        <th>
		                        	<!--<input id="search-current-map-yn-all" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
		                        	<select id="search-current-map-yn-passed" class="form-control select-sm pd0 input-filter">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
		                        </th>
		                        
		                        <th>
									<!--<input id="search-current-comp-yn-all" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
									<select id="search-current-comp-yn-passed" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
								</th>
		                        
		                        <th><input id="search-current-gp-passed" type="text" class="form-control input-sm text-right pd0 input-filter negative-number"/></th>                        
		                        <th><input id="search-next-pack-size-passed" type="text" class="form-control input-sm text-left input-filter pd0"/></th>
		                        <th><input id="search-next-cost-passed" type="text" class="form-control input-sm text-right pd0 input-filter input-decimal"/></th>
		                        <th><input id="search-next-retail-passed" type="text" class="form-control input-sm text-right pd0 input-filter input-decimal"/></th>
		                        
		                        <th>
									<!--<input id="search-next-map-yn-passed" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
									<select id="search-next-map-yn-passed" class="form-control select-sm pd0 input-filter">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
								</th>
		                        
		                        <th><input id="search-next-gp-passed" type="text" class="form-control input-sm text-right pd0 input-filter negative-number"/></th>
		                        
		                        <th>
									<!--<input id="search-status-passed" type="text" class="form-control input-sm text-left pd0 input-filter"/> -->
									<select id="search-status-passed" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="passed">Passed</option>
									  <option value="failed">Failed</option>
									</select>
								</th>
		                        
		                        <th><input id="search-effective-date-passed" type="text" class="form-control input-sm text-right input-filter pd0"/></th>                   
		                        <th>
		                        	<select id="search-change-reason-passed" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="PACK">PACK</option>
									  <option value="COST">COST</option>
									  <option value="COMP">COMP</option>
									</select>
		                        </th>
		                      	
		                     </tr>
		                  </thead>
		               </table>
		               
		            </div>
		            <div class="clearfix padding-top5 margin-bottom5">
						<div class="pull-right">
							<button id="approve-btn-passed" class="btn btn-danger margin-right5 approve-btn-cls margin-top5">Approve</button>
							<button id="reject-btn-passed" class="btn btn-danger btn-danger-rejected margin-right5 reject-btn-cls margin-top5">Reject</button>
							<button id="remove-btn-passed" class="btn btn-danger btn-danger-rejected margin-right5 remove-btn-cls margin-top5">Remove</button>
							<button id="save-btn-passed" class="btn btn-danger btn-danger-rejected save-btn-cls margin-top5">Save</button>
						</div>
					</div>
				</div>
				<div class="tab-pane common-content-tab" id="failed">
					<div id="alert-wp-failed" class="col-md-12 margin-top10 padding-left0 padding-right0 min-width-alert-wp hidden">
                  		<div class="alert alert-danger margin-bottom10" role="alert">
				      		<span id="message-alert-failed"></span>
				    	</div>
                  	</div>
					<div class="col-sm-12 padding-left0 padding-right0 table-responsive table-responsive-min-width">
		               <table id="new-and-changes-failed-table" class="table table-condensed common-table-style table-hover table-striped table-bordered">
		                  <colgroup>
		                     <col class="check-box-group">
		                     <col class="no-group">
		                     <col class="upc-group">
		                     <col class="product-desc-group">
		                     <col class="pack-size-group">
		                     <col class="current-cost-group">
		                     <col class="current-retail-group">
		                     <col class="current-map-yn-group">
		                     <col class="current-comp-yn-group">
		                     <col class="current-gp-group">		                     
		                     <col class="next-pack-size-group">
		                     <col class="next-cost-group">
		                     <col class="next-retail-group">
		                     <col class="next-map-yn-group">
		                     <col class="next-gp-group">
		                     <col class="status-group">
		                     <col class="effective-date-group">	                       
		                     <col class="change-reason-group"> 
		                  </colgroup>
	
		                  <thead>
		                   	 <tr class="zero-tr head-tr">
							 	<th></th>
		                        <th></th>
		                        <th></th>
		                        <th></th>
		                        <th colspan="6">Current</th>	                        
		                        <th colspan="7">Next</th>		                        
		                        <th></th>
		                     </tr>
		                     <tr class="firs-tr head-tr">
		                        <th></th>
		                        <th>S. No.</th>
		                        <th>UPC</th>
		                        <th>Product Description</th>
		                        <th>Pack/Size</th>
		                        <th>Cost</th>
		                        <th>Retail</th>
		                        <th>MAP</th>
		                        <th>COMP</th>
		                        <th>GP%</th>	                        
		                        <th>Pack/Size</th>
		                        <th>Cost</th>
		                        <th>Retail</th>
		                        <th>MAP</th>
		                        <th>GP%</th>
		                        <th>Status</th>
		                        <th>Effective Date</th>		                        
		                        <th>Change Reason</th>
		                     </tr>
		                  </thead>
		                  <thead>
		                     <tr class="second-tr head-tr second-tr-failed">
		                        <th>
									<button id="clear-filtter-btn-failed" type="button" class="btn btn-default btn-sm filter-btn-style" title="Clear the Filter"></button>
		                        </th>
		                        <th><input id="no-failed" type="text" class="form-control input-sm text-right pd0 input-filter input-number" disabled="disabled"/></th>
		                        <th><input id="search-upc-failed" type="text" class="form-control input-sm text-right pd0 input-filter input-number"/></th>
		                        <th><input id="search-product-desc-failed" type="text" class="form-control input-sm text-left pd0 input-filter"/></th>
		                        <th><input id="search-pack-size-failed" type="text" class="form-control input-sm text-left pd0 input-filter"/></th>
		                        <th><input id="search-current-cost-failed" type="text" class="form-control input-sm text-right pd0 input-decimal input-filter"/></th>
		                        <th><input id="search-current-retail-failed" type="text" class="form-control input-sm text-right pd0 input-decimal input-filter"/></th>         
		                        
		                        <th>
									<!--<input id="search-current-map-yn-failed" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
									<select id="search-current-map-yn-failed" class="form-control select-sm pd0 input-filter">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
								</th>
								
		                       	<th>
									<!--<input id="search-current-comp-yn-failed" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
									<select id="search-current-comp-yn-failed" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
								</th>
		                        
		                        <th><input id="search-current-gp-failed" type="text" class="form-control input-sm text-right pd0 negative-number input-filter"/></th>                        
		                        <th><input id="search-next-pack-size-failed" type="text" class="form-control input-sm text-left pd0 input-filter"/></th>
		                        <th><input id="search-next-cost-failed" type="text" class="form-control input-sm text-right pd0 input-decimal input-filter"/></th>
		                        <th><input id="search-next-retail-failed" type="text" class="form-control input-sm text-right pd0 input-decimal input-filter"/></th>
		                        
		                        <th>
									<!--<input id="search-next-map-yn-failed" type="text" class="form-control input-sm text-right pd0 input-filter"/> -->
									<select id="search-next-map-yn-failed" class="form-control select-sm pd0 input-filter">
									  <option value=""></option>
									  <option value="Y">Yes</option>
									  <option value="N">No</option>
									</select>
								</th>
		                        
		                        <th><input id="search-next-gp-failed" type="text" class="form-control input-sm text-right pd0 negative-number input-filter"/></th>
		                        
		                        <th>
									<!--<input id="search-status-failed" type="text" class="form-control input-sm text-left pd0 input-filter"/> -->
									<select id="search-status-failed" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="passed">Passed</option>
									  <option value="failed">Failed</option>
									</select>
								</th>
		                        
		                        <th><input id="search-effective-date-failed" type="text" class="form-control input-sm text-right pd0 input-filter"/></th>                   
		                        <th>
		                        	<select id="search-change-reason-passed" class="form-control select-sm pd0 input-filter" disabled="disabled">
									  <option value=""></option>
									  <option value="PACK">PACK</option>
									  <option value="COST">COST</option>
									  <option value="COMP">COMP</option>
									</select>
		                        </th>	
		                     </tr>
		                  </thead>
		               </table>
		            </div>
		            <div class="clearfix padding-top5 margin-bottom5">
						<div class="pull-right">
							<button id="accept-btn-failed" class="btn btn-danger margin-right5 accept-btn-cls margin-top5">Accept</button>
							<button id="remove-btn-failed" class="btn btn-danger btn-danger-rejected margin-right5 remove-btn-cls margin-top5">Remove</button>
							<button id="reject-btn-failed" class="btn btn-danger btn-danger-rejected margin-right5 reject-btn-cls margin-top5">Reject</button>
							<button id="save-btn-failed" class="btn btn-danger btn-danger-rejected save-btn-cls margin-top5">Save</button>
						</div>
					</div>
				</div>
			</div>
            
         </div>
      </div>
      <!-- end body -->
   </div>
</div>

<!--[if IE]>
	<style>
		#new-and-changes-all-table .text-ellipsis
		#new-and-changes-passed-table .text-ellipsis,
		#new-and-changes-failed-table .text-ellipsis{
			width: 150px;
			padding: 0 !important;
		}
		#new-and-changes-all-table .text-ellipsis span,
		#new-and-changes-passed-table .text-ellipsis span,
		#new-and-changes-failed-table .text-ellipsis span{
			display: table-caption;
			word-break: break-all;
			overflow: hidden;
			text-overflow: ellipsis;
			margin: 0 !important;
			width: inherit;
		}
	</style>
<![endif]-->