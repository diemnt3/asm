<div class="container-fluid container-override">
	<!-- <div id="message" class="alert alert-success"></div> -->
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="titleAdd">User Access</div>
			<div class="userIcon">
				<button type="button" class="btn btn-primary" id="add-user-icon">
					<span class="glyphicon glyphicon-plus" title="Add user" disabled="disabled"></span>
				</button>
				<button type="button" data-target="#vendor-modal" class="btn btn-primary" id="save-user-icon" disabled="disabled">
					<span class="glyphicon glyphicon-floppy-disk" title="Save"></span>
				</button>
			</div>
			<!-- <div style="clear:both"></div> -->
		</div>
		<div class="panel-body style-panel-body" id="user-management">
			<form onsubmit="return false" role="form" class="form-horizontal" id="frm-user-management">
				<!-- table -->
				<div class="table-responsive">
					<table class="table table-condensed common-table-style table-hover table-striped table-bordered" id="user-table">
						<thead>
							<tr class="firs-tr">
								<th>No</th>
								<th>User ID</th>
								<th>User Name</th>
								<th>Access Type</th>
								<th>Assigned Vendors</th>
								<th>Action</th>
								<th></th>
								<th></th>
							</tr>
						</thead>	
						<tbody class="body-table">	
						</tbody>
					</table>
				</div>	
				<div style="clear: both"></div>
				<div id="user-btns">
					<button type="button" class="btn btn-primary" id="add-user-btn">
						<span class="glyphicon glyphicon-plus"></span> Add user
					</button>
					<button type="button" data-target="#vendor-modal" class="btn btn-primary" id="save-user-btn" disabled="disabled">
						<span class="glyphicon glyphicon-floppy-disk"></span> Save
					</button>
				</div>
				<!-- end table -->
			</form>
			<div id="message-err"></div>
		</div>
	</div>
</div>
<!-- Modal for vendor data -->
<div class="modal fade" id="vendor-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h5 class="modal-title" id="myModalLabel">Vendor List</h5>
			</div>
			<div class="modal-body">
				<label id="hidden-user">vn96434</label>
				<!-- table -->
				<div class="form-group" id="vendor-search">
					<label class="col-md-2">Filter</label>
					<div class="col-md-10">
						<input id="search-vendor-name-btn" type="text" class="form-control input-sm text-left pd0 hasclear" />
						<!-- <span class="clearer glyphicon glyphicon-remove form-control-feedback"></span> -->
						<span class="clearer form-control-feedback cursor-pointer" aria-hidden="true">&times;</span>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-condensed common-table-style-add table-hover table-striped table-bordered" id="vendor-table">
						<thead>
							<tr class="firs-tr">
								<th><input type="checkbox" /></th>
								<th>Vendor ID</th>
								<th>Vendor Name</th>
								<th>Is Selected</th>
							</tr>
						</thead>
						<tbody class="body-table">
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
				<button type="button" class="btn btn-primary" id="btn-save-change" data-dismiss="modal" disabled="disabled">
					<span class="glyphicon glyphicon-list-alt"></span> Save
				</button>
			</div>
			<div style="clear: both"></div>
		</div>
	</div>
</div>
<!-- confirm dialog to delete -->
<div class="modal fade" id="confirm-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h5 class="modal-title" id="confirm-dialog-label">Confirm</h5>
			</div>
			<div class="modal-body">Are you sure you want to remove this user?</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="yes-select"
					data-dismiss="modal">Yes</button>
				<button type="button" class="btn btn-primary" id="no-select" data-dismiss="modal">No</button>				
			</div>
			<div style="clear: both"></div>
		</div>
	</div>
</div>
<!-- alert for input data -->
<div class="modal fade" id="alert-dialog" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="alert-dialog-label">Warning!</h4>
			</div>
			<div class="modal-body">Please, input data for the last row!</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="yes-select" data-dismiss="modal">Ok</button>
			</div>
			<div style="clear: both"></div>
		</div>
	</div>
</div>
