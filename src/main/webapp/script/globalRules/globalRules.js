var searchHebTable = null;
var searchItemsTable = null;
//Array to track the ids of the details displayed rows
var detailRows = [];

$(document).ready(function() {
	datepickerOrderSearch();
	ctrlKeyDownStatusSettup();

	$('#search-heb-form input.need-validation').focusout(function() {
		validateTwoDateCompare("order-date-begin", "order-date-end");
    });

	var adjReason = [];
	var table = initInvoiceDetailsTable();
	var no = 0;
	table.on( 'draw.dt', function () {
    	table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
    		//debugger;
    		no = i + 1 + table.page.info().start;
    		cell.innerHTML = no;
    		$(cell).attr("id", no);
    		
    		var selectorTr = $(cell).parent(); 
    		//filldata
    		var itemObj = {
    			id: $(selectorTr).attr("id")
    		}
    		if (inArrays(items, itemObj)) {
    			var dataFill = getItemInArraysByKey(items, itemObj);
        		fillDataForAdjustment($(selectorTr), dataFill);
    		}
    	});
	} );


	
	$("#invoice-details-save-btn").click(function() {
    	saveInvoiceDetail();
    });
	
	$(document).on("change",".checkbox-table", function () {
		//debugger;
		var selectorTr = $(this).parent().parent();
		var cost = 0;
		var qty = 0;
		// var vendorCost = $(selectorTr).find("td:eq(7)").text();
		var vendorCost = $("#invoice-details-table").DataTable().row(selectorTr).data().vendorCost;
		var vendorQty = $(selectorTr).find("td:eq(8)").text();
		var reason = [];
		if ($(this).prop('value') === 'vendor') {
			cost = vendorCost;
			qty = vendorQty;
		} else {
			// cost = $(selectorTr).find("td:eq(11)").text();
			cost = $("#invoice-details-table").DataTable().row(selectorTr).data().hebCost;
			qty = $(selectorTr).find("td:eq(12)").text();
		}
		
		$(selectorTr).find("td:eq(14)").find('input').val(cost);
		$(selectorTr).find("td:eq(15)").find('input').val(qty);
		$(selectorTr).find("td:eq(16)").text($.number(parseFloat(cost) * parseFloat(qty), 2));
		
		if (parseFloat(vendorCost) - parseFloat($(selectorTr).find("td:eq(14)").find('input').val()) !== 0) {
			reason.push("cost");
		} 
		if(parseFloat(vendorQty) - parseFloat($(selectorTr).find("td:eq(15)").find('input').val()) !== 0) {
			reason.push("qty");
		}
		$(selectorTr).find("td:eq(17)").text(reason);
		$(selectorTr).find("td:eq(17)").attr("adj", reason);
		
		//save state
		var itemObj = {
			id: $(selectorTr).attr('id')
		}
		//debugger;
		if (inArrays(items, itemObj)) {
			var listRemove = removeInArrays(items, itemObj.id);
			var listUpdate = updateItems(listRemove, selectorTr);
		}else {
			var listUpdate = updateItems(items, selectorTr);
		}
		items = listUpdate;

		$(selectorTr).addClass('success');
		
		validateDecimal($(selectorTr).find("td:eq(14)").find('input'), Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT, false);
		validateInteger($(selectorTr).find("td:eq(15)").find('input'), Constants.MAX_DIGIT, false);
	});
	
	$(document).on("change",".cost-qty-input", function () {
		//debugger;
		var selectorTr = $(this).parent().parent().parent();
		var currentTd = $(this).parent();
		var cost = $(selectorTr).find("td:eq(14)").find('input').val();
		var qty = $(selectorTr).find("td:eq(15)").find('input').val();
		// var vendorCost = $(selectorTr).find("td:eq(7)").text();
		var vendorCost = $("#invoice-details-table").DataTable().row(selectorTr).data().vendorCost;
		var vendorQty = $(selectorTr).find("td:eq(8)").text();

		$(selectorTr).find("td:eq(14)").find('input').val(cost === "" ? "" : parseFloat(cost).toFixed(Constants.AFTER_DIGIT));
		$(selectorTr).find("td:eq(16)").text($.number(parseFloat(cost) * parseFloat(qty), Constants.AFTER_DIGIT));
		
		if ($(selectorTr).find("td:eq(17)").attr('adj') === undefined || $(selectorTr).find("td:eq(17)").attr('adj') === "") {
			adjReason = [];
		} else {
			adjReason = String($(selectorTr).find("td:eq(17)").attr('adj')).split(',');
		}
		
		if (_.isEmpty($(this).val())) {
			var index = $.inArray($(this).attr('cost-qty'), adjReason); 
			if (index !== -1) {
				adjReason.splice( index, 1 );
			}
		} else {
			var valTemp = 0;
			var index = $.inArray($(this).attr('cost-qty'), adjReason);
			if ($(this).attr('cost-qty') === "cost") {
				valTemp = parseFloat($(this).val()) - parseFloat($(selectorTr).find("td:eq(7)").text());
			}
			if ($(this).attr('cost-qty') === "qty") {
				valTemp = parseFloat($(this).val()) - parseFloat($(selectorTr).find("td:eq(8)").text());
			}
			if (index === -1 && valTemp !== 0) {
				adjReason.push($(this).attr('cost-qty'));
			} else if (index !== -1 && valTemp === 0) {
				adjReason.splice( index, 1 );
			}
		}
		
		if (cost === "" && qty === "") {
			$(selectorTr).find('input:checked').prop('checked', false);
			adjReason = [];
		}
		$(selectorTr).find("td:eq(17)").text(adjReason);
		$(selectorTr).find("td:eq(17)").attr("adj", adjReason);

		var itemObj = {
			id: $(selectorTr).attr('id')
		}
		if (inArrays(items, itemObj)) {
			var listRemove = removeInArrays(items, itemObj.id);
			var listUpdate = updateItems(listRemove, selectorTr);
		}else {
			var listUpdate = updateItems(items, selectorTr);
		}
		items = listUpdate;
		
		$(selectorTr).addClass('success');
		
	});
	
	
	/************************************************************************************************************
	 * MODAL POPUP
	 ************************************************************************************************************/
	
	$(document).on("click",".popup-to-search-orderline", function () {
		//alert("Will open a dialog for searching order line. TBD");
		$("#orderId-input").val($(this).parent().parent().find('td:eq(1)').text());
		
		$("#orderId-input").attr('orderId', $(this).parent().parent().find('td:eq(1)').text());
		$("#orderId-input").attr('date', $(this).parent().parent().find('td:eq(2)').text());
		$("#orderId-input").attr('item', $(this).parent().parent().find('td:eq(3)').text());
		
		/*data send*/
		$('#search-heb-order-modal').modal({
			keyboard : true,
    		backdrop : "static",
    		show : true,
		});
		
	});
	
	$('#search-heb-order-modal').on('shown.bs.modal', function (e) {
		$('#dateEnd, #dateBegin').datepicker({
		    autoclose: true,
		    //todayBtn:"linked",
		    todayHighlight: true
		});
		
		if (!$.fn.DataTable.isDataTable("#search-order-table")) {
			//searchItemsTable = initSearchItemsTable();
			$("#search-order-table tbody tr").remove();
			$("#search-order-table tbody").html('<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">No data available in table</td></tr>');
		}
		//$("#back-order-search").addClass('hidden');
		$(".searchclear").show();
		$("#vendor-input").focus();
	});
	
	$('#search-heb-order-modal').on('hidden.bs.modal', function (e) {
		//debugger;
		resetForm($("#search-heb-header"));
		hiddenModal();
	});
	
	//----------------------------------
	$(document).on("click",".popup-matching-orders-lines", function () {
		//console.log("a");
		$('#matching-orders-lines-modal').modal({
			keyboard : true,
    		backdrop : "static",
    		show : true,
		});

		if ($.fn.DataTable.isDataTable('#matching-orders-table')) {
			$('#matching-orders-table').DataTable().draw();
		} else {
			initMatchingOrdersTable();
		}
		// initSearchHebTable();
	});
	
	/*$('#matching-orders-lines-modal').on('shown.bs.modal', function (e) {
		initMatchingOrdersTable();
	});*/
	
	$(document).on("click","#search-heb-order-btn", function () {
		$("#item-input").attr('value', $("#item-input").val());
		
		if ($.fn.DataTable.isDataTable('#search-order-table')) {
			//console.log("Draw");
			searchHebTable.draw();
		} else {
			//console.log("init");
			searchHebTable = initSearchHebTable();
		}
	});
	
	$(document).on("click","#clear-search-heb-order-btn", function () {
		resetForm($("#search-heb-header"));
		$("#vendor-input").focus();
		$(".searchclear").hide();
	});
	
	/*ADD ITEM -------------------------------------*/
	$(document).on("click","#add-item-search", function () {
		//debugger;
		var oderId = $("#orderId-input").attr('orderid');
		var item = $("#orderId-input").attr('item');
		var date = $("#orderId-input").attr('date');
		
		var trChildChecked =  $("#search-order-table").find("input[type='radio']:checked").closest("tr");
		var tableChildCheckedId = $("#search-order-table").find("input[type='radio']:checked").closest("table").attr('id');
		var parentTableChildCheckedId = tableChildCheckedId.substring(6, tableChildCheckedId.length);
		var rowData = $("#search-order-table").DataTable().row($("#"+parentTableChildCheckedId)).data();
		var invoiceId = $("#invoice-id").attr('value');
    	var currentOrderNbr = rowData.orderNbr;
    	var currentOrderDate = rowData.orderDate;
    	var currentItemId = $("#"+tableChildCheckedId).DataTable().row($(trChildChecked)).data().ItemUPC;
		
		$.ajax({
	        type: "POST",
	        url: appName + "/invoice-details/save-order",
	        data: {
	        	orderNbr: oderId,
	        	itemId: item,
	            orderDate: date,
	            invoiceID: invoiceId,
	            currentOrderNbr: currentOrderNbr,
	            currentOrderDate: currentOrderDate,
	            currentItemId: currentItemId
	        }
	    })
	    .done(function(msg) {
	    	$('#search-heb-order-modal').modal('hide');
	    	hiddenModal();
	    	table.draw();
	    }).fail(function() {
	        alert("error");
	    }).always(function() {
	        //alert("complete");
	    });
	});
	/*END ADD ITEM ---------------------------------*/
	
	$(document).on("click",".searchclear", function () {
		var td = $(this).prev();
		$(td).val("");
		$(td).focus();
		$(this).hide();
	});
	
	$(document).on("keyup","#orderId-input, #item-input", function () {
		var td = $(this);
		if (!_.isEmpty($(td).val())) {
			$(td).next().show();
		} else {
			$(td).next().hide();
		}
	});
	
	$('#search-order-table').on('xhr.dt', function ( e, settings, json ) {
    });
	
	$(document).on("click","#back-order-search", function () {
		$("#orderId-input").val("");
		$("#item-input").val($("#item-input").attr('value'));
		$("#items-wp").addClass('hidden');
		$("#order-wp").removeClass('hidden');
		$(".searchclear").hide();
		$(this).addClass('hidden');
		
	});
	
	$('#search-order-table tbody').on( 'click', '.details-control', function () {
        var tr = $(this).closest('tr');
        var row = $('#search-order-table').DataTable().row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
        var tableId = "table-" +  tr.attr('id');
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        } else {
            tr.addClass( 'details' );
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
			if (row.child() === "" || row.child() === undefined) {
				row.child( format( row.data() ) ).show();
    			initSearchItemsTable(tableId, tr);
			}else {
				row.child.show();
			}
        }
        
    } );
	
	function format ( d ) {
	    return '<table id="table-'+d.DT_RowId+'" class="table table-condensed table-hover table-striped table-bordered pd0 margin-bottom0 sub-table" cellpadding="5" cellspacing="0" border="0" style="padding-left:0px;">'+
			    '<colgroup>'+
			    	'<col class="radio-btn-group">'+
			    	'<col class="item-upc-group">'+
			    	'<col class="desc-group">'+
			    	'<col class="size-group">'+ 
			    	'<col class="cost-group">'+
			    	'<col class="qty-group">'+
			    '</colgroup>'+
	    		'<thead>'+
		    		 '<tr class="first-tr-sub">'+
		    		 	'<th></th>'+
		    		 	'<th>Item#/UPC</th>'+    
				        '<th>Description</th>'+
				        '<th>Size</th>'+
				        '<th>Cost</th>'+
				        '<th>Qty</th>'+
				    '</tr>'+
	    		'</thead>'+
			'</table>';
	}
	
	
	function resetForm(selector) {
		$(selector).find('input[type=text]').each(function(index, value) { 
			$(value).val(""); 
		})
	}
	
	function hiddenModal() {
		if ($.fn.DataTable.isDataTable("#search-order-table")) {
			$("#search-order-table tbody tr").each(function(index, value){			
				var row = $('#search-order-table').DataTable().row(value);
				row.child( false ).remove();
			});
		}
		$("#date-search-heb-order").val("");
		$("#orderId-input").val("");
		$("#item-input").val("");

		if ($.fn.DataTable.isDataTable("#search-order-table")) {
			$("#search-order-table").DataTable().destroy();
		} 

		//$("#search-items-table tbody tr").remove();
		$("#search-order-table tbody tr").remove();
		
		//$("#search-items-table tbody").html('<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">No data available in table</td></tr>');
		$("#search-order-table tbody").html('<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">No data available in table</td></tr>');
		
		$('#dateEnd, #dateBegin').datepicker("remove");
	}
	/************************************************************************************************************
	 * END FUNCTION-MODAL
	 ************************************************************************************************************/
	
});

function exportListDetailsInvoiceToExcel(invoiceId) {
	$("body").append('<div id="temp"></div>')
	$("#temp").append('<form target="_blank" id="frmExcel" action="/assortment/invoice-details/export-to-excel-directly" method="POST"></form>');
	$("#frmExcel").append('<input type="hidden" name="invoiceId" value="'+ invoiceId +'">');
	
	var numberRow = $('#invoice-details-table').DataTable().page.info().recordsTotal;
	
	if(numberRow != null && !isNaN(numberRow)) {
		if(parseInt(numberRow) == 0) {
			alert("Search result is empty!!!");
			$("#temp").remove();
		}else if(parseInt(numberRow) < 1000) {
			$("#frmExcel").removeAttr("target");
			$('#frmExcel').submit();
			$("#temp").remove();
		}else {
			$("#frmExcel").append('<input type="hidden" name="nameProcess" value="invoice-details-list-export-process">');
			$('#frmExcel').attr('action','/assortment/download');
			$('#frmExcel').submit();
			$("#temp").remove();
		}
	}
}


/*SAVE INVOICE-DETAILS----------------------------*/
function saveInvoiceDetail(){
	$.ajax({
	        type: 'POST',
	        url: appName + "/saveDetails",
	        data:{
	        	strJson: JSON.stringify(items)
	        }
	    }).done(function(msg) {
	    	//alert("done !!!");
	    }).fail(function() {
	        //alert("error");
	    }).always(function() {
	        //alert("complete");
	    });	
}
/*END SAVE INVOICE-DETAILS------------------------*/

function renderNotFound() {
	$(".not-found").each(function (index, value) {
		var currentTd = $(value).parent();
		var currentTr = $(value).parent().parent();

		$(currentTd).addClass('text-center');
		$(currentTd).html("");
		
		$(currentTr).find("td:eq(11)").remove();	
		$(currentTr).find("td:eq(12)").remove();
		$(currentTr).find("td:eq(13)").remove();
		$(currentTr).find("td:eq(14)").remove();
		$(currentTr).find("td:eq(15)").remove();
		$(currentTr).find("td:eq(16)").remove();
		
		$(currentTd).attr("colspan", 7);
		$(currentTd).html('<a class="popup-to-search-orderline"><span class="glyphicon glyphicon-search"></span>&nbsp;No Match found, please search and add</a>');
		
		$(currentTd).next().html("");
		$(currentTd).next().next().html("");
		$(currentTd).next().next().next().next().html("");
		$(currentTd).next().next().next().remove();
		$(currentTd).next().next().remove();			
		$(currentTr).find("td:last").html('<span class="deleteRow action glyphicon glyphicon-remove cursor-pointer" style="color:#d9534f; font-size:20px"></span>');
	});
}


function updateItems(items, selectorTr) {
	//debugger;
	var row = {
			id:$(selectorTr).attr('id'),
			select: _.isEmpty($(selectorTr).find("input[type='radio']:checked").attr("value")) ? "" : $(selectorTr).find("input[type='radio']:checked").attr("value"),
			cost: $(selectorTr).find("td:eq(14)").find("input").val(),
			qty: $(selectorTr).find("td:eq(15)").find("input").val(),
			extCost: $(selectorTr).find("td:eq(16)").text(),
			reason: $(selectorTr).find("td:eq(17)").text()
	}
	if (row.cost !== "" || row.qty !== "") {
		items.push(row);	
	}
	return items;
}

function getItemInArraysByKey (itemsTmp, itemObj) {
	return _.where(itemsTmp, itemObj); 
}

function inArrays (itemsTmp, itemObj) {
	return _.size(_.where(itemsTmp, itemObj)) === 0 ? false : true; 
}

function removeInArrays(itemsTmp, id) {
	var val = _.filter(itemsTmp, function(item){ return item.id !== id });
	return val;
}

function fillDataForAdjustment (selectorTr, itemObj) {
	if (itemObj[0].select === 'heb') {
		$(selectorTr).find("td:eq(10)").find('input[type="radio"]').prop('checked', 'true');
	}else if(itemObj[0].select === 'vendor') {
		$(selectorTr).find("td:eq(6)").find('input[type="radio"]').prop('checked', 'true');
	}
	
	$(selectorTr).find("td:eq(14)").find('input').val(_.isEmpty(itemObj[0].cost) ? "" : $.number(itemObj[0].cost, 2));
	$(selectorTr).find("td:eq(15)").find('input').val(_.isEmpty(itemObj[0].qty) ? "" : $.number(itemObj[0].qty, 2));
	$(selectorTr).find("td:eq(16)").text(_.isEmpty(itemObj[0].extCost) ? "" : $.number(itemObj[0].extCost, 2));
	$(selectorTr).find("td:eq(17)").text(itemObj[0].reason);
}

/************************************************************************************************************
 * FUNCTION-TABLE
 ************************************************************************************************************/

function initInvoiceDetailsTable() {
	var table = $('#invoice-details-table').DataTable( {
		"columnDefs": [
			{
				"searchable": false,
				"orderable": false,
				"targets": [0, 4, 5, 7, 8, 9, 11, 12, 13, 16, 17]
			},
			{
				"render": function ( data, type, row ) {
	            	return '<input id="" class="checkbox-table" value="vendor" type="radio" name="'+data+'">';
	            },
				"searchable": false,
				"orderable": false,
				"targets": 6
			},
			/*{
				"render": function ( data, type, row ) {
	            	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
	            },
				"targets": [7, 9, 11, 13, 16]
			},*/
			{
				"render": function ( data, type, row ) {
				  if (row.matchFlag === "1") {
					  return '<input id="" class="checkbox-table not-found" value="heb" type="radio" name="'+data+'">';		  
				  } else {
					  return '<input id="" class="checkbox-table" value="heb" type="radio" name="'+data+'">';
				  }
					
	            },
				"searchable": false,
				"orderable": false,
				"targets": 10
			},
			{
				"render": function ( data, type, row ) {
		    		// return '<div class="input-group input-group-sm form-group"> <span class="input-group-addon input-group-addon-cell">$</span> <input type="text" maxlength="9" class="form-control input-sm text-right pd0 currency-format-input format-input-cell cost-qty-input number-input" name="cost" cost-qty="cost"></div>';
		    		return '<div class="input-group input-group-sm form-group"> <input type="text" maxlength="9" class="form-control input-sm text-right pd0 currency-format-input format-input-cell cost-qty-input number-input" name="cost" cost-qty="cost"></div>';
	            },
				"searchable": false,
				"orderable": false,
				"targets": 14
			},
			{
				"render": function ( data, type, row ) {
	            	return '<div class="form-group"><input class="input-sm form-control text-right cost-qty-input" cost-qty="qty" type="text" placeholder="" id="" name="qty"></div>';
	            },
				"searchable": false,
				"orderable": false,
				"targets": 15
			},
			{
				"render": function ( data, type, row ) {
	           		return '<span class="deleteRow action glyphicon glyphicon-remove cursor-pointer" style="color:#d9534f; font-size:20px"></span>';
	            },
				"searchable": false,
				"orderable": false,
				"targets": 18
			},
			{
				"width": "2%",
				"targets": 0
			},
			{
				"width": "5%",
				"targets": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 17, 18]
			},
			{
				"width": "9%",
				"targets": [14, 15]
			}
		 ],
		 //	"order": [[ 1, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],
    	"sDom": "tripl",
    	"bSort" : true,
    	"orderMulti": false,
    	"pagingType": "full_numbers",
    	"stateSave": false,
		"processing": false,
	    "serverSide": true,
	    "ajax": {
	        "url": appName + '/invoice-details/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	d.invoiceId = $("#invoice-id").attr('value');
            }
	    },
	    "columns": [
	                { "data": "no", "className": "no-cls" },
	                { "data": "orderNbr", "className": "orderNbr-cls" },
	                { "data": "orderDate", "className": "orderDate-cls"},
	                { "data": "itemUPC", "className": "itemUPC-cls" },
	                { "data": "description", "className": "description-cls" },
	                { "data": "size", "className": "size-cls" },
	                { "data": "vendorSelect", "className": "vendorSelect-cls" },
	                { "data": "vendorCost", "className": "vendorCost-cls" },                
	                { "data": "vendorQuantity", "className": "vendorQuantity-cls" },
	                { "data": "vendorExtendedCost", "className": "vendorExtendedCost-cls" },
	                { "data": "hebSelect", "className": "hebSelect-cls" },
	                { "data": "hebCost", "className": "hebCost-cls" },
	                { "data": "hebQuantity", "className": "hebQuantity-cls" },
	                { "data": "hebExtendedCost", "className": "hebExtendedCost-cls" },
	                { "data": "finalCost", "className": "finalCost-cls" },
	                { "data": "finalQuantity", "className": "finalQuantity-cls" },
	                { "data": "finalExtendedCost", "className": "finalExtendedCost-cls" },                
	                { "data": "adjustmentReason", "className": "adjustmentReason-cls" },
	                { "data": "deleteBtn", "className": "deleteBtn-cls" }
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		},
		"fnDrawCallback": function( oSettings ) {
			// formatCurrency ();
			renderNotFound();
			validateDataTableForm();
			
		},
		"initComplete": function (settings, json) {
			$("#total-matching").text(json.match);
		},
		"rowCallback": function( row, data ) {		  
			 /* add class for cell */
			  $('td:eq(0)', row).addClass('text-right');
			  $('td:eq(1)', row).addClass('text-left');
			  $('td:eq(2)', row).addClass('text-center');
			  $('td:eq(3)', row).addClass('text-left');
			  $('td:eq(4)', row).addClass('text-left text-ellipsis');
			  $('td:eq(5)', row).addClass('text-left');
			  $('td:eq(6)', row).addClass('text-center');
			  
			  $('td:eq(7)', row).addClass('text-right');
			  $('td:eq(8)', row).addClass('text-right');
			  $('td:eq(9)', row).addClass('text-right');
			  $('td:eq(10)', row).addClass('text-center');
			  $('td:eq(11)', row).addClass('text-right');
			  $('td:eq(12)', row).addClass('text-right');
			  
			  $('td:eq(13)', row).addClass('text-right');
			  $('td:eq(14)', row).addClass('');
			  $('td:eq(15)', row).addClass('');
			  $('td:eq(16)', row).addClass('text-right');
			  $('td:eq(17)', row).addClass('text-left');
			  $('td:eq(18)', row).addClass('text-center');
			  
			  $('td:eq(4)', row).attr( "title", data.description);
			  
		}     
	});

	return table;
}
	
	
function initSearchHebTable () {
	var table = $('#search-order-table').DataTable( {
		"autoWidth": false,
		"columnDefs": [
			{
				"render": function ( data, type, row ) {
	            	return '<span class="link-order-details" order-date-val="'+row.orderDate+'" order-nbr='+row.orderNbr+'>'+data+'</span>';
	            },
				"searchable": false,
				"orderable": false,
				"targets": 1
			},
			{
				"render": function ( data, type, row ) {
	            	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
	            },
				"targets": 4
			}
		 ],
		"order": [[ 1, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100],
		    [10, 25, 50, 100]
		],
    	"sDom": "tripl",
    	"bSort" : false,
    	"orderMulti": false,
    	"pagingType": "full",
    	"stateSave": false,
		"processing": false,
	    "serverSide": true,
	    "ajax": {
	        "url": appName + '/invoice-details/search-heb-order',
	        "type": 'GET',
	        "data": function ( d ) {
	        	d.invoiceId = $("#invoice-id").attr('value');
	        	d.orderNbr = $("#orderId-input").val(),
	        	d.orderDateBegin = $("#dateBegin").val(),
	        	d.orderDateEnd = $("#dateEnd").val(),
	        	d.itemId = $("#item-input").val(),
	        	d.vendorID = $("#vendor-input").attr('vendorid') === undefined ? $("#vendor-input").val() : $("#vendor-input").attr('vendorid')
            }
	    },
	    "columns": [ 
	                {
		                "class":"details-control",
		                "orderable":false,
		                "data":null,
		                "defaultContent": ""
	                },
	                { "data": "orderNbr" },
	                { "data": "orderDate" },
	                { "data": "vendorID" },
	                { "data": "extendedCost" }
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		},
		"preDrawCallback": function( settings ) {
		},
		"fnDrawCallback": function( oSettings ) {
		},
		"initComplete": function (settings, json) {
			$("#total-matching").text(json.match);
		},
		"rowCallback": function( row, data ) {		  
			 /* add class for cell */
			  //$('td:eq(0)', row).addClass('text-left');
			  $('td:eq(1)', row).addClass('text-center');
			  $('td:eq(2)', row).addClass('text-center');
			  $('td:eq(4)', row).addClass('text-right currency-format');
		}     
	});
	
	return table;
}

function initSearchItemsTable (selectorId, tr) {
	var table = $('#'+ selectorId).DataTable( {
		"autoWidth": false,
		"columnDefs": [
			{
				"render": function ( data, type, row ) {
			    	return '<input id="" class="checkbox-table" value="vendor" type="radio" name="test">';;
			    },
				"searchable": true,
				"orderable": true,
				"targets": 0
			},
			{
				"render": function ( data, type, row ) {
			    	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
			    },
				"targets": 4
			},
			{
				"searchable": false,
				"orderable": false,
				"targets": [1, 2, 3, 4]
			}
		 ],
		"order": [[ 0, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100],
		    [10, 25, 50, 100]
		],
    	"sDom": "",//tripl
    	"bSort" : false,
    	"orderMulti": false,
    	"paging": false,
    	//"pagingType": "full",
    	"stateSave": false,
    	//"processing": false,
	    "serverSide": false,
	    "ajax": {
	        "url": appName + '/invoice-details/search-items',
	        "type": 'GET',
	        "data": function ( d ) {
	        	d.orderNbr = $(tr).find("td:eq(1)").find(".link-order-details").attr('order-nbr'),
	        	d.orderDate = $(tr).find("td:eq(1)").find(".link-order-details").attr('order-date-val'),
	        	d.vendorID = $("#vendor-input").attr('vendorid') === undefined ? "" : $("#vendor-input").attr('vendorid')	
            }
	    },
	    "columns": [
	                { "data": "radioBtn" },
	                { "data": "ItemUPC" },
	                { "data": "Description" },
	                { "data": "size" },
	                { "data": "Cost" },
	                { "data": "Quantity" }
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		},
		"fnDrawCallback": function( oSettings ) {
			if (_.size(oSettings.aoData) > 0) {
				$("#orderId-input").val(oSettings.aoData[0]._aData.orderNbr);
			}
			//$("#orderId-input").val(_.size(oSettings.aoData) > 0 ? oSettings.aoData[0]._aData.orderNbr : "");
			if(!_.isEmpty($("#orderId-input").val())){
				$("#searchclear-orderId").show();
			}
			// formatCurrency();
		},
		"initComplete": function (settings, json) {
			$("#total-matching").text(json.match);
		},
		"rowCallback": function( row, data ) {		  
			 /* add class for cell */
			$('td:eq(0)', row).addClass('text-center');
			$('td:eq(1)', row).addClass('text-left')
			$('td:eq(2)', row).addClass('text-left');
			$('td:eq(3)', row).addClass('text-center');
			$('td:eq(4)', row).addClass('text-right');
			$('td:eq(5)', row).addClass('text-right');
		}     
	});
	
	return table;
}

function initMatchingOrdersTable() {
	var table = $('#matching-orders-table').DataTable( {
		"columnDefs": [
			{
				"orderable": false,
				"targets": 0
			},
			{
				"width": "2%",
				"targets": 0
			},
			{
				"width": "7%",
				"targets": [1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
			},
			{
				"width": "*",
				"targets": 4
			}
		 ],
		 "order": [[ 1, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],
    	"sDom": "tripl",
    	"bSort" : true,
    	"orderMulti": false,
    	"pagingType": "full_numbers",
    	"stateSave": false,
		"processing": false,
	    "serverSide": true,
	    "ajax": {
	        "url": appName + '/invoice-details/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	d.invoiceId = $("#invoice-id").attr('value');
            }
	    },
	    "columns": [
	                { "data": "no", "className": "no-cls" },
	                { "data": "orderNbr", "className": "orderNbr-cls" },
	                { "data": "orderDate", "className": "orderDate-cls"},
	                { "data": "itemUPC", "className": "itemUPC-cls" },
	                { "data": "description", "className": "description-cls" },
	                { "data": "size", "className": "size-cls" },
	                { "data": "vendorSelect", "className": "vendorSelect-cls" },
	                { "data": "vendorCost", "className": "vendorCost-cls" },                
	                { "data": "vendorQuantity", "className": "vendorQuantity-cls" },
	                { "data": "vendorExtendedCost", "className": "vendorExtendedCost-cls" },
	                { "data": "hebSelect", "className": "hebSelect-cls" },
	                { "data": "hebCost", "className": "hebCost-cls" },
	                { "data": "hebQuantity", "className": "hebQuantity-cls" },
	                { "data": "hebExtendedCost", "className": "hebExtendedCost-cls" },
	                // { "data": "finalCost", "className": "finalCost-cls" },
	                // { "data": "finalQuantity", "className": "finalQuantity-cls" },
	                { "data": "finalExtendedCost", "className": "finalExtendedCost-cls" }
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		},
		"fnDrawCallback": function( oSettings ) {
			renderNotFound();
			$("#invoice-details-table thead tr th:eq(0)").removeClass('sorting_asc');
		},
		"initComplete": function (settings, json) {
			$("#total-matching").text(json.match);
		},
		"rowCallback": function( row, data ) {		  
			 /* add class for cell */
			  $('td:eq(0)', row).addClass('text-right');
			  $('td:eq(1)', row).addClass('text-left');
			  $('td:eq(2)', row).addClass('text-center');
			  $('td:eq(3)', row).addClass('text-left');
			  $('td:eq(4)', row).addClass('text-left text-ellipsis');
			  $('td:eq(5)', row).addClass('text-left');
			  $('td:eq(6)', row).addClass('text-center');
			  
			  $('td:eq(7)', row).addClass('text-right');
			  $('td:eq(8)', row).addClass('text-right');
			  $('td:eq(9)', row).addClass('text-right');
			  $('td:eq(10)', row).addClass('text-right');
			  $('td:eq(11)', row).addClass('text-right');
			  $('td:eq(12)', row).addClass('text-right');
			  
			  $('td:eq(13)', row).addClass('text-right');
			  $('td:eq(14)', row).addClass('text-right');
			  $('td:eq(4)', row).attr( "title", data.description);
			  
		}     
	});
	return table;
}
/************************************************************************************************************
 * END FUNCTION-TABLE
 ************************************************************************************************************/

/**
 * validate data input on dataTable.
 * @author giaule
 */
function validateDataTableForm(){
	//validation
	$('#invoice-detail-form input[name="cost"]').change(function() {
       if (validateDecimal(this, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT, false)) {
       		$('#invoice-details-save-btn').attr('disabled', false);
       }else{
       		$('#invoice-details-save-btn').attr('disabled','disabled');
       }
    });
    $('#invoice-detail-form input[name="qty"]').change(function() {
       if (validateInteger(this, Constants.MAX_DIGIT, false)) {
       		$('#invoice-details-save-btn').attr('disabled', false);
       }else{
       		$('#invoice-details-save-btn').attr('disabled','disabled');
       }
    });

    $("#invoice-details-table").on('keydown', '.cost-qty-input', function(event) {
		if($(this).attr("name") === "cost") {
			acceptDecimalNumber(this, event, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
		}else if($(this).attr("name") === "qty") {
			acceptNumber(this, event, Constants.MAX_DIGIT);
		}
	});
}

/**
 * init datepicker.
 * @author giaule
 */
function datepickerOrderSearch(){
	$("#order-date-begin,#order-date-end").datepicker({
		autoclose : false,
		forceParse: false,
		endDate: '0d'
	}).on('changeDate', function(e) {
		validateTwoDateCompare("order-date-begin", "order-date-end");
    });
	forceDateFromAndTo( '#order-date-begin', '#order-date-end' );
	$("#order-date-begin-icon").click(function() {
		$("#order-date-begin").trigger('focus');
	});

	$("#order-date-end-icon").click(function() {
		$("#order-date-end").trigger('focus');
	});
}