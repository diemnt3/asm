//var common = new Common();
//var location= common.getAppName(window.location.pathname);
//console.log(location);
var Constants = {
		EMPTY_STRING : "",
		SPACE_STRING : " ",
		AFTER_DIGIT: 2,
		TOTAL_NUMBER: 9, 
		TOTAL_NUMBER_INTEGER: 38, 
		INVALID_VALUE : "Invalid value",
		INVALID_MARGIN : "Maximum margin should be greater than Minimum margin",
		INVALID_DATE : '"To date"  should be greater than "From date"',
		REJECT_PRODS_PATH : "/product-review/rejectProduct",
		APPROVE_PRODS_PATH : "/product-review/approveProduct",
		UN_REJECT_PRODS_PATH : "/product-review/un-rejectProduct",
		SAVE_PRODS_PATH : "/product-review/updateProduct",
		AJAX_COMMONDITIES : "/ajax/get-data-autocomplete-commodities",
		AJAX_VENDOR : "/ajax/get-data-autocomplete-vendor",
		AJAX_BRAND : "/ajax/get-data-autocomplete-brand",
		AJAX_PRODUCT : "/ajax/get-data-autocomplete-product",
		msgLoseChange :"You will lose your changes, do you wish to continue?",
		msgHebRetailLimit : "Only the value between 0 and 100000 is accepted",
		msgRequiredInput : "This field is required."
};

function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("span.indicator")
        .toggleClass('glyphicon-minus glyphicon-plus');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
var resetCount = true;
//$('.check-all').change(function() {
//    $('.body-table tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
//});
var selectedAll = [];
var selectedPassed = [];
var selectedFailed = [];
var selectedReject = [];
var searchBdmValue = [];
var allTable;
var passTable;
var failedTable;
var rejectTable;
$(document).ready(function() {
	$(document).on('keyup input', '.hebRetail-class', function(event){
	 	checkHebRetailInput(this);
	});
	$(document).on('blur', '.input-number, #product-input, #brand-input', function(event){
	 	$(this).val($.trim($(this).val()));
	});

	/************************************************************************************************************
	 * Header
	 ************************************************************************************************************/
	/*INIT COMMODITIES AUTOCOMPLETE*/
	initDataForAutocomplete("commodities-input", Constants.AJAX_COMMONDITIES);
	/*INIT VENDOR AUTOCOMPLETE*/
	/*INIT BRAND AUTOCOMPLETE*/
	initAutoCompleteSearch("brand-input", Constants.AJAX_BRAND);
	/*INIT BRAND AUTOCOMPLETE*/
//	initAutoCompleteSearch("product-input", Constants.AJAX_PRODUCT);
	/*INIT FAILED REASON  COMBOBOX*/
	initDataForCombobox("failed-input", "/ajax/get-data-failed-reason");
	/*INIT VENDOR  COMBOBOX*/
	initDataForCombobox("vendor-input", Constants.AJAX_VENDOR);
	/*START VALIDATION FORM*/
	var dateBeginValue = new Date();
	dateBeginValue.setDate(dateBeginValue.getDate() - 7);
	$("#dateBegin").datepicker("setDates", dateBeginValue);
	$("#dateEnd").datepicker("setDates", new Date());
	/*END VALIDATION FORM*/
	/*Click button search-----------*/
	$("#search-new-product-btn").click(function() { 
		var tabActive = getTabActive();
		if(isEmptyValueSearchForm()) {
			showAlertDanger("#alert-bdm-form","Search criteria is required");
		} else if(checkValidateSearchFrom()){
			if(isChanges()) {
				bootbox.dialog({
					closeButton: true,
					title : "Confirmation",
					message: Constants.msgLoseChange,
					buttons: {
						main: {
							label: "Yes",
							className: "btn-primary",
							callback: function() {
								searchFuntion();
							}	
						},	
						cancel: {
							label: "No",
							className: "btn-default",
							callback : function(){				
											   
							}
						}
					}
				});
			} else {
				searchFuntion();
			}
		}
    });
    $("#reset-new-product-btn").click(function() { 
		resetFuntion();
		// searchFuntion(true);
    });
	/*end click button search-------*/
	
	/*collapse--------------------------*/
	$('#bdm-header').on('shown.bs.collapse', function () {
		if(isEmptyValueSearchForm()) {
			showAlertDanger("#alert-bdm-form","Search criteria is required");
		} 
		$("#fa-angle-double-down-style").removeClass('fa-angle-double-right');
		$("#fa-angle-double-down-style").addClass('fa-angle-double-down');
	})
	
	$('#bdm-header').on('hidden.bs.collapse', function () {
		removeAlert("#alert-bdm-form");
		$("#fa-angle-double-down-style").removeClass('fa-angle-double-down');
		$("#fa-angle-double-down-style").addClass('fa-angle-double-right');
	})
	/*end collapse-----------------------*/
	$('table thead').on("change", 'input:text', function() { 
		if($(this).hasClass('input-decimal') || $(this).hasClass('negative-number')){
			$(this).attr('title', $.number($(this).val(), 2));
		} else {
			$(this).attr('title', $(this).val());
		}
		
	});
	
	/************************************************************************************************************
	 * end header
	 ************************************************************************************************************/
	
	/************************************************************************************************************
	 * Search Results
	 ************************************************************************************************************/
	 searchBdmValue = setValueFormSearch();
	/*init datatable----------------*/
	var allTable = initDataTable("all", selectedAll);
	/*end init datatable------------*/
	/*tab---------------------------*/
	$('#tabResult a').click(function(e) {
	    e.preventDefault()
		$(this).tab('show')
	});
	$('#tabResult li').keypress(function(event) {
	  var code = event.keyCode || event.which;
		if(code == 13) {
			 $(this).find('a').click();
		}
	});
	$("#bdm-header .input-sm").keypress(function(event) {
		var code = event.keyCode || event.which;
		if(code == 13) {
			 $("#search-new-product-btn").click();
		}
	});
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		tabActive = $(e.target).attr('value');
		$('#' + tabActive +'-table thead th.has-error .input-sm').each(function () {
			clearTooltipForValidationBDM("#"+$(this).attr('id'));
			$(this).val("");
		});
		if (tabActive === 'all') {
			if (!$("#all-table").hasClass('dataTable')) {
				allTable = initDataTable("all", selectedAll);
			} else {
				allTable.draw();
			}
		}
		else if (tabActive === 'passed') {
			if (!$("#passed-table").hasClass('dataTable')) {
				passTable = initDataTable("passed", selectedPassed);
			} else {
				passTable.draw();
			}
		}
		else if (tabActive === 'failed') {
			if (!$("#failed-table").hasClass('dataTable')) {
				failedTable = initDataTable("failed", selectedFailed);
			}else {
				failedTable.draw();
			}
		}
		else if (tabActive === 'rejected') {
			if (!$("#rejected-table").hasClass('dataTable')) {
				rejectTable = initDataTable("rejected", selectedReject);
			}else {
				rejectTable.draw();
			}
		}
		 var numberRowTabActive = getNumberRowTabActive();
        if(_.isEmpty(numberRowTabActive) || parseInt(numberRowTabActive) == 0){
       			 	$("#product-review-export-btn").addClass('disable-class');
        } else {
       			 	$("#product-review-export-btn").removeClass('disable-class');
       	}
	})
	/*end tab-----------------------*/
		/*Export Excel--------------------*/	
	$("#product-review-export-btn").click(function() {
        $("body").append('<div id="temp"></div>');
        $("#temp").append('<form target="_blank" id="frmExcel" action="/assortment/new-product-review/export-to-excel-directly" method="POST"></form>');                
        $("#frmExcel").append('<input type="hidden" name="commodittyID" value="'+searchBdmValue['0']+'">');
        $("#frmExcel").append('<input type="hidden" name="brand" value="'+searchBdmValue['2']+'">');
        $("#frmExcel").append('<input type="hidden" name="dateBegin" value="'+searchBdmValue['4']+'">');
        $("#frmExcel").append('<input type="hidden" name="dateEnd" value="'+ searchBdmValue['5']+'">');
        $("#frmExcel").append('<input type="hidden" name="marginBegin" value="'+searchBdmValue['6']+'">');
        $("#frmExcel").append('<input type="hidden" name="marginEnd" value="'+searchBdmValue['7']+'">');
        $("#frmExcel").append('<input type="hidden" name="failReason" value="'+searchBdmValue['8']+'">');
        $("#frmExcel").append('<input type="hidden" name="vendorID" value="'+searchBdmValue['1']+'">');
        $("#frmExcel").append('<input type="hidden" name="prodDes" value="'+searchBdmValue['3']+'">');
        $("#frmExcel").append('<input type="hidden" name="showAllMapCheck" value="'+searchBdmValue['9']+'">');
        $("#frmExcel").append('<input type="hidden" name="showOnlyPrePriceCheck" value="'+searchBdmValue['10']+'">');     
        var type = getTabActive();      
        var aaSorting = $("#" + type + '-table').dataTable().fnSettings().aaSorting;
		// sort direction.
		var sSortDir = typeof aaSorting[0][1] !== 'undefined' ? aaSorting[0][1] : 'asc';
		// Sort column.
		var iSortColumn = typeof aaSorting[0][0] !== 'undefined' ? aaSorting[0][0] : '1';

		$("#frmExcel").append('<input type="hidden" name="sSortDir" value="' + sSortDir + '">');
		$("#frmExcel").append('<input type="hidden" name="iSortColumn" value="' + iSortColumn + '">');

        var tabActive = '0';
        if (getTabActive() === 'all') {
            tabActive = '0';
        } else if (getTabActive() === 'passed') {
            tabActive = '1';
        } else if(getTabActive() === 'failed') {
            tabActive = '2';
        } else {
            tabActive = '3';
        }
        $("#frmExcel").append('<input type="hidden" name="status" value="'+tabActive+'">');
        var numberRowTabActive = getNumberRowTabActive();
        if(numberRowTabActive != null && !isNaN(numberRowTabActive)) {
            if(parseInt(numberRowTabActive) == 0) {
                // alert("Search result is empty!!!");
                $("#temp").remove();
            }else {
            	$('#frmExcel').submit();
                $("#temp").remove();
            } 

            // if(parseInt(numberRowTabActive) < 2000) {
            //     $("#frmExcel").removeAttr("target");
            //     $('#frmExcel').submit();
            //     $("#temp").remove();
            // }else {
            //     $("#frmExcel").append('<input type="hidden" name="nameProcess" value="new-product-review-export-process">');
            //     $('#frmExcel').attr('action','/assortment/download');
            //     $('#frmExcel').submit();
            //     $("#temp").remove();
            // }
        } 
    });
	/*End export Excel----------------*/

	/************************************************************************************************************
	 * end search results
	 ************************************************************************************************************/

    /************************************************************************************************************
	 * Function
	 ************************************************************************************************************/
    /*Reset filter------------------*/
    function resetFilter() {
		var type = getTabActive();
		$(".second-tr-" + type).find('th:not(:first)').each(function (){
				$(this).find("input").val("");
				$(this).find("select").val("");
		});
		$(".second-tr-" + type + ' th.has-error').find('.input-sm').each(function (){
				clearTooltipForValidationBDM("#"+$(this).attr('id'));
		});
		$("#search-first-received-btn-" + type).attr('title',"");
		$("#search-first-received-btn-" + type).find('.from-date-val').val("");
	    $("#search-first-received-btn-" + type).find('.to-date-val').val("");
	    $("#search-last-edited-btn-" + type).attr('title',"");
	    $("#search-last-edited-btn-" + type).find('.from-date-val').val("");
 		$("#search-last-edited-btn-" + type).find('.to-date-val').val("");
		if (getTabActive() === 'all') {
			allTable.columns($("#" + type + '-table  ' + '.second-tr > th > input:text')).search($("#" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
			//$("#search-upc-btn-passed").trigger("change");
		} else if (getTabActive() === 'passed') {
			passTable.columns($("#" + type + '-table  ' + '.second-tr > th > input:text')).search($("#" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
			//$("#search-upc-btn-passed").trigger("change");
		} else if(getTabActive() === 'failed') {
			failedTable.columns($("#" + type + '-table  ' + '.second-tr > th > input:text')).search($("#" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
			//$("#search-upc-btn-failed").trigger("change");
		} else {
			rejectTable.columns($("#" + type + '-table  ' + '.second-tr > th > input:text')).search($("#" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
			//$("#search-upc-btn-rejected").trigger("change");
		}
	}
    /*end reset filter--------------*/
	
	/*Get active--------------------*/
	function getTabActive() {
		var tabActive = "all";
		if ($("#tab-all-li").hasClass('active')) {
			tabActive = "all";
		}else if ($("#tab-passed-li").hasClass('active')) {
			tabActive = "passed";
		}else if ($("#tab-failed-li").hasClass('active')) {
			tabActive = "failed";
		} else if($("#tab-rejected-li").hasClass('active')) {
			tabActive = "rejected";
		}		
		return tabActive;
	}
	/*end get active----------------*/
	
	/*getNumberRowTabActive-----------*/
	function getNumberRowTabActive() {
		var type = getTabActive();
		var numberRowStr;
		if(type == "all") {
			numberRowStr = $("#badge-all").html();
		}else if(type == "passed") {
			numberRowStr = $("#badge-passed").html();
		}else if(type == "failed") {
			numberRowStr = $("#badge-failed").html();
		}else {
			numberRowStr = $("#badge-rejected").html();
		}
		return $.trim(numberRowStr).substring(1, numberRowStr.length - 1);
	}
	/*end getNumberRowTabActive-------*/
	
	/*INIT TABLE----------------------*/
	function initDataTable(type, selected) {

		var table = $('#'+ type + '-table').DataTable( {
			"autoWidth": false,
			"columnDefs": [ {
				"searchable": false,
				"orderable": false,
				"targets": [0,1,13,14,15,16]
			},
			{
				"render": function ( data, type, row ) {
	            	return '<span>'+data+'</span>';
	            },
				"targets": [3, 14]
			},
			{
				"render": function ( data, type, row ) {
			    	return '<span class="currency-format">'+ data +'</span>';
			    },
				"targets": [6,7,8, 9]
			},
			{
				"render": function ( data, type, row ) {
	            	return '<span class="currency-format">' +data+' </span><input type="text" class="form-control input-sm input-decimal text-right pd0 currency-format-tmp hidden" tmp="'+data+'" value="'+data+'">';
	            },
				"targets": [11]
			},	
			{
				"render": function ( data, type, row ) {
	            	return '<span class="currency-format">' +data+' </span><input type="text" class="form-control input-sm input-decimal text-right pd0 currency-format-tmp hidden" tmp="'+data+'" value="'+data+'">';
	            },
				"targets": [10]
			},	
			{
				"render": function ( data, type, row ) {
					return '<div class="input-group input-group-sm"><input type="text" class="form-control input-sm input-decimal text-right pd0 currency-format-input format-input-cell hebRetail-class" tmp="'+data+'" value="'+data+'" title="'+data+'""></div>';
	            },
				"targets": [12]
			},
			{
				"render": function ( data, type, row ) {
	            	return '<span class="show-competitor cursor-pointer" data-html="true" data-container="body" data-toggle="popover" data-placement="left" data-content="<div><img  width = 150px height =150px src='+data+'></div>"><img  width = "25px" height ="25px" src="'+data+'"></span>';
	            },
				"targets": [13]
			}
			],
			"aLengthMenu": [
			    [10, 25, 50, 100, 200, 500],
			    [10, 25, 50, 100, 200, 500]
			],
	    	"sDom": '<"wp-table-1" <"wp-table-2" <"wp-table-3"t>r> ipl>',
	    	"pagingType": "full_numbers",
	    	"orderMulti": false,
	    	"stateSave": false,
			"processing": false,
		    "serverSide": true,
		    "ajax": {
		        "url": appName + '/product-review/search-product',
		        "type": 'POST',
		        "data": function ( d ) {
		        	d.commodittyId = searchBdmValue['0'];
			        d.vendorId = searchBdmValue['1'];
			        d.brand = searchBdmValue['2'];
			        d.prodDes = searchBdmValue['3'];
			        d.dateBegin = searchBdmValue['4'];
			        d.dateEnd = searchBdmValue['5'];
			        d.marginBegin = searchBdmValue['6'];
			        d.marginEnd = searchBdmValue['7'];
			        d.failReason = searchBdmValue['8'];
			        d.allMapProduct = searchBdmValue['9'];
		            d.onlyPrePriceProduct = searchBdmValue['10'];
		        	/*---------------------------------*/
	                d.upcIdFilter = $.trim($("#search-upc-btn-" + type).val());
	                d.prodDescFilter = $.trim($("#search-description-btn-" + type).val());
	                d.prodSizeFilter = $.trim($("#search-size-btn-" + type).val());
	                d.mapFilter = $.trim($("#search-map-btn-" + type).val());
	                d.prePriceFilter = $.trim($("#search-prePrice-btn-" + type).val());
	                d.msrpFilter = $.trim($("#search-msrp-btn-" + type).val());
	                d.srpFilter = $.trim($("#search-srp-btn-" + type).val());
	                d.costFilter = $.trim($("#search-cost-btn-" + type).val());
	                d.perGpFilter = $.trim($("#search-gp-btn-" + type).val());
	                d.hebRetailFilter = $.trim($("#search-heb-retail-btn-" + type).val());
	                d.pennyProfitFilter = $.trim($("#search-penny-btn-" + type).val());
 					d.failReasonFilter = $.trim($("#search-failed-btn-" + type).val());
 					d.fromReceivedTSFilter = $.trim($("#search-first-received-btn-" + type).find('.from-date-val').val());
	                d.toReceivedTSFilter = $.trim($("#search-first-received-btn-" + type).find('.to-date-val').val());
	                d.fromEditedTSFilter = $.trim($("#search-last-edited-btn-" + type).find('.from-date-val').val());
 					d.toEditedTSFilter = $.trim($("#search-last-edited-btn-" + type).find('.to-date-val').val());
	    
	               	var tabActive = '0';
	                if (getTabActive() === 'all') {
	                	tabActive = '0';
					} else if (getTabActive() === 'passed') {
						tabActive = '1';
					} else if(getTabActive() === 'failed') {
						tabActive = '2';
					} else {
						tabActive = '3';
					}
	                d.status = tabActive;
	            }
		    },
		    "columns": [
		                { "data": "no" },
		                { "data": "no" },
		                { "data": "upc"},
		                { "width":"8%","data": "description" },
		                { "width":"5%","data": "size" },
		                { "width":"5%","data": "map" },
		                { "width":"6%","data": "prePrice" },
		                { "width":"6%","data": "msrp" },
		                { "width":"6%","data": "srp" },
		                { "width":"6%","data": "cost" },
		                { "width":"6%","data": "gppercent" },
		                { "width":"6%","data": "pennyProfit" },
		                { "width":"7%", "data": "hebRetail" },
		                { "data": "image" },
		                { "data": "failedReasonCode"},
		                { "data": "reviewStatus"}, 
		                { "data": "webStatusCd" },
		                { "width":"9%","data": "firstReceivedTS"},
		                { "width":"7%","data": "lastEditedTS"} 
		     ],
		    "oLanguage" : {
				"sLengthMenu" : "Show _MENU_ rows",
				"sInfoEmpty" : "No records to show",
				"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
			},
			"fnDrawCallback": function( oSettings ) {
				var type = getTabActive();
				if (type === 'all') {
					selected = selectedPassed.concat(selectedFailed).concat(selectedReject);
				} else if (type === 'passed') {
					selected = selectedPassed;
				} else if(type === 'failed') {
					selected = selectedFailed;
				} else {
					selected = selectedReject;
				}
				formatCurrency();
				controlCheckAll(type);
				controlVisibleBtn(type);
				controlButtonBDMReview(selected, type);
			 },
			 "initComplete": function (settings, json) {
					if (Common.getIEVersion() > 0) {
						var width = 150;
						setTimeout(function(){
							$("#passed-table .text-ellipsis span").each(function(){
								$(this).width(width -10 + "px");
							});
						}, 1000);
					}
			
					$(window).resize(function() {
						if (Common.getIEVersion() > 0) {
							var width = $("#passed-table .firs-tr th:nth-child(4)").width();
							setTimeout(function(){
								$("#passed-table .text-ellipsis span").each(function(){
									$(this).width(width -10 + "px");
								});
							}, 1000);
						}
					});
			 	var tabActive = getTabActive();
				$('#search-failed-btn-' + tabActive).html($('#failed-input').html());
				$('.disabled-input-' + tabActive ).prop('disabled', true);
			 },
			 /* selected */
			 "rowCallback": function( row, data ) {
				 	if (getTabActive() === 'all') {
						selected = selectedPassed.concat(selectedFailed).concat(selectedReject);
					} else if (getTabActive() === 'passed') {
						selected = selectedPassed;
					} else if(getTabActive() === 'failed') {
						selected = selectedFailed;
					} else {
						selected = selectedReject;
					}

				  /* add class for cell */
				  $('td:eq(0)', row).addClass('text-center checkbox-class sorting_disabled').html("<input type='checkbox' value=''>");
				  $('td:eq(1)', row).addClass('text-left');
				  $('td:eq(2)', row).addClass('text-left');
				  $('td:eq(3)', row).addClass('text-left text-ellipsis').attr( "title", data.description);
				  $('td:eq(4)', row).addClass('text-right');
				  $('td:eq(5)', row).addClass('text-right currency-format');
				  $('td:eq(6)', row).addClass('text-right');
				  $('td:eq(7)', row).addClass('text-right');
				  $('td:eq(8)', row).addClass('text-right');
				  $('td:eq(9)', row).addClass('text-right');
				  $('td:eq(10)', row).addClass('text-right')//currency-format
				  $('td:eq(11)', row).addClass('text-right');
				  $('td:eq(12)', row).addClass('text-right ').find('input').attr('id', 'hebRetail'+data.upc);;//currency-format
				  $('td:eq(13)', row).addClass('text-center ');
				  $('td:eq(14)', row).addClass('text-left text-ellipsis');
				  $('td:eq(15)', row).addClass('text-left hidden');
				  $('td:eq(16)', row).addClass('text-left text-ellipsis').attr('title',data.status);
				  if (getTabActive() === "passed") {
					  $('td:eq(14)', row).addClass('text-left hidden');
				  } else {
					  $('td:eq(14)', row).addClass('text-left text-ellipsis abc');
					  $('td:eq(14)', row).attr("title", data.failedReason);
				  }
				  /*add attr*/
			 	  if(data.failedReasonCode === null) {
			 	  	$('td:eq(14)',row).html("");
			 	  }
			 	  
			 	  var itemObj = {
				  		upc:data.upc
				  }
				  if(isExistInArray(selected, itemObj)){
				  		var obj = getItemInArray(selected, itemObj);
				  		if(obj[0].checked) {
							$(row).find('input:checkbox').prop('checked',true);
							$(row).addClass('success');
				  		}
				  		if(obj[0].changeVal) {
							$(row).find('td:eq(12)').find('input').addClass('change-background');
							$(row).find('td:eq(12)').find('input').val(obj[0].hebRetail);
							calculatorWhenChangeHeb($(row));
				  		}
				  }
				  if(type === 'all') {
				 	  	 if(data.reviewStatus.indexOf('RJECT') > -1) {
					 	  	$(row).addClass('rejected');
					 	 } 
				  }
				  if(data.reviewStatus.indexOf('APPRV') > -1) {
					 	$(row).find("input").attr('disabled', true);
					 	 $(row).find("input:checkbox").attr('disabled', true);
					 	 $(row).addClass('approved');
				   }
			 }     
		} );

	    table.on( 'order.dt search.dt draw.dt init.dt', function () {
	  //   	table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	  //   		cell.innerHTML = i + 1 + table.page.info().start;
			// } );
		} );
	    /*INIT CLEAR FILTER*/
		$("#clear-filtter-btn-"+type).click(function() {
			if (!checkFilterEmpty(getTabActive())) {
				resetFilter();
			}
		});
		/*END CLEAR FILTER*/
		/*INIT CHECK BOX*/
		$('#' + type +'-table thead').on("change", 'input:checkbox', function() {
			var checked = $(this).prop("checked");
			$('#' + type +'-table tbody tr:not(.approved) input:checkbox').prop('checked', checked);

			$('#' + type +'-table tbody tr').each(function () {
				if(!$(this).hasClass('approved')){
					var statusProds = $(this).find('td:eq(15)').text();
					if(checked) {
						if (!$(this).find('td').hasClass('dataTables_empty')) {
							$(this).addClass("success");
							if(type === 'all' && statusProds.indexOf('RJECT') > -1) {
								$(this).removeClass('rejected');
							 	
							}
						}
					} else {
						$(this).removeClass("success");
						if(type === 'all' && statusProds.indexOf('RJECT') > -1) {
							$(this).addClass('rejected');
						 }
					}
					var changeVal = parseFloat($(this).find("td:eq(12)").find("input").val(), 2) !== parseFloat($(this).find("td:eq(12)").find("input").attr('tmp'), 2);
					var itemObj = {
						upc:  $(this).find('td:eq(2)').text(),
						hebRetail: $(this).find("td:eq(12)").find("input").val(),
						oldRetail: $(this).find("td:eq(12)").find("input").attr('tmp'),  
						status : statusProds, 
						changeVal : changeVal, 
						checked : checked
					}
			 		if(statusProds.indexOf('RJECT') > -1 ) {
			 			selectedReject = removeInArrays(selectedReject, itemObj);
			 			if(checked || changeVal){
				 			selectedReject = insertItemToArray(selectedReject, itemObj);
				 		}
			 		} else if(statusProds.indexOf('F') > -1 ) {
			 			selectedFailed = removeInArrays(selectedFailed, itemObj);
			 			if(checked || changeVal){
					 		selectedFailed = insertItemToArray(selectedFailed, itemObj);
					 	}
			 		} else {
			 			selectedPassed = removeInArrays(selectedPassed, itemObj);
			 			if(checked || changeVal){
				 				selectedPassed = insertItemToArray(selectedPassed, itemObj);
				 		}
			 		}
			 	}
						 		
			});
			if (getTabActive() === 'all') {
				selected = selectedPassed.concat(selectedFailed).concat(selectedReject);
			} else if (getTabActive() === 'passed') {
				selected = selectedPassed;
			} else if(getTabActive() === 'failed') {
				selected = selectedFailed;
			} else {
				selected = selectedReject;
			}
			controlButtonBDMReview(selected, type);
	    });
		/*END CHECK BOX*/
		/*INIt EVENT TABLE CLICK */
		$('#' + type +'-table tbody').on('change', 'input:checkbox', function () {
			var selectorTr = $(this).parent().parent();
			if (!$(selectorTr).find('td').hasClass('dataTables_empty')) {
	        	$(selectorTr).toggleClass('success');	
			}
			var changeVal = parseFloat($(selectorTr).find("td:eq(12)").find("input").val(),2) !== parseFloat($(selectorTr).find("td:eq(12)").find("input").attr('tmp'), 2);
			var checked =  $(this).prop('checked');
	     	var itemObj = {
				upc:  $(selectorTr).find('td:eq(2)').text(),
				hebRetail: $(selectorTr).find("td:eq(12)").find("input").val(), 
				oldRetail: $(selectorTr).find("td:eq(12)").find("input").attr('tmp'),  
				status: $(selectorTr).find('td:eq(15)').text(),
				changeVal : changeVal, 
				checked :  checked
			}
			var statusProds = $(selectorTr).find('td:eq(15)').text();
			if(type === 'all'){
				if(checked && statusProds.indexOf('RJECT') > -1){
					$(selectorTr).removeClass('rejected');
				} else if(statusProds.indexOf('RJECT') > -1) {
					$(selectorTr).addClass('rejected');
				} 
			}
			if(!$(this).prop('checked') && !changeVal) {
				if(statusProds.indexOf('RJECT') > -1 ) {
				 	selectedReject = removeInArrays(selectedReject, itemObj);
			 	} else if(statusProds.indexOf('F') > -1 ) {
			 		selectedFailed = removeInArrays(selectedFailed, itemObj);
			 	} else {
			 		selectedPassed = removeInArrays(selectedPassed, itemObj);
			 	}
			} else if(!_.isEmpty(itemObj.upc)) {
			 	if(statusProds.indexOf('RJECT') > -1 ) {
			 		selectedReject = removeInArrays(selectedReject, itemObj);
			 		selectedReject = insertItemToArray(selectedReject, itemObj);
			 	} else if(statusProds.indexOf('F') > -1 ) {
			 		selectedFailed = removeInArrays(selectedFailed, itemObj);
			 		selectedFailed = insertItemToArray(selectedFailed, itemObj);
			 	} else {
			 		selectedPassed = removeInArrays(selectedPassed, itemObj);
			 		selectedPassed = insertItemToArray(selectedPassed, itemObj);
			 	}
			}
			if (getTabActive() === 'all') {
				selected = selectedPassed.concat(selectedFailed).concat(selectedReject);
			} else if (getTabActive() === 'passed') {
				selected = selectedPassed;
			} else if(getTabActive() === 'failed') {
				selected = selectedFailed;
			} else {
				selected = selectedReject;
			}
			controlCheckAll(type);
			controlButtonBDMReview(selected, type);
	    } );
		/*END EVENT TABLE CLICK */
		/*INIT ACTION BUTTON BDM REVIEW*/
		$('#grp-btn button').click(function () { 
			var typeAction = $(this).val();
			if($(this).val() === 'reset') {
				$('#' + type +'-table tbody tr').each(function () {
					if($(this).find('td:eq(12)').find('input').hasClass('change-background')) {
						revertHebRetail($(this));
						$(this).find('td:eq(12)').find('input').removeClass('change-background');
					}
				});
				if (getTabActive() === 'all') {
					selectedPassed = resetGridFuntion(selectedPassed);
					selectedFailed = resetGridFuntion(selectedFailed);
					selectedReject = resetGridFuntion(selectedReject);
					selected = selectedPassed.concat(selectedFailed).concat(selectedReject);
				} else if (getTabActive() === 'passed') {
					selectedPassed = resetGridFuntion(selectedPassed);
					selected = selectedPassed;
				} else if(getTabActive() === 'failed') {
					selectedFailed = resetGridFuntion(selectedFailed);
					selected = selectedFailed;
				} else {
					selectedReject = resetGridFuntion(selectedReject);
					selected = selectedReject;
				}
				controlButtonBDMReview(selected, type);
			} else if($(this).val() === 'save' && selected.length >  0){
				callbackConfirm(selected, typeAction, "Do you want to save the changes?");
			} else if (selected.length >  0) {
				callbackConfirm(selected, typeAction, "Selected products will be " + typeAction + ". Please confirm.");
			}
		});
		/*END ACTION BUTTON BDM REVIEW*/
		$('#' + type +'-table tbody').on('change', '.format-input-cell', function () {
    		//debugger;
    		var selectorTr = $(this).parent().parent().parent();
    		var id = $(selectorTr).attr('id');//this.id
    		var hebValue = $(this).val().replace(/,/g, "");
    		if($.trim(hebValue) === "") {
    			hebValue = 0;
    			$(this).val(0);
    		}
    		var oldRetail = $(this).attr('tmp');
    		var isDecimalVal = isDecimalNumberBDM(hebValue, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
    		if(!isDecimalVal || !checkHebRetailInput(this)){
    			var hebInput = $(selectorTr).find("td:eq(12)").find("input");
    			$(hebInput).val($(hebInput).attr('tmp'));
    		}
    		var statusProds = $(selectorTr).find('td:eq(15)').text();
	   		var changeVal = isDecimalVal && parseFloat($(this).val(), 2) !== parseFloat($(this).attr('tmp'),2);
	   		var itemObj = {
						upc:  $(selectorTr).find('td:eq(2)').text(),
						hebRetail: hebValue,
						oldRetail : oldRetail, 
						status: statusProds,
						changeVal : changeVal, 
						checked : $(selectorTr).find('td:eq(0)').find('input:checkbox').prop('checked')
			}
	    	if (changeVal) {
	    			$(selectorTr).find('td:eq(12)').find('input').addClass('change-background');
	    			calculatorWhenChangeHeb(selectorTr);
			} else {
					$(selectorTr).find('td:eq(12)').find('input').removeClass('change-background');
					clearTooltipForValidationBDM($(selectorTr).find('td:eq(12)').find('input'));
					revertHebRetail(selectorTr);
			}
			if(statusProds.indexOf('RJECT') > -1 ) {
					 	selectedReject = removeInArrays(selectedReject, itemObj);
					 	selectedReject = insertItemToArray(selectedReject, itemObj);
			} else if(statusProds.indexOf('F') > -1 ) {
					 	selectedFailed = removeInArrays(selectedFailed, itemObj);
					 	selectedFailed = insertItemToArray(selectedFailed, itemObj);
			} else {
				 		selectedPassed = removeInArrays(selectedPassed, itemObj);
						selectedPassed = insertItemToArray(selectedPassed, itemObj);
			}
			 if (getTabActive() === 'all') {
					selected = selectedPassed.concat(selectedFailed).concat(selectedReject);
			} else if (getTabActive() === 'passed') {
					selected = selectedPassed;
			} else if(getTabActive() === 'failed') {
					selected = selectedFailed;
			} else {
					selected = selectedReject;
			}
			// formatCurrency();	
			controlCheckAll(type);
			controlButtonBDMReview(selected, type);
    	});
		/*smartfilter*/
		// $("#" + type + '-table  ' + '.second-tr > th > input').on( 'change', function () {//:not(.confirm-filter)
		// 	if(checkValidateFilter()){
		// 		if (timer.isActive) {
		// 			timer.stop();
		// 		}
		// 		timer.play();
		// 	}
		// } );

		// var timer = $.timer(function() {
		// 	if(checkValidateFilter()){
		// 		table.columns($("#" + type + '-table  ' + '.second-tr > th > input:text')).search($("#" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
		// 		timer.stop();
		// 	}
		// });
		// timer.set({ time : 1000, autostart : false });

		$("#" + type + '-table  ' + '.second-tr > th > select, ' +"#" + type + '-table  ' + '.second-tr > th > input').on( 'change', function () {
			if(checkValidateFilter()){
				table.columns($("#" + type + '-table  ' + '.second-tr > th > input:text')).search($("#" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
			}
		} );
		
		/*end smartfilter*/
		/*INIT MODAL*/
		$('.icon-calendar').click(function(e) {
		    $('#datefilterModal').modal({
		       	keyboard: false,
		        show:true
		    });
		    $("#datefilterModal").find("form").attr('id',$(this).attr("id")+'form');
		    forceBeginDateAndEndDate("#from-date-modal", "#to-date-modal");
		    $("#from-date-modal").val($(this).find(".from-date-val").val());
		    $("#to-date-modal").val($(this).find(".to-date-val").val());
		});
		$(".btn-cancel-modal").on('click', function() { 
			$('#datefilterModal').modal('hide');
		});
		$(".btn-ok-modal").on('click', function() {
			var fromDate = $("#from-date-modal").val();
			var toDate = $("#to-date-modal").val();
			var title = Constants.EMPTY_STRING;
			if(!_.isEmpty(fromDate)) {
				title =  "From : " + fromDate;
			}
			if(!_.isEmpty(toDate)) {
				if(!_.isEmpty(fromDate)) {
					title = title + " - "
				}
				title = title + "To : " + toDate;
			}
			var type = getTabActive();
			var idFormModal =  $("#datefilterModal").find("form").attr('id');
			if(idFormModal.indexOf("search-first-received-btn-"+type) > -1) {
				$("#search-first-received-btn-"+type).attr('title', title);
				$("#search-first-received-btn-"+type).find(".from-date-val").val(fromDate);
				$("#search-first-received-btn-"+type).find(".to-date-val").val(toDate);
			} else {
				$("#search-last-edited-btn-"+type).attr('title', title);
				$("#search-last-edited-btn-"+type).find(".from-date-val").val(fromDate);
				$("#search-last-edited-btn-"+type).find(".to-date-val").val(toDate);
			}
			$('#datefilterModal').modal('hide');
			if(checkValidateFilter()){
				table.columns($("#" + type + '-table  ' + '.second-tr > th > input:text')).search($("#" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
			}
		});

		/*END MODAL*/
		
		table.on( 'xhr', function () {
			if(resetCount) {
				var json = table.ajax.json();
			    var tabActive = getTabActive();
			    var totalAll  = json.totalpassed + json.totalfailed + json.totalrejected;
			    $(".badge-all-style").text("("  + totalAll + ")");
				$(".badge-passed-style").text("("  + json.totalpassed + ")");
				$(".badge-failed-style").text("("  + json.totalfailed + ")");
				$(".badge-rejected-style").text("("  + json.totalrejected + ")");
				 var numberRowTabActive = getNumberRowTabActive();
       			 if(_.isEmpty(numberRowTabActive) || parseInt(numberRowTabActive) == 0){
       			 	$("#product-review-export-btn").addClass('disable-class');
       			 } else {
       			 	$("#product-review-export-btn").removeClass('disable-class');
       			 }
				resetCount = false;
			}
		} );
		
		return table;
	}
	/*END INIT TABLE*/

	function insertItemToArray(lstItem, itemObj) {
		if(!_.isEmpty(itemObj.upc)) {
			lstItem.push(itemObj);
		}
	    return lstItem;
	}
	function isExistInArray(lstItem, itemObj) {
		return _.size(_.where(lstItem, itemObj)) === 0 ? false : true; 
	}
	function getItemInArray(lstItem, itemObj) {
		return _.where(lstItem, itemObj); 
	}
	function removeInArrays(lstItem, itemObj) {
		var val = _.filter(lstItem, function(item){ return item.upc !== itemObj.upc });
		return val;
	}
	function checkFilterEmpty (type) {
		var val = true;
		$(".second-tr-" + type).find('th:not(:first) input, th:not(:first) select').each(function (){
			if ($(this).val().length !== 0) {
				val = false;
				return;
			}
		});
		return val;
	}
	
	function formatCurrency () {
		$(".currency-format").each(function (index, value) {
			$(value).text($.number($(value).text(), 2));
		})
		$(".currency-format-input").each(function (index, value) {
			$(value).attr('title',$.number($(value).val(), 2));
			$(value).val(parseFloat($(value).val(), 2).toFixed(2));
		})
		$(".currency-format-tmp").each(function (index, value) {
			$(value).attr('tmp',$.number($(value).val(), 2));
		})
	}
	
	
	/**
	 * AUTO COMPLETE FOR COMMONDITIES AND BRAND.
	 */
	function initAutoCompleteSearch(idInput, urlAjax){
		var autoCompleteInputData = [];
		var autoCompleteInputDataValue = [];
		var autoCompleteInputDataLabel = [];
		$("#"+idInput).autocomplete({
			 minLength: 0,
			 search: function(event, ui) {
		            $(this).addClass("wait");
		            $(this).parent().find("span").removeClass("caret");
		    },
		    open: function(event, ui) {
		            $(this).removeClass("wait");
		            $(this).parent().find("span").addClass("caret");
		    },
	        source: function(request, response) {
	        	 $(document).unbind('ajaxSend');
	        	 $.ajax({
	             	type: "POST",
	                 url: appName + urlAjax,
	                 data: {
	                	 termSearch : $.trim(request.term)
	                 },
	                 dataType: "json",
	                 success: function (data) {
	                     dataAutoCompleteInput = data.data;
	                     for (var i in dataAutoCompleteInput) {
	                    	 autoCompleteInputDataLabel.push(dataAutoCompleteInput[i].value);
	                     }

	                     response(dataAutoCompleteInput);

	                     if(dataAutoCompleteInput.length == 0){
	                         $("#" + idInput).removeClass("wait");
	                         $("#" + idInput).parent().find("span").addClass("caret");
	                     }
	                     //re-attach loading screen
	                     $(document).bind("ajaxSend", function(e, xhr, settings){
	                         $.blockUI({
	                             message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
	                             fadeIn: 100,
	                             onUnblock: function() {
	                             }
	                         });

	                         
	                     }).bind("ajaxStop", function(){
	                         $.unblockUI();
	                     }).bind('ajaxError', handleAjaxError);

	                 },
	                 error: function (result) {
	                     // alert("Error......");
	                 }
	             });
				
			},
	        focus: function( event, ui ) {
	           $( "#"+idInput ).val( ui.item.label );
	           return false;
	        },
	        select: function( event, ui ) {
	  	        	 return false;
	         }
	    }).keypress(function(e){
	    	if(e.keyCode == 13) {	
	    	 	$(this).autocomplete("close");
	    	}
	    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	        .append( "<a>" + item.label + "</a>" )
	        .appendTo( ul );
	    };
		
	}
	/*END INIT AUTOCOMPLETE FUNCTION*/

	function checkValidateSearchFrom(){
		var isValid = true;
		var count = 0;
		compareDateBDM("#dateBegin", "#dateEnd", true);
		compareMarginBDM("#marginBegin","#marginEnd", true );
	    _.each($(".input-sm"), function(value, key) {
	    	if ($(value).closest('div').hasClass('has-error')) {
	    		count++;
			}
	    });	
	    if(count > 0) {
	    	isValid = false;
	    } 
	    return isValid;
	}
	function checkValidateFilter(){
		var isValid = true;
		var count = 0;
		_.each($(".input-decimal"), function(value, key) {
	    	var idItem = $(value).attr('id');
	    	validateDecimalBDM("#"+idItem, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
	    });	
	    _.each($(".input-number"), function(value, key) {
	    	var idItem = $(value).attr('id');
			validateIntegerBDM("#"+idItem,38);
	    });	
	    _.each($(".negative-number"), function(value, key) {
	    	var idItem = $(value).attr('id');
	    	validateNegativeDecimalBDM("#"+idItem, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
	    });	
	    _.each($(".input-sm"), function(value, key) {
	    	if ($(value).closest('th').hasClass('has-error')) {
	    		count++;
			}
	    });	
	    if(count > 0) {
	    	isValid = false;
	    } 
	    return isValid;
	}
	function isEmptyValueSearchForm(){
	   	var check = true;
		$("#bdm-header .input-sm").each(function () {
			if(! _.isEmpty($(this).val())) {
				check = false;
			}
		});
		if($("#show-all-map-check").prop('checked')) {
			check = false;
		}
		if($("#show-only-prePrice-check").prop('checked')) {
			check = false;
		}
		return check;
	}
 
	function actionBDMReview(selectedObject, type) {
		var tabActive = getTabActive();
		var urlAction = Constants.EMPTY_STRING;
		var selectedItems = [];
		if(type === 'approved'){
			$.each(selectedObject, function( key, value ) {
				if(value.checked) {
					var item = {
						upc : value.upc,
						hebRetail : value.hebRetail
					}
			  		selectedItems.push(item);
			  	}
			});
			urlAction = Constants.APPROVE_PRODS_PATH;
		} else if(type === 'rejected'){
			$.each(selectedObject, function( key, value ) {
				if(value.checked) {
			  		selectedItems.push(value.upc);
			  	}
			});
			urlAction = Constants.REJECT_PRODS_PATH;
		} else if(type === 'un-rejected'){
			$.each(selectedObject, function( key, value ) {
				if(value.checked) {
			  		selectedItems.push(value.upc);
			  	}
			});
			urlAction = Constants.UN_REJECT_PRODS_PATH;
		} else if(type === 'save') {
			$.each(selectedObject, function( key, value ) {
				if(value.changeVal) {
					var item = {
						upc : value.upc,
						hebRetail : value.hebRetail
					}
			  		selectedItems.push(item);
			  	}
			});
			urlAction = Constants.SAVE_PRODS_PATH;
		} 
		$.ajax({
	        type: "POST",
	        contentType: "application/json; charset=utf-8",
	        url: appName + urlAction,
	        cache: false,
            async: false,   
           	data: JSON.stringify(selectedItems),
	        dataType: "json",
	        contentType: "application/json",
	        success: function (data) {
	        	if (typeof(data.errors) !== "undefined" ){
	        		showAlertDanger("#alert-bdm",data.errors);
	        	} else {
	        		// showAlertSuccess("#alert-bdm","success");
	        		selectedAll = [];
				    selectedPassed = [];
				    selectedFailed = [];
				    selectedReject = [];
				    selectedRejectTabAll = [];
				    $(".second-tr-" + tabActive).find('th:not(:first)').each(function (){
						$(this).find("input").val("");
						$(this).find("select").val("");
					});
					$('#' + tabActive +'-table thead th.has-error .input-sm').each(function () {
						clearTooltipForValidationBDM("#"+$(this).attr('id'));
					});
				    resetCount = true;
	        		if(tabActive === 'all') {
		        		allTable = allTable.ajax.reload();
		        	} else if(tabActive === 'passed') {
		        		passTable = passTable.ajax.reload();
		        	} else if (tabActive === 'failed'){
		        		failedTable = failedTable.ajax.reload();
		        	} else {
		        		rejectTable = rejectTable.ajax.reload();
		        	}
	        	}
	    	},
	    	error: function (result) {
	            // alert("Error......");
	    	}
		});
	}

	function controlButtonBDMReview(selected, type) {
			var totalRejected = 0;
			var totalSave = 0;
			var totalChecked = 0;
			if(selected.length > 0 ) {
				$.each(selected, function( key, value ) {
					if(value.checked){
						totalChecked = parseInt(totalChecked) + 1;
						if(value.status.indexOf('RJECT') > -1 ) {
						  	totalRejected = parseInt(totalRejected) + 1;
						} 
					}
				  
				  if(value.changeVal) {
				  	totalSave = parseInt(totalSave) + 1;
				  }
				});
			}
			if(totalChecked === 0) {
				$("#reject-btn").prop("disabled", true);
	        	$("#un-reject-btn").prop("disabled", true);
	    		$("#approve-btn").prop("disabled", true);
			} else if(totalRejected > 0 && totalChecked == totalRejected) {
				$("#un-reject-btn").prop("disabled", false);
			} else  if(totalRejected === 0) {
				$("#reject-btn").prop("disabled", false);
		    	$("#approve-btn").prop("disabled", false);
			} else {
				$("#reject-btn").prop("disabled", true);
	        	$("#un-reject-btn").prop("disabled", true);
	    		$("#approve-btn").prop("disabled", true);
			}
			if(totalSave > 0 ) {
				setChangeStatus(true);
				$("#save-btn").prop("disabled", false);
				$("#reset-btn").prop("disabled", false);
				$("input.confirm-filter").prop("disabled", true);
				$("#"+type+"-table th.comfirm-filter").addClass("disabled");
			} else {
				setChangeStatus(false);
				$("#save-btn").prop("disabled", true);
				$("#reset-btn").prop("disabled", true);
				$("input.confirm-filter").prop("disabled", false);
				$("#"+type+"-table th.comfirm-filter").removeClass("disabled");
			}

	}
	
	function controlCheckAll(type){
		$('#' + type +'-table thead .check-all').prop('disabled', false);
		var isCheckAll = true;
		var allApproved = true;
		$('#' + type +'-table tbody tr:not(.approved)').each(function () {
			allApproved = false;
			if (!$(this).hasClass('success')) {
				isCheckAll = false;
			}
		});
		if(!allApproved && $('#' + type +'-table tbody tr td.dataTables_empty').length < 1) {
			$('#' + type +'-table thead .check-all').prop('checked', isCheckAll);
		} else {
			$('#' + type +'-table thead .check-all').prop('disabled', true);
			$('#' + type +'-table thead .check-all').attr('checked', false);
		}
	}

   	function showAlertDanger(idAlert, errorMessage) {
    	 $(idAlert).find('.alert').removeClass('alert-success');
    	 $(idAlert).find('.alert').addClass('alert-danger');
    	 $(idAlert).removeClass('hidden');
    	 $(idAlert +" #message-bdm").html(errorMessage);
    	  // removeAlert(idAlert,3000);
    }
     
     function showAlertSuccess(idAlert, errorMessage) {
    	 $(idAlert).find('.alert').addClass('alert-success');
    	 $(idAlert).find('.alert').removeClass('alert-danger');
    	 $(idAlert).removeClass('hidden');
    	 $(idAlert +" #message-bdm").html(errorMessage);
    	 removeAlert(idAlert,3000);
    	 
     }
     
     function removeAlert(idAlert,time) {
		$(idAlert).addClass('hidden');
    	$(idAlert +" #message-bdm").text(Constants.EMPTY_STRING);
    }
    function searchFuntion(){
		removeAlert("#alert-bdm-form",0);
		$('#' + tabActive +'-table thead th.has-error .input-sm').each(function () {
				clearTooltipForValidationBDM("#"+$(this).attr('id'));
		});
		resetCount = true;
		var type = getTabActive();
		$(".second-tr-" + type).find('th:not(:first)').each(function (){
					$(this).find("input").val("");
					$(this).find("select").val("");
		});
		$("#search-first-received-btn-" + tabActive).attr('title',"");
		$("#search-first-received-btn-" + tabActive).find('.from-date-val').val("");
		$("#search-first-received-btn-" + tabActive).find('.to-date-val').val("");
		$("#search-last-edited-btn-" + tabActive).attr('title',"");
		$("#search-last-edited-btn-" + tabActive).find('.from-date-val').val("");
	 	$("#search-last-edited-btn-" + tabActive).find('.to-date-val').val("");
		selectedAll = [];
		selectedPassed = [];
		selectedFailed = [];
		selectedReject = [];
		selectedRejectTabAll = [];
		searchBdmValue = setValueFormSearch();
		$('#'+ getTabActive() + '-table').DataTable().draw();
     }
     function setValueFormSearch () {
     	   	searchBdmValue = [];
     		searchBdmValue['0'] = $.trim($("#commodities-input").attr('commodities-inputid'));
		    // searchBdmValue['1'] = $("#vendor-input").attr('vendor-inputid');
		    searchBdmValue['1'] = $.trim($("#vendor-input").val());
		   	searchBdmValue['2'] = $.trim($("#brand-input").val());
		  	searchBdmValue['3'] = $.trim($("#product-input").val());
		    searchBdmValue['4'] = $.trim($("#dateBegin").val());
		    searchBdmValue['5'] = $.trim($("#dateEnd").val());
		    searchBdmValue['6'] = $.trim($("#marginBegin").val());
		    searchBdmValue['7'] = $.trim($("#marginEnd").val());
		    searchBdmValue['8'] = $.trim($("#failed-input").val());
		    searchBdmValue['9'] = $("#show-all-map-check").prop("checked");
	        searchBdmValue['10'] = $("#show-only-prePrice-check").prop("checked");
	        return searchBdmValue;

     }
     function resetFuntion(){
    	removeAlert("#alert-bdm-form",0);
		$('#bdm-header .has-error .input-sm').each(function () {
			clearTooltipForValidationBDM("#"+$(this).attr('id'));
		});
		// resetFilter();
	    $("#bdm-header input.input-decimal").each(function () {
			$(this).val(Constants.EMPTY_STRING);
		});
		$("#bdm-header input.negative-number").each(function () {
			$(this).val(Constants.EMPTY_STRING);
		});
	    $("#commodities-input").attr("commodities-inputid", "");
	    $("#commodities-input:text").val("");
		$("#brand-input").attr("brand-inputid", "");
	    $("#brand-input:text").val("");	
	    $("#product-input:text").val("");
		$("#show-all-map-check").prop('checked', false);
		$("#show-only-prePrice-check").prop('checked', false);
		$("#failed-input").val(Constants.EMPTY_STRING);
		$("#vendor-input").val(Constants.EMPTY_STRING);	

		$("#dateEnd").datepicker('setEndDate', new Date());
 		var dateBeginEnd = new Date();
		dateBeginEnd.setDate(dateBeginEnd.getDate() - 1);
		$("#dateBegin").datepicker("setEndDate", dateBeginEnd);

		var dateBeginValue = new Date();
		dateBeginValue.setDate(dateBeginValue.getDate() - 7);
		$("#dateBegin").datepicker("setDates", dateBeginValue);
		$("#dateEnd").datepicker("setDates", new Date());
		 $("#commodities-input").focus();
     }

	function openDialogConfirm(messageConfirm, callbackFn){	
		bootbox.dialog({
			title : "Confirmation",
			message: messageConfirm,
			buttons: {
				main: {
			      label: "Yes",
			      className: "btn-primary",
			      callback: function() {		        
			        callbackFn(true);
			      }
			    },			
			    cancel: {
			      label: "No",
			      className: "btn-default",
			      callback: function() {
			        callbackFn(false);
			      }
			    }		    	    
		  	}		
		});
	}
	function callbackConfirm(selected, typeAction, messageConfirm){
		openDialogConfirm(messageConfirm, function(result){
			if(result){
				actionBDMReview(selected, typeAction); 
			}
		}); 
	}

	function callbackConfirmFilter(messageConfirm, table, type, selectorInput){
		openDialogConfirm(messageConfirm, function(result){
			if(result){
				if(checkValidateFilter()){
					$(selectorInput).attr('tmp', $(selectorInput).val());
					table.columns($("#" + type + '-table  ' + '.second-tr > th > input:text')).search($("#" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
				}
			} else {
				$(selectorInput).val( $(selectorInput).attr('tmp'));
			}
		}); 
	}
	function calculatorWhenChangeHeb(selectorTr){
		var hebValue = $(selectorTr).find('td:eq(12)').find('input').val().replace(/,/g, "");
	    var costVal = $(selectorTr).find('td:eq(9)').find('span.currency-format').text().replace(/,/g, "");
	    var margin = parseFloat(hebValue) - parseFloat(costVal) ;
	    var gppercent = (margin/costVal)*100;
	    $(selectorTr).find('td:eq(12)').find('input').attr('title', $.number(hebValue, 2));
	    $(selectorTr).find('td:eq(10)').find('span.currency-format').text($.number(gppercent, 2));
	    $(selectorTr).find('td:eq(11)').find('span.currency-format').text($.number(margin, 2));

	}

	function revertHebRetail(selectorTr){
		var oldHeb = $(selectorTr).find('td:eq(12)').find('input').attr('tmp');
	    $(selectorTr).find('td:eq(12)').find('input').attr('title', $.number(oldHeb, 2) );
	    $(selectorTr).find('td:eq(12)').find('input').val(oldHeb);
		$(selectorTr).find('td:eq(10)').find('span.currency-format').text($(selectorTr).find('td:eq(10)').find('input').attr('tmp'));
		$(selectorTr).find('td:eq(11)').find('span.currency-format').text($(selectorTr).find('td:eq(11)').find('input').attr('tmp'));
	}
	function checkHebRetailInput(element) {
		var value = $(element).val();
		if(!isNaN(parseFloat(value)) && (parseFloat(value) > 100000)) {
			tooltipForValidationBDM(element , Constants.msgHebRetailLimit);
			return false;
		}
		return true;
	}
	function resetGridFuntion(selectedObject){
		$.each(selectedObject, function( key, value ) {
			var item = {
				upc : value.upc,
				hebRetail : value.oldRetail, 
				changeVal: false,
				checked: value.checked,
				oldRetail:value.oldRetail, 
				status:value.status
			}
			selectedObject = removeInArrays(selectedObject, item);
			if(value.checked) {
			  	selectedObject = insertItemToArray(selectedObject,item);
			}
		});
		return selectedObject;
    }

   	function controlVisibleBtn(type) {
   		if(type === 'all') {
   			$("#reject-btn").removeClass('display-none');
	        $("#un-reject-btn").removeClass('display-none');
	    	$("#approve-btn").removeClass('display-none');
   		} else if( type === 'passed' || type === 'failed') {
   			$("#reject-btn").removeClass('display-none');
	        $("#un-reject-btn").addClass('display-none');
	    	$("#approve-btn").removeClass('display-none');
   		} else if( type === 'rejected') {
   			$("#reject-btn").addClass('display-none');
	        $("#un-reject-btn").removeClass('display-none');
	    	$("#approve-btn").addClass('display-none');
   		}
   	}

    /************************************************************************************************************
	 * end function
	 ************************************************************************************************************/
});