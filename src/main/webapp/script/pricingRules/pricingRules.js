/************************************************************************************************************
 * PUBLIC VALUE
 ************************************************************************************************************/
var dataAutoCompleteInputValue = [];
var dataAutoCompleteInputLabel = [];
var globalModel 			   = [];
var comboboxList 			   = [];
var msgInvalid				   = "Invalid value";
var msgRequired 			   = "This field is required";
var msgRangePercent 		   = "Only the value between -10,000 and 10,000 is accepted";
var msgRangePositivePercent	   = "Only the value between 0 and 10,000 is accepted";
var msgRangeDollar 			   = "Only the value between -100,000 and 100,000 is accepted";
var msgRangePositiveDollar 	   = "Only the value between 0 and 100,000 is accepted";
var msgGP					   = "Entered Gross Profit value is outside of defined Margin boundaries";
var msgMin					   = "Minimum margin should be lower than Maximum margin";
var msgMax					   = "Maximum margin should be greater than Minimum margin";
var msgGPRequired			   = "Target Gross Profit is required";
var msgCompetitor			   = "Please select at least one competitor.";
var isValid 				   = false;
var isSettingValidate		   = false;
var rounding				   = 0;
var vendorSelected			   = "";
var idAutocomplete			   = "";
var textAutocomplete		   = "";

var Constants = {
		EMPTY_STRING: "",
		SPACE_STRING: " ",
		MAX_DIGIT	: 3,
		AFTER_DIGIT	: 2,
		TOTAL_NUMBER: 11, //for validate decimal number
		TIME_OUT_TT	: 1500,
		INV_MAX_LEN	: 50
};

function setValid(valid) { isValid = valid; }
function getValid() { return isValid; }

$(document).ready(function(){
	$("#commodityInput").focus();
	getDataForAutocomplete("commodityInput", "/ajax/get-autocomplete-commodities");
	getVendor("/vendor-management/search-for-vendors");
	clickDropAutoCompleteVendorManagement("commodityInput");
	initCompetitorPricing();
	initForm();
	resetForm();
	$(".rule-content").css("display", "none");
	$("#commodityInput").val("");
	$("#subCommodityInput").val("");
	$("#vendorInput").val("");
	$("#showUserAndTimeUpdate").css('display','none');

	$("#12-1-input").on("keypress", function(event) {
		allowDecimalNumber(this, event, 12, 2);
	});
	$("#12-2-input, #11-input, #13-input, #15-1-input, #16-1-input").on("keypress", function(event) {
		allowDecimalNumber(this, event, 11, 2);
	});
	$("#15-2-input, #16-2-input").on("keypress", function(event) {
		allowDecimalNumber(this, event, 12, 2);
	});
});

function precise_roundFloatNum(num) {
	if (num == "") {
		return;
	}
	var decimals = 2;
	var t 		 = Math.pow(10, decimals);
	return (Math.floor(num * 100)/100 + (decimals>0?1:0)* (10 / Math.floor(100, decimals)) / t).toFixed(decimals);
}

function allowDecimalNumber(element, keyPressEvent, totalDigitsLimit, afterDigitsLimit){
	totalDigitsLimit = typeof totalDigitsLimit !== 'undefined' ? totalDigitsLimit : Constants.TOTAL_NUMBER;
	afterDigitsLimit = typeof afterDigitsLimit !== 'undefined' ? afterDigitsLimit : Constants.AFTER_DIGIT;
	// Control key down, just allow decimal number(totalDigitsLimit, afterDigitsLimit).
	var valueInput 	 = $(element).val();
	var chr 		 = String.fromCharCode(keyPressEvent.which);
	var charCode 	 = (keyPressEvent.which) ? keyPressEvent.which : keyPressEvent.keyCode;
	var isDotKey 	 = charCode === 46;
	var isSubtract 	 = charCode === 45;
	//var isMainKey 	 = $.inArray(charCode, [8, 9, 13, 35, 36, 37, 38, 39, 40, 99, 118]) !== -1; // enter, backspace
	var isMainKey 	 = $.inArray(charCode, [8, 9, 13, 35, 36, 37, 38, 39, 40]) !== -1; // enter, backspace
	var isNumberKey  = (charCode > 47 && charCode < 58);
	var isCtrl  	 = false;
   	if(keyPressEvent.ctrlKey) { isCtrl = true; }
	if(isCtrl && ( $.inArray(charCode, [80, 97, 99, 118]) !== -1)){ return true; }
	var isSpecial	 = ("~!@#$%^&*()_=+/?<>,[]{}\|".indexOf(chr) >= 0) ? true : false;
	if((!isDotKey && !isSubtract  && !isNumberKey && !isMainKey) || isSpecial) {
		keyPressEvent.preventDefault();
		return false;
	}
	var selectionStart = getInputSelection(element).start;
	var selectionEnd   = getInputSelection(element).end;
	var isSelected     = (element.selectionStart == 0 && element.selectionEnd == element.value.length) ? true : false;
	if(isSelected) {
		return true;
	}
	if(isNumberKey || isDotKey || isSubtract) {
		var beforePoint = 0;
		var afterPoint 	= 0;
		if(valueInput.split('.')[0] != null) {
			beforePoint = valueInput.split('.')[0].length;
		}
		if(valueInput.split('.')[1] != null) {
			afterPoint = valueInput.split('.')[1].length;
		}
		if(valueInput.lastIndexOf('-') >= 0) {
			beforePoint = beforePoint - 1;
		}
		var totalDigits = beforePoint + afterPoint;
		if(isNumberKey && (valueInput.lastIndexOf('.') < 0) && (beforePoint >= (totalDigitsLimit - afterDigitsLimit)) && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isNumberKey && (valueInput.lastIndexOf('.') >= 0) && (beforePoint >= (totalDigitsLimit - afterDigitsLimit)) && (selectionStart === selectionEnd) && (selectionStart < valueInput.lastIndexOf('.'))) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(!isSubtract && totalDigits >= totalDigitsLimit && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isNumberKey && (valueInput.lastIndexOf('.') < 0) && (valueInput.length >= totalDigitsLimit) && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if((isNumberKey || isDotKey ) && selectionStart <= valueInput.lastIndexOf('-')) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isDotKey && (valueInput.lastIndexOf('.') >= 0) && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isDotKey && (valueInput.lastIndexOf('.') < 0) && (valueInput.length - selectionEnd > afterDigitsLimit)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(!isSubtract && (valueInput.lastIndexOf('.') >= 0) && (selectionStart === selectionEnd) && (selectionEnd - valueInput.lastIndexOf('.') > afterDigitsLimit)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(!isSubtract && (valueInput.lastIndexOf('.') >= 0) && (valueInput.lastIndexOf('.') < selectionEnd) && (afterPoint >= afterDigitsLimit) && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isSubtract && (valueInput.lastIndexOf('-') >= 0 || selectionStart > 0)) {
			keyPressEvent.preventDefault();
			return false;
		}
	}
}

/************************************************************************************************************
 * END PUBLIC VALUE
 ************************************************************************************************************/

/************************************************************************************************************
 * AUTOCOMPLETE
 ************************************************************************************************************/
function getDataForAutocomplete(idInput, url) {
    $.ajax({
		type	   : "POST",
        contentType: "application/json; charset=utf-8",
        url		   : appName + url,
        dataType   : "json",
        success	   : function (data) {
            dataAutoCompleteInput = data.data;
            if (url.indexOf("subCommodities") >= 0) {
            	$("#subCommodityInput").val(dataAutoCompleteInput[0].label);
            	$("#subCommodityInput").attr("subcommodityinputid", dataAutoCompleteInput[0].label);
            	$("#subCommodityInput").attr("subcommodityinputtext", dataAutoCompleteInput[0].label);
            	pushItem("subCommodityInput");
            }
            for (var i in dataAutoCompleteInput) {
				dataAutoCompleteInputValue.push(dataAutoCompleteInput[i].label);
                dataAutoCompleteInputLabel.push(dataAutoCompleteInput[i].value);
            }
            autoCompleteInitVendorManagement(idInput, dataAutoCompleteInput);
        }
    });
}

function getVendor(url) {
	$.ajax({
		type	   : "POST",
        contentType: "application/json; charset=utf-8",
        url		   : appName + url,
        dataType   : "json",
        success	   : function (data) {
        	dataAutoCompleteInput = data.data;
            $("#vendorInput").append(new Option("", ""));
            for (var i in dataAutoCompleteInput) {
            	$("#vendorInput").append(new Option(dataAutoCompleteInput[i].label, dataAutoCompleteInput[i].value));
            	dataAutoCompleteInputValue.push(dataAutoCompleteInput[i].label);
                dataAutoCompleteInputLabel.push(dataAutoCompleteInput[i].value);
            }
        }
    });
    $("#vendorInput").on("change", function() {
    	var selectedText = $("#vendorInput option:selected").text();
    	if (_.isEmpty(selectedText) && selectedText != $(this).attr("vendorinputtext")) {
    		$("#vendorInput").val(vendorSelected);
    	} else {
    		$("#vendorInput").attr("vendorinputtext", selectedText);
    		if (isChangedOriginalData()) {
				bootbox.dialog({
					title  : "Confirmation",
					message: "You will lose your changes, do you wish to continue?",
					buttons: {
						main  : {
							label	 : "Yes",
							className: "btn-primary",
							callback : function() {
								$("#commodityInput").val("");
					        	$("#subCommodityInput").val("");
					        	if($("#subCommodityInput").autocomplete("instance") !== undefined) {
					        		//$("#subCommodityInput").autocomplete("destroy");
					        	}
					        	$("#commodityInput").attr("commodityinputid", "");
					        	$("#subCommodityInput").attr("subcommodityinputid", "");
					        	$("#commodityInput").attr("title", "");
	        					$("#subCommodityInput").attr("title", "");
								getPricingRules();
								$("#vendorInput").attr("title", $("#vendorInput option:selected").text());
							}
						},
						cancel: {
							label	 : "No",
							className: "btn-default",
							callback : function() {
								revertComboboxList();
							}
						}
					},
					onEscape: function() {
						revertComboboxList();
					}
				});
			} else {
				$("#commodityInput").val("");
	        	$("#subCommodityInput").val("");
	        	if($("#subCommodityInput").autocomplete("instance") !== undefined) {
	        		//$("#subCommodityInput").autocomplete("destroy"); 
	        	}
	        	$("#commodityInput").attr("commodityinputid", "");
	        	$("#subCommodityInput").attr("subcommodityinputid", "");
	        	$("#commodityInput").attr("title", "");
	        	$("#subCommodityInput").attr("title", "");
	        	getPricingRules();
	        	$("#vendorInput").attr("title", $("#vendorInput option:selected").text());
			}
    	}
    });
}

function getComboboxWidth(data) {
	var max = "";
	for (var key in data) {
		var obj = data[key];
		if (obj.label.length > max.length) {
			max = obj.label;
		}
	}
	return max.length * 10;
}

function autoCompleteInitVendorManagement(idInput, data) {
	var width 		 = $("#" + idInput).closest("div").width();
	var contentWidth = getComboboxWidth(data);
	if (parseInt(contentWidth) > parseInt(width)) {
		width = contentWidth;
	}
   	$( "#" + idInput ).autocomplete({
		autoFocus: true,
	    search 	 : function(event, ui) {},
	    open 	 : function(event, ui) {},
	    response : function(event, ui) {
	    	width = $("#" + idInput).closest("div").width();
	    	if (getComboboxWidth(ui.content) > width) {
				width = getComboboxWidth(ui.content);
			}
	    },
	    source 	 : function(request, response) {
	    	var result  = [];
	        var keyword = $.trim($("#" + idInput).val());
	        result 		= searchForAutocomplete(idInput, keyword, data);
	        response(result);
	    },
	    minLength: 0,
	    select 	 : function(event, ui) {
	        $("#" + idInput).val(ui.item.label);
	        if ($.inArray($("#" + idInput).val(), dataAutoCompleteInputValue) !== -1
	            || $.inArray($("#" + idInput).val(), dataAutoCompleteInputLabel) !== -1) {
	       	 	$( "#" + idInput ).attr( idInput + "text", ui.item.label );
	       	 	$( "#" + idInput ).attr( idInput + "id", ui.item.value );
	       	 	idAutocomplete	 = ui.item.value;
	       	 	textAutocomplete = ui.item.label;
	       	 	//$( "#" + idInput).attr("title", ui.item.label);
	        } else {
	        	//$( "#" + idInput ).attr( idInput + "id", "");
	        	//$( "#" + idInput).attr( "title", "" );
	        }
	        selectedLabelBefore = ui.item.label;
	        selectedIdBefore 	= ui.item.value;
	        if (isChangedOriginalData()) {
				bootbox.dialog({
					title  : "Confirmation",
					message: "You will lose your changes, do you wish to continue?",
					buttons: {
						main  : {
							label	 : "Yes",
							className: "btn-primary",
							callback : function() {
								if (idInput === "commodityInput") {
						        	if($("#subCommodityInput").autocomplete("instance") !== undefined) {
						        		$("#subCommodityInput").autocomplete("destroy");
						        		$("#subCommodityInput").val("");
						        	}
						        	getDataForAutocomplete("subCommodityInput", "/ajax/get-autocomplete-subCommodities?commodityId=" + ui.item.value);
						        	$("#vendorInput").val("");
						        	$("#subCommodityInput").attr("subcommodityinputid", "");
						        	//$("#vendorInput").attr("vendorinputid", "");
						        	$("#vendorInput").val("");
								}
						        if (idInput === "subCommodityInput") { $("#vendorInput").val(""); }
								getPricingRules();
								$("#vendorInput").attr("title", "");
								$("#commodityInput").attr("title", $("#commodityInput").val());
								$("#subCommodityInput").attr("title", $("#subCommodityInput").val());
							}
						},
						cancel: {
							label	 : "No",
							className: "btn-default",
							callback : function() {
								revertComboboxList();
							}
						}
					},
					onEscape: function() {
						revertComboboxList();
					}
				});
			} else {
				if (idInput === "commodityInput") {
		        	if($("#subCommodityInput").autocomplete("instance") !== undefined) {
		        		$("#subCommodityInput").autocomplete("destroy");
		        		$("#subCommodityInput").val("");
		        	}
		        	getDataForAutocomplete("subCommodityInput", "/ajax/get-autocomplete-subCommodities?commodityId=" + ui.item.value);
		        	$("#vendorInput").val("");
		        	$("#subCommodityInput").attr("subcommodityinputid", "");
		        	$("#vendorInput").attr("vendorinputid", "");
				}
		        if (idInput === "subCommodityInput") { $("#vendorInput").val(""); }
		        getPricingRules();
		        $("#vendorInput").attr("title", "");
		        $("#commodityInput").attr("title", $("#commodityInput").val());
				$("#subCommodityInput").attr("title", $("#subCommodityInput").val());
			}
	        return false;
	    },
	    focus: function(event, ui) {
   	//$("#" + idInput).val(ui.item.label);
   			//$("#" + idInput).val(strTrim($("#" + idInput).val()));
	       	if ($.inArray($.trim($("#" + idInput).val()), dataAutoCompleteInputValue) !== -1 
	               || $.inArray($.trim($("#" + idInput).val()), dataAutoCompleteInputLabel) !== -1) {
	            //$('.ui-menu-item').attr( "title", ui.item.label);
	            
	//            if (!_.isEmpty($( "#" + idInput ).attr(idInput +"id"))) {
	//           	 $( "#" + idInput ).attr(idInput +"id-old", $( "#" + idInput ).attr(idInput +"id"));
	//            }
	//            
	//            if (!_.isEmpty($( "#" + idInput ).attr(idInput +"text"))) {
	//           	 $( "#" + idInput ).attr(idInput +"text-old", $( "#" + idInput ).attr(idInput +"text"));
	//            }
	            
	            $( "#" + idInput ).attr( idInput + "id", ui.item.value );
	       	} else {
	         	$( "#" + idInput ).attr( idInput + "id", "");
	       	}
	       	selectedLabelBefore = ui.item.label;
	       	selectedIdBefore    = ui.item.value;
	       	$(".ui-autocomplete").css("width", width + "px");
	       	return false;
   		}
 	}).focusout(function() {
		if ($.inArray($.trim($("#" + idInput).val()), dataAutoCompleteInputValue) === -1) {
			$( "#" + idInput ).attr( idInput +"id", "");
		}
		$(this).autocomplete("option", "source", data);
 	});
}

function searchForAutocomplete(id, keyword, data) {
	var result    = [];
	var key 	  = $.trim(keyword);
	if (key.length > 0) {
		if (id == "subCommodityInput" && key.toLowerCase() == "all") {
			return data;
		} else {
			var find   = "-";
			var regExp = RegExp(find, 'g');
			key 	   = key.replace(regExp, " ");
			var arrKey = key.split(" ");
			for (var i=0; i<arrKey.length; i++) {
				if (arrKey[i] == "") {
					arrKey.splice(i, 1);
					i = -1;
				}
			}
			arrKey = arrKey.join().split(",");
			var n  = arrKey.length;
			for (var j=0; j<data.length; j++) {
				var temp 	 = data[j].label;
				temp 	 	 = temp.replace(regExp, " ");
				var arrLabel = temp.split(" ");

				for (var k=0; k<arrLabel.length; k++) {
					if (arrLabel[k] == "") {
						arrLabel.splice(k, 1);
						k = -1;
					}
				}
				var m = arrLabel.length;
				if (n <= m) {
					for (var t=0; t< (m - n + 1); t++) {
						for (var l=0; l<n; l++) {
							if (l == n - 1) {
								if (arrLabel[l + t].toLowerCase().indexOf(arrKey[l].toLowerCase()) == 0) {
									result.push(data[j]);
								}
							} else {
								if (arrKey[l].toLowerCase() != arrLabel[l + t].toLowerCase()) {
									break;
								}
							}
						}
					}
				}
			}
		}
	}
    return result;
}

/**
 * click to dropdown auto list in autoComplete.
 */
function clickDropAutoCompleteVendorManagement() {
	$('.wrap-caret').click(function() {
		$(".ui-autocomplete").css("display", "none");
		if($("#" + $(this).parent("div").find("input").attr("id")).autocomplete("instance") !== undefined) {
			$("#" + $(this).parent("div").find("input").attr("id")).autocomplete("search", "");
		}
		$("#" + $(this).parent("div").find("input").attr("id")).focus();
	});
}
/************************************************************************************************************
 * END AUTOCOMPLETE
 ************************************************************************************************************/

/************************************************************************************************************
 * GET DATA
 ************************************************************************************************************/

function getPricingRules() {
	$(".alert").hide();
	vendorSelected = $("#vendorInput").val();
	$.ajax({
		cache: false,
	    type: 'GET',
	    url : appName + "/getPricingRules",
	    data:{
	    	commodityId	  : $("#commodityInput").attr("commodityinputid"),
	    	subCommodityId: $("#subCommodityInput").attr("subcommodityinputid"),
	    	vendorId	  : $("#vendorInput").val()
	    }
	}).success(function(data) {
		$(".rule-content").css("display", "block");
		fillRricingRule(data);
		setValidatePricingRules();
	}).done(function() {
	}).fail(function() {
		globalModel = [];
	}).always(function() {
	});
	if ($("#commodityInput").val() != "") { pushItem("commodityInput"); }
	else {
		$("#commodityInput").attr("commodityinputid", "");
		$("#commodityInput").attr("commodityinputtext", "");
		removeItem("commodityInput");
	}
	if ($("#subCommodityInput").val() != "") { pushItem("subCommodityInput"); } 
	else {
		$("#subCommodityInput").attr("subcommodityinputid", "");
		$("#subCommodityInput").attr("subcommodityinputtext", "");
		removeItem("subCommodityInput");
	}
	if ($("#vendorInput").val() != "") { pushItem("vendorInput"); } 
	else {
		$("#vendorInput").attr("vendorinputid", "");
		$("#vendorInput").attr("vendorinputtext", "");
		removeItem("vendorInput");
	}
	if ($("#commodityInput").val() != "") {
		$("#vendorInput").attr("vendorinputtext", "");
		$("#vendorInput").attr("vendorinputid", "");
		$("#subCommodityInput").prop("disabled", false);
		$("#subCommodityInput").attr("disabled", "");
	}
	if ($("#vendorInput").val() != "") {
		$("#commodityInput").attr("commodityinputid", "");
		$("#commodityInput").attr("commodityinputtext", "");
		$("#subCommodityInput").attr("subcommodityinputid", "");
		$("#subCommodityInput").attr("subcommodityinputtext", "");
		$("#subCommodityInput").prop("disabled", true);
	}
	$("#round_rule").val("round_up");
	$("#pricing-save-btn").prop("disabled", true);
	$("#11-input, #12-1-input, #12-2-input, #14-1-input, #14-2-input, #15-1-input, #15-2-input, #16-1-input, #16-2-input, #17-1-input, #17-2-input, #13-input, #13-select")
	.each(function() { clearTooltipForValidationDiv($(this).attr("id")); });
}

function removeItem(type) {
	if (comboboxList.length != 0) {
		for (var key in comboboxList) {
			var item = comboboxList[key];
			if (item.type == type) { comboboxList.splice(key, 1); }
		}
	}
}

function pushItem(type) {
	var length = comboboxList.length;
	var count  = 0;
	for (var key in comboboxList) {
		var item = comboboxList[key];
		if (item.type == type) {
			if (type == "commodityInput") {
				item.id   = $("#commodityInput").attr("commodityinputid");
				item.text = $("#commodityInput").attr("commodityinputtext");
			}
			if (type == "subCommodityInput") {
				item.id   = $("#subCommodityInput").attr("subcommodityinputid");
				item.text = $("#subCommodityInput").attr("subcommodityinputtext");
			}
			if (type == "vendorInput") {
				item.id   = $("#vendorInput").val();
				item.text = $("#vendorInput").attr("vendorinputtext");
			}
			return;
		} else { count++; }
	}
	if (count == length) {
		if (type == "commodityInput") {
			comboboxList.push({
				type: "commodityInput",
				id 	: $("#commodityInput").attr("commodityinputid"),
				text: $("#commodityInput").attr("commodityinputtext")
			});
		}
		if (type == "subCommodityInput") {
			comboboxList.push({
				type: "subCommodityInput",
				id 	: $("#subCommodityInput").attr("subcommodityinputid"),
				text: $("#subCommodityInput").attr("subcommodityinputtext")
			});
		}
		if (type == "vendorInput") {
			comboboxList.push({
				type: "vendorInput",
				id 	: $("#vendorInput").val(),
				text: $("#vendorInput").attr("vendorinputtext")
			});
		}
	}
}

function fillRricingRule(data) {
	if ($("#commodityInput").attr("commodityinputtext") != "") {
		$("#subCommodityInput").prop("disabled", false);
	} else {
		$("#subCommodityInput").prop("disabled", true);
	}
	if (data.lstAssortmentRulesVODisplays != null) {
		if (data.lstAssortmentRulesVODisplays["12"].activeSws[0] == "N") {
			data.lstAssortmentRulesVODisplays["12"].values[0] = "";
		}
		if (data.lstAssortmentRulesVODisplays["12"].activeSws[1] == "N") {
			data.lstAssortmentRulesVODisplays["12"].values[1] = "";
		}
		globalModel = data;
		enableMinMax(false);
	} else {
		globalModel = [];
		resetForm();
	}
	$("#competitor-tree").jstree().uncheck_all();
	if (_.size(data.lstCompetitors) > 0) {
		$("#competitor-tree").jstree().check_node(data.lstCompetitors);
		//$("#competitor-tree").jstree().uncheck_all();
	}
	if (_.size(data.lstAssortmentRulesVODisplays) > 0) {
		$("#11-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["11"].values[0])); 
		if (data.lstAssortmentRulesVODisplays["12"].activeSws[0] == "Y") {
			$("#12-1-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["12"].values[0])); 
			$("#17-1-input").attr("maxLength", 10);
			$("#17-2-input").attr("maxLength", 10);
			$("#17-1-input").on("keypress", function(event) {
				allowDecimalNumber(this, event, 12, 2);
			});
			$("#17-2-input").on("keypress", function(event) {
				allowDecimalNumber(this, event, 12, 2);
			});
		} else {
			$("#12-1-input").val("");
		}
		if (data.lstAssortmentRulesVODisplays["12"].activeSws[1] == "Y") {
			$("#12-2-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["12"].values[1]));
			$("#17-1-input").attr("maxLength", 9);
			$("#17-2-input").attr("maxLength", 9);
			$("#17-1-input").on("keypress", function(event) {
				allowDecimalNumber(this, event, 11, 2);
			});
			$("#17-2-input").on("keypress", function(event) {
				allowDecimalNumber(this, event, 11, 2);
			});
		} else {
			$("#12-2-input").val("");
		}
		$("#13-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["13"].values[0]));
		$("#13-select").val($.trim(data.lstAssortmentRulesVODisplays["13"].actTypCd));
		var rule14 = data.lstAssortmentRulesVODisplays["14"].values[0];
		if (parseInt(rule14) == 0) {
			rule14 = "00";
		} else {
			rule14 = rule14.substring(rule14.indexOf(".") + 1, rule14.length);
			rule14 = (rule14.length == 2) ? rule14 : rule14 + "0";
		}
		if (data.lstAssortmentRulesVODisplays["14"].values[1] == 0) {
			$("#14-1-input").prop("disabled", false);
			$("#14-2-input").prop("disabled", true);
			$("#14-2-input").val("");
			$("input:radio[name='rounding_rule']").filter("[value='0']").prop("checked", true);
			$("#14-1-input").val(rule14);
		} else if (data.lstAssortmentRulesVODisplays["14"].values[1] == 1) {
			$("#14-1-input").prop("disabled", true);
			$("#14-2-input").prop("disabled", false);
			$("#14-1-input").val("");
			$("input:radio[name='rounding_rule']").filter("[value='1']").prop("checked", true);
			$("#14-2-input").val(rule14);
		}
		$("#15-1-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["15"].values[0]));
		$("#15-2-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["15"].values[1]));
		$("#16-1-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["16"].values[0]));
		$("#16-2-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["16"].values[1]));
		$("#17-1-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["17"].values[0]));
		$("#17-2-input").val(precise_roundFloatNum(data.lstAssortmentRulesVODisplays["17"].values[1]));
		$("#pricing-strategy").val($.trim(data.lvlType));
		$("#pricing-save-btn").prop("disabled", true);
		// Append HTML
		$("#showUserAndTimeUpdate").css('display','');
		$("#uidAndTsUpdate").html("<span class='userAndTimeUpdateStyle'>Last updated by "+data.lstAssortmentRulesVODisplays["11"].lstUpdateUid+"</span>" + " <span class='userAndTimeUpdateStyle'>on " + data.lstAssortmentRulesVODisplays["11"].lstUpdateTs+"</span>");
	} else {
		$(".input-number").val("");
		$(".input-decimal").val("");
		$("#13-select").val("");
		$("#pricing-strategy").val("LEAD");
		$("#showUserAndTimeUpdate").css('display','none');
	}
}

function revertComboboxList() {
	$("#commodityInput").attr("commodityinputid", "");
	$("#commodityInput").attr("commodityinputtext", "");
	$("#commodityInput").val("");
	$("#subCommodityInput").attr("subcommodityinputid", "");
	$("#subCommodityInput").attr("subcommodityinputtext", "");
	$("#subCommodityInput").val("");
	$("#vendorInput").attr("vendorinputtext", "");
	$("#vendorInput").val("");
	for (var key in comboboxList) {
		var obj = comboboxList[key];
		if (obj.type == "commodityInput") {
			$("#commodityInput").attr("commodityinputid", obj.id);
			$("#commodityInput").attr("commodityinputtext", obj.text);
			$("#commodityInput").val(obj.text);
		} else if (obj.type == "subCommodityInput") {
			$("#subCommodityInput").attr("subcommodityinputid", obj.id);
			$("#subCommodityInput").attr("subcommodityinputtext", obj.text);
			$("#subCommodityInput").val(obj.text);
		} else if (obj.type == "vendorInput") {
			$("#vendorInput").val(obj.id);
			$("#vendorInput").attr("vendorinputtext", obj.text);
		}
	}
}

function initCompetitorPricing () {
	getOnlineCompetitors(function (results){
		$('#competitor-tree').jstree({
			"core"	 : {
				"data"			: results,
				"check_callback": true,
				"themes"		: {
					"icons"	 : false,
					"dots" 	 : false,
					"variant": "large",
					"stripes": false
				}
			},
			"plugins": ["checkbox", "wholerow"]
		}).on('ready.jstree', function() { $(this).jstree("open_all"); });
	});
}
	

/**
* Get Online Competitors.
* @author Duyen.le
*/
function getOnlineCompetitors(callback) {   
	$.ajax({
		type   : 'POST',
		url	   : appName + "/ajax/getOnlineCompetitors",                
		success: function (data) { callback(JSON.parse(data)); },
		error  : function (xhr, ajaxOptions, error) {}
	});
}

/************************************************************************************************************
 * END GET DATA
 ************************************************************************************************************/

/************************************************************************************************************
 * SAVE
 ************************************************************************************************************/
$("#pricing-save-btn").click(function () {
	validatePricingRules();
	if (getValid()) {
		var lstCompetitors = [];
		_.each($("#competitor-tree").jstree('get_selected'), function(value, key) {
			if (value !== "0_root") { lstCompetitors.push(value); }
		});
		if (lstCompetitors.length == 0) { 
			bootbox.dialog({
				title  : "Warning",
				message: msgCompetitor,
				buttons: {
					main: {
						label	 : "Ok",
						className: "btn-primary"
					}
				}
			});
		} else {
			bootbox.dialog({
						title  : "Confirmation",
						message: "Do you want to save the changes?",
						buttons: {
							main  : {
								label	 : "Yes",
								className: "btn-primary",
								callback : function() { savePricingRules(); }
							},
							cancel: {
								label	 : "No",
								className: "btn-default"
							}				    	    
						}
			});
		}
	}
});

function savePricingRules() {
	var lstCompetitors = [];
	var lstAssortmentRulesVOs = [];
	var lstPricingRuleVO = [];
	_.each($("#competitor-tree").jstree('get_selected'), function(value, key) {
		if (value !== "0_root") { lstCompetitors.push(value); }
	});
	if (lstCompetitors.length == 0) { bootbox.alert("Please select at least one competitor."); }
	else {
		lstAssortmentRulesVOs.push(getMAPSRPPricingIndex());
		lstAssortmentRulesVOs.push(getTargetGp());
		lstAssortmentRulesVOs.push(getCompetitorIndex());
		lstAssortmentRulesVOs.push(getRecommendedPrice());
		lstAssortmentRulesVOs.push(getIgnorePriceChange());
		lstAssortmentRulesVOs.push(getFlagPriceChangeThan());
		lstAssortmentRulesVOs.push(getFlagPriceChangeNotBetween());
		var pricingRuleVO = {
						commodityId 		 : $("#commodityInput").attr("commodityinputid"),
						subCommodityId 	  	 : ($("#subCommodityInput").attr("subcommodityinputid") == "ALL") ? "" : $("#subCommodityInput").attr("subcommodityinputid"),
						vendorId 			 : $("#vendorInput").val(),
						lvlType 			 : $( "#pricing-strategy option:selected" ).val(),
						lstCompetitors 	  	 : lstCompetitors,
						lstAssortmentRulesVOs: lstAssortmentRulesVOs
			};
		lstPricingRuleVO.push(pricingRuleVO);
		$.ajax({
			type: 'POST',
			url : appName + "/updateRules",
			data:{
	//        	parntEntyId: dataRow.lowestCatId,
	//        	childEntyId: dataRow.entyIdSubCommodity,
				strJson: JSON.stringify(pricingRuleVO)
				//vendorId:$("#vendor-search-txt").attr('vendor-search-txtid')
			}
		}).success(function(data) {
			//debugger;
//			var jsonData = $.parseJSON(data);
			if (data.model.lstAssortmentRulesVODisplays["12"].activeSws[0] == "N") {
				data.model.lstAssortmentRulesVODisplays["12"].values[0] = "";
			}
			if (data.model.lstAssortmentRulesVODisplays["12"].activeSws[1] == "N") {
				data.model.lstAssortmentRulesVODisplays["12"].values[1] = "";
			}
			globalModel = data.model;
			if (!_.isEmpty(data.errorMessage)) { 
				//alert(data); 
			}
			else {
				var successMsg = "Save successfully";
				showNotify(successMsg, "success");
			}
			$("#showUserAndTimeUpdate").css('display','');
			$("#uidAndTsUpdate").html("<span class='userAndTimeUpdateStyle'>Last updated by "+data.model.lstAssortmentRulesVODisplays["11"].lstUpdateUid+"</span>" + " <span class='userAndTimeUpdateStyle'>on " + data.model.lstAssortmentRulesVODisplays["11"].lstUpdateTs+"</span>");
		}).done(function() {
		}).fail(function() {
		}).always(function() {
		});
		$("#pricing-save-btn").prop("disabled", true);
	}
}

function getMAPSRPPricingIndex() {
	var values = [];
	values.push($("#11-input").val());
	var assortmentRulesVO = {
		id 		: "11",
		values 	: values,
		actTypCd: "",
		pctSW	: "Y"
		//type:$("#pricing-strategy option:selected").val()
    }
	return assortmentRulesVO;
}

function getTargetGp() {
	var values = [];
	var yOrN   = "";
	values.push($("#12-1-input").val());
	values.push($("#12-2-input").val());
	var assortmentRulesVO = {
		id 		: "12",
		values  : values,
		actTypCd: "",
		pctSW	: "Y"
		//type:$("#pricing-strategy option:selected").val()
    }
	return assortmentRulesVO;
}

function getCompetitorIndex() {
	var values = [];
	values.push($("#13-input").val());
	var assortmentRulesVO = {
		id 		: "13",
		values 	: values,
		actTypCd: $("#13-select option:selected").val(),
		pctSW	: "N"
		//type:$("#pricing-strategy option:selected").val()
    }
	return assortmentRulesVO;
}

function getRecommendedPrice() {
	var values = [];
	if ($("input:radio[name=rounding_rule]:checked").val() == "0") {
		values.push($("#14-1-input").val());
		values.push("0");
	} else {
		values.push($("#14-2-input").val());
		values.push("1");
	}
	var assortmentRulesVO = {
		id 		: "14",
		values  : values,
		actTypCd: "",
		pctSW	:"N"
		//type:$("#pricing-strategy option:selected").val()
    }
	return assortmentRulesVO;
}

function getIgnorePriceChange() {
	var values = [];
	values.push($("#15-1-input").val());
	values.push($("#15-2-input").val());
	var assortmentRulesVO = {
		id 		: "15",
		values 	: values,
		actTypCd: "",
		pctSW	:"Y"
		//type:$("#pricing-strategy option:selected").val()
    }
	return assortmentRulesVO;
}

function getFlagPriceChangeThan() {
	var values = [];
	values.push($("#16-1-input").val());
	values.push($("#16-2-input").val());
	var assortmentRulesVO = {
		id 		: "16",
		values 	: values,
		actTypCd: "",
		pctSW	:"Y"
		//type:$("#pricing-strategy option:selected").val()
    }
	return assortmentRulesVO;
}

function getFlagPriceChangeNotBetween() {
	var values = [];
	var yOrN   = "";
	if (_.isEmpty($("#12-1-input").val()) == false) { yOrN = "N"; }
	if (_.isEmpty($("#12-2-input").val()) == false) { yOrN = "Y"; }
	values.push($("#17-1-input").val());
	values.push($("#17-2-input").val());
	var assortmentRulesVO = {
		id 		: "17",
		values 	: values,
		actTypCd: "",
		pctSW	: yOrN
		//type:$("#pricing-strategy option:selected").val()
    }
	return assortmentRulesVO;
}

/************************************************************************************************************
 * END SAVE
 ************************************************************************************************************/

/************************************************************************************************************
 * VALIDATION
 ************************************************************************************************************/

/************************************************************************************************************
 * END VALIDATION
 ************************************************************************************************************/

/**
 * display error message by tooltip.
 * @param inputId id of element
 * @param message error 
 * @param placement the placement of message top|right|bottom|left, optional param.
 */
function tooltipForValidationDiv(selectorInput, message, placement, paramFormat){
	if(paramFormat != null){ message = message.format(paramFormat); }
	$(selectorInput).closest('div').addClass("has-error");
	placement = (typeof placement === "undefined") ? "top" : placement;
	$(selectorInput).attr("data-toggle", "tooltip");
	$(selectorInput).attr("data-original-title", message);
	$(selectorInput).tooltip({'placement':placement, 'trigger':'hover manual'});
}

/**
 * clear error message by tooltip.
 * @param inputId id of element
 */
function clearTooltipForValidationDiv(id) {
	$("#" + id).closest('div').removeClass("has-error");
	$("#" + id).tooltip('hide');
	$("#" + id).removeAttr("data-toggle");
	$("#" + id).removeAttr("data-original-title");
}

function setInputsValidate() {
	$("#11-input, #12-1-input, #12-2-input, #14-input, #15-1-input, #15-2-input, #16-1-input, #16-2-input, #17-1-input, #17-2-input, #13-input, #13-select")
	.on("change", function() {
		if ($(this).attr("id") == "13-select") {
			if (_.isEmpty($(this).val())) { tooltipForValidationDiv($("#13-select"), "Field is required", "top"); }
			else { clearTooltipForValidationDiv("13-select"); }
		} else {
			if (_.isEmpty($(this).val()) || isNaN($(this).val())) { tooltipForValidationDiv($("#" + $(this).attr("id")), "Field is required", "top"); } 
			else { clearTooltipForValidationDiv($(this).attr("id")); }
		}
	});
}

function isInvalidToSave() {
	var invalidInputs = [];
	$("#11-input, #12-1-input, #12-2-input, #14-input, #15-1-input, #15-2-input, #16-1-input, #16-2-input, #17-1-input, #17-2-input, #13-input, #13-select")
	.each(function() {
		if ($(this).attr("id") == "13-select") {
			if ($(this).val() == "") {
				tooltipForValidationDiv($("#13-select"), "Field is required", "top");
				invalidInputs.push($(this).attr("id"));
			}
		} else {
			if ($(this).val() == "" || isNaN($(this).val())) {
				tooltipForValidationDiv($("#" + $(this).attr("id")), "Field is required", "top");
				invalidInputs.push($(this).attr("id"));
			}
		}
	});
	if (invalidInputs.length > 0) { return true; } 
	else { return false; }
}

function initForm() {
	
	$("#11-input, #12-1-input, #12-2-input, #14-1-input, #14-2-input, #15-1-input, #15-2-input, #16-1-input, #16-2-input, #17-1-input, #17-2-input, #13-input")
	.on("input", function() {
		if (!$(".alert").is(":hidden")) {
			$(".alert").hide();
		}
	});
	$("#11-input, #12-1-input, #12-2-input, #14-1-input, #14-2-input, #15-1-input, #15-2-input, #16-1-input, #16-2-input, #17-1-input, #17-2-input, #13-input, #13-select, #pricing-strategy")
	.on("change", function() {
		if ($(this).attr("id") == "14-1-input" || $(this).attr("id") == "14-2-input") {
			if (!_.isEmpty($(this).val()) && !isNaN($(this).val())) {
				if (parseInt($(this).val()) == 0) {
					$(this).val("00");
				} else {
					if ($(this).val().length == 1) {
						$(this).val($(this).val() + "0");
					}
				}
			}
		}
		if (isChangedOriginalData()) { $("#pricing-save-btn").prop("disabled", false); } 
		else { $("#pricing-save-btn").prop("disabled", true); }
	});
	//$("#competitor-tree").on("select_node.jstree", function() {
	$("#competitor-tree").on("click", function() {
		if (isChangedOriginalData()) { $("#pricing-save-btn").prop("disabled", false); }
		else { $("#pricing-save-btn").prop("disabled", true); }
	});
	$("input:radio[name='rounding_rule']").on("click", function() {
		if ($("input:radio[name='rounding_rule']:checked").val() == "0") {
			rounding = 0;
			$("#14-1-input").prop("disabled", false);
			$("#14-2-input").val("");
			$("#14-2-input").prop("disabled", true);
			clearTooltipForValidationDiv("14-2-input");
			$("#14-1-input").focus();
		} else {
			rounding = 1;
			$("#14-2-input").prop("disabled", false);
			$("#14-1-input").val("");
			$("#14-1-input").prop("disabled", true);
			clearTooltipForValidationDiv("14-1-input");
			$("#14-2-input").focus();
		}
		if (!isChangedOriginalData()) { $("#pricing-save-btn").prop("disabled", true); } 
		else { $("#pricing-save-btn").prop("disabled", false); }
	});
//	$("#pricing-save-btn").prop("disabled", true);
	$("#commodityInput, #subCommodityInput").on("change blur", function() {
		if ($(this).attr("id") == "commodityInput") {
			if ($(this).val().trim() != $(this).attr("commodityinputtext")) { $(this).val($(this).attr("commodityinputtext")); }
		} else if ($(this).attr("id") == "subCommodityInput") {
			if ($(this).val().trim() != $(this).attr("subcommodityinputtext")) { $(this).val($(this).attr("subcommodityinputtext")); }
		}
	});
	$("#pricing-save-btn").prop("disabled", true);
	$("#subCommodityInput").prop("disabled", true);
	setFormatValueInputs();
}

function resetForm() {
	$("#11-input, #12-1-input, #12-2-input, #14-1-input, #14-2-input, #15-1-input, #15-2-input, #16-1-input, #16-2-input, #17-1-input, #17-2-input, #13-input")
	.each(function() {
		$(this).val("");
		if ($(this).attr("id") == "14-1-input") { $(this).prop("disabled", false); }
		if ($(this).attr("id") == "14-2-input") { $(this).prop("disabled", true); }
	});
	$("input:radio[name='rounding_rule']").filter("[value='0']").prop("checked", true);
	$("#13-select").val("");
	$("#pricing-strategy").val("LEAD");
	$("#17-1-input").prop("disabled", true);
	$("#17-2-input").prop("disabled", true);
}

function setFormatValueInputs() {
	$("#14-1-input, #14-2-input").on("keypress", function(keyPressEvent) {
		var chr 		 = String.fromCharCode(keyPressEvent.which);
		var isSpecial	 = ("~!@#$%^&*()_=+/?<>,[]{}\|".indexOf(chr) >= 0) ? true : false;
		var charCode 	 = (keyPressEvent.which) ? keyPressEvent.which : keyPressEvent.keyCode;
		var isMainKey 	 = $.inArray(charCode, [8, 9, 13, 35, 36, 37, 38, 39, 40, 99, 118]) !== -1; // enter, backspace
		var isNumberKey  = (charCode > 47 && charCode < 58);
		if((!isNumberKey && !isMainKey) || isSpecial) {
			keyPressEvent.preventDefault();
			return false;
		}

	});
}

function isChangedOriginalData() {
	if (!$(".alert").is(":hidden")) {
		$(".alert").hide();
	}
	if (comboboxList.length == 0) {
		setChangeStatus(false);
		return false;
	}
	if (globalModel.lstAssortmentRulesVODisplays == null && ($("#commodityInput").attr("commodityinputtext") != "" || $("#subCommodityInput").attr("subcommodityinputtext") != ""
		|| $("#vendorInput").attr("vendorinputtext") != "")) {
		var arrInput = [];
		$("#11-input, #12-1-input, #12-2-input, #15-1-input, #15-2-input, #16-1-input, #16-2-input, #17-1-input, #17-2-input, #13-input, #13-select")
		.each(function() {
			if ($(this).val() != "") { arrInput.push($(this).attr("id")); }
		});
		if ($("input:radio[name='rounding_rule']:checked").val() == 0) {
			if ($("#14-1-input").val() != "") { arrInput.push("14-1-input"); }
		} else if ($("input:radio[name='rounding_rule']:checked").val() == 1) {
			if ($("#14-2-input").val() != "") { arrInput.push("14-2-input"); }
		}
		if (arrInput.length > 0) { 
			setChangeStatus(true);
			return true; }
		if ($("#pricing-strategy").val().trim() == "FOLOW") { 
			setChangeStatus(true);
			return true; }
		if ($("#competitor-tree").jstree('get_selected').length > 0) { 
			setChangeStatus(true);
			return true; }
	} else if (globalModel != "") {
		if (format($("#11-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["11"].values[0])) { 
			setChangeStatus(true);
			return true; }
		if (format($("#12-1-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["12"].values[0])) { 
			setChangeStatus(true);
			return true; }
		if (format($("#12-2-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["12"].values[1])) { 
			setChangeStatus(true);
			return true; }
		if (format($("#13-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["13"].values[0])) { 
			setChangeStatus(true);
			return true; }
		if ($("#13-select").val() != $.trim(globalModel.lstAssortmentRulesVODisplays["13"].actTypCd)) { 
			setChangeStatus(true);
			return true; }
		if (format($("#15-1-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["15"].values[0])) { 
			setChangeStatus(true);
			return true; }
		if (format($("#15-2-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["15"].values[1])) { 
			setChangeStatus(true);
			return true; }
		if (format($("#16-1-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["16"].values[0])) { 
			setChangeStatus(true);
			return true; }
		if (format($("#16-2-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["16"].values[1])) { 
			setChangeStatus(true);
			return true; }
		if (format($("#17-1-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["17"].values[0])) { 
			setChangeStatus(true);
			return true; }
		if (format($("#17-2-input").val()) !== format(globalModel.lstAssortmentRulesVODisplays["17"].values[1])) { 
			setChangeStatus(true);
			return true; }
		if ($("#pricing-strategy").val() != $.trim(globalModel.lvlType)) { 
			setChangeStatus(true);
			return true; }
		var rule14 = globalModel.lstAssortmentRulesVODisplays["14"].values[0];
		if (parseInt(rule14) == 0) {
			rule14 = "00";
		} else {
			rule14 = rule14.substring(rule14.indexOf(".") + 1, rule14.length);
			rule14 = (rule14.length == 2) ? rule14 : rule14 + "0";
		}
		if ($("input:radio[name='rounding_rule']:checked").val() != globalModel.lstAssortmentRulesVODisplays["14"].values[1]) {
			setChangeStatus(true);
			return true;
		} else {
			if ($("input:radio[name='rounding_rule']:checked").val() == 0) {
				if ($("#14-1-input").val() != rule14) {
					setChangeStatus(true);
					return true; 
				}
			} else {
				if ($("#14-2-input").val() != rule14) { 
					setChangeStatus(true);
					return true; 
				}
			}
		}
		if ($("#competitor-tree").jstree('get_selected').length != globalModel.lstCompetitors.length) { 
			if ($("#competitor-tree").jstree('get_selected').indexOf("0_root") < 0) {
				setChangeStatus(true);
				return true;
			} else {
				if (($("#competitor-tree").jstree('get_selected').length - 1) > globalModel.lstCompetitors.length) {
					setChangeStatus(true);
					return true;
				}
			}
		}
		else {
			for (var j=0; j<$("#competitor-tree").jstree('get_selected').length; j++) {
				var count = 0;
				for (var i=0; i<globalModel.lstCompetitors.length; i++) {
					if ($("#competitor-tree").jstree('get_selected')[j] != globalModel.lstCompetitors[i]) { count++; }
				}
				if (parseInt(count) == parseInt(globalModel.lstCompetitors.length)) {
					setChangeStatus(true);
					return true; 
				}
			}
		}
		setChangeStatus(false);
		return false;
	}
	setChangeStatus(false);
	return false;
}

function format(value) {
	return (value == "") ? "" : parseFloat(value);
}

function formatStringNumer(value) {
	var str = value;
	if (str.length > 0) {
		var strNew = "";
		if (str[0] == "-" && str[1] == "0") {
			for (var i=1; i<str.length; i++) {
				var j = i;
				if (str[i] == "0") {
					strNew = str.substring(j + 1, str.length);
				} else {
					break;
				}
			}
			str = "-" + strNew;
		} else if (str[0] == "0") {
			for (var i=0; i<str.length; i++) {
		    	var j=i;
		       	if (str[i] == "0") {
		         	strNew = str.substring(j + 1, str.length);
		        } else {
		        	break;
		        }
		  	}
		  	str = strNew;
		}
	}
	return str;
}

function enableMinMax(enable) {
	if (enable == true) {
		$("#17-1-input").val("");
		$("#17-2-input").val("");
		clearTooltipForValidationDiv("17-1-input");
		clearTooltipForValidationDiv("17-2-input");
		$("#17-1-input").prop("disabled", true);
		$("#17-2-input").prop("disabled", true);
	} else {
		$("#17-1-input").val("");
		$("#17-2-input").val("");
		clearTooltipForValidationDiv("17-1-input");
		clearTooltipForValidationDiv("17-2-input");
		$("#17-1-input").prop("disabled", false);
		$("#17-2-input").prop("disabled", false);
	}
}

function validatePricingRules() {
	var arrError = [];
	var lstCompetitors = [];
	_.each($("#competitor-tree").jstree('get_selected'), function(value, key) {
		if (value !== "0_root") {
			lstCompetitors.push(value);
		}
	});
	$("#11-input, #13-input, #13-select, #14-1-input, #14-2-input, #15-1-input, #15-2-input, #16-1-input, #16-2-input")
	.each(function() {
		if ($(this).attr("id") != "14-1-input" && $(this).attr("id") != "14-2-input") {
			if ($(this).closest("div").hasClass("has-error")) {
				arrError.push($(this).attr("id"));
			} else if (_.isEmpty($(this).val())) {
				tooltipForValidationDiv(this, msgRequired, "top");
				arrError.push($(this).attr("id"));
			}
		} else {
			if (!$(this).prop("disabled") && $(this).closest("div").hasClass("has-error")) {
				arrError.push($(this).attr("id"));
			} else if (!$(this).prop("disabled") && _.isEmpty($(this).val())) {
				tooltipForValidationDiv(this, msgRequired, "top");
				arrError.push($(this).attr("id"));
			}
		}
	});
	if (arrError.length == 0) {
		var msgError       = "";
		var errorList 	   = [];
		if (_.isEmpty($("#12-1-input").val()) && _.isEmpty($("#12-2-input").val())) {
			setValid(false);
			errorList.push(msgGPRequired);
			if (lstCompetitors.length == 0) {
				errorList.push(msgCompetitor);
			}
			if (errorList.length == 1) {
				msgError = errorList[0];
			} else {
				msgError = errorList[0] + "<br/>";
				msgError += errorList[1];
			}
			bootbox.dialog({
						title  : "Warning",
						message: msgError,
						buttons: {
							main: {
								label	 : "Ok",
								className: "btn-primary"
							}
						}
					});
		} else {
			$("#12-1-input, #12-2-input").each(function() {
				if ($(this).closest("div").hasClass("has-error")) {
					arrError.push($(this).attr("id"));
				}
			});
			$("#17-1-input, #17-2-input").each(function() {
				if ($(this).closest("div").hasClass("has-error")) {
					arrError.push($(this).attr("id"));
				} else if (_.isEmpty($(this).val())) {
					tooltipForValidationDiv(this, msgRequired, "top");
					arrError.push($(this).attr("id"));
				}
			});
			if (arrError.length == 0) {
				if (lstCompetitors.length == 0) {
					bootbox.dialog({
							title  : "Warning",
							message: msgCompetitor,
							buttons: {
								main: {
									label	 : "Ok",
									className: "btn-primary"
								}
							}
						});
					setValid(false);
				} else {
					setValid(true);
				}
			} else {
				setValid(false);
			}
		}
	} else {
		setValid(false);
	}
}

function strTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

function isBelongToPercent(id) {
	if (parseFloat($("#" + id).val()) >= -10000 && parseFloat($("#" + id).val()) <= 10000) {
		return true;
	}
	return false;
}

function isBelongToMoney(id) {
	if (parseFloat($("#" + id).val()) >= -100000 && parseFloat($("#" + id).val()) <= 100000) {
		return true;
	}
	return false;
}

function isBelongToPositiveMoney(id) {
	if (parseFloat($("#" + id).val()) >= 0 && parseFloat($("#" + id).val()) <= 100000) {
		return true;
	}
	return false;
}

function isBelongToPositivePercent(id) {
	if (parseFloat($("#" + id).val()) >= 0 && parseFloat($("#" + id).val()) <= 10000) {
		return true;
	}
	return false;
}

function isMinMaxError() {
	if ($("#17-1-input").closest("div").hasClass("has-error") || $("#17-2-input").closest("div").hasClass("has-error")) {
		return true;
	}
	return false;
}

function setValidatePricingRules() {
	$("#13-select").on("change", function() {
		if (_.isEmpty($(this).val())) {
			tooltipForValidationDiv(this, msgRequired, "top");
		} else {
			clearTooltipForValidationDiv($(this).attr("id"));
		}
	});
	$("#13-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		var value = $(this).val();
		if (_.isEmpty(value)) {
			tooltipForValidationDiv(this, msgRequired, "top");
		} else {
			if (isNaN(value)) {
				tooltipForValidationDiv(this, msgInvalid, "top");
			} else {
				if (isBelongToPercent($(this).attr("id"))) {
					clearTooltipForValidationDiv($(this).attr("id"));
					$(this).val(precise_roundFloatNum(value));
				} else {
					tooltipForValidationDiv(this, msgRangePercent, "top");
				}
			}
		}
	});
	$("#14-1-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		if (rounding == 0 && _.isEmpty($(this).val())) {
			tooltipForValidationDiv(this, msgRequired, "top");
		} else {
			if (isNaN($(this).val())) {
				tooltipForValidationDiv(this, msgInvalid, "top");
			} else {
				clearTooltipForValidationDiv($(this).attr("id"));
			}
		}
	});
	$("#14-2-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		if (rounding == 1 && _.isEmpty($(this).val())) {
			tooltipForValidationDiv(this, msgRequired, "top");
		} else {
			if (isNaN($(this).val())) {
				tooltipForValidationDiv(this, msgInvalid, "top");
			} else {
				clearTooltipForValidationDiv($(this).attr("id"));
			}
		}
	});
	$("#11-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		var value = $(this).val();
		if (_.isEmpty(value)) {
			tooltipForValidationDiv(this, msgRequired, "top");
		} else {
			if (isNaN(value)) {
				tooltipForValidationDiv(this, msgInvalid, "top");
			} else {
				if (isBelongToPercent($(this).attr("id"))) {
					clearTooltipForValidationDiv($(this).attr("id"));
					$(this).val(precise_roundFloatNum(value));
				} else {
					tooltipForValidationDiv(this, msgRangePercent, "top");
				}
			}
		}
	});
	$("#15-1-input, #16-1-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		var value = $(this).val();
		if (_.isEmpty(value)) {
			tooltipForValidationDiv(this, msgRequired, "top");
		} else {
			if (isNaN(value)) {
				tooltipForValidationDiv(this, msgInvalid, "top");
			} else {
				if (isBelongToPositiveMoney($(this).attr("id"))) {
					clearTooltipForValidationDiv($(this).attr("id"));
					$(this).val(precise_roundFloatNum(value));
				} else {
					tooltipForValidationDiv(this, msgRangePositiveDollar, "top");
				}
			}
		}
	});
	$("#15-2-input, #16-2-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		var value = $(this).val();
		if (_.isEmpty(value)) {
			tooltipForValidationDiv(this, msgRequired, "top");
		} else {
			if (isNaN(value)) {
				tooltipForValidationDiv(this, msgInvalid, "top");
			} else {
				if (isBelongToPositivePercent($(this).attr("id"))) {
					clearTooltipForValidationDiv($(this).attr("id"));
					$(this).val(precise_roundFloatNum(value));
				} else {
					tooltipForValidationDiv(this, msgRangePositivePercent, "top");
				}
			}
		}
	});
	$("#12-1-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		var value = $(this).val();
		if (_.isEmpty(value)) {
			enableMinMax(true);
			clearTooltipForValidationDiv($(this).attr("id"));
		} else {
			if (!_.isEmpty(strTrim($("#12-2-input").val()))) {
				$("#12-2-input").val("");
				clearTooltipForValidationDiv("12-2-input");
				enableMinMax(false);
			}
			if (isNaN(value)) {
				enableMinMax(true);
				tooltipForValidationDiv(this, msgInvalid, "top");
			} else {
				if ($("#17-1-input").prop("disabled")) {
					if (isBelongToMoney($(this).attr("id"))) {
						clearTooltipForValidationDiv($(this).attr("id"));
						$("#17-1-input").attr("maxLength", 10);
						$("#17-2-input").attr("maxLength", 10);
						$("#17-1-input").on("keypress", function(event) {
							allowDecimalNumber(this, event, 12, 2);
						});
						$("#17-2-input").on("keypress", function(event) {
							allowDecimalNumber(this, event, 12, 2);
						});
						enableMinMax(false);
					} else {
						tooltipForValidationDiv(this, msgRangeDollar, "top");
					}
				} else {
					if (isMinMaxError()) {
						if (isBelongToMoney($(this).attr("id"))) {
							clearTooltipForValidationDiv($(this).attr("id"));
						} else {
							enableMinMax(true);
							tooltipForValidationDiv(this, msgRangeDollar, "top");
						}
					} else {
						var min = $("#17-1-input").val();
						var max = $("#17-2-input").val();
						if (isBelongToMoney($(this).attr("id"))) {
							if (parseFloat(value) < parseFloat(min) || parseFloat(value) > parseFloat(max)) {
								tooltipForValidationDiv(this, msgGP, "top");
							} else {
								clearTooltipForValidationDiv($(this).attr("id"));
							}
						} else {
							enableMinMax(true);
							tooltipForValidationDiv(this, msgRangeDollar, "top");
						}
					}
				}
				if (!_.isEmpty(strTrim($(this).val())) && isBelongToMoney("12-1-input")) {
					$(this).val(precise_roundFloatNum($(this).val()));
				}
			}
		}
	});
	$("#12-2-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		var value = $(this).val();
		if (_.isEmpty(value)) {
			enableMinMax(true);
			clearTooltipForValidationDiv($(this).attr("id"));
		} else {
			if (!_.isEmpty(strTrim($("#12-1-input").val()))) {
				$("#12-1-input").val("");
				clearTooltipForValidationDiv("12-1-input");
				enableMinMax(false);
			}
			if (isNaN(value)) {
				enableMinMax(true);
				tooltipForValidationDiv(this, msgInvalid, "top");
			} else {
				if ($("#17-1-input").prop("disabled")) {
					if (isBelongToPercent($(this).attr("id"))) {
						clearTooltipForValidationDiv($(this).attr("id"));
						$("#17-1-input").attr("maxLength", 9);
						$("#17-2-input").attr("maxLength", 9);
						$("#17-1-input").on("keypress", function(event) {
							allowDecimalNumber(this, event, 11, 2);
						});
						$("#17-2-input").on("keypress", function(event) {
							allowDecimalNumber(this, event, 11, 2);
						});
						enableMinMax(false);
						$(this).val(precise_roundFloatNum(value));
					} else {
						tooltipForValidationDiv(this, msgRangePercent, "top");
					}
				} else {
					if (isMinMaxError()) {
						if (isBelongToPercent($(this).attr("id"))) {
							clearTooltipForValidationDiv($(this).attr("id"));
						} else {
							enableMinMax(true);
							tooltipForValidationDiv(this, msgRangePercent, "top");
						}
					} else {
						var min = $("#17-1-input").val();
						var max = $("#17-2-input").val();
						if (isBelongToPercent($(this).attr("id"))) {
							if (parseFloat(value) < parseFloat(min) || parseFloat(value) > parseFloat(max)) {
								tooltipForValidationDiv(this, msgGP, "top");
							} else {
								clearTooltipForValidationDiv($(this).attr("id"));
							}
						} else {
							enableMinMax(true);
							tooltipForValidationDiv(this, msgRangePercent, "top");
						}
					}
				}
				if (!_.isEmpty(strTrim($(this).val())) && isBelongToPercent("12-2-input")) {
					$(this).val(precise_roundFloatNum($(this).val()));
				}
			}
		}
	});
	
	$("#17-1-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		var value = $(this).val();
		if (!_.isEmpty($("#12-1-input").val())) {
			if (_.isEmpty(value)) {
				clearTooltipForValidationDiv("12-1-input");
				clearTooltipForValidationDiv("12-2-input");
				if (isBelongToMoney("17-2-input") && !_.isEmpty($("#17-2-input").val())) {
					clearTooltipForValidationDiv("17-2-input");
				}
				tooltipForValidationDiv(this, msgRequired, "top");
			} else {
				if (isNaN(value)) {
					clearTooltipForValidationDiv("12-1-input");
					clearTooltipForValidationDiv("12-2-input");
					if (isBelongToMoney("17-2-input") && !_.isEmpty($("#17-2-input").val())) {
						clearTooltipForValidationDiv("17-2-input");
					}
					tooltipForValidationDiv(this, msgInvalid, "top");
				} else {
					if (isBelongToMoney($(this).attr("id"))) {
						var max = $("#17-2-input").val();
						clearTooltipForValidationDiv($(this).attr("id"));
						if (!_.isEmpty(max) && isBelongToMoney("17-2-input")) {
							if (parseFloat(value) >= parseFloat(max)) {
								clearTooltipForValidationDiv("17-2-input");
								clearTooltipForValidationDiv("12-1-input");
								clearTooltipForValidationDiv("12-2-input");
								tooltipForValidationDiv(this, msgMin, "top");
							} else {
								clearTooltipForValidationDiv($(this).attr("id"));
								clearTooltipForValidationDiv("17-2-input");
								if (parseFloat($("#12-1-input").val()) < parseFloat(value) || parseFloat($("#12-1-input").val()) > parseFloat(max)) {
									tooltipForValidationDiv($("#12-1-input"), msgGP, "top");
								} else {
									clearTooltipForValidationDiv("12-1-input");
								}
							}
						}
					} else {
						clearTooltipForValidationDiv("12-1-input");
						clearTooltipForValidationDiv("12-2-input");
						tooltipForValidationDiv(this, msgRangeDollar, "top");
					}
				}
			}
			if (!_.isEmpty(strTrim($(this).val())) && isBelongToMoney("17-1-input")) {
				$(this).val(precise_roundFloatNum($(this).val()));
			}
		}
		if (!_.isEmpty($("#12-2-input").val())) {
			$(this).val(strTrim($(this).val()));
			var value = $(this).val();
			if (_.isEmpty(value)) {
				clearTooltipForValidationDiv("12-1-input");
				clearTooltipForValidationDiv("12-2-input");
				if (isBelongToPercent("17-2-input") && !_.isEmpty($("#17-2-input").val())) {
					clearTooltipForValidationDiv("17-2-input");
				}
				tooltipForValidationDiv(this, msgRequired, "top");
			} else {
				if (isNaN(value)) {
					clearTooltipForValidationDiv("12-1-input");
					clearTooltipForValidationDiv("12-2-input");
					if (isBelongToPercent("17-2-input") && !_.isEmpty($("#17-2-input").val())) {
						clearTooltipForValidationDiv("17-2-input");
					}
					tooltipForValidationDiv(this, msgInvalid, "top");
				} else {
					if (isBelongToPercent($(this).attr("id"))) {
						var max = $("#17-2-input").val();
						clearTooltipForValidationDiv($(this).attr("id"));
						if (!_.isEmpty(max) && isBelongToPercent("17-2-input")) {
							if (parseFloat(value) >= parseFloat(max)) {
								clearTooltipForValidationDiv("17-2-input");
								clearTooltipForValidationDiv("12-1-input");
								clearTooltipForValidationDiv("12-2-input");
								tooltipForValidationDiv(this, msgMin, "top");
								$(this).val(precise_roundFloatNum(value));
							} else {
								clearTooltipForValidationDiv($(this).attr("id"));
								clearTooltipForValidationDiv("17-2-input");
								if (parseFloat($("#12-2-input").val()) < parseFloat(value) || parseFloat($("#12-2-input").val()) > parseFloat(max)) {
									tooltipForValidationDiv($("#12-2-input"), msgGP, "top");
								} else {
									clearTooltipForValidationDiv("12-2-input");
								}
							}
						}
					} else {
						clearTooltipForValidationDiv("12-1-input");
						clearTooltipForValidationDiv("12-2-input");
						tooltipForValidationDiv(this, msgRangePercent, "top");
					}
				}
			}
			if (!_.isEmpty(strTrim($(this).val())) && isBelongToPercent("17-1-input")) {
				$(this).val(precise_roundFloatNum($(this).val()));
			}
		}
	});
	$("#17-2-input").on("change", function() {
		$(this).val(strTrim($(this).val()));
		var value = $(this).val();
		if (!_.isEmpty($("#12-1-input").val())) {
			if (_.isEmpty(value)) {
				clearTooltipForValidationDiv("12-1-input");
				clearTooltipForValidationDiv("12-2-input");
				if (isBelongToMoney("17-1-input") && !_.isEmpty($("#17-1-input").val())) {
					clearTooltipForValidationDiv("17-1-input");
				}
				tooltipForValidationDiv(this, msgRequired, "top");
			} else {
				if (isNaN(value)) {
					clearTooltipForValidationDiv("12-1-input");
					clearTooltipForValidationDiv("12-2-input");
					if (isBelongToMoney("17-1-input") && !_.isEmpty($("#17-1-input").val())) {
						clearTooltipForValidationDiv("17-1-input");
					}
					tooltipForValidationDiv(this, msgInvalid, "top");
				} else {
					if (isBelongToMoney($(this).attr("id"))) {
						var min = $("#17-1-input").val();
						clearTooltipForValidationDiv($(this).attr("id"));
						if (!_.isEmpty(min) && isBelongToMoney("17-1-input")) {
							if (parseFloat(value) <= parseFloat(min)) {
								clearTooltipForValidationDiv("17-1-input");
								clearTooltipForValidationDiv("12-1-input");
								clearTooltipForValidationDiv("12-2-input");
								tooltipForValidationDiv(this, msgMax, "top");
							} else {
								clearTooltipForValidationDiv($(this).attr("id"));
								clearTooltipForValidationDiv("17-1-input");
								if (parseFloat($("#12-1-input").val()) > parseFloat(value) || parseFloat($("#12-1-input").val()) < parseFloat(min)) {
									tooltipForValidationDiv($("#12-1-input"), msgGP, "top");
								} else {
									clearTooltipForValidationDiv("12-1-input");
								}
							}
						}
					} else {
						clearTooltipForValidationDiv("12-1-input");
						clearTooltipForValidationDiv("12-2-input");
						tooltipForValidationDiv(this, msgRangeDollar, "top");
					}
				}
			}
			if (!_.isEmpty(strTrim($(this).val())) && isBelongToMoney("17-2-input")) {
				$(this).val(precise_roundFloatNum($(this).val()));
			}
		}
		if (!_.isEmpty($("#12-2-input").val())) {
			if (_.isEmpty(value)) {
				clearTooltipForValidationDiv("12-1-input");
				clearTooltipForValidationDiv("12-2-input");
				if (isBelongToPercent("17-1-input") && !_.isEmpty($("#17-1-input").val())) {
					clearTooltipForValidationDiv("17-1-input");
				}
				tooltipForValidationDiv(this, msgRequired, "top");
			} else {
				if (isNaN(value)) {
					clearTooltipForValidationDiv("12-1-input");
					clearTooltipForValidationDiv("12-2-input");
					if (isBelongToPercent("17-1-input") && !_.isEmpty($("#17-1-input").val())) {
						clearTooltipForValidationDiv("17-1-input");
					}
					tooltipForValidationDiv(this, msgInvalid, "top");
				} else {
					if (isBelongToPercent($(this).attr("id"))) {
						var min = $("#17-1-input").val();
						clearTooltipForValidationDiv($(this).attr("id"));
						if (!_.isEmpty(min) && isBelongToPercent("17-1-input")) {
							if (parseFloat(value) <= parseFloat(min)) {
								clearTooltipForValidationDiv("17-1-input");
								clearTooltipForValidationDiv("12-1-input");
								clearTooltipForValidationDiv("12-2-input");
								tooltipForValidationDiv(this, msgMax, "top");
								$(this).val(precise_roundFloatNum(value));
							} else {
								clearTooltipForValidationDiv($(this).attr("id"));
								clearTooltipForValidationDiv("17-1-input");
								if (parseFloat($("#12-2-input").val()) > parseFloat(value) || parseFloat($("#12-2-input").val()) < parseFloat(min)) {
									tooltipForValidationDiv($("#12-2-input"), msgGP, "top");
								} else {
									clearTooltipForValidationDiv("12-2-input");
								}
							}
						}
					} else {
						clearTooltipForValidationDiv("12-1-input");
						clearTooltipForValidationDiv("12-2-input");
						tooltipForValidationDiv(this, msgRangePercent, "top");
					}
				}
			}
			if (!_.isEmpty(strTrim($(this).val())) && isBelongToPercent("17-2-input")) {
				$(this).val(precise_roundFloatNum($(this).val()));
			}
		}
	});
}




//var dataAutoCompleteInput = [];
//    dataAutoCompleteInputValue= [];
//    dataAutoCompleteInputLabel= [];
//$(document).ready(function(){
//  
//  autoCompleteInit("vendorInput", "/vendor-management/search-for-vendors");
//  $("#aSelectAll").click(function(){
//     $(this).closest('form').find('input[type=checkbox]').each(function() { //loop through each checkbox
//      this.checked = true;                       
//      });         
//
//    });
//  
//  
//  getOnlineCompetitors(function (results){
//    $('#competitor-tree').jstree({
//      "core": {
//        "data": results,
//        "check_callback": true,
//            "themes":{
//                "icons":false,
//          "dots" : false,
//          "variant" : "large",
//          "stripes" : false
//            }
//        },
//        "plugins" : ["checkbox", "wholerow"]
//    }).on('ready.jstree', function() {
//        $(this).jstree("open_all");          
//    });
//   });
//  autoCompleteInit("commodityInput", "/ajax/get-autocomplete-commodities");
//  autoCompleteGetData("subCommodityInput", "/ajax/get-autocomplete-subCommodities");  
//});
//
///**
// * Get Online Competitors.
// * @author Duyen.le
// */
//function getOnlineCompetitors(callback) {   
//    $.ajax({
//        type: 'POST',
//        url: appName + "/ajax/getOnlineCompetitors",                
//       
//        success: function (data) {                  
//            callback(JSON.parse(data));
//        },
//        error: function (xhr, ajaxOptions, error) {                    
//            alert('Error: ' + xhr.responseText);
//        }
//    });
//}
///**
// * common auto complete for textboxes to search.
// * @author Duyenle
// */
//selectedLabelBefore = "";
//selectedIdBefore = "";
//function autoCompleteGetData(idInput, url){
//  var inputData;
//  
//  if(idInput==='subCommodityInput'){
//    inputData = $("#commodityInput").val();
//  }  
//  $( "#" + idInput ).autocomplete({
//        search: function(event, ui) {
//            $(this).addClass("wait");
//            $(this).parent().find("span").removeClass("caret");
//        },
//        open: function(event, ui) {
//            $(this).removeClass("wait");
//            $(this).parent().find("span").addClass("caret");
//        },
//
//       source:function (request, response) {
//            var inputRaw = idInput.split("-");
//            var nameProperty = inputRaw[0];
//            //remove loading screen
//            $(document).unbind('ajaxSend');
//            $.ajax({
//                type: "POST",
//                url: appName + url,
//                data: {
//                  commodityId : inputData
//                },
//                dataType: "json",
//                success: function (data) {
//                    dataAutoCompleteInput = data.data;
//                    for (var i in dataAutoCompleteInput) {
//                        dataAutoCompleteInputValue.push(dataAutoCompleteInput[i].label);
//                        dataAutoCompleteInputLabel.push(dataAutoCompleteInput[i].value);
//                    }
//                    response(dataAutoCompleteInput);
//
//                    if(dataAutoCompleteInput.length == 0){
//                        $("#" + idInput).removeClass("wait");
//                        $("#" + idInput).parent().find("span").addClass("caret");
//                    }
//                    //re-attach loading screen
//                    $(document).bind("ajaxSend", function(e, xhr, settings){
//                        $.blockUI({
//                            message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
//                            fadeIn: 100,
//                            onUnblock: function() {
//                            }
//                        });
//
//                        
//                    }).bind("ajaxStop", function(){
//                        $.unblockUI();
//                    }).bind('ajaxError', handleAjaxError);
//
//                },
//                error: function (result) {
//                    alert("Error......");
//                }
//            });
//        },
//       minLength: 1,
//       select: function(event, ui) {
//           $("#"+idInput).val(ui.item.label);
//           if ($.inArray($("#"+idInput).val(), dataAutoCompleteInputValue) !== -1 
//                   || $.inArray($("#"+idInput).val(), dataAutoCompleteInputLabel) !== -1) {
//               
//                   $( "#" + idInput ).attr( idInput +"id", ui.item.value );
//           } else {
//             $( "#" + idInput ).attr( idInput +"id", "");
//           }
//           selectedLabelBefore = ui.item.label;
//           selectedIdBefore = ui.item.value;
//           return false;
//       },
//       focus: function(event, ui) {
//           $("#"+idInput).val(ui.item.label);
//
//           if ($.inArray($("#"+idInput).val(), dataAutoCompleteInputValue) !== -1 
//                   || $.inArray($("#"+idInput).val(), dataAutoCompleteInputLabel) !== -1) {
//                
//                $('.ui-menu-item').attr( "title", ui.item.label);
//
//                $( "#" + idInput ).attr( idInput +"id", ui.item.value );
//           } else {
//             $( "#" + idInput ).attr( idInput +"id", "");
//           }
//           selectedLabelBefore = ui.item.label;
//           selectedIdBefore = ui.item.value;
//           
//           return false;
//       }
//    }).focusout(function() {
//        if(!$( "#" + idInput ).attr(idInput +"id")){
//            $("#"+idInput).val(selectedLabelBefore);
//            $( "#" + idInput ).attr( idInput +"id", selectedIdBefore);
//        }
//        
//    }).keyup(function (e) {
//        $(this).attr(idInput +"id", "");
//    }).focus(); 
//}
