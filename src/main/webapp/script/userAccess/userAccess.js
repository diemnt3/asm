/* @author Khoi.Le */
var tbl = initDataTable();
var counter = 0;
var selectedVendor = [];
var venTbl = initVendorDataTable();
var dataAutoCompleteInput = [];
var dataAutoCompleteInputValue= [];
var dataAutoCompleteInputLabel= [];
var inputId = "";
$(document).ready(function() {	
	defaultButton();
	$(".hasclear").keyup(function () {
	  var t = $(this);
	  t.next('span').toggle(Boolean(t.val()));
	});
	$(".clearer").hide($(this).prev('input').val());
	$(".clearer").click(function () {
	  $(this).prev('input').val('').focus();
	  $(this).hide();
	  $('#vendor-table').DataTable().draw();
	});
	$("#add-user-icon, #add-user-btn").click(function() {
		disableButton();
		jQuery(".user-id").focus();
		tbl.column( '6' ).order( 'asc' );	
		var inputId = "user-id-input" + counter;		
		var rowNode = $('#user-table').dataTable().fnAddData(
        {
	        "rowNumber": "",
	        "userId":   '<div class="form-group">'
	        	+ '<input name="inputUserId" id="user-id-input'+counter+'" type="text" class="form-control input-sm new-cell user-id user-id-auto">'
	        	+ '</div>',
	        'userName': '',
	        'accType': ''
        }, true);
		$('.new-cell').parent().parent().find('td').addClass('highlight');
		autoComplete(inputId);
		$('#user-table>tbody>tr:first>td:nth-child(2) input.user-id').focus();
	});
	$("#user-table input.user-id").focusout(function() {
		var inputId = "user-id-input" + counter;
		autoComplete(inputId);
	});
	//save form function
	$("#save-user-icon, #save-user-btn").click(function() {
		check = true;
		$('#user-table>tbody>tr:first>td:nth-child(2)>input').each(function() {
			var enterval = $(this).val();
			if(enterval == "" || enterval == null) {
				enterval = $(this).text();
			}
			$('#user-table>tbody>tr>td:nth-child(2)').each(function() {
				if($(this).text() == enterval){
					showNotify(' User Id already existed. Please input another User Id.', 'danger');
					updateValidationStatus();
					check = false;
					return false;
				}
			});		  		
		});
		$('#user-table>tbody>tr:first>td:nth-child(3)').each(function() {
			if($(this).text() == ""){
				showNotify(' User Name and Access Type are invalid.', 'danger');
				check = false;
				return false;
			}
		});
		//save data
		if(check == true){				
			var dataTable = $('#user-table').DataTable().rows().data();
			var dta = {};
			var lstUserId = "";
			for(i=0; i<dataTable.length; i++) {
				if(dataTable[i].isChecked){
					lstUserId = dataTable[i].userId;
					
					dta[dataTable[i].userId] = [];
					dta[dataTable[i].userId] = dataTable[i].vendor;
				}
			}
			$.ajax({
				type: "POST",
				dataType: "json",
				beforeSend: function(jqXHR, settings) {
					$.blockUI({
						message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
						fadeIn: 100,
						onUnblock: function() {
						}
					});
				},
				data: {data: JSON.stringify( dta)},
				url: appName + "/user/create-user",
				success: function(data) {						
					if(data.data == false){
						var mess = 'Please select at least one vendor to continue';
						showNotify(mess, 'danger');
					} else {
						defaultButton();
						if(lstUserId != ""){
							var mess = 'User has been created successfully.';
							showNotify(mess, 'success');
							replaceContent(2);
							// update vendor list
							var text = '<li><a href="#" class="show-list-vendor"><span class="glyphicon glyphicon-star"></span> Select Vendors</a></li>';
							for(i=0; i<dataTable.length; i++) {
								if(dataTable[i].isChecked && dataTable[i].vendor[0].id == "-1"){
									dataTable[i].isChecked = false;										
									$("#vendor-menu-" + dataTable[i].userId).html(text);
								}
							}
						} else {
							var mess = 'User has been updated successfully.';
							showNotify(mess, 'success');
						}
						$("#user-table>tbody>tr").each(function() {
							$(this).removeClass("success");
							$(this).parent().find("td.highlight").each(function() {
								$(this).removeClass("highlight");
							});
						});
					}			        	
				},
				error: function(jqXHR, textStatus, errorThrown) {
					var mess = 'Please select at least one vendor to continue';
					showNotify(mess, 'danger');				        	
				},
				complete: function(jqXHR, textStatus) {
					$.unblockUI();
					$("#save-user-btn, #save-user-icon").attr("disabled", true);
				}
			});
		}
	});	
	//index for table
	tbl.on( 'order.dt', function () {
        tbl.column(0, {order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        });
    } ).draw();	
	// filter DB 
	$("#search-vendor-name-btn").keyup(function(event) {
		//remove loading screen
		$(document).unbind('ajaxSend');
		if (timer.isActive) { timer.stop();}
		timer.play();		
	});
});
//end document ready

/* @author Khoi.Le */
//update input status
function updateValidationStatus() {
	$(".user-id").parent().removeClass("has-success");
	$(".user-id").parent().addClass("has-error");
	$("i[data-bv-icon-for=inputUserId]").removeClass("glyphicon-ok");
	$("i[data-bv-icon-for=inputUserId]").addClass("glyphicon-remove");
}
//1s after key up
var timer = $.timer(function() {
	$('#vendor-table').DataTable().draw();
	timer.stop();
});
timer.set({ time : 500, autostart : false });
// functions
/* @author Khoi.Le */
// initiate user dataTable
function initDataTable(){
	var user_tbl = $('#user-table').DataTable( {
		"columnDefs": [ 
			{	    	
				"orderable": false,
				"render": function ( data, type, row ) {
					var i = 0;
					return i;
				},
				"width": "5%",
				"targets": 0
			},
			{
				"render": function ( data, type, row ) {
					return data;
				},	      
				"width": "20%",
				"targets": 1
			},
			{	      
				"width": "20%",
				"render": function ( data, type, row ) {
					return data;
				},
				"targets": 2
			},
			{	    	
				"width": "20%",
				"render": function ( data, type, row ) {
					return data;
				},
				"targets": 3
			},
			{
				"width": "30%",
				"render": function ( data, type, row ) {
					var vendorLst = "";
					if(row['vendor']){
						generateId = row['userId'];
						if(row['vendor'][0].id== -1){
							return '<div class="btn-group">'+
								' <a class="btn btn-default dropdown-toggle btn-select number-vendors" data-toggle="dropdown" href="#">All Vendors <span class="caret"></span></a> '+
								'<ul class="dropdown-menu" id="vendor-menu-'+generateId+'">'+
								'<li><a href="#" class="show-list-vendor"><span class="glyphicon glyphicon-star"></span> Edit Vendor List</a></li>'+
								'<li><a href="javascript:void(0)" class="number-vendors-choose" vendor-id="0" class="choose-combo"></a></li>'+
								'</ul></div>';
						}else{
							for (var i = 0; i < row['vendor'].length; i++) {
								var vendorLst = vendorLst + '<li class="vendor-item"><a vendor-id="'+row['vendor'][i].id+'"><span class="text-ellipsis">'+row['vendor'][i].nameFormat+'</span> <span class="del-icon"></span></a></li>'
							};
							return '<div class="btn-group">'+
							'<a class="btn btn-default dropdown-toggle btn-select number-vendors" data-toggle="dropdown" href="#">'+row['vendor'].length+' Vendors <span class="caret"></span></a>'+
							'<ul class="dropdown-menu" id="vendor-menu-'+generateId+'">'+ vendorLst +
							'<li class="divider"></li><li><a href="#" class="show-list-vendor"><span class="glyphicon glyphicon-star"></span> Edit Vendor List</a></li><li><a href="javascript:void(0)" vendor-id="all" class="choose-combo all"><span class="glyphicon glyphicon-star"></span> All Vendors</a></li>';
						}
					}else{
						return '<div class="btn-group">'+
								' <a class="btn btn-default dropdown-toggle btn-select number-vendors" data-toggle="dropdown" href="#" vendor-id="all">All Vendors <span class="caret"></span></a> '+
								'<ul class="dropdown-menu" id="vendor-menu'+counter+'">'+
								'<li><a href="#" class="show-list-vendor"><span class="glyphicon glyphicon-star"></span> Edit Vendor List</a></li>'+
								'</ul></div>';
					}
				},
				"targets": 4
			},
			{
				"orderable": false,
				"render": function ( data, type, row ) {
					return '<span class="action glyphicon glyphicon-remove cursor-pointer" style="color:#d9534f; font-size:20px"></span>';
				},	      
				"width": "5%",
				"targets": 5	      
			},
			{	    	
				"width": "0%",
				"render": function ( data, type, row ) {
					counter--;
					return counter;
				},		  
				"targets": 6,
				"visible": false	
			},
			{	    	
				"width": "0%",
				"render": function ( data, type, row ) {
					return "<input type='checkbox' name='setChecked' value='" + row.isChecked + "' />";
				},		  
				"targets": 7,
				"visible": false	
			}
		],
		"processing": false,
		"order": [[ 0, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],
    	"sDom": "tripl",
    	"pagingType": "full_numbers",
    	"stateSave": true,
	    "ajax": {
	        "url": appName + '/user/get-user-list',
	        "type": 'GET'	       
	    },
	    "columns": [
				{ "data": "" },
				{ "data": "userId"},
				{ "data": "userName" },
				{ "data": "accType" },
				{ "data": "vendor" },
				{ "data": "" },
    			{ "data": "" },
				{ "data": "isChecked" }
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		},
		"rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-right');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-left');
           $('td:eq(3)', row).addClass('text-left');
           $('td:eq(4)', row).addClass('text-center');
           $('td:eq(5)', row).addClass('text-center');
		   $('td:eq(6)', row).addClass('hidden');
		   data.flag = 0;
		}
	});	
	//mouseover to show icon to quick delete
	$(document).on('mouseover', '#user-table tbody li.vendor-item', function () {
		$(this).children().find('.del-icon').addClass('glyphicon glyphicon-remove remove-vendor');
	}).on('mouseout', '#user-table tbody li.vendor-item', function() {
        $(this).children().find('.del-icon').removeClass('glyphicon glyphicon-remove remove-vendor');
    });
    //end mouseover to show icon to quick delete
    //mouseover to show background icon to quick delete
    $('#user-table tbody').on('mouseover', 'li.vendor-item span.del-icon', function () {
		$(this).addClass('showbgIcon');
	}).on('mouseout', 'li.vendor-item span.del-icon', function() {
        $(this).removeClass('showbgIcon');
    });
	//end mouseover to show background icon to quick delete
	//remove quickly vendor on dropdown list
	$( document ).on( "click","#user-table tr td .vendor-item span", function() {
		var oldNumbVendor = $(this).parent().parent().parent().parent().find('li.vendor-item').length;
		var newNumbVendor = oldNumbVendor - 1;
		validateToEnableSaveBtn();	
		if(newNumbVendor > 0) {
			$(this).parent().parent().parent().parent().find('.number-vendors').html('<span class="glyphicon glyphicon-star"></span> ' + newNumbVendor + ' Vendors');
			$(this).parent().parent().parent().parent().find('.number-vendors').html(newNumbVendor + " Vendors" + '<span class="caret"></span>');
			$(this).parent().parent().parent().find("a.number-vendors-choose").html('<span class="glyphicon glyphicon-star"></span> ' + newNumbVendor + ' Vendors');
			table = $('#user-table').DataTable();
			var vendorId = $(this).parent().attr("vendor-id");
			var buLstVendor = table.row($(this).parent().parent().parent().parent().parent().parent()).data().vendor;
			var lstVendor = [];
			for (i = 0; i < buLstVendor.length; ++i) {
				if(vendorId !== buLstVendor[i].id){
					lstVendor.push(buLstVendor[i]);
				}
			}
			table.row($(this).parent().parent().parent().parent().parent().parent()).data().isChecked = true;
			table.row($(this).parent().parent().parent().parent().parent().parent()).data().vendor = lstVendor;
		} else {
			disableButton();
			var check = false;
			if($(this).parent().length == 1) {
				check = true;
			}
			if(check == true) {
				$(this).parent().parent().parent().parent().find('.number-vendors').html('<span class="glyphicon glyphicon-star"></span> Edit Vendor List');
				$(this).parent().parent().parent().parent().find('.number-vendors').html('Edit Vendor List' + '<span class="caret"></span>');
				var text = $(this).parent().parent().parent().parent().parent().parent().parent().find("td:nth-child(2)").html();
				if(text.indexOf("<input")) {
					text = $(this).parent().parent().parent().parent().parent().parent().parent().find("td:nth-child(2) input").val();
				}
				var mess = 'Please assign at least one vendor for user ' + text;
				showNotify(mess, 'danger');
				$(this).parent().parent().parent().find("a.number-vendors-choose").html('<span class="glyphicon glyphicon-star"></span> All Vendors');
			}
		}
		$(this).parent().parent().remove();	
	});
	//end remove quickly vendor on dropdown list	
	//custom choose combo list
	$( document ).on( "click","#user-table tbody tr td a.choose-combo", function() {
		if($(this).parent().parent().parent().parent().parent().find("td:nth-child(2)").html().indexOf("<div") === -1){
			$("#add-user-btn, #add-user-icon").attr("disabled", false);
		}
		var selText = $(this).text();
		var oldText = $(this).parents('.btn-group').find('.dropdown-toggle').html().trim().split(" ");
		var oldVendor = oldText[0] + " " + oldText[1];
		if(oldVendor != selText){
			$(this).parents('.btn-group').parent().parent().find('td').addClass('highlight');
		}
		$(this).parents('.btn-group').find('.dropdown-toggle').attr("vendor-id", $(this).attr("vendor-id"));
		$(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
		if(selText.trim() == "All Vendors"){
			if(oldVendor != "Edit Vendor") {
				$(this).parents('.btn-group').find('ul').append('<li><a href="javascript:void(0)" vendor-id="0" class="choose-combo number-vendors-choose"><span class="glyphicon glyphicon-star"></span> '+oldVendor+'</a></li>');		
			}
			// Set All Vendor infor
			table = $('#user-table').DataTable();
			table.row($(this).parent().parent().parent().parent().parent()).data().isChecked = true;
			table.row($(this).parent().parent().parent().parent().parent()).data().userId = $(this).parent().parent().parent().parent().parent().find("td:nth-child(2)").html();
			table.row($(this).parent().parent().parent().parent().parent()).data().vendor = [{'id': '-1'}];
		}else{
			$(this).parents('.btn-group').find('ul').append('<li><a href="javascript:void(0)" vendor-id="all" class="choose-combo all"><span class="glyphicon glyphicon-star"></span> All Vendors</a></li>');
			$(this).parents('.btn-group').find('number-vendors-choose').remove();
		}
		$(this).remove();
		validateToEnableSaveBtn();
	});
	//end custom choose combo list
	// show vendor list of an user
	$( document ).on( "click","#user-table tbody a.show-list-vendor", function() {
		var useridShowPopup = $(this).parents('tr').find('td:eq(1)').text();
		$('#hidden-user').text(useridShowPopup);
		listVendorId = [];
		selectedVendor = [];
		$($(this).parent().parent().find('a')).each(function() {
			if($(this).attr("vendor-id") && $(this).attr("vendor-id") != "all" && $(this).attr("vendor-id") != "0"){
				var vendorsArr = {
						id:$(this).attr("vendor-id"),
						name:$(this).text(),
				}
				listVendorId.push(vendorsArr);
			}
		});
		if(listVendorId.length != 0){
			for (var i = 0; i < listVendorId.length; i++) {
				var itemObj = {
					id: listVendorId[i]
				}
				if (inArrays(selectedVendor, itemObj)) {
					var listRemove = removeInArrays(selectedVendor, itemObj.id);
					var listUpdate = updateItemsShowPopup(listRemove, listVendorId[i].id, listVendorId[i].name);
				}else {
					var listUpdate = updateItemsShowPopup(selectedVendor, listVendorId[i].id, listVendorId[i].name);
				}
				selectedVendor = listUpdate;
			};
		}else{
			selectedVendor = [];
		}
		venTbl.column( '3' ).order( 'desc' );
		$("#btn-save-change").attr("disabled", true);
		$("#search-vendor-name-btn").val("");
		$("#vendor-table thead tr th input").attr('checked', false);
		$('#vendor-modal').modal('show');  
		$('#vendor-table').DataTable().draw();
	});
	// end show vendor list of an user
	return user_tbl;
}
//function for remove row on user table
$( document ).on( "click","#user-table tr td span.action", function() {
	$(".notifications").html("");
	$("#save-user-btn, #save-user-icon").attr("disabled", true);
  	$(this).parents('tr').find('td').addClass('highlight');
	var useridDel = $(this).parents('tr').find('td:eq(1)').text();
	var e = $(this).parents('tr');
	var mess = "";
	if($(this).parents('tr').find('td:eq(1)').find("input").attr("name") == "inputUserId"){
		tbl.row($(e)).remove().draw();		
	}else{
		// ajax delete row by userID
		tbl.row($(e)).data().flag = 1;
		$('#confirm-dialog').modal('show');		
	}
	$("#add-user-btn, #add-user-icon").attr("disabled", false);	
	var saveChecked = true;
	$($("#user-table tr").find('td:nth-child(3)')).each(function() {
		if($(this).html() == ""){
			saveChecked = false;
		}
	});
	dataTable = $('#user-table').DataTable().rows().data();
	if(saveChecked == true){				
		for(i=0; i<dataTable.length; i++) {
			var userId = dataTable[i].userId;
			var check = "";
			if($("#vendor-menu-" + userId).parent().find(".number-vendors").length > 0) {
				check = $("#vendor-menu-" + userId).parent().find(".number-vendors").html();
			}
			if(dataTable[i].isChecked && check.indexOf("Edit Vendor List") == -1){
				activeSaveButton();
				return false;
			} else {
				$("#save-user-btn, #save-user-icon").attr("disabled", true);
			}
		}
	} else {
		$("#save-user-btn, #save-user-icon").attr("disabled", true);
	}
	for(i=0; i<dataTable.length; i++) {
		var userId = dataTable[i].userId;
		check = $("#vendor-menu-" + userId).parent().find(".number-vendors").html();
		if(check.indexOf("Edit Vendor List") > -1){
			$("#add-user-btn, #add-user-icon").attr("disabled", true);
			return false;
		}
	}
});
/* Confirm delete user that exist */
$("#yes-select").click(function() {	
	$("#save-user-btn, #save-user-icon").attr("disabled", true);
	dataTable = $('#user-table').DataTable().rows().data();
	if(dataTable.length > 1){
		for(i=0; i<dataTable.length; i++) {
			if(dataTable[i].flag == 1){
				selected_row = i;
				break;
			}
		}
	} else {
		if(dataTable[0].flag == 1){
			selected_row = 0;
		}
	}
	$.ajax({
        type: "POST",
        dataType: "json",
        beforeSend: function(jqXHR, settings) {
        	$.blockUI({
                message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
                fadeIn: 100,
                onUnblock: function() {
                }
            });
        },
        data: {userId: dataTable[selected_row].userId},
        url: appName + "/user/delete-user",
        success: function(data) {
			tbl.row(dataTable.length - 1 - selected_row).remove().draw();
			var saveChecked = true;
			$($("#user-table tr").find('td:nth-child(3)')).each(function() {
				if($(this).html() == ""){
					saveChecked = false;
				}
			});			
			if(saveChecked == true){
				newtbl = $('#user-table').DataTable().rows().data();
				for(i=0; i<newtbl.length; i++) {
					if(newtbl[i].isChecked){
						activeSaveButton();
					} else {
						$("#save-user-btn, #save-user-icon").attr("disabled", true);
					}
				}
			} else {
				$("#save-user-btn, #save-user-icon").attr("disabled", true);
			}
    		var mess = ' User Id '+ dataTable[selected_row].userId +'</b> has been deleted successfully.';
        	showNotify(mess, 'success');
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	var mess = ' Failed to delete user. Please try again later.';
        	showNotify(mess, 'danger');				        	
        }
    });
});	
//end function for remove row on user table
//initiate vendor table
function initVendorDataTable(){
	var vendor_tbl = $('#vendor-table').DataTable( {
		"columnDefs": [ 
		{
			"orderable": false,
			"width": "5%",
	    	"render": function ( data, type, row ) {
		       	return '<input type="checkbox" class="cursor-pointer checkbox-class" value=""/>';
		      },
	      "targets": 0
	    },
	    {	      
	      "width": "25%",
	      "targets": 1
	    },
	    {	      
	      "width": "70%",
	      "targets": 2
	    },
	    {	    	
	      "width": "0%",
	      "targets": 3,
	      "visible": false	
	    }],
	  "processing": false,
	  "serverSide": true,	    
		"order": [[ 3, "desc" ]],
		"aLengthMenu": [
		    [10, 25, 50],
		    [10, 25, 50]
		],
    	"sDom": "tripl",
    	"pagingType": "full_numbers",
    	"stateSave": false,
	    "ajax": {
	        "url": appName + '/user/vendors',
	        "type": 'GET',
	        "data": function ( d ) {
	        	d.userId = $('#hidden-user').text();
	        	d.vendorId = $('#search-vendor-name-btn').val();
	        	d.vendorName = $('#search-vendor-name-btn').val();
	        }
	    },
	    "columns": [
				{ "data": "vendorId" },
				{ "data": "vendorId"},
				{ "data": "vendorName" },
				{ "data": "isSelected" }    
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		  },
        "fnDrawCallback": function( oSettings ) {
			selected = selectedVendor;
			// check all vendor
			if($("#vendor-table th input:checkbox:checked").length > 0){
		 	   $('#vendor-table tbody tr td input:checkbox').prop('checked',true);
		 	   $("#btn-save-change").attr("disabled", false);
		    } else {
		 	   $('#vendor-table tbody tr td input:checkbox').prop('checked',false);
		 	   $('#vendor-table tbody tr').removeClass('success');
		    };
			$(".checkbox-class").each(function () {
				var selectorTr = $(this).parent().parent(); 
				var itemObj = {
		    		id: $(selectorTr).attr("id")
		    	}
	    		if (inArrays(selected, itemObj)) {
	    			var dataFill = getItemInArraysByKey(selected, itemObj);
	    			fillData($(selectorTr), dataFill);
	    			$(selectorTr).addClass('success');
	    			if ($(selectorTr).hasClass('success')) {
		        		$(selectorTr).find("td:eq(0) > input:checkbox").prop('checked', true);
					} else {
						$(selectorTr).find("td:eq(0) > input:checkbox").prop('checked', false);
					}
	    		}
			});
		},
      "rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-center');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-left');
		   // check all vendor
		   if($("#vendor-table th input:checkbox:checked").length > 0){
	    	   $('#vendor-table tbody tr td input:checkbox').prop('checked',true);
	    	   $('#vendor-table tbody tr').addClass('success');
	    	   $("#btn-save-change").attr("disabled", false);
	       } else {
	    	   $('#vendor-table tbody tr td input:checkbox').prop('checked',false);
	    	   $('#vendor-table tbody tr').removeClass('success');
	       };
           //vendors table
			$("#vendor-table th input:checkbox").change(function() {
				$("#btn-save-change").attr("disabled", false);
			    if(this.checked) {
		        	$('td input:checkbox').prop('checked',this.checked);
		        	$('#vendor-table tbody tr').addClass('success');
		        	$('#vendor-table tbody tr').each(function(){
		        		var itemObj = {
								id: $(this).attr('id')
						}
						if (inArrays(selectedVendor, itemObj)) {
							var listRemove = removeInArrays(selectedVendor, itemObj.id);
							var listUpdate = updateItems(listRemove, $(this));
						}else {
							var listUpdate = updateItems(selectedVendor, $(this));
						}
						selectedVendor = listUpdate;
		        	});
		    	}else{
		    		$('td input:checkbox').prop('checked',false);
		    		$('#vendor-table tr').removeClass('success');
		    		$('#vendor-table tbody tr').each(function(){
		    			var itemObj = {
								id: $(this).attr('id')
						}
		    			var listRemove = removeInArrays(selectedVendor, itemObj.id);
						selectedVendor = listRemove;
		    		});		    		
		    	}
			});			
       }
	});
	//check change
	$('#vendor-table tbody').on('change', 'input[type="checkbox"]', function () {
    	var selectorTr = $(this).parent().parent();
		if (!$(selectorTr).find('td').hasClass('dataTables_empty')) {
	    	$(selectorTr).toggleClass('success');
	    	if ($(selectorTr).hasClass('success')) {
	    		$(selectorTr).find("td:eq(0) > input:checkbox").prop('checked', true);
			} else {
				$(selectorTr).find("td:eq(0) > input:checkbox").prop('checked', false);
			}
		}
		var itemObj = {
				id: $(selectorTr).attr('id')
		}
		var userId = $('#hidden-user').text();
		var vendorName = $(selectorTr).find('td:eq(2)').text();
		if(this.checked) {
			if (inArrays(selectedVendor, itemObj)) {
				var listRemove = removeInArrays(selectedVendor, itemObj.id);
				var listUpdate = updateItems(listRemove, selectorTr);
			}else {
				var listUpdate = updateItems(selectedVendor, selectorTr);
			}
			selectedVendor = listUpdate;			
			var checked = false;
			$('#vendor-table tbody tr td input[type="checkbox"]').each(function(){
				if(this.checked){
					checked = true;
					return false;
				}
			});
			if(checked == true){
				$("#btn-save-change").attr("disabled", false);
			}
		}else{
			var listRemove = removeInArrays(selectedVendor, itemObj.id);
			selectedVendor = listRemove;			
			var checked = false;
			$('#vendor-table tbody tr td input[type="checkbox"]').each(function(){
				if(this.checked){
					checked = true;
					return false;
				}
			});
			if(checked == true){
				$("#btn-save-change").attr("disabled", false);
			}else{
				$("#btn-save-change").attr("disabled", true);
			}
		}
	});
	//end check change vendor table
	//click save btn on pop up, update data on user table
	$("#btn-save-change").click(function() {
		var userId = $('#hidden-user').text();
		var html = '<li class="divider"></li>'+
					'<li><a href="#" vendor-id="all" class="choose-combo"><span class="glyphicon glyphicon-star"></span> All Vendors</a></li>'+
					'<li><a href="#" class="show-list-vendor"><span class="glyphicon glyphicon-star"></span> Edit Vendor List</a></li>';		
		$('#user-table>tbody>tr').each(function() {
		  var test = $.trim($(this).find('td:eq(1)').text());
		  if(test == $.trim(userId)){
		  	$(this).find('td:eq(4) .btn-group ul li').remove();
		  	$(this).find('td:eq(4) .btn-group ul').prepend(html);
		  	return false;
		  }
		});
		for (var i = 0; i < selectedVendor.length; i++) {
			$('#user-table>tbody>tr').each(function() {
			  var test = $.trim($(this).find('td:eq(1)').text());
			  if(test == $.trim(userId)){
			  	$(this).toggleClass('success');
			  	$(this).find('td').addClass('highlight');
			  	$(this).find('td:eq(4) .btn-group .number-vendors').html(selectedVendor.length + " Vendors" + '<span class="caret"></span>');
			  	$(this).find('td:eq(4) .btn-group .number-vendors').attr("vendor-id", "");
			  	$(this).find('td:eq(4) .btn-group .number-vendors-choose').html('<span class="glyphicon glyphicon-star"></span> ' + selectedVendor.length + " Vendors");
			  	$(this).find('td:eq(4) .btn-group ul').prepend('<li class="vendor-item"><a vendor-id="' + selectedVendor[i].id + '"><span class="text-ellipsis" title="'+selectedVendor[i].name+'">' + selectedVendor[i].id + ' - '+selectedVendor[i].name+'</span><span class="del-icon" ></span></a></li>');
				tbl.row($(this)).data().isChecked = true;
				tbl.row($(this)).data().vendor = selectedVendor;
				return false;
			  }
			});
		};
		if($('#user-table>tbody>tr:first-child>td:nth-child(3)').html() != "") {
			activeSaveButton();
		}
	});
	//end click save pop up
	return vendor_tbl;
}
function updateItems(items, selectorTr) {
	var row = {
			id:$(selectorTr).attr('id'),
			name:$(selectorTr).find("td:eq(2)").text(),
			select:$(selectorTr).find("td:eq(0)").find("input").prop('disabled'),
	}
	if (row.id !== "") {
		items.push(row);	
	}
	return items;
}
function getItemInArraysByKey (itemsTmp, itemObj) {
	return _.where(itemsTmp, itemObj); 
}
function fillData (selectorTr, itemObj) {
	$(selectorTr).find("td:eq(0)").find('input').prop("disabled", itemObj[0].select);
}
function inArrays (itemsTmp, itemObj) {
	return _.size(_.where(itemsTmp, itemObj)) === 0 ? false : true; 
}
function removeInArrays(itemsTmp, id) {
	var val = _.filter(itemsTmp, function(item){ return item.id !== id });
	return val;
}
function updateItemsShowPopup(items, id, name) {
	var row = {
			id:id,
			name:name,
			select:false,
	}
	if (row.id !== "") {
		items.push(row);	
	}
	return items;
}
/* @author Khoi.Le */
function replaceContent(el){
	$('#user-table>tbody>tr').find('td:nth-child(' + el + ')').each(function() {
		$(this).html($(this).find("input").val());
	});
}
/* @author Khoi.Le */
function defaultButton(){
	$("#add-user-btn, #add-user-icon").attr("disabled", false);
	$("#save-user-btn, #save-user-icon").attr("disabled", true);
}
/* @author Khoi.Le */
function disableButton(){
	$("#add-user-btn, #add-user-icon").attr("disabled", true);
	$("#save-user-btn, #save-user-icon").attr("disabled", true);
}
/* @author Khoi.Le */
function activeSaveButton(){
	$("#save-user-btn, #save-user-icon").attr("disabled", false);
}
/* @author Khoi.Le */
function validateToEnableSaveBtn(){
	dataTable = $('#user-table').DataTable().rows().data();
	var canSave = true;
	for(i=0; i<dataTable.length; i++) {
		if(dataTable[i].userId.indexOf("<div") > -1) {
			canSave = false;
			//return false;
		}
	}
	if (canSave == true) {activeSaveButton()};
}
/* @author Khoi.Le */
function checkInputEmplty() {
	$("#user-table tr td input[name=inputUserId]").each(function() {
		if($(this).val() === "") {
			$(this).parent().addClass("has-success");
			$(this).parent().addClass("has-error");
		} else {
			$(this).parent().removeClass("has-error");
		}
	});
	$("#user-table tr td:nth-child(3)").each(function() {
		if($(this).html() === "") {
			$(this).parent().find("td:nth-child(2) div").removeClass("has-success");
			$(this).parent().find("td:nth-child(2) div").addClass("has-error");
		} else {
			$(this).parent().find("td:nth-child(2) div").removeClass("has-error");
		}
	});
}
//------------------------------------
function autoComplete(idInput){	
	var bufdata = [];
	$( "#" + idInput ).autocomplete({
		search: function(event, ui) {
            $(this).addClass("wait");
            $(this).parent().find("span").removeClass("caret");
        },
        open: function(event, ui) {
            $(this).removeClass("wait");
            $(this).parent().find("span").addClass("caret");
        },
		source:function (request, response) {
	   		var inputRaw = idInput.split("-");
			var nameProperty = inputRaw[0];
			//remove loading screen
			$(document).unbind('ajaxSend');			
            $.ajax({
                // type: "POST",
                contentType: "application/json; charset=utf-8",
                url: appName + '/user/get-user',
                data: {"userId" : $("#" + inputId).val(), "autoType":nameProperty},
                dataType: "json",
                success: function (data) {
                	dataAutoCompleteInput = data.data;
                	for (var i in dataAutoCompleteInput) {
						dataAutoCompleteInputValue.push(dataAutoCompleteInput[i].label);
						dataAutoCompleteInputLabel.push(dataAutoCompleteInput[i].value);
						bufdata.push(dataAutoCompleteInput[i].value);
					}
                    response(dataAutoCompleteInput);
                    if(dataAutoCompleteInput.length == 0){
                        $("#" + idInput).removeClass("wait");
                        $("#" + idInput).parent().find("span").addClass("caret");						
                    }
					$("#" + idInput).parent().parent().parent().find("input[name='setChecked']").prop("checked", true);
					$("#" + idInput).parent().parent().parent().find("td").addClass("highlight");
                    //re-attach loading screen
                    $(document).bind("ajaxSend", function(e, xhr, settings){
                    	$.blockUI({
                            message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
                            fadeIn: 100,
                            onUnblock: function() {
                            }
                        });                    	
                    }).bind("ajaxStop", function(){
                    	$.unblockUI();
                    }).bind('ajaxError', handleAjaxError);
                },
                error: function (result) {
                    alert("Error......");
                }
            });
        },
		minLength: 3,
		select: function(event, ui) {
			event.preventDefault();
			$("#"+idInput).val(ui.item.value);
			if ($.inArray($("#"+idInput).val(), dataAutoCompleteInputValue) !== -1 
	            || $.inArray($("#"+idInput).val(), dataAutoCompleteInputLabel) !== -1) {
	    		$( "#" + idInput ).attr( idInput +"id", ui.item.value );
			} else {
				$( "#" + idInput ).attr( idInput +"id", "");
			}
			$("#"+idInput).closest("td").next().text(ui.item.name).next().text(ui.item.roles);
			return false;
		},
		focus: function(event, ui) {			
			$("#"+idInput).val(ui.item.value);
			if ($.inArray($("#"+idInput).val(), dataAutoCompleteInputValue) !== -1 
	            || $.inArray($("#"+idInput).val(), dataAutoCompleteInputLabel) !== -1) {  	   	
	    	   	$('.ui-menu-item').attr( "title", ui.item.label);
	    		$( "#" + idInput ).attr( idInput +"id", ui.item.value );
			} else {
				$( "#" + idInput ).attr( idInput +"id", "");
			}
			return false;
		}
	}).focusout(function() {
		$("#" + idInput).removeClass("wait");
		if(jQuery.inArray($("#" + idInput).val(),bufdata) == -1) {
			$("#" + idInput).parent().parent().parent().find("td:nth-child(3)").html("");
			$("#" + idInput).parent().parent().parent().find("td:nth-child(4)").html("");
			showNotify('Please input an User Id.', 'danger');
			tbl.row($("#" + idInput).parent().parent().parent()).data().isChecked = false;	
			disableButton();			
		} else {
			tbl.row($("#" + idInput).parent().parent().parent()).data().isChecked = true;
			tbl.row($("#" + idInput).parent().parent().parent()).data().vendor = [{'id': '-1'}];
			tbl.row($("#" + idInput).parent().parent().parent()).data().userId = $("#" + idInput).val();
			$("#" + idInput).parent().parent().parent().find("td").addClass("highlight");
			if(tbl.row($("#" + idInput).parent().parent().parent()).data().userId == "") {
				showNotify('User does not exist. Please input a correct User Id.', 'danger');
				updateValidationStatus();
				$("#save-user-btn, #save-user-icon").attr("disabled", true);
			} else {
				var bufInut = [];
				var check = false;
				$('#user-table>tbody>tr>td:nth-child(2) input').each(function() {					
					if($.inArray($(this).val(), bufInut) > -1){
						check = true;
						return false;
					}
					bufInut.push($(this).val());
				});
				$('#user-table>tbody>tr>td:nth-child(2)').each(function() {					
					entertext = $(this).text();
					if(($("#" + idInput).val() != "" && $("#" + idInput).val() == entertext) || check === true){
						showNotify('User already existed. Please input another User Id.', 'danger');
						updateValidationStatus();
						$("#" + idInput).parent().parent().parent().find("td:nth-child(3)").html("");
						$("#" + idInput).parent().parent().parent().find("td:nth-child(4)").html("");
						disableButton();
						return false;
					} else if ($("#" + idInput).parent().parent().parent().parent().find("td:nth-child(3)").html() != "") {
						$("#save-user-btn, #save-user-icon").attr("disabled", false);
						$("#add-user-btn, #add-user-icon").attr("disabled", false);
					}
				});
			}
		}
		checkInputEmplty();
    });
}