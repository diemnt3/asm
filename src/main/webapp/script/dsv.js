/************************************************************************************************************
 * CONSTANTS VALUE
 ************************************************************************************************************/
var requestURL ;
var CONST = {
    YES: "Y",
    NO: "N",
    EMPTY_STRING:""
};
/************************************************************************************************************
 * END CONSTANTS VALUE
 ************************************************************************************************************/

/************************************************************************************************************
 * Default Events and  settings
***********************************************************************************************************/
addNavigationMessage(true);

/************************************************************************************************************
 * Public setting
 ************************************************************************************************************/
$(document).bind("ajaxSend", function(e, xhr, settings){
  $.blockUI({
        message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
        fadeIn: 100,
        onUnblock: function() {
        }
    });

  
}).bind("ajaxStop", function(){
  $.unblockUI();
}).bind('ajaxError', handleAjaxError);

/************************************************************************************************************
 * Public value
 ************************************************************************************************************/
var appName = contextPath;
var items = [];
var Common = new Common();
var tabActive = "all";
var internalServerErrorMessage = "";
var isChangeDefault = true;
/**
 * Handle ajax error.
 * @param jqXHR
 * @param errorStatus errorStatus
 * @param exception exception
 */
function handleAjaxError(jqXHR, errorStatus, exception, url){
  var errorStr = '';  
  var reqUrl = exception.url + '';  

    if (errorStatus.status == 102) {
      errorStr = getMessage(languageCode, 'connection_refused');
      
    } else if (errorStatus.status == 0) {
      errorStr = 'Not connect.\nVerify Network.';
      window.location.href=document.URL;
      
    }else if (errorStatus.status == 302) {
      
    } else if (errorStatus.status == 404) {
      errorStr = 'Requested page not found. [404]';
      
    } else if (errorStatus.status == 500) {
      
      internalServerErrorMessage = errorStatus.responseText;
      internalServerErrorMessage = $(internalServerErrorMessage).find("#message").text();
      errorStr = 'Internal Server Error [500].';
      window.location.href="internal-server-error?message=" + internalServerErrorMessage;
      
    } else if (errorStatus.status == 901) {      
      if(requestURL=== undefined){          
          showPopupSessionTimeout();
      } else {
          if((requestURL!== undefined || requestURL!== '') && requestURL.indexOf('logout') > -1){
            isChangeDefault = false;
            window.location.href = appName;
          } else {              
              showPopupSessionTimeout();
          }
           
      }
       
    } else if (exception === 'parsererror') {
      errorStr = 'Requested JSON parse failed.';
      
    } else if (exception === 'timeout') {
      errorStr = 'Request Time out.';
      
    } else if (exception === 'abort') {
      errorStr = 'Ajax request aborted.';
      
    } else {
      errorStr = 'Uncaught Error.\n' + errorStatus.statusText;
      //window.location.href = appName;
    }

    if(errorStatus.status != 901){
      //alert(errorStr);
    }
    
    return false;
}
function showPopupSessionTimeout(){
  isChangeDefault = false;
  bootbox.dialog({
        title : "Warning",
        message: "System is logged out as it is inactive for long time. Please sign in again",
        buttons: {
          main: {
              label: "Ok",
              className: "btn-primary",
              callback: function() {            
                  window.location.href = appName;
              }
            }                          
          },
        onEscape: function() {
           window.location.href = appName;
        }   
      });
}
/* Show/Hidden Block e*/
function displayBlockCtr(e){
  $('#' + e).toggle("slow");
}

function formatCurrency () {
  $(".currency-format").each(function (index, value) {
    $(value).text($.number($(value).text(), 2));
  })
//  $(".currency-format-input").each(function (index, value) {
//    $(value).val($.number($(value).val(), 2));
//  });
}

function formatCurrencyInput () {
//  debugger;
  $(".currency-format-input").each(function (index, value) {
    $(value).val($.number($(value).val(), 2));
  });
}

//Replaces all instances of the given substring.
String.prototype.replaceAll = function(
    strTarget, // The substring you want to replace
    strSubString // The string you want to replace in.
) {
    var strText = this;
    var intIndexOfMatch = strText.indexOf(strTarget);
    // Keep looping while an instance of the target string
    // still exists in the string.
    while(intIndexOfMatch != -1) {
        // Relace out the current instance.
        strText = strText.replace(strTarget, strSubString)
        // Get the index of any next matching substring.
        intIndexOfMatch = strText.indexOf(strTarget);
    }
    // Return the updated string with ALL the target strings
    // replaced out with the new substring.
    return(strText);
}

/**
 * Bootstrap notify.
 * [option] @param fadeOut_enabled: is auto hide ?
 * [option] @param closable: can close me ?
 * [option] @param showIcon: auto show icon base on type ?
 * @param content: HTML string to show
 * @param type: Alert style, omit alert- from style name, ex: 'success'
 * @author ANNGUYEN
 */
function showNotify(content, type, fadeOut_enabled, closable, showIcon) {
  // auto fadeOut
  fadeOut_enabled = typeof fadeOut_enabled !== 'undefined' ? fadeOut_enabled : true;
  closable = typeof closable !== 'undefined' ? closable : true;
  showIcon = false;//typeof showIcon !== 'undefined' ? showIcon : true;
  
  if (showIcon) {
    var iconName = 'ok';
    
    if (type == "info") {
      iconName = 'warning-sign';
    } else if (type == "warning") {
      iconName = 'warning-sign';
    } else if (type == "danger") {
      iconName = 'warning-sign';
    }
    
    var iconHTML = '<span class="glyphicon glyphicon-' + iconName + '"></span>';
    content = iconHTML + content;
  }
  
  $('.notifications.top-right').notify({
    message: {html: content },
    type: type, 
    closable: closable,
    fadeOut: { enabled: fadeOut_enabled, delay: 3000 }
  }).show(); // for the ones that aren't closable and don't fade out there is a .close() function.
}


/**
 * click to dropdown auto list in autoComplete.
 * @author GiauLe
 */

function clickDropAutoComplete(){
  $('.wrap-caret').click(function() {
     inputId = $(this).parent().find('input').attr("id");
     $('#'+inputId).val("");
     $('#'+inputId).trigger("focus"); 
     $("#"+inputId).autocomplete( "search", "   " );
  });
}

///**
// * Accept input [A-z][0-9].
// * @author ANNGUYEN
// */
//$('.number-input').keydown(function(event){
////  acceptDecimalNumber(this, event); 
////  return;
//  if (!(!event.shiftKey //Disallow: any Shift+digit combination
//            && !(event.keyCode < 48 || event.keyCode > 57) //Disallow: everything but digits
//            || !(event.keyCode < 96 || event.keyCode > 105) //Allow: numeric pad digits
//            || event.keyCode == 46 // Allow: delete
//            || event.keyCode == 8  // Allow: backspace
//            || event.keyCode == 9  // Allow: tab
//            || event.keyCode == 27 // Allow: escape
//            || event.keyCode == 190 // Allow: .
//            || event.keyCode == 110 // Allow: . right
//            || (event.keyCode == 65 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+A
//            || (event.keyCode == 67 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+C
//            //Uncommenting the next line allows Ctrl+V usage, but requires additional code from you to disallow pasting non-numeric symbols
//            || (event.keyCode == 86 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+Vpasting 
//            || (event.keyCode == 88 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+X
//            || (event.keyCode >= 35 && event.keyCode <= 39) // Allow: Home, End
//            )) {
//        event.preventDefault();
//    }
//  
//  var invoiceAmountValueInput = $(this).val();
//  var dotIndex = invoiceAmountValueInput.indexOf('.');
//  var maxLenAfterDot = 2;
//  var nLeft = maxLenAfterDot - (invoiceAmountValueInput.length - 1 - dotIndex);
//  var charCode = (event.which) ? event.which : event.keyCode;
//  if (dotIndex >= 0 && event.target.selectionStart > dotIndex &&
//      (!(charCode < 48 || charCode > 57) || !(charCode < 96 || charCode > 105) ) && nLeft == 0) {
//    event.preventDefault();
//  }
//  var isDotKey = (charCode == 190 || charCode == 110);
//  if(isDotKey && 
//    ( !invoiceAmountValueInput || dotIndex >= 0) ) {
//    event.preventDefault();
//  }
//});



/**
 * common auto complete for textboxes to search.
 * @author GiauLe
 */
selectedLabelBefore = "";
selectedIdBefore = "";
function autoCompleteInit(idInput, url){
  var dataAutoCompleteInput = [];
    dataAutoCompleteInputValue= [];
    dataAutoCompleteInputLabel= [];
  
    $( "#" + idInput ).autocomplete({
        search: function(event, ui) {
            $(this).addClass("wait");
            $(this).parent().find("span").removeClass("caret");
        },
        open: function(event, ui) {
            $(this).removeClass("wait");
            $(this).parent().find("span").addClass("caret");
        },

       source:function (request, response) {
            var inputRaw = idInput.split("-");
            var nameProperty = inputRaw[0];
            //remove loading screen
            $(document).unbind('ajaxSend');
            $.ajax({
              type: "POST",
                contentType: "application/json; charset=utf-8",
                url: appName + url,
                data: {"name" : $( "#" + idInput ).val()},
                dataType: "json",
                success: function (data) {
                    dataAutoCompleteInput = data.data;
                    for (var i in dataAutoCompleteInput) {
                        dataAutoCompleteInputValue.push(dataAutoCompleteInput[i].label);
                        dataAutoCompleteInputLabel.push(dataAutoCompleteInput[i].value);
                    }

                    response(dataAutoCompleteInput);

                    if(dataAutoCompleteInput.length == 0){
                        $("#" + idInput).removeClass("wait");
                        $("#" + idInput).parent().find("span").addClass("caret");
                    }
                    //re-attach loading screen
                    $(document).bind("ajaxSend", function(e, xhr, settings){
                        $.blockUI({
                            message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
                            fadeIn: 100,
                            onUnblock: function() {
                            }
                        });

                        
                    }).bind("ajaxStop", function(){
                        $.unblockUI();
                    }).bind('ajaxError', handleAjaxError);

                },
                error: function (result) {
                    //alert("Error......");
                }
            });
        },
       minLength: 1,
       select: function(event, ui) {
         //console.log("select");
           $("#"+idInput).val(ui.item.label);
           if ($.inArray($("#"+idInput).val(), dataAutoCompleteInputValue) !== -1 
                   || $.inArray($("#"+idInput).val(), dataAutoCompleteInputLabel) !== -1) {
               
                   $( "#" + idInput ).attr( idInput +"id", ui.item.value );
           } else {
             $( "#" + idInput ).attr( idInput +"id", "");
           }
           selectedLabelBefore = ui.item.label;
           selectedIdBefore = ui.item.value;
           //console.log("enter");
           return false;
       },
       focus: function(event, ui) {
           $("#"+idInput).val(ui.item.label);

           if ($.inArray($("#"+idInput).val(), dataAutoCompleteInputValue) !== -1 
                   || $.inArray($("#"+idInput).val(), dataAutoCompleteInputLabel) !== -1) {
                
                $('.ui-menu-item').attr( "title", ui.item.label);

                $( "#" + idInput ).attr( idInput +"id", ui.item.value );
           } else {
             $( "#" + idInput ).attr( idInput +"id", "");
           }
           selectedLabelBefore = ui.item.label;
           selectedIdBefore = ui.item.value;
              
           return false;
       }
    }).focusout(function() {
        if(!$( "#" + idInput ).attr(idInput +"id")){
            $("#"+idInput).val(selectedLabelBefore);
            $( "#" + idInput ).attr( idInput +"id", selectedIdBefore);
        }
        
    }).keyup(function (e) {
        //$(this).attr(idInput +"id", "");
    }).focus();
}

//Format string
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}
$(window).on('beforeunload', function(e) {
  if(isChanges() && isChangeDefault) {
     return "Any changes you make to this page will be permanently deleted.";
  }
});
$(".a-menu").click(function(e) {
  var url = $(this).attr('href');  
  requestURL = url;
  e.preventDefault();

      //$(document).unbind('ajaxSend');
    
    if((requestURL!== undefined || requestURL!== '') && requestURL.indexOf('#') >-1) {
      
    } else {
        if(isChanges()) {
          bootbox.dialog({
              title : "Confirmation",
              message: "Are you sure you want to leave this page?<br/>If so, any changes you make to this page will be permanently deleted.",
              buttons: {
                main: {
                    label: "Leave page",
                    className: "btn-primary",
                    callback: function() {     
                        isChangeDefault = false;      
                        
                        $.ajax({
                             type: 'GET',
                             url: appName + "/checkSessionTimeOut",
                             data:{
                             }
                        }).success(function(data) {
                          var obj = jQuery.parseJSON(data);
                          if (obj.session) {        
                             window.location.href=url;
                          }
                        }).done(function() {
                        }).fail(function(jqXHR, errorStatus, exception) {
                        }).always(function() {
                        });
                    }
                  },  
                  cancel: {
                    label: "Stay on page",
                    className: "btn-default"
                  }                           
                },
                onEscape: function() {}  
            });
        } else {        
          $.ajax({
             type: 'GET',
             url: appName + "/checkSessionTimeOut",
             data:{
             }
          }).success(function(data) {
            var obj = jQuery.parseJSON(data);
            if (obj.session) {        
                window.location.href=url;
            }
          }).done(function() {
          }).fail(function(jqXHR, errorStatus, exception) {
          }).always(function() {
          });
        }
    }
});

/**
 * add navigation message.
 */
function addNavigationMessage(bind) {
   //window.onbeforeunload = bind ? getBeforeunloadMessage : null;
//  $(window).bind('beforeunload', function() {
//    debugger;
//    if(isChanges()) {
//      bootbox.confirm("You have not saved your changes. Do you want to load new vendor?", function(result) {
//        if (!result) {
//          e.preventDefault();
//          return false;  
//        }
//      });
//    }
//  });
}

/**
 * change status for navigate message.
 */
function setChangeStatus(changed) {
    isMakeChanges = changed;
}
/**
 * get change status for navigate message.
 */
function isChanges(changed) {
    return isMakeChanges;
}

/**
 * get Beforeunload event Message.
 */
function getBeforeunloadMessage(e) {
    if(isMakeChanges){
        return 'You have not saved your changes.';
    }
}

/**
 * Accept input [A-z][0-9].
 * @author Duyen.le
 */
$('.number-input').keydown(function(event){
//  acceptDecimalNumber(this, event); 
//  return;
    if (!(!event.shiftKey //Disallow: any Shift+digit combination
            && !(event.keyCode < 48 || event.keyCode > 57) //Disallow: everything but digits
            || !(event.keyCode < 96 || event.keyCode > 105) //Allow: numeric pad digits
            || event.keyCode == 46 // Allow: delete
            || event.keyCode == 8  // Allow: backspace
            || event.keyCode == 9  // Allow: tab
            || event.keyCode == 27 // Allow: escape
            || event.keyCode == 190 // Allow: .
            || event.keyCode == 110 // Allow: . right
            || (event.keyCode == 65 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+A
            || (event.keyCode == 67 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+C
            //Uncommenting the next line allows Ctrl+V usage, but requires additional code from you to disallow pasting non-numeric symbols
            || (event.keyCode == 86 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+Vpasting 
            || (event.keyCode == 88 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+X
            || (event.keyCode >= 35 && event.keyCode <= 39) // Allow: Home, End
            )) {
        event.preventDefault();
    }
    
    var invoiceAmountValueInput = $(this).val();
    var dotIndex = invoiceAmountValueInput.indexOf('.');
    var maxLenAfterDot = 2;
    var nLeft = maxLenAfterDot - (invoiceAmountValueInput.length - 1 - dotIndex);
    var charCode = (event.which) ? event.which : event.keyCode;
    if (dotIndex >= 0 && event.target.selectionStart > dotIndex &&
            (!(charCode < 48 || charCode > 57) || !(charCode < 96 || charCode > 105) ) && nLeft == 0) {
        event.preventDefault();
    }
    var isDotKey = (charCode == 190 || charCode == 110);
    if(isDotKey && 
        ( !invoiceAmountValueInput || dotIndex >= 0) ) {
        event.preventDefault();
    }
});

function truncate(div, maxLength, surfix) {
    var str = div.html();
    if (str.length > maxLength) {
        div.attr("title", div.text());
        str = str.substring(0, maxLength + 1);
        str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
        str = str + surfix;
    }
    return str;
}

function getVendorTabActive() {
  return $("#vendorTabs").find(".active").find("a").attr("id");
}

function getDateTimeSystem(){
    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();
    var hour = d.getHours();
    var h = hour ;
      if (h >= 24) {
          h = hour -24;
          
      }
      if (h == 0) {
          h = 24;
      }
    hour = h;
    var minute = d.getMinutes();
    var second = d.getSeconds();

    var output = ((''+month).length<2 ? '0' : '') + month + '/' +
        ((''+day).length<2 ? '0' : '') + day + '/' +
        d.getFullYear() + ' ' +
        ((''+hour).length<2 ? '0' :'') + hour + ':' +
        ((''+minute).length<2 ? '0' :'') + minute;

    return output;
  }




