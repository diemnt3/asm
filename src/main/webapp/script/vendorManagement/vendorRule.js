var TreeNode = [];

var msgRequiredInput = "This field is required.";

var msgGreaterThanZezo = "The input value should be greater than zero.";
var msgInvalidValue = "Invalid value.";
var msgRange0To100Accept = "Only the value between <br>0 and 100 is accepted.";
var msgRangeMarginAccept = "Only the value between <br>-10000 and 10000 is accepted.";
var msgRange0To100000Accept = "Only the value between <br>0 and 100000 is accepted.";
var msgLoseChange = "You will lose your changes, do you wish to continue?";
var msgSaveConfirm = "Do you want to save the changes?";

var saveChangeArrAfterSave = [];
var saveChangeArrAfterLoad = [];

var saveChangeArrAfterLoadAssign = [];
var saveChangeArrAssignAfterSave = [];

var saveChangeArrAfterLoadAssignForVendor = [];
var saveChangeArrAfterSaveAssignForVendor = [];
var checkFirstLoad = 0;
var currentNode=0;

var flagHaveActivate = false;
var onArr = []; 
var offArr = [];

var inheritRuleArr = [];
var inheritRuleArr2 = [];
var turnAllIsON = false;
var select_node = false;
var offFirstLoadArr = [];

$(document).ready(
    function() {  
        $( "#vendor-input" ).on('change',(function(d,dt) {          
            var txt = $(this).val();
            
        }));
        $("#contentColRight").hide();
        
        /**
         * Init value of Rules [type Handel Checkbox].
         * @author Duyen.le
        */
        $("#turnOnAllRule").iButton({
            labelOff: "OFF",
            labelOn: "ON",
            easing: "swing",
            enableDrag: false,
            resizeHandle: false,
            resizeContainer: false,
            classContainer: "ibutton-container",
            change: function ($input){                                      
                if ($input.is(":checked")) { 
                    $($input).parent().addClass('ibutton-container-on') ;
                    $("#formHie div:not(.ibutton-container-checkmark) > input[name^='chk-']").attr('checked',true);                             
                    $("#formHie div:not(.ibutton-container-checkmark) > input[name^='chk-']").iButton().trigger("change");                   
                    $("#retailStart").trigger('focus');
                }else {
                    $($input).closest('.form-group').find('input').val(""); 
                    $($input).parent().removeClass('ibutton-container-on') ;
                    $("#formHie input[name^='chk-']").attr('checked',false);                                
                    $("#formHie input[name^='chk-']").iButton().trigger("change");  
                }
            },
            init: function($input){                
                if ($input.is(":checked")) {
                    $($input).parent().addClass('ibutton-container-on');                    
                }else {
                    $($input).parent().removeClass('ibutton-container-on');
                }
            },
            click: function(d, data){            
                if (data.is(":checked")) {
                    offArr = [];
                    $("#assortment-save-btn").prop("disabled", false);
                    turnAllIsON = true;
                    if(saveChangeArrAssignAfterSave.length>0){
                        var compareRule = compare2Arr(saveChangeArrAssignAfterSave, getRuleWhenTurnAll(turnAllIsON));                        
                        if(compareRule.length>0){
                            $("#assortment-save-btn").prop('disabled', false);
                            setChangeStatus(true);
                        } else {
                            $("#assortment-save-btn").prop('disabled', true);
                            setChangeStatus(false);
                        }
                    }                   
                    $("#formHie input[name^='chk-']").each(function(d,dt){
                        if(!$(dt).parent().hasClass('ibutton-container-checkmark')){   
                            var id =   $(dt).attr('name').split('-')[1];
                            if($.inArray(id, onArr) === -1){
                                onArr.push(id);  
                            }                       
                        }
                    });

                    if($("#vendorRules").hasClass('VendorIsClicked')){                     
                        $("#assortment-save-btn").prop('disabled', false);
                        setChangeStatus(true);                        
                    }           
                } else {                  
                    onArr = [];
                    turnAllIsON = false;                    
                    if($("#vendorRules").hasClass('VendorIsClicked')){
                        if(saveChangeArrAssignAfterSave.length > 0){  
                            var changedArr = compare2Arr(saveChangeArrAssignAfterSave,getRuleChanges());
                            if(changedArr.length > 0){ 
                                setChangeStatus(true);
                                $("#assortment-save-btn").prop("disabled", false);
                            } else {
                                setChangeStatus(false);
                                $("#assortment-save-btn").prop("disabled", true);
                            }
                        } else {
                            if(saveChangeArrAfterLoadAssignForVendor.length > 0){
                                $("#assortment-save-btn").prop('disabled', false);
                                setChangeStatus(true);
                            } else {
                                $("#assortment-save-btn").prop('disabled', true);
                                setChangeStatus(false);
                            }
                        }
                    } else {
                        if(saveChangeArrAssignAfterSave.length>0){
                            var compareRule = compare2Arr(saveChangeArrAssignAfterSave, getRuleWhenTurnAll(turnAllIsON));                        
                            if(compareRule.length>0){
                                $("#assortment-save-btn").prop('disabled', false);
                                setChangeStatus(true);
                            } else {
                                $("#assortment-save-btn").prop('disabled', true);
                                setChangeStatus(false);
                            }
                        } else {
                            if(inheritRuleArr.length>0){
                                $("#assortment-save-btn").prop("disabled", false);
                                setChangeStatus(true);                    
                            } else {
                                if(saveChangeArrAfterLoadAssign.length> 0 ){
                                    $("#global-save-btn").prop("disabled", false);
                                    setChangeStatus(true); 
                                } else {
                                    $("#global-save-btn").prop("disabled", true);
                                    setChangeStatus(false);
                                }
                            }
                        }
                    }
                }                
            }
        });
        $(".my-checkbox").iButton({
            labelOff: "NO",
            labelOn: "YES",
            easing: "swing",
            enableDrag: false,
            resizeHandle: false,
            resizeContainer: false
        });
        $(".checkboxRule").iButton({
            labelOff: "OFF",
            labelOn: "ON",
            easing: "swing",
            resizeHandle: "auto",
            enableDrag: false,
            resizeContainer: false,
            classContainer: "ibutton-container sub-cb-item",
            change: function ($input){            
                if ($input.is(":checked")) {// CHECKED                  
                    $($input).parent().addClass('ibutton-container-on').removeClass("ibutton-container-checkmark");
                    // Enable [Handle checkbox YES/NO] when ON Rule
                    if (typeof $($input).attr("data-left-check") !== 'undefined') {
                        var index_check = $($input).attr("data-left-check");                        
                        var enableNext = $("input[data-right-check="+index_check+"]");  
                            
                        enableNext.iButton('disable', false);
                        enableNext.prop('disabled', false);
                    }
                    //
                    if($.trim($input.attr('name'))==='chk-2'){
                        $("#retailStart").focus();                                          
                    } else if($.trim($input.attr('name'))==='chk-3'){
                        $("#maginStart").focus();                       
                    } else if($.trim($input.attr('id'))==='chk-10'){
                        $("#rule10").focus();                       
                    }

                }else {     // UNCHECKED    
                    if($($input).attr('name')=='chk-2'){
                        clearTooltipForValidationDiv("retailStart");
                        clearTooltipForValidationDiv("retailEnd");
                    } 
                    if($($input).attr('name')=='chk-3'){
                        clearTooltipForValidationDiv("maginStart");
                        clearTooltipForValidationDiv("maginEnd");
                    }
                    if($($input).attr('id')==='chk-10'){
                        clearTooltipForValidationDiv("rule10");
                    }
                    $($input).closest('.form-group').find('input').val("");
                    $($input).parent().removeClass('ibutton-container-on').removeClass("ibutton-container-checkmark");
                    // Disable [Handle checkbox YES/NO] when OFF Rule
                    if (typeof $($input).attr("data-left-check") !== 'undefined') {
                        var index_check = $($input).attr("data-left-check");                        
                        var enableNext = $("input[data-right-check="+index_check+"]");  
                            
                        enableNext.iButton('disable', true);    
                        enableNext.iButton('toggle', false);
                        enableNext.prop('disabled', true);                                  
                    }
                    //
                }
                $($input).next().removeClass("ibutton-handle-checkmark").addClass("ibutton-handle");
            },
            click: function(d, data){
                var changesRuleArr = [];    
                var totalCheckbox = $(".sub-cb-item input[type=checkbox]").length;
                var totalChecked = $(".sub-cb-item input[type=checkbox]:checked").length;
                var totalUncheck = totalCheckbox - totalChecked;
                // #turnOnAllRule
                if (totalChecked == totalCheckbox){
                    $("#turnOnAllRule").iButton("toggle", true);
                } else if (totalChecked == 0){
                    $("#turnOnAllRule").iButton("toggle", false);
                } else{
                    var parentSelector = $("#turnOnAllRule").parent();
                    $("#turnOnAllRule").attr('checked', false);
                    parentSelector.removeClass('ibutton-container-on');
                    parentSelector.find(".ibutton-handle").attr('style', 'left: 0px;');
                    parentSelector.find(".ibutton-label-off span").attr('style', 'margin-right: 0px;');
                    parentSelector.find(".ibutton-label-on span").attr('style', 'margin-left: -31px;');
                }
                var id = data.attr('name').split('chk-')[1];
                if(data.is(":checked")){                    
                    if($.inArray(onArr, id) === -1){
                        onArr.push(id);
                    }                                                                              
                    
                    if(offArr.length>0){
                        var idexOfItem = offArr.indexOf(id.toString() );
                        offArr.splice(idexOfItem, 1);
                    }
                } else {                    
                    data.focus();                   
                    offArr.push(id);                
                    var idexOfItem = onArr.indexOf(id.toString());
                    if(!data.parent().hasClass('ibutton-container-checkmark')){
                        onArr.splice(idexOfItem, 1);
                    }
                }                
                checkChangeRule(onArr);
            },
            init: function($input){
                
                if ($input.is(":checked")) {
                    $($input).parent().addClass('ibutton-container-on');                    
                }else {
                    $($input).parent().removeClass('ibutton-container-on');
                }
            }
        });        

        $("#vendorTabs a").click(function(){
            select_node = false;
            var $this = $(this);
            loadurl = $this.attr('href');
            disableRuleTextBoxwhenInit ();
            if ($("#jstreeCommo").hasClass('jstree')) {
                if(isFoundVendor && loadurl=='#assortment-rules'){
                    $("#jstreeCommo").jstree(true).destroy();
                }
            }
            var msgLoseChangeLocal = "Are you sure you want to leave this page? </br> If so, any changes you make to this page will be permanently deleted."
            if(loadurl=='#assortment-rules'){
                $("#pMsg").html("<span class='col-md-12'> Please activate Commodities/Sub-commodities and setup Assortment Rules for <span class='title-style vendorTxt'><b>"+vendor+"</b></span></span>");
                if(isFoundVendor){            
                    load_Commodity_Tree();
                    $("#contentColRight").css('display','none');
                }
                
                $this.tab('show');
            } else {
                    checkRuleChanged();                    
                    if(isChanges()) {                                           
                        bootbox.dialog({
                            title : "Confirmation",
                            message: msgLoseChangeLocal,
                            buttons: {
                                main: {
                                  label: "Leave page",
                                  className: "btn-primary",
                                  callback: function() {                
                                        $("#vendorRules").removeClass('VendorIsClicked');                                        
                                        $this.tab('show');
                                        removeAlertSuccess(0);
                                        checkFirstLoad = 0;
                                        currentNode = 0;
                                        setChangeStatus(false);
                                        saveChangeArrAfterLoadAssign = [];
                                        saveChangeArrAssignAfterSave = [];
                                        inheritRuleArr2 = [];
                                        
                                  }
                                },  
                                cancel: {
                                  label: "Stay on page",
                                  className: "btn-default"
                                }                                               
                            }       
                        });
                    } else {
                        $this.tab('show');
                        removeAlertSuccess(0);
                        checkFirstLoad = 0;
                        currentNode = 0;
                        setChangeStatus(false);
                        saveChangeArrAfterLoadAssign = [];                                               
                        saveChangeArrAssignAfterSave = [];
                    }
            }
            return false;
        });
        $(".btn-search-vendor-header").click(function() {           
            var allowedLoadTreeAsmRule = $("#assortment-rules-a").parent().hasClass('active');            
            if(allowedLoadTreeAsmRule){
                $("#vendorRules").removeClass('VendorIsClicked');
                $("#pMsg").html("<span class='col-md-12'> Please activate Commodities/Sub-commodities and setup Assortment Rules for <span class='title-style vendorTxt'><b>"+vendor+"</b></span></span>");
                //checkRuleChanged();
                load_Commodity_Tree();                
                clearAllRule();
                currentNode = 0;
                checkFirstLoad = 0;
            }
         });
         $('.txt-search-vendor-header').on("keypress", function(e) {               
            var allowedLoadTreeAsmRule = $("#assortment-rules-a").parent().hasClass('active');            
            if(e.keyCode === 13) {
                var vendor = $("#vendor-input option:selected").text();
                $(".vendorTxt").html('<b>'+vendor.toUpperCase()+'</b>');
                $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+'</b>');
                if(allowedLoadTreeAsmRule){
                    //checkRuleChanged();
                    $("#vendorRules").removeClass('VendorIsClicked');
                    $("#pMsg").html("<span class='col-md-12'> Please activate Commodities/Sub-commodities and setup Assortment Rules for <span class='title-style vendorTxt'><b>"+vendor+"</b></span></span>");
                    load_Commodity_Tree();                    
                    clearAllRule();
                    currentNode = 0;
                    checkFirstLoad = 0;
                }
            }
         });       

        $("#vendorRules").click(function(){
            $("#assortment-save-btn").show();
            if($(this).hasClass('VendorIsClicked')){                               
                $("#assortment-save-btn").prop("disabled", false);
                setChangeStatus(true);
            }else{                
                onArr = [];
                offArr = [];
                inheritRuleArr2 = [];
                offFirstLoadArr = [];
                $(this).addClass('VendorIsClicked');                
                $("#assortment-save-btn").prop("disabled", true);
                setChangeStatus(false);
            }
            
            var vendor = $("#vendor-input option:selected").text();           
            select_node = false;
                      
            $("#jstreeCommo").jstree("deselect_all");
            // kiem tra
             $("#contentColRight").show();
             // Set title
             $("#contentColRight > h4.page-heading").html("Please select the Vendor rules for <span class='vendorTxt1'></span>");
             $("#vendorAsmText, #vendorAttrText").css('display','none');
             $("#pMsg").html("<span class='col-md-12'> Please select the Sub Commodities to activate for <span class='title-style vendorTxt'><b>"+vendor+"</b></span></span>");
             $("#vendorBufferDivHeading").css('display', '');             
             //

            var listActives = [];
            if($('.jstree-checked').length>0){
                $("#vendorRule").show();
            }
            $('.jstree-checked').each(function(){            
                var id   = $(this).attr('id');
                listActives.push(id);
            })
            
            if(listActives.length>0){
                $("#vendorAttr").show();
                $("#vendorRule").show();
                $("#reset-toDefault-btn").show();
            } else {
                $("#vendorAttr").show();
                $("#vendorRule").hide();
                $("#reset-toDefault-btn").hide();                
            }
            
            $(".vendorTxt1").html($(this).text()); 
            $("#vendorAttr").show();     
            var request = [];
                request.type = "vend";
                request.parentNodeValue ='';
                request.childNodeValue ='';
            checkFirstLoad = 0;
           var hiddenVendorRule =  $("#vendorRule").is(":hidden");
           flagHaveActivate = false;
           
            if(currentNode !== $("#vendorRules").text()) {
                checkRuleChanged();                
                
                if(isChanges()) {                
                    bootbox.dialog({
                        title : "Confirmation",
                        message: msgLoseChange,
                        buttons: {
                            main: {
                              label: "Yes",
                              className: "btn-primary",
                              callback: function() {                
                                    load_RulesSet(request);                            
                                    setChangeStatus(false);
                                    saveChangeArrAfterLoadAssignForVendor = [];
                                    if(!hiddenVendorRule){ 
                                        saveChangeArrAfterLoadAssignForVendor = saveChangeArrAfterLoad;
                                        flagHaveActivate = true;
                                    }                            
                                    
                                    saveChangeArrAssignAfterSave = [];   
                                    currentNode = $("#vendorRules").text();
                                    disableSaveButton(saveChangeArrAfterLoadAssign);
                                    if(saveChangeArrAfterLoadAssignForVendor.length>0){
                                        $("#reset-toDefault-btn").prop("disabled", false);                                                  
                                    }
                              }
                            },  
                            cancel: {
                              label: "No",
                              className: "btn-default",
                              callback: function(){
                                    $("#jstreeCommo .jstree-container-ul li#"+currentNode+" > .jstree-anchor").trigger('click');
                                    $("#treeHie .jstree-container-ul .jstree-children li#"+currentNode+" > .jstree-anchor").trigger('click');
                                    if(saveChangeArrAfterLoadAssignForVendor.length<=0){
                                        $("#assortment-save-btn").prop("disabled", true);                                                   
                                    } else {
                                        $("#assortment-save-btn").prop("disabled", false);
                                    }
                              }
                            }                                               
                        },
                        onEscape : function(){
                            $("#jstreeCommo .jstree-container-ul li#"+currentNode+" > .jstree-anchor").trigger('click');
                            $("#treeHie .jstree-container-ul .jstree-children li#"+currentNode+" > .jstree-anchor").trigger('click');
                            if(saveChangeArrAfterLoadAssignForVendor.length<=0){
                                $("#assortment-save-btn").prop("disabled", true);                                                   
                            } else {
                                $("#assortment-save-btn").prop("disabled", false);
                            }
                        }      
                    });
                } else {
                    load_RulesSet(request);

                    setChangeStatus(false);
                    saveChangeArrAfterLoadAssignForVendor = [];
                    
                    if(!hiddenVendorRule){ 
                        saveChangeArrAfterLoadAssignForVendor = saveChangeArrAfterLoad;
                        flagHaveActivate = true;
                    }
                    
                    saveChangeArrAssignAfterSave = [];
                    currentNode = $("#vendorRules").text();                    
                    disableSaveButton(saveChangeArrAfterLoadAssign);
                }
            }                                      
        });
        $("#assortment-save-btn").click(function () {
            if(requestURL !== undefined){               
                requestURL = '';
            }
            if(!$("#vendorAttr").is(':hidden')){
                TreeNode.parentNodeValue ='';
                TreeNode.childNodeValue =''; 
            }
            var listAttrs = [];
            var attr;            
            $("#formHie input[id^='attr-']").each(function(index,data){                
                attrId = $(this).attr("id").split("-");                
                attr = {
                    id : attrId[1],
                    value:$(this).val()
                }                               
                listAttrs.push(attr);
            });
            if($("#attrCheckbox-9").attr('checked')) {
                attr = {
                        id : 9,
                        value:'Y'
                    }
                listAttrs.push(attr);
            } else {
                attr = {
                        id : 9,
                        value:'N'
                    }
                listAttrs.push(attr);
            }
            var lstRules = getRuleChanges();           

            var arrEmpty = [];
            var arrValidValue = [];
            var arrValidValue2 = [];
            var arrOnlyValueAccept = [];
            var arrValidateMargin = [];
            var arrValidateRetail = [];
            $("#retailStart, #retailEnd, #maginStart, #maginEnd, #rule10").each(function(){
                if (_.isEmpty($(this).val()) && $(this).attr('disabled') === undefined) {
                   arrEmpty.push($(this).attr('id'));
                } else {                    
                    if($(this).attr('id')==='retailStart' && $(this).attr('disabled') === undefined){
                        var retailEnd = $("#retailEnd").val();
                        if ($.isNumeric($(this).val())) {
                            if(isNum($(this).val())&& $(this).val()<=100000 && $(this).val()>=0){
                                if(retailEnd.length > 0){
                                    if($(this).val() === retailEnd && $(this).val()===0){
                                        if($.inArray("retailEnd", arrValidValue) == -1){
                                            arrValidValue.push("retailEnd");
                                        }
                                    }
                                    if(!isNum($(this).val())){                      
                                        arrValidValue2.push("retailStart");
                                    }
                                } else {
                                    clearTooltipForValidationDiv("retailStart");
                                }                               
                            } else {
                                arrValidateRetail.push("retailStart");
                            }                  
                        }
                    } else if ($(this).attr('id')==='retailEnd' && $(this).attr('disabled') === undefined){
                        var retailStart = $("#retailStart").val();
                        if ($.isNumeric($(this).val())) {
                            if(isNum($(this).val())&& $(this).val()<=100000 && $(this).val()>=0){
                                if(retailStart.length > 0){
                                    if($(this).val() === retailStart && $(this).val()===0){
                                        if($.inArray($(this).attr('id'), arrValidValue) == -1){
                                            arrValidValue.push($(this).attr('id'));
                                        }
                                    }
                                    if(!isNum($(this).val())){
                                        arrValidValue2.push("retailEnd");
                                    }
                                    if($(this).val()>100000) {
                                        arrOnlyValueAccept.push($(this).attr('id'));
                                        tooltipForValidationDiv(this, msgRange0To100000Accept, "top");
                                    }       
                                } else {
                                    clearTooltipForValidationDiv("retailEnd");  
                                }
                            } else {
                                arrValidateRetail.push("retailEnd");
                            }
                        }                                               
                    } else if($(this).attr('id')==='maginStart' && $(this).attr('disabled') === undefined){
                        var marginEnd = $("#maginEnd").val();
                        if ($.isNumeric($(this).val())) {
                            if(isNum($(this).val())&& $(this).val()<=10000 && $(this).val()>=-10000){
                                if(marginEnd.length > 0){
                                    if ($.isNumeric($(this).val())) {                           
                                        if($(this).val() === marginEnd && $(this).val()===-10000 || $(this).val() === marginEnd && $(this).val()===10000){
                                            if($.inArray("maginEnd", arrValidValue) == -1){
                                                arrValidValue.push("maginEnd");
                                            }
                                        }
                                    } else {
                                        arrValidValue2.push("maginStart");
                                    }
                                } else {
                                    clearTooltipForValidationDiv("maginStart");
                                }
                            } else {
                                arrValidateMargin.push("maginStart");
                            }
                        }                       
                    } else if($(this).attr('id')=='maginEnd' && $(this).attr('disabled') === undefined){
                        var maginStart = $("#maginStart").val();
                        if ($.isNumeric($(this).val())) {
                            if(isNum($(this).val())&& $(this).val()<=10000 && $(this).val()>=-10000){
                                if(maginStart.length > 0){                          
                                    if ($.isNumeric($(this).val())) {                           
                                        if($(this).val() === maginStart && $(this).val()===-10000 || $(this).val() === maginStart && $(this).val()===10000){                        
                                            if($.inArray($(this).attr('id'), arrValidValue) == -1){
                                                arrValidValue.push($(this).attr('id'));
                                            }
                                        }
                                    } else {
                                        arrValidValue2.push("maginEnd");
                                    }
                                } else {                            
                                    clearTooltipForValidationDiv("maginEnd");
                                }
                            } else {
                                arrValidateMargin.push("maginEnd"); 
                            }
                        }                                                               
                    } else if($(this).attr('id')=='rule10' && $(this).attr('disabled') === undefined){
                        var valueOFRule = $("#rule10").val();
                        if(valueOFRule <0 || valueOFRule >100){
                            arrOnlyValueAccept.push($(this).attr('id'));                        
                        }
                        if(!isNum($(this).val())){
                            arrValidValue2.push("rule10");
                        }
                    }
                                        
                }
            });      
            
            if(arrEmpty.length<=0 && arrValidValue.length <=0 && arrOnlyValueAccept.length<=0 && arrValidValue2.length <=0 && arrValidateMargin.length<=0 && arrValidateRetail.length <=0){
                                     
                updateAssortmentRules(TreeNode, lstRules, listAttrs);
            } else{        
                if(arrEmpty.length > 0){
                    $.each(arrEmpty,function(d, data){
                        tooltipForValidationDiv($("#"+data), msgRequiredInput, "top");
                        $("#"+data).trigger('focus');
                    });
                } else if(arrValidValue.length > 0){
                    $.each(arrValidValue,function(d, data){
                        tooltipForValidationDiv($("#"+data), msgGreaterThanZezo, "top");
                        $("#"+data).trigger('focus');
                    });
                } else if(arrOnlyValueAccept.length > 0){
                    $.each(arrValidValue,function(d, data){
                        tooltipForValidationDiv($("#"+data), msgRange0To100Accept, "top");
                        $("#"+data).trigger('focus');
                    });
                } else if(arrValidValue2.length > 0){
                    $.each(arrValidValue2,function(d, data){
                        tooltipForValidationDiv($("#"+data), msgInvalidValue, "top");
                        $("#"+data).trigger('focus');
                    });
                } else if(arrValidateMargin.length > 0){
                    $.each(arrValidateMargin,function(d, data){
                        tooltipForValidationDiv($("#"+data), msgRangeMarginAccept, "top");
                        $("#"+data).trigger('focus');
                    });
                } else if(arrValidateRetail.length > 0) {
                    $.each(arrValidateRetail,function(d, data){
                        tooltipForValidationDiv($("#"+data), msgRange0To100000Accept, "top");
                        $("#"+data).trigger('focus');
                    });                    
                }                
            }  
        });

        $("#reset-toDefault-btn").click(function(){
            if(requestURL !== undefined){               
                requestURL = '';
            }          
             var vendorId = getVendorId(); 
            var comd = '';
            var subComd = '';
            var message = "<h5>Do you wish to reset the rules to higher level?</h5>";   

            if(TreeNode != null){
                if($("#vendorRules").hasClass('VendorIsClicked')){
                    comd = '';
                    subComd = '';
                    message = "<h5>Do you wish to reset the rules?</h5>";
                } else {
                    if($.trim(TreeNode.parentNodeValue) =='#'){
                        comd = TreeNode.childNodeValue;
                        subComd = '';
                    }else{
                        comd = TreeNode.parentNodeValue;                        
                        subComd = TreeNode.childNodeValue.split('id')[1];
                    }
                }               
            }
            if(subComd != '') {
                comd = '';       
            }
            
            bootbox.dialog({
                title : "Confirmation",
                message: message,
                buttons: {
                    main: {
                      label: "Yes",
                      className: "btn-primary",
                      callback: function() {
                        resetToHigherLevel(vendorId, comd,subComd);
                      }
                    },         
                    cancel: {
                      label: "No",
                      className: "btn-default"                      
                    }                           
                }       
            });
        });

/************************************************************************************************************
* VALIDATION
* ************************************************************************************************************/
        $(document).on('keypress ', '.input-decimal', function(event){
            acceptDecimalNumber(this, event, 11, 2);
        });
        $(document).on('keypress ', '#maginStart,#maginEnd', function(event){
            acceptMaginNumber(this, event, 11, 2);
        });     
        $("#rule10, #retailStart, #retailEnd, #maginStart, #maginEnd").on('blur',function(){
            hideToolTip(this);
            if($(this).attr('id')!=="rule10"){
                if($(this).attr('id')==="retailStart" || $(this).attr('id')==="retailEnd" ){
                    var val = $(this).val();
                    if (!_.isEmpty(val) && $.isNumeric(val)) {
                        if(isNum(val)&& val<=100000 && val>=0){
                            $(this).val(precise_roundFloatNum(val));            
                        }
                        if(isNum(val) && val <0) {
                            var number = val.toString();
                            if(number.indexOf(".")>=0){
                                var numArr = number.split(".");
                                var decimals = numArr[1].toString();
                                var num;
                                if(parseInt(numArr[0]) === 0){
                                    numArr[0] = '-'+0;
                                } else {
                                    numArr[0] = parseInt(numArr[0]);
                                }
                                
                                if(decimals.length > 2){
                                    num = numArr[0] +"." + decimals.substr(0,2);
                                } else if(decimals.length ===2){
                                    num = numArr[0] +"." + decimals;
                                } else if(decimals.length===0){
                                    num = parseInt(number);
                                } else {
                                    decimals = decimals + "0000";
                                    num = parseInt(numArr[0]) +"." + decimals.substr(0,2);
                                }                       
                                $(this).val(num);
                            } else {       
                                var num = parseInt(number);
                                $(this).val(num);
                            }
                        }
                        if(isNum(val) && val>100000) {
                            var number = val.toString();
                            if(number.indexOf(".")>=0){
                                var numArr = number.split(".");
                                var decimals = numArr[1].toString();
                                if(decimals.length ===0){                                   
                                    $(this).val(parseInt(number));
                                }
                            }
                        }                       
                    }
                }
                if($(this).attr('id')==="maginStart" || $(this).attr('id')==="maginEnd" ){
                    var val = $(this).val();
                    if (!_.isEmpty(val) && $.isNumeric(val)) {
                        if(isNum(val)&& val<=10000 && val>=-10000){
                            $(this).val(precise_roundFloatNum(val));        
                        }
                        if(isNum(val) && val <-10000) {
                            var number = val.toString();
                            if(number.indexOf(".")>=0){
                                var numArr = number.split(".");
                                var decimals = numArr[1].toString();
                                var num;
                                if(decimals.length > 2){
                                    num = parseInt(numArr[0]) +"." + decimals.substr(0,2);
                                } else if(decimals.length ===2){
                                    num = parseInt(numArr[0]) +"." + decimals;
                                } else if(decimals.length ===0){
                                    num = parseInt(number); 
                                }else {
                                    decimals = decimals + "0000";
                                    num = parseInt(numArr[0]) +"." + decimals.substr(0,2);
                                }                               
                                $(this).val(num);
                            } else {
                                var num = parseInt(number);
                                $(this).val(num);
                            }
                        }
                        if(isNum(val) && val>10000) {
                            var number = val.toString();
                            if(number.indexOf(".")>=0){
                                var numArr = number.split(".");
                                var decimals = numArr[1].toString();
                                if(decimals.length ===0){                                   
                                    $(this).val(parseInt(number));
                                }
                            }
                        }            
                    }
                }
            }
            var changesRuleArr = getRuleChanges();
            if(saveChangeArrAssignAfterSave.length > 0){
                var changedArr = compare2Arr(saveChangeArrAssignAfterSave,changesRuleArr);      
                    
                if(changedArr.length > 0){
                    setChangeStatus(true);
                    $("#assortment-save-btn").prop("disabled", false);
                } else {
                    setChangeStatus(false);
                    $("#assortment-save-btn").prop("disabled", true);
                }
            } else {
                var changesArr = compare2Arr(changesRuleArr, saveChangeArrAfterLoadAssign);
                if(saveChangeArrAfterLoadAssign.length<=0){                 
                    if(inheritRuleArr.length>0){ 
                        var OnRuleArr = getRuleON();
                        var compareRule = compare2Arr(OnRuleArr, inheritRuleArr);
                        
                        if(compareRule.length>0){
                            $.each(compareRule, function(d, dt){
                                $("#chk-"+dt).parent().removeClass('ibutton-container-checkmark').addClass('ibutton-container-on');
                                $("#chk-"+dt).next().removeClass("ibutton-handle-checkmark").addClass("ibutton-handle");
                            });
                            setChangeStatus(true);
                            $("#assortment-save-btn").prop("disabled", false);
                        }
                    }
                    
                } else if(changesArr.length>0) {           
                    setChangeStatus(true);
                    $("#assortment-save-btn").prop("disabled", false);
                } else {              
                    setChangeStatus(false);
                    $("#assortment-save-btn").prop("disabled", true);
                }
            }

        });
        $("#rule10").on('change',function(){
            if (_.isEmpty($(this).val())) {             
                tooltipForValidationDiv(this, msgRequiredInput, "top",null);                
            } else {                
                clearTooltipForValidationDiv("rule10");             
                var val = $(this).val();
                if(!isNum(val) && $(this).val()>100){                   
                    tooltipForValidationDiv($(this), msgRange0To100Accept, "top");
                } else if(val <0 || val >100){
                    tooltipForValidationDiv($(this), msgRange0To100Accept, "top");
                } else {
                    clearTooltipForValidationDiv("rule10");
                    $(this).val(parseInt($(this).val()));
                }

            }
        });
        $("#retailStart, #maginStart").on('change',function(){          
            if (_.isEmpty($(this).val())) {             
                tooltipForValidationDiv(this, msgRequiredInput, "top",null);                
            } else {

                if (!$.isNumeric($(this).val())) {
                    if(isNum($("#retailEnd").val())) {
                        clearTooltipForValidationDiv("retailEnd");  
                    }                                   
                    tooltipForValidationDiv(this, msgInvalidValue, "top");
                } else {
                    clearTooltipForValidationDiv($(this).attr('id'));
                    if($(this).attr('id') === "retailStart") {
                        var retailEnd = $("#retailEnd").val();
                        var endValue = precise_roundFloatNum(retailEnd);    
                        if(isNum($(this).val())&& $(this).val()<=100000 && $(this).val()>=0){                           
                            if(isNum(retailEnd)){                               
                                var val = precise_roundFloatNum($(this).val());                                                         
                                $(this).val(val);   
                                var startValue = val;
                                    
                                if (!_.isEmpty(retailEnd)) {
                                    if(retailEnd<0 || retailEnd>100000){
                                        tooltipForValidationDiv("retailEnd", msgRange0To100000Accept, "top");
                                    } else {
                                        if(parseFloat(startValue) >parseFloat(endValue) || parseFloat(startValue) === parseFloat(endValue) ){                                       
                                            //Maximum allowed {0} should be greater than Minimum allowed {0}    
                                            clearTooltipForValidationDiv("retailEnd");
                                            tooltipForValidationDiv(this, "Minimum allowed {0} should be lower than Maximum allowed {0}", "top","retail");
                                        } else if(startValue === 0 && endValue ===0){ 
                                            tooltipForValidationDiv($("#retailEnd"), msgGreaterThanZezo, "top");
                                        }else {                         
                                            clearTooltipForValidationDiv("retailEnd");
                                        }
                                    }
                                }
                            }
                            
                        } else {                            
                            if (!_.isEmpty(retailEnd)) {
                                if(retailEnd>0 && retailEnd<100000){
                                    clearTooltipForValidationDiv("retailEnd");
                                }
                            }
                            tooltipForValidationDiv(this, msgRange0To100000Accept, "top");
                        }
                    }
                    if($(this).attr('id') === "maginStart") {                       
                        if(validFormatMagin($(this).val())){                    
                            var maginEnd = $("#maginEnd").val();
                            var endValue = precise_roundFloatNum(maginEnd);     
                            if(isNum($(this).val())&& $(this).val()<=10000 && $(this).val()>=-10000){                           
                                if(isNum(maginEnd)){                                
                                    var val = precise_roundFloatNum($(this).val());
                                    $(this).val(val);   
                                    var startValue = val;
                                    
                                    if (!_.isEmpty(maginEnd)) {
                                        if(maginEnd<-10000 || maginEnd>10000){
                                            tooltipForValidationDiv("maginEnd", msgRangeMarginAccept, "top");
                                        } else {
                                            if(parseFloat(startValue) >parseFloat(endValue) || parseFloat(startValue) === parseFloat(endValue) ){                                           
                                                clearTooltipForValidationDiv("maginEnd");
                                                tooltipForValidationDiv(this, "Minimum allowed {0} should be lower than Maximum allowed {0}", "top","Margin");
                                            } else if(startValue === -10000 && endValue ===10000){ 
                                                tooltipForValidationDiv($("#maginEnd"), "The input value should be greater than -10,000.", "top");
                                            }else {                         
                                                clearTooltipForValidationDiv("maginEnd");
                                            }
                                        }
                                    }
                                }
                                
                            } else {
                                if (!_.isEmpty(maginEnd)) {
                                    if(endValue<=10000 && endValue>=-10000) {
                                        clearTooltipForValidationDiv("maginEnd");
                                    }
                                }
                                tooltipForValidationDiv(this, msgRangeMarginAccept, "top");
                            }
                        } else {
                            tooltipForValidationDiv(this, msgInvalidValue, "top");
                        }
                    }
                }
            }
        });
        $("#retailEnd, #maginEnd").on('change',function(){          
            if (_.isEmpty($(this).val())) {
                tooltipForValidationDiv(this, msgRequiredInput, "top");
            } else {
                if (!$.isNumeric($(this).val())) {
                    if(isNum($("#retailStart").val())) {
                        clearTooltipForValidationDiv("retailStart");    
                    }
                    tooltipForValidationDiv(this, msgInvalidValue, "top");
                } else {
                    clearTooltipForValidationDiv($(this).attr('id'));
                    if($(this).attr('id') === "retailEnd") {        
                        var retainStart = $("#retailStart").val();
                        if(isNum($(this).val())&& $(this).val()<=100000 && $(this).val()>=0){                       
                            if(isNum(retainStart)) {
                                var val = precise_roundFloatNum($(this).val());
                                $(this).val(val);   
                                var endValue = val; 
                                //$("#retailStart").val()   
                                if (!_.isEmpty(retainStart)) {
                                    if(retainStart<0 || retainStart>100000){
                                        tooltipForValidationDiv("retailStart", msgRange0To100000Accept, "top");
                                    } else {
                                        var startValue = precise_roundFloatNum(retainStart);                                    
                                                                        
                                        if(parseFloat(startValue) >parseFloat(endValue) || parseFloat(startValue) === parseFloat(endValue)){                            
                                            clearTooltipForValidationDiv("retailStart");
                                            tooltipForValidationDiv(this, "Maximum allowed {0} should be greater than Minimum allowed {0}", "top","retail");
                                        } else if(parseFloat(startValue) === 0 && parseFloat(endValue) ===0){ 
                                            if($(this).attr('id')==='retailStart') {
                                                tooltipForValidationDiv(this, msgGreaterThanZezo, "top");
                                            }
                                        } else {                                    
                                            clearTooltipForValidationDiv("retailStart");
                                        }
                                    }
                                }
                                
                            }                       
                        } else {
                            if (!_.isEmpty(retainStart)) {
                                if(retainStart>0 && retainStart<100000){
                                    clearTooltipForValidationDiv("retailStart");
                                }
                            }
                            tooltipForValidationDiv(this, msgRange0To100000Accept, "top");
                        }   
                    }
                    if($(this).attr('id') === "maginEnd") {
                        if(validFormatMagin($(this).val())){    
                            var maginStart = $("#maginStart").val();
                            if(isNum($(this).val())&& $(this).val()<=10000 && $(this).val()>=-10000){                       
                                if(isNum(maginStart)) {
                                    var val = precise_roundFloatNum($(this).val());
                                    $(this).val(val);   
                                    var endValue = val; 
                                    //$("#retailStart").val()   
                                    if (!_.isEmpty(maginStart)) {
                                        if(maginStart<-10000 || maginStart>10000){
                                            tooltipForValidationDiv("maginStart", msgRangeMarginAccept, "top");
                                        } else {
                                            var startValue = precise_roundFloatNum(maginStart);                                 
                                                                            
                                            if(parseFloat(startValue) >parseFloat(endValue) || parseFloat(startValue) === parseFloat(endValue)){                            
                                                clearTooltipForValidationDiv("maginStart");
                                                tooltipForValidationDiv(this, "Maximum allowed {0} should be greater than Minimum allowed {0}", "top","Margin");
                                            } else if(parseFloat(startValue) === 0 && parseFloat(endValue) ===0){ 
                                                if($(this).attr('id')==='maginStart') {
                                                    tooltipForValidationDiv(this, "The input value should be greater than -10,000.", "top");
                                                }
                                            } else {                                    
                                                clearTooltipForValidationDiv("maginStart");
                                            }
                                        }
                                    }
                                    
                                }                       
                            } else {
                                if (!_.isEmpty(maginStart)) {
                                    if(maginStart>-10000 && maginStart<10000){
                                        clearTooltipForValidationDiv("maginStart");
                                    }
                                }
                                tooltipForValidationDiv(this, msgRangeMarginAccept, "top");
                            }
                        } else {
                            tooltipForValidationDiv(this, msgInvalidValue, "top");
                        }
                    }               
                }
            }
            
            
        });
        
        /*$("#retailEnd, #maginEnd,#retailStart, #maginStart, #rule10").keydown(function (e) {      
            var arrValid = [46, 8, 9, 27, 13, 189, 109,110, 190];
            if($(this).attr("id") !=='maginEnd' && $(this).attr("id") !=='maginStart' && $(this).attr("id") !=='rule10'){
                arrValid = [46, 8, 9, 27, 13, 110, 190];
            }
        // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, arrValid) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });*/
       
/************************************************************************************************************
* END VALIDATION
************************************************************************************************************/
    });

function getRuleChanges() {
    var listRules = [];            
    var uniqueNames = [];
    
    var rule;
    var ruleId;
    var ruleName;             
    var activeSw = 'Y';
    var valType = '1';
    var pctSw = 'N';
    $("#formHie input[name='chk-4'],input[name='chk-2'],input[name='chk-5'],input[name='chk-6'],input[name='chk-7'],input[name='chk-8'],input[name='chk-9'],input[name='chk-1'],input[name='chk-3'], input[name='chk-10']").each(function(index,data){
        //log($(this));
        var values = [];                
        ruleId = $(this).attr("name").split("chk-");
        ruleName = $(this).attr('name').split("chk-");
        if($(this).attr('checked')){
            if (typeof $(this).attr("data-left-check") !== 'undefined') {
                var index_checkbox = $(this).attr("data-left-check");
                                                
                var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");
                activeSw = 'Y';
                if(checkbox_Data.length > 1){
                valType = '2';                            
                    $.each(checkbox_Data,function (index, dInData) {                                
                        if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){                                                                                               
                            values.push("Start:" +$(dInData).val());
                            if($(dInData).attr('id') == 'maginStart'){
                                pctSw = 'Y';
                            } else {
                                pctSw = 'N';
                            }
                        }   
                        if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){                                        
                            values.push("End:" +$(dInData).val());                              
                        }                                                           
                    });
                }else{                            
                    if($(this).attr("name")==='chk-10'){
                        pctSw = 'Y';
                        valType = '2';
                        values.push(parseInt($("#rule10").val()));
                    } else {
                        pctSw = 'N';
                        valType = '1';                          
                        values.push('');
                    }
                }
                rule = {
                    
                    id: ruleId[1],
                    name: ruleName[1],
                    values:values,
                    valType : valType,
                    activeSw : activeSw,
                    pctSW : pctSw                        
                }
            }
        } else {
            valType = '1';  
            activeSw = 'N';                                     
            if (typeof $(this).attr("data-left-check") !== 'undefined') {
                var index_checkbox = $(this).attr("data-left-check");
                                                
                var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");                      
                if(checkbox_Data.length > 1){
                    valType = "2";
                    $.each(checkbox_Data,function (index, dInData) {                                
                        if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){
                            values.push("End:" +0);                           
                            if($(dInData).attr('id') === 'maginEnd'){
                                pctSw = 'Y';
                            } else {
                                pctSw = 'N';
                            }
                        }
                        if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){
                            values.push("Start:" +0);
                        }   
                    });                        
                }else{
                    if($(this).attr("name")==='chk-10'){
                        pctSw = 'Y';
                        valType = '2';                          
                        values.push(0);
                    } else {
                        valType = '1';  
                        pctSw = 'N';                            
                        values.push('');
                    }                
                }
                rule = {                            
                    id: ruleId[1],
                    name: ruleName[1],
                    values: values,
                    valType : valType,
                    activeSw : activeSw,
                    pctSW : pctSw                            
                }
            }                   
        }
        listRules.push(rule);
        uniqueNames = [];
        $.each(listRules, function(i, el){
            if(el != undefined && $.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
        });
    });
    
    return uniqueNames;
}
 /**
 * display error message by tooltip.
 * @param inputId id of element
 * @param message error 
 * @param placement the placement of message top|right|bottom|left, optional param.
 */
function tooltipForValidationDiv(selectorInput, message, placement, paramFormat){    
    if(paramFormat != null){
        message = message.format(paramFormat);
    }       
    $(selectorInput).closest('div').addClass("has-error");
    placement = (typeof placement === "undefined") ? "top" : placement;
    $(selectorInput).attr("data-toggle", "tooltip");
    $(selectorInput).attr("data-original-title", message);
    $(selectorInput).tooltip({
        html:true,
        placement:placement,
        trigger:'hover manual'      
    }).tooltip('show');    
}
function hideToolTip(selectorInput){
    $(selectorInput).tooltip('hide');
}
/**
 * clear error message by tooltip.
 * @param inputId id of element
 */
function clearTooltipForValidationDiv(id){
    $("#" + id).closest('div').removeClass("has-error");
    $("#" + id).tooltip('hide');
    $("#" + id).removeAttr("data-toggle");
    $("#" + id).removeAttr("data-original-title");
}
/**
 * Update Assortment Rules.
 * @author Duyen.le
*/
function updateAssortmentRules(levelKey, lstRules, lstAttrs) {
    changedArr = [];
    var vendorId = getVendorId();
    var comd = '';
    var subComd = '';   
    if(levelKey !== null){ 
        if($.trim(levelKey.parentNodeValue) =='#'){
            comd = levelKey.childNodeValue;
            subComd = '';
        }else{ 
            comd = levelKey.parentNodeValue;
            subComd = levelKey.childNodeValue.split('id')[1];
        }
    }
    var AttrAndRuleVendorVO;

    log("vendorId : " +vendorId);
    log("commodityID : " +comd);
    log("sub : " +subComd);

    if(vendorId != undefined) {     
        if ((comd === '' || comd === undefined) && (subComd ==='' || subComd === undefined)){
            // hard code lstActive               
            AttrAndRuleVendorVO = {
                lstVendorAttrs : lstAttrs,
                lstVendorRules : lstRules,
                vendorId : vendorId,
                commodityId : comd,
                subCommdId : subComd
            }
        } else { 
            if(comd != '') {
                if(subComd == ''){
                    //console.log("commodityID");
                    AttrAndRuleVendorVO = {
                        lstVendorRules : lstRules,
                        vendorId : vendorId,
                        commodityId : comd
                    }
                } else {
                    //console.log(" sub Commodity");
                    AttrAndRuleVendorVO = {
                        lstVendorRules : lstRules,
                        vendorId : vendorId,
                        subCommdId : subComd
                    }
                }
            }
            
        }

    }    

    var count = 0;
    _.each($(".input-sm"), function(value, key) {
        if ($(value).closest('div').hasClass('has-error')) {
            count++;
        }
    });
    if (count === 0 ){        
        callbackConfirmSave(AttrAndRuleVendorVO);
    }    
}
/**
 * Save Rule.
 * @author Duyen.le
*/
function saveRule(AttrAndRuleVendorVO){
    $.ajax({
        type: 'POST',
        url: appName + "/ajax/assortment-updateRules",
        cache: false,
        async: false,   
        data: JSON.stringify(AttrAndRuleVendorVO),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {              
            if(data.rs == true){
                showAlertSuccess('Save successfully!');
                removeAlertSuccess(3000);
                load_RulesSet(TreeNode);
                // set flag to Node ------------------
               
                
                    
                if(AttrAndRuleVendorVO.vendorId !=='') {                     
                     if((AttrAndRuleVendorVO.commodityId !== '' && AttrAndRuleVendorVO.commodityId !== undefined)|| (AttrAndRuleVendorVO.subCommdId !== '' && AttrAndRuleVendorVO.subCommdId !== undefined)) {
                        var node = $('#jstreeCommo').jstree("get_selected");
                        var nodeData = $('#jstreeCommo').jstree(true).get_node(node);
                        var parentOfSelectedNode =  nodeData.parent;
                         nodeData.a_attr.class= "rule-flag";
                        $('#jstreeCommo').jstree(true).redraw_node(node,true);

                        if(parentOfSelectedNode !=='#'){
                            $("#jstreeCommo .jstree-container-ul li#"+parentOfSelectedNode+" .jstree-children li .jstree-anchor").each(function(d, dt){                
                                var str = $(this).html();
                                var x = str.split('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>')[1];                           
                                if (x.length > 20) {
                                    var decoded = $('<div/>').html(x).text();                       
                                    $(this).attr('title',decoded);
                                    //$(this).addClass('text-ellipsis-Rule');                                  
                                    $(this).html('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>'+ x.substr(0,15) + '...');   
                                }
                            });
                        } else {
                            if (nodeData.text.length > 23) {                              
                                $("#jstreeCommo .jstree-container-ul li#"+node+" .jstree-anchor").attr('title',nodeData.text);  
                                $("#jstreeCommo .jstree-container-ul li#"+node+" .jstree-anchor").html('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>'+ nodeData.text.substr(0,20) + '...');                                         
                            }
                        }
                    }
                }                                               

                $.each($("input[type=checkbox]"),function(id, dData){
                    if($(this).parent().hasClass('ibutton-container-checkmark')){
                        $(this).next().removeClass('ibutton-handle-checkmark').addClass('ibutton-handle');
                        $(this).parent().removeClass('ibutton-container-checkmark').addClass('ibutton-container-on');
                    }

                });
                setChangeStatus(false);
            } else {
                showAlertDanger('The system has encountered an error. Please try again later!');
                removeAlertSuccess(3000);
                setChangeStatus(false);
            }
        },
        error: function (xhr, ajaxOptions, error) {                    
            alert('Error: ' + xhr.responseText);
        }
    });
}
/**
 * load Commodity [Tree].
 * @author Duyen.le
*/
function load_Commodity_Tree(){

   var vendor = $("#vendor-input option:selected").text();
     var vendorId = getVendorId();        
    //
    if(vendorId !== '') {        
        $('#jstreeCommo').jstree("destroy");
        $('#jstreeCommo').jstree({
            'core' : {
                'data' : {
                    'type': 'POST',
                    'url' : appName + "/ajax/getCommodityAndSub",
                    'data' : function (obj, cb) {                                           
                        nodeId = obj.id;   
                        return { 'id' : nodeId, 'vendorId': vendorId };
                    }
                },
                'check_callback' : true,
                'themes' : {
                    'responsive' : false,
                    "icons": false,
                    "stripes": false,
                    "variant": "large",
                    "dots": true
                }
            },
            "checkbox" : {
                //"keep_selected_style" : false,
                //"tie_selection" :  true,
                //"undetermined" : false
                //"whole_node" : false
                "keep_selected_style" : true,
              "tie_selection" :  false,
              "undetermined" : false,
              "whole_node" : false,
              "select_multiple" : false
            },
            "plugins": ["types", "wholerow","checkbox"]  //"dnd", "contextmenu", "search",  
        }).on('select_node.jstree', function (e, data) {
            var vendor = $("#vendor-input option:selected").text();          
            removeAlertSuccess(0);
            $("#contentColRight > h4.page-heading").html("Please select the default rules for <span class='vendorTxt1'></span>");
            $("#pMsg").html("<span class='col-md-12'> Please select the Sub Commodities to activate for <span class='title-style vendorTxt'><b>"+vendor+"</b></span></span>");
            $("#vendorBufferDivHeading").css('display', 'none'); 
            select_node = true;
            $("#vendorRules").removeClass('VendorIsClicked');
            $("#vendorAsmText, #vendorAttrText").css('display','');
            saveChangeArrAfterSave = [];
            saveChangeArrAfterLoad = [];

            $("#vendorAttr").hide();
             $("#contentColRight").show();
            $("#vendorRule").show();
            $("#reset-toDefault-btn").show();            
            $("#turnOnAllRule").iButton('disable', false);
            $("#formHie input[name^='chk-']").iButton('disable', false);

            var i, j, r = [];
            for(i = 0, j = data.selected.length; i < j; i++) {
              r.push(data.instance.get_node(data.selected[i]).id);
            }                            
            TreeNode.childNodeValue = data.node.id;
            TreeNode.parentNodeValue = data.node.parent;
                       
            inheritRuleArr = [];
            if(TreeNode.childNodeValue !==currentNode){
                $("#assortment-save-btn").prop("disabled", true);
                $("#reset-toDefault-btn").prop("disabled", true);
                if(checkFirstLoad>=1){
                    if(!isChanges()){
                        checkRuleChanged();
                    }                                     
                    
                    if(isChanges()) {              
                        bootbox.dialog({
                            title : "Confirmation",
                            message: msgLoseChange,
                            buttons: {
                                main: {
                                  label: "Yes",
                                  className: "btn-primary",
                                  callback: function() {
                                        inheritRuleArr = [];
                                        offFirstLoadArr = [];             
                                        load_RulesSet(TreeNode);
                                        setChangeStatus(false);
                                        saveChangeArrAfterLoadAssign = [];
                                        saveChangeArrAfterLoadAssign = saveChangeArrAfterLoad;
                                        currentNode = TreeNode.childNodeValue;
                                        saveChangeArrAssignAfterSave = [];
                                        disableSaveButton(saveChangeArrAfterLoadAssign);
                                        
                                        $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+ ' and ' + data.node.text +'</b>');
                                        if(saveChangeArrAfterLoadAssign.length>0){
                                            $("#reset-toDefault-btn").prop("disabled", false);
                                            $("#global-save-btn").prop("disabled", true);                                              
                                        } else {
                                            $("#global-save-btn").prop("disabled", true);
                                        }
                                        onArr = [];
                                        offArr = [];
                                    }
                                },  
                                cancel: {
                                  label: "No",
                                  className: "btn-default",
                                  callback : function(){              
                                    $("#jstreeCommo .jstree-container-ul li#"+currentNode+"> .jstree-anchor").trigger('click');
                                    $("#treeHie .jstree-container-ul .jstree-children li#"+currentNode+"> .jstree-anchor").trigger('click');
                                    $("#assortment-save-btn").prop("disabled", false);
                                    if(saveChangeArrAfterLoadAssign.length<=0){
                                        $("#reset-toDefault-btn").prop("disabled", true);    
                                    } else {
                                        $("#reset-toDefault-btn").prop("disabled", false);
                                    }
                                    if(saveChangeArrAssignAfterSave.length>0){
                                        var changedArr = compare2Arr(saveChangeArrAssignAfterSave,getRuleChanges());                                                                                       
                                        if(changedArr.length > 0){                                                  
                                            $("#reset-toDefault-btn").prop('disabled', false);
                                        }
                                    }
                                  }                                  
                                }                                               
                            },
                        onEscape: function() {
                            $("#jstreeCommo .jstree-container-ul li#"+currentNode+"> .jstree-anchor").trigger('click');
                            $("#treeHie .jstree-container-ul .jstree-children li#"+currentNode+"> .jstree-anchor").trigger('click');
                            $("#assortment-save-btn").prop("disabled", false);
                            if(saveChangeArrAfterLoadAssign.length<=0){
                                $("#reset-toDefault-btn").prop("disabled", true);    
                            } else {
                                $("#reset-toDefault-btn").prop("disabled", false);
                            }
                            if(saveChangeArrAssignAfterSave.length>0){
                                var changedArr = compare2Arr(saveChangeArrAssignAfterSave,getRuleChanges());                                                                                       
                                if(changedArr.length > 0){                                                  
                                    $("#reset-toDefault-btn").prop('disabled', false);
                                }
                            }
                        }      
                        });
                    } else {
                        onArr = [];
                        offArr = [];
                        offFirstLoadArr = []; 
                        load_RulesSet(TreeNode);
                        setChangeStatus(false);
                        saveChangeArrAfterLoadAssign = [];
                        saveChangeArrAfterLoadAssign = saveChangeArrAfterLoad;
                        currentNode = TreeNode.childNodeValue;
                        saveChangeArrAssignAfterSave = [];
                        disableSaveButton(saveChangeArrAfterLoadAssign);
                        
                        $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+ ' and ' + data.node.text +'</b>'); 
                    }
                } else {
                    if(!isChanges()){
                        changesRuleArr = getRuleChanges();
                        if(saveChangeArrAssignAfterSave.length > 0){
                            var changedArr = compare2Arr(saveChangeArrAssignAfterSave,changesRuleArr);      
                                
                            if(changedArr.length > 0){
                                setChangeStatus(true);
                            } else {
                                setChangeStatus(false);
                            }
                        } else {
                            var changesArr = compare2Arr(changesRuleArr, saveChangeArrAfterLoadAssignForVendor); 
                                         
                            if(flagHaveActivate === true && saveChangeArrAfterLoadAssignForVendor.length<=0){
                                
                                var off = true;
                                $.each(changesRuleArr, function(d, data){               
                                    if(data.activeSw==='Y'){    
                                        if($("#formHie").find(".ibutton-container-checkmark").length > 0){
                                            if($("#formHie").find(".ibutton-container-on").length > 0){
                                                off = false;
                                            } else {
                                                off = true;
                                            }
                                        } else {
                                            off = false;
                                        }
                                    }
                                });
                                if(off){
                                    setChangeStatus(false);
                                } else{
                                    setChangeStatus(true);
                                }  
                            } else if(changesArr.length>0) {
                                setChangeStatus(true);
                            } else {
                                setChangeStatus(false);
                            }
                        }
                    }
                                        
                    if(isChanges()){                        
                        bootbox.dialog({
                            title : "Confirmation",
                            message: msgLoseChange,
                            buttons: {
                                main: {
                                  label: "Yes",
                                  className: "btn-primary",
                                  callback: function() {     
                                        offFirstLoadArr = [];            
                                        load_RulesSet(TreeNode);
                                        setChangeStatus(false);
                                        saveChangeArrAfterLoadAssign = saveChangeArrAfterLoad;
                                        currentNode = TreeNode.childNodeValue;
                                        saveChangeArrAssignAfterSave = [];
                                        disableSaveButton(saveChangeArrAfterLoadAssign);
                                        
                                        $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+ ' and ' + data.node.text +'</b>'); 
                                  }
                                },  
                                cancel: {
                                  label: "No",
                                  className: "btn-default",
                                  callback : function(){                                    
                                    $("#vendorRules").addClass('VendorIsClicked');         
                                    $("#vendorRules").trigger('click');

                                    $(".vendorTxt1").html('<b>'+vendor.toUpperCase());                                    
                                  }
                                }                                               
                        },
                        onEscape: function() {
                            $("#vendorRules").addClass('VendorIsClicked');
                            $("#vendorRules").trigger('click');
                            $(".vendorTxt1").html('<b>'+vendor.toUpperCase()); 
                        }      
                        });  
                    } else {
                        offFirstLoadArr = []; 
                        load_RulesSet(TreeNode);
                        setChangeStatus(false);
                        saveChangeArrAfterLoadAssign = saveChangeArrAfterLoad;
                        currentNode = TreeNode.childNodeValue;
                        saveChangeArrAssignAfterSave = [];
                        disableSaveButton(saveChangeArrAfterLoadAssign);
                        $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+ ' and ' + data.node.text +'</b>'); 
                    }
                }
            } else {                
                $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+ ' and ' + data.node.text +'</b>'); 
            }
            
            
            checkFirstLoad ++;
            // end test

        }).on('uncheck_node.jstree', function (e, data) {            
            var type;
            var value;
            if(data.node.parent === "#"){                
                type =  "Commodity";
                value = data.node.id;
            } else {                
                type =  "SubCommodity";
                value = data.node.id.split('id')[1];
            }            
            $.ajax({
                type: 'POST',
                url: appName + "/ajax/deActivateRule",
                data: {                    
                    type : type,
                    value : value,
                    vendorId : getVendorId()        
                },
                cache: false,
                async: false,
                success: function (data) {
                    //callback(JSON.parse(data));
                },
                error: function (xhr, ajaxOptions, error) {                    
                    alert('Error: ' + xhr.responseText);
                }
            });
            //console.log($("#jstreeCommo").find('a').hasClass('jstree-checked'), $("#jstreeCommo").find('a').hasClass('jstree-checked'), $("#jstreeCommo").find('i').hasClass('jstree-undetermined'));
            
            if($("#jstreeCommo").find('a').hasClass('jstree-checked') || $("#jstreeCommo").find('i').hasClass('jstree-undetermined')){                
                
            } else {
                $("#vendorRule").hide();
            }
              
        }).on("check_node.jstree", function (e, data) {
            
            if($("#vendorRules").hasClass('VendorIsClicked')){
                $("#reset-toDefault-btn").show();
                if(saveChangeArrAfterLoadAssignForVendor.length > 0){
                    $("#reset-toDefault-btn").prop("disabled", false);
                } else {
                    $("#reset-toDefault-btn").prop("disabled", true);
                }          
                $("#vendorRule").show();
               
            }              
            var type;
            var value;
            if(data.node.parent === "#"){                
                type =  "Commodity";
                value = data.node.id;
            } else {                
                type =  "SubCommodity";
                value = data.node.id.split('id')[1];
            }            
            $.ajax({
                type: 'POST',
                url: appName + "/ajax/activateRule",
                async : true,
                data: {                    
                    type : type,
                    value : value,
                    vendorId : getVendorId()        
                },
                cache: false,
                async: false,
                success: function (data) {
                    if(data === true){

                    } else {

                    }
                    //callback(JSON.parse(data));
                },
                error: function (xhr, ajaxOptions, error) {                    
                    alert('Error: ' + xhr.responseText);
                }                
            });
        }).on("ready.jstree", function (e, data){            
            /*$("#jstreeCommo .jstree-container-ul > li").off('click');
            $("#jstreeCommo .jstree-container-ul > li").on('click', function(e){
                if (e.ctrlKey || e.shiftKey){
                    e.preventDefault();
                    return false;
                }
            });*/            
            $("#jstreeCommo .jstree-container-ul li .jstree-anchor").each(function(d, dt){                
                var str = $(this).html();
                var x = str.split('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>')[1];                           
                if (x.length > 23) {                  
                    var decoded = $('<div/>').html(x).text();     
                    $(this).attr('title', decoded);                   
                    $(this).html('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>'+ x.substr(0,20) + '...');                                 
                }
            });
         
        }).bind("open_node.jstree", function (event, data) {
            $("#jstreeCommo .jstree-container-ul li#"+data.node.id+" .jstree-children li .jstree-anchor").each(function(d, dt){                
                var str = $(this).html();
                var x = str.split('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>')[1];                                       
                if (x.length > 20) {                    
                    var decoded = $('<div/>').html(x).text();                    
                    $(this).attr('title', decoded);
                    //$(this).addClass('text-ellipsis-Rule');    
                    $(this).html('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>'+ x.substr(0,15) + '...');                                 
                }
            });
      });;  
              
    }
    
}
/**
 * load assortmentRules.
 * @author Duyen.le
*/
function load_assortmentRules(callback){    
    $.ajax({
        type: 'POST',
        url: appName + "/ajax/getAssortmentRules",
        cache: false,
        async: false,
        success: function (data) {
            callback(JSON.parse(data));
        },
        error: function (xhr, ajaxOptions, error) {                    
            alert('Error: ' + xhr.responseText);
        }
    });
}
/**
 * Get Hierachy Information.
 * @author Duyen.le
 */
function load_RulesSet(params) {    
    saveChangeArrAfterLoad = [];
    var vendorId = getVendorId();
    var comd = '';
    var subComd = '';   
    if(params != null){
        if($("#vendorRules").hasClass('VendorIsClicked')){
            comd = '';
            subComd = '';            
        } else {
            if($.trim(params.parentNodeValue) =='#'){
                comd = params.childNodeValue;
                subComd = '';
            }else{
                comd = params.parentNodeValue;
                subComd = params.childNodeValue;
                subComd = subComd.split('id')[1];
            }
        }        
    }
    

    log("vendorId : " +vendorId);
    log("commodityID : " +comd);
    log("sub : " +subComd);
    
    $.ajax({
        type: 'POST',
        url: appName + "/ajax/assortment-loadRule",
        cache: false,
        async: false,        
        data: {
            vendorID : vendorId,
            commodityID : comd,
            subCommodityID : subComd            
        },
        success: function (data) {
            hideLstUserAndTsUpdate();            
            data = JSON.parse(data);            
            disableAllRulesBeforeGetRuleData();                

            onlyVend = data.onlyVend;
            if(onlyVend == true) {
                //console.log("vendor!!!!!!!!!!!!");
                var vendorId = data.vendorRules[0].vendorId;
                vendorRules = data.vendorRules;
                vendorAttrs = data.vendorAttrs;
                if(vendorAttrs.length>0){
                    generateAttrsForVendor(vendorAttrs);                        
                }
                if(vendorRules.length>0){
                    //console.log("vendorId : ",vendorId);                
                    if(vendorId !== null && vendorId !== ''){
                        generateRuleDataForVendor(vendorRules);
                    } else {                        
                        generateInheritRuleDataForVendor(vendorRules);
                    }
                    /*if($('.jstree-checked').length>0){ 
                         
                    }*/
                    turnAllRule(vendorRules);                   
                } else {
                    clearTooltipForValidationDiv("maginEnd");
                    clearTooltipForValidationDiv("maginStart");
                    clearTooltipForValidationDiv("retailEnd");
                    clearTooltipForValidationDiv("retailStart");                       
                }
            } else {
                if(comd != '') {            
                    if(subComd == ''){
                        //console.log(" for Commodity");
                        var lstRules = data.lstRules;                            
                        if(lstRules.length >0){
                           generateRuleDataForCommodity(lstRules);
                        }

                    } else {
                        //console.log(" sub Commodity");
                        var lstRules = data.lstRules;                        
                        if(lstRules.length>0){
                           generateRuleDataForSubCommodity(lstRules);
                        }
                    }
                }
            }            
        },
        error: function (xhr, ajaxOptions, error) {                    
            alert('Error: ' + xhr.responseText);
        }
    });

}
function log(message){
    //console.log(message);
}
/**
 * Disable All Rules before getting Rule's Data.
 * @author Duyen.le
 */
function disableAllRulesBeforeGetRuleData(){
    $.each($('.input-between'), function (index){
        $(this).val("");
    });
    $("input[id^='attr-']").each(function(index){
        $(this).val("");
    });
    $.each($('input:checkbox[name^="chk-"]'),function (index, dInData) {                        
        if($(dInData).attr('checked')){                         
            $(dInData).attr('checked', false);      
            $(dInData).iButton().trigger("change");
        }
    }); 
    $('#turnOnAllRule').attr('checked', false);      
    $('#turnOnAllRule').iButton().trigger("change");
}


function generateAttrsForVendor(attrsRules) {   
    $.each(vendorAttrs, function(index, dInData){
        $("#formHie input[id^='attr-']").each(function(index,data){
            var id = $(this).attr("id").split("-");            
            if(dInData.id == id[1]){
                var val = dInData.value; 
                if(val=='Y'){
                    $(this).attr('checked',true);                    
                    $(this).iButton().trigger("change");
                }
                if(val=='N'){
                    $(this).attr('checked',false);                    
                    $(this).iButton().trigger("change");
                } else {
                    $(this).val(dInData.value);
                }
            }
        });
    });
}
 /**
 * Disable YES/NO checkbox if the Rule is OFF.
 * @author Duyen.le
 */
function disableCheckboxOFFStatus(){
    $.each($('input:checkbox[name^="chk"]'),function (index, dInData) {                
        if(!$(dInData).attr('checked')){
            if($(dInData).attr("data-left-check") !=undefined){
                rightCheck = $(dInData).attr("data-left-check");
                var checkbox_Data = $("input[data-right-check="+rightCheck+"]");
                
                checkbox_Data.iButton('disable', true);
                checkbox_Data.prop('disabled', true);
            }                           
        }
   });  
}

function generateRuleDataForVendor(vendorRules) {
    $.each(vendorRules,function (index, dInData) {
        var lstValues = dInData.values;
       
        var lstSeqNbr = dInData.lstSeqNbrs;
        var values= [];
        if(dInData.activeSw=='Y'){
            $('input:checkbox[name=chk-'+dInData.id+']').attr('checked',true);                      
            $('input:checkbox[name=chk-'+dInData.id+']').iButton().trigger("change");
            if(lstValues.length > 1){
                if(lstSeqNbr!==null){                    
                    $.each(lstSeqNbr,function (index, seqNbr) {                                                                     
                        if(seqNbr=='1'){
                            if(dInData.id==2){
                                $("#retailStart").val(precise_roundFloatNum(lstValues[1]));
                                values.push("Start:"+precise_roundFloatNum(lstValues[1]));      
                            }
                            if(dInData.id==3){
                                $("#maginStart").val(precise_roundFloatNum(lstValues[0]));
                                values.push("Start:"+precise_roundFloatNum(lstValues[0]));   
                            }
                        }
                        if(seqNbr=='2'){                                            
                            if(dInData.id==2){
                                $("#retailEnd").val(precise_roundFloatNum(lstValues[0]));
                                values.push("End:"+precise_roundFloatNum(lstValues[0]));
                            }
                            if(dInData.id==3){
                                $("#maginEnd").val(precise_roundFloatNum(lstValues[1]));
                                values.push("End:"+precise_roundFloatNum(lstValues[1]));
                            }
                        }
                    // End block code [2]                                   
                });
                }
                if(dInData.id==3){
                    values = swap2ElementInArray(values);
                }
                                           
            } else {
                if(dInData.id ==='10'){                                         
                    $("#rule10").val(parseInt(lstValues));
                    values.push(parseInt(lstValues[0]));
                } else {
                    values.push('');
                }
                if(lstValues =='Y'){        // Rule have value 'TRUE'                   
                    $('input:checkbox[name=chk-'+dInData.id+'-value]').attr('checked',true);                                
                    $('input:checkbox[name=chk-'+dInData.id+'-value]').iButton().trigger("change");
                }
            }
            var RuleCheck = {
                    id: dInData.id,
                    values: values,
                    activeSw: dInData.activeSw
                }
        } else {
            if(dInData.id === 2 || dInData.id ===3){                                    
                values.push("Start:"+0);    
                values.push("End:"+0);  
            } else if(dInData.id === 10){
                values.push(0);
            } else {
                values.push('');
            }
            var RuleCheck = {
                id: dInData.id,
                values: values,
                activeSw: dInData.activeSw
            }
        }
        saveChangeArrAfterLoad.push(RuleCheck);
    });
    showLstUserAndTsUpdate(vendorRules);
}
function generateInheritRuleDataForVendor(vendorRules) {
    $.each(vendorRules,function (index, dInData) {
        showDataInheritRuleOnUi(dInData);
    });
    turnAllRule(vendorRules);        
}
function generateRuleDataForCommodity(lstRules){

    $.each(lstRules,function (index, dInData) {                               
        var hebComdId = dInData.HebComdId;
        var vendorId = dInData.vendorId;
        var subDeptId = dInData.subDeptId;       
        var lstSeqNbr = dInData.seqNbr;
        log("hebComdId : " + hebComdId);
        if(hebComdId != null && hebComdId > 0){
            showDataRuleOnUI(dInData);            
            showLstUserAndTsUpdate(lstRules);            
        } else {                      
            showDataInheritRuleOnUi(dInData);            
        }        
    });
    turnAllRule(lstRules);     
}
function generateRuleDataForSubCommodity(lstRules){   
    $.each(lstRules,function (index, dInData) {                               
        var subComdId = dInData.subComdId;
        var lstValues = dInData.values;
        var lstSeqNbr = dInData.seqNbr;
        if(subComdId != null && subComdId > 0){
            showDataRuleOnUI(dInData);            
            showLstUserAndTsUpdate(lstRules);
        } else {            
            showDataInheritRuleOnUi(dInData);
        }        
    });
    turnAllRule(lstRules);
}
function showDataInheritRuleOnUi(dInData){
    var values= [];
    var lstValues = dInData.values;
    var lstSeqNbr = dInData.seqNbr;

    if(dInData.activeSw=='Y'){
        $('input:checkbox[name=chk-'+dInData.id+']').attr('checked',true);                      
        $('input:checkbox[name=chk-'+dInData.id+']').iButton().trigger("change");
        if(lstValues.length > 1){
            // 2.Rule for Retail Between and Margin Between.
            $.each(lstSeqNbr,function (index, seqNbr) {                                                                     
                    if(seqNbr=='1'){
                        if(dInData.id==2){
                            $("#retailStart").val(precise_roundFloatNum(lstValues[0]));
                            values.push("Start:"+precise_roundFloatNum(lstValues[0])); 
                        }
                        if(dInData.id==3){
                            $("#maginStart").val(precise_roundFloatNum(lstValues[0]));
                            values.push("Start:"+precise_roundFloatNum(lstValues[0])); 
                        }
                    }
                    if(seqNbr=='2'){                                            
                        if(dInData.id==2){
                            $("#retailEnd").val(precise_roundFloatNum(lstValues[1]));
                            values.push("End:"+precise_roundFloatNum(lstValues[1]));
                        }
                        if(dInData.id==3){
                            $("#maginEnd").val(precise_roundFloatNum(lstValues[1]));
                            values.push("End:"+precise_roundFloatNum(lstValues[1]));   
                        }
                    }
                // End block code [2]                                   
            });
            values = swap2ElementInArray(values);            
        } else {            
                if(dInData.id ==='10'){                                                                             
                    $("#rule10").val(parseInt(lstValues));
                    values.push(parseInt(lstValues[0]));
                } else {
                    values.push('');
                }
            }
            
            if(lstValues =='Y'){                
                $('input:checkbox[name=chk-'+dInData.id+'-value]').attr('checked',true);                                
                $('input:checkbox[name=chk-'+dInData.id+'-value]').iButton().trigger("change");
            }
        var RuleCheck = {
                    id: dInData.id,
                    values: values,
                    activeSw: dInData.activeSw
                }
        
        // 3.Indicate isInherit status of Rule                      
        $('input:checkbox[name=chk-'+dInData.id+']').next().removeClass("ibutton-handle").addClass("ibutton-handle-checkmark");
        $('input:checkbox[name=chk-'+dInData.id+']').parent().removeClass("ibutton-container-on").addClass("ibutton-container-checkmark");
        inheritRuleArr.push(RuleCheck);
        inheritRuleArr2 = inheritRuleArr;        
        
        // 4.Add class "isInherit" to mark that the input is inherit.       
    } else {
        //dInData.activeSw=='N'
        offFirstLoadArr.push(dInData.id);
    }
    
}

function showDataRuleOnUI(dInData){
    var lstValues = dInData.values;
    var lstSeqNbr = dInData.seqNbr;
    var values= [];
    if(dInData.activeSw=='Y'){
        $('input:checkbox[name=chk-'+dInData.id+']').attr('checked',true);                      
        $('input:checkbox[name=chk-'+dInData.id+']').iButton().trigger("change");
        if(lstValues.length > 1){
            $.each(lstSeqNbr,function (index, seqNbr) {                                                                     
                    if(seqNbr==1){
                        if(dInData.id==2){
                            $("#retailStart").val(precise_roundFloatNum(lstValues[0]));    
                            values.push("Start:"+precise_roundFloatNum(lstValues[0]));     
                        }
                        if(dInData.id==3){
                            $("#maginStart").val(precise_roundFloatNum(lstValues[0]));       
                            values.push("Start:"+precise_roundFloatNum(lstValues[0]));                                               
                        }
                        
                    }
                    if(seqNbr==2){                                      
                        if(dInData.id==2){
                            $("#retailEnd").val(precise_roundFloatNum(lstValues[1]));
                            values.push("End:"+precise_roundFloatNum(lstValues[1]));
                        }
                        if(dInData.id==3){
                            $("#maginEnd").val(precise_roundFloatNum(lstValues[1]));
                            values.push("End:"+precise_roundFloatNum(lstValues[1]));
                        }
                        
                    }
                // End block code [2]                                   
            });
            values = swap2ElementInArray(values);                        
        } else {
            if(dInData.id ==='10'){                                         
                 $("#rule10").val(parseInt(lstValues));
                 values.push(parseInt(lstValues[0]));
            } else {
                values.push('');
            }
        }
        var RuleCheck = {
                id: dInData.id,
                values: values,
                activeSw: dInData.activeSw
            }
    } else {
        if(dInData.id === 2 || dInData.id ===3){
            values.push("Start:"+0);    
            values.push("End:"+0);  
        } else if(dInData.id === 10){
            values.push(0);
        } else {
            values.push('');
        }
        var RuleCheck = {
            id: dInData.id,
            values: values,
            activeSw: dInData.activeSw
        }
    }
    saveChangeArrAfterLoad.push(RuleCheck);
}

function disableRuleTextBoxwhenInit(){
    $("input[name^='chk-']").each(function(index){
        if ($(this).is(":checked")) {// CHECKED                  
                    $(this).parent().addClass('ibutton-container-on').removeClass("ibutton-container-checkmark");
                    // Enable [Handle checkbox YES/NO] when ON Rule
                    if (typeof $(this).attr("data-left-check") !== 'undefined') {
                        var index_check = $(this).attr("data-left-check");                        
                        var enableNext = $("input[data-right-check="+index_check+"]");  
                            
                        enableNext.iButton('disable', false);
                        enableNext.prop('disabled', false);
                    }
                    //
                }else {     // UNCHECKED            
                    $(this).parent().removeClass('ibutton-container-on').removeClass("ibutton-container-checkmark");
                    // Disable [Handle checkbox YES/NO] when OFF Rule
                    if (typeof $(this).attr("data-left-check") !== 'undefined') {
                        var index_check = $(this).attr("data-left-check");                        
                        var enableNext = $("input[data-right-check="+index_check+"]");  
                            
                        enableNext.iButton('disable', true);    
                        enableNext.iButton('toggle', false);
                        enableNext.prop('disabled', true);                                  
                    }
                    //
                }
    });
}

function resetToHigherLevel(vendorId,comd,subComd){    
    $.ajax({
        type: 'POST',
        url: appName + "/ajax/resetRule",
        cache: false,
        async: false,
        data : {
            vendorId:vendorId,
            commodityId : comd,
            subCommdId : subComd
        },
        success: function (data) {
            if(data=='true'){
                showAlertSuccess('Reset successfully!');
                onArr = [];
                offArr = [];
                offFirstLoadArr = [];
                inheritRuleArr2 = [];
                removeAlertSuccess(3000);                
                // set flag to Node ------------------
                
                if(vendorId!=='') {                     
                     if((comd !== '' && comd !== undefined)|| (subComd !== '' && subComd !== undefined)) {
                        var node = $('#jstreeCommo').jstree("get_selected");
                        var nodeData = $('#jstreeCommo').jstree(true).get_node(node);
                        var parentOfSelectedNode =  nodeData.parent;
                         nodeData.a_attr.class= "";
                        $('#jstreeCommo').jstree(true).redraw_node(node,true);

                        if(parentOfSelectedNode !=='#'){
                            $("#jstreeCommo .jstree-container-ul li#"+parentOfSelectedNode+" .jstree-children li .jstree-anchor").each(function(d, dt){                
                                var str = $(this).html();
                                var x = str.split('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>')[1];                           
                                if (x.length > 20) {
                                    var decoded = $('<div/>').html(x).text();
                                    $(this).attr('title',decoded);
                                    //$(this).addClass('text-ellipsis-Rule');                                  
                                    $(this).html('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>'+ x.substr(0,15) + '...');   
                                }
                            });
                        } else {
                            if (nodeData.text.length > 23) {                              
                                $("#jstreeCommo .jstree-container-ul li#"+node+" .jstree-anchor").attr('title',nodeData.text);  
                                $("#jstreeCommo .jstree-container-ul li#"+node+" .jstree-anchor").html('<i class="jstree-icon jstree-checkbox"></i><i class="jstree-icon jstree-themeicon"></i>'+ nodeData.text.substr(0,20) + '...');                                         
                            }
                        }                         
                    }
                } else{
                    saveChangeArrAfterLoadAssignForVendor = [];
                }           
                //////////////////////////////////                
                load_RulesSet(TreeNode);
                saveChangeArrAssignAfterSave = [];
                saveChangeArrAfterLoadAssign = [];
                
                $("#assortment-save-btn").prop("disabled", true);
                $("#reset-toDefault-btn").prop("disabled", true);
                setChangeStatus(false);
            } else {                    
                showAlertDanger('The system has encountered an error. Please try again later!');
                removeAlertSuccess(3000);
            }                 
        },
        error: function (xhr, ajaxOptions, error) {                    
            alert('Error: ' + xhr.responseText);
        }
    });
    
}

function turnAllRule (lstData){    
    var countChk = 0;
    $.each($('input:checkbox[name^="chk-"]'),function (index, dInData) {                        
        if($(dInData).attr('checked')){                         
           countChk ++;
        }
    }); 
    if(countChk == 9) {
        $('#turnOnAllRule').attr('checked', true);      
        $('#turnOnAllRule').iButton().trigger("change");
    }
    return countChk;
}
function isNum(num){
    return !isNaN(num) && num<4000000000;
}

/**
 * Compare 2 Array.
 * @author Duyen.le
 */

function compare2Arr(Arr1, Arr2){
   /* $.each(Arr1, function(d, dt){
        
    })  ;
    
    $.each(Arr2, function(d, dt){
    
    })  ;*/
    var result = [];
    if(Arr1.length > 0 && Arr2.length > 0){
        $.each(Arr1, function(d, data1){
            if(data1.activeSw || data1.values || data1.id)  {
                $.each(Arr2, function(d, data2){                
                        if(data1.id === data2.id){
                            
                                if(data1.activeSw === data2.activeSw && data1.activeSw ==='Y'){
                                    var values1 = data1.values;
                                    var values2 = data2.values;
                                    if(values1.length > 1 && values2.length > 1){
                                        if(values1[0] !== values2[0] || values1[1] !== values2[1]) {
                                            result.push(data1.id);
                                        }
                                    } else {
                                        if(values1[0] !== values2[0]){
                                            result.push(data1.id);
                                        }
                                    }                                   
                                } else if (data1.activeSw !== data2.activeSw){
                                    result.push(data1.id);
                                }
                            
                        }           
                });
            } else {            
                if(data1 != undefined && $.inArray(data1, Arr2) === -1){
                    result.push(data1); 
                }           
            }
        });
    }
    
    //console.log(result);
    return result;
}
/**
 * Swap Array's element.
 * @author Duyen.le
 */
function swap2ElementInArray(arr) {
    return [arr[1], arr[0]];
}
/**
 * Show tree.
 * @author Duyen.le
 */
function showTree(){

}

function clearAllRule(){
    $("#formHie input[name^='chk-']").attr('checked',false);                                
    $("#formHie input[name^='chk-']").iButton().trigger("change");
    $("#contentColRight").css('display','none');
}
function getVendorId(){
    var vendor = $("#vendor-input option:selected").text();
    var vendorId = 12765;
    vendorNbr = vendor.match(/\d+/);
    if(vendorNbr === null) {            
        vendorId = '';
    } else {
        vendorId = vendorNbr[0];
    }
    return vendorId;
}

function removeAlertSuccess(time) {     
    setTimeout(function(){
    $("#alert-msg").css('display','none');
    $("#message").html('');
    }, time);
}

function showAlertDanger(errorMessage) {
     //remove class
    
        $("#alert-msg").css('display','');
        $("#alert-msg").find('.alert').removeClass('alert-success');
         //add class
         $("#alert-msg").find('.alert').addClass('alert-danger');
         //set message
         $("#message").html(errorMessage);
    
}

function showAlertSuccess(errorMessage) {
    
     $("#alert-msg").css('display','');
     $("#alert-msg").find('.alert').addClass('alert-success');
     //add class
     $("#alert-msg").find('.alert').removeClass('alert-danger');     
     //set message
     $("#message").html(errorMessage);
    
}
function checkRuleChanged(){    
    changesRuleArr = getRuleChanges();
    if(saveChangeArrAssignAfterSave.length > 0){
        var changedArr = compare2Arr(saveChangeArrAssignAfterSave,changesRuleArr);
        if(changedArr.length > 0){
            setChangeStatus(true);
        } else {
            setChangeStatus(false);
        }
    } else {
        if(!turnAllIsON &&  select_node){
            changesRuleArr = [];
        }
        var changesArr = compare2Arr(changesRuleArr, saveChangeArrAfterLoadAssign); 
                         
        if(saveChangeArrAfterLoadAssign.length<=0){
            var off = true;
            $.each(changesRuleArr, function(d, data){               
                if(data.activeSw==='Y'){    
                    if($("#formHie").find(".ibutton-container-checkmark").length > 0){
                        if($("#formHie").find(".ibutton-container-on").length > 0 && $("#formHie").find(".ibutton-container-on>input").attr('id')!=='turnOnAllRule'){
                            off = false;
                        } else {
                            off = true;
                        }
                    } else {
                        off = false;
                    }
                }
            });
            if(off){
                setChangeStatus(false);
            } else{
                setChangeStatus(true);
            }
            if(inheritRuleArr2.length>0){
                var OnRuleArr = getRuleON();                                            
                if(OnRuleArr.length>0){
                    if(inheritRuleArr2.length !==OnRuleArr.length){
                        setChangeStatus(true);
                    } else {
                        var compareRule = compare2Arr(OnRuleArr, inheritRuleArr2);
                        if(compareRule.length>0){
                            setChangeStatus(true);
                        }
                    }                                               
                } else {
                    var OffRuleArr = getRuleOff();
                    if(OffRuleArr.length>0){
                        if(inheritRuleArr2.length !==OnRuleArr.length){
                            setChangeStatus(true);
                        } else {
                            var compareRule = compare2Arr(OffRuleArr, inheritRuleArr2);
                            if(compareRule.length>0){
                                setChangeStatus(true);
                            }
                        }
                    }                                               
                }                                                                                                                                   
            }  
        } else if(changesArr.length>0) {         
            setChangeStatus(true);
        } else {
            setChangeStatus(false);
        }
    }      
}

function openDialogConfirmWhenSave(callbackFn){ 
    bootbox.dialog({
        title : "Confirmation",
        message: msgSaveConfirm,
        buttons: {
            main: {
              label: "Yes",
              className: "btn-primary",
              callback: function() {                
                callbackFn(true);
              }
            },         
            cancel: {
              label: "No",
              className: "btn-default",
              callback: function() {
                callbackFn(false);
              }
            }                  
        }       
    });
}
function callbackConfirmSave(AttrAndRuleVendorVO){
    openDialogConfirmWhenSave(function(result){
        if(result){
            saveRule(AttrAndRuleVendorVO);
            saveChangeArrAfterSave = AttrAndRuleVendorVO.lstVendorRules;
            saveChangeArrAssignAfterSave = saveChangeArrAfterSave;

            
            $("#assortment-save-btn").prop("disabled", true);
            $("#reset-toDefault-btn").prop("disabled", false);
        }
    }); 
}
function disableSaveButton(saveChangeArrAfterLoadAssign){
    $("#assortment-save-btn").prop("disabled", true);    
    if(saveChangeArrAfterLoadAssign.length > 0 || saveChangeArrAfterLoadAssignForVendor.length>0){
        $("#reset-toDefault-btn").prop("disabled", false);
    }
}
function checkChangeRule(onArr){ 

    var changesRuleArr = getRuleChanges();
    if(saveChangeArrAssignAfterSave.length > 0){  
        var changedArr = compare2Arr(saveChangeArrAssignAfterSave,changesRuleArr);
        if(changedArr.length > 0){ 
            setChangeStatus(true);
            $("#assortment-save-btn").prop("disabled", false);
        } else {
            setChangeStatus(false);
            $("#assortment-save-btn").prop("disabled", true);
        }
    } else {        
        if($("#vendorRules").hasClass('VendorIsClicked')){
            if(saveChangeArrAfterLoadAssignForVendor.length <=0){
                checkChangeFlagNoAnyRules();
            } else {
                var changesArr = compare2Arr(changesRuleArr, saveChangeArrAfterLoadAssignForVendor);
                if(changesArr.length > 0){
                    $("#assortment-save-btn").prop("disabled", false);
                    setChangeStatus(true);
                } else {
                    $("#assortment-save-btn").prop("disabled", true);
                    setChangeStatus(false);
                }
            }
        } else {
            var changesArr = compare2Arr(changesRuleArr, saveChangeArrAfterLoadAssign);
           if(saveChangeArrAfterLoadAssign.length<=0){
                checkChangeFlagNoAnyRules();
            } else if(changesArr.length>0) {
                $("#assortment-save-btn").prop("disabled", false);
                setChangeStatus(true);
            } else {
                $("#assortment-save-btn").prop("disabled", true);
                setChangeStatus(false);
            } 
        }        
    }
}
function showLstUserAndTsUpdate (lstData) {    
    $("#showUserAndTimeUpdate").css('display','');
    $("#uidAndTsUpdate").html("Last Updated By <label style='padding-right:20px; font-weight: normal'>"+lstData[0].lstUpdateUid+"</label>" + " On <label style='font-weight: normal'>" + lstData[0].lstUpdateTs+"</label>");    
}
function hideLstUserAndTsUpdate () {
    $("#showUserAndTimeUpdate").css('display','none');
}

function getRuleON() {
    var listRules = [];            
    var uniqueNames = [];
    
    var rule;
    var ruleId;             
    var activeSw = 'Y';
    $("#formHie input[name='chk-4'],input[name='chk-2'],input[name='chk-5'],input[name='chk-6'],input[name='chk-7'],input[name='chk-8'],input[name='chk-9'],input[name='chk-1'],input[name='chk-3'], input[name='chk-10']").each(function(index,data){
        
        var values = [];                
        ruleId = $(this).attr("name").split("chk-");
        if($(this).attr('checked')){
            if (typeof $(this).attr("data-left-check") !== 'undefined') {
                var index_checkbox = $(this).attr("data-left-check");
                                                
                var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");
                activeSw = 'Y';
                if(checkbox_Data.length > 1){                                         
                    $.each(checkbox_Data,function (index, dInData) {                                
                        if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){                                                                                               
                            values.push("Start:" +$(dInData).val());                            
                        }   
                        if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){                                        
                            values.push("End:" +$(dInData).val());                              
                        }                                                           
                    });
                }else{                            
                    if($(this).attr("name")==='chk-10'){
                        values.push(parseInt($("#rule10").val()));
                    } else {                        
                        values.push('');
                    }
                }
                rule = {                    
                    id: ruleId[1],
                    values:values,
                    activeSw : activeSw                      
                }
            }
        }
        listRules.push(rule);
        uniqueNames = [];
        $.each(listRules, function(i, el){
            if(el != undefined && $.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
        });
    });
    
    return uniqueNames;
}

function getRuleOff(){
    var listRules = [];
    var uniqueNames = [];
    var rule;
    var ruleId; 
    var activeSw = 'N'; 
    $("#formHie input[name^='chk-']").each(function(index,data){
        var values = [];
        ruleId = $(this).attr("name").split("chk-");
        if($(this).attr('checked') === undefined){
                if (typeof $(this).attr("data-left-check") !== 'undefined') {
                var index_checkbox = $(this).attr("data-left-check");                                                       
                var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");                      
                if(checkbox_Data.length > 1){
                    $.each(checkbox_Data,function (index, dInData) {                                
                        if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){
                            values.push("End:" +0);                    
                        }
                        if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){
                            values.push("Start:" +0);
                        }   
                    });                     
                }else{                              
                    if($(this).attr("name")==='chk-10'){                        
                        values.push(0);
                    } else {                        
                        values.push('');
                    }                       
                }
                rule = {                            
                    id: ruleId[1],
                    values: values,
                    activeSw : activeSw                         
                }
            }
            listRules.push(rule);
            uniqueNames = [];
            $.each(listRules, function(i, el){
                if(el != undefined && $.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
            });
        }   
    });
    return uniqueNames;
}
function getRuleWhenTurnAll(on){
    var listRules = [];
    if(on){ 
        var activeSw = 'Y'; 
        listRules = forEachRule(activeSw);
    } else {
        var activeSw = 'N'; 
        listRules = forEachRule(activeSw);
    }
    return listRules;
}

function forEachRule(activeSw){     
    var rule;
    var ruleId;
    var listRules = [];
    $("#formHie input[name^='chk-']").each(function(index,data){        
        var values = [];
        ruleId = $(this).attr("name").split("chk-");
            if (typeof $(this).attr("data-left-check") !== 'undefined') {
            var index_checkbox = $(this).attr("data-left-check");                                                       
            var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");                      
            if(checkbox_Data.length > 1){
                $.each(checkbox_Data,function (index, dInData) {                                
                    if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){
                        values.push("End:" +0);                    
                    }
                    if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){
                        values.push("Start:" +0);
                    }   
                });                     
            }else{                              
                if($(this).attr("name")==='chk-10'){                        
                    values.push(0);
                } else {                        
                    values.push('');
                }                       
            }
            rule = {                            
                id: ruleId[1],
                values: values,
                activeSw : activeSw                         
            }
        }
        listRules.push(rule);               
            
    });
    return listRules;
}

function checkChangeFlagNoAnyRules(){    
    var inheritRuleArrLocal = [];
    var inheritFlagArr = [];
    $("#formHie input[name^='chk-']").each(function(d,dt){
        if($(dt).parent().hasClass('ibutton-container-checkmark')){
            inheritFlagArr.push($(dt).attr('name').split('-')[1]);
        }
    }); 
     if(onArr.length > 0 ){
        $.each(onArr, function(d, id){
            var idexOfItem = inheritFlagArr.indexOf(id);
            if(idexOfItem > -1){
                inheritFlagArr.splice(idexOfItem,1);    
            }            
        });
     }
     if(offArr.length > 0){
        $.each(offArr, function(d, id){
            var idexOfItem = inheritFlagArr.indexOf(id);
            if(idexOfItem > -1){
                inheritFlagArr.splice(idexOfItem,1);    
            }
        });
     }
     $.each(inheritRuleArr2, function(d, dt){
        inheritRuleArrLocal.push(dt.id);
     });
   // console.log("test : ", onArr, offArr, inheritRuleArrLocal , inheritFlagArr, offFirstLoadArr, saveChangeArrAfterLoadAssignForVendor);
    
        
    if($("#vendorRules").hasClass('VendorIsClicked')){
        if(onArr.length > 0){
            $("#assortment-save-btn").prop("disabled", false);
            setChangeStatus(true);
        } else {
            $("#assortment-save-btn").prop("disabled", true);
            setChangeStatus(false);
        }
    } else {
        var compareChangeInher = compare2Arr(inheritRuleArrLocal,inheritFlagArr);
        if(compareChangeInher.length>0){
            $("#assortment-save-btn").prop("disabled", false);
            setChangeStatus(true);
        } else {
            if(onArr.length>0){
                $("#assortment-save-btn").prop("disabled", false);
                setChangeStatus(true);
            } else {
                $("#assortment-save-btn").prop("disabled", true);
                setChangeStatus(false);
            }
            
        }
    }
}