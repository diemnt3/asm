/************************************************************************************************************
 * PUBLIC VALUE
 ************************************************************************************************************/
var txtSubCommo_current_selector;
var vendor;
var selectedLabelBefore = "";
var selectedIdBefore = "";
var dataAutoCompleteInput = [];
var dataAutoCompleteInputValue = [];
var dataAutoCompleteInputLabel = [];
var isFoundVendor = false;
var listResult = [];
var listResultRest = [];
var listShow = [];
var listChange = [];
var currentVendorId ="";
/************************************************************************************************************
 * END PUBLIC VALUE
 ************************************************************************************************************/
$(document).ready(function() {
	$("#vendor-search-txt").val("");
	$("#vendor-input").val("");
/************************************************************************************************************
 * SEARCH VENDOR
 ************************************************************************************************************/
	//$("#vendor-search-txt").focus();
	//getDataForAutocomplete("vendor-search-txt");
	//autoCompleteInitVendorManagement("vendor-search-txt", "/vendor-management/search-for-vendors");
	//clickDropAutoCompleteVendorManagement();
  
/************************************************************************************************************
 * END VENDOR
 ************************************************************************************************************/		
		
/************************************************************************************************************
 * DATATABLE
 ************************************************************************************************************/
	$(document).on('change', '#vendor-hierarchy-mapping-table .textbox-table', function() {
    	var currentTr = $(this).closest('tr');
    	var table = $("#vendor-hierarchy-mapping-table").DataTable();
    	if ($(this).val() === $(this).attr('value-db')) {
    		$(this).closest('tr').removeClass('success');
    		table.row($(currentTr)).data().statusChange = CONST.NO;
		} else{
			$(this).closest('tr').addClass('success');
			table.row($(currentTr)).data().statusChange = CONST.YES;
		}
 	});
//    $(document).on('keypress', '#vendor-search-txt', function(event) {
//       acceptAlphaNumberHasDashCharacter(this, event);
//    });
//    $(document).on('input change propertychange', '#vendor-search-txt', function(event) {
//       filterAlphaNumberHasDashCharacter(this);
//    });
	
	$(document).on('change', '.category1-cls', function() {
    	var id = $(this).closest('tr').attr('id');
    	$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + id)).data().category1Input = $.trim($(this).find('input').val());
 	});
	
	$(document).on('change', '.category2-cls', function() {
		var id = $(this).closest('tr').attr('id');
		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + id)).data().category2Input = $.trim($(this).find('input').val());
 	});
	
	$(document).on('change', '.category3-cls', function() {
		var id = $(this).closest('tr').attr('id');
		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + id)).data().category3Input = $.trim($(this).find('input').val());
 	});
	
	$(document).on('change', '.category4-cls', function() {
		var id = $(this).closest('tr').attr('id');
		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + id)).data().category4Input = $.trim($(this).find('input').val());
 	});
	
	$('#vendor-hierarchy-mapping-table').on('length.dt', function ( e, settings, len ) {
	    $("#vendor-hierarchy-mapping-table").DataTable().draw();
	} );
/************************************************************************************************************
 * END DATATABLE
 ************************************************************************************************************/				

/************************************************************************************************************
 * UPLOAD EXCEL
 ************************************************************************************************************/
//     $('#upload-Hierarchy-map-input').change(function() {
//    	 $('#form-upload').ajaxForm({
//             beforeSubmit: function(a,f,o) {
//             },
//             success: function(data) {
//            	var jsonObj = jQuery.parseJSON(data);
//            	if (jsonObj !== null) {
//            		$("#vendor-hierarchy-mapping-table").DataTable().clear();
//                	//add & draw
//                	var listCurrentData = jsonObj.data;
//     				for (idx in listCurrentData) {
//     					//debugger;
//     					$("#vendor-hierarchy-mapping-table").DataTable().row.add( {
//     						DT_RowId: listCurrentData[idx].DT_RowId,
//     						category1: listCurrentData[idx].category1,
//     						category2: listCurrentData[idx].category2,
//     						category3: listCurrentData[idx].category3,
//     						category4: listCurrentData[idx].category4,
//     						statusChange: listCurrentData[idx].statusChange,
//     						statusNew: listCurrentData[idx].statusNew,
//     						searchSubCommodity: listCurrentData[idx].searchSubCommodity,
//     						subCommodity: listCurrentData[idx].subCommodity,
//     						vendorId: listCurrentData[idx].vendorId
//     				    });
//     				}
//     				//$("#vendor-hierarchy-mapping-table").DataTable().column('').order('asc');
//     				$("#vendor-hierarchy-mapping-table").DataTable().draw(false);
//				}
//            	 var fileInput = $("#upload-Hierarchy-map-input");
//        		 fileInput.replaceWith( fileInput = fileInput.clone( true ) );
//             }
//         });
//    	 if (!checkNewOrChange()) {
//    		 $("#form-upload").submit();
//    		 var fileInput = $("#upload-Hierarchy-map-input");
//    		 //fileInput.replaceWith( fileInput = fileInput.clone( true ) );
//    	 } else {
//    		 $('#comfirm-modal').modal('show');
//    	 }
//     	//showLoadingTable();
//     });
//     
//     $("#ok-comfirm-btn").click(function(){
//    	 $("#form-upload").submit();
//     });
//     
//     $("#cancel-comfirm-btn").click(function(){
//    	 var fileInput = $("#upload-Hierarchy-map-input");
//    	 fileInput.replaceWith( fileInput = fileInput.clone( true ) );
//     });
     
     
/************************************************************************************************************
 * END UPLOAD EXCEL
 ************************************************************************************************************/
    
/************************************************************************************************************
 * DOWNLOAD TEMPLATE
 ************************************************************************************************************/
     
     /**
      * Download template
      * @author anhtran
      */
//     $("#download-template-btn").click(function(){
////     	var vendorId = $("#vendor-input").attr('vendor-inputid') === undefined ? '': $("#vendor-input").attr('vendor-inputid');
//// 		var numberRow = $('#invoice-list-table').DataTable().page.info().recordsTotal; //1001; //
//// 		var invoiceStatus = $.trim( $("#invoice-status").attr('value'));
//// 		var invoiceType = $.trim( $("#invoice-type").attr('value') );
// 		$("body").append('<div id="temp"></div>');
// 		$("#temp").append('<form target="_blank" id="frmDownloadTemplate" action="'+ appName +'/vendor-management/download-template-file" method="GET"></form>');
//// 		$("#frmDownloadTemplate").append('<input type="hidden" name="start" value="0">');
//// 		$("#frmDownloadTemplate").append('<input type="hidden" name="orderNbr" value="'+$("#order-no-input").val()+'">');
// 		$('#frmDownloadTemplate').submit();
//			$("#temp").remove();
//     });
     
/************************************************************************************************************
 * END DOWNLOAD TEMPLATE
 ************************************************************************************************************/
		
/************************************************************************************************************
 * HIERARCHY
 ************************************************************************************************************/
     $(document).on('click', '#vendor-hierarchy-mapping-table .fa-plus-square, #vendor-hierarchy-mapping-table .subCommodity-link', function() {
    	var trSelector = $(this).closest('tr');
    	txtSubCommo_current_selector = $(this).closest('tr').find(".subCommodity-cls a");
    	
        if (_.isEmpty($(trSelector).find(".category1-cls > input").val()) && $("#vendor-hierarchy-mapping-table").DataTable().row($(trSelector)).data().statusNew === "Y") {
        	$('#category-required-modal-sm').modal({
	        	  keyboard: false,
	        	  show:true
	        })
		} else {
			$('#subCommodityModal').modal({
	        	  keyboard: false,
	        	  show:true
	        	})
	         if (!$("#jstree").hasClass('jstree')) {
	        	 initHebHierarchy();
	         }
		}
    	
 	});
     
     $("#btn-search-commodity-modal").click(function() {
    	 $("#search-subCommodity-modal").attr('search-value', $("#search-subCommodity-modal").val());
    	 $("#jstree").jstree('close_all');
    	 $(".jstree-search").removeClass('jstree-search');
    	 removeAlertJsTree();
    	 var v = $.trim($('#search-subCommodity-modal').val());
    	 if(!_.isEmpty(v)) {
			$('#jstree').jstree(true).search(v);
 			$('#jstree').addClass('hidden');
 			$('#jstree').parent().block({ 
 				message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>'
 			});
		}else {
			$("#jstree").jstree("clear_search");
			$("#jstree").jstree('close_all');
			$(".jstree-node").css('display', '');
			//$("#show-top50-jstree").addClass('hidden');
		}
     });
     
     $('#search-subCommodity-modal').keypress(function(event) {
    	 var keycode = (event.keyCode ? event.keyCode : event.which);
    		if(keycode == '13'){
    			$(".jstree-search").removeClass('jstree-search');
    			$("#search-subCommodity-modal").attr('search-value', $("#search-subCommodity-modal").val());		
    			$("#jstree").jstree('close_all');
    			removeAlertJsTree();
    			event.preventDefault();
    			event.stopPropagation();
    			var v = $.trim($('#search-subCommodity-modal').val());
    			$(".jstree-node").css('display', '');
    			if(!_.isEmpty(v)) {
    				$('#jstree').jstree(true).search(v);
        			$('#jstree').addClass('hidden');
        			$(".ui-autocomplete").css('display', 'none');		
        			$('#jstree').parent().block({ 
        				message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>'
        			});
    			}else {
    				//$(".jstree-node").css('display', '');
    				$("#jstree").jstree("clear_search");
    				$("#jstree").jstree('close_all');
    			}
    		}
    		
     });

     $('#subCommodityModal').on('hidden.bs.modal', function(e) {
         $('#search-subCommodity-modal').val("");
         $(".jstree-node").css('display', '');
         $("#jstree").jstree("clear_search");
         $("#jstree").jstree('close_all');
         $('#jstree').jstree("deselect_all");
         //$("#show-top50-jstree").addClass('hidden');
         if (!_.isEmpty($("#message-alert-jstree").text())) {
        	 $("#jstree").jstree().destroy();
         }
         removeAlertJsTree();
         $("#search-subCommodity-modal").attr('search-value', '');
     });

     $('#subCommodityModal').on('shown.bs.modal', function(e) {
    	 $(document).unbind('ajaxSend');
    	 if (_.size($(".blockUI")) > 0) {
    		 $(".jstree-node").css('display', 'none');
         }
    	 //$("#search-subCommodity-modal").focus();
         if(txtSubCommo_current_selector !== undefined && $(txtSubCommo_current_selector).val() != "") {
             $("#jstree").jstree("search", $(txtSubCommo_current_selector).val());
             $('#search-subCommodity-modal').val($(txtSubCommo_current_selector).val());
         } else {
             $('#search-subCommodity-modal').val("");
             $("#jstree").find(".jstree-wholerow-clicked").removeClass('jstree-wholerow-clicked');
             $('#jstree').jstree('close_all');
         }
     });
     //END SEARCH------------------------

     $(".btn-ok-modal").on('click', function() {
        if (!_.isEmpty($('.jstree-clicked').find(".subCommodityDesc-cls").text()) && _.size($('.jstree-clicked')) !== 0) {
        	var table = $("#vendor-hierarchy-mapping-table").DataTable();
            var currentTr = $(txtSubCommo_current_selector).closest('tr');
            var listRemove = [];
            var dataRow = $("#vendor-hierarchy-mapping-table").DataTable().row($(currentTr)).data();
            var itemObj = {
        			DT_RowId: dataRow.DT_RowId
        	}
            
            
            $(txtSubCommo_current_selector).text($('.jstree-clicked').find(".subCommodityDesc-cls").text());
            if ($(txtSubCommo_current_selector).attr('value-db') !== $('.jstree-clicked').find(".subCommodityDesc-cls").text()) {
            	$(currentTr).addClass('success');
            	table.row($(currentTr)).data().statusChange = CONST.YES;
            	table.row($(currentTr)).data().subCommodity = $('.jstree-clicked').find(".subCommodityDesc-cls").text();
            	setChangeStatus(true);
            	if (!inArrays(listChange, itemObj)) {
            		listChange.push($("#vendor-hierarchy-mapping-table").DataTable().row($("#" + itemObj.DT_RowId)).data())
            	}
            } else {
            	setChangeStatus(false);
           	 	$(currentTr).removeClass('success');
           	 	table.row($(currentTr)).data().statusChange = CONST.NO;
	           	if (inArrays(listChange, itemObj)) {
	           		listRemove = removeInArrays(listChange, itemObj.DT_RowId);
	           		//listChange.push($("#vendor-hierarchy-mapping-table").DataTable().row($("#" + rowId)).data())
	           	}
           	 	listChange = listRemove;
            }
            table.row($(currentTr)).data().subCommodityCd = $('.jstree-clicked').find('.subCommodityCd-cls').text();
		}
        $('#subCommodityModal').modal('hide');
     });

     
     $("#vendor-input").change(function (){
    	if (isChanges()) {
	   		  bootbox.dialog({
	   		      title : "Confirmation",
	   		      message: "You have not saved your changes. Do you want to load new vendor?",
	   		      buttons: {
	   		        main: {
	   		            label: "Yes",
	   		            className: "btn-primary",
	   		            callback: function() {
		       			    	 removeAlertSuccess();
		       			    	 vendor = $("#vendor-input option:selected").val();
		       			         if(!(vendor === "E")) {
		       			        	 $("#table-wrapper").addClass('hidden');
		       			             $(".style-add-new-row").addClass('hidden');
		       			             showLoadingVendorSearch();
		       			             removeAlertVendor();
	                                 $(".vendorTxt").html('<b>'+vendor.toUpperCase()+'</b>');
		                             $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+'</b>');
		       			         }else {
		       			        	 clearDataTable();
		       			        	 $("#vendor-hierarchy-mapping-table").DataTable().destroy();	 
		       			        	 $(".tabs-style").addClass('hidden');
		       			         }
		       			         listChange = [];
		       			         checkAndSetChangeStatus();
	   		            }
	   		          },  
	   		          cancel: {
	   		            label: "No",
	   		            className: "btn-default",
	   		            callback: function() {
	   		            	  $("#vendor-input").val(currentVendorId);
	   		            }
	   		          }                           
	   		        }   
	   		    }); 
	   	 } else {
	       	 removeAlertSuccess();
	       	 vendor = $("#vendor-input option:selected").text();
	       	 if($("#vendor-input").val() !== "E") {
	       		 $("#table-wrapper").addClass('hidden');
	       		 $(".style-add-new-row").addClass('hidden');
	       		 showLoadingVendorSearch();
	       		 removeAlertVendor();
	       		 isFoundVendor = true;
	       		 $(".vendorTxt").html('<b>'+vendor.toUpperCase()+'</b>');
	       		 $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+'</b>');
	       	 }else {
	       		 clearDataTable();
	       		 $("#vendor-hierarchy-mapping-table").DataTable().destroy();	 
	       		 $(".tabs-style").addClass('hidden');
	       		 isFoundVendor = false;
	       	 }
	   	 }
     })
     
//     $(".btn-search-vendor-header").click(function() {  	 
//    	  if (isChanges()) {
//    		  bootbox.dialog({
//    		      title : "Confirmation",
//    		      message: "You have not saved your changes. Do you want to load new vendor?",
//    		      buttons: {
//    		        main: {
//    		            label: "Yes",
//    		            className: "btn-primary",
//    		            callback: function() {            
//    		            	 $("#vendor-search-txt").val($.trim($("#vendor-search-txt").val()));
//	       			    	 $(".ui-autocomplete").css('display', 'none');
//	       			    	 setChangeStatus(false);
//	       			    	 removeAlertSuccess();
//	       			    	 vendor = $("#vendor-search-txt").val();
//	       			         
//	       			         if(!_.isEmpty($("#vendor-search-txt").attr('vendor-search-txtid'))) {
//	       			        	 $("#vendor-search-txt").val($.trim($("#vendor-search-txt").val()));
//	       			        	 $("#table-wrapper").addClass('hidden');
//	       			             $(".style-add-new-row").addClass('hidden');
//	       			             showLoadingVendorSearch();
//	       			             removeAlertVendor();
//	                                isFoundVendor = true;
//	                                $(".vendorTxt").html('<b>'+vendor.toUpperCase()+'</b>');
//	                               $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+'</b>');
//	       			         }else {
//	       			        	 clearDataTable();
//	       			        	 $("#vendor-hierarchy-mapping-table").DataTable().destroy();	 
//	       			        	 $(".tabs-style").addClass('hidden');
//	       			        	 showAlertDangerVendor("Vendor not found");
//	                                isFoundVendor = false;
//	       			         }
//    		            }
//    		          },  
//    		          cancel: {
//    		            label: "No",
//    		            className: "btn-default",
//    		            callback: function() {
//    		            	  $("#vendor-search-txt").val($("#vendor-search-txt").attr('vendor-search-txttext-old'));
//    	    				  $("#vendor-search-txt").attr('vendor-search-txtid', $("#vendor-search-txt").attr('vendor-search-txtid-old'));
//    	    				  $("#vendor-search-txt").attr('vendor-search-txttext', $("#vendor-search-txt").attr('vendor-search-txttext-old'));
//    		            }
//    		          }                           
//    		        }   
//    		    }); 
//    	 } else {
//    		 $("#vendor-search-txt").val($.trim($("#vendor-search-txt").val()));
//        	 $(".ui-autocomplete").css('display', 'none');
//        	 setChangeStatus(false);
//        	 removeAlertSuccess();
//        	 vendor = $("#vendor-search-txt").val();
//             
//             if(!_.isEmpty($("#vendor-search-txt").attr('vendor-search-txtid'))) {
//            	 $("#vendor-search-txt").val($.trim($("#vendor-search-txt").val()));
//            	 $("#table-wrapper").addClass('hidden');
//                 $(".style-add-new-row").addClass('hidden');
//                 showLoadingVendorSearch();
//                 removeAlertVendor();
//                 isFoundVendor = true;
//                 $(".vendorTxt").html('<b>'+vendor.toUpperCase()+'</b>');
//                $(".vendorTxt1").html('<b>'+vendor.toUpperCase()+'</b>');
//             }else {
//            	 clearDataTable();
//            	 $("#vendor-hierarchy-mapping-table").DataTable().destroy();	 
//            	 $(".tabs-style").addClass('hidden');
//            	 showAlertDangerVendor("Vendor not found");
//                 isFoundVendor = false;
//             }
//    	 }
//     });
     
//     $('.txt-search-vendor-header').on("keypress", function(e) {
//        isFoundVendor = false;
//         if(e.keyCode == 13) {
//        	 $("#vendor-search-txt").autocomplete("close");
//        	 $("#vendor-search-txt").val($.trim($("#vendor-search-txt").val()));
//        	 
//        	 vendor = $.trim($("#vendor-search-txt").val());
//        	 if (isChanges()) {
//        		 bootbox.dialog({
//        		      title : "Confirmation",
//        		      message: "You have not saved your changes. Do you want to load new vendor?",
//        		      buttons: {
//        		        main: {
//        		            label: "Yes",
//        		            className: "btn-primary",
//        		            callback: function() {            
//        		            	 removeAlertSuccess();
//	           		             $(this).trigger('focusout');
//	           		             $(this).focus();
//	           		             setChangeStatus(false);
//	           		             if (!_.isEmpty($("#vendor-search-txt").attr('vendor-search-txtid'))) {
//	           		            	 $("#table-wrapper").addClass('hidden');
//	           		                 $(".style-add-new-row").addClass('hidden');
//	           		                 showLoadingVendorSearch();
//	           		                 removeAlertVendor();
//	                                    isFoundVendor = true;
//	           		             } else {
//	           		            	 clearDataTable();
//	           		            	 $("#vendor-hierarchy-mapping-table").DataTable().destroy();	 
//	           		            	 $(".tabs-style").addClass('hidden');
//	           		            	 showAlertDangerVendor("Vendor not found");
//	                                    isFoundVendor = false;
//	           		             }
//        		            }
//        		          },  
//        		          cancel: {
//        		            label: "No",
//        		            className: "btn-default",
//        		            callback: function() {
//        		            	$("#vendor-search-txt").val($("#vendor-search-txt").attr('vendor-search-txttext-old'));
//              				  	$("#vendor-search-txt").attr('vendor-search-txtid', $("#vendor-search-txt").attr('vendor-search-txtid-old'));
//              				  	$("#vendor-search-txt").attr('vendor-search-txttext', $("#vendor-search-txt").attr('vendor-search-txttext-old'));
//        		            }
//        		          }                           
//        		        }   
//        		    });  		 
//        	 } else {
//        		 removeAlertSuccess();
//                 $(this).trigger('focusout');
//                 $(this).focus();
//                 if (!_.isEmpty($("#vendor-search-txt").attr('vendor-search-txtid'))) {
//                	 $("#table-wrapper").addClass('hidden');
//                     $(".style-add-new-row").addClass('hidden');
//                     showLoadingVendorSearch();
//                     removeAlertVendor();
//                     isFoundVendor = true;
//                 } else {
//                	 clearDataTable();
//                	 $("#vendor-hierarchy-mapping-table").DataTable().destroy();	 
//                	 $(".tabs-style").addClass('hidden');
//                	 showAlertDangerVendor("Vendor not found");
//                     isFoundVendor = false;
//                 }
//        	 }
//         }
//     });
/************************************************************************************************************
 * END HIERARCHY
 ************************************************************************************************************/
     
/************************************************************************************************************
* ADD NEW ROW
************************************************************************************************************/
 $("#add-new-row-id").click(function(){
	//debugger;
	removeAlertSuccess();
	var listObj = getListObjFromDataTable();
	
	clearDataTable();
	var rowId = _.size(listObj) + 1;
	var newRow = 
	{
		DT_RowId: _.size(listObj) + 1,
		category1: "input",
		category2: "input",
		category3: "input",
		category4: "input",
		statusChange: CONST.NO,
		statusNew: CONST.YES,
		searchSubCommodity: CONST.EMPTY_STRING,
		subCommodity: CONST.EMPTY_STRING,
		vendorId: CONST.EMPTY_STRING
	};
	addRow(newRow);
	
	_.each(listObj, function(value, key){
		addRow(value);
	});
	
	if ($("#vendor-hierarchy-mapping-table").DataTable().page() > 0) {
		$("#vendor-hierarchy-mapping-table").DataTable().page(0);
	}
	
	$("#vendor-hierarchy-mapping-table").DataTable().draw(false);
	console.log("rowId: " + rowId);
	listChange.push($("#vendor-hierarchy-mapping-table").DataTable().row($("#" + rowId)).data())
	checkAndSetChangeStatus();
 });
 
 function getListObjFromDataTable() {
	 var listObj = [];
	 _.each($("#vendor-hierarchy-mapping-table").DataTable().data(), function(value, key){
		 if (value.statusNew === "Y") {
			 //-------------
			 value.catStr = "";
			 value.catStr += _.isEmpty(value.category1Input) ? "" : value.category1Input;
			 value.catStr += _.isEmpty(value.category2Input) ? ">" : ">" + value.category2Input;
			 value.catStr += _.isEmpty(value.category3Input) ? ">" : ">" + value.category3Input;
			 value.catStr += _.isEmpty(value.category4Input) ? ">" : ">" + value.category4Input;
			 value.catStr += _.isEmpty(value.subCommodity) ? ">" : ">" + value.subCommodity;
			 //-------------
		 }else {
			 value.catStr = value.category1 + ">" + value.category2 + ">" + value.category3 + ">" + value.category4
		 }
		 listObj.push(value);
	 });
	 return listObj;
 }
 
 function clearDataTable(){
	 $("#vendor-hierarchy-mapping-table").DataTable().clear().draw();
 }
 
 function addRow(data){
	 $("#vendor-hierarchy-mapping-table").DataTable().row.add( {
			DT_RowId: data.DT_RowId,
			category1: data.category1,
			category2: data.category2,
			category3: data.category3,
			category4: data.category4,
			statusChange: data.statusChange,
			statusNew: data.statusNew,
			searchSubCommodity: data.searchSubCommodity,
			subCommodity: data.subCommodity,
			vendorId: data.vendorId,
			category1Input: data.catStr === undefined ? "" : data.catStr.split(">")[0],
			category2Input: data.catStr === undefined ? "" : data.catStr.split(">")[1] === undefined ? "" : data.catStr.split(">")[1],
			category3Input: data.catStr === undefined ? "" : data.catStr.split(">")[2] === undefined ? "" : data.catStr.split(">")[2],
			category4Input: data.catStr === undefined ? "" : data.catStr.split(">")[3] === undefined ? "" : data.catStr.split(">")[3],
			lowestCatId: data.lowestCatId === undefined ? "" : data.lowestCatId,
			entyIdSubCommodity: data.entyIdSubCommodity === undefined ? "" : data.entyIdSubCommodity,
			category1InputError: data.category1InputError,
			category2InputError: data.category2InputError,
			category3InputError: data.category3InputError,
			category4InputError: data.category4InputError,
			subCommodityCd:data.subCommodityCd,
			categoryIds:data.categoryIds
	    });
 }
 
 
 
/************************************************************************************************************
* END ADD NEW ROW
************************************************************************************************************/

/************************************************************************************************************
* DELETE NEW ROW
************************************************************************************************************/
$(document).on('click', '.delete-cls', function() {
	//debugger;
	removeAlertSuccess();
	var dataRow = $("#vendor-hierarchy-mapping-table").DataTable().row($(this).closest('tr')).data();
	var list = [];
	var trId = dataRow.DT_RowId;
	var listRemove = [];
	var itemObj = {
			DT_RowId: dataRow.DT_RowId
	}
	
	bootbox.dialog({
	      title : "Confirmation",
	      message: "Do you wish to delete the selected row?",
	      buttons: {
	        main: {
	            label: "Yes",
	            className: "btn-primary",
	            callback: function() {
	            	//debugger;
	            	list.push(dataRow);
	            	if (dataRow.statusNew === "Y") {
	            		deleteRow(trId);		
	            		if (inArrays(listChange, itemObj)) {
	            			listRemove = removeInArrays(listChange, itemObj.DT_RowId);
	            			//var listUpdate = updateItems(listRemove, selectorTr);
	            		}
	            		listChange = listRemove;
	            		checkAndSetChangeStatus();
	            	} else if(dataRow.statusNew === "N") {
	            		$.ajax({
	            	        type: 'POST',
	            	        url: appName + "/vendor-management/delete-entyRlshp",
	            	        data:{
//	            	        	parntEntyId: dataRow.lowestCatId,
//	            	        	childEntyId: dataRow.entyIdSubCommodity,
	            	        	strJson: JSON.stringify(list),
	            	        	vendorId:$("#vendor-input").val()
	            	        }
	            		}).success(function(data) {
	            			var jsonData = $.parseJSON(data);
	            			if (!_.isEmpty(jsonData.errorMessage)) {
	            	    		showAlertDanger(jsonData.errorMessage);
	            			}else {
	            	    		showAlertSuccess("Delete successfully");
	            			}
	            			deleteRow(trId);
	            			if (inArrays(listChange, itemObj)) {
		            			listRemove = removeInArrays(listChange, itemObj.DT_RowId);
		            			//var listUpdate = updateItems(listRemove, selectorTr);
		            		}
	            			listChange = listRemove;
	            			checkAndSetChangeStatus();
	            	    }).done(function() {
	            	    }).fail(function() {
	            	    }).always(function() {
	            	    });	
	            	}
	            }
	          },  
	          cancel: {
	            label: "No",
	            className: "btn-default",
	            callback: function() {
	            	checkAndSetChangeStatus();
	            }
	          }                           
	        }   
	    });
});

function deleteRow(trId) {
	var listObj = getListObjFromDataTable();
	clearDataTable();
	_.each(listObj, function(value, key){
		if (parseInt(value.DT_RowId) !== parseInt(trId)) {
			addRow(value);
		}
	});
	
	if ($("#vendor-hierarchy-mapping-table").DataTable().page() > 0) {
		$("#vendor-hierarchy-mapping-table").DataTable().page(0);
	}
	$("#vendor-hierarchy-mapping-table").DataTable().draw(true);
}
/************************************************************************************************************
* END DELETE NEW ROW
************************************************************************************************************/
 
/************************************************************************************************************
* SAVE
************************************************************************************************************/
 $("#vendor-hierarchy-save-btn").click(function() {
	 saveVendorHierarchy();
 });
 function saveVendorHierarchy() {
	 $("#vendor-search-txt").val($("#vendor-search-txt").attr('vendor-search-txttext-old'));
	 $("#vendor-search-txt").attr('vendor-search-txtid', $("#vendor-search-txt").attr('vendor-search-txtid-old'));
	 $("#vendor-search-txt").attr('vendor-search-txttext', $("#vendor-search-txt").attr('vendor-search-txttext-old'));
	 
//	 var len = $("#vendor-hierarchy-mapping-table").DataTable().page.len();
//	 $("#vendor-hierarchy-mapping-table").DataTable().page.len(-1);
//	 var rowChange = _.size($(".success"));
	 
	 
	 setChangeStatus(true);
	 //debugger;
	 removeAlertSuccess();
	 var listSave = [];
	 var listMappingErrorRow = [];
	 var listExistsErrorRow = [];
	 var duplicateList = [];
	 var emptyRowList = [];
	 var category1RequiredList = [];
	 
	 var countEntyExists = 0;
		$("#vendor-hierarchy-mapping-table").DataTable().data().each(function(val, index) {
			if(val.statusChange == CONST.YES || val.statusNew == CONST.YES) {
				var statusNew = val.statusNew;
				var cats = "";
				if (statusNew == CONST.YES) {
					_.each($("#" + val.DT_RowId).find('td input'), function(val1, key, list) {
						if (key === 0) {
							cats += $.trim($(val1).val()) === "" ? "" : $.trim($(val1).val());
							//validate category1Required
							if (_.isEmpty($.trim($(val1).val()))) {
								if (!_.contains(category1RequiredList, $("#" + val.DT_RowId).find('.no-cls').text())) {
									category1RequiredList.push($("#" + val.DT_RowId).find('.no-cls').text());
								}
							}
							//end validate category1Required
						} else {
							cats += $.trim($(val1).val()) === "" ? "" : ">" + $.trim($(val1).val());
						}
						
						if ($(val1).closest('td').hasClass('has-error')) {
							countEntyExists++;
							if (!_.contains(listExistsErrorRow, $("#" + val.DT_RowId).find('.no-cls').text())) {
								listExistsErrorRow.push($("#" + val.DT_RowId).find('.no-cls').text());
							}
						}
					});
					var subCommodityTxt = $("#" + val.DT_RowId).find(".subCommodity-link").text();
					cats += subCommodityTxt === "" ? "" : ">" + subCommodityTxt;
					_.each($("#" + val.DT_RowId).find('td input:not(:first)'), function(val2, key, list) {
						if (key > 0 && _.isEmpty($.trim($(val2).closest('td').prev().find('input').val())) && !_.isEmpty($.trim($(val2).val()))) {
							if (!_.contains(emptyRowList, $("#" + val.DT_RowId).find("td:first").text())) {
								emptyRowList.push($("#" + val.DT_RowId).find("td:first").text());
							}
						}
					});
				}else {
					_.each($("#" + val.DT_RowId).find('td:not(:first)'), function(val, key, list) {
						if (key === list.length - 2) {
							return false;
						} else {
							if (key === 0) {
								cats += $.trim($(val).text()) === "" ? "" : $.trim($(val).text());
							} else {
								cats += $.trim($(val).text()) === "" ? "" : ">" + $.trim($(val).text());
							}
						}	
					});
				}
				//check row mapping
				var subCommodityLinkSelector = $("#" + val.DT_RowId).find(".subCommodity-link");
				if (_.isEmpty($.trim($(subCommodityLinkSelector).text()))) {
					listMappingErrorRow.push($("#" + val.DT_RowId).find('.no-cls').text());
				}
				val.rowId = val.DT_RowId;
				val.cats = cats;
				listSave.push(val);
			}
			//search duplicate item
			var result = [];
			if(_.size($("#vendor-hierarchy-mapping-table tbody tr.success")) > 0) {
				if (_.isEmpty(val.category1Input)) {
					result = 0;
				} else {
					result = _.where($("#vendor-hierarchy-mapping-table").DataTable().data(), {category1Input: val.category1Input, category2Input: val.category2Input, category3Input: val.category3Input, category4Input: val.category4Input});
				}
			}
				 
			if (_.size(result) >= 2) {
				var obj = [];
				for (var int = 0; int < _.size(result); int++) {
					obj.push($("#" + result[int].DT_RowId).find("td:first").text());
					obj = obj.sort(function(a, b){
						return a-b;
					});
				}
				if (!_.contains(duplicateList, obj.toString())) {
					duplicateList.push(obj.toString());
				}	
			}
		});
//		$("#vendor-hierarchy-mapping-table").DataTable().page.len(len);
		if(_.size(listChange) === 0) {
			showAlertDanger("There is no change");
		} else if (countEntyExists === 0 && _.isEmpty(listMappingErrorRow) && !_.isEmpty(listSave) && _.isEmpty(duplicateList) && _.isEmpty(emptyRowList) && _.isEmpty(category1RequiredList)) {
			$.ajax({
		        type: 'POST',
		        url: appName + "/vendor-management/save-vendor-hierarchy",
		        data:{
		        	strJson: JSON.stringify(listSave),
		        	vendorId:$("#vendor-input").val()
		        	//vendorId:$("#vendor-search-txt").attr('vendor-search-txtid')
		        }
			}).success(function(data) {
				var jsonData = $.parseJSON(data);
				if (!_.isEmpty(jsonData.errorMessage)) {
		    		showAlertDanger(jsonData.errorMessage);
				}else {
					$("#vendor-hierarchy-mapping-table").DataTable().destroy();
		    		initVendorHierarchyMappingTable();
		    		showAlertSuccess("Save successfully");
				}
				setChangeStatus(false);
				listChange = [];
		    }).done(function() {
		    }).fail(function() {
		    }).always(function() {
		    });	
		} else if(!_.isEmpty(listMappingErrorRow) || countEntyExists > 0 || _.size(duplicateList) !== 0 || _.size(emptyRowList) !== 0 || !_.isEmpty(category1RequiredList)) {
			var message = "";
			if (!_.isEmpty(listMappingErrorRow)) {
				message+= "<strong>Sub Commodity is missing in line number: </strong>" + listMappingErrorRow.toString() + "<br/>";
			}
			if (countEntyExists > 0) {
				message+= "<strong>Category exists in different category levels in line number: </strong>" + listExistsErrorRow.toString() + "<br/>";
			}
			
			if (_.size(duplicateList) !== 0) {
				message+= "<strong>Duplicate rows:</strong>&nbsp;";
				for (i = 0; i < duplicateList.length; i++) { 
					if (i === 0) {
						message += duplicateList[i].toString();	
					} else {
						message += " and " + duplicateList[i].toString();
					}	
				}
				message += "<br/>" ;
			}
			
			if (_.size(emptyRowList) !== 0) {
				message+= "<strong>Category cannot be empty in line number: </strong>" + emptyRowList.toString() + "<br/>";
			}
			
			if (!_.isEmpty(category1RequiredList)) { 
				if (_.size(_.difference(category1RequiredList, emptyRowList)) > 0) {
					message+= "<strong>Category 1 is required in line number: </strong>" + _.difference(category1RequiredList, emptyRowList).toString() + "<br/>";
				}
			}
			showAlertDanger(message);	
		}
 }
/************************************************************************************************************
* END SAVE
************************************************************************************************************/
		
/************************************************************************************************************
 * FUNCTION
 ************************************************************************************************************/
     function showLoadingVendorSearch() {
    	 if ($.fn.DataTable.isDataTable("#vendor-hierarchy-mapping-table")) {
    		//$("#vendor-hierarchy-mapping-table").DataTable().ajax.reload();
    		 $("#vendor-hierarchy-mapping-table").DataTable().destroy();
    		 initVendorHierarchyMappingTable();
		} else {
			initVendorHierarchyMappingTable();
		}
    	 
       $(".tabs-style").removeClass('hidden');
       $("#table-wrapper").removeClass('hidden');
       $(".style-add-new-row").removeClass('hidden');
       $(".empty-data-tbody").removeClass('hidden');
       $(".data-tbody").addClass('hidden');
     }

     function showLoadingTable() {
         $.blockUI({
             message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
             fadeIn: 100,
             timeout: 100,
             onUnblock: function() {
                 $(".tabs-style").removeClass('hidden');
                 $("#table-wrapper").removeClass('hidden');
                 $(".style-add-new-row").removeClass('hidden');

                 $(".empty-data-tbody").addClass('hidden');
                 $(".data-tbody").removeClass('hidden');
             }
         });
     }
     
     //initVendorHierarchyMapping 
     function initVendorHierarchyMappingTable() {
    		var table = $('#vendor-hierarchy-mapping-table').DataTable( {
    			"columnDefs": [
					{
						"render": function ( data, type, row, meta ) {
							return "";
					    },
						"searchable": false,
						//"orderable": false,
						"targets": 0
					},
					{
						"render": function ( data, type, row ) {
							if (row.category1 === "input") {
								return '<input maxlength="255" class="textbox-table form-control input-sm wd100 new-change-cls input-sm-table" type="text" categoryId1="'+row.categoryId1+'" error="'+row.category1InputError+'" value="'+row.category1Input+'" value-db="'+data+'">';
							} else {
								return '<span title="'+data+'">'+data+'</span>';
							}
					    },
						"targets": 1
					},
					{
						"render": function ( data, type, row ) {
							if (row.category2 === "input") {
								return '<input maxlength="255" class="textbox-table form-control input-sm wd100 new-change-cls input-sm-table" type="text" categoryId2="'+row.categoryId2+'" error="'+row.category2InputError+'" value="'+row.category2Input+'" value-db="'+data+'">';
							} else {
								return '<span title="'+data+'">'+data+'</span>';
							}
					    },
						"targets": 2
					},
					{
						"render": function ( data, type, row ) {
							if (row.category3 === "input") {
								return '<input maxlength="255" class="textbox-table form-control input-sm wd100 new-change-cls input-sm-table" type="text" categoryId3="'+row.categoryId3+'" error="'+row.category3InputError+'" value="'+row.category3Input+'" value-db="'+data+'">';
							} else {
								return '<span title="'+data+'">'+data+'</span>';
							}
					    },
						"targets": 3
					},
					{
						"render": function ( data, type, row ) {
							if (row.category4 === "input") {
								return '<input maxlength="255" class="textbox-table form-control input-sm wd100 new-change-cls input-sm-table" type="text" categoryId4="'+row.categoryId4+'" error="'+row.category4InputError+'" value="'+row.category4Input+'" value-db="'+data+'">';
							} else {
								return '<span title="'+data+'">'+data+'</span>';
							}
					    },
						"targets": 4
					},
    				{
    					"render": function ( data, type, row ) {
    						if (row.statusNew === "Y" || row.statusChange === "Y") {
    							//return '<input class="textbox-table form-control input-sm wd100 new-change-cls" type="text" value-db="'+data+'" value="'+data+'">';
    							return '<a class="textbox-table subCommodity-link new-change-cls" value-db="'+data+'">'+data+'</a>';
    						} else {
    							return '<a class="textbox-table subCommodity-link" value-db="'+data+'">'+data+'</a>';
    						}
    		            },
    					"searchable": false,
    					"orderable": false,
    					"targets": 5
    				},
    				{
    					"render": function ( data, type, row ) {
    							return '<i title="Add Sub Commodity" class="fa fa-plus-square"></i>';
    		            },
    					"searchable": false,
    					"orderable": false,
    					"targets": 6
    				},
    				{
    					"render": function ( data, type, row ) {
    						return '<span title="Delete row" class="action glyphicon glyphicon-remove cursor-pointer delete-cls"></span>';
    		            },
    					"searchable": false,
    					"orderable": false,
    					"targets": 7
    				},
    				{
    					"width": "16%",
    					"targets": [1, 2, 3, 4, 5]
    				},
    				{
    					"width": "2%",
    					"targets": 0
    				},
    				{
    					"width": "3%",
    					"targets": [6, 7]
    				}
    			 ],
    			//"order":  [[ 1, 'asc' ], [ 2, 'asc' ],[ 3, 'asc' ], [ 4, 'asc' ]],
    			"aLengthMenu": [
    			    [10, 25, 50, 100],
    			    [10, 25, 50, 100]
    			],
    			"sDom": "tripl",
    	    	"bSort" : false,
    	    	"bAutoWidth": false,
    	    	"ordering": false,
    	    	"orderMulti": true,
    	    	"pagingType": "full_numbers",
    	    	"stateSave": false,
    			"processing": false,
    		    "ajax": {
    		        "url": appName + '/vendor-management/get-vendor-hierarchy',
    		        "type": 'GET',
    		        "cache" : false,
    		        "data": function ( d ) {
    		        	d.vendorId = $("#vendor-input").val();
    	            }
    		    },
    		    "columns": [
    		                { "data": "no", "className": "no-cls" },
    		                { "data": "category1", "className": "category1-cls" },
    		                { "data": "category2", "className": "category2-cls"},
    		                { "data": "category3", "className": "category3-cls" },
    		                { "data": "category4", "className": "category4-cls" },
    		                { "data": "subCommodity", "className": "subCommodity-cls" },
    		                { "data": "searchSubCommodity", "className": "search-subCommodity-cls" },
    		                { "data": "deleteBtn", "className": "deleteBtn-cls" }
    		     ],
    		    "oLanguage" : {
    				"sLengthMenu" : "Show _MENU_ rows",
    				"sInfoEmpty" : "No records found",
    				"sInfo" : "Showing _START_ to _END_ of _TOTAL_"
    			},
    			"preDrawCallback": function( settings ) {
    			},
    			"fnDrawCallback": function( oSettings ) {
    				currentVendorId = $("#vendor-input").val();
    				highlightNewOrChange();
    				checkErrorInput();
    				$("#vendor-hierarchy-mapping-table").find('tbody tr:first').find('td.category1-cls input').focus();	
    				if(this.DataTable().data().length > 0) {
    					this.DataTable().column(0,{search:'applied', order:'applied'}).nodes().each( function (cell, i) {
    			            cell.innerHTML = i+1;
    			        });
    				}
    				//listChange = [];
    			},
    			"initComplete": function (settings, json) {
    				$("#vendor-search-txt").attr('vendor-search-txtid-old', $("#vendor-search-txt").attr('vendor-search-txtid'));
    				$("#vendor-search-txt").attr('vendor-search-txttext-old', $("#vendor-search-txt").attr('vendor-search-txttext'));
    				if (!_.isEmpty(json.errorMessage)) {
    					showAlertDanger(json.errorMessage);
					}
    				listChange = [];
    				checkAndSetChangeStatus();
    			},
    			"rowCallback": function( row, data ) {		  
    				 /* add class for cell */
    				$('td:eq(0)', row).addClass('text-left');
    				$('td:eq(1)', row).addClass('text-left text-ellipsis');
    				$('td:eq(1)', row).addClass('text-left text-ellipsis');
    				$('td:eq(2)', row).addClass('text-left text-ellipsis');
    				$('td:eq(3)', row).addClass('text-left text-ellipsis');
    				$('td:eq(4)', row).addClass('text-left text-ellipsis');
    				$('td:eq(5)', row).addClass('text-left text-ellipsis');
    				$('td:eq(6)', row).addClass('text-center add-subCommodity');
    				$('td:eq(7)', row).addClass('text-center');
    			}     
    		});

    		return table;
    	}

   function initHebHierarchy(){
	   var nodeId = "";
	   $.jstree.defaults.search.show_only_matches = false;
	   $.jstree.defaults.search.case_sensitive = false;
	   $.jstree.defaults.search.search_leaves_only = true;
	   $('#jstree')
		.jstree({
			'core' : {
				'data' : {
					'url' : appName + "/vendor-management/get-heb-hierarchy-root",
					'data' : function (node) {
						nodeId = node.id.split("_")[0];
						//console.log(node.id);
						return { 'id' : nodeId };
					},
					"success": function(data) {
						//console.log("root");
						//console.log(data);
						if (!_.isEmpty(data) && data[0].error !== undefined) {
							showAlertDangerJsTree(data[0].error);
						} else {
							if (nodeId !== "#") {
								//debugger;
								if (_.size(data) > 0) {
									$("#" + data[0].id).closest('ul').css('display', '');
								}
							}	
							removeAlertJsTree();
							$("#search-subCommodity-modal").removeClass('disabled');
							$("#search-subCommodity-modal").focus();
						}		
			        },
			        "error" : function(data) {
			        }
				},
				'check_callback' : true,
				'themes' : {
					'responsive' : false,
					"icons": false,
					"stripes": false,
					"variant": "large",
					"dots": true
				},
				"multiple" : false
			},
			
			"search" : {
				  'fuzzy' : false,
				  'ajax' : {
				   'url' : appName + "/vendor-management/search-heb-hierarchy",
				   'data' : function(str) {
					   return { "search_str" : str };
				   },
				   "dataFilter" : function (data) {
					   listShow = [];
					   listResultRest = [];
					   listResult = [];
					   listResult = JSON.parse(data);
					   if (_.size(listResult) > 20) {
						   _.each(listResult, function(value, key) {
							   if (key <= 19) {
								   listShow.push(value);
							   } else {
								   listResultRest.push(value);
							   }
							   
						   })
					   } else {
						   listShow = listResult;
					   }
					   return JSON.stringify(listShow);
				   },
				   "success": function(data) {
					   //debugger;
					   //console.log(data);
					   if (!_.isEmpty(data) && data[0] === "error") {
						   //$("#jstree").jstree().destroy();
						   $('#jstree').parent().unblock();
						   showAlertDangerJsTree(data[1]);
					   }else {
						   if (_.size(data) === 0) {
							   showAlertDangerJsTree("No results found");
						   } else {
							   $(".jstree-node ").css('display', 'none');  
							   _.each(data, function(value, key) { $("#" + value).css('display','') })						   
							   removeAlertJsTree();
							   $('#jstree').removeClass('hidden');
						   }
						   $('#jstree').parent().unblock();
					   }
		            }
				  },
				 },
			'plugins' : ['search','wholerow']
		})
		.on('changed.jstree', function (e, data) {
		})
		.on('after_open.jstree', function (e, data) {
			_.each($("#jstree").jstree().get_children_dom($("#" + data.node.id)), function (value, key) {
				var text = $(value).find('.subCommodityCd-cls').text() + $(value).find('.subCommodityDesc-cls').text();
				if (!_.isEmpty($("#search-subCommodity-modal").attr('search-value')) && text.indexOf($("#search-subCommodity-modal").attr('search-value').toUpperCase()) > -1) {//&& _.size($("#" + data.node.id).find('.jstree-search')) === 0
					$(value).find('a').addClass('jstree-search');
				}
			});
		}).on('search.jstree', function (e, data) {
			//$(".jstree-open").removeClass('jstree-open').addClass('jstree-closed');
			if (_.isEmpty($("#search-subCommodity-modal").attr('search-value'))) {
				 $(".jstree-node").css('display', '');
		         $("#jstree").jstree("clear_search");
		         $("#jstree").jstree('close_all');
		         $('#jstree').jstree("deselect_all");
			}else {
//				$('#jstree').jstree('close_all');
//				_.each(listShow, function(value, key) {
////					console.log("search.jstree - value: " + value);
//					$("#" + value).css('display','');
//				})
				_.each(listResultRest, function(value, key) {
					//console.log("search.jstree - value: " + value);
					$('#jstree').jstree(true).close_node($("#" + value));
					$("#" + value).css('display','');
				})	
			}
		});
  }
     
     function checkNewOrChange() {
		var i = 0;
		_.each($("#vendor-hierarchy-mapping-table").DataTable().data(), function(val, index, list){
			if(val.statusNew === "Y" || val.statusChange === "Y") {
				i++;
				return false;
			}
		});
		return i === 0 ? false : true;
     }
     
     function highlightNewOrChange() {
    	_.each($(".new-change-cls"), function(val, key){
    		$(val).closest('tr').addClass('success');
		})
     }
     
     function showAlertDanger(errorMessage) {
    	 //remove class
    	 $("#alert-wp").find('.alert').removeClass('alert-success');
    	 //add class
    	 $("#alert-wp").find('.alert').addClass('alert-danger');
    	 //show
    	 $("#alert-wp").removeClass('hidden');
    	 //set message
    	 $("#message-alert").html(errorMessage);
    	 
     }
     
     function showAlertSuccess(errorMessage) {
    	 //remove class
    	 $("#alert-wp").find('.alert').addClass('alert-success');
    	 //add class
    	 $("#alert-wp").find('.alert').removeClass('alert-danger');
    	 //show
    	 $("#alert-wp").removeClass('hidden');
    	 //set message
    	 $("#message-alert").html(errorMessage);
    	 
     }
     
     function removeAlertSuccess() {
    	 $("#alert-wp").addClass('hidden');
    	 $("#message-alert").text(CONST.EMPTY_STRING);
     }
     
     function showAlertDangerJsTree(errorMessage) {
    	$("#jstree").addClass("hidden");
		$("#message-alert-jstree").text(errorMessage);
		$("#alert-wp-jstree").removeClass("hidden");
     }
     
     function removeAlertJsTree() {
    	$("#jstree").removeClass("hidden");
		$("#message-alert-jstree").text("");
		$("#alert-wp-jstree").addClass("hidden");
     }
     
     function showAlertDangerVendor(errorMessage) {
 		  $("#vendor-message-alert").text(errorMessage);
 		  $("#vendor-alert-wp").removeClass("hidden");
      }
      
      function removeAlertVendor() {
    	  $("#vendor-message-alert").text("");
   		  $("#vendor-alert-wp").addClass("hidden");
      }
     
     /**
      * Auto complete for textboxes to search.
      */
     function getDataForAutocomplete(idInput) {
         $.ajax({
         	 type: "POST",
             contentType: "application/json; charset=utf-8",
             url: appName + "/vendor-management/search-for-vendors",
             dataType: "json",
             success: function (data) {
            	 //debugger;
            	 //console.log(data);
                 dataAutoCompleteInput = data.data;
                 for (var i in dataAutoCompleteInput) {
                     dataAutoCompleteInputValue.push(dataAutoCompleteInput[i].label);
                     dataAutoCompleteInputLabel.push(dataAutoCompleteInput[i].value);
                 }
                 autoCompleteInitVendorManagement(idInput, dataAutoCompleteInput);
             },
             error: function (result) {
                 //alert("Error......");
             }
         })
     }
     
     function autoCompleteInitVendorManagement(idInput, data){
        $( "#" + idInput ).autocomplete({
         autoFocus: true,
         search: function(event, ui) {
         },
         open: function(event, ui) {
         },
         source: data,
         minLength: 0,
         select: function(event, ui) {
             $("#"+idInput).val(ui.item.label);
             if ($.inArray($("#"+idInput).val(), dataAutoCompleteInputValue) !== -1 
                     || $.inArray($("#"+idInput).val(), dataAutoCompleteInputLabel) !== -1) {
            	 	$( "#" + idInput ).attr( idInput +"text", ui.item.label ); 
            	 	$( "#" + idInput ).attr( idInput +"id", ui.item.value );
             } else {
               $( "#" + idInput ).attr( idInput +"id", "");
             }
             selectedLabelBefore = ui.item.label;
             selectedIdBefore = ui.item.value;
             return false;
         },
         focus: function(event, ui) {
        	 //debugger
        	//$("#"+idInput).val(ui.item.label);
//	        if ($.inArray($.trim($("#"+idInput).val()), dataAutoCompleteInputValue) !== -1 
//	                || $.inArray($.trim($("#"+idInput).val()), dataAutoCompleteInputLabel) !== -1) {
//	             $('.ui-menu-item').attr( "title", ui.item.label);
//	             
//	             if (!_.isEmpty($( "#" + idInput ).attr(idInput +"id"))) {
//	            	 $( "#" + idInput ).attr(idInput +"id-old", $( "#" + idInput ).attr(idInput +"id"));
//	             }
//	             
//	             if (!_.isEmpty($( "#" + idInput ).attr(idInput +"text"))) {
//	            	 $( "#" + idInput ).attr(idInput +"text-old", $( "#" + idInput ).attr(idInput +"text"));
//	             }
//	             $( "#" + idInput ).attr( idInput +"id", ui.item.value );
//	        } else {
//	          $( "#" + idInput ).attr( idInput +"id", "");
//	        }
//	        selectedLabelBefore = ui.item.label;
//	        selectedIdBefore = ui.item.value;
//	        return false;
         }
      }).focusout(function() {
    	  if (_.isEmpty($.trim($("#vendor-search-txt").val()))) {
    		  $("#vendor-search-txt").val(selectedLabelBefore);
    	  } else {
    		  if ($.inArray($.trim($("#"+idInput).val()), dataAutoCompleteInputValue) === -1) {
        		  $( "#" + idInput ).attr( idInput +"id", "");
        		  $( "#" + idInput ).attr( idInput +"text", "");
              } else {
            	  $( "#" + idInput ).attr( idInput +"id", selectedIdBefore);
        		  $( "#" + idInput ).attr( idInput +"text", selectedLabelBefore);
              }
    	  }
      })
     }
     
     /**
      * click to dropdown auto list in autoComplete.
      */
     function clickDropAutoCompleteVendorManagement(){
     	$('.wrap-caret').click(function() {
     		$("#vendor-search-txt").autocomplete("search", "");
     		$("#" + $(this).parent("div").find("input").attr("id")).focus();
     	});
     }
     
//     function checkAndSetChangeStatus() {
//    	 var len = $("#vendor-hierarchy-mapping-table").DataTable().page.len();
//    	 $("#vendor-hierarchy-mapping-table").DataTable().page.len(-1);
//    	 if (_.size($(".success")) === 0) {
//     		setChangeStatus(false);
//	     } else {
//	      	setChangeStatus(true);
//	     }
//    	 $("#vendor-hierarchy-mapping-table").DataTable().page.len(len);
//     }
     
     function inArrays (itemsTmp, itemObj) {
    	 return _.size(_.where(itemsTmp, itemObj)) === 0 ? false : true; 
   	 }

     function getItemInArraysByKey (itemsTmp, itemObj) {
    	 return _.where(itemsTmp, itemObj); 
     }

     function removeInArrays(itemsTmp, id) {
    	 //debugger;
    	 var val = _.filter(itemsTmp, function(item){ 
    		 return item.DT_RowId !== id 
    	 });
    	 return val;
     }
     
     function checkAndSetChangeStatus() {
    	 if (_.size(listChange) > 0) {
    		setChangeStatus(true);
    	 } else {
         	setChangeStatus(false);
    	 }
     }
/************************************************************************************************************
 * END FUNCTION
 ************************************************************************************************************/
     
/************************************************************************************************************
 * VALIDATION
************************************************************************************************************/
     $(document).on('change', '.input-sm-table', function() {
    	var trId = $(this).closest('tr').attr('id');
    	var count = 0; 
      	var currentTr = $(this).closest('tr');
    	_.each($(currentTr).find(".input-sm-table"), function(value, key, list) {
    		_.each($(currentTr).find(".input-sm-table"), function(value1, key1) {
    	    	if ($.trim($(value).val()) === $.trim($(value1).val()) && !_.isEmpty($.trim($(value).val()))) {
    	    		count++;
				}
    	    })
    	    
    	    if (count >= 2) {
    	    	tooltipForValidation($(value), "Category exists in different category levels", "top");
    	    	if ($(value).closest('td').hasClass('category1-cls')) {
    	    		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + trId)).data().category1InputError = CONST.YES;
				} else if($(value).closest('td').hasClass('category2-cls')) {
					$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + trId)).data().category2InputError = CONST.YES;
    	    	} else if($(value).closest('td').hasClass('category3-cls')) {
    	    		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + trId)).data().category3InputError = CONST.YES;
    	    	} else if($(value).closest('td').hasClass('category4-cls')) {
    	    		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + trId)).data().category4InputError = CONST.YES;
    	    	}
    	    	
			}else {
				clearTooltipForValidation($(value));
				if ($(value).closest('td').hasClass('category1-cls')) {
    	    		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + trId)).data().category1InputError = CONST.NO;
				} else if($(value).closest('td').hasClass('category2-cls')) {
					$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + trId)).data().category2InputError = CONST.NO;
    	    	} else if($(value).closest('td').hasClass('category3-cls')) {
    	    		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + trId)).data().category3InputError = CONST.NO;
    	    	} else if($(value).closest('td').hasClass('category4-cls')) {
    	    		$("#vendor-hierarchy-mapping-table").DataTable().row($("#" + trId)).data().category4InputError = CONST.NO;
    	    	}
			}
    		count = 0;
	    });
    	 
  	});
     
     function checkErrorInput() {
    	 _.each($(".input-sm-table"), function(value, key) {
    		if ($(value).attr('error') === 'Y') {
    			//debugger;
    			tooltipForValidation(value, "Category exists in different category levels", "top");
			}
    	 });
     }
     
     /**
      * display error message by tooltip.
      * @param inputId id of element
      * @param message error 
      * @param placement the placement of message top|right|bottom|left, optional param.
      */
     function tooltipForValidationDiv(selectorInput, message, placement){
         $(selectorInput).closest('div').addClass("has-error");
         placement = (typeof placement === "undefined") ? "top" : placement;
         $(selectorInput).attr("data-toggle", "tooltip");
         $(selectorInput).attr("data-original-title", message);
         $(selectorInput).tooltip({'placement':placement, 'trigger':'hover manual'});
         $(selectorInput).tooltip('show');
     }

     /**
      * clear error message by tooltip.
      * @param inputId id of element
      */
     function clearTooltipForValidationDiv(id){
         $("#" + id).closest('div').removeClass("has-error");
         $("#" + id).tooltip('hide');
         $("#" + id).removeAttr("data-toggle");
         $("#" + id).removeAttr("data-original-title");
     }
     
     /**
      * display error message by tooltip.
      * @param inputId id of element
      * @param message error 
      * @param placement the placement of message top|right|bottom|left, optional param.
      */
     function tooltipForValidation(selectorInput, message, placement){
     	//debugger;
     	$(selectorInput).closest('td').addClass("has-error");
     	placement = (typeof placement === "undefined") ? "top" : placement;
     	$(selectorInput).attr("data-toggle", "tooltip");
     	$(selectorInput).attr("data-original-title", message);
     	$(selectorInput).tooltip({'placement':placement, 'trigger':'hover manual'});
     	$(selectorInput).tooltip('show');

     }

     /**
      * clear error message by tooltip.
      * @param inputId id of element
      */
     function clearTooltipForValidation(selectorInput){
     	$(selectorInput).closest('td').removeClass("has-error");
     	$(selectorInput).tooltip('hide');
     	$(selectorInput).removeAttr("data-toggle");
     	$(selectorInput).removeAttr("data-original-title");
     }
/************************************************************************************************************
* END VALIDATION
************************************************************************************************************/
     
/************************************************************************************************************
* TAB
************************************************************************************************************/
/************************************************************************************************************
* END TAB
************************************************************************************************************/
    });