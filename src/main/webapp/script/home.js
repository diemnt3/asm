var TreeNode = [];

var requestRules = [];
var msgRequiredInput = "This field is required.";
var msgGreaterThanZezo = "The input value should be greater than zero.";
var msgInvalidValue = "Invalid value.";
var msgRange0To100Accept = "Only the value between <br> 0 and 100 is accepted.";
var msgRangeMarginAccept = "Only the value between <br>-10,000 and 10,000 is accepted.";
var msgRange0To100000Accept = "Only the value between <br>0 and 100,000 is accepted.";
var msgLoseChange = "You will lose your changes, do you wish to continue?";
var msgSaveConfirm = "Do you want to save the changes?";

var saveChangeArrAfterSave = [];
var saveChangeArrAfterLoad = [];
var saveChangeArrAfterLoadAssign = [];
var saveChangeArrAssignAfterSave = [];
var firstLoad = 0;
var currentNode=0;
var onArr = [];	
var offArr = [];
var inheritRuleArr = [];
var inheritRuleArr2 = [];
var on = false;
var offFirstLoadArr = [];
var selectedText;

$(document).ready(function () {
	$(function () {
		beforeLoadScreen();
		
		/**
		 * Load Tree.
		 * @author Duyen.le
		 */
		 loadTree(function (result) {
		 	//console.log(result);		 		 	
                $('#treeHie').jstree({
                    "core": {
                        "data": result,
                        "multiple": false,
                        "animation": 200,
                        "check_callback": true,
                        "themes": {
                            "icons": false,
                            "stripes": true,
                            "dots": true
                        }
                    },
                    "ui" : {
				        "initially_select" : [ "root" ]
				    },
                    "plugins": ["types", "wholerow"]  //"dnd", "contextmenu", "search",  
                }).on('select_node.jstree', function (e, data) {
                	removeAlertSuccess(0);	
					saveChangeArrAfterSave = [];
					saveChangeArrAfterLoad = [];
					$("#contentColRight").css('display','');
					$("#chooseRulesDiv").css('display','');									   				     
				   	TreeNode.childNodeValue = data.node.id;
				   	TreeNode.parentNodeValue = data.node.parent;
										
					inheritRuleArr = [];
					if(TreeNode.childNodeValue !==currentNode){
						$("#reset-toDefault-btn").show();			
						$("#global-save-btn").show();
						$("#global-save-btn").prop("disabled", true);
						$("#reset-toDefault-btn").prop("disabled", true);
						if(firstLoad>=1){
							if(!isChanges()){
								changesRuleArr = getRuleChanges();							
								if(saveChangeArrAssignAfterSave.length > 0){		
					        		var changedArr = compare2Arr(saveChangeArrAssignAfterSave,changesRuleArr);
					        		if(changedArr.length > 0){
					        			setChangeStatus(true);
					        		} else {
					        			setChangeStatus(false);
					        		}
					        	} else {
					        		if(!on){
					        			changesRuleArr = [];
					        		}
					        		var changesArr = compare2Arr(changesRuleArr, saveChangeArrAfterLoadAssign);					        		
					    			//console.log( changesArr.length, saveChangeArrAfterLoadAssign.length);	    			
					    			if(saveChangeArrAfterLoadAssign.length<=0){
					    				/*var off = true;
						                $.each(changesRuleArr, function(d, data){              	
						                    if(data.activeSw==='Y'){	
						                    	if($("#formHie").find(".ibutton-container-checkmark").length> 0){
						                    		if($("#formHie").find(".ibutton-container-on").length > 0 && $("#formHie").find(".ibutton-container-on>input").attr('id')!=='turnOnAllRule'){
														off = false;
							                    	} else {
							                    		off = true;
							                    	}
						                    	} else {
						                    		off = false;
						                    	}
						                    }
						                });
						                if(off){console.log("####### 1");
						                    setChangeStatus(false);
						                } else{console.log("####### 2");
						                    setChangeStatus(true);
						                }*/
						                
						                if(inheritRuleArr2.length>0){
						                	var OnRuleArr = getRuleON();						                	
					                		if(OnRuleArr.length>0){
												var compareRule = compare2Arr(OnRuleArr, inheritRuleArr2);
								                	if(compareRule.length>0){
								                		setChangeStatus(true);
								                	} else {
								                		setChangeStatus(false);
								                	}						                		
						                	} else {
						                		var OffRuleArr = getRuleOff();
						                		if(OffRuleArr.length>0){
						                			if(inheritRuleArr2.length !==OnRuleArr.length){
						                				setChangeStatus(true);
						                			} else {
							                			var compareRule = compare2Arr(OffRuleArr, inheritRuleArr2);
									                	if(compareRule.length>0){
									                		setChangeStatus(true);
									                	}
									                }
						                		}						                		
						                	}						                							                							                	
						                }  
					    			} else if(changesArr.length>0) {
					    				setChangeStatus(true);
					    			} else {
					    				setChangeStatus(false);
					    			}
					        	}
							}
							
							if(isChanges()) {								
								bootbox.dialog({
									closeButton: true,
									title : "Confirmation",
									message: msgLoseChange,
									buttons: {
										main: {
									      label: "Yes",
									      className: "btn-primary",
									      callback: function() {
									      		inheritRuleArr2 = []; 
									      		offFirstLoadArr = [];      
										        loadCommodityRulesSet(TreeNode);

												setChangeStatus(false);
												saveChangeArrAfterLoadAssign = [];
												saveChangeArrAfterLoadAssign = saveChangeArrAfterLoad;
												$("#textRule").html(data.node.text);
												currentNode = TreeNode.childNodeValue;
												saveChangeArrAssignAfterSave = [];												
												if(saveChangeArrAfterLoadAssign.length>0){
													$("#reset-toDefault-btn").prop("disabled", false);
													$("#global-save-btn").prop("disabled", true);												
												} else {
													$("#global-save-btn").prop("disabled", true);
												}
												onArr = [];
												offArr = [];
									      }
									    },	
									    cancel: {
									      label: "No",
									      className: "btn-default",
									      callback : function(){				
									      	cancalCallBackSelectNode();											
									      }
									    }
								  	},
								  	onEscape: function() {
								    	cancalCallBackSelectNode();
								    }		
								});
							} else {														
								offFirstLoadArr = [];
								loadCommodityRulesSet(TreeNode);
								setChangeStatus(false);
								saveChangeArrAfterLoadAssign = [];
								saveChangeArrAfterLoadAssign = saveChangeArrAfterLoad;
								$("#textRule").html(data.node.text);
								currentNode = TreeNode.childNodeValue;
								saveChangeArrAssignAfterSave = [];								
								disableSaveButton(saveChangeArrAfterLoadAssign);
																
							}
						} else {						
							offFirstLoadArr = [];						
							loadCommodityRulesSet(TreeNode);
							setChangeStatus(false);
							saveChangeArrAfterLoadAssign = saveChangeArrAfterLoad;							
							$("#textRule").html(data.node.text);
							currentNode = TreeNode.childNodeValue;
							saveChangeArrAssignAfterSave = [];
							disableSaveButton(saveChangeArrAfterLoadAssign);							
						}
					}
				    firstLoad ++;
				   
				  }).bind("open_node.jstree", function (event, data) {				  	
						$("#treeHie .jstree-container-ul li#"+data.node.id+" .jstree-children li .jstree-anchor").each(function(d, dt){							
							//$(this).html($.trim(truncate($(this), 30, "...")));							
							var str = $(this).html();	
							//console.log(str);
							var x = str.split('<i class="jstree-icon jstree-themeicon"></i>')[1];							
							//console.log(x, x.length);
							if (x.length > 25) {								
								$(this).attr('title',x);
								$(this).html('<i class="jstree-icon jstree-themeicon"></i>'+ x.substr(0,25) + '...');
								//$(this).addClass('text-ellipsis');									
							}
						});
				  });
            });
        });

		$("#turnOnAllRule").iButton({
	        labelOff: "OFF",
	        labelOn: "ON",
	        easing: "swing",
	        enableDrag: false,
	        resizeHandle: false,
	        resizeContainer: false,
	        classContainer: "ibutton-container",
	        change: function ($input){	        	
        		if ($input.is(":checked")) {// CHECKED	
					$($input).parent().addClass('ibutton-container-on')	;										
					$("#formHie div:not(.ibutton-container-checkmark) > input[name^='chk-']").attr('checked',true);			    				
					$("#formHie div:not(.ibutton-container-checkmark) > input[name^='chk-']").iButton().trigger("change");	
					//$("#formHie input[name^='chk-']").attr('checked',true);			    				
					//$("#formHie input[name^='chk-']").iButton().trigger("change");					
					$("#retailStart").trigger('focus');
				}else {		// UNCHECKED	
					$($input).parent().removeClass('ibutton-container-on')	;
					//$("#formHie div:not(.ibutton-container-checkmark) > input[name^='chk-']").attr('checked',false);			    				
					//$("#formHie div:not(.ibutton-container-checkmark) > input[name^='chk-']").iButton().trigger("change");
					$("#formHie input[name^='chk-']").attr('checked',false);			    				
					$("#formHie input[name^='chk-']").iButton().trigger("change");					
				}
			},
			click: function(d, data){				
				if (data.is(":checked")) {
					offArr = [];

					$("#global-save-btn").prop("disabled", false);
					on = true;
					if(saveChangeArrAssignAfterSave.length>0){
						var compareRule = compare2Arr(saveChangeArrAssignAfterSave, getRuleWhenTurnAll(on));						
						if(compareRule.length>0){
							$("#global-save-btn").prop('disabled', false);
						} else {
							$("#global-save-btn").prop('disabled', true);
						}
					}
					$("#formHie input[name^='chk-']").each(function(d,dt){
						if(!$(dt).parent().hasClass('ibutton-container-checkmark')){							
							var id =   $(dt).attr('id').split('-')[1];
                            if($.inArray(id, onArr) === -1){
                                onArr.push(id);  
                            }
						}
					});				
				} else {
					on = false;	
					onArr = [];				
					if(saveChangeArrAssignAfterSave.length>0){
						var compareRule = compare2Arr(saveChangeArrAssignAfterSave, getRuleWhenTurnAll(on));						
						if(compareRule.length>0){
							$("#global-save-btn").prop('disabled', false);
						} else {
							$("#global-save-btn").prop('disabled', true);
						}
					} else {
						if(inheritRuleArr.length>0|| inheritRuleArr2.length>0){
							$("#global-save-btn").prop("disabled", false);
							setChangeStatus(true);
						} else {	
							if(saveChangeArrAfterLoadAssign.length> 0 ){
								$("#global-save-btn").prop("disabled", false);
								setChangeStatus(true); 
							} else {
								$("#global-save-btn").prop("disabled", true);
								setChangeStatus(false);
							}

						}
					}
				}										
			}
	    });
		
		$("#reset-toDefault-btn").click(function () {
			if(requestURL !== undefined){				
				requestURL = '';
			}
			var title;
			if($.trim(TreeNode.parentNodeValue) =='#'){
				subDeptId = TreeNode.childNodeValue;
				commodityID = '';
			}else{
				subDeptId = TreeNode.parentNodeValue;
				commodityID = TreeNode.childNodeValue;
			}			
			if(commodityID === ''){
				title = "<h5>Do you wish to reset the rules?</h5>";				
			} else {
				title = "<h5>Do you wish to reset the rules to higher level?</h5>";				
			}			
			bootbox.dialog({
				title : "Confirmation",
				message: title,
				buttons: {
					main: {
				      label: "Yes",
				      className: "btn-primary",
				      callback: function() {		        
				        resetToHigherLevel(subDeptId, commodityID);
				      }
				    },			
				    cancel: {
				      label: "No",
				      className: "btn-default"
				    }				    	    
			  	}		
			});				
		});
		$("#global-save-btn").click(function () {
			if(requestURL !== undefined){				
				requestURL = '';
			}
			var uniqueNames = getRuleChanges();		
			var arrEmpty = [];
			var arrValidValue = [];
			var arrValidValue2 = [];
			var arrOnlyValueAccept = [];
			var arrValidateMargin = [];
			var arrValidateRetail = [];
			$("#retailStart, #retailEnd, #maginStart, #maginEnd, #rule10").each(function(){
				if (_.isEmpty($(this).val()) && $(this).attr('disabled') === undefined) {
				   arrEmpty.push($(this).attr('id'));
				} else {					
					if($(this).attr('id')==='retailStart' && $(this).attr('disabled') === undefined){
						var retailEnd = $("#retailEnd").val();
						if ($.isNumeric($(this).val())) {
		                	if(isNum($(this).val())&& $(this).val()<=100000 && $(this).val()>=0){
		                		if(retailEnd.length > 0){
									if($(this).val() === retailEnd && $(this).val()===0){
										if($.inArray("retailEnd", arrValidValue) == -1){
											arrValidValue.push("retailEnd");
										}
									}
									if(!isNum($(this).val())){						
										arrValidValue2.push("retailStart");
									}
								} else {
									clearTooltipForValidationDiv("retailStart");
								}		                		
		                	} else {
		                		arrValidateRetail.push("retailStart");
		                	}                  
		                }
					} else if ($(this).attr('id')==='retailEnd' && $(this).attr('disabled') === undefined){
						var retailStart = $("#retailStart").val();
						if ($.isNumeric($(this).val())) {
		                	if(isNum($(this).val())&& $(this).val()<=100000 && $(this).val()>=0){
		                		if(retailStart.length > 0){
									if($(this).val() === retailStart && $(this).val()===0){
										if($.inArray($(this).attr('id'), arrValidValue) == -1){
											arrValidValue.push($(this).attr('id'));
										}
									}
									if(!isNum($(this).val())){
										arrValidValue2.push("retailEnd");
									}
									if($(this).val()>100000) {
										arrOnlyValueAccept.push($(this).attr('id'));
										tooltipForValidationDiv(this, msgRange0To100000Accept, "top");
									}		
								} else {
									clearTooltipForValidationDiv("retailEnd");	
								}
		                	} else {
		                		arrValidateRetail.push("retailEnd");
		                	}
		                }												
					} else if($(this).attr('id')==='maginStart' && $(this).attr('disabled') === undefined){
						var marginEnd = $("#maginEnd").val();
						if ($.isNumeric($(this).val())) {
							if(isNum($(this).val())&& $(this).val()<=10000 && $(this).val()>=-10000){
								if(marginEnd.length > 0){
									if ($.isNumeric($(this).val())) {							
										if($(this).val() === marginEnd && $(this).val()===-10000 || $(this).val() === marginEnd && $(this).val()===10000){
											if($.inArray("maginEnd", arrValidValue) == -1){
												arrValidValue.push("maginEnd");
											}
										}
									} else {
										arrValidValue2.push("maginStart");
									}
								} else {
									clearTooltipForValidationDiv("maginStart");
								}
							} else {
								arrValidateMargin.push("maginStart");
							}
						}						
					} else if($(this).attr('id')=='maginEnd' && $(this).attr('disabled') === undefined){
						var maginStart = $("#maginStart").val();
						if ($.isNumeric($(this).val())) {
							if(isNum($(this).val())&& $(this).val()<=10000 && $(this).val()>=-10000){
								if(maginStart.length > 0){							
									if ($.isNumeric($(this).val())) {							
										if($(this).val() === maginStart && $(this).val()===-10000 || $(this).val() === maginStart && $(this).val()===10000){						
											if($.inArray($(this).attr('id'), arrValidValue) == -1){
												arrValidValue.push($(this).attr('id'));
											}
										}
									} else {
										arrValidValue2.push("maginEnd");
									}
								} else {							
									clearTooltipForValidationDiv("maginEnd");
								}
							} else {
								arrValidateMargin.push("maginEnd");	
							}
						}																
					} else if($(this).attr('id')=='rule10' && $(this).attr('disabled') === undefined){
						var valueOFRule = $("#rule10").val();
						if(valueOFRule <0 || valueOFRule >100){
							arrOnlyValueAccept.push($(this).attr('id'));						
						}
						if(!isNum($(this).val())){
							arrValidValue2.push("rule10");
						}
					}
										
				}
			});			
			
			if(arrEmpty.length<=0 && arrValidValue.length <=0 && arrOnlyValueAccept.length<=0 && arrValidValue2.length <=0 && arrValidateMargin.length<=0 && arrValidateRetail.length <=0){							    
			    updateCommodityRulesSet(TreeNode, uniqueNames);
			} else{				   
			   	if(arrEmpty.length > 0){
			   		$.each(arrEmpty,function(d, data){
						tooltipForValidationDiv($("#"+data), msgRequiredInput, "top");
						$("#"+data).trigger('focus');
					});
			   	} else if(arrValidValue.length > 0){
			   		$.each(arrValidValue,function(d, data){
						tooltipForValidationDiv($("#"+data), msgGreaterThanZezo, "top");
						$("#"+data).trigger('focus');
					});
			   	} else if(arrOnlyValueAccept.length > 0){
			   		$.each(arrValidValue,function(d, data){
						tooltipForValidationDiv($("#"+data), msgRange0To100Accept, "top");
						$("#"+data).trigger('focus');
					});
			   	} else if(arrValidValue2.length > 0){
			   		$.each(arrValidValue2,function(d, data){
						tooltipForValidationDiv($("#"+data), msgInvalidValue, "top");
						$("#"+data).trigger('focus');
					});
			   	} else if(arrValidateMargin.length > 0){
			   		$.each(arrValidateMargin,function(d, data){
						tooltipForValidationDiv($("#"+data), msgRangeMarginAccept, "top");
						$("#"+data).trigger('focus');
					});
			   	} else if(arrValidateRetail.length > 0) {
					$.each(arrValidateRetail,function(d, data){
						tooltipForValidationDiv($("#"+data), msgRange0To100000Accept, "top");
						$("#"+data).trigger('focus');
					});
			   		
			   	}
			    
			}           
        });
				
		$(".my-checkbox").iButton({
			labelOff: "NO",
			labelOn: "YES",
			easing: "swing",
			enableDrag: false,
			resizeHandle: false,
			resizeContainer: false
		});
		
		$("#formHie input[name='chk-4'],input[name='chk-2'],input[name='chk-5'],input[name='chk-6'],input[name='chk-7'],input[name='chk-8'],input[name='chk-9'],input[name='chk-1'],input[name='chk-3'],input[name='chk-10']").iButton({
			labelOff: "OFF",
			labelOn: "ON",
			easing: "swing",
			resizeHandle: "auto",
			enableDrag: false,
			resizeContainer: false,
			classContainer: "ibutton-container sub-cb-item",
			change: function ($input){											
				if ($input.is(":checked")) {// CHECKED
					$($input).parent().addClass('ibutton-container-on').removeClass("ibutton-container-checkmark");
					// Enable [Handle checkbox YES/NO] when ON Rule
					if (typeof $($input).attr("data-left-check") !== 'undefined') {
						var index_check = $($input).attr("data-left-check");						
						var enableNext = $("input[data-right-check="+index_check+"]");	
							
						enableNext.iButton('disable', false);
						enableNext.prop('disabled', false);
					}					
					if($.trim($input.attr('id'))==='chk-2'){
						$("#retailStart").trigger('focus');											
					} else if($.trim($input.attr('id'))==='chk-3'){
						$("#maginStart").trigger('focus');						
					} else if($.trim($input.attr('id'))==='chk-10'){
						$("#rule10").trigger('focus');						
					}					
					//
				}else {		// UNCHECKED
					if($($input).attr('id')==='chk-2'){						
						clearTooltipForValidationDiv("retailStart");
						clearTooltipForValidationDiv("retailEnd");
					} 
					if($($input).attr('id')==='chk-3'){
						clearTooltipForValidationDiv("maginStart");
						clearTooltipForValidationDiv("maginEnd");
					} 
					if($($input).attr('id')==='chk-10'){
						clearTooltipForValidationDiv("rule10");
					}
					$($input).closest('.form-group').find('input').val("");					
					clearTooltipForValidationDiv($($input).closest('.form-group').find('#retailStart').attr('id'));
					clearTooltipForValidationDiv($($input).closest('.form-group').find('#retailEnd').attr('id'));

					$($input).parent().removeClass('ibutton-container-on').removeClass("ibutton-container-checkmark");
					// Disable [Handle checkbox YES/NO] when OFF Rule
					if (typeof $($input).attr("data-left-check") !== 'undefined') {
						var index_check = $($input).attr("data-left-check");						
						var enableNext = $("input[data-right-check="+index_check+"]");	
						enableNext.iButton('disable', true);	
						enableNext.iButton('toggle', false);
						enableNext.prop('disabled', true);									
					}
					//
				}

    			$($input).next().removeClass("ibutton-handle-checkmark").addClass("ibutton-handle");

    			
			},
			click: function(d, data){			
				var changesRuleArr = [];			
				var totalCheckbox = $(".sub-cb-item input[type=checkbox]").length;
    			var totalChecked = $(".sub-cb-item input[type=checkbox]:checked").length;
    			var totalUncheck = totalCheckbox - totalChecked;    			

    			// #turnOnAllRule
    			if (totalChecked == totalCheckbox){
    				$("#turnOnAllRule").iButton("toggle", true);
    			} else if (totalChecked == 0){
    				$("#turnOnAllRule").iButton("toggle", false);
    			} else{    				
    				var parentSelector = $("#turnOnAllRule").parent();
    				$("#turnOnAllRule").attr('checked', false);
    				parentSelector.removeClass('ibutton-container-on');
    				parentSelector.find(".ibutton-handle").attr('style', 'left: 0px;');
    				parentSelector.find(".ibutton-label-off span").attr('style', 'margin-right: 0px;');
    				parentSelector.find(".ibutton-label-on span").attr('style', 'margin-left: -31px;');
    			}
    			var id = data.attr('id').split('chk-')[1]; 
    			if(data.is(":checked")){				   				    				    				
    				onArr.push(id);
    				if(offArr.length>0){
    					var idexOfItem = offArr.indexOf(id.toString() );
						offArr.splice(idexOfItem, 1);
    				}
    			} else {
    				data.focus();    				
    				offArr.push(id); 				
					var idexOfItem = onArr.indexOf(id.toString());
					if(!data.parent().hasClass('ibutton-container-checkmark')){
						onArr.splice(idexOfItem, 1);
					}
    			}  
    			//console.log(onArr, offArr);  			
    			checkChangeRule(onArr);

			},
			init: function($input){
				
				if ($input.is(":checked")) {
					$($input).parent().addClass('ibutton-container-on');					
				}else {
					$($input).parent().removeClass('ibutton-container-on');
				}
			}
		});
	
		// Add icon 
		 function toggleChevron(e) {
		    $(e.target)
		        .prev('#contentDashboard .panel-heading,#contentDsv .panel-heading,#collapseHie .parent-label')
		        .find("span.fa")
		        .toggleClass('fa-minus-square fa-plus-square');
		 }
		 $('#accordion').on('hidden.bs.collapse', toggleChevron);
		 $('#accordion').on('shown.bs.collapse', toggleChevron);
		
		 $('#dateBegin,#dateEnd').datepicker({
		 	autoclose: true,
		 });	

		$('#tabResult a').click(function (e) {
		  e.preventDefault();
		  $(this).tab('show');
		});
		
		
/************************************************************************************************************
* VALIDATION
* ************************************************************************************************************/	
		$(document).on('keypress ', '.input-decimal', function(event){
			acceptDecimalNumber(this, event, 9, 2);
		});
		$(document).on('keypress ', '#maginStart,#maginEnd', function(event){
			acceptMaginNumber(this, event, 9, 2);
		});	
		
		$("#rule10, #retailStart, #retailEnd, #maginStart, #maginEnd").on('blur',function(){
			hideToolTip(this);
			if($(this).attr('id')!=="rule10"){
				if($(this).attr('id')==="retailStart" || $(this).attr('id')==="retailEnd" ){
					var val = $(this).val();
	                if (!_.isEmpty(val) && $.isNumeric(val)) {
	                	if(isNum(val)&& val<=100000 && val>=0){
	                		$(this).val(precise_roundFloatNum(val));         	
	                	}
	                	if(isNum(val) && val <0) {
	                		var number = val.toString();
                			if(number.indexOf(".")>=0){
                				var numArr = number.split(".");
                				var decimals = numArr[1].toString();
                				var num;
                				if(parseInt(numArr[0]) === 0){
                					numArr[0] = '-'+0;
                				} else {
                					numArr[0] = parseInt(numArr[0]);
                				}
                				
                				if(decimals.length > 2){
                					num = numArr[0] +"." + decimals.substr(0,2);
                				} else if(decimals.length ===2){
                					num = numArr[0] +"." + decimals;
                				} else if(decimals.length===0){
                					num = parseInt(number);
                				} else {
                					decimals = decimals + "0000";
                					num = parseInt(numArr[0]) +"." + decimals.substr(0,2);
                				}              			
	                			$(this).val(num);
	                		} else {       
	                			var	num = parseInt(number);
	                			$(this).val(num);
	                		}
	                	}
	                	if(isNum(val) && val>100000) {
	                		var number = val.toString();
                			if(number.indexOf(".")>=0){
                				var numArr = number.split(".");
                				var decimals = numArr[1].toString();
                				if(decimals.length ===0){                					
	                				$(this).val(parseInt(number));
                				}
                			}
	                	}	                	
	                }
	            }
	            if($(this).attr('id')==="maginStart" || $(this).attr('id')==="maginEnd" ){
	            	var val = $(this).val();
	                if (!_.isEmpty(val) && $.isNumeric(val)) {
	                	if(isNum(val)&& val<=10000 && val>=-10000){
	                		$(this).val(precise_roundFloatNum(val));      	
	                	}
	                	if(isNum(val) && val <-10000) {
	                		var number = val.toString();
                			if(number.indexOf(".")>=0){
                				var numArr = number.split(".");
                				var decimals = numArr[1].toString();
                				var num;
                				if(decimals.length > 2){
                					num = parseInt(numArr[0]) +"." + decimals.substr(0,2);
                				} else if(decimals.length ===2){
                					num = parseInt(numArr[0]) +"." + decimals;
                				} else if(decimals.length ===0){
                					num = parseInt(number);	
                				}else {
                					decimals = decimals + "0000";
                					num = parseInt(numArr[0]) +"." + decimals.substr(0,2);
                				}	                			
	                			$(this).val(num);
	                		} else {
	                			var	num = parseInt(number);
	                			$(this).val(num);
	                		}
	                	}
	                	if(isNum(val) && val>10000) {
	                		var number = val.toString();
                			if(number.indexOf(".")>=0){
                				var numArr = number.split(".");
                				var decimals = numArr[1].toString();
                				if(decimals.length ===0){                					
	                				$(this).val(parseInt(number));
                				}
                			}
	                	}            
	                }
	            }
			}
			var changesRuleArr = getRuleChanges();
			if(saveChangeArrAssignAfterSave.length > 0){  
				var changedArr = compare2Arr(saveChangeArrAssignAfterSave,changesRuleArr);    
					
				if(changedArr.length > 0){
					setChangeStatus(true);
					$("#global-save-btn").prop("disabled", false);
				} else {
					setChangeStatus(false);
					$("#global-save-btn").prop("disabled", true);
				}
			} else { 
				var changesArr = compare2Arr(changesRuleArr, saveChangeArrAfterLoadAssign);
                if(saveChangeArrAfterLoadAssign.length<=0){                	
                    if(inheritRuleArr.length>0){ 
	                	var OnRuleArr = getRuleON();
	                	var compareRule = compare2Arr(OnRuleArr, inheritRuleArr);
	                	
	                	if(compareRule.length>0){
	                		$.each(compareRule, function(d, dt){
	                			$("#chk-"+dt).parent().removeClass('ibutton-container-checkmark').addClass('ibutton-container-on');
	                			$("#chk-"+dt).next().removeClass("ibutton-handle-checkmark").addClass("ibutton-handle");
	                		});
	                		setChangeStatus(true);
                    		$("#global-save-btn").prop("disabled", false);
	                	}
	                }
                    
                } else if(changesArr.length>0) {                	
                    setChangeStatus(true);
                    $("#global-save-btn").prop("disabled", false);
                } else {                	
                    setChangeStatus(false);
                    $("#global-save-btn").prop("disabled", true);
                }
			}

		});
		$("#rule10").on('change',function(){
			if (_.isEmpty($(this).val())) {				
				tooltipForValidationDiv(this, msgRequiredInput, "top",null);	    		
			} else {				
				clearTooltipForValidationDiv("rule10");				
				var val = $(this).val();
				if(!isNum(val) && $(this).val()>100){					
					tooltipForValidationDiv($(this), msgRange0To100Accept, "top");
				} else if(val <0 || val >100){
					tooltipForValidationDiv($(this), msgRange0To100Accept, "top");
				} else {
					clearTooltipForValidationDiv("rule10");
					$(this).val(parseInt($(this).val()));
				}
			}
		});
		$("#retailStart, #maginStart").on('change',function(){			
			if (_.isEmpty($(this).val())) {				
				tooltipForValidationDiv(this, msgRequiredInput, "top",null);	    		
			} else {

				if (!$.isNumeric($(this).val())) {
					if(isNum($("#retailEnd").val())) {
						clearTooltipForValidationDiv("retailEnd");	
					}									
					tooltipForValidationDiv(this, msgInvalidValue, "top");
				} else {
					clearTooltipForValidationDiv($(this).attr('id'));
					if($(this).attr('id') === "retailStart") {
						var retailEnd = $("#retailEnd").val();
						var endValue = precise_roundFloatNum(retailEnd);	
						if(isNum($(this).val())&& $(this).val()<=100000 && $(this).val()>=0){							
							if(isNum(retailEnd)){								
								var val = precise_roundFloatNum($(this).val());															
								$(this).val(val);	
								var startValue = val;
									
								if (!_.isEmpty(retailEnd)) {
									if(retailEnd<0 || retailEnd>100000){
										tooltipForValidationDiv("retailEnd", msgRange0To100000Accept, "top");
									} else {
										if(parseFloat(startValue) >parseFloat(endValue) || parseFloat(startValue) === parseFloat(endValue) ){										
											//Maximum allowed {0} should be greater than Minimum allowed {0}	
											clearTooltipForValidationDiv("retailEnd");
											tooltipForValidationDiv(this, "Minimum allowed {0} should be lower than Maximum allowed {0}", "top","retail");
										} else if(startValue === 0 && endValue ===0){ 
											tooltipForValidationDiv($("#retailEnd"), msgGreaterThanZezo, "top");
										}else {							
											clearTooltipForValidationDiv("retailEnd");
										}
									}
								}
							}
							
						} else {							
							if (!_.isEmpty(retailEnd)) {
								if(retailEnd>0 && retailEnd<100000){
									clearTooltipForValidationDiv("retailEnd");
								}
							}
							tooltipForValidationDiv(this, msgRange0To100000Accept, "top");
						}
					}
					if($(this).attr('id') === "maginStart") {						
						if(validFormatMagin($(this).val())){					
							var maginEnd = $("#maginEnd").val();
							var endValue = precise_roundFloatNum(maginEnd);		
							if(isNum($(this).val())&& $(this).val()<=10000 && $(this).val()>=-10000){							
								if(isNum(maginEnd)){								
									var val = precise_roundFloatNum($(this).val());
									$(this).val(val);	
									var startValue = val;
									
									if (!_.isEmpty(maginEnd)) {
										if(maginEnd<-10000 || maginEnd>10000){
											tooltipForValidationDiv("maginEnd", msgRangeMarginAccept, "top");
										} else {
											if(parseFloat(startValue) >parseFloat(endValue) || parseFloat(startValue) === parseFloat(endValue) ){											
												clearTooltipForValidationDiv("maginEnd");
												tooltipForValidationDiv(this, "Minimum allowed {0} should be lower than Maximum allowed {0}", "top","Margin");
											} else if(startValue === -10000 && endValue ===10000){ 
												tooltipForValidationDiv($("#maginEnd"), "The input value should be greater than -10,000.", "top");
											}else {							
												clearTooltipForValidationDiv("maginEnd");
											}
										}
									}
								}
								
							} else {
								if (!_.isEmpty(maginEnd)) {
									if(endValue<=10000 && endValue>=-10000)	{
										clearTooltipForValidationDiv("maginEnd");
									}
								}
								tooltipForValidationDiv(this, msgRangeMarginAccept, "top");
							}
						} else {
							tooltipForValidationDiv(this, msgInvalidValue, "top");
						}
					}
				}
			}
		});
		$("#retailEnd, #maginEnd").on('change',function(){			
			if (_.isEmpty($(this).val())) {
	    		tooltipForValidationDiv(this, msgRequiredInput, "top");
			} else {
				if (!$.isNumeric($(this).val())) {
					if(isNum($("#retailStart").val())) {
						clearTooltipForValidationDiv("retailStart");	
					}
					tooltipForValidationDiv(this, msgInvalidValue, "top");
				} else {
					clearTooltipForValidationDiv($(this).attr('id'));
					if($(this).attr('id') === "retailEnd") {		
						var retainStart = $("#retailStart").val();
						if(isNum($(this).val())&& $(this).val()<=100000 && $(this).val()>=0){						
							if(isNum(retainStart)) {
								var val = precise_roundFloatNum($(this).val());
								$(this).val(val);	
								var endValue = val;	
								//$("#retailStart").val()	
								if (!_.isEmpty(retainStart)) {
									if(retainStart<0 || retainStart>100000){
										tooltipForValidationDiv("retailStart", msgRange0To100000Accept, "top");
									} else {
										var	startValue = precise_roundFloatNum(retainStart);									
																		
										if(parseFloat(startValue) >parseFloat(endValue) || parseFloat(startValue) === parseFloat(endValue)){							
											clearTooltipForValidationDiv("retailStart");
											tooltipForValidationDiv(this, "Maximum allowed {0} should be greater than Minimum allowed {0}", "top","retail");
										} else if(parseFloat(startValue) === 0 && parseFloat(endValue) ===0){ 
											if($(this).attr('id')==='retailStart') {
												tooltipForValidationDiv(this, msgGreaterThanZezo, "top");
											}
										} else {									
											clearTooltipForValidationDiv("retailStart");
										}
									}
								}
								
							}						
						} else {
							if (!_.isEmpty(retainStart)) {
								if(retainStart>0 && retainStart<100000){
									clearTooltipForValidationDiv("retailStart");
								}
							}
							tooltipForValidationDiv(this, msgRange0To100000Accept, "top");
						}	
					}
					if($(this).attr('id') === "maginEnd") {
						if(validFormatMagin($(this).val())){	
							var maginStart = $("#maginStart").val();
							if(isNum($(this).val())&& $(this).val()<=10000 && $(this).val()>=-10000){						
								if(isNum(maginStart)) {
									var val = precise_roundFloatNum($(this).val());
									$(this).val(val);	
									var endValue = val;	
									//$("#retailStart").val()	
									if (!_.isEmpty(maginStart)) {
										if(maginStart<-10000 || maginStart>10000){
											tooltipForValidationDiv("maginStart", msgRangeMarginAccept, "top");
										} else {
											var	startValue = precise_roundFloatNum(maginStart);									
																			
											if(parseFloat(startValue) >parseFloat(endValue) || parseFloat(startValue) === parseFloat(endValue)){							
												clearTooltipForValidationDiv("maginStart");
												tooltipForValidationDiv(this, "Maximum allowed {0} should be greater than Minimum allowed {0}", "top","Margin");
											} else if(parseFloat(startValue) === 0 && parseFloat(endValue) ===0){ 
												if($(this).attr('id')==='maginStart') {
													tooltipForValidationDiv(this, "The input value should be greater than -10,000.", "top");
												}
											} else {									
												clearTooltipForValidationDiv("maginStart");
											}
										}
									}
									
								}						
							} else {
								if (!_.isEmpty(maginStart)) {
									if(maginStart>-10000 && maginStart<10000){
										clearTooltipForValidationDiv("maginStart");
									}
								}
								tooltipForValidationDiv(this, msgRangeMarginAccept, "top");
							}
						} else {
							tooltipForValidationDiv(this, msgInvalidValue, "top");
						}
					}				
				}
			}
			
			
		});
		
		/*$("#retailEnd, #maginEnd,#retailStart, #maginStart, #rule10").keydown(function (e) {		
			var arrValid = [46, 8, 9, 27, 13, 189, 109,110, 190];
			if($(this).attr("id") !=='maginEnd' && $(this).attr("id") !=='maginStart' && $(this).attr("id") !=='rule10'){
				arrValid = [46, 8, 9, 27, 13, 110, 190];
			}
        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, arrValid) !== -1 ||
	             // Allow: Ctrl+A
	            (e.keyCode == 65 && e.ctrlKey === true) || 
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });*/

/************************************************************************************************************
* END VALIDATION
************************************************************************************************************/	
});
/**
 * Get Rules set after changed.
 * @author: Duyen.le
 */
function getRuleChanges(){
	var listRules = [];
	var uniqueNames = [];
	var rule;
	var ruleId;
	var ruleName;
	
	var activeSw = 'Y';
	var valType = '1';
	var pctSw = 'N';
	
    $("#formHie input[name='chk-4'],input[name='chk-2'],input[name='chk-5'],input[name='chk-6'],input[name='chk-7'],input[name='chk-8'],input[name='chk-9'],input[name='chk-1'],input[name='chk-3'],input[name='chk-10']").each(function(index,data){
    	//log($(this));
    	var values = [];            	
    	ruleId = $(this).attr("id").split("chk-");
    	ruleName = $(this).attr('name').split("chk-");
    	if($(this).attr('checked')){
    		if (typeof $(this).attr("data-left-check") !== 'undefined') {
        		var index_checkbox = $(this).attr("data-left-check");
        										
				var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");
				activeSw = 'Y';
				if(checkbox_Data.length > 1){
				valType = '2';
					$.each(checkbox_Data,function (index, dInData) {								
						if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){																								
							values.push("Start:" +$(dInData).val());
							if($(dInData).attr('id') == 'retailStart'){
								pctSw = 'N';
							} else {
								pctSw = 'Y';
							}

						}	
						if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){										
							values.push("End:" +$(dInData).val());								
						}

					});
				}else{							
					if($(this).attr("name")==='chk-10'){
						pctSw = 'Y';
						valType = '2';							
						values.push(parseInt($("#rule10").val()));
					} else {
						pctSw = 'N';
						valType = '1';							
						values.push('');
					}					
				}
				rule = {
        			id: ruleId[1],
        			name: ruleName[1],
        			values:values,
        			valType : valType,
        			activeSw : activeSw,
        			pctSW : pctSw
        		}
        	}
    	} else {
    		valType = '1';	
			activeSw = 'N';										
    		if (typeof $(this).attr("data-left-check") !== 'undefined') {
        		var index_checkbox = $(this).attr("data-left-check");	            										
				var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");						
				if(checkbox_Data.length > 1){
					valType = "2";
					$.each(checkbox_Data,function (index, dInData) {                                
                        if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){
                            values.push("End:" +0);
                            if($(dInData).attr('id') == 'maginEnd'){
								pctSw = 'Y';
							} else {
								pctSw = 'N';
							}
                        }
                        if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){
                            values.push("Start:" +0);
                        }   
                    });						
				}else{								
					if($(this).attr("name")==='chk-10'){
						pctSw = 'Y';
						valType = '2';							
						values.push(0);
					} else {
						valType = '1';	
						pctSw = 'N';							
						values.push('');
					}						
				}
				rule = {							
        			id: ruleId[1],
        			name: ruleName[1],
        			values: values,
        			valType : valType,
        			activeSw : activeSw,
        			pctSW : pctSw	            			
        		}
        	}            		
    	}
    	listRules.push(rule);
    	uniqueNames = [];
		$.each(listRules, function(i, el){
		    if(el != undefined && $.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
		});
	});
	return uniqueNames;
}
/**
 * display error message by tooltip.
 * @param inputId id of element
 * @param message error 
 * @param placement the placement of message top|right|bottom|left, optional param.
 */
function tooltipForValidationDiv(selectorInput, message, placement, paramFormat){
	//console.log("tooltipForValidationDiv");
	if(paramFormat != null){
		message = message.format(paramFormat);
	}		
	$(selectorInput).closest('div').addClass("has-error");
	placement = (typeof placement === "undefined") ? "top" : placement;
	$(selectorInput).attr("data-toggle", "tooltip");
	$(selectorInput).attr("data-original-title", message);

	$(selectorInput).tooltip({
		html:true,
		placement:placement,
		trigger:'hover manual focus'
	}).tooltip('show');
}

function hideToolTip(selectorInput){
	$(selectorInput).tooltip('hide');
}
/**
 * clear error message by tooltip.
 * @param inputId id of element
 */
function clearTooltipForValidationDiv(id){			
	$("#" + id).closest('div').removeClass("has-error");
	$("#" + id).tooltip('hide');
	$("#" + id).removeAttr("data-toggle");
	$("#" + id).removeAttr("data-original-title");
}
/**
 * Get Hierachy Information.
 * @author Duyen.le
 */
	function loadTree(callback) {			
        $.ajax({
            type: 'POST',
            url: appName + "/ajax/getHierachy",                
           
            success: function (data) {
                callback(JSON.parse(data));
            },
            error: function (xhr, ajaxOptions, error) {                    
                //alert('Error: ' + xhr.responseText);
            }
        });
    }
    /**
	 * Get Hierachy Information.
	 * @author Duyen.le
	 */
	function loadCommodityRulesSet(parameters) {
		saveChangeArrAfterLoad = [];
		if($.trim(parameters.parentNodeValue) =='#'){
			subDeptId = parameters.childNodeValue;
			commodityID = '';
		}else{
			subDeptId = parameters.parentNodeValue;
			commodityID = parameters.childNodeValue;
		}
		
        $.ajax({
            type: 'POST',
            url: appName + "/ajax/commodity-loadRule",
            cache: false,
            async: false,
           	data: {
	        	subDeptId : subDeptId,
		    	commodityID : commodityID
	        },
            success: function (data) {        		
            	var lstData = JSON.parse(data).data;
            	if(lstData.length > 0){            		
            		disableAllRulesBeforeGetRuleData();
            		if(commodityID == ''){
            		// *************************LOAD RULES FOR SUBDEPT***************************//
	            		var subDeptId = lstData[0].subDeptId;
	            		if(subDeptId === '' || subDeptId === undefined){
	            			showInheritRules(lstData);
	            		} else {
	            			showSelfRules(lstData);
	            		}	            		
	            	} else {
	            		//*************************LOAD RULES FOR COMMODITY**********************//
			            var HebComdId = lstData[0].HebComdId;
			            if(HebComdId > 0){
			            	showSelfRules(lstData);
	            		} else {
	            			showInheritRules(lstData);
	            		}			
	            	}            	
            	} else {
            		hideLstUserAndTsUpdate();
            		disableAllRulesBeforeGetRuleData();	
            	}    	
		    	focusTextbox();            	
            },
            error: function (xhr, ajaxOptions, error) {                            
                //alert('Error: ' + xhr.responseText);
            }
        });
    }
	function updateCommodityRulesSet(parameters1, parameters2) {			
		changedArr = [];
		if($.trim(parameters1.parentNodeValue) =='#'){
			subDeptId = parameters1.childNodeValue;
			commodityID = '';
		}else{
			subDeptId = parameters1.parentNodeValue;
			commodityID = parameters1.childNodeValue;
		}
	    var dataRequest =  {
	    	lstRule : JSON.stringify(parameters2)
	    }
	    var count = 0;
	    _.each($(".input-sm"), function(value, key) {
	    	if ($(value).closest('div').hasClass('has-error')) {
	    		count++;
			}
	    });	    
        if (count === 0 && (subDeptId !== undefined || commodityID !== undefined)) {        	
        	//start
        	callbackConfirmSave(subDeptId,commodityID,parameters2);        	
        	// end       		
		}
    }
    
    function log(e){
    	//console.log(e);
    }
    /**
	 * Save Rules set function.
	 * @author Duyen.le
	 */
    function saveRule(subDeptId, commodityID, listRules){    	
		$.ajax({
            type: 'POST',
            url: appName + "/ajax/commodity-updateRule?subDeptId=" +  subDeptId + "&commodityID=" + commodityID,
            cache: false,
            async: false,   
           	data: JSON.stringify(listRules),
	        dataType: "json",
	        contentType: "application/json",
            success: function (data) {
            	if(data.rs == true){            		
            		showAlertSuccess('Save successfully!');            		            		
            		var lstData = data.data;            		 		
            		showLstUserAndTsUpdate(lstData);
            		removeAlertSuccess(3000);            		
            		
            		$.each($("input[type=checkbox]"),function(id, dData){
						if($(this).parent().hasClass('ibutton-container-checkmark')){
							$(this).next().removeClass('ibutton-handle-checkmark').addClass('ibutton-handle');
							$(this).parent().removeClass('ibutton-container-checkmark').addClass('ibutton-container-on');
						}
					});
					setChangeStatus(false);

					// set flag to Node ------------------
					var node = $('#treeHie').jstree("get_selected");
					var nodeData = $('#treeHie').jstree(true).get_node(node);
					var parentOfSelectedNode =  nodeData.parent;
					if(parentOfSelectedNode !== '#'){
						nodeData.a_attr.class= "rule-flag";		
						$('#treeHie').jstree(true).redraw_node(node,true);
					}					

					//$('#treeHie').jstree(true).redraw(true);
					
					// End set flag to Node ---------------
									
					if(parentOfSelectedNode !=='#'){
						$("#treeHie .jstree-container-ul li#"+parentOfSelectedNode+" .jstree-children li .jstree-anchor").each(function(d, dt){												
							var str = $(this).html();	
							var x = str.split('<i class="jstree-icon jstree-themeicon"></i>')[1];							
							if (x.length > 25) {
								$(this).attr('title',x);
								//$(this).addClass('text-ellipsis');	
								$(this).html('<i class="jstree-icon jstree-themeicon"></i>'+ x.substr(0,25) + '...');								
							}
						});
					}					
            	} else {            		
            		showAlertDanger('The system has encountered an error. Please try again later!');
            		removeAlertSuccess(3000);
            		setChangeStatus(false);
            	}
            },
            error: function (xhr, ajaxOptions, error) {                    
                //alert('Error: ' + xhr.responseText);
            }
        });		
    }
    /**
	 * Disable YES/NO checkbox if the Rule is OFF.
	 * @author Duyen.le
	 */
    function disableCheckboxOFFStatus(){
    	$.each($('input:checkbox[name^="chk"]'),function (index, dInData) {				   
		   	if(!$(dInData).attr('checked')){
		   		if($(dInData).attr("data-left-check") !=undefined){
		   			rightCheck = $(dInData).attr("data-left-check");
		   			var checkbox_Data = $("input[data-right-check="+rightCheck+"]");
		   			
		   			checkbox_Data.iButton('disable', true);
		   			checkbox_Data.prop('disabled', true);
		   		}					   		
		   	}
	   });	
    }
	/**
	 * Disable All Rules before getting Rule's Data.
	 * @author Duyen.le
	 */
    function disableAllRulesBeforeGetRuleData(){
    	$.each($('input:text'), function (index, dIndata){
    		$(dIndata).val("");
    	});
    	$.each($('input:checkbox[name^="chk-"]'),function (index, dInData) {            			
		   	if($(dInData).attr('checked')){					   		
		   		$(dInData).attr('checked', false);		
				$(dInData).iButton().trigger("change");
		   	}
	    });
	    $('#turnOnAllRule').attr('checked', false);      
    	$('#turnOnAllRule').iButton().trigger("change");
    }
    function resetToHigherLevel(subDeptId, commodityID){    	  	
        $.ajax({
            type: 'POST',
            url: appName + "/ajax/resetRuleToHigherLevel",
            cache: false,
            async: false,
            data : {
            	subDeptId : subDeptId,
            	commodityId : commodityID
            },
            success: function (data) { 
            	if(data =='true'){
            		onArr = [];
            		offArr = [];
            		showAlertSuccess('Reset successfully!');
            		removeAlertSuccess(3000);   
            		// remove flag from node ---------------------

					var node = $('#treeHie').jstree("get_selected");
					var nodeData = $('#treeHie').jstree(true).get_node(node);
					var parentOfSelectedNode =  nodeData.parent;
					if(parentOfSelectedNode !== '#'){
						nodeData.a_attr.class= "";		
						$('#treeHie').jstree(true).redraw_node(node,true);
					}
					if(parentOfSelectedNode !=='#'){
						$("#treeHie .jstree-container-ul li#"+parentOfSelectedNode+" .jstree-children li .jstree-anchor").each(function(d, dt){												
							var str = $(this).html();	
							var x = str.split('<i class="jstree-icon jstree-themeicon"></i>')[1];							
							if (x.length > 25) {
								$(this).attr('title',x);
								//$(this).addClass('text-ellipsis');
								$(this).html('<i class="jstree-icon jstree-themeicon"></i>'+ x.substr(0,25) + '...');									
							}
						});
					}
					// End remove flag from node -----------------
					saveChangeArrAssignAfterSave = [];
            		saveChangeArrAfterLoadAssign = [];
            		inheritRuleArr2 = [];
            		loadCommodityRulesSet(TreeNode);            		
            		
            		$("#global-save-btn").prop("disabled", true);
					$("#reset-toDefault-btn").prop("disabled", true);
					setChangeStatus(false);
            	} else {
            		showAlertDanger('The system has encountered an error. Please try again later!');
                	removeAlertSuccess(3000);
            	}	            
            },
            error: function (xhr, ajaxOptions, error) {                    
                //alert('Error: ' + xhr.responseText);
            }
        });
    }
/**
 * action before load screen.
 * @author Duyen.le
 */
function beforeLoadScreen(){
	$("#reset-toDefault-btn").hide();
	$("#global-save-btn").hide();
	disableCheckboxOFFStatus();
}	
function isNum(num){
	return !isNaN(num) && num<4000000000;
}
/**
 * focus on Textbox.
 * @author Duyen.le
 */
function focusTextbox(){	
	var arr = [];
	$("#retailStart, #retailEnd, #maginStart, #maginEnd").each(function(){
		if($(this).attr('disabled') === undefined){
		  arr.push($(this).attr('id'));
		}
	});
	//console.log(arr.length , arr, $.inArray("retailStart", arr) !== -1, $.inArray("maginStart", arr) !== -1);
	if(arr.length > 0){
		if($.inArray("retailStart", arr) !== -1){			
			$("#retailStart").trigger('focus');
		} else {			
			if($.inArray("maginStart", arr) !== -1){
				$("#maginStart").trigger('focus');
			} 
		}
	}
}
/**
 * Compare 2 Array.
 * @author Duyen.le
 */

function compare2Arr(Arr1, Arr2){
	var result = [];
	if(Arr1.length > 0 && Arr2.length > 0){

		
		/*$.each(Arr1, function(d, dt){
	console.log(dt);
		})	;
		console.log("------------------");
		$.each(Arr2, function(d, dt){
			console.log(dt);
		})	;*/
		

		/*var Arr1 = [];
		var Arr2 = [];
		var rule0 = {							
				id: 1,
				values: ["1","2"],
				activeSw : "Y"           			
			}
		var rule1 = {							
				id: 2,
				values: ["3","45"],
				activeSw : "Y"           			
			}
		var rule3 = {							
				id: 3,
				values: [""],
				activeSw : "N"           			
			}
		Arr1.push(rule0);
		Arr1.push(rule1);
		Arr1.push(rule3);
		var rule4 = {							
				id: 1,
				values: ["1","2"],
				activeSw : "Y"           			
			}
		var rule5 = {							
				id: 2,
				values: ["3","4"],
				activeSw : "Y"           			
			}
		var rule6 = {							
				id: 3,
				values: [""],
				activeSw : "Y"           			
			}
		Arr2.push(rule4);
		Arr2.push(rule5);
		Arr2.push(rule6);*/
		$.each(Arr1, function(d, data1){
			if(data1.activeSw || data1.values || data1.id)	{
				$.each(Arr2, function(d, data2){				
						if(data1.id === data2.id){
							
								if(data1.activeSw === data2.activeSw && data1.activeSw ==='Y'){
									var values1 = data1.values;
									var values2 = data2.values;
									if(values1.length > 1 && values2.length > 1){
										if(values1[0] !== values2[0] || values1[1] !== values2[1]) {
											result.push(data1.id);
										}
									} else {
										if(values1[0] !== values2[0]){
											result.push(data1.id);
										}
									}									
								} else if (data1.activeSw !== data2.activeSw){
									result.push(data1.id);
								}
							
						}			
				});
			} else {			
				if(data1 != undefined && $.inArray(data1, Arr2) === -1){
					result.push(data1);	
				}			
			}
		});

		//console.log("result; ",result);
	}
	return result;
}
function swap2ElementInArray(arr) {
    return [arr[1], arr[0]];
}
/*function roundingFloatNum(num){
    return parseFloat(Math.round(num * 100) / 100).toFixed(2);
}*/
function removeAlertSuccess(time) {		
	setTimeout(function(){
	$("#alert-msg").css('display','none');
	$("#message").html('');
	}, time);
}

function showAlertDanger(errorMessage) {
	 //remove class
	
		$("#alert-msg").css('display','');
		$("#alert-msg").find('.alert').removeClass('alert-success');
		 //add class
		 $("#alert-msg").find('.alert').addClass('alert-danger');
		 //set message
		 $("#message").html(errorMessage);
	
}

function showAlertSuccess(errorMessage) {
	
	 $("#alert-msg").css('display','');
	 $("#alert-msg").find('.alert').addClass('alert-success');
	 //add class
	 $("#alert-msg").find('.alert').removeClass('alert-danger');	 
	 //set message
	 $("#message").html(errorMessage);
	
}
function openDialogConfirmWhenSave(callbackFn){	
	bootbox.dialog({
		title : "Confirmation",
		message: msgSaveConfirm,
		buttons: {
			main: {
		      label: "Yes",
		      className: "btn-primary",
		      callback: function() {		        
		        callbackFn(true);
		      }
		    },			
		    cancel: {
		      label: "No",
		      className: "btn-default",
		      callback: function() {
		        callbackFn(false);
		      }
		    }		    	    
	  	}		
	});
}
function callbackConfirmSave(subDeptId,commodityID,parameters2){
	openDialogConfirmWhenSave(function(result){
		if(result){
			saveRule(subDeptId,commodityID,parameters2);
			saveChangeArrAfterSave = parameters2;
			saveChangeArrAssignAfterSave = saveChangeArrAfterSave;
			saveChangeArrAfterLoadAssign = [];
			$("#global-save-btn").prop("disabled", true);
			$("#reset-toDefault-btn").prop("disabled", false);			
		}
	}); 
}
function disableSaveButton(saveChangeArrAfterLoadAssign){
	$("#global-save-btn").prop("disabled", true);
	if(saveChangeArrAfterLoadAssign.length > 0){
		$("#reset-toDefault-btn").prop("disabled", false);
	}
}
function truncate(div, maxLength, surfix) {
	var str = div.html();	
	var x = str.split('<i class="jstree-icon jstree-themeicon"></i>')[1];
	if (x.length > maxLength) {
		div.attr('title',x);
		x = x.substring(0, maxLength + 1);		
		x = x.substring(0, Math.min(x.length, x.lastIndexOf(" ")));
		//x = x.substring(0, Math.min(x.length, maxLength-20));
		x = x + surfix;		
	}
	return x;
}
function checkChangeRule(onArr){	
	var changesRuleArr = getRuleChanges();
    if(saveChangeArrAssignAfterSave.length > 0){
        var changedArr = compare2Arr(saveChangeArrAssignAfterSave,changesRuleArr);
        if(changedArr.length > 0){
            setChangeStatus(true);
            $("#global-save-btn").prop("disabled", false);
        } else {
            setChangeStatus(false);
            $("#global-save-btn").prop("disabled", true);
        }
    } else {
        var changesArr = compare2Arr(changesRuleArr, saveChangeArrAfterLoadAssign);
        //var inheritArr = $("#formHie").find(".ibutton-container-checkmark").length;        
        if(saveChangeArrAfterLoadAssign.length<=0){  
        	// check ON/OFF Flag Incase have no rules/inherit Rules
            checkChangeFlagNoAnyRules();            
        } else if(changesArr.length>0) {
            $("#global-save-btn").prop("disabled", false);
            setChangeStatus(true);
        } else {
            $("#global-save-btn").prop("disabled", true);
            setChangeStatus(false);
        }
    }
}

function showLstUserAndTsUpdate (lstData) {
	$("#showUserAndTimeUpdate").css('display','');
	$("#uidAndTsUpdate").html("Last updated by <label style='font-weight: normal'>"+lstData[0].lstUpdateUid+"</label>" + " on <label style='font-weight: normal'>" + lstData[0].lstUpdateTs+"</label>");    
}
function hideLstUserAndTsUpdate () {
	$("#showUserAndTimeUpdate").css('display','none');
}

function getRuleON(){
	var listRules = [];
	var uniqueNames = [];
	var rule;
	var ruleId;	
	var activeSw = 'Y';	
    $("#formHie input[name^='chk-']").each(function(index,data){
    	var values = [];            	
    	ruleId = $(this).attr("id").split("chk-");    	
    	if($(this).attr('checked')){
    		if (typeof $(this).attr("data-left-check") !== 'undefined') {
        		var index_checkbox = $(this).attr("data-left-check");
        										
				var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");
				activeSw = 'Y';
				if(checkbox_Data.length > 1){
					$.each(checkbox_Data,function (index, dInData) {								
						if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){																								
							values.push("Start:" +$(dInData).val());
						}	
						if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){										
							values.push("End:" +$(dInData).val());								
						}
					});
				}else{							
					if($(this).attr("name")==='chk-10'){						
						values.push(parseInt($("#rule10").val()));
					} else {						
						values.push('');
					}					
				}
				rule = {
        			id: ruleId[1],
        			values:values,
        			activeSw : activeSw
        		}
        	}
    	}
    	listRules.push(rule);
    	uniqueNames = [];
		$.each(listRules, function(i, el){
		    if(el != undefined && $.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
		});
	});

	return uniqueNames;
}
function getRuleOff(){
	var listRules = [];
	var uniqueNames = [];
	var rule;
	var ruleId;	
	var activeSw = 'N';	
    $("#formHie input[name^='chk-']").each(function(index,data){
	    var values = [];
	    ruleId = $(this).attr("id").split("chk-");
	    if($(this).attr('checked') === undefined){
		    	if (typeof $(this).attr("data-left-check") !== 'undefined') {
				var index_checkbox = $(this).attr("data-left-check");	            										
				var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");						
				if(checkbox_Data.length > 1){
					$.each(checkbox_Data,function (index, dInData) {                                
		                if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){
		                    values.push("End:" +0);                    
		                }
		                if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){
		                    values.push("Start:" +0);
		                }   
		            });						
				}else{								
					if($(this).attr("name")==='chk-10'){						
						values.push(0);
					} else {						
						values.push('');
					}						
				}
				rule = {							
					id: ruleId[1],
					values: values,
					activeSw : activeSw	            			
				}
			}
			listRules.push(rule);
			uniqueNames = [];
			$.each(listRules, function(i, el){
			    if(el != undefined && $.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
			});
	    }	
	});
	return uniqueNames;
}

function getRuleWhenTurnAll(on){
	var listRules = [];
	if(on){ 
		var activeSw = 'Y';	
		listRules = forEachRule(activeSw);
	} else {
		var activeSw = 'N';	
		listRules = forEachRule(activeSw);
	}
	return listRules;
}

function forEachRule(activeSw){		
	var rule;
	var ruleId;
	var listRules = [];
	$("#formHie input[name^='chk-']").each(function(index,data){		
	    var values = [];
	    ruleId = $(this).attr("id").split("chk-");
	    	if (typeof $(this).attr("data-left-check") !== 'undefined') {
			var index_checkbox = $(this).attr("data-left-check");	            										
			var checkbox_Data = $("input[data-right-check='"+index_checkbox+"']");						
			if(checkbox_Data.length > 1){
				$.each(checkbox_Data,function (index, dInData) {                                
	                if($(dInData).attr('id') == 'retailEnd' || $(dInData).attr('id') == 'maginEnd'){
	                    values.push("End:" +0);                    
	                }
	                if($(dInData).attr('id') == 'retailStart' || $(dInData).attr('id') == 'maginStart' ){
	                    values.push("Start:" +0);
	                }   
	            });						
			}else{								
				if($(this).attr("name")==='chk-10'){						
					values.push(0);
				} else {						
					values.push('');
				}						
			}
			rule = {							
				id: ruleId[1],
				values: values,
				activeSw : activeSw	            			
			}
		}
		listRules.push(rule);				
	    	
	});
	return listRules;
}
function cancalCallBackSelectNode(){
	var rootItemTree = $("#treeHie .jstree-container-ul li#"+currentNode+" > .jstree-anchor");
  	var childItemsTree = $("#treeHie .jstree-container-ul .jstree-children li#"+currentNode+" .jstree-anchor");
  	if(rootItemTree.length>0){
  		rootItemTree.trigger('click');

  	}					
  	if (childItemsTree.length > 0){
  		childItemsTree.trigger('click');	
  	}
	$("#global-save-btn").prop("disabled", false);
	if(saveChangeArrAfterLoadAssign.length<=0){
		$("#reset-toDefault-btn").prop("disabled", true);													
	} else {
		$("#reset-toDefault-btn").prop("disabled", false);
	}
	if(saveChangeArrAssignAfterSave.length>0){
		var changedArr = compare2Arr(saveChangeArrAssignAfterSave,getRuleChanges());																				        		
		if(changedArr.length > 0){								        			
			$("#reset-toDefault-btn").prop('disabled', false);
		}
	}
}
function showInheritRules(lstData){
	var countChk = 0;
	$.each(lstData,function (index, dInData) {
		var values= [];  	            			
		var lstValues = dInData.values;
		var lstSeqNbr = dInData.seqNbr;

		if(dInData.activeSw=='Y'){
			$('input:checkbox[name=chk-'+dInData.id+']').attr('checked',true);		    			
			$('input:checkbox[name=chk-'+dInData.id+']').iButton().trigger("change");
			if(lstValues.length > 1){
				// 2.Rule for Retail Between and Margin Between.
				$.each(lstSeqNbr,function (index, seqNbr) {			    								  	  					
    					if(seqNbr=='1'){
		    				if(dInData.id==2){
		    					$("#retailStart").val(precise_roundFloatNum(lstValues[0]));
		    					values.push("Start:"+precise_roundFloatNum(lstValues[0]));		
		    				}
		    				if(dInData.id==3){
		    					$("#maginStart").val(precise_roundFloatNum(lstValues[0]));	
		    					values.push("Start:"+precise_roundFloatNum(lstValues[0]));		
		    				}
		    			}
		    			if(seqNbr=='2'){						    				
		    				if(dInData.id==2){
		    					$("#retailEnd").val(precise_roundFloatNum(lstValues[1]));
		    					values.push("End:"+precise_roundFloatNum(lstValues[1]));
		    				}
		    				if(dInData.id==3){
		    					$("#maginEnd").val(precise_roundFloatNum(lstValues[1]));
		    					values.push("End:"+precise_roundFloatNum(lstValues[1]));
		    				}
		    			}
	    			// End block code [2]					    			
				});
				values = swap2ElementInArray(values);
			} else {
				if(dInData.id ==='10'){				    									    					
					$("#rule10").val(parseInt(lstValues));
					values.push(parseInt(lstValues[0]));
				} else {
					values.push('');
				}
			}
			var RuleCheck = {
	     			id: dInData.id,
	     			values: values,
	     			activeSw: dInData.activeSw
	     		}
			// 3.Indicate isInherit status of Rule		    			
			$('input:checkbox[name=chk-'+dInData.id+']').next().removeClass("ibutton-handle").addClass("ibutton-handle-checkmark");
			$('input:checkbox[name=chk-'+dInData.id+']').parent().removeClass("ibutton-container-on").addClass("ibutton-container-checkmark");
			
			inheritRuleArr.push(RuleCheck);
			inheritRuleArr2 = inheritRuleArr;
		} else {
			//dInData.activeSw=='N'
			offFirstLoadArr.push(dInData.id);
		}
		hideLstUserAndTsUpdate();
	});
	$.each($('input:checkbox[name^="chk-"]'),function (index, dInData) {                        
        if($(dInData).attr('checked')){                         
           countChk ++;
        }
    });	            
    if(countChk == 9) {
        $('#turnOnAllRule').attr('checked', true);      
        $('#turnOnAllRule').iButton().trigger("change");
    }
}
function showSelfRules(lstData){
	var countChk = 0;
	$.each(lstData,function (index, dInData) {        		
		var HebComdId = dInData.HebComdId;		            		
		var lstValues = dInData.values;
		var lstSeqNbr = dInData.seqNbr;
		// create arr Rule to check if have any changes
     	var values= [];
     	// end ---------------					     
		if(dInData.activeSw=='Y'){
			$('input:checkbox[name=chk-'+dInData.id+']').attr('checked',true);		    			
			$('input:checkbox[name=chk-'+dInData.id+']').iButton().trigger("change");
			if(lstValues.length > 1){
				$.each(lstSeqNbr,function (index, seqNbr) {			    								  	  					
    					if(seqNbr==1){
		    				if(dInData.id==2){
		    					$("#retailStart").val(precise_roundFloatNum(lstValues[0]));	
		    					values.push("Start:"+precise_roundFloatNum(lstValues[0]));	
		    				}
		    				if(dInData.id==3){
		    					$("#maginStart").val(precise_roundFloatNum(lstValues[0]));		
		    					values.push("Start:"+precise_roundFloatNum(lstValues[0]));							    					
		    				}
		    				
		    			}
		    			if(seqNbr==2){					    				
		    				if(dInData.id==2){
		    					$("#retailEnd").val(precise_roundFloatNum(lstValues[1]));
		    					values.push("End:"+precise_roundFloatNum(lstValues[1]));
		    				}
		    				if(dInData.id==3){
		    					$("#maginEnd").val(precise_roundFloatNum(lstValues[1]));
		    					values.push("End:"+precise_roundFloatNum(lstValues[1]));
		    				}
		    				
		    			}
	    			// End block code [2]					    			
				});
				values = swap2ElementInArray(values);		    			
			} else {				    				
				if(dInData.id ==='10'){					    					
					$("#rule10").val(parseInt(lstValues));
					values.push(parseInt(lstValues[0]));
				} else {
					values.push('');
				}
			}
			var RuleCheck = {
     			id: dInData.id,
     			values: values,
     			activeSw: dInData.activeSw
     		}						     		
		} else {
			$('input:checkbox[name=chk-'+dInData.id+']').attr('checked',false);		    			
			$('input:checkbox[name=chk-'+dInData.id+']').iButton().trigger("change");
			
			if(dInData.id === 2 || dInData.id ===3){	            					
				values.push("Start:"+0);	
				values.push("End:"+0);	
			} else if(dInData.id === 10){
				values.push(0);
			} else {
				values.push('');
			}
			var RuleCheck = {
     			id: dInData.id,
     			values: values,
     			activeSw: dInData.activeSw
     		}
     		
		}
		saveChangeArrAfterLoad.push(RuleCheck);
	});
	
	$.each($('input:checkbox[name^="chk-"]'),function (index, dInData) {                        
        if($(dInData).attr('checked')){                         
           countChk ++;
        }
    });	            
    if(countChk == 9) {
        $('#turnOnAllRule').attr('checked', true);      
        $('#turnOnAllRule').iButton().trigger("change");
    }
    // end Sub Dept
    showLstUserAndTsUpdate(lstData);
}
function checkChangeFlagNoAnyRules(){
    var inheritRuleArrLocal = [];
    var inheritFlagArr = [];
    $("#formHie input[name^='chk-']").each(function(d,dt){
        if($(dt).parent().hasClass('ibutton-container-checkmark')){
            inheritFlagArr.push($(dt).attr('name').split('-')[1]);
        }
    });
    
     if(onArr.length > 0 ){
        $.each(onArr, function(d, id){
            var idexOfItem = inheritFlagArr.indexOf(id);
            if(idexOfItem > -1){
                inheritFlagArr.splice(idexOfItem,1);    
            }            
        });
     }     
     

     if(offArr.length > 0){
        $.each(offArr, function(d, id){
            var idexOfItem = inheritFlagArr.indexOf(id);
            if(idexOfItem > -1){
                inheritFlagArr.splice(idexOfItem,1);    
            }
        });
     }     
     
     $.each(inheritRuleArr2, function(d, dt){
        inheritRuleArrLocal.push(dt.id);
     });
     var onArrUnique = [];
     $.each(onArr, function(d,dt){
     	if($.inArray(dt,onArrUnique) === -1){
     		onArrUnique.push(dt);
     	}
     });
    onArr = onArrUnique;  
    
    var offFirstLoadArrUnique = [];
     $.each(offFirstLoadArr, function(d,dt){
     	if($.inArray(dt,offFirstLoadArrUnique) === -1){
     		offFirstLoadArrUnique.push(dt);
     	}
     });
    offFirstLoadArr = offFirstLoadArrUnique;      
    //console.log("test : ", onArr, offArr, inheritRuleArrLocal , inheritFlagArr, offFirstLoadArr);
     				//		[]    ["8"]    []                   ["5", "6", "7"] ["3", "2", "1", "4", "9"]

    if(inheritFlagArr.length >0){
    	var compareChangeInher = compare2Arr(inheritRuleArrLocal,inheritFlagArr);
	    if(compareChangeInher.length>0){
	        $("#global-save-btn").prop("disabled", false);
	        setChangeStatus(true);
	    } else {
	        if(onArr.length>0){
	            $("#global-save-btn").prop("disabled", false);
	            setChangeStatus(true);
	        } else {
	        	if(inheritRuleArrLocal.length <= 0 && offArr.length > 0){
	        		var compareChangeInher1 = compare2Arr(offArr,inheritFlagArr);
	        		var compareChangeInher2 = compare2Arr(offArr,offFirstLoadArr);	
	        		if(compareChangeInher2.length >0){
	        			if(compareChangeInher1.length >0){
	        				$("#global-save-btn").prop("disabled", false);
	            			setChangeStatus(true);
	        			}
	        		} else {
	        			if(compareChangeInher1.length >0){
	        				$("#global-save-btn").prop("disabled", true);
	            			setChangeStatus(false);
	        			}
	        		}
	        		
	        	} else {
	        		$("#global-save-btn").prop("disabled", true);
	            	setChangeStatus(false);
	        	}
	            
	        }        
	    }
	    
    } else {
    	var compareChangeInher = compare2Arr(onArr,inheritRuleArrLocal);
    	var compareChangeOff = compare2Arr(offArr,offFirstLoadArr);
    	
    	if(compareChangeInher.length>0 || compareChangeOff.length >0){
    		$("#global-save-btn").prop("disabled", false);
	        setChangeStatus(true);
    	} else {
    		$("#global-save-btn").prop("disabled", true);
	        setChangeStatus(false);
    		/*if(inheritRuleArrLocal.length > 0){ console.log("####333 2");    			
    				if(inheritFlagArr.length <= 0 ){ console.log("####333 4");
    					$("#global-save-btn").prop("disabled", false);
	        			setChangeStatus(true);
    				}    			
    		}*/
    		
    	}
    }
    
}
