var Constants = {
		EMPTY_STRING : "",
		SPACE_STRING : " ",
		AFTER_DIGIT: 2,
		TOTAL_NUMBER: 9, 
		TOTAL_NUMBER_INTEGER: 38, 
		INVALID_VALUE : "Invalid value",
		INVALID_MARGIN : "Maximum margin should be greater than Minimum margin",
		INVALID_DATE : '"To date"  should be greater than "From date"',
		REJECT_PRODS_PATH : "/product-review/rejectProduct",
		APPROVE_PRODS_PATH : "/product-review/approveProduct",
		UN_REJECT_PRODS_PATH : "/product-review/un-rejectProduct",
		SAVE_PRODS_PATH : "/product-review/updateProduct",
		AJAX_COMMONDITIES : "/ajax/get-data-autocomplete-commodities",
		AJAX_VENDOR : "/ajax/get-data-autocomplete-vendor",
		AJAX_BRAND : "/ajax/get-data-autocomplete-brand",
		AJAX_PRODUCT : "/ajax/get-data-autocomplete-product",
		msgLoseChange :"You will lose your changes, do you wish to continue?",
		msgHebRetailLimit : "Only the value between 0 and 100000 is accepted",
		msgRequiredInput : "This field is required."
};
$(document).ready(function() {
	/*show competitor*/
	$(document).on("mouseover",".show-competitor", function () {
	    $(this).popover('show');
	});

	$(document).on("mouseout",".show-competitor", function () {
	    $(this).popover('hide');
	});
	/*end show competitor*/
	/*INIT VALIDATION */
	$(document).on('keypress', '.input-decimal', function(event){
	 	acceptDecimalNumber(this, event, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
	});
	$(document).on('keyup input', '.input-decimal', function(event){
		validateDecimalBDM(this, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
	});
	$(document).on('change', '.input-decimal', function(event){
	 	var val = $.trim($(this).val()).replace(/,/g, "");
	 	if(isDecimalNumberBDM(val, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT) && !_.isEmpty($(this).val())){
	 		$(this).val(parseFloat($.trim(val), 2).toFixed(2));
	 	}
	});

	$(document).on('keypress', '.negative-number', function(event){
	 	acceptNegativeDecimalNumberBDM(this, event, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
	});
	$(document).on('keyup input', '.negative-number', function(event){
		validateNegativeDecimalBDM(this, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
	});
	$(document).on('change ', '.negative-number', function(event){
	 	var val = $.trim($(this).val());
	 	if(isDecimalNegativeNumberBDM(val, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT) && !_.isEmpty($(this).val())){
	 		$(this).val(parseFloat($.trim(val), 2).toFixed(2));
	 	}
	});

	$(document).on('keypress', '.input-number', function(event){
	 	acceptNumberBDM(this, event, 38);
	});
	$(document).on('keyup input', '.input-number', function(event){
		validateIntegerBDM(this,38);
	});
	$(document).on('keypress', '#dateBegin, #dateEnd', function(event){
		var charCode = (event.which) ? event.which : event.keyCode;
		if((charCode >= 97 && charCode <= 122) || (charCode >= 65 && charCode <= 90) || charCode === 32) {
			event.preventDefault();
		}
	});
	$(document).on('blur', '#marginBegin, #marginEnd', function(event){
		compareMarginBDM("#marginBegin","#marginEnd", false );
	});

	forceBeginDateAndEndDate("#dateBegin", "#dateEnd");
	/*END VALIDATION */
});
/*INIT TOOLTIP*/
function tooltipForValidationBDM(selectorInput, message, placement){
     	//debugger;
     	$(selectorInput).parent().addClass("has-error");
     	placement = (typeof placement === "undefined") ? "top" : placement;
     	$(selectorInput).attr("data-toggle", "tooltip");
     	$(selectorInput).attr("data-original-title", message);
     	$(selectorInput).tooltip({'placement':placement, 'trigger':'hover manual'});
     	$(selectorInput).tooltip('show');

}

function clearTooltipForValidationBDM(selectorInput){
		$(selectorInput).parent().removeClass("has-error");
		$(selectorInput).tooltip('hide');
		$(selectorInput).removeAttr("data-toggle");
		$(selectorInput).removeAttr("data-original-title");
}
/*END TOOLTIP*/
/*INIT NUMBER VALIDATION*/
function acceptNumberBDM(element, keyPressEvent, maxDigit){
		var valueInput = $(element).val();
		var charCode = (keyPressEvent.which) ? keyPressEvent.which : keyPressEvent.keyCode;
		var isMainKey = $.inArray(charCode, [8, 9, 13, 46, 37, 38, 39, 40]) !== -1; // enter, backspace
		var isNumberKey = (charCode > 47 && charCode < 58);
		var isCtrl  = false;
		var chr    = String.fromCharCode(keyPressEvent.which);
		var isSpecial  = ("~!@#$%^&*()_=+/?<>,[]{}\|".indexOf(chr) >= 0) ? true : false;
	 	if(keyPressEvent.ctrlKey) isCtrl = true;
		if(isCtrl && ( $.inArray(charCode, [80, 97, 99, 118]) !== -1)){
	      	return true;
		}
		if((!isNumberKey && !isMainKey) || isSpecial) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isNumberKey) {
			if(valueInput.length >= maxDigit && getInputSelection(element).end === getInputSelection(element).start) {
				keyPressEvent.preventDefault();
				return false;
			}
		}
}
function validateIntegerBDM(element, maxDigit) {
		var intRegex = /^\d+$/;
		var isValid = true;
		var valueTxt = $.trim($(element).val());
		var idItem = $(element).attr('id');
		if(!(valueTxt === null || valueTxt === Constants.EMPTY_STRING) && !intRegex.test(valueTxt) || valueTxt.length > maxDigit) {
			tooltipForValidationBDM(element , Constants.INVALID_VALUE);
			isValid = false;
		}else {
			$(element).parent().removeClass("has-error");
			clearTooltipForValidationBDM(element);
		}
		return isValid;
}
/*END NUMBER VALIDATION*/
/*INIT DECIMAL VALIDATION*/
function isDecimalNumberBDM(value, totalNumber, afterDigits) {
	 	var isValid = false;
	 	var patt = /^\d*\.?\d*$/;
	 	var numberStr = "";
	 	var value = $.trim(value);
	 	if(isNaN(parseFloat(value))) {
	 	 	return false;
	 	}
	 	if(!isNaN(parseFloat(value))) {
	 	 	numberStr = parseFloat(value).toFixed(afterDigits).toString();
	 	}
	 	if(patt.test(value)) {
	 		if((numberStr.split('.').length === 1) 
	 			|| (((numberStr.split('.')[0].length + numberStr.split('.')[1].length) <= totalNumber) 
	 				&& numberStr.split('.')[1].length <= afterDigits)) {
	 			isValid = true;
	 		}
	 	}
	 	return isValid;
}
function validateDecimalBDM(element, totalNumber, afterDigits) {
		var isValid = true;
		var valueTxt = $.trim($(element).val());
		valueTxt = valueTxt.replace(/,/g, "");
		var idItem = $(element).attr('id');
		if(!(valueTxt === null || valueTxt === Constants.EMPTY_STRING) && !isDecimalNumberBDM(valueTxt, totalNumber, afterDigits)) {
			tooltipForValidationBDM(element , Constants.INVALID_VALUE);
			isValid = false;
		}else {
			clearTooltipForValidationBDM(element);
		}
		return isValid;
}
/*END DECIMAL VALIDATION*/

/*INIT NEGATIVE DECIMAL VALIDATION*/
function acceptNegativeDecimalNumberBDM(element, keyPressEvent, totalDigitsLimit, afterDigitsLimit){
		totalDigitsLimit = typeof totalDigitsLimit !== 'undefined' ? totalDigitsLimit : Constants.TOTAL_NUMBER;
		afterDigitsLimit = typeof afterDigitsLimit !== 'undefined' ? afterDigitsLimit : Constants.AFTER_DIGIT;
		// Control key down, just allow decimal number(totalDigitsLimit, afterDigitsLimit).
		var valueInput = $(element).val();
		var charCode = (keyPressEvent.which) ? keyPressEvent.which : keyPressEvent.keyCode;
		var chr    = String.fromCharCode(keyPressEvent.which);
		var isSpecial  = ("~!@#$%^&*()_=+/?<>,[]{}\|".indexOf(chr) >= 0) ? true : false;
		var isDotKey = charCode === 46;
		var isSubtract = charCode === 45;	
		var isMainKey = $.inArray(charCode, [8, 9, 13, 35, 36, 37, 38, 39, 40]) !== -1; // enter, backspace
		var isNumberKey = (charCode > 47 && charCode < 58);
		var isCtrl  = false;
	 	if(keyPressEvent.ctrlKey) isCtrl = true;
		if(isCtrl && ( $.inArray(charCode, [80, 97, 99, 118]) !== -1)){
	      	return true;
	    }
		if((!isDotKey && !isSubtract  && !isNumberKey && !isMainKey) || isSpecial) {
			keyPressEvent.preventDefault();
			return false;
		}
		var selectionStart = getInputSelection(element).start;
		var selectionEnd = getInputSelection(element).end;
		var isSelected   = (element.selectionStart == 0 && element.selectionEnd == element.value.length) ? true : false;
		if(isSelected) {
			return true;
		}
		if(isNumberKey || isDotKey || isSubtract) {
			var beforePoint = 0;
			var afterPoint = 0;
			if(valueInput.split('.')[0] != null) {
				beforePoint = valueInput.split('.')[0].length;
			}
			if(valueInput.split('.')[1] != null) {
				afterPoint = valueInput.split('.')[1].length;
			}
			if(valueInput.lastIndexOf('-') >= 0) {
				beforePoint = beforePoint - 1;
			}
			var totalDigits = beforePoint + afterPoint;
			if(isNumberKey && (valueInput.lastIndexOf('.') < 0) && (beforePoint >= (totalDigitsLimit - afterDigitsLimit)) && (selectionStart === selectionEnd)) {
				keyPressEvent.preventDefault();
				return false;
			}
			if(isNumberKey && (valueInput.lastIndexOf('.') >= 0) && (beforePoint >= (totalDigitsLimit - afterDigitsLimit)) && (selectionStart === selectionEnd) && (selectionStart < valueInput.lastIndexOf('.'))) {
				keyPressEvent.preventDefault();
				return false;
			}
			if(!isSubtract && totalDigits >= totalDigitsLimit && (selectionStart === selectionEnd)) {
				keyPressEvent.preventDefault();
				return false;
			}
			if(isNumberKey && (valueInput.lastIndexOf('.') < 0) && (valueInput.length >= totalDigitsLimit) && (selectionStart === selectionEnd)) {
				keyPressEvent.preventDefault();
				return false;
			}
			if((isNumberKey || isDotKey ) && selectionStart <= valueInput.lastIndexOf('-')) {
				keyPressEvent.preventDefault();
				return false;
			}
			if(isDotKey && (valueInput.lastIndexOf('.') >= 0) && (selectionStart === selectionEnd)) {
				keyPressEvent.preventDefault();
				return false;
			}
			if(isDotKey && (valueInput.lastIndexOf('.') < 0) && (valueInput.length - selectionEnd > afterDigitsLimit)) {
				keyPressEvent.preventDefault();
				return false;
			}
			if(!isSubtract && (valueInput.lastIndexOf('.') >= 0) && (selectionStart === selectionEnd) && (selectionEnd - valueInput.lastIndexOf('.') > afterDigitsLimit)) {
				keyPressEvent.preventDefault();
				return false;
			}
			if(!isSubtract && (valueInput.lastIndexOf('.') >= 0) && (valueInput.lastIndexOf('.') < selectionEnd) && (afterPoint >= afterDigitsLimit) && (selectionStart === selectionEnd)) {
				keyPressEvent.preventDefault();
				return false;
			}
			if(isSubtract && (valueInput.lastIndexOf('-') >= 0 || selectionStart > 0)) {
				keyPressEvent.preventDefault();
				return false;
			}
		}

		
}
function isDecimalNegativeNumberBDM(value, totalNumber, afterDigits) {
	 	var isValid = true;
	 	if(!_.isEmpty(value)) {
	 		if(!isNaN(parseFloat(value)) && isFinite(value)) {
	 			var numberStr = parseFloat($.trim(value)).toFixed(afterDigits).toString();
	 			if((numberStr.indexOf('-') > -1 && numberStr.length > 11) || (numberStr.indexOf('-') === -1 && numberStr.length > 10)) {
	 				isValid = false;
	 			}
	 		} else {
	 			isValid = false;
	 		}
	 	}
	 	return isValid;
}

function validateNegativeDecimalBDM(element, totalNumber, afterDigits) {
		var isValid = true;
		var valueTxt = $.trim($(element).val());
		valueTxt = valueTxt.replace(/,/g, "");
		var idItem = $(element).attr('id');
		if(!(valueTxt === null || valueTxt === Constants.EMPTY_STRING) && !isDecimalNegativeNumberBDM(valueTxt, totalNumber, afterDigits)) {
			tooltipForValidationBDM(element , Constants.INVALID_VALUE);
			isValid = false;
		}else {
			clearTooltipForValidationBDM(element);
		}
		return isValid;
}
/*END NEGATIVE DECIMAL VALIDATION*/
/*INIT COMPARE DATE FUNCTION*/ 
function compareDateBDM(dateBeginId, dateEndId, isCompare) {
		var dateBeginValue = $(dateBeginId).val();
		var dateEndValue = $(dateEndId).val();
		if(validateDateBDM(dateBeginId) && validateDateBDM(dateEndId)) {
			if(isCompare && (!_.isEmpty(dateBeginValue) && _.isEmpty(dateEndValue))){
				tooltipForValidationBDM(dateEndId, Constants.msgRequiredInput,'top');
			} else if(isCompare && (_.isEmpty(dateBeginValue) && !_.isEmpty(dateEndValue) )){
				tooltipForValidationBDM(dateBeginId, Constants.msgRequiredInput,'top');
			} else if(!_.isEmpty(dateBeginValue) && !_.isEmpty(dateEndValue) ){
				var dateBeginD = new Date($(dateBeginId).val());
				var dateEndD = new Date($(dateEndId).val());
				if(dateEndD <= dateBeginD){
					tooltipForValidationBDM(dateEndId, Constants.INVALID_DATE,'top');
				}
			}
		}
}
/*END COMPARE DATE FUNCTION*/ 
/*INIT COMPARE MARGIN FUNCTION*/ 
function compareMarginBDM(marginBeginId, marginEndId, isCompare) {
		var marginBeginValue = $(marginBeginId).val();
		var marginEndValue = $(marginEndId).val();
		if(validateNegativeDecimalBDM(marginBeginId, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT) && validateNegativeDecimalBDM(marginEndId, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT)){
			if(isCompare && (!_.isEmpty(marginBeginValue) && _.isEmpty(marginEndValue))){
				tooltipForValidationBDM(marginEndId, Constants.msgRequiredInput,'top');
			} else if(isCompare && (_.isEmpty(marginBeginValue) && !_.isEmpty(marginEndValue))){
				tooltipForValidationBDM(marginBeginId, Constants.msgRequiredInput,'top');
			} else if( !_.isEmpty(marginBeginValue) && !_.isEmpty(marginEndValue) 
				&& parseFloat(marginBeginValue) >= parseFloat(marginEndValue)){
				tooltipForValidationBDM(marginEndId, Constants.INVALID_MARGIN,'top');
			}
		}
}
/*END COMPARE MARGIN FUNCTION*/

function validateDateBDM(element) {
		var isValid = true;
		var valueTxt = $.trim($(element).val());
		var idItem = $(element).attr('id');
		if(!(valueTxt === null || valueTxt === Constants.EMPTY_STRING) && !isDateBDM(valueTxt)) {
			tooltipForValidationBDM(element , Constants.INVALID_VALUE);
			isValid = false;
		}else {
			clearTooltipForValidationBDM(element);
		}
		return isValid;

}
function isDateBDM(s) {
		var regexp = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
		var test = regexp.test(s);
		if(regexp.test(s)) {
			var bits = s.split('/');
			var y = bits[2], m  = bits[0], d = bits[1];
			var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
			if ( (!(y % 4) && y % 100) || !(y % 400)) {
			  daysInMonth[1] = 29;
			}
			var mTemp = m-1;
			return parseInt(y) > 0 && parseInt(m, 10) > 0 && parseInt(m, 10) < 13 && parseInt(d, 10) > 0 && d <= daysInMonth[mTemp];
		}else {
			return false;
		}
}
function forceBeginDateAndEndDate(beginDateId, endDateId){
		var startDate = new Date();
		var endDateE = new Date();
		endDateE.setDate(endDateE.getDate()+365);
		$(beginDateId).datepicker({
		    weekStart: 1,
		    autoclose: true,
		    // defaultDate : '-1m',
		    forceParse: false
		}).on('changeDate', function(selected){
			if(typeof selected.date !== 'undefined'){
				startDate = new Date(selected.date.valueOf());
			    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf()))+1);
			    $(endDateId).datepicker('setStartDate', startDate);
			} else {
		    	$(endDateId).datepicker('setStartDate', "");
			}

	    }).on("blur", function(){
			if($(this).val() === Constants.EMPTY_STRING ) {
		    	$(endDateId).datepicker('setStartDate', "");
		    } 
		    compareDateBDM(beginDateId, endDateId, false);
		}).on('keyup change', function(e) {
			if($(this).val() === ""){
				var startDatev = new Date();
				startDatev.setDate(startDatev.getDate()-7);
				var beginDate = $.datepicker.formatDate('mm/dd/yy', startDatev);
	    		$(this).datepicker('setDates', beginDate).val('');
	    	}
	    	validateDateBDM(beginDateId);
	    }); 

		$(endDateId).datepicker({
	        weekStart: 1,
	        autoclose: true,
	        forceParse: false
		}).on('changeDate', function(selected){
			if(typeof selected.date !== 'undefined'){
				endDateE = new Date(selected.date.valueOf());
			    endDateE.setDate(endDateE.getDate(new Date(selected.date.valueOf())) - 1);
			    $(beginDateId).datepicker('setEndDate', endDateE);
			} else {
				endDateE = new Date();
		    	endDateE.setDate(endDateE.getDate(new Date()) - 1);
		    	$(beginDateId).datepicker('setEndDate', endDateE);
			}
		 
		}).on("blur", function(){
			if($(this).val() === Constants.EMPTY_STRING) {
				endDateE = new Date();
		    	endDateE.setDate(endDateE.getDate(new Date()) - 1);
		    	$(beginDateId).datepicker('setEndDate', endDateE);
		    } 
		    compareDateBDM(beginDateId, endDateId, false);
		}).on('keyup change', function(e) {
	    	if($(this).val() === ""){
	    		var today = $.datepicker.formatDate('mm/dd/yy', new Date())
				$(this).datepicker('setDates', today).val('');
	    	}
	    	validateDateBDM(endDateId);
	    });

	    $(endDateId).datepicker('setEndDate', new Date());
 		var dateBeginValue = new Date();
		dateBeginValue.setDate(dateBeginValue.getDate() - 1);
		$(beginDateId).datepicker("setEndDate", dateBeginValue);
		$(beginDateId+"-icon-wp").click(function(){ 
			$(beginDateId).trigger('focus'); 
		});
		
		$(endDateId+"-icon-wp").click(function(){ 
			$(endDateId).trigger('focus'); 
		});
}

/*START INIT AUTOCOMPLETE FUNCTION*/
var autoCompleteInputData = [];
var autoCompleteInputDataValue = [];
var autoCompleteInputDataLabel = [];
function initDataForAutocomplete(idInput, urlAjax) {
		autoCompleteInputData = [];
		autoCompleteInputDataValue = [];
		autoCompleteInputDataLabel = [];
	   $.ajax({
	        type: "POST",
	        contentType: "application/json; charset=utf-8",
	        url: appName + urlAjax,
	        dataType: "json",
	        success: function (data) {
	            //debugger;
		        autoCompleteInputData = data.data;
		        for (var i in autoCompleteInputData) {
		            autoCompleteInputDataValue.push(autoCompleteInputData[i].label);
		            autoCompleteInputDataLabel.push(autoCompleteInputData[i].value);
		        }
		        initAutoComplete(idInput, autoCompleteInputData);
	       },
	       error: function (result) {
	            // alert("Error......");
	       }
	   })
}
	
function initAutoComplete(idInput, data){
		$("#"+idInput).autocomplete({
			 minLength: 0,
	         // source: data,
	         source: function( request, response ) {
			    var searchspace = data;
			    var searchwords = $.trim(request.term);
				searchspace = $.ui.autocomplete.filter(searchspace, searchwords);
			    // var mySlice = searchspace.slice(0, 10);
			    response(searchspace);
			},
	         focus: function( event, ui ) {
	           $( "#"+idInput ).val( ui.item.label );
	           return false;
	         },
	         select: function( event, ui ) {
	        	 $( "#" +idInput).val( ui.item.label );
	        	 if ($.inArray($("#"+idInput).val(), autoCompleteInputDataValue) !== -1 
	        			 || $.inArray($("#"+idInput).val(), autoCompleteInputDataLabel) !== -1) {
	        		 $( "#"+idInput ).attr( idInput+"Id", ui.item.value );
	        	 } else {
	        		 $( "#"+idInput ).attr( idInput+"Id", "");
	        	 }
	        	
	           return false;
	         }
	    }).focusout(function() {
	    	if ($.inArray($(this).val(), autoCompleteInputDataValue) === -1 
	   			 && $.inArray($(this).val(), autoCompleteInputDataLabel) === -1) {
	    		$(this).attr( idInput+"id", "");
	    		$("#"+idInput+":text").val("");
		   	}
	    }).keypress(function(e){
	    	if(e.keyCode == 13) {	
	    	 	$(this).autocomplete("close");
	    	}
	    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	        .append( "<a>" + item.label + "</a>" )
	        .appendTo( ul );
	    };
		
}
/*START INIT DATA COMBOBOX FAILED REASON*/
function initDataForCombobox(idInput, urlAjax) {
		var autoComboboxInputData = [];
	   $.ajax({
	        type: "POST",
	        contentType: "application/json; charset=utf-8",
	        url: appName + urlAjax,
	        dataType: "json",
	        success: function (data) {
	            //debugger;
	        	autoComboboxInputData = data.data;
	        	for (var i in autoComboboxInputData) {
	        		$("#"+idInput).append("<option value='"+autoComboboxInputData[i].value+"' >"+autoComboboxInputData[i].label+"</option>");

		  		}
	       },
	       error: function (result) {
	            // alert("Error......");
	       }
	   })
}