$(document).ready(function() {
    $("#viewError").click(function(){
        if($("#message").hasClass("hide")){
            $("#message").removeClass("hide").addClass("show");
            $("#message").show();
        }else{
            $("#message").removeClass("show").addClass("hide");
            $("#message").hide();
        }
    });
});