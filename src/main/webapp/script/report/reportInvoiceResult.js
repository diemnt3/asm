$(document).ready(function() {
	initInvoiceReport1Table()
	initInvoiceReport2Table();
	initInvoiceReport3Table();
	$("#next").click(function(){ 
		$('#report-result-3-table').DataTable().page( 'next' ).draw( false );
	});
	$("#previous").click(function(){ 
		$('#report-result-3-table').DataTable().page( 'previous' ).draw( false );
	});	

});

//initiate dataTable
function initInvoiceReport1Table(){
	$('#report-result-1-table').DataTable( {
		"bFilter": false,
		"bLengthChange": false,
		"bInfo": false,
		"columnDefs": [ 
		{
			"orderable": false,
			'sortable': false,
			"targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
		},
		/*{
			"width": "*",
			"targets": 14
		},
		{
			"width": "6%",
			"targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
		},*/
		{
	    	"render": function ( data, type, row ) {
           	return '<span class="title-detail">Invoice Number: </span> INV0001';
          },
	      "targets": [0]
		},
		{
	    	"render": function ( data, type, row ) {
           	return '<span class="title-detail">Date Range: </span> 12/09/2014 - 12/09/2014';
	        },
		    "targets": [1]
		},
		{
	    	"render": function ( data, type, row ) {
           	return '<span class="title-detail">Date Invoice:</span> 12/09/2014';
	        },
		    "targets": [2]
		}
		],
 	  "processing": false,
	  "serverSide": true,
	    
		// "order": [[ 1, "asc" ]],
    	"pagingType": "simple",
    	// "stateSave": true,
	    "ajax": {
	        "url": appName + '/invoice-match/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	test = $("#vendor").html();
	        }
	    },

	    "columns": [
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "invoiceId" },
				{ "data": "invoiceId"},
				{ "data": "invoiceId" },
				{ "data": "invoiceId"}
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "View page _START_ of _TOTAL_ "
		  },
		  "aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],

      "fnDrawCallback": function( oSettings ) {
          $('#report-result-1-table>tbody>tr').each(function() {
          		var tr = $(this);
			    var row = $('#report-result-1-table').DataTable().row( tr );
			    var tableId = "table-" +  tr.attr('id');
			    if ( row.child.isShown() ) {
			        tr.removeClass( 'details' );
			        row.child.hide();
			    } else {
			        tr.addClass( 'details' );
	
					if (row.child() === "" || row.child() === undefined) {
						row.child( formatLineItem( row.data() ) ).show();
						initInvoiceReport1DetailTable(row.data());
					}else {
						
					}
			    }
		  });
		  colspanAfterRender();
       },
      "rowCallback": function( row, data ) {

       }
	});
}

function initInvoiceReport1VendorTable(d){
	$('#report-result-1-nestest-'+d.DT_RowId+'-table').DataTable( {
		// "bSort": false,
		"bFilter": false,
		"bLengthChange": false,
		"bInfo": false,
		"columnDefs": [ 
		{
			"orderable": false,
			"targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
		},
		{
			"width": "*",
			"targets": 15
		},
		{
			"width": "6.25%",
			"targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
		},
		{
	    	"render": function ( data, type, row ) {
           	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
          },
	      "targets": [3, 4, 5, 7, 8, 9, 11, 12, 13]
	    }

		],
 	  "processing": false,
	  "serverSide": true,
	    
		// "order": [[ 1, "asc" ]],
    	"pagingType": "simple",
    	// "stateSave": true,
	    "ajax": {
	        "url": appName + '/invoice-match/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	test = $("#vendor").html();
	        }
	    },

	    "columns": [
				{ "data": "invoiceId" },
				{ "data": "invoiceId" },
				{ "data": "invoiceDate" },
				{ "data": "freightCostFinal" },
				{ "data": "invoiceId" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "invoiceId" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "invoiceId"},
				{ "data": "freightCostFinal" },
				{ "data": "invoiceId"}
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "View page _START_ of _TOTAL_ "
		  },
		  "aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],

      "fnDrawCallback": function( oSettings ) {
          formatCurrency();
       },
      "rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-left');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-right');
           $('td:eq(3)', row).addClass('text-right');
           $('td:eq(4)', row).addClass('text-right');
           $('td:eq(5)', row).addClass('text-right');
           $('td:eq(6)', row).addClass('text-right');
           $('td:eq(7)', row).addClass('text-right');
		   $('td:eq(8)', row).addClass('text-right');
		   $('td:eq(9)', row).addClass('text-right');
		   $('td:eq(10)', row).addClass('text-right');
		   $('td:eq(11)', row).addClass('text-right');
		   $('td:eq(12)', row).addClass('text-right');
		   $('td:eq(13)', row).addClass('text-right');
		   $('td:eq(14)', row).addClass('text-right');
		   $('td:eq(15)', row).addClass('text-left');
       }
	});
}

function formatLineItem1 ( d ) {
        return '<table id="report-result-1-nestest-'+d.DT_RowId+'-table" class="add-nestest table table-condensed common-table-style table-hover table-striped table-bordered add-place-id">'+
	               '<thead class="hidden-title">'+
	                 '<tr class="firs-tr head-tr addon">'+       
	                 	'<th rowspan="2">Order No</th>'+
	                    '<th rowspan="2">Line Item</th>'+      
	                 	'<th rowspan="2">Reconciled Date</th>'+
	                    '<th colspan="4" >Vendor</th>'+
	                    '<th colspan="4">HEB</th>'+
	                    '<th colspan="4">Final(After Adjustment)</th>'+
	                    '<th rowspan="2">Adjustment Reason</th>'+
	                 '</tr>'+
	                 '<tr class="second-tr head-tr">'+
	                     '<th>Extended Cost</th>'+
	                     '<th>Freight Cost</th>'+
	                     '<th>Line Item Cost</th>'+
	                     '<th>Line Item Qty</th>'+
	                     '<th>Extended Cost</th>'+
	                     '<th>Freight Cost</th>'+
	                     '<th>Line Item Cost</th>'+
	                     '<th>Line Item Qty</th>'+
	                     '<th>Extended Cost</th>'+
	                     '<th>Freight Cost</th>'+
	                     '<th>Line Item Cost</th>'+
	                     '<th style="border-right-width: 1px" >Line Item Qty</th>'+
	                 '</tr> '+
	              '</thead>'+
	            '</table>';

}


function initInvoiceReport1DetailTable(d){
	$('#report-result-1-nestest-'+d.DT_RowId+'-table').DataTable( {
		// "bSort": false,
		"bFilter": false,
		"bLengthChange": false,
		"bInfo": false,
		"columnDefs": [ 
		{
			"orderable": false,
			"targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
		},
		{
			"width": "*",
			"targets": 15
		},
		{
			"width": "6.25%",
			"targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
		},
		{
	    	"render": function ( data, type, row ) {
           	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
          },
	      "targets": [3, 4, 5, 7, 8, 9, 11, 12, 13]
	    }

		],
 	  "processing": false,
	  "serverSide": true,
	    
		// "order": [[ 1, "asc" ]],
    	"pagingType": "simple",
    	// "stateSave": true,
	    "ajax": {
	        "url": appName + '/invoice-match/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	test = $("#vendor").html();
	        }
	    },

	    "columns": [
				{ "data": "invoiceId" },
				{ "data": "invoiceId" },
				{ "data": "invoiceDate" },
				{ "data": "freightCostFinal" },
				{ "data": "invoiceId" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "invoiceId" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "freightCostFinal" },
				{ "data": "invoiceId"},
				{ "data": "freightCostFinal" },
				{ "data": "invoiceId"}
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "View page _START_ of _TOTAL_ "
		  },
		  "aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],

      "fnDrawCallback": function( oSettings ) {
          formatCurrency();
       },
      "rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-left');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-right');
           $('td:eq(3)', row).addClass('text-right');
           $('td:eq(4)', row).addClass('text-right');
           $('td:eq(5)', row).addClass('text-right');
           $('td:eq(6)', row).addClass('text-right');
           $('td:eq(7)', row).addClass('text-right');
		   $('td:eq(8)', row).addClass('text-right');
		   $('td:eq(9)', row).addClass('text-right');
		   $('td:eq(10)', row).addClass('text-right');
		   $('td:eq(11)', row).addClass('text-right');
		   $('td:eq(12)', row).addClass('text-right');
		   $('td:eq(13)', row).addClass('text-right');
		   $('td:eq(14)', row).addClass('text-right');
		   $('td:eq(15)', row).addClass('text-left');
       }
	});
}

function formatLineItem ( d ) {
    return '<table id="report-result-1-nestest-'+d.DT_RowId+'-table" class="add-nestest table table-condensed common-table-style table-hover table-striped table-bordered add-place-id">'+
               '<thead class="hidden-title">'+
                 '<tr class="firs-tr head-tr addon">'+       
                 	'<th rowspan="2">Order No</th>'+
                    '<th rowspan="2">Line Item</th>'+      
                 	'<th rowspan="2">Reconciled Date</th>'+
                    '<th colspan="4" >Vendor</th>'+
                    '<th colspan="4">HEB</th>'+
                    '<th colspan="4">Final(After Adjustment)</th>'+
                    '<th rowspan="2">Adjustment Reason</th>'+
                 '</tr>'+
                 '<tr class="second-tr head-tr">'+
                     '<th>Extended Cost</th>'+
                     '<th>Freight Cost</th>'+
                     '<th>Line Item Cost</th>'+
                     '<th>Line Item Qty</th>'+
                     '<th>Extended Cost</th>'+
                     '<th>Freight Cost</th>'+
                     '<th>Line Item Cost</th>'+
                     '<th>Line Item Qty</th>'+
                     '<th>Extended Cost</th>'+
                     '<th>Freight Cost</th>'+
                     '<th>Line Item Cost</th>'+
                     '<th style="border-right-width: 1px" >Line Item Qty</th>'+
                 '</tr> '+
              '</thead>'+
            '</table>';

}
function colspanAfterRender() {
	$('#report-result-1-table>tbody>tr.details').each(function () {
		var currentTd = $(this).children();
		var currentTr = $(this);

		$(currentTr).find("td:eq(15)").remove();
		$(currentTr).find("td:eq(14)").remove();
		$(currentTr).find("td:eq(0)").attr("colspan", 3);

		$(currentTr).find("td:eq(13)").remove();
		$(currentTr).find("td:eq(12)").remove();
		$(currentTr).find("td:eq(1)").attr("colspan", 3);

		$(currentTr).find("td:eq(11)").remove();
		$(currentTr).find("td:eq(10)").remove();
		$(currentTr).find("td:eq(2)").attr("colspan", 10);
				
		$(currentTr).find("td:eq(9)").remove();
		$(currentTr).find("td:eq(8)").remove();
		$(currentTr).find("td:eq(7)").remove();
		$(currentTr).find("td:eq(6)").remove();
		$(currentTr).find("td:eq(5)").remove();
		$(currentTr).find("td:eq(4)").remove();
		$(currentTr).find("td:eq(3)").remove();

		});
}

function initInvoiceReport2Table(){
	$('#report-result-2-table').DataTable( {
		// "bSort": false,
		"sDom": '<"top"<"actions">p<"clear">><"clear">rt<"bottom">',
		"bLengthChange": false,
		"bInfo": false,
		"columnDefs": [ 
		{
			"orderable": false,
			"targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
		},
		{
			// "orderable": false,
			"width": "*",
			"targets": 0
		},
		{
			"width": "9%",
			"targets": [1, 2, 3, 4, 5, 6, 7]
		},
		{
			"width": "10%",
			"targets": [8, 9]
		},
		{
	    	"render": function ( data, type, row ) {
           	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
          },
	      "targets": [5, 8]

	    },

		/*{
            "render": function ( data, type, row ) {
            	return '<a class="link-invoice-details-a" value="'+row.invoiceId+'">'+data+'</a>';	
            },
            "targets": 1
        }*/
		],
 	  "processing": false,
	  "serverSide": true,
	    
		"order": [[ 1, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],
    	"sDom": "tripl",
    	"pagingType": "simple",
    	"stateSave": true,
	    "ajax": {
	        "url": appName + '/invoice-match/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	test = $("#vendor").html();
	        }
	    },

	    "columns": [
				{ "data": "invoiceId" },
				{ "data": "invoiceId"},
				{ "data": "invoiceDate" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" }
				
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		  },

      "fnDrawCallback": function( oSettings ) {
          formatCurrency();
       },
      "rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-left');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-left');
           $('td:eq(3)', row).addClass('text-left text-ellipsis');
           $('td:eq(4)', row).addClass('text-left');
           $('td:eq(5)', row).addClass('text-right');
           $('td:eq(6)', row).addClass('text-left');
           $('td:eq(7)', row).addClass('text-left');
		   $('td:eq(8)', row).addClass('text-right');
		   $('td:eq(9)', row).addClass('text-right');
		   $('td:eq(0)', row).attr( "title", data.prodDesc);
       }
	});
}


function initInvoiceReport3Table(){
	$('#report-result-3-table').DataTable( {
		// "sDom": '<"top"<"actions">l<"clear">><"clear">rt<"bottom">',
		// "bPaginate": false,
		"bLengthChange": false,
		"bInfo": false,
		"columnDefs": [ 
		{
			"orderable": false,
			"targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
		},
		{
			"width": "*",
			"targets": 0
		},
		{
			"width": "9%",
			"targets": [1, 2, 3, 4, 5, 6, 7]
		},
		{
			"width": "10%",
			"targets": [8, 9]
		},
		{
	    	"render": function ( data, type, row ) {
           	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
          },
	      "targets": [5, 8]
	    }
		],
 	    "processing": false,
	    "serverSide": true,
	    
		"order": [[ 1, "asc" ]],
    	"sDom": "tripl",
    	"pagingType": "simple",
    	"stateSave": true,
	    "ajax": {
	        "url": appName + '/invoice-match/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	test = $("#vendor").html();
	        }
	    },

	    "columns": [
				{ "data": "invoiceId" },
				{ "data": "invoiceId"},
				{ "data": "invoiceDate" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" }
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		  },

      "fnDrawCallback": function( oSettings ) {
          formatCurrency();
       },
      "rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-left');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-left');
           $('td:eq(3)', row).addClass('text-left text-ellipsis');
           $('td:eq(4)', row).addClass('text-left');
           $('td:eq(5)', row).addClass('text-right');
           $('td:eq(6)', row).addClass('text-left');
           $('td:eq(7)', row).addClass('text-left');
		   $('td:eq(8)', row).addClass('text-right');
		   $('td:eq(9)', row).addClass('text-right');
		   $('td:eq(0)', row).attr( "title", data.prodDesc);
       }
	});
}

