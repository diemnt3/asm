$(document).ready(function() {
	autoCompleteInit( 'vendor-input' );
	clickDropAutoComplete();

	/*DATE PICKER-------------------*/
	$('#dateBegin, #dateEnd, #asn-date').datepicker({
	    autoclose: true,
	});

	$("#dateBegin-icon-wp").click(function(){ 
		$("#dateBegin").trigger('focus'); 
	});

	$("#dateEnd-icon-wp").click(function(){ 
		$("#dateEnd").trigger('focus'); 
	});
	
	$("#asn-date-from-icon-wp").click(function(){ 
		$("#asn-date-from").trigger('focus'); 
	});

	$("#asn-date-to-icon-wp").click(function(){ 
		$("#asn-date-to").trigger('focus'); 
	});
	/*END DATE PICKER---------------*/


	$( document ).on( "click","a.export-to-choose", function() {
		var selText = $(this).text();
		$(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
	});
	
	//date select by report type
	$( "#report-type" ).change(function() {
	  if($(this).val() == 3){
	  	$(".asn-date").css("display", "block");
	  	$(".inv-date").css("display", "none");
	  }else{
	  	$(".asn-date").css("display", "none");
	  	$(".inv-date").css("display", "block");
	  }
	});

	//change interval time for invoice date
	$( "#inv-date" ).change(function() {
		
		var intervalOption = $(this).val();
		setIntervalDate (intervalOption, "#dateBegin", "#dateEnd");
	  
	});

	//change interval time for asn date
	$( "#asn-date" ).change(function() {
		
		var intervalOption = $(this).val();
		setIntervalDate (intervalOption, "#asn-date-from", "#asn-date-to");
	  
	});

	initDatePicker("#dateBegin", "#dateEnd");
	initDatePicker("#asn-date-from", "#asn-date-to");

});



