$(document).ready(function() {
	initInvoiceDetailTable();
	resetFilter( 'order-details-table' );
	filterDataTable( 'order-details-table' );
	
});

//initiate dataTable
function initInvoiceDetailTable(){
	$('#HEB-order-details-table').DataTable( {
		"columnDefs": [ 
		{
			"searchable": false,
			"orderable": false,
			"width": "3%",
			"targets": 0
		},
		{
			"width": "10%",
			"targets": [1, 2, 4, 5, 6]
		},
		{
			"width": "*",
			"targets": 3
		},
		
		/*{
			"render": function ( data, type, row ) {
		   	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
		  },
		  "targets": [5]
		
		}*/

		],
 	  "processing": false,
	  // "serverSide": true,
	    
		"order": [[ 1, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],
    	"sDom": "tripl",
    	"pagingType": "full_numbers",
    	"stateSave": true,
	    /*"ajax": {
	        "url": appName + '/invoice-match/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	
	        }
	    },

	    "columns": [
				{ "data": "invoiceId" },
				{ "data": "invoiceId"},
				{ "data": "totalFinalCost" },
				{ "data": "invoiceId" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" }
				
	     ],*/
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		  },

      "fnDrawCallback": function( oSettings ) {
          formatCurrency();
       },
      "rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-right');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-left');
           $('td:eq(3)', row).addClass('text-left');
           $('td:eq(4)', row).addClass('text-right');
           $('td:eq(5)', row).addClass('text-right');
           $('td:eq(6)', row).addClass('text-right');
/*           $('td:eq(9)', row).addClass('text-left text-ellipsis');
           $('td:eq(9)', row).attr( "title", data.prodDesc);*/
       }
	});
}

