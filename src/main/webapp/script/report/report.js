//common file for reports

$(document).ready(function() {
	$("#dateBegin, #dateEnd, #approved-date-from, #approved-date-to, #payment-date-from, #payment-date-to").keypress(function(){
		return false;
	}); 

	//validate begin date is less than end date
	// forceDateFromAndTo( '#dateBegin', '#dateEnd' );
	//	forceDateFromAndTo( '#asn-date-from', '#asn-date-to' );
});

/**
 * validate begin date is less than end date
 * @param dateFrom and dateTo.
 * @author GiauLe
 */
function forceDateFromAndTo(beginDateId, endDateId){
	var FromEndDate = new Date();
	var ToEndDate = new Date();

	ToEndDate.setDate(ToEndDate.getDate()+365);

	$(beginDateId).datepicker({
	    weekStart: 1,
	    "setDate": new Date(),
	    endDate: '0d',
	    autoclose: true
	}).on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $(endDateId).datepicker('setStartDate', startDate);
    }); 
	$(endDateId).datepicker({
        weekStart: 1,
        "setDate": new Date(),
        endDate: '0d',
        autoclose: true
	}).on('changeDate', function(selected){
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $(beginDateId).datepicker('setEndDate', FromEndDate);
	});
}

/**
 * set interval time, today, this week, this month, this year...
 * @param intervalOption
 * @param fromId: id of begin date input
 * @param ToId: id of end date input.
 * @author GiauLe
 */
function setIntervalDate (intervalOption, fromId, ToId){
	var currentDate = new Date();

	var firstDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
	var lastDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);

	var firstDayOfYear = new Date(currentDate.getFullYear(), 0, 1);
	var lastDayOfYear = new Date(currentDate.getFullYear(), 11, 31);

	switch (intervalOption) { 
	    case "1": 
	        $(fromId).datepicker("setDate", currentDate);
	  		$(ToId).datepicker("setDate", currentDate);
	        break;
	    case "2": 
	        $(fromId).datepicker("setDate", getMonday( new Date() ));
	  		$(ToId).datepicker("setDate", getSunday( new Date() ));
	        break;
	    case "3": 
	        $(fromId).datepicker("setDate", firstDayOfMonth);
	  		$(ToId).datepicker("setDate", lastDayOfMonth);
	        break;      
	    case "4": 
	        $(fromId).datepicker("setDate", firstDayOfYear);
	  		$(ToId).datepicker("setDate", lastDayOfYear);
	        break;
	    default:
	        $(fromId).datepicker("setDate", "");
	  		$(ToId).datepicker("setDate", "");
	  		$(fromId).attr('disabled', false);
	  		$(ToId).attr('disabled', false);
	}
}

/**
 * init today is the interval time for report.
 * @param fromId: id of begin date input
 * @param ToId: id of end date input
 * @author GiauLe
 */
function initDatePicker(fromId, ToId){
	var currentDate = new Date();
	$(fromId).datepicker("setDate", currentDate);
	$(ToId).datepicker("setDate", currentDate);
/*	$(fromId).attr('disabled', 'disabled');
	$(ToId).attr('disabled', 'disabled');*/
}

/**
 * get first date of week.
 * param d: date in that week.
 * @author GiauLe
 */
function getMonday(d) {
  d = new Date(d);
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}

/**
 * get last date of week.
 * @param d: date in that week.
 * @author GiauLe
 */
function getSunday(d) {
  d = new Date(d);
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff + 6));
}

