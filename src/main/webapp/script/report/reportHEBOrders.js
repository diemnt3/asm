$(document).ready(function() {
	initInvoiceDetailTable();
	autoCompleteInit( 'vendor-input' );
	clickDropAutoComplete();
	resetFilter( 'report-order-table' );
	filterDataTable( 'report-order-table' );
	/*DATE PICKER-------------------*/
	$('#dateBegin,#dateEnd').datepicker({
	    autoclose: true,
	});

	$("#dateBegin-icon-wp").click(function(){ 
		$("#dateBegin").trigger('focus'); 
	});

	$("#dateEnd-icon-wp").click(function(){ 
		$("#dateEnd").trigger('focus'); 
	});
	
	/*END DATE PICKER---------------*/
	/*link to order details*/
	
	$(document).on("click",".link-order-details-a", function() {
		$("body").append('<div id="temp-submit-invoice-details"></div>');
		$("#temp-submit-invoice-details").append('<form id="frmLinkInvoiceDetails" action="/assortment/report-HEB-order-detail" method="POST"></form>');
		
		$("#frmLinkInvoiceDetails").append('<input type="hidden" name="invoice" value="'+$(this).attr('value')+'">');
		$("#frmLinkInvoiceDetails").append('<input type="hidden" name="vendor" value="Kehe Inc - 123">');
		$("#frmLinkInvoiceDetails").append('<input type="hidden" name="forPage" value="reportForOrder">');
	
		$('#frmLinkInvoiceDetails').submit();
		$("#frmLinkInvoiceDetails").click();
		$("#temp-submit-invoice-details").remove();
	});
});

//initiate dataTable
function initInvoiceDetailTable(){
	$('#report-order-table').DataTable( {
		"columnDefs": [ 
			{
				"searchable": false,
				"orderable": false,
				"width": "3%",
				"targets": 0
			},
			{
				"width": "10%",
				"targets": [1, 3, 5]
			},
			{
				"width": "*",
				"targets": 2
			},
			{
				"width": "15%",
				"targets": 4
			},
			{
				"render": function ( data, type, row ) {
			   	return '<span class="pull-left">$</span> <span class="currency-format">'+ data +'</span>';
			  },
			  "targets": [5]
			
			},
			{
	            "render": function ( data, type, row ) {
	            	return '<a class="link-order-details-a" value="'+row.invoiceId+'" >'+data+'</a>';	
	            },
	            "targets": 1
        	}

		],
 	  "processing": false,
	  "serverSide": true,
	    
		"order": [[ 1, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],
    	"sDom": "tripl",
    	"pagingType": "full_numbers",
    	"stateSave": true,
	    "ajax": {
	        "url": appName + '/invoice-match/search',
	        "type": 'GET',
	        "data": function ( d ) {
	        	
	        }
	    },

	    "columns": [
				{ "data": "invoiceId" },
				{ "data": "invoiceId"},
				{ "data": "invoiceId" },
				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" },
//				{ "data": "totalFinalCost" },
				{ "data": "totalFinalCost" }
				
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		  },

      "fnDrawCallback": function( oSettings ) {
          formatCurrency();
       },
      "rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-right');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-left');
           $('td:eq(3)', row).addClass('text-right text-ellipsis');
           $('td:eq(4)', row).addClass('text-right');
           $('td:eq(5)', row).addClass('text-right');
           $('td:eq(6)', row).addClass('text-right');
           $('td:eq(3)', row).attr( "title", data.prodDesc);
       }
	});
}

