$(document).ready(function() {
	if (_.isEmpty(errorMessage)) {
		$("#viewError").removeClass("show").addClass("hide");
	} else {
		$("#viewError").removeClass("hide").addClass("show");
		$("#message").text(errorMessage);	
		$("#viewError").click(function(){
		 	if ($("#message").hasClass("hide")) {
		 		$(this).text("Hide details");
		     	$("#message").removeClass("hide").addClass("show");
		     	$("#message").show();
		 	} else {
		 		$(this).text("See more");
		     	$("#message").removeClass("show").addClass("hide");
		     	$("#message").hide();
		 	}
		});
	}
});
