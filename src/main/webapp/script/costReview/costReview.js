/************************************************************************************************************
 * PUBLIC VALUE
 ************************************************************************************************************/
var selectedPassedNewAndChanges = [];
var selectedFailedNewAndChanges = [];
var selectedAllNewAndChanges = [];

var vendorBdmValue = [];
var vendorBdmLabel = [];

var subCommodityBdmValue = [];
var subCommodityBdmLabel = [];

var brandBdmValue = [];
var brandBdmLabel = [];

var productBdmValue = [];
var productBdmLabel = [];

var passedTableNewAndChanges;
var failedTableNewAndChanges;
var allTableNewAndChanges;
var ConstantsCostReview = {
		EMPTY_STRING : "",
		SPACE_STRING : " ",
		AJAX_COMMONDITIES : "/ajax/get-data-autocomplete-commodities",
		AJAX_VENDOR : "/ajax/get-data-autocomplete-vendor",
		AJAX_BRAND : "/ajax/get-data-autocomplete-brand",
		AJAX_PRODUCT : "/ajax/get-data-autocomplete-product",
		msgLoseChange :"You will lose your changes, do you wish to continue?"
};
var searchCostReviewObject = {
		tabActive: "",
    	commodittyId:"",
    	vendorId: "",
    	brand: "",
    	prodDes: "",
    	receiveDateBegin: "",
    	receiveDateEnd: "",
    	allMapProduct: "",
    	onlyPrePriceProduct: "",
    	marginBegin: "",
    	marginEnd: "",
    	failReason: "",
    	columnsJsonStr: "",
    	orderJsonStr:"" 
}
var resetCount = true;
/************************************************************************************************************
 * END PUBLIC VALUE
 ************************************************************************************************************/

$(document).ready(function() {
/************************************************************************************************************
 * SEARCH CRITERIA
 ************************************************************************************************************/
/*-------------------------------------------*/
/*AUTOCOMPLETE-------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*END AUTOCOMPLETE---------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*FORM---------------------------------------*/
/*-------------------------------------------*/
/*Click button search--------------*/
$("#search-cost-review-btn").click(function() {
	if(isChanges()) {
		bootbox.dialog({
			closeButton: true,
			title : "Confirmation",
			message: Constants.msgLoseChange,
			buttons: {
				main: {
					label: "Yes",
					className: "btn-primary",
					callback: function() {
						searchCostReview();
					}	
				},	
				cancel: {
					label: "No",
					className: "btn-default",
					callback : function(){				
									   
					}
				}
			}
		});
	} else {
		searchCostReview();
	}
});

$("#new-and-changes-header .input-sm").keypress(function(event){ 
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		if(isChanges()) {
			bootbox.dialog({
				closeButton: true,
				title : "Confirmation",
				message: Constants.msgLoseChange,
				buttons: {
					main: {
						label: "Yes",
						className: "btn-primary",
						callback: function() {
							searchCostReview();
						}	
					},	
					cancel: {
						label: "No",
						className: "btn-default",
						callback : function(){				
										   
						}
					}
				}
			});
		} else {
			searchCostReview();
		}
	}
});

function searchCostReview() {
	if(isEmptyValueSearchForm()) {
		showAlertDanger("Search criteria is required", "header");
	} else {
		removeAlert("header");
		setSearchCostReviewObject();
		selectedPassedNewAndChanges = [];
		selectedFailedNewAndChanges = [];
		selectedAllNewAndChanges = [];
		resetCount = true;
		$('#new-and-changes-'+ getTabActive() +'-table').DataTable().draw();
	}
}

/*End click button search-----------*/

/*CLEAR*/
$("#reset-cost-review-btn").click(function () {
	resetForm();
	removeAlert("header");
});
/*END CLEAR*/
/*Datepicker------------------------*/
$('#dateBegin,#dateEnd').datepicker({
    autoclose: true,
});

$("#dateBegin-icon-wp").click(function(){ 
	$("#dateBegin").trigger('focus'); 
})

$("#dateEnd-icon-wp").click(function(){ 
	$("#dateEnd").trigger('focus'); 
})
/*end datepicker--------------------*/

/*collapse--------------------------*/
 $('#new-and-changes-header').on('shown.bs.collapse', function () {
	if(isEmptyValueSearchForm()) {
		showAlertDanger("Search criteria is required", "header");
	}
	$("#fa-angle-double-down-style").removeClass('fa-angle-double-right');
	$("#fa-angle-double-down-style").addClass('fa-angle-double-down');
})

$('#new-and-changes-header').on('hidden.bs.collapse', function () {
	removeAlert("header");
	$("#fa-angle-double-down-style").removeClass('fa-angle-double-down');
	$("#fa-angle-double-down-style").addClass('fa-angle-double-right');
})

/*end collapse----------------------*/

/*INIT FAILED REASON  COMBOBOX*/
initDataForCombobox("failed-input", "/get-boundary-rule");
/*INIT COMMODITIES AUTOCOMPLETE*/
initDataForAutocomplete("commodities-input", ConstantsCostReview.AJAX_COMMONDITIES);
/*INIT VENDOR AUTOCOMPLETE*/
initDataForCombobox("vendor-input", ConstantsCostReview.AJAX_VENDOR);
//initDataForAutocomplete("vendor-input", ConstantsCostReview.AJAX_VENDOR);
/*INIT BRAND AUTOCOMPLETE*/
//initAutoCompleteSearch("brand-input", ConstantsCostReview.AJAX_BRAND);
/*INIT BRAND AUTOCOMPLETE*/
//initAutoCompleteSearch("product-input", ConstantsCostReview.AJAX_PRODUCT);
/*START VALIDATION FORM*/
initDatePickerCostReview();
/*-------------------------------------------*/
/*END FORM-----------------------------------*/
/*-------------------------------------------*/
	
/*-------------------------------------------*/
/*FUNCTION-----------------------------------*/
/*-------------------------------------------*/
/*START INIT DATA COMBOBOX FAILED REASON*/
function initDataForCombobox(idInput, urlAjax) {
	var autoComboboxInputData = [];
   $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: appName + urlAjax,
        dataType: "json",
        success: function (data) {
            //debugger;
        	autoComboboxInputData = data.data;
        	for (var i in autoComboboxInputData) {
	      		$("#"+idInput).append("<option value='"+autoComboboxInputData[i].value+"' >"+autoComboboxInputData[i].label+"</option>");
	  		}
       },
       error: function (result) {
            alert("Error......");
       }
   })
}

/*START INIT AUTOCOMPLETE FUNCTION*/
var autoCompleteInputData = [];
var autoCompleteInputDataValue = [];
var autoCompleteInputDataLabel = [];
function initDataForAutocomplete(idInput, urlAjax) {
	autoCompleteInputData = [];
	autoCompleteInputDataValue = [];
	autoCompleteInputDataLabel = [];
   $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: appName + urlAjax,
        dataType: "json",
        success: function (data) {
            //debugger;
	        autoCompleteInputData = data.data;
	        for (var i in autoCompleteInputData) {
	            autoCompleteInputDataValue.push(autoCompleteInputData[i].label);
	            autoCompleteInputDataLabel.push(autoCompleteInputData[i].value);
	        }
	        initAutoComplete(idInput, autoCompleteInputData);
       },
       error: function (result) {
            alert("Error......");
       }
   })
}

function initAutoComplete(idInput, data){
	$("#"+idInput).autocomplete({
		 minLength: 0,
         source: data,
         focus: function( event, ui ) {
           $( "#"+idInput ).val( ui.item.label );
           return false;
         },
         select: function( event, ui ) {
        	 $( "#" +idInput).val( ui.item.label );
        	 if ($.inArray($("#"+idInput).val(), autoCompleteInputDataValue) !== -1 
        			 || $.inArray($("#"+idInput).val(), autoCompleteInputDataLabel) !== -1) {
        		 $( "#"+idInput ).attr( idInput+"-id", ui.item.value );
        	 } else {
        		 $( "#"+idInput ).attr( idInput+"-id", "");
        	 }
        	
           return false;
         }
    }).focusout(function() {
    	if ($.inArray($(this).val(), autoCompleteInputDataValue) === -1 
   			 && $.inArray($(this).val(), autoCompleteInputDataLabel) === -1) {
    		$(this).attr( idInput+"-id", "");
    		$("#"+idInput+":text").val("");
	   	 }
    }).keypress(function(e){
    	if(e.keyCode == 13) {	
    	 	$(this).autocomplete("close");
    	}
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label + "</a>" )
        .appendTo( ul );
    };
	
}

/*INIT AUTOCOMPLETE BRAND*/
function initAutoCompleteSearch(idInput, urlAjax){
	var autoCompleteInputData = [];
	var autoCompleteInputDataValue = [];
	var autoCompleteInputDataLabel = [];
	$("#"+idInput).autocomplete({
		 minLength: 0,
		 search: function(event, ui) {
	            $(this).addClass("wait");
	            $(this).parent().find("span").removeClass("caret");
	    },
	    open: function(event, ui) {
	            $(this).removeClass("wait");
	            $(this).parent().find("span").addClass("caret");
	    },
        source: function(request, response) {
        	 $(document).unbind('ajaxSend');
        	 $.ajax({
             	type: "POST",
                 url: appName + urlAjax,
                 data: {
                	 termSearch : request.term
                 },
                 dataType: "json",
                 success: function (data) {
                     dataAutoCompleteInput = data.data;
                     for (var i in dataAutoCompleteInput) {
                    	 autoCompleteInputDataLabel.push(dataAutoCompleteInput[i].value);
                     }

                     response(dataAutoCompleteInput);

                     if(dataAutoCompleteInput.length == 0){
                         $("#" + idInput).removeClass("wait");
                         $("#" + idInput).parent().find("span").addClass("caret");
                     }
                     //re-attach loading screen
                     $(document).bind("ajaxSend", function(e, xhr, settings){
                         $.blockUI({
                             message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
                             fadeIn: 100,
                             onUnblock: function() {
                             }
                         });

                         
                     }).bind("ajaxStop", function(){
                         $.unblockUI();
                     }).bind('ajaxError', handleAjaxError);

                 },
                 error: function (result) {
                     alert("Error......");
                 }
             });
			
		},
        focus: function( event, ui ) {
           $( "#"+idInput ).val( ui.item.label );
           return false;
        },
        select: function( event, ui ) {
        	$( "#"+idInput ).val( ui.item.label );
        	$( "#"+idInput ).attr(idInput + "-id", ui.item.value);
        	return false;
         }
    }).keypress(function(e){
    	if(e.keyCode == 13) {	
    	 	$(this).autocomplete("close");
    	}
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label + "</a>" )
        .appendTo( ul );
    };
	
}

/*END INIT AUTOCOMPLETE BRAND*/

function resetForm(){
	$("#commodities-input").val("");
    $("#commodities-input").attr("commodities-input-id", "");
	$("#brand-input").val(""); 
    $("#product-input").val("");
    $("#dateBegin").val("");
    $("#dateEnd").val("");
    $("#marginBegin").val("");
    $("#marginEnd").val("");
    $("#failed-input").val(Constants.EMPTY_STRING);  
	$("#show-all-map-check").prop('checked', false);
	$("#show-only-prePrice-check").prop('checked', false);	
	$("#vendor-input").val(Constants.EMPTY_STRING);
	initDatePickerCostReview();
 }

function initDatePickerCostReview() {
	/*DATE PICKER-------------------*/
	var dateBeginValue = new Date();
	dateBeginValue.setDate(dateBeginValue.getDate() - 7);
	$("#dateBegin").datepicker("setDates", dateBeginValue);
	$("#dateEnd").datepicker("setDates", new Date());
	/*END DATE PICKER---------------*/

}

function isEmptyValueSearchForm(){
   	var check = true;
	$("#new-and-changes-header .input-sm").each(function () {
		if(! _.isEmpty($(this).val())) {
			check = false;
		}
	});
	if($("#show-all-map-check").prop('checked')) {
		check = false;
	}
	if($("#show-only-prePrice-check").prop('checked')) {
		check = false;
	}
	return check;
}

function isEmptyValueFilter(){
   	var check = true;
	$("#new-and-changes-"+getTabActive()+"-table").find(".input-filter").each(function () {
		if(! _.isEmpty($(this).val())) {
			check = false;
		}
	});
	
	return check;
}


/*-------------------------------------------*/
/*END FUNCTION-------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*VALIDATION---------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*END VALIDATION-----------------------------*/
/*-------------------------------------------*/
/************************************************************************************************************
 * END SEARCH CRITERIA
 ************************************************************************************************************/

/************************************************************************************************************
 * SEARCH RESULT
 ************************************************************************************************************/
/*-------------------------------------------*/
/*TAB----------------------------------------*/
/*-------------------------------------------*/

$('#tabResult a').click(function (e) {
	
});

 $('#tabResult a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
 });
 
 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	tabActive = $(e.target).attr('value');
	removeAlert(tabActive);
	
	if (tabActive === 'passed') {
		if (!$("#new-and-changes-passed-table").hasClass('dataTable')) {
			passedTableNewAndChanges = initDataTable("passed", selectedPassedNewAndChanges);
		} else {
			passedTableNewAndChanges.draw();
		}
	} else if (tabActive === 'failed') {
		if (!$("#new-and-changes-failed-table").hasClass('dataTable')) {
			failedTableNewAndChanges = initDataTable("failed", selectedFailedNewAndChanges);
		}else {
			failedTableNewAndChanges.draw();
		}
	} else if (tabActive === 'all') {
		if (!$("#new-and-changes-all-table").hasClass('dataTable')) {
			allTableNewAndChanges = initDataTable("all", selectedAllNewAndChanges);
		}else {
			allTableNewAndChanges.draw();
		}
	}
 });

/*-------------------------------------------*/
/*END TAB------------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*TAB ALL------------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*END TAB ALL--------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*TAB PASSED---------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*END TAB PASSED-----------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*TAB FAILED---------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*END TAB FAILED-----------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*TABLE--------------------------------------*/
/*-------------------------------------------*/

//init datatable
setSearchCostReviewObject();
var allTableNewAndChanges = initDataTable("all", selectedAllNewAndChanges);

function initDataTable(type, selected) {
	var table = $('#new-and-changes-'+ type +'-table').DataTable( {
		"autoWidth": false,
		"columnDefs": [
			{
				"render": function ( data, type, row ) {
			    	return '<input type="checkbox" class="cursor-pointer checkbox-class" value="">';
			    },
			    "searchable": false,
				"orderable": false,
				"targets": 0
			},
			{
				"searchable": false,
				"orderable": false,
				"targets": 1
			},
			{
				"render": function ( data, type, row ) {
			    	return '<span>'+data+'</span>';
			    },
				"targets": [2, 3]
			},
			{
				"render": function ( data, type, row ) {
			    	return '<span class="currency-format current-cost-span">'+ data +'</span>';
			    	//<span class="pull-left">$</span>
				},
				"targets": 5
			},
			{
				"render": function ( data, type, row ) {//<span class="pull-left">$</span><input id="" type="text" class="form-control input-sm text-right pd0 currency-format-input" value="'+data+'"/>
					return '<div class="input-group input-group-sm"> <input type="text" class="form-control input-sm text-right pd0 currency-format-input format-input-cell current-retail-input" tmp="'+data+'" value="'+data+'"></div>';
					// <span class="input-group-addon input-group-addon-cell">$</span>
				},
				"targets": 6
			},
			{
				"render": function ( data, type, row ) {
					if (data === 'Y') {
			    		return '<span class="label label-warning">'+ data +'</span>';
					} else {
						return '<span>'+ data +'</span>';
					}
					
			    },
			    "searchable": false,
				"orderable": false,
				"targets": [7, 13]
			},
			{
				"render": function ( data, type, row ) {
			    	if (data === 'Y') {
			    		//return '<a class="label label-warning show-competitor-cost-reiew cursor-pointer" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-html="true" title="" data-content="<table id=&quot;&quot; class=&quot;table table-condensed common-table-style table-hover table-striped table-bordered pd0 margin-bottom0&quot; style=&quot;width: 100% !important;border: none;&quot;><thead><tr class=&quot;firs-tr&quot;><th></th><th>Old</th><th>Date</th><th>New</th><th>Date</th></tr></thead><tbody><tr><td class=&quot;table-align-left&quot;>Wal-Mart:</td><td>$1.20</td><td>7/28/2014</td><td>$1.10</td><td>8/1/2014</td></tr><tr><td class=&quot;table-align-left&quot;>Amazon:</td><td>$1.10</td><td>7/28/2014</td><td>$0.99</td><td>8/1/2014</td></tr><tr><td class=&quot;table-align-left&quot;>Target:</td><td>$1.30</td><td>7/28/2014</td><td>$1.20</td><td>8/1/2014</td></tr><tr class=&quot;font-bold&quot;><td class=&quot;table-align-left&quot;>Average:</td><td>$1.20</td><td></td><td>$1.10</td><td></td></tr></tbody></table>">Y</a>'
			    		return '<a tabindex="3" class="label label-warning show-competitor-cost-reiew cursor-pointer" role="button" data-trigger="focus" data-html="true" data-container="body" data-toggle="popover" data-placement="top" data-content="<table id=&quot;&quot; class=&quot;table table-condensed common-table-style table-hover table-striped table-bordered pd0 margin-bottom0&quot; style=&quot;width: 100% !important;border: none;&quot;><thead><tr class=&quot;firs-tr&quot;><th></th><th>Old</th><th>Date</th><th>New</th><th>Date</th></tr></thead><tbody><tr><td class=&quot;table-align-left&quot;>Wal-Mart:</td><td>$1.20</td><td>7/28/2014</td><td>$1.10</td><td>8/1/2014</td></tr><tr><td class=&quot;table-align-left&quot;>Amazon:</td><td>$1.10</td><td>7/28/2014</td><td>$0.99</td><td>8/1/2014</td></tr><tr><td class=&quot;table-align-left&quot;>Target:</td><td>$1.30</td><td>7/28/2014</td><td>$1.20</td><td>8/1/2014</td></tr><tr class=&quot;font-bold&quot;><td class=&quot;table-align-left&quot;>Average:</td><td>$1.20</td><td></td><td>$1.10</td><td></td></tr></tbody></table>">' + data + '</a>';						
			    		//return '<button type="button" class="btn btn-lg btn-danger" data-toggle="popover" title="Popover title" data-content="<table id=&quot;&quot; class=&quot;table table-condensed common-table-style table-hover table-striped table-bordered pd0 margin-bottom0&quot; style=&quot;width: 100% !important;border: none;&quot;><thead><tr class=&quot;firs-tr&quot;><th></th><th>Old</th><th>Date</th><th>New</th><th>Date</th></tr></thead><tbody><tr><td class=&quot;table-align-left&quot;>Wal-Mart:</td><td>$1.20</td><td>7/28/2014</td><td>$1.10</td><td>8/1/2014</td></tr><tr><td class=&quot;table-align-left&quot;>Amazon:</td><td>$1.10</td><td>7/28/2014</td><td>$0.99</td><td>8/1/2014</td></tr><tr><td class=&quot;table-align-left&quot;>Target:</td><td>$1.30</td><td>7/28/2014</td><td>$1.20</td><td>8/1/2014</td></tr><tr class=&quot;font-bold&quot;><td class=&quot;table-align-left&quot;>Average:</td><td>$1.20</td><td></td><td>$1.10</td><td></td></tr></tbody></table>">Click to toggle popover</button>';
			    	} else {
						return data;
					}
					
			    },
			    "searchable": false,
				"orderable": false,
				"targets": 8
			},
			{
				"render": function ( data, type, row ) {
					return '<span class="currency-format current-gp-span">'+ data +'</span>';				
			    },
				"targets": 9
			},
			{
				"render": function ( data, type, row ) {
					var revStat = row.revStatCd;
			    	if (parseFloat(data) < parseFloat(row.currentCost) && (_.isEmpty(revStat) || revStat === 'ACEPT')) {
			    		return '<span class="currency-format label label-info next-cost-span">'+ data +'</span>';
			    		//<span class="pull-left">$</span> 
			    	} else if (parseFloat(data) > parseFloat(row.currentCost) && (_.isEmpty(revStat) || revStat === 'ACEPT')) {
						return '<span class="currency-format label label-danger next-cost-span">'+ data +'</span>';
						//<span class="pull-left">$</span>
			    	} else {
						return '<span class="currency-format next-cost-span">'+ data +'</span>';
						//<span class="pull-left">$</span>
			    	}
					
			    },
				"targets": 11
			},
			{
				"render": function ( data, type, row ) {// <span class="pull-left">$</span><input id="" type="text" class="form-control input-sm text-right pd0 currency-format-input" value="'+data+'"/>
					var revStat = row.revStatCd;
					if (parseFloat(data) > parseFloat(row.currentRetail) && (_.isEmpty(revStat) || revStat === 'ACEPT')) {
			    		return '<div class="input-group input-group-sm"> <input type="text" class="form-control input-sm text-right pd0 currency-format-input format-input-cell label-info-input next-retail-input" tmp="'+data+'" value="'+data+'"></div>';
			    		//<span class="input-group-addon input-group-addon-cell">$</span>
					} else if(parseFloat(data) < parseFloat(row.currentRetail) && (_.isEmpty(revStat) || revStat === 'ACEPT')) {
						return '<div class="input-group input-group-sm"> <input type="text" class="form-control input-sm text-right pd0 currency-format-input format-input-cell label-danger-input next-retail-input" tmp="'+data+'" value="'+data+'"></div>';
						//<span class="input-group-addon input-group-addon-cell">$</span>
					} else {
						return '<div class="input-group input-group-sm"> <input type="text" class="form-control input-sm text-right pd0 currency-format-input format-input-cell next-retail-input" tmp="'+data+'" value="'+data+'"></div>';
						//<span class="input-group-addon input-group-addon-cell">$</span>
					}
			    },
				"targets": 12
			},
			{
				"render": function ( data, type, row ) {
					var revStat = row.revStatCd;
					if (parseFloat(data) > parseFloat(row.currentGP) && (_.isEmpty(revStat) || revStat === 'ACEPT')) {
						return '<span class="gp-value label label-info currency-format next-gp-span">'+ data +'</span>';
					} else if (parseFloat(data) < parseFloat(row.currentGP) && (_.isEmpty(revStat) || revStat === 'ACEPT')) {
						return '<span class="gp-value label label-danger clurrency-format next-gp-span">'+ data +'</span>';
					} else {
						return '<span class="gp-value currency-format next-gp-span">'+ data +'</span>';
					}
					
			    },
				"targets": 14
			},
			{
				"render": function ( data, type, row ) {
					if (data === "PASS") {
						return '<span class="label label-success">'+ data +'</span>';
					} else {
						return '<span title="'+row.errTypDes+'" class="label label-danger info-style">'+ data +'</span>';
					}
					
			    },
			    "searchable": false,
				"orderable": false,
				"targets": 15
			},
			{
				"render": function ( data, type, row ) {
					return '<span title="'+ row.prcChgRsnTxt +'" class="label label-default info-style">'+ row.changeReason +'</span>';
			    },
			    "searchable": false,
				"orderable": false,
				"targets": 17
			}
		],
		"order": [[ 16, "asc" ]],
		"aLengthMenu": [
		    [10, 25, 50, 100],
		    [10, 25, 50, 100]
		],
    	"sDom": '<"wp-table-1" <"wp-table-2" <"wp-table-3"t>r> ipl>',
    	"pagingType": "full_numbers",
    	"orderMulti": false,
    	"stateSave": false,
		"processing": false,
	    "serverSide": true,
	    "ajax": {
	        "url": appName +'/cost-review/search',
	        "type": 'GET',
	        "data": function ( d ) {        	
	        	d.tabActive = getTabActive();
	        	d.commodittyId = searchCostReviewObject.commodittyId;
	        	d.vendorId = searchCostReviewObject.vendorId;
	        	d.brand = searchCostReviewObject.brand;
	        	d.prodDes = searchCostReviewObject.prodDes;
	        	d.receiveDateBegin = searchCostReviewObject.receiveDateBegin;
	        	d.receiveDateEnd = searchCostReviewObject.receiveDateEnd;
	        	d.allMapProduct = searchCostReviewObject.allMapProduct;
	        	d.onlyPrePriceProduct = searchCostReviewObject.onlyPrePriceProduct;
	        	d.marginBegin =searchCostReviewObject.marginBegin;
	        	d.marginEnd =searchCostReviewObject.marginEnd;
	        	d.failReason =searchCostReviewObject.failReason;
	        	
	        	d.columnsJsonStr = JSON.stringify(d.columns);
	        	d.orderJsonStr = JSON.stringify(d.order);
	        	/*filter--------------------------------*/
	        	d.upcIdFilter = $("#search-upc-" + type).val();
	        	d.prodDescFilter = $("#search-product-desc-" + type).val();
	        	d.packSizeFilter = $("#search-pack-size-" + type).val();
	        	d.costFilter = $("#search-current-cost-" + type).val();
	        	d.retailFilter = $("#search-current-retail-" + type).val();
	        	d.mapFilter = $("#search-current-map-yn-" + type).val();
	        	d.compFilter = $("#search-current-comp-yn-" + type).val();
	        	d.gpFilter = $("#search-current-gp-" + type).val();
	        	d.nextPackSizeFilter = $("#search-next-pack-size-" + type).val();
	        	d.nextCostFilter = $("#search-next-cost-" + type).val();
	        	d.nextRetailFilter = $("#search-next-retail-" + type).val();
	        	d.nextMapFilter = $("#search-next-map-yn-" + type).val();
	        	d.nextGPFilter = $("#search-next-gp-" + type).val();
	        	d.statusProduct = $("#search-status-" + type).val();
	        	d.nextEffectiveDateFilter = $("#search-effective-date-" + type).val();
	        	d.changeReasonFilter = $("#search-change-reason-" + type).val();
	        	
	        	searchCostReviewObject.columnsJsonStr = d.columns;
	        	searchCostReviewObject.orderJsonStr = d.order;
	        },
	        "type": "POST"
	    },
	    "columns": [
	                { "data": "no", "className": "no-cls" },
	                { "data": "checkBox", "className": "check-box-cls"},
	                { "data": "upc", "className": "upc-cls" },
	                { "data": "desc", "className": "desc-cls" },
	                { "data": "packSize", "className": "pack-size-cls"},
	                { "data": "currentCost", "className": "current-cost-cls" },
	                { "data": "currentRetail", "className": "current-retail-cls" },
	                { "data": "currentMap", "className": "current-map-cls" },
	                { "data": "currentComp", "className": "current-comp-cls" },
	                { "data": "currentGP", "className": "current-gp-cls"},
	                { "data": "nextPackSize", "className": "next-pack-size-cls" },
	                { "data": "nextCost", "className": "next-cost-cls" },
	                { "data": "nextRetail", "className": "next-retail-cls" },
	                { "data": "nextMap", "className": "next-map-cls" },
	                { "data": "nextGP", "className": "next-gp-cls" },
	                { "data": "status", "className": "status-cls" },
	                { "data": "effectDate", "className": "effective-date-cls"},
	                { "data": "changeReason", "className": "change-reason-cls" }       
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		},
		"fnDrawCallback": function( oSettings ) {
			$(".show-competitor-cost-reiew").popover();
			if(this.DataTable().data().length > 0) {
				this.DataTable().column(1,{search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = i+1;
		        });
			}
			
			
			if (getTabActive() === 'passed') {
				selected = selectedPassedNewAndChanges;
			} else if(getTabActive() === 'failed') {
				selected = selectedFailedNewAndChanges;
			}	else if(getTabActive() === 'all') {
				selected = mergeList(selectedPassedNewAndChanges, selectedFailedNewAndChanges);
			}
			$("#new-and-changes-"+ getTabActive() +"-table").find(".checkbox-class").each(function () {
				var selectorTr = $(this).closest('tr'); 
				var dataRow = $("#new-and-changes-"+ getTabActive() +"-table").DataTable().row($("#" + $(selectorTr).attr('id'))).data();
				
				var itemObj = {
					rowId: dataRow.rowId
		    	}
				//debugger;
	    		if (inArrays(selected, itemObj)) {
	    			var dataFill = getItemInArraysByKey(selected, itemObj);
	    			fillData($(selectorTr), dataFill[0]);
	    			updateForDataRow(dataRow, dataFill[0]);
	    			checkChangeForInputCell($(selectorTr), dataFill[0]);
	    		}
			});
			formatCurrency();
			formatCurrencyInput();
		},
		"initComplete": function (settings, json) {
			if (Common.getIEVersion() > 0) {
				 setTimeout(function(){//$("#passed-table .firs-tr th:nth-child(3)").width()
						var width = 150;
						$("#new-and-changes-"+getTabActive()+"-table .text-ellipsis span").each(function(){//new-and-changes-failed-table
							$(this).width(width -1 + "px");
						});
				}, 1000);
			}
			 
			$(window).resize(function() {
				if (Common.getIEVersion() > 0) {
					var width = $("#new-and-changes-"+getTabActive()+"-table .firs-tr th:nth-child(3)").width();
					setTimeout(function(){
						$("#new-and-changes-"+getTabActive()+"-table .text-ellipsis span").each(function(){
							$(this).width(width -1 + "px");
						});
				}, 1000);
				}
			});
		},
		"rowCallback": function( row, data ) {
			 /* add class for cell */
			 $('td:eq(0)', row).addClass('text-center');
			 $('td:eq(1)', row).addClass('text-right');
			 $('td:eq(2)', row).addClass('text-left');
			 $('td:eq(3)', row).addClass('text-left text-ellipsis');
			 $('td:eq(4)', row).addClass('text-left');
			 $('td:eq(5)', row).addClass('text-right');
			 $('td:eq(6)', row).addClass('text-right');
			 $('td:eq(7)', row).addClass('text-center');
			 if (data.currentComp === 'N') {
				 $('td:eq(8)', row).addClass('text-center');
			 }else {
				 $('td:eq(8)', row).addClass('text-center');//bullet
			 }
			 $('td:eq(11)', row).addClass('text-right');
			 $('td:eq(8)', row).addClass('text-center');
			 $('td:eq(9)', row).addClass('text-right');
			 $('td:eq(10)', row).addClass('text-left');
//			 $('td:eq(10)', row).addClass('text-right');
			 $('td:eq(12)', row).addClass('text-right');
			 $('td:eq(13)', row).addClass('text-center');
			 $('td:eq(14)', row).addClass('text-right');
			 $('td:eq(15)', row).addClass('text-center');
			 $('td:eq(16)', row).addClass('text-right');
			 $('td:eq(17)', row).addClass('text-center');
			  
			 $('td:eq(3)', row).attr( "title", data.desc);
		}     
	});
	
	/*select tr-------------------*/
	$('#new-and-changes-' + type +'-table tbody').on('change', 'input[type="checkbox"]', function () {
		var selectorTr = $(this).closest('tr');
		var dataRow = $("#new-and-changes-"+ getTabActive() +"-table").DataTable().row($("#" + $(selectorTr).attr('id'))).data();

        if (!$(selectorTr).find('td').hasClass('dataTables_empty')) {
        	$(selectorTr).toggleClass('success');
        	if ($(selectorTr).hasClass('success')) {
        		$(selectorTr).find(".checkbox-class").prop('checked', true);
			} else {
				$(selectorTr).find(".checkbox-class").prop('checked', false);
			}
		}
        
        if ($(this).prop("checked")) {
        	dataRow.checkStatus = 'Y';
		} else {
			dataRow.checkStatus = 'N';
		}

		var itemObj = {
				rowId: $("#new-and-changes-"+ getTabActive() +"-table").DataTable().row($("#" + $(selectorTr).attr('id'))).data().rowId
		}
		 
        if (getTabActive() === 'passed') {
        	//debugger;
			if (dataRow.checkStatus === 'Y'|| !_.isEmpty(dataRow.currentRetailChange) || !_.isEmpty(dataRow.nextRetailChange)) {
				if (inArrays(selectedPassedNewAndChanges, itemObj)) {
					var listRemove = removeInArrays(selectedPassedNewAndChanges, itemObj.rowId);
					var listUpdate = updateItems(listRemove, selectorTr);
				}else {
					var listUpdate = updateItems(selectedPassedNewAndChanges, selectorTr);
				}
				selectedPassedNewAndChanges = listUpdate;
			} else {
				var listRemove = removeInArrays(selectedPassedNewAndChanges, itemObj.rowId);
				selectedPassedNewAndChanges = listRemove;
			}		
			selected = selectedPassedNewAndChanges;
        } else if (getTabActive() === 'failed') {
			//debugger;
			if (dataRow.checkStatus === 'Y' || !_.isEmpty(dataRow.currentRetailChange) || !_.isEmpty(dataRow.nextRetailChange)) {
				if (inArrays(selectedFailedNewAndChanges, itemObj)) {
					var listRemove = removeInArrays(selectedFailedNewAndChanges, itemObj.rowId);
					var listUpdate = updateItems(listRemove, selectorTr);
				}else {
					var listUpdate = updateItems(selectedFailedNewAndChanges, selectorTr);
				}
				selectedFailedNewAndChanges = listUpdate;
			} else {
				var listRemove = removeInArrays(selectedFailedNewAndChanges, itemObj.rowId);
				selectedFailedNewAndChanges = listRemove;
			}
			selected = selectedFailedNewAndChanges;
		} else if (getTabActive() === 'all') {
			//debugger;
			var selectedPassedNewAndChangesTemp = [];
			var selectedFailedNewAndChangesTemp = [];
			if (dataRow.checkStatus === 'Y' || !_.isEmpty(dataRow.currentRetailChange) || !_.isEmpty(dataRow.nextRetailChange)) {
				if (inArrays(selectedAllNewAndChanges, itemObj)) {
					var listRemove = removeInArrays(selectedAllNewAndChanges, itemObj.rowId);
					var listUpdate = updateItems(listRemove, selectorTr);
				}else {
					var listUpdate = updateItems(selectedAllNewAndChanges, selectorTr);
				}
				selectedAllNewAndChanges = listUpdate;
			} else {
				var listRemove = removeInArrays(selectedAllNewAndChanges, itemObj.rowId);
				selectedAllNewAndChanges = listRemove;
			}
			
			_.each(selectedAllNewAndChanges, function(value, key){
				if (value.status === "PASS") {
					selectedPassedNewAndChangesTemp.push(value);
				} else {
					selectedFailedNewAndChangesTemp.push(value);
				}
			}) 
			
			selected = selectedAllNewAndChanges;
			selectedPassedNewAndChanges = selectedPassedNewAndChangesTemp;
			selectedFailedNewAndChanges = selectedFailedNewAndChangesTemp;
		}
    } );
	/*end select tr---------------*/
	
	/*smartfilter*/
	$("#new-and-changes-" + type + '-table  ' + '.second-tr > th > input').on( 'change', function () {//:not(.confirm-filter)
		//if(checkValidateFilter()){
			if (timer.isActive) {
				timer.stop();
			}
			
			timer.play();
		//}
	} );
	
	var timer = $.timer(function() {
		//if(checkValidateFilter()){
			table.columns($("#new-and-changes-" + type + '-table  ' + '.second-tr > th > input:text')).search($("#new-and-changes-" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
			timer.stop();
		//}
	});
	timer.set({ time : 1000, autostart : false });

	$("#new-and-changes-" + type + '-table  ' + '.second-tr > th > select').on( 'change', function () {
		//if(checkValidateFilter()){
			table.columns($("#new-and-changes-" + type + '-table  ' + '.second-tr > th > input:text')).search($("#new-and-changes-" + type + '-table  ' + '.second-tr > th > input:text').val()).draw();
		//}
	} );
	
	/*end smartfilter*/
	
	
	/*RETAIL CHANGE--------------------------*/
	$('#new-and-changes-' + type +'-table tbody').on('change', '.format-input-cell', function () {
		//debugger;
		var selectorTr = $(this).closest('tr');
		var dataRow = $("#new-and-changes-"+ getTabActive() +"-table").DataTable().row($("#" + $(selectorTr).attr('id'))).data();
		
		if ($(this).val() !== $(this).attr('tmp')) {
			if (_.isEmpty($(this).val())) {
				$(this).val(0);
			}
			$(this).addClass('change-background');
			if ($(this).closest('td').hasClass('next-retail-cls')) {
				dataRow.nextRetailChange = $(this).val();
				var retail = parseFloat($(this).val());
				var cost = parseFloat($(selectorTr).find(".next-cost-span").text());
				var margin = retail - cost;
				var gp = ((margin/cost) * 100).toFixed(2);
				dataRow.nextGPChange = gp;
			} else {
				dataRow.currentRetailChange = $(this).val();
				var retail = parseFloat($(this).val());
				var cost = parseFloat($(selectorTr).find(".current-cost-span").text());
				var margin = retail - cost;
				var gp = ((margin/cost) * 100).toFixed(2);
				dataRow.currentGPChange = gp;
			}
			
		} else {
			$(this).removeClass('change-background');
			if ($(this).closest('td').hasClass('next-retail-cls')) {
				dataRow.nextRetailChange = "";
				dataRow.nextGPChange = "";
			} else {
				dataRow.currentRetailChange = "";
				dataRow.currentGPChange = "";
			}
		}
		//change-background
		
		//save state
		var itemObj = {
			rowId: dataRow.rowId
		}
        
        if (getTabActive() === 'passed') {
        	//debugger;
        	if (dataRow.checkStatus === "Y" || !_.isEmpty(dataRow.currentRetailChange) || !_.isEmpty(dataRow.nextRetailChange)) {
        		if (inArrays(selectedPassedNewAndChanges, itemObj)) {
    				var listRemove = removeInArrays(selectedPassedNewAndChanges, itemObj.rowId);
    				var listUpdate = updateItems(listRemove, selectorTr);
    			}else {
    				var listUpdate = updateItems(selectedPassedNewAndChanges, selectorTr);
    			}
    			selectedPassedNewAndChanges = listUpdate;
        	} else {
        		var listRemove = removeInArrays(selectedPassedNewAndChanges, itemObj.rowId);
        		selectedPassedNewAndChanges = listRemove;
        	}
        	
        } else if(getTabActive() === 'failed') {
			//debugger;
        	if (dataRow.checkStatus === "Y" || !_.isEmpty(dataRow.currentRetailChange) || !_.isEmpty(dataRow.nextRetailChange)) {
        		if (inArrays(selectedFailedNewAndChanges, itemObj)) {
    				var listRemove = removeInArrays(selectedFailedNewAndChanges, itemObj.rowId);
    				var listUpdate = updateItems(listRemove, selectorTr);
    			}else {
    				var listUpdate = updateItems(selectedFailedNewAndChanges, selectorTr);
    			}
    			selectedFailedNewAndChanges = listUpdate;
        	} else {
        		var listRemove = removeInArrays(selectedFailedNewAndChanges, itemObj.rowId);
        		selectedFailedNewAndChanges = listRemove;
        	}

		} else if(getTabActive() === 'all') {
			var selectedPassedNewAndChangesTemp = [];
			var selectedFailedNewAndChangesTemp = [];
			//debugger;
			
			if (dataRow.checkStatus === "Y" || !_.isEmpty(dataRow.currentRetailChange) || !_.isEmpty(dataRow.nextRetailChange)) {
				if (inArrays(selectedAllNewAndChanges, itemObj)) {
					var listRemove = removeInArrays(selectedAllNewAndChanges, itemObj.rowId);
					var listUpdate = updateItems(listRemove, selectorTr);
				}else {
					var listUpdate = updateItems(selectedAllNewAndChanges, selectorTr);
				}
				selectedAllNewAndChanges = listUpdate;
			} else {
				var listRemove = removeInArrays(selectedAllNewAndChanges, itemObj.rowId);
				selectedAllNewAndChanges = listRemove;
			}
			
			_.each(selectedAllNewAndChanges, function(value, key){
				if (value.status === "PASS") {
					selectedPassedNewAndChangesTemp.push(value);
				} else {
					selectedFailedNewAndChangesTemp.push(value);
				}
			}) 
			
			selected = selectedAllNewAndChanges;
			selectedPassedNewAndChanges = selectedPassedNewAndChangesTemp;
			selectedFailedNewAndChanges = selectedFailedNewAndChangesTemp;
		}
		refreshRow($(selectorTr));
		$(this).val($.number($(this).val(), 2));
	});

	$('#new-and-changes-' + type +'-table tbody').on('focusout', '.format-input-cell', function () {
		$(this).val($.number($(this).val(), 2));
	});
	/*END RETAIL CHANGE----------------------*/
	
	table.on( 'xhr', function () {
		if (isEmptyValueFilter()) {
			resetCount = true;	
		} else {
			resetCount = false;
		}
		
		if(resetCount) {
		    var json = table.ajax.json();
		    $(".badge-passed-style").text("("  + json.passed + ")");
		    $(".badge-failed-style").text("("  + json.failed + ")");
		    $(".badge-all-style").text("("  + json.all + ")");
		    
		    var numberRowTabActive = getNumberRowTabActive();
			if(_.isEmpty(numberRowTabActive) || parseInt(numberRowTabActive) === 0){
			 	$("#cost-review-export-btn").addClass('disable-class');
			} else {
				$("#cost-review-export-btn").removeClass('disable-class');
			}
			resetCount = false;
		}
	});
	return table;
}

/*show competitor*/
//$(document).on("click",".show-competitor-cost-reiew", function () {
	//debugger;
//	$(".show-competitor-cost-reiew").popover('hide');
	
//	$(this).popover('show');
	
//	if ($(this).hasClass('is-show-competitor')) {
//		$(this).popover('hide');
//		$(this).removeClass('is-show-competitor');
//	} else {
//		$(this).popover('show');
//		$(this).addClass('is-show-competitor');
//	}
//});


/*[Main Scenario 5.1]*/
$(document).on("change",".current-retail-input", function () {
	removeAlert(getTabActive());
	var trSelector = $(this).closest("tr");
	var retail = parseFloat($(this).val());
	var cost = parseFloat($(trSelector).find(".current-cost-span").text());
	var margin = retail - cost;
	var gp = ((margin/cost) * 100).toFixed(2);
	$(trSelector).find(".current-gp-span").text(gp);
});
/*[End 5.1]*/


/*[Main Scenario 5.2]*/
$(document).on("change",".next-retail-input", function () {
	removeAlert(getTabActive());
	var trSelector = $(this).closest("tr");
	var retail = parseFloat($(this).val());
	var cost = parseFloat($(trSelector).find(".next-cost-span").text());
	var margin = retail - cost;
	var gp = ((margin/cost) * 100).toFixed(2);
	$(trSelector).find(".next-gp-span").text(gp);
});
/*[End 5.2]*/

/*[Main Scenario 15.1]*/
$(document).on("click",".approve-btn-cls", function () {
	bootbox.dialog({
	      title : "Confirmation",
	      message: "Do you wish to publish the cost and retail changes for selected products?",
	      buttons: {
	        main: {
	            label: "Yes",
	            className: "btn-primary",
	            callback: function() {
	            	updateProducts("/cost-review/approve", "Approve");           	
	            }
	          },  
	          cancel: {
	            label: "No",
	            className: "btn-default",
	            callback: function() {
	            	 
	            }
	          }                           
	        }   
	    }); 
});
/*[End 15.1]*/

/*[Main Scenario 15.1]*/
$(document).on("click",".accept-btn-cls", function () {
	bootbox.dialog({
	      title : "Confirmation",
	      message: "Do you wish to accept the cost and retail changes for selected products?",
	      buttons: {
	        main: {
	            label: "Yes",
	            className: "btn-primary",
	            callback: function() {
	            	updateProducts("/cost-review/accept", "Accept");
	            }
	          },  
	          cancel: {
	            label: "No",
	            className: "btn-default",
	            callback: function() {
	            	 
	            }
	          }                           
	        }   
	    }); 
});
/*[End 15.1]*/

/*[Main Scenario 17.1]*/
$(document).on("click",".reject-btn-cls", function () {
	bootbox.dialog({
	      title : "Confirmation",
	      message: "Do you wish to reject the cost and retail changes for selected products?",
	      buttons: {
	        main: {
	            label: "Yes",
	            className: "btn-primary",
	            callback: function() {            
	            	updateProducts("/cost-review/reject", "Reject");
	            }
	          },  
	          cancel: {
	            label: "No",
	            className: "btn-default",
	            callback: function() {
	            	 
	            }
	          }                           
	        }   
	    }); 
});
/*[End 17.1]*/

/*[Main Scenario 18.1]*/
$(document).on("click",".remove-btn-cls", function () {
	bootbox.dialog({
	      title : "Confirmation",
	      message: "Do you wish to unpublished selected products from the website?",
	      buttons: {
	        main: {
	            label: "Yes",
	            className: "btn-primary",
	            callback: function() {            
	            	updateProducts("/cost-review/remove", "Remove");
	            }
	          },  
	          cancel: {
	            label: "No",
	            className: "btn-default",
	            callback: function() {
	            	 
	            }
	          }                           
	        }   
	    }); 
});
/*[End 18.1]*/

/*[Main Scenario 5.3]*/
$(document).on("click",".save-btn-cls", function () {
	bootbox.dialog({
	      title : "Confirmation",
	      message: "Do you wish to save the changes?",
	      buttons: {
	        main: {
	            label: "Yes",
	            className: "btn-primary",
	            callback: function() {            
	            	updateProducts("/cost-review/save", "Save");
	            }
	          },  
	          cancel: {
	            label: "No",
	            className: "btn-default",
	            callback: function() {
	            	 
	            }
	          }                           
	        }   
	    }); 
});
/*[End 5.3]*/

/*Export Excel--------------------*/	
$("#cost-review-export-btn").click(function() {
	if (parseInt(getNumberRowTabActive()) > 0 ) {
		$("body").append('<div id="temp"></div>');
	    $("#temp").append('<form target="_blank" id="frmExcel" action="'+appName+'/cost-review/export-to-excel-directly" method="POST"></form>');                
	    
	    $("#frmExcel").append('<input type="hidden" name="tabActive" value="'+getTabActive()+'">');
	    $("#frmExcel").append('<input type="hidden" name="commodittyId" value="'+ searchCostReviewObject.commodittyId +'">');
	    $("#frmExcel").append('<input type="hidden" name="vendorID" value="'+ searchCostReviewObject.vendorId +'">');
	    $("#frmExcel").append('<input type="hidden" name="brand" value="'+ searchCostReviewObject.brand +'">');
	    $("#frmExcel").append('<input type="hidden" name="prodDes" value="'+ searchCostReviewObject.prodDes +'">');
	    $("#frmExcel").append('<input type="hidden" name="receiveDateBegin" value="'+ searchCostReviewObject.receiveDateBegin +'">');
	    $("#frmExcel").append('<input type="hidden" name="receiveDateEnd" value="'+ searchCostReviewObject.receiveDateEnd +'">');
	    $("#frmExcel").append('<input type="hidden" name="showAllMapCheck" value="'+ searchCostReviewObject.allMapProduct +'">');
	    $("#frmExcel").append('<input type="hidden" name="showOnlyPrePriceCheck" value="'+ searchCostReviewObject.onlyPrePriceProduct +'">');
	    $("#frmExcel").append('<input type="hidden" name="marginBegin" value="'+ searchCostReviewObject.marginBegin +'">');
	    $("#frmExcel").append('<input type="hidden" name="marginEnd" value="'+ searchCostReviewObject.marginEnd +'">');
	    $("#frmExcel").append('<input type="hidden" name="failReason" value="'+ searchCostReviewObject.failReason +'">');

	    var aaSorting = $("#new-and-changes-" + getTabActive() + '-table').dataTable().fnSettings().aaSorting;
		// sort direction.
		var sSortDir = typeof aaSorting[0][1] !== 'undefined' ? aaSorting[0][1] : 'asc';
		// Sort column.
		var iSortColumn = typeof aaSorting[0][0] !== 'undefined' ? aaSorting[0][0] : '1';

		$("#frmExcel").append('<input type="hidden" name="sSortDir" value="' + sSortDir + '">');
		$("#frmExcel").append('<input type="hidden" name="iSortColumn" value="' + iSortColumn + '">');
		$("#frmExcel").append('<input type="hidden" name="action" value="' + "exportExcel" + '">');
	    //$("#frmExcel").append('<input type="hidden" name="columnsJsonStr" value="'+ JSON.stringify(searchCostReviewObject.columnsJsonStr) +'">');
	    //$("#frmExcel").append('<input type="hidden" name="orderJsonStr" value="'+ JSON.stringify(searchCostReviewObject.orderJsonStr) +'">');
//	    d.columnsJsonStr = JSON.stringify(d.columns);
//	    d.orderJsonStr = JSON.stringify(d.order);
	    
	    //$("#frmExcel").removeAttr("target");
        $('#frmExcel').submit();
        $("#temp").remove();
	}
    
});
/*End export Excel----------------*/

//$(document).on("change",".tab-content", function () {
//	checkAndSetChangeStatus();
//	checkAndSetButtonStatus();
//});

$(document).ajaxSuccess(function() {
	checkAndSetChangeStatus();
	checkAndSetButtonStatus();
	checkAndDisabledInput();
})

$(document).on("change",".tab-content", function () {
	checkAndSetChangeStatus();
	checkAndSetButtonStatus();
});

$(document).on("click",".filter-btn-style", function () {
	resetFilter();
});


function getListByTabActive() {
	var list = [];
	if (getTabActive() === 'passed') {
		list = selectedPassedNewAndChanges;
	} else if (getTabActive() === 'failed') {
		list = selectedFailedNewAndChanges;
	} else {
		list = mergeList(selectedPassedNewAndChanges, selectedFailedNewAndChanges);
	}
	return list;
}

function updateProducts(url, revStat){
	var list = getListByTabActive();
	
	if (revStat === 'Reject') {
		_.each(list, function(value, key) {
			value.nextRetailChange = _.isEmpty(value.currentRetailChange) ? value.currentRetail : value.currentRetailChange;
		});
	}
	$.ajax({
		type: 'POST',
		url: appName + url,
		data:{
			strJson: JSON.stringify(list)
		}
	}).success(function(data) {
		selectedPassedNewAndChanges = [];
		selectedFailedNewAndChanges = [];
		selectedAllNewAndChanges = [];
		showAlertSuccess(revStat + " " +"successfully", getTabActive());
		$("#new-and-changes-"+ getTabActive() +"-table").DataTable().ajax.reload();
	}).done(function() {
	}).fail(function() {
	}).always(function() {
	});
}


/*-------------------------------------------*/
/*END TABLE----------------------------------*/
/*-------------------------------------------*/

/*-------------------------------------------*/
/*FUNCTION-----------------------------------*/
/*-------------------------------------------*/
function getTabActive() {
	return tabActive;
}

function removeInArrays(itemsTmp, id) {
	var val = _.filter(itemsTmp, function(item){ 
		return item.rowId !== id 
	});
	return val;
}

function updateItems(items, selectorTr) {
//	var row = {
//			id:,
//			select:$(selectorTr).find("td:eq(0)").find("input").prop('disabled'),
//			currentRetail: $(selectorTr).find("td:eq(4)").find("input").val(),
//			nextRetail: $(selectorTr).find("td:eq(9)").find("input").val()
//	}
	
	var row = $("#new-and-changes-" + getTabActive() + "-table").DataTable().row($("#" + $(selectorTr).attr('id'))).data();

	if ($(selectorTr).attr('id') !== "") {
		items.push(row);	
	}
	return items;
}

function getItemInArraysByKey (itemsTmp, itemObj) {
	return _.where(itemsTmp, itemObj); 
}

function inArrays (itemsTmp, itemObj) {
	return _.size(_.where(itemsTmp, itemObj)) === 0 ? false : true; 
}

//function removeInArrays(itemsTmp, id) {
//	var val = _.filter(itemsTmp, function(item){ return item.id !== id });
//	return val;
//}

function fillData (selectorTr, itemObj) {
	//debugger;
	
	if (!_.isEmpty(itemObj.currentRetailChange)) {
		$(selectorTr).find(".current-retail-input").val(itemObj.currentRetailChange);
		$(selectorTr).find(".current-gp-span").text(itemObj.currentGPChange);
	}
	if (!_.isEmpty(itemObj.nextRetailChange)) {
		$(selectorTr).find(".next-retail-input").val(itemObj.nextRetailChange);
		$(selectorTr).find(".next-gp-span").text(itemObj.nextGPChange);
	}
	
	if (itemObj.checkStatus === 'Y') {
		$(selectorTr).find(".checkbox-class").prop('checked', true);
		$(selectorTr).addClass('success');
	} else {
		$(selectorTr).find(".checkbox-class").prop('checked', false);
		$(selectorTr).removeClass('success');
	}
}

function updateForDataRow(dataRow, itemObj) {
	dataRow.checkStatus = itemObj.checkStatus;
	dataRow.currentRetailChange = itemObj.currentRetailChange;
	dataRow.currentRetail = itemObj.currentRetail;
	dataRow.nextRetailChange = itemObj.nextRetailChange;
	dataRow.nextRetail = itemObj.nextRetail;	
	dataRow.currentGPChange = itemObj.currentGPChange;
	dataRow.nextGPChange = itemObj.nextGPChange;
}

function checkChangeForInputCell (selectorTr, itemObj) {
		if (!_.isEmpty(itemObj.currentRetailChange) && itemObj.currentRetailChange !== $(selectorTr).find(".current-retail-input").attr('tmp')) {
			$(selectorTr).find(".current-retail-input").addClass('change-background');
		}
		if (!_.isEmpty(itemObj.nextRetailChange) && itemObj.nextRetailChange !== $(selectorTr).find(".next-retail-input").attr('tmp')) {
			$(selectorTr).find(".next-retail-input").addClass('change-background');
		}
}

function showAlertDanger(errorMessage, status) {
	 //remove class
	 $("#alert-wp-" + status).find('.alert').removeClass('alert-success');
	 //add class
	 $("#alert-wp-" + status).find('.alert').addClass('alert-danger');
	 //show
	 $("#alert-wp-" + status).removeClass('hidden');
	 //set message
	 $("#message-alert-" + status).html(errorMessage);
}

function showAlertSuccess(errorMessage, status) {
	 //remove class
	 $("#alert-wp-" + status).find('.alert').addClass('alert-success');
	 //add class
	 $("#alert-wp-" + status).find('.alert').removeClass('alert-danger');
	 //show
	 $("#alert-wp-" + status).removeClass('hidden');
	 //set message
	 $("#message-alert-" + status).html(errorMessage);
}

function removeAlert(status) {
	 $("#alert-wp-" + status).addClass('hidden');
	 $("#message-alert-" + status).text(CONST.EMPTY_STRING);
}

function setSearchCostReviewObject() {
//	searchCostReviewObject.tabActive = getTabActive(),

	searchCostReviewObject.commodittyId = $("#commodities-input").attr("commodities-input-id") === undefined ? "" : $("#commodities-input").attr("commodities-input-id"),
	searchCostReviewObject.vendorId = $("#vendor-input").val(),
	searchCostReviewObject.brand = $("#brand-input").val(),
	searchCostReviewObject.prodDes = $("#product-input").val(),
	searchCostReviewObject.receiveDateBegin = $("#dateBegin").val(),
	searchCostReviewObject.receiveDateEnd = $("#dateEnd").val(),
	searchCostReviewObject.allMapProduct = $('#show-all-map-check').is(":checked"),
	searchCostReviewObject.onlyPrePriceProduct = $('#show-only-prePrice-check').is(":checked"),
	searchCostReviewObject.marginBegin = $("#marginBegin").val(),
	searchCostReviewObject.marginEnd = $("#marginEnd").val(),
	searchCostReviewObject.failReason = $("#failed-input").val()
}

/*getNumberRowTabActive-----------*/
function getNumberRowTabActive() {
	var type = getTabActive();
	var numberRowStr;
	if(type == "all") {
		numberRowStr = $("#badge-all").html();
	}else if(type == "passed") {
		numberRowStr = $("#badge-passed").html();
	}else if(type == "failed") {
		numberRowStr = $("#badge-failed").html();
	}else {
		numberRowStr = $("#badge-rejected").html();
	}
	return $.trim(numberRowStr).substring(1, numberRowStr.length - 1);
}
/*end getNumberRowTabActive-------*/

function mergeList(passedList, failedList) {
	var allList = [];
	_.each(passedList, function(value, key) {
		allList.push(value);
	});
	_.each(failedList, function(value, key) {
		allList.push(value);
	});
	return allList;
}

function checkAndSetChangeStatus() {
	 if (_.size(selectedAllNewAndChanges) > 0 || _.size(selectedFailedNewAndChanges) > 0 || _.size(selectedPassedNewAndChanges) > 0) {
		setChangeStatus(true);
	 } else {
     	setChangeStatus(false);
	 }
}

function checkAndSetButtonStatus() {
	//debugger;
	var status = getTabActive();
	
	if (status === 'passed') {
		if (countObjectChange(selectedPassedNewAndChanges) > 0) {
			$("#save-btn-" + status).prop('disabled', false);
		} else {
			$("#save-btn-" + status).prop('disabled', true);
		}
		
		if (countObjectChecked(selectedPassedNewAndChanges) > 0) {
			$("#approve-btn-" + status).prop('disabled', false);
			$("#reject-btn-" + status).prop('disabled', false);
			$("#remove-btn-" + status).prop('disabled', false);
		 } else {
			$("#approve-btn-" + status).prop('disabled', true);
			$("#reject-btn-" + status).prop('disabled', true);
			$("#remove-btn-" + status).prop('disabled', true);
		 }
	} else if(getTabActive() === 'failed') {
		if (countObjectChange(selectedFailedNewAndChanges) > 0) {
			$("#save-btn-" + status).prop('disabled', false);
		} else {
			$("#save-btn-" + status).prop('disabled', true);
		}
		
		if (countObjectChecked(selectedFailedNewAndChanges) > 0) {
			$("#accept-btn-" + status).prop('disabled', false);
			$("#reject-btn-" + status).prop('disabled', false);
			$("#remove-btn-" + status).prop('disabled', false);
		} else {
			$("#accept-btn-" + status).prop('disabled', true);
			$("#reject-btn-" + status).prop('disabled', true);
			$("#remove-btn-" + status).prop('disabled', true);
		}
	} else if(getTabActive() === 'all') {
		
		if (countObjectChange(selectedPassedNewAndChanges) > 0 || countObjectChange(selectedFailedNewAndChanges) > 0) {
			$("#save-btn-" + status).prop('disabled', false);
		} else {
			$("#save-btn-" + status).prop('disabled', true);
		}
		
		if (countObjectChecked(selectedPassedNewAndChanges) > 0 && countObjectChecked(selectedFailedNewAndChanges) > 0) {
			$("#accept-btn-" + status).prop('disabled', true);
			$("#approve-btn-" + status).prop('disabled', true);
			$("#reject-btn-" + status).prop('disabled', false);
			$("#remove-btn-" + status).prop('disabled', false);
		} else if (countObjectChecked(selectedPassedNewAndChanges) > 0 || countObjectChecked(selectedFailedNewAndChanges) > 0) {
			if (countObjectChecked(selectedFailedNewAndChanges) > 0) {
				$("#accept-btn-" + status).prop('disabled', false);
			} else {
				$("#accept-btn-" + status).prop('disabled', true);
			}
			if (countObjectChecked(selectedPassedNewAndChanges) > 0) {
				$("#approve-btn-" + status).prop('disabled', false);
			} else {
				$("#approve-btn-" + status).prop('disabled', true);
			}
			
			$("#reject-btn-" + status).prop('disabled', false);
			$("#remove-btn-" + status).prop('disabled', false);	
		} else {
			if (countObjectChecked(selectedFailedNewAndChanges) > 0) {
				$("#accept-btn-" + status).prop('disabled', false);
			} else {
				$("#accept-btn-" + status).prop('disabled', true);
			}
			
			if (countObjectChecked(selectedPassedNewAndChanges) > 0) {
				$("#approve-btn-" + status).prop('disabled', false);
			} else {
				$("#approve-btn-" + status).prop('disabled', true);
			}
			
			$("#reject-btn-" + status).prop('disabled', true);
			$("#remove-btn-" + status).prop('disabled', true);
		}
	}
}

function countObjectChange(list) {
	var count = 0;
	_.each(list, function(value, key) {
		if (!_.isEmpty(value.currentRetailChange) || !_.isEmpty(value.nextRetailChange)) {
			count++;
			return;
		}
	});
	return count;
}

function countObjectChecked(list) {
	var count = 0;
	_.each(list, function(value, key) {
		if (value.checkStatus === 'Y') {
			count++;
			return;
		}
	});
	return count;
}

function countObjectStatusFail(list) {
	var count = 0;
	_.each(list, function(value, key) {
		if (value.status === 'FAIL') {
			count++;
			return;
		}
	});
	return count;
}

function checkAndDisabledInput() {
	_.each($("#new-and-changes-"+getTabActive()+"-table tbody tr[id]"), function(value, key){
		var dataRow = $("#new-and-changes-"+ getTabActive() +"-table").DataTable().row($(value)).data();
		if (dataRow.revStatCd === 'APPRV' || dataRow.revStatCd === 'RJECT' || dataRow.revStatCd === 'REMOV') {
			$(value).find(".checkbox-class").prop('disabled', true);
			$(value).find(".current-retail-input").prop('disabled', true);
			$(value).find(".next-retail-input").prop('disabled', true);
		}
	});
}

function refreshRow(trSelector) {
	var dataRow = $("#new-and-changes-"+ getTabActive() +"-table").DataTable().row($(trSelector)).data();
	var currentRetail = _.isEmpty(dataRow.currentRetailChange) ? dataRow.currentRetail : dataRow.currentRetailChange;
	var nextRetail = _.isEmpty(dataRow.nextRetailChange) ? dataRow.nextRetail : dataRow.nextRetailChange;
	var currentGP = _.isEmpty(dataRow.currentGPChange) ? dataRow.currentGP : dataRow.currentGPChange;
	var nextGP = _.isEmpty(dataRow.nextGPChange) ? dataRow.nextGP : dataRow.nextGPChange;
	
	
	$(trSelector).find(".next-retail-input").removeClass('label-danger-input');
	$(trSelector).find(".next-retail-input").removeClass('label-info-input');
	$(trSelector).find(".next-gp-span").removeClass('label-info');
	$(trSelector).find(".next-gp-span").removeClass('label');
	$(trSelector).find(".next-gp-span").removeClass('label-danger');
	// label-danger-input 
	//label-info-input
	//label label-danger
	//label label-info
	//label label-danger
	if (currentRetail < nextRetail) {
		$(trSelector).find(".next-retail-input").addClass('label-info-input');
	} else if (currentRetail > nextRetail) {
		$(trSelector).find(".next-retail-input").addClass('label-danger-input');
	}
	
	if (currentGP < nextGP) {
		$(trSelector).find(".next-gp-span").addClass('label');
		$(trSelector).find(".next-gp-span").addClass('label-info');
	} else if (currentGP > nextGP) {
		$(trSelector).find(".next-gp-span").addClass('label');
		$(trSelector).find(".next-gp-span").addClass('label-danger');
	}
}

function resetFilter() {
	if (!isEmptyValueFilter()) {
		$("#new-and-changes-"+getTabActive()+"-table").find(".input-filter").val("");
		$("#new-and-changes-"+ getTabActive() +"-table").DataTable().ajax.reload();
	}
}
/*-------------------------------------------*/
/*END FUNCTION-------------------------------*/
/*-------------------------------------------*/
/************************************************************************************************************
 * END SEARCH RESULT
 ************************************************************************************************************/
});
