// Invoice Type DB Constants
var INV_TYP = { INVALID: '0', COGS:'1', FREIGHT: '2', BOTH: '3'};
/**
 * Rule will save for reload data.
 */
var rules = null;
/**
 * Changed status.
 */
var _changed = false;
/**
 * Loaded data successful.
 */
var _loaded = false;

/**
 * ==================
 * Auto complete js variable 
 * @author giau.le
 * ===================
 */
var dataAutoCompleteInput = [];
var dataAutoCompleteInputLabel= [];
var dataAutoCompleteInputValue= [];
var dataAutoCompleteInputName= [];
var selectedLabelBefore = "";
var selectedIdBefore = "";
var idInput = 'vendor-input';


/**
 * Ready document.
 * @author ANNGUYEN
 */
$(document).ready(function() {
	clickDropAutoComplete();
	getVendorListLoadFirst();
	$('#vendor-input').autocomplete({
		search: function(event, ui) {
            $(this).addClass("wait");
            $(this).parent().find("span").removeClass("caret");
            if(dataAutoCompleteInput.length == 0){
            	$(this).removeClass("wait");
            	$(this).parent().find("span").addClass("caret");
            }
        },
        open: function(event, ui) {
            $(this).removeClass("wait");
            $(this).parent().find("span").addClass("caret");
        },
		source: getVendorList,
		minLength: 3,
		select: function(e, ui) {
			onSelectAutoComplete(e, ui);
			if (selectedIdBefore != ui.item.value) {
				selectedLabelBefore = ui.item.label;
				selectedIdBefore = ui.item.value;
				getVendorRules(ui.item.value);
			}
			return false;
		},
		focus: function(e, ui) {
            selectedLabelBefore = ui.item.label;
			return false;
		}
	}).focusout(function() {
        if(!$(this).attr("vendor-inputid")){
            $(this).attr("vendor-inputid", selectedIdBefore);
        } else {
        	if($(this).attr("vendor-inputname")) {
        		$(this).val($(this).attr("vendor-inputlabel"));
        	}
        }
        
    }).on("keyup", function(e) {
    	e.preventDefault();
    	var vendorId = $(this).val().split('-')[0];
    	var vendorName = $(this).val().split('-')[1];
    	// enter key
        if (e.keyCode == 13 ) {
            if($(this).attr("vendor-inputid")){
            	$(this).val( $(this).attr('vendor-inputlabel'));
            } 
        	
            // if vendorId is different than before
            if ($(this).attr("vendor-inputid")) {
            	if ($('.txt-search-vendor-header').val().length > 0 && 
        			selectedIdBefore != $(this).attr("vendor-inputid") && 
        			$.inArray( $.trim(vendorId), dataAutoCompleteInputValue) != -1) {
        		 getVendorRules($(this).attr("vendor-inputid"));
        		}
            }else{
            	if ($('.txt-search-vendor-header').val().length > 0 && 
        			selectedIdBefore != vendorId && 
        			$.inArray( $.trim(vendorId), dataAutoCompleteInputValue) != -1 &&
        			$.inArray( $.trim(vendorName), dataAutoCompleteInputName) != -1) {
        		 	getVendorRules(vendorId);
	        		$(this).attr("vendor-inputid", vendorId);
	        		$(this).attr("vendor-inputlabel", $(this).val());
	        		$(this).attr("vendor-inputname", vendorName);
	        		selectedIdBefore = vendorId;
	        		selectedLabelBefore = $(this).val();
        		}
            };
        	
        } 

    }).focus(); 

	addEvents();
	
});

/**
 * When select or focus on auto complete box.
 * @param e
 * @param ui
 * @author giaule
 */
function onSelectAutoComplete(e, ui) {
	 e.preventDefault();
     $("#"+idInput).val(ui.item.label);
     if ($.inArray($("#"+idInput).val(), dataAutoCompleteInputLabel) !== -1 
             || $.inArray($("#"+idInput).val(), dataAutoCompleteInputValue) !== -1) {
  	   
  		   $( "#" + idInput ).attr( idInput +"id", ui.item.value );
  		   $( "#" + idInput ).attr( idInput +"name", ui.item.name );
  		   $( "#" + idInput ).attr( idInput +"label", ui.item.label );
     } else {
       $( "#" + idInput ).attr( idInput +"id", "");
     }
}

/**
 * getVendorListLoadFirst.
 * @author giaule
 */
function getVendorListLoadFirst() {
	var inputRaw = idInput.split("-");
	var nameProperty = inputRaw[0];
	
	//remove loading screen
	$(document).unbind('ajaxSend');
    $.ajax({
        contentType: "application/json; charset=utf-8",
        url: appName + '/vendor-rule/get-autocomplete-data',
        data: {"name" : $( "#" + idInput ).val()},
        dataType: "json",
        success: function (data) {
        	if (!data || !data.data) {
        		$("#rules-result").css("display", "none");
        		return;
        	}
			dataAutoCompleteInput = data.data;
        	for (var i in dataAutoCompleteInput) {
				dataAutoCompleteInputLabel.push(dataAutoCompleteInput[i].label);
				dataAutoCompleteInputValue.push(dataAutoCompleteInput[i].value);
				dataAutoCompleteInputName.push(dataAutoCompleteInput[i].name);
			}

            //re-attach loading screen
            $(document).bind("ajaxSend", function(e, xhr, settings){
            	$.blockUI({
                    message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
                    fadeIn: 100,
                    onUnblock: function() {
                    }
                });
            }).bind("ajaxStop", function(){
            	$.unblockUI();
            }).bind('ajaxError', handleAjaxError);
        },
        error: function (result) {
        	showNotify('The system has encountered an error. Please try again later.', 'danger');
        }
    });
}

/**
 * Add events after page load successful.
 * @author ANNGUYEN
 */
function addEvents() {
	$("#vendor-radio :radio").iButton({allowRadioUncheck: true, duration:0});
	
	/**
	 * Two check Automatically approve and Automatically use lower change event.
	 */ 
	$("#formVendorRules input[name='chk-approve'],input[name='chk-use-lower']").iButton({
		duration:0,
		labelOff: "OFF",
		labelOn: "ON",
		easing: "swing",
		resizeHandle: "auto",
		enableDrag: false,
		resizeContainer: false,
		change: function ($input){
			if ($input.is(":checked")) {
				$($input).parent().addClass('ibutton-container-on');
			}else {
				$($input).parent().removeClass('ibutton-container-on');
			}
			enableResetBtn();
			validateForm();
	    	$('#assortment-save-btn').focus();
		},
		init: function($input){
			if ($input.is(":checked")) {
				$($input).parent().addClass('ibutton-container-on');
			}else {
				$($input).parent().removeClass('ibutton-container-on');
			}
		}
	});
	
	/**
	 * Three attributes of vendor change event.
	 */
	$('#formVendorRules input:radio').change(function(e){
		// FF fixed
		$(this).focus();
		enableResetBtn();
		// TODO: get another way to disable submit button
		var isNotchecked = $('#formVendorRules input:radio:checked').length == 0;
		if ( isNotchecked && !$('#assortment-save-btn').prop('disabled')) {
			disableSubmitBtn();
		}
	});
	
    /*
     * Vendor attribute radio change event.
     */
	$('#vendor-radio input:radio').change(function(e){
		$('#vendor-radio input:text').each( function(){
			$(this).val('');
			showError(false);
		});
        $('#vendor-radio input:radio').each(function (){
            
			if ($(this).is(':checked')) {
	            var idCheckRadio = $(this).attr("id");
	            $("#"+idCheckRadio+"-show").prop('disabled',false);
	            $("#"+idCheckRadio).parent().addClass('ibutton-container-on');
	            $("#"+idCheckRadio+"-show").focus();
	        }else{
	        	var idUnCheckRadio = $(this).attr("id");
	        	$("#"+idUnCheckRadio+"-show").prop('disabled',true);
	        	$("#"+idUnCheckRadio).parent().removeClass('ibutton-container-on');
	        }
		});
        
       
    });
	
	/**
	 * Format input value.
	 * @author ANNGUYEN
	 */
	$("#vendor-radio input:text").on('focusout', function(e) {
		// FF error so add args
		var toElem = (window.event) ? (e.relatedTarget || e.toElement) : e.relatedTarget;
		if (toElem && toElem.id == 'assortment-reset-btn') {
			e.preventDefault();
			return;
		}
		if ($(this).val().length > 0) {
			if ($.number($(this).val(), 2) == 0.00) {
				// Error validator
				$(this).val('').change();
				validateForm();
			} else {
				var str = $(this).val();
				// remove at the end
//				if (str.charAt(str.length - 1) == '.' ) {
//					$(this).val(str.substring(0, str.length - 1) );
////					$(this).val($.number($(this).val(), 2));
//				}
				$(this).val($.number(str, 2).replace(/\,/g, ''));
			}
		} else {
			validateForm();
		}
	});
	$('.currency-format-ipt').on('input change', validateForm);
	$('#assortment-reset-btn').click(loadVendorRules);
	$('#assortment-save-btn').click(function(e) { e.preventDefault(); saveVendorRules(); });
}
/**
 * return true if data has just loaded. Not change any more.
 * @returns {Boolean}
 * @author ANNGUYEN
 */
function isJustLoaded() {
	return !(_loaded || _changed);
}

/**
 * get tolerance textbox.
 * @returns
 * @author ANNGUYEN
 */
function getToleranceBox() {
	var tbox = null;
	var checked_inputs = $('#formVendorRules input:radio:checked');
	if (checked_inputs.length > 0) {
		tbox = $('#' + checked_inputs[0].id + '-show');
	}
	return tbox;
}

/**
 * Show or hide error and tooltip on tolerance textbox.
 * @param showed
 * @param tbox
 * @author ANNGUYEN
 */
function showError(showed, tbox) {
	if (isJustLoaded() ) {
		return;
	}
	showed = typeof showed !== 'undefined' ? showed : true;
	if (showed) {
		tbox = typeof tbox !== 'undefined' ? tbox : getToleranceBox();
		if (!tbox || !tbox.length) {
			return;
		}
		tbox.closest('.form-group').addClass('has-error');
		tooltipForValidation(tbox[0].id, 'Invoice level tolerance is required!');
	} else {
		removeErrors();
	}
}
/**
 *  Remove error and tooltip on tolerance textbox.
 *  @author ANNGUYEN
 */
function removeErrors() {
	$('.has-error').removeClass('has-error');
	$('[data-toggle="tooltip"]').tooltip('destroy');
}

/**
 * Validate tolerance value.
 * @author ANNGUYEN
 */
function validateForm() {
	var is_valid = false;
	var tbox = getToleranceBox();
	if (!tbox || !tbox.length || !tbox.val()) {
		is_valid = false;
	} else {
		var tolerance = tbox.val();
		var num_pattern = /^\d*\.?\d*$/;
		is_valid = num_pattern.test(tolerance);
	}
//	console.log(is_valid);
	disableSubmitBtn(!is_valid);
	showError(!is_valid, tbox);
}

/**
 * enable or disable by data change or not (not yet apply now).
 * @author ANNGUYEN
 */
function changeStatusSubmitBtn() {
	var my_changed = checkChanged();
	disableSubmitBtn(!my_changed);
}

/**
 * Check if data changed or not (not yet apply now).
 * @returns {Boolean}
 * @author ANNGUYEN
 */
function checkChanged() {
	if (!rules) {
		return false;
	}
	var newRules = getInputVendorRules();
	// if empty tolerence
	if (!newRules.invoiceTolerence || newRules.invoiceType == INV_TYP.INVALID ||
			$.number(rules.invoiceTolerence, 2) == 0.00) {
		return false;
	}
	var my_changed = newRules.invoiceType != rules.invoiceType ||
	newRules.autoApprove != rules.autoApprove ||
	newRules.autoUseLower != rules.autoUseLower ||
	$.number(newRules.invoiceTolerence, 2) != $.number(rules.invoiceTolerence, 2);
	return my_changed;
}

/**
 * Get vendor list - ajax.
 * @param request
 * @param response
 * @author ANNGUYEN
 */
function getVendorList(request, response) {
	var inputRaw = idInput.split("-");
	var nameProperty = inputRaw[0];
	
	//remove loading screen
	$(document).unbind('ajaxSend');
	var vendor = $( "#" + idInput ).val();
	var vendorId = '';
	var vendorName = '';
	if(vendor.split('-').length == 2){
		vendorId = $.trim(vendor.split('-')[0]);
    	vendorName = $.trim(vendor.split('-')[1]);
		if(vendorName == ''){
			vendorName = vendorId;
			vendorId = '';
		}
	}else{
		vendorId = "";
		vendorName = $.trim(vendor);
	}
	
    $.ajax({
        contentType: "application/json; charset=utf-8",
        url: appName + '/vendor-rule/get-autocomplete-data',
        data: {"name" : vendorName, "id":vendorId},
        dataType: "json",
        success: function (data) { 	
        	if (!data || !data.data) {
        		$("#rules-result").css("display", "none");
//        		$('#formVendorSearch').data('bootstrapValidator').updateStatus('vendor', 'INVALID', 'notEmpty');
        		return;
        	}
			dataAutoCompleteInput = data.data;
        	for (var i in dataAutoCompleteInput) {
				dataAutoCompleteInputLabel.push(dataAutoCompleteInput[i].label);
				dataAutoCompleteInputValue.push(dataAutoCompleteInput[i].value);
				dataAutoCompleteInputName.push(dataAutoCompleteInput[i].name);
			}

            response(dataAutoCompleteInput);

            //re-attach loading screen
            $(document).bind("ajaxSend", function(e, xhr, settings){
            	$.blockUI({
                    message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
                    fadeIn: 100,
                    onUnblock: function() {
                    }
                });
            }).bind("ajaxStop", function(){
            	$.unblockUI();
            }).bind('ajaxError', handleAjaxError);
        },
        error: function (result) {
        	showNotify('The system has encountered an error. Please try again later.', 'danger');
        }
    });
}




/**
 * Reset to the previous loaded data.
 * @author ANNGUYEN
 */
function resetRules() {
	$("#for-cogs").iButton("toggle", false);
	$("#for-cogs-show").val("");
	$("#for-freight").iButton("toggle", false);
	$("#for-freight-show").val("");
	$("#for-both").iButton("toggle", false);
	$("#for-both-show").val("");
	
	$("input[name='chk-approve']").iButton("toggle", false);
	$("input[name='chk-use-lower']").iButton("toggle", false);
	
//	showError(false);
	setDisableVendorAttrs(true);
//	_loaded = true;
}

/**
 * load Vendor Rules to GUI from JS.
 * @author ANNGUYEN
 */
function loadVendorRules() {
	_loaded = false;
	if (!rules) {
		resetRules();
		return;
	}
	
	if (rules.invoiceType == INV_TYP.COGS) {
		$("#for-cogs").iButton("toggle", true);
		$("#for-cogs-show").val(rules.invoiceTolerence);
	} else if (rules.invoiceType == INV_TYP.FREIGHT) {
		$("#for-freight").iButton("toggle", true);
		$("#for-freight-show").val(rules.invoiceTolerence);
	} else if (rules.invoiceType == INV_TYP.BOTH) {
		$("#for-both").iButton("toggle", true);
		$("#for-both-show").val(rules.invoiceTolerence);
	} else {
		// reset radio group
        $('#vendor-radio input:checkbox, #vendor-radio input:radio').each( function(){
        	$(this).iButton("toggle", false);
        });
	}
	
	$("input[name='chk-approve']").iButton("toggle", rules.autoApprove);
	$("input[name='chk-use-lower']").iButton("toggle", rules.autoUseLower);
	
//	showError(false);
	setDisableVendorAttrs(true);
//	_loaded = true;
}

/**
 * set is Disable to submit button.
 * @param isDiable
 * @author ANNGUYEN
 */
function setDisableVendorAttrs(disabled) {
	_changed = false;
	enableResetBtn(false);
	disableSubmitBtn(true);
	removeErrors();
//	showError(false);
	_loaded = true;
}

/**
 * When user change input or checkbox, open reset btn.
 * @param enabled
 * @author ANNGUYEN
 */
function enableResetBtn(enabled) {
	if (isJustLoaded()) {
		return;
	}
	if (!_loaded) {
		if (_changed) {
			$('#assortment-reset-btn').prop('disabled', true);
			_changed = false; // then will return in future
		}
	} else {
		_changed = true;
		enabled = enabled || true;
		$('#assortment-reset-btn').prop('disabled', !enabled);
	}
}
/**
 * disable Submit button.
 * @param disabled
 * @author ANNGUYEN
 */
function disableSubmitBtn(disabled) {
	if(typeof disabled === 'undefined'){
		disabled = true;
	};
	$('#assortment-save-btn').prop('disabled', disabled);
}

/**
 * get Vendor Invoice Mathcing rules - AJAX.
 * @author ANNGUYEN
 */
function getVendorRules(vendor) {
	/*if (!$('#vendor-input').attr('vendor-inputid')) {
		$('#vendor-input').attr('vendor-inputname', "");
		$("#rules-result").css("display", "none");
		return;
	};*/
    $.ajax({
        type: "GET",
        beforeSend: function(jqXHR, settings) {
        	 $.blockUI({
    	        message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
    	        fadeIn: 100,
    	        timeout: 100,
    	        onUnblock: function() {
    	        	$("#rules-result").css("display", "block");
    	        }
    	    });
        },
        data: { 
        	vendor: vendor
        },
        url: "/assortment/vendor-rules/get-rules",
        success: function(response) {
        	// save current rules
        	rules = response.vendorRules;
        	loadVendorRules();
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	showNotify('The system has encountered an error. Please try again later!','danger');
        },
        complete: function(jqXHR, textStatus) {
        	$.unblockUI();
        }
    });
}

/**
 * Get vendor Rule from UI.
 * @returns {___vendorRules}
 * @author ANNGUYEN
 */
function getInputVendorRules() {
	var vendorRules = {};
	vendorRules.vendorId = $('#vendor-input').attr("vendor-inputid");
	vendorRules.invoiceType = INV_TYP.INVALID;
	vendorRules.invoiceTolerence = '0';
	vendorRules.autoApprove = $('input[name="chk-approve"]').prop('checked');
	vendorRules.autoUseLower = $('input[name="chk-use-lower"]').prop('checked');
	if ($("#for-cogs").prop('checked')) {
		vendorRules.invoiceType = INV_TYP.COGS;
		vendorRules.invoiceTolerence = $('#for-cogs-show').val();
	}else if ($("#for-freight").prop('checked')) {
		vendorRules.invoiceType = INV_TYP.FREIGHT;
		vendorRules.invoiceTolerence = $('#for-freight-show').val();
	}else if ($("#for-both").prop('checked')) {
		vendorRules.invoiceType = INV_TYP.BOTH;
		vendorRules.invoiceTolerence = $('#for-both-show').val();
	}
	return vendorRules;
}


/**
 * Save Vendor Invoice Mathcing rules - AJAX.
 * @author ANNGUYEN
 */
function saveVendorRules() {
    var vendorRules = getInputVendorRules();
	if ( $.number(vendorRules.invoiceTolerence, 2) ===  0.00 ) {
		showNotify('Invoice level tolerance is required!', 'danger');
		return;
	}
//	vendorRules.positions =  JSON.stringify(getChangePosition());
	$.ajax({
        type: "POST",
        dataType: "json",
        beforeSend: function(jqXHR, settings) {
        	$.blockUI({
                message: '<h4><img src="/assortment/image/loading.gif" /> Please wait...</h4>',
                fadeIn: 100,
                onUnblock: function() {
                }
            });
        },
        data: vendorRules,
        url: "/assortment/vendor-rules/save-rules",
        success: function(data, textStatus, jqXHR) {
        	if (data.success) {
        		// save current rules
        		rules = getInputVendorRules();
        		var mess = $('#vendor-input').attr('vendor-inputname') +"'s configuration has been updated successfully!";
        		showNotify(mess, 'success');
        	} else {
        		showNotify(data.message, 'danger');
        	}
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	var vendor_name = "";
        	if ($('#vendor-input').attr('vendor-inputname')) {
        		vendor_name = $('#vendor-input').attr('vendor-inputname');
        	}
        	var mess = 'Failed to update ' +vendor_name  + '.';
        	showNotify(mess, 'danger');
        },
        complete: function(jqXHR, textStatus) {
        	$.unblockUI();
        	setDisableVendorAttrs(true);
        }
    });
}

