/**
 * This file contains functions to validate.
 * @author giaule
 */

var Constants = {
		EMPTY_STRING : "",
		SPACE_STRING : " ",
		MAX_DIGIT: 3,
		AFTER_DIGIT: 2,
		TOTAL_NUMBER: 11, //for validate decimal number
		TIME_OUT_TT: 1500,
		INV_MAX_LEN: 50
};
var ctrlKey = 17;
var ctrlDown = false;
/**
 * Check date string.
 * @author thanghoang
 */
function isDate(s) {
	var regexp = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
	var test = regexp.test(s);
	if(regexp.test(s)) {
		var bits = s.split('/');
		var y = bits[2], m  = bits[0], d = bits[1];
		// Assume not leap year by default (note zero index for Jan)
		var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
	
		// If evenly divisible by 4 and not evenly divisible by 100,
		// or is evenly divisible by 400, then a leap year
		if ( (!(y % 4) && y % 100) || !(y % 400)) {
		  daysInMonth[1] = 29;
		}
		var mTemp = m-1;
		return parseInt(y) > 0 && parseInt(m, 10) > 0 && parseInt(m, 10) < 13 && parseInt(d, 10) > 0 && d <= daysInMonth[mTemp];
	}else {
		return false;
	}
}

/**
 * Get current day.
 * @author thanghoang
 */
function getCurrentDate() {
	var today = new Date();
	var tdate = today.getDate();
	var dd = (tdate<10 ? '0' : '') + tdate;
	var month = today.getMonth()+1;
	var mm = (month<10 ? '0' : '') + month;
	var yyyy = today.getFullYear();
	return  mm+'/'+dd+'/'+yyyy;
};

/**
 * compare 2 date string.(return 0 if date 1 >= date 2, return 1 if date2 > date1, otherwise return -1)
 * @author thanghoang
 */
function compareDate(date1, date2) {
	var result = 0;
	var dateOne = new Date(date1.split('/')[2], date1.split('/')[0], date1.split('/')[1]);
	var dateTwo = new Date(date2.split('/')[2], date2.split('/')[0], date2.split('/')[1]);
	if(dateOne < dateTwo){
		result = 1;
	}
	return result;
}

/**
 * validate date input.
 * @author giaule
 */
function validateDateInput(inputId, inputName, isRequired) {
	var isValid = true;
	var dateTxt = $.trim($('#'+inputId).val());
	if(isRequired && (dateTxt === null || dateTxt === Constants.EMPTY_STRING)) {
		$('#' + inputId).parent().addClass("has-error");
		tooltipForValidation(inputId, inputName + " is required");
		isValid = false;
	}else if(!(dateTxt === null || dateTxt === Constants.EMPTY_STRING) && !isDate(dateTxt)){
		$('#' + inputId).parent().addClass("has-error");
		tooltipForValidation(inputId, inputName + " is invalid");
		isValid = false;
	}else if(isDate(dateTxt) && compareDate(getCurrentDate(), dateTxt) === 1) {
		$('#' + inputId).parent().addClass("has-error");
		// tooltipForValidation(inputId, inputName + " should be greater than current date");
		tooltipForValidation(inputId, "Future date is not allowed");
		isValid = false;
	}else {
		$('#' + inputId).parent().removeClass("has-error");
		clearTooltipForValidation(inputId);
	}
	return isValid;
}

/**
 * display error message by tooltip.
 * @param inputId id of element
 * @param message error 
 * @param placement the placement of message top|right|bottom|left, optional param.
 * @author giaule
 */
function tooltipForValidation(selectorInput, message, placement){
	$(selectorInput).closest('td').addClass("has-error");
	placement = (typeof placement === "undefined") ? "top" : placement;
	$(selectorInput).attr("data-toggle", "tooltip");
	$(selectorInput).attr("data-original-title", message);
	$(selectorInput).tooltip({'placement':placement, 'trigger':'hover manual'});
	$(selectorInput).tooltip('show');

//	idTooltip = $(selectorInput).parent().find('.tooltip').attr('id');
//	$(selectorInput).show().delay(Constants.TIME_OUT_TT).queue(function (next) {
//	    $(this).fadeOut(1);
//	    next();
//	});
}

/**
 * clear error message by tooltip.
 * @param inputId id of element
 * @author giaule
 */
function clearTooltipForValidation(selectorInput){
	$(selectorInput).closest('td').removeClass("has-error");
	$(selectorInput).tooltip('hide');
	$(selectorInput).removeAttr("data-toggle");
	$(selectorInput).removeAttr("data-original-title");
}

/**
 * validate to compare 2 dates.
 * @author giaule
 */
function validateTwoDateCompare(beginDateId, endDateId){
	var isValid = true;
	beginDate = $.trim($('#'+beginDateId).val());
	endDate = $.trim($('#'+endDateId).val());
	/*beginDateValid = validateDateInput(beginDateId, "Begin date", false);
	endDateValid = validateDateInput(endDateId, "End date", false);*/
	if(!(beginDate === null || beginDate === Constants.EMPTY_STRING) &&
		!(endDate === null || endDate === Constants.EMPTY_STRING)){
		/*if(compareDate(endDate, beginDate) === 1){
			$('#' + beginDateId).parent().addClass("has-error");
			$('#' + endDateId).parent().addClass("has-error");
			tooltipForValidation(beginDateId, "Start date should be earlier than end date");
			tooltipForValidation(endDateId, "End date should be greater than start date");
			isValid = false;
		}else{ */

		clearTooltipForValidation(beginDateId);
		clearTooltipForValidation(endDateId);
	}
	
	return isValid;
}


/**
 * Check decimal number.
 * @author thanghoang
 */
function isDecimalNumber(value, totalNumber, afterDigits) {
	var isValid = false;
	var patt = /^\d*\.?\d*$/;
	var numberStr = parseFloat($.trim(value)).toString();
	if(patt.test($.trim(value))) {
		if((numberStr.split('.').length === 1) || (((numberStr.split('.')[0].length + numberStr.split('.')[1].length) <= totalNumber) && numberStr.split('.')[1].length <= afterDigits)) {
			isValid = true;
		}
	}
	return isValid;
}

/**
 * validate decimal number.
 * @author giaule
 */
function validateDecimal(element, totalNumber, afterDigits, isRequired) {
	var isValid = true;
	var valueTxt = $.trim($(element).val());
	if(isRequired && (valueTxt === null || valueTxt === Constants.EMPTY_STRING)) {
		$(element).parent().addClass("has-error");
		isValid = false;
	}else if(!(valueTxt === null || valueTxt === Constants.EMPTY_STRING) && !isDecimalNumber(valueTxt, totalNumber, afterDigits)) {
		$(element).parent().addClass("has-error");
		isValid = false;
	}else {
		$(element).parent().removeClass("has-error");
	}
	return isValid;
}

/**
 * validate integer number.
 * @author giaule
 */
function validateInteger(element, maxDigit, isRequired) {
	var intRegex = /^\d+$/;
	var isValid = true;
	var valueTxt = $.trim($(element).val());
	if(isRequired && (valueTxt === null || valueTxt === Constants.EMPTY_STRING)) {
		$(element).parent().addClass("has-error");
		isValid = false;
	}else if(!(valueTxt === null || valueTxt === Constants.EMPTY_STRING) && !intRegex.test(valueTxt) || valueTxt.length > maxDigit) {
		$(element).parent().addClass("has-error");
		isValid = false;
	}else {
		$(element).parent().removeClass("has-error");
	}
	return isValid;
}

/**
 * saveCtrlKeyDownStatus.
 * @author thanghoang
 */
function ctrlKeyDownStatusSettup() {
	$(document).keydown(function(e){
		if (e.keyCode === ctrlKey) ctrlDown = true;
	}).keyup(function(e){
		if (e.keyCode === ctrlKey) ctrlDown = false;
	});
}

/**
 * acceptDecimalNumber.
 * @author thanghoang
 */
function acceptDecimalNumber(element, keyPressEvent, totalDigitsLimit, afterDigitsLimit){
	totalDigitsLimit = typeof totalDigitsLimit !== 'undefined' ? totalDigitsLimit : Constants.MAX_DIGIT;
	afterDigitsLimit = typeof afterDigitsLimit !== 'undefined' ? afterDigitsLimit : Constants.AFTER_DIGIT;
	// Control key down, just allow decimal number(totalDigitsLimit, afterDigitsLimit).
	var valueInput = $(element).val();
	var charCode = (keyPressEvent.which) ? keyPressEvent.which : keyPressEvent.keyCode;
	var isDotKey = charCode === 46;
	var isMainKey = $.inArray(charCode, [8, 9, 13, 35, 36, 37, 38, 39, 40]) !== -1; // enter, backspace, tab
	var isNumberKey = (charCode > 47 && charCode < 58);
	var isCtrl  = false;
	var chr 		 = String.fromCharCode(keyPressEvent.which);
	var isSpecial	 = ("~!@#$%^&*()_=+/?<>,[]{}\|".indexOf(chr) >= 0) ? true : false;
	if(keyPressEvent.ctrlKey) isCtrl = true;
	if(isCtrl && ( $.inArray(charCode, [80, 97, 99, 118]) !== -1)){
	      	return true;
	}
	if((!isDotKey && !isNumberKey && !isMainKey) || isSpecial) {
		keyPressEvent.preventDefault();
		return false;
	}
	var selectionStart = getInputSelection(element).start;
	var selectionEnd = getInputSelection(element).end;
	if(isNumberKey || isDotKey) {
		var beforePoint = 0;
		var afterPoint = 0;
		if(valueInput.split('.')[0] != null) {
			beforePoint = valueInput.split('.')[0].length;
		}
		if(valueInput.split('.')[1] != null) {
			afterPoint = valueInput.split('.')[1].length;
		}
		var totalDigits = beforePoint + afterPoint;
		if(isNumberKey && (valueInput.lastIndexOf('.') < 0) && (beforePoint >= (totalDigitsLimit - afterDigitsLimit)) && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isNumberKey && (valueInput.lastIndexOf('.') >= 0) && (beforePoint >= (totalDigitsLimit - afterDigitsLimit)) && (selectionStart === selectionEnd) && (selectionStart < valueInput.lastIndexOf('.'))) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(totalDigits >= totalDigitsLimit && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isNumberKey && (valueInput.lastIndexOf('.') < 0) && (valueInput.length >= totalDigitsLimit) && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isDotKey && (valueInput.lastIndexOf('.') >= 0) && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if(isDotKey && (valueInput.lastIndexOf('.') < 0) && (valueInput.length - selectionEnd > afterDigitsLimit)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if((valueInput.lastIndexOf('.') >= 0) && (selectionStart === selectionEnd) && (selectionEnd - valueInput.lastIndexOf('.') > afterDigitsLimit)) {
			keyPressEvent.preventDefault();
			return false;
		}
		if((valueInput.lastIndexOf('.') >= 0) && (valueInput.lastIndexOf('.') < selectionEnd) && (afterPoint >= afterDigitsLimit) && (selectionStart === selectionEnd)) {
			keyPressEvent.preventDefault();
			return false;
		}
	}
}

/**
 * acceptNumber.
 * @author thanghoang
 */
function acceptNumber(element, keyPressEvent, maxDigit){
	// Control key down, just allow number(max digit = maxDigit).
	var valueInput = $(element).val();
	var charCode = (keyPressEvent.which) ? keyPressEvent.which : keyPressEvent.keyCode;
	var isMainKey = $.inArray(charCode, [8, 9, 13, 46, 37, 38, 39, 40]) !== -1; // enter, backspace
	var isNumberKey = (charCode > 47 && charCode < 58);
	if(!isNumberKey && !isMainKey) {
		keyPressEvent.preventDefault();
		return false;
	}
	if(isNumberKey) {
		if(valueInput.length >= maxDigit && getInputSelection(element).end === getInputSelection(element).start) {
			keyPressEvent.preventDefault();
			return false;
		}
	}
}

/**
 * acceptAlphanumeric.
 * @author ANNGUYEN
 */
function acceptAlphanumeric(e, max_len, value) {
	if (typeof max_len === 'undefined') {
        max_len = Constants.INV_MAX_LEN;
    }
    value = typeof value !== 'undefined' ? value : $(this).val();

	if (!(!event.shiftKey //Disallow: any Shift+digit combination
        && !(event.keyCode < 48 || event.keyCode > 57) //Disallow: everything but digits
        || !(event.keyCode < 65 || event.keyCode > 90) //Allow: numeric pad digits
        || !(event.keyCode < 96 || event.keyCode > 105) //Allow: numeric pad digits
        || event.keyCode == 46 // Allow: delete
        || event.keyCode == 8  // Allow: backspace
        || event.keyCode == 9  // Allow: tab
        || event.keyCode == 27 // Allow: escape
        || (event.keyCode == 65 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+A
        || (event.keyCode == 67 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+C
        //Uncommenting the next line allows Ctrl+V usage, but requires additional code from you to disallow pasting non-numeric symbols
        || (event.keyCode == 86 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+Vpasting 
        || (event.keyCode == 88 && (event.ctrlKey === true || event.metaKey === true)) // Allow: Ctrl+X
        || (event.keyCode >= 35 && event.keyCode <= 39) // Allow: Home, End
        )) {
        (event.preventDefault) ? event.preventDefault() : event.returnValue = false;
    }
    /* else if (value && value.length >= max_len) {
    	(event.preventDefault) ? event.preventDefault() : event.returnValue = false;
    }*/
}

/**
 * add Alphanumeric Event to object.
 * @author ANNGUYEN
 */
function addAlphanumericEvent(objQuery, max_len) {
	var obj = $(objQuery);
	// not direct callback so we have to show obj.val to acceptAlphanumeric function.
	obj.on('keydown', function(e) { acceptAlphanumeric(e, max_len, obj.val()); });
	obj.on('input change', getAlphanumericValue);
	// IE fix on input event
	obj.each(function(){
		if (this.attachEvent) { // Internet Explorer and Opera
			this.attachEvent ("onpropertychange", function OnPropChanged (e) {
	            if (e && e.propertyName && e.propertyName.toLowerCase() == "value") {
	            	getAlphanumericValue(e);
	            }
	        });
	    }
	});
}

/**
 * check is Alphanumeric.
 * @author ANNGUYEN
 */
function isAlphanumeric(val) {
	var pattern = /[a-zA-Z0-9]+$/;
    return pattern.test(val);
}

/**
 * get Alphanumeric Value.
 * @author ANNGUYEN
 */
function getAlphanumericValue(e) {
	var pattern = /[\W_]+/g;
	var value = $(this).val();
	value = value.replace(pattern, '');
    return $(this).val(value);
}


/**
 * getInputSelection.
 * @author thanghoang
 */
function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;
    if (typeof el.selectionStart === "number" && typeof el.selectionEnd === "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() === el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start,
        end: end
    };
}

/**
 * From date and To date.
 * @author giaule
 */
function forceDateFromAndTo(beginDateId, endDateId){
	var FromEndDate = new Date();
	var ToEndDate = new Date();

	ToEndDate.setDate(ToEndDate.getDate()+365);

	$(beginDateId).datepicker({
		forceParse: false,
	    weekStart: 1,
	    "setDate": new Date(),
	    endDate: '0d',
	    autoclose: true
	}).on('changeDate', function(selected){
		if($(this).val() !== ""){
			if (selected.date != null && selected.date != undefined && isDate(selected.date)) {
	        	startDate = new Date(selected.date.valueOf());
		        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
		        $(endDateId).datepicker('setStartDate', startDate);
	        }else{
	        	$(endDateId).datepicker('setStartDate', "");
	        };
		}
        
    }).change(function(){
    	val = $(this).val();
    	startDate = new Date($(this).val());
        if(!(val === null || val === Constants.EMPTY_STRING) && isDate(val)){
        	startDate.setDate(startDate.getDate(new Date($(this).val())));
        	$(endDateId).datepicker('setStartDate', startDate);
        }else{
        	$(endDateId).datepicker('setStartDate', "");
        }
    }).on('keyup', function(e) {
    	if($(this).val() === ""){
			$(this).datepicker('setDates', getCurrentDate());
			$(this).datepicker('clearDates');
    	}
    }); 


	$(endDateId).datepicker({
		forceParse: false,
        weekStart: 1,
        "setDate": new Date(),
        endDate: '0d',
        autoclose: true
	}).on('changeDate', function(selected){
		if (selected.date != null && selected.date != undefined && isDate(selected.date)) {
			FromEndDate = new Date($(this).val());
	        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
	        $(beginDateId).datepicker('setEndDate', FromEndDate);
		}else{
			$(beginDateId).datepicker('setEndDate', "");
		}
	}).change(function(selected){
		endDateVal = $(this).val();
    	FromEndDate = new Date(endDateVal);
    	if(!(endDateVal === null || endDateVal === Constants.EMPTY_STRING) && isDate(endDateVal)){
    		FromEndDate.setDate(FromEndDate.getDate(new Date($(this).val())));
        	$(beginDateId).datepicker('setEndDate', FromEndDate);
    	}else{
			$(beginDateId).datepicker('setEndDate', "");
		}
    }).on('keyup', function(e) {
    	if($(this).val() === ""){
			$(this).datepicker('setDates', getCurrentDate());
			$(this).datepicker('clearDates');
    	}
    });
}

/**
 * Convert to currency.
 * @author thanghoang
 */
function convertToCurrencyString(numberInput) {
	var number = Constants.EMPTY_STRING;
	if(numberInput !== null && numberInput !== undefined) {
		number = numberInput.toString();
	}
//    var number = numberInput.toString(), 
    dollars = number.split('.')[0], 
    cents = (number.split('.')[1] || '') +'00';
    dollars = dollars.split('').reverse().join('')
        .replace(/(\d{3}(?!$))/g, '$1,')
        .split('').reverse().join('');
    return dollars + '.' + cents.slice(0, 2);
}

/**
 * Accept alpha and number.
 */
function acceptAlphaNumberHasDashCharacter(element, keyPressEvent, maxDigit) {
	// Control key down, just allow alpha number(max digit = maxDigit).
	var valueInput = $(element).val();
	var isIEBrowser = (keyPressEvent.which === keyPressEvent.keyCode);
	var charCodeIE = keyPressEvent.keyCode;
	var charCodeFF = keyPressEvent.which;
	var isMainKey = true;
	var isNumberKey = true;
	var isAlphaKey = true;
	if(isIEBrowser) {
		isMainKey = $.inArray(charCodeIE, [8, 9, 13, 32, 45, 95]) !== -1; // enter, backspace
		isNumberKey = (charCodeIE > 47 && charCodeIE < 58);
		isAlphaKey = (charCodeIE > 64 && charCodeIE < 91) || (charCodeIE > 96 && charCodeIE < 123);
	}else {
		isMainKey = $.inArray(charCodeFF, [0, 8, 9, 32, 13, 45, 95]) !== -1;
		isNumberKey = (charCodeFF > 47 && charCodeFF < 58);
		isAlphaKey = (charCodeFF > 64 && charCodeFF < 91) || (charCodeFF > 96 && charCodeFF < 123);
	}
	if(!isAlphaKey && !isNumberKey && !isMainKey) {
		keyPressEvent.preventDefault();
		return false;
	}
	if(maxDigit !== undefined || maxDigit !== null && (isNumberKey || isAlphaKey)) {
		if(valueInput.length >= maxDigit && getInputSelection(element).end === getInputSelection(element).start) {
			keyPressEvent.preventDefault();
			return false;
		}
	}
}
/**
 * 
 * @param element
 */
function filterAlphaNumberHasDashCharacter(element) {
	$(element).val($(element).val().replace(/[^a-zA-Z0-9\-_ ]+/g, ''));
}
/**
 * @author : DuyenLe
 * @param element
 */
function acceptMaginNumber(element, e, totalDigitsLimit, afterDigitsLimit){		
	totalDigitsLimit = typeof totalDigitsLimit !== 'undefined' ? totalDigitsLimit : Constants.MAX_DIGIT;
	afterDigitsLimit = typeof afterDigitsLimit !== 'undefined' ? afterDigitsLimit : Constants.AFTER_DIGIT;
	// Control key down, just allow decimal number(totalDigitsLimit, afterDigitsLimit).
	var valueInput = $(element).val();
	var charCode = (e.which) ? e.which : e.keyCode;
	var isDotKey = charCode === 46;
	var nagativeKey = charCode === 45;	
	var isMainKey = $.inArray(charCode, [8, 9, 13, 35, 36, 37, 38, 39, 40]) !== -1; // enter, backspace, tab
	var isNumberKey = (charCode > 47 && charCode < 58);
	var isCtrl  = false;
	var chr 		 = String.fromCharCode(e.which);
	var isSpecial	 = ("~!@#$%^&*()_=+/?<>,[]{}\|".indexOf(chr) >= 0) ? true : false;
	if(e.ctrlKey) isCtrl = true;
	if(isCtrl && ( $.inArray(charCode, [80, 97, 99, 118]) !== -1)){
	      	return true;
	}
	if((!isDotKey && !isNumberKey && !isMainKey && !nagativeKey) || isSpecial ) {
		e.preventDefault();
		return false;
	}
	var selectionStart = getInputSelection(element).start;
	var selectionEnd = getInputSelection(element).end;
	if(isNumberKey || isDotKey || nagativeKey) {
		var beforePoint = 0;
		var afterPoint = 0;
		if(valueInput.split('.')[0] != null) {
			beforePoint = valueInput.split('.')[0].length;
		}
		if(valueInput.split('.')[1] != null) {
			afterPoint = valueInput.split('.')[1].length;
		}
		var totalDigits = beforePoint + afterPoint;
		if(isNumberKey && (valueInput.lastIndexOf('.') < 0) && (beforePoint >= (totalDigitsLimit - afterDigitsLimit)) && (selectionStart === selectionEnd)) {
			e.preventDefault();
			return false;
		}
		if(isNumberKey && (valueInput.lastIndexOf('.') >= 0) && (beforePoint >= (totalDigitsLimit - afterDigitsLimit)) && (selectionStart === selectionEnd) && (selectionStart < valueInput.lastIndexOf('.'))) {
			e.preventDefault();
			return false;
		}
		if(totalDigits >= totalDigitsLimit && (selectionStart === selectionEnd)) {
			e.preventDefault();
			return false;
		}
		if(isNumberKey && (valueInput.lastIndexOf('.') < 0) && (valueInput.length >= totalDigitsLimit) && (selectionStart === selectionEnd)) {
			e.preventDefault();
			return false;
		}
		if(isDotKey && (valueInput.lastIndexOf('.') >= 0) && (selectionStart === selectionEnd)) {
			e.preventDefault();
			return false;
		}
		if(isDotKey && (valueInput.lastIndexOf('.') < 0) && (valueInput.length - selectionEnd > afterDigitsLimit)) {
			e.preventDefault();
			return false;
		}
		//// Duyen add
		
		if(nagativeKey && (valueInput.lastIndexOf('-') >= 0) && (selectionStart === selectionEnd)) {
			e.preventDefault();
			return false;
		}		
		if(nagativeKey && (valueInput.lastIndexOf('-') < 0) && (selectionStart > 0)) {
			e.preventDefault();
			return false;
		}
		// end
		if((valueInput.lastIndexOf('.') >= 0) && (selectionStart === selectionEnd) && (selectionEnd - valueInput.lastIndexOf('.') > afterDigitsLimit)) {
			e.preventDefault();
			return false;
		}
		if((valueInput.lastIndexOf('.') >= 0) && (valueInput.lastIndexOf('.') < selectionEnd) && (afterPoint >= afterDigitsLimit) && (selectionStart === selectionEnd)) {
			e.preventDefault();
			return false;
		}
	}
}
/**
 * @author : DuyenLe
	@purpose	format Float number
 * @param element
 */
function precise_roundFloatNum(numberInput) {
	var dollar = '';
	var number = numberInput.toString();
	var num;
	var numArr;
	if(validFormatMagin(numberInput)){
		if(number.length >0){
			if(number.indexOf(".")>=0){
				numArr = number.split(".");				
				decimals = numArr[1].toString();
				
				if(numArr[0]=== ''){
					numArr[0] = 0;
				}
				if(numArr[0] ==='-'){
					numArr[0] = "-"+0;
				}
				if(decimals.length > 2) {					
					if(number.indexOf('-')===0){
						if(parseFloat(numberInput) === 0){
							num = dollar+0.00;
						} else {
							if(parseInt(numArr[0]) > 0 || parseInt(numArr[0]) <0){
								num = dollar+parseInt(numArr[0])+"."+decimals.substr(0,2);	
							} else {
								num = dollar+"-"+parseInt(numArr[0])+"."+decimals.substr(0,2);;	
							}
						}
					} else {
						num = dollar+parseInt(numArr[0])+"."+decimals.substr(0,2);
					}		
				} else if(decimals.length === 2) {
					if(number.indexOf('-')===0){
						if(parseFloat(numberInput) === 0){
							num = dollar+0.00;
						} else {
							if(parseInt(numArr[0]) > 0 || parseInt(numArr[0]) <0){
								num = dollar+parseInt(numArr[0])+"."+numArr[1];	
							} else {
								num = dollar+"-"+parseInt(numArr[0])+"."+numArr[1];	
							}
						}
					} else {
						num = dollar+parseInt(numArr[0])+"."+numArr[1];	
					}			
				} else {
					decimals = decimals + "0000";
					if(number.indexOf('-')===0){
						if(parseFloat(numberInput) === 0){
							num = dollar+0.00;
						} else {
							if(parseInt(numArr[0]) > 0 || parseInt(numArr[0]) <0){
								num = dollar+parseInt(numArr[0])+"."+decimals.substr(0,2);	
							} else {
								num = dollar+"-"+parseInt(numArr[0])+"."+decimals.substr(0,2);;	
							}
						}
					} else {
						num = dollar+parseInt(numArr[0])+"."+decimals.substr(0,2);
					}
				}		
			} else { 
				num = dollar+parseInt(number)+".00";
			}
		}
	} else {
		num = number;
	}
	return num;
}
/**
 * @author : DuyenLe
	@purpose	test format Float number
 * @param element
 */
function validFormatMagin(numberInput) {
	var isValid;
	var number = numberInput.toString();
	if(number.length > 0){
		if(number.indexOf(".")>=0){
			var numArr = number.split(".");				
			var decimals = numArr[1];

			if(decimals.length > 2) {
				number = numArr[0]+"."+decimals.substr(0,2);			
			} 
		}
	}
	var regularExpression1 = /^(\-)?([0-9]+)?\.([0-9][0-9])?$/;
	var regularExpression2 = /^(\-)?([0-9]+)?\.([0-9])?$/;
	var intergerExpression = /^(\-)?\d+$/;
	//console.log(regularExpression1.test(number),regularExpression2.test(number), intergerExpression.test(number));
	if(regularExpression1.test(number)||regularExpression2.test(number)|| intergerExpression.test(number)){
		isValid = true;
	} else {
		isValid = false;
	}
	return isValid;
}