var Constants = {
		EMPTY_STRING : "",
		SPACE_STRING : " ",
		AFTER_DIGIT: 2,
		TOTAL_NUMBER: 9, 
		TOTAL_NUMBER_INTEGER: 38, 
		INVALID_VALUE : "Invalid value",
		AJAX_COMMONDITIES : "/ajax/get-data-autocomplete-commodities",
		AJAX_VENDOR : "/ajax/get-data-autocomplete-vendor",
		msgLoseChange :"You will lose your changes, do you wish to continue?",
		msgHebRetailLimit : "Only the value between 0 and 100000 is accepted"
};
var searchInquiryValue = [];
$(document).ready(function() {
	/*INIT COMMODITIES AUTOCOMPLETE*/
	initDataForAutocomplete("commodity-input", Constants.AJAX_COMMONDITIES);
	/*INIT VENDOR  COMBOBOX*/
	initDataForCombobox("vendor-input", Constants.AJAX_VENDOR);

	setValueFormSearch();
	initDataTable();

	$("#search-retail-inquiry-btn").click(function() {
		setValueFormSearch();
		if (!checkFilterEmpty()) {
			$(".second-tr-retailInquiry").find('th:not(:first)').each(function (){
				$(this).find("input").val("");
				if($(this).hasClass('has-error')) {
					clearTooltipForValidationBDM($(this).find("input"));
				}
			});
		}
		$('#inquiry_detail').DataTable().draw();
	});
	 $("#reset-retail-inquiry-btn").click(function() { 
		$("#commodity-input").attr("commodity-inputid", "");
	    $("#commodity-input:text").val("");
		$("#product-input:text").val("");
		$("#vendor-input").val(Constants.EMPTY_STRING);	
    });
	$("#clear-filtter-btn-retailInquiry").click(function() {
		if (!checkFilterEmpty()) {
			resetFilter();
		}
	});

	$("#filter .input-sm").keypress(function( event ) {
		var code = event.keyCode || event.which;
		if(code == 13) {
			 $("#search-retail-inquiry-btn").trigger('click');
		}
	
	});
	
	$("#search-image-btn").change(function() {
	    $('#inquiry_detail').DataTable().draw();
	});

});

// function
// initiate dataTable
function initDataTable(){
	var table = $('#inquiry_detail').DataTable( {
		"autoWidth": false,
		"columnDefs": [ 
		{
			"searchable": false,
			"orderable": false,
			"targets": 0
		},
	    {
	    	"render": function ( data, type, row ) {
	           	return '<span class="currency-format">'+ data +'</span>';
	        },
	      	"targets": [4, 5, 6, 8]

	    },
	  	{
				"render": function ( data, type, row ) {
	            	return '<span class="show-competitor cursor-pointer" data-html="true" data-container="body" data-toggle="popover" data-placement="left" data-content="<div><img  width = 150px height =150px src='+data+'></div>"><img  width = "25px" height ="25px" src="'+data+'"></span>';
	            },
	             "width": "10%",
				"targets": 9
		}
		],
 		"processing": false,
	  	"serverSide": true,
	  	"orderMulti": false,
		"aLengthMenu": [
		    [10, 25, 50, 100, 200, 500],
		    [10, 25, 50, 100, 200, 500]
		],
    	"sDom": '<"wp-table-1" <"wp-table-2" <"wp-table-3"t>r> ipl>',
    	"pagingType": "full_numbers",
    	"stateSave": false,
	    "ajax": {
	        "url": appName + '/retail-inquiry/search',
	        "type": 'POST',
	        "data": function ( d ) {
	        	d.commodittyId = searchInquiryValue['0'];
			    d.vendorId = searchInquiryValue['1'];
			    d.prodDes = searchInquiryValue['2'];

			    /*---------------------------------*/
	            d.upcIdFilter = $("#search-upc-btn").val();
	            d.prodDescFilter =  $.trim($("#search-description-btn").val());
	            d.prodSizeFilter = $("#search-size-btn").val();
	            d.msrpFilter = $("#search-msrp-btn").val();
	            d.hebRetailFilter = $("#search-retail-btn").val();
	            d.costFilter = $("#search-cost-btn").val();
	            d.perGpFilter = $("#search-gp-btn").val();
	            d.pennyProfitFilter = $("#search-penny-btn").val();
	        }
	    },

	    "columns": [
				{ "data": "rowNumber" },
				{ "data": "upc"},
				{ "data": "description" },
				{ "data": "size" },
				{ "data": "msrp" },
				{ "data": "hebRetail" },
				{ "data": "cost" },
				{ "data": "gppercent" },
				{ "data": "pennyProfit" },
				{ "data": "image" }
    
	     ],
	    "oLanguage" : {
			"sLengthMenu" : "Show _MENU_ rows",
			"sInfoEmpty" : "No records to show",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ rows"
		  },

      "fnDrawCallback": function( oSettings ) {
          	formatCurrency();
          	var numberRow = $('#inquiry_detail').DataTable().page.info().recordsTotal;
          	if(isNaN(numberRow) || parseInt(numberRow) == 0){
       			 	$("#retail-inquiry-export-btn").addClass('disable-class');
            } else {
       			 	$("#retail-inquiry-export-btn").removeClass('disable-class');
       	    }
       },
      "rowCallback": function( row, data ) {
           /* add class for cell */
           $('td:eq(0)', row).addClass('text-right');
           $('td:eq(1)', row).addClass('text-left');
           $('td:eq(2)', row).addClass('text-left text-ellipsis');
           $('td:eq(3)', row).addClass('text-right');
           $('td:eq(4)', row).addClass('text-right');
           $('td:eq(5)', row).addClass('text-right');
           $('td:eq(6)', row).addClass('text-right');
           $('td:eq(7)', row).addClass('text-right currency-format');
           $('td:eq(8)', row).addClass('text-right');
           $('td:eq(9)', row).addClass('text-center');
           $('td:eq(2)', row).attr( "title", data.prodDesc);
       }
	});
  	table.on( 'order.dt search.dt draw.dt init.dt', function () {
	    table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	    	cell.innerHTML = i + 1 + table.page.info().start;
		} );
	} );
}

//format currency
/*function formatCurrency () {
  $(".currency-format").each(function (index, value) {
    $(value).text($.number($(value).text(), 2));
  })
}*/

//reset filters in dataTable
function resetFilter() {
	$(".second-tr-retailInquiry").find('th:not(:first)').each(function (){
		$(this).find("input").val("");
		if($(this).hasClass('has-error')) {
			clearTooltipForValidationBDM($(this).find("input"));
		}
	});
	$('#inquiry_detail').DataTable().draw();	
}

//check Filter empty or not
function checkFilterEmpty () {
	var val = true;
	$(".second-tr-retailInquiry").find('th:not(:first)').each(function (){
		if ($(this).find('input').val() != "") {
			val = false;
			return;
		}
	});
	return val;
}

//filter 
$("#inquiry_detail" + ' .second-tr > th > input:text').on( 'change', function () {
	if(checkValidateFilter()) {
		$('#inquiry_detail').DataTable().columns($("#inquiry_detail" + ' .second-tr > th > input:text')).search($("#inquiry_detail" + ' .second-tr > th > input:text').val()).draw();
//		if (timer.isActive) {
//			timer.stop();
//		}
//		timer.play();
	}
} );

//var timer = $.timer(function() {
//	$('#inquiry_detail').DataTable().columns($("#inquiry_detail" + ' .second-tr > th > input:text')).search($("#inquiry_detail" + ' .second-tr > th > input:text').val()).draw();
//	timer.stop();
//});
//timer.set({ time : 1000, autostart : false });
	
//check empty input 
function checkEmptyField(idInput){
	var id = "#" + idInput;
	var val = true;
	if($("#"+ idInput).val().length != 0){
		val = false;
	}
	return val;
}	

//remove attribute when deleting data in textboxes
function delAttributeAutoComplete(){
	$("#filter input:text" ).on( 'keyup', function (event) {
		var attri = $(this).attr("id");
		var attrAutoComp = attri + "id";
		if ( event.which != 13 ) {
		    $( "#"+ attri ).attr( attrAutoComp, "");
		}
		
	});

}

$("#retail-inquiry-export-btn:not(.disable-class)").click(function() {
	$("body").append('<div id="temp"></div>');
	$("#temp").append('<form target="_blank" id="frmExcel" action="/assortment/retail-inquiry/export-to-excel-directly" method="POST"></form>');		
	$("#frmExcel").append('<input type="hidden" name="start" value="0">');
	$("#frmExcel").append('<input type="hidden" name="length" value="RN">');
	$("#frmExcel").append('<input type="hidden" name="commodittyID" value="'+searchInquiryValue[0]+'">');
	$("#frmExcel").append('<input type="hidden" name="vendorID" value="'+searchInquiryValue[1]+'">');
	$("#frmExcel").append('<input type="hidden" name="prodDes" value="'+searchInquiryValue[2]+'">');
	var aaSorting = $('#inquiry_detail').dataTable().fnSettings().aaSorting;
		// sort direction.
	var sSortDir = typeof aaSorting[0][1] !== 'undefined' ? aaSorting[0][1] : 'asc';
		// Sort column.
	var iSortColumn = typeof aaSorting[0][0] !== 'undefined' ? aaSorting[0][0] : '1';

	$("#frmExcel").append('<input type="hidden" name="sSortDir" value="' + sSortDir + '">');
	$("#frmExcel").append('<input type="hidden" name="iSortColumn" value="' + iSortColumn + '">');
	var numberRow = $('#inquiry_detail').DataTable().page.info().recordsTotal;
	if(numberRow != null && !isNaN(numberRow)) {
		if(parseInt(numberRow) == 0) {
			// alert("Search result is empty!!!");
			$("#temp").remove();
		}else {
			$('#frmExcel').submit();
			$("#temp").remove();
		} 
//		if(parseInt(numberRow) < 2000) {
//			$("#frmExcel").removeAttr("target");
//			$('#frmExcel').submit();
//			$("#temp").remove();
//		}else {
//			$("#frmExcel").append('<input type="hidden" name="nameProcess" value="retail-inquiry-export-process">');
//			$('#frmExcel').attr('action','/assortment/download');
//			$('#frmExcel').submit();
//			$("#temp").remove();
//		}
	}
});

function setValueFormSearch () {
    searchInquiryValue = [];
    searchInquiryValue['0'] = $("#commodity-input").attr('commodity-inputid');
	searchInquiryValue['1'] = $("#vendor-input").val();
	searchInquiryValue['2'] =  $.trim($("#product-input").val());
	return searchInquiryValue;

}

function checkValidateFilter(){
	var isValid = true;
	var count = 0;
	_.each($(".input-decimal"), function(value, key) {
	    	var idItem = $(value).attr('id');
	    	validateDecimalBDM("#"+idItem, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
	});	
	_.each($(".input-number"), function(value, key) {
	    	var idItem = $(value).attr('id');
			validateIntegerBDM("#"+idItem,38);
	});	
	_.each($(".negative-number"), function(value, key) {
	    	var idItem = $(value).attr('id');
	    	validateNegativeDecimalBDM("#"+idItem, Constants.TOTAL_NUMBER, Constants.AFTER_DIGIT);
	});	
	_.each($(".input-sm"), function(value, key) {
	    	if ($(value).closest('th').hasClass('has-error')) {
	    		count++;
			}
	});	
	if(count > 0) {
	    	isValid = false;
	} 
	return isValid;
}


