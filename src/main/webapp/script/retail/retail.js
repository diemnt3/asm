$('.tooltip-class').tooltip();
$('.can-change').click( function() {
	event.stopPropagation();
	var old_html = $(this).html();
	if(old_html.indexOf('<input') == -1) {
		$(this).html('<input value="' + old_html + '" name="" />');
	}
});
$(document).click( function(){
	$('.can-change input').each(function() {
		var old_html = $(this).val();
		$(this).parent().html(old_html);
	});
});