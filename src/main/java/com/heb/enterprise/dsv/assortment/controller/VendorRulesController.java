/*
 * $Id: VendorRulesController.java,v 1.14 2015/03/19 04:14:38 vn55228 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.GlobalRulesService;
import com.heb.enterprise.dsv.assortment.service.VendorManagementService;
import com.heb.enterprise.dsv.assortment.service.VendorRulesService;
import com.heb.enterprise.dsv.assortment.utils.ConstantsWeb;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.AttrAndRuleVendorVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;
import com.heb.enterprise.dsv.vo.JSTreeVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;

/**
 * VendorRulesController.
 * @author Duyen.le.
 */
@Controller
public class VendorRulesController {
	private static final Logger LOG = Logger.getLogger(VendorRulesController.class);
	// this name show in address bar
	private static final String VENDOR_PATH = Constants.SLASH + ConstantsWeb.VENDOR_RULE_PAGE;
	private static final String GET_COMMODITY_AND_SUB_PATH = ConstantsWeb.AJAX_GET_COMMODITY_AND_SUB;
	private static final String AJAX_GET_RULES = ConstantsWeb.AJAX_GET_RULESET_FOR_VENDOR;
	private static final String AJAX_GET_ASM_RULES = ConstantsWeb.GET_ASSORTMENT_RULES;
	private static final String UPDATE_ASM_RULES = ConstantsWeb.UPDATE_ASM_RULES;
	private static final String RESET_RULES_PATH = Constants.SLASH + ConstantsWeb.RESET_RULES_VENDOR;
	private static final String ACTIVATE_RULES_PATH = ConstantsWeb.ACTIVATE_RULES_VENDOR;
	private static final String DEACTIVATE_RULES_PATH = ConstantsWeb.DEACTIVATE_RULES_VENDOR;

	// TILE
	private static final String VENDOR_TILE = "vendor.tile";
	// Attr
	private final String classJsTreeChecked = " jstree-checked";
	private final String classMarked = " markMapped";
	private final String classRuleFlag = " rule-flag";

	/**
	 * Inject VendorRulesService.
	 */
	@Autowired
	private VendorRulesService vendorRuleService;

	@Autowired
	private CacheService cacheService;

	/**
	 * Inject GlobalRulesService.
	 */
	@Autowired
	private GlobalRulesService globalService;

	@Autowired
	private VendorManagementService vendorManagementService;
	@Autowired
	private CommonService commonService;

	/**
	 * Display The Vendor Rules screen.
	 * @return String
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@RequestMapping(value = { VENDOR_PATH }, method = RequestMethod.GET)
	public String showVendorRulesScreen() throws DSVException {
		LOG.info("show vendor Rules screen by DuyenLe");
		return VENDOR_TILE;
	}

	/**
	 * get Commodity Rules have existed from Database.
	 * @return String JSON String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { AJAX_GET_ASM_RULES }, method = RequestMethod.POST)
	@ResponseBody
	public String getAsmRules() throws DSVException {
		LOG.info("Get Assortment Rules");
		JSONObject json = new JSONObject();
		return Helper.parseToJsonString(json);
	}

	/**
	 * get Commodity Rules have existed from Database.
	 * @param vendorId
	 *            String
	 * @param commodityId
	 *            String
	 * @param subCommodityId
	 *            String
	 * @return String JSON String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { AJAX_GET_RULES }, method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getRuleSettingsForVendor(@RequestParam("vendorID") String vendorId, @RequestParam(value = "commodityID", required = false) String commodityId,
			@RequestParam(value = "subCommodityID", required = false) String subCommodityId) throws DSVException {
		JSONObject jsonResult = new JSONObject();
		Map<String, AssortmentRulesVO> lstRuleRs = null;
		boolean onlyVend = false;
		LOG.info("vendor ID : " + vendorId + " commodity : " + commodityId + " and sub Commodity : " + subCommodityId);
		if (Helper.isEmpty(commodityId) && Helper.isEmpty(subCommodityId)) {
			onlyVend = true;
			AttrAndRuleVendorVO settings = this.vendorRuleService.getRuleSettingsByVendor(vendorId);
			jsonResult.put("vendorAttrs", settings.getLstVendorAttrs());
			jsonResult.put("vendorRules", settings.getLstVendorRules());
		} else {
			lstRuleRs = this.vendorRuleService.getRuleSettingsForVendor(vendorId, commodityId, subCommodityId);
			List<JSONObject> lstJson = new ArrayList<JSONObject>();
			for (Map.Entry<String, AssortmentRulesVO> entry : lstRuleRs.entrySet()) {
				if (!Helper.isEmpty(entry.getValue())) {
					AssortmentRulesVO asm = entry.getValue();
					JSONObject json = new JSONObject();
					json.put("id", asm.getId());
					json.put("name", asm.getName());
					json.put("values", asm.getValues());
					json.put("seqNbr", asm.getLstSeqNbrs());
					json.put("activeSw", asm.getActiveSw());
					json.put("HebComdId", asm.getHebComdId());
					json.put("subDeptId", asm.getSubDeptId());
					json.put("subComdId", asm.getHebSubComdId());
					json.put("lvlId", asm.getLvlId());
					json.put("lstUpdateUid", asm.getLstUpdateUid());
					json.put("lstUpdateTs", asm.getLstUpdateTs());
					lstJson.add(json);
				}
			}
			jsonResult.put("lstRules", lstJson);
		}
		jsonResult.put("onlyVend", onlyVend);
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * save Commodity Rules to Database.
	 * @param vendorRule
	 *            AttrAndRuleVendorVO
	 * @param request
	 *            HttpServletRequest
	 * @return String JSON String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { UPDATE_ASM_RULES }, method = RequestMethod.POST, headers = { "Content-type=application/json" })
	@ResponseBody
	public String updateRuleSettings(@RequestBody AttrAndRuleVendorVO vendorRule, HttpServletRequest request) throws DSVException {
		LOG.info("Update Assortment Rules");
		JSONObject jsonResult = new JSONObject();
		HebUserDetails user = this.commonService.getUserLogin();
		String userId = Constants.EMPTY_STRING;
		if (!Helper.isEmpty(user)) {
			userId = user.getUsername();
		}
		vendorRule.setUserId(userId);
		boolean rs = this.vendorRuleService.updateRuleSettingsForVendor(vendorRule);
		jsonResult.put("rs", rs);
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Display The Vendor Rules screen.
	 * @param commodityCd
	 *            String
	 * @param vendorId
	 *            :String
	 * @return List<JSONObject>
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_COMMODITY_AND_SUB_PATH }, method = RequestMethod.POST)
	@ResponseBody
	public List<JSONObject> ajaxGetCommodityAndSub(@RequestParam("vendorId") String vendorId, @RequestParam("id") String commodityCd) throws DSVException {
		LOG.info("ajaxGetCommodityAndSub by DuyenLe");
		List<JSONObject> commodityJsonObjList = new ArrayList<JSONObject>();

		List<CommodityVO> finalCom = null;
		List<Integer> asmLvlModified = null;

		List<Integer> lstHebsubComd = this.vendorRuleService.getActiveSw(vendorId);
		Map<String, String> subCommodityMap = this.convertHierarchyMapVOToSubCommodityMap(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId));

		List<CommodityVO> lstComHierachy = this.cacheService.getCommodityDetailList();

		finalCom = this.getlstCommodityFromVendorHierarchyMapping(subCommodityMap, lstComHierachy);
		if (Constants.NUMBER_SIGN.equals(commodityCd)) {
			List<LevelVO> lstActiveSubCom = this.vendorRuleService.getLstSubActivateBySub();
			asmLvlModified = this.vendorRuleService.getAsmLVLModified("comd", vendorId);
			for (CommodityVO commodityVO : finalCom) {
				JSONObject commodityJsonObj = new JSONObject();
				JSONObject aAttrObjCommodity = new JSONObject();
				JSONObject state = new JSONObject();
				String classAttr = Constants.EMPTY_STRING;
				List<SubCommodityVO> lstSub = commodityVO.getSubCommodityVOs();
				List<SubCommodityVO> mappedArr = new ArrayList<SubCommodityVO>();
				for (SubCommodityVO subCommodityVO : lstSub) {
					if (subCommodityVO.getSort() == 1) {
						mappedArr.add(subCommodityVO);
					}
				}
				if (!Helper.isEmpty(lstActiveSubCom)) {
					for (LevelVO lvlVO : lstActiveSubCom) {
						if (lvlVO.getComdId() == commodityVO.getCommodityCd()) {
							if (mappedArr.size() == lvlVO.getNumberOfActiveSubCom()) {
								state.put(JSTreeVO.CHECKED, true);
								state.put(JSTreeVO.SELECTED, false);
							} else if (mappedArr.size() > lvlVO.getNumberOfActiveSubCom()) {
								LOG.info(" ********* add jstree-undetermined ***********");
								state.put(JSTreeVO.UNDETERMINED, true);
							}
						}
					}
				}
				commodityJsonObj.put(JSTreeVO.TEXT, commodityVO.getCommodityCd() + Constants.DASH_WHITE_SPACE + commodityVO.getCommodityDesc());
				commodityJsonObj.put(JSTreeVO.ID, commodityVO.getCommodityCd());
				if (commodityVO.getMarkMapped() == 1) {
					classAttr = classAttr.concat(this.classMarked);
				}
				if (!Helper.isEmpty(asmLvlModified) && asmLvlModified.contains(commodityVO.getCommodityCd())) {
					classAttr = classAttr.concat(this.classRuleFlag);
				}
				if (commodityVO.getSubCommodityVOs().size() > AntiMagicNumber.ZERO) {
					commodityJsonObj.put(JSTreeVO.CHILDREN, true);
				} else {
					commodityJsonObj.put(JSTreeVO.CHILDREN, false);
				}
				aAttrObjCommodity.put(JSTreeVO.CLASS, classAttr);
				commodityJsonObj.put(JSTreeVO.A_ATTR, aAttrObjCommodity);
				commodityJsonObj.put(JSTreeVO.STATE, state);

				commodityJsonObjList.add(commodityJsonObj);
			}
		} else {
			LOG.info("getHEBHierarchyChild with commodityCd: " + commodityCd);
			asmLvlModified = this.vendorRuleService.getAsmLVLModified("sub", vendorId);
			List<SubCommodityVO> subCommodityList = this.cacheService.getSubCommByCommodityId(commodityCd);
			// List<SubCommodityVO> noMappedArr = new ArrayList<SubCommodityVO>();
			List<SubCommodityVO> mappedArr = new ArrayList<SubCommodityVO>();
			for (SubCommodityVO subCommodityVO : subCommodityList) {
				if (subCommodityVO.getSort() == 1) {
					mappedArr.add(subCommodityVO);
				}
				// else {
				// noMappedArr.add(subCommodityVO);
				// }
			}
			Collections.sort(mappedArr, SubCommodityVO.SubCommodityVOComparator);
			// Collections.sort(noMappedArr, SubCommodityVO.SubCommodityVOComparator);
			subCommodityList = new ArrayList<SubCommodityVO>();
			subCommodityList.addAll(mappedArr);
			// subCommodityList.addAll(noMappedArr);
			for (SubCommodityVO subCommodityVO : subCommodityList) {
				String classAttr = Constants.EMPTY_STRING;
				JSONObject subCommodityJsonObj = new JSONObject();
				JSONObject aAttrObjCommodity = new JSONObject();
				JSONObject state = new JSONObject();
				// if (subCommodityVO.getSort() == 1) {
				// classAttr = classAttr.concat(this.classMarked);
				// }
				if (Helper.isNormalList(lstHebsubComd)) {
					if (lstHebsubComd.contains(subCommodityVO.getSubCommodityCd())) {
						state.put(JSTreeVO.CHECKED, true);
						state.put(JSTreeVO.SELECTED, false);
					}
				}
				if (!Helper.isEmpty(asmLvlModified) && asmLvlModified.contains(subCommodityVO.getSubCommodityCd())) {
					classAttr = classAttr.concat(this.classRuleFlag);
				}
				aAttrObjCommodity.put(JSTreeVO.CLASS, classAttr);
				subCommodityJsonObj.put(JSTreeVO.ID, Constants.STRING_ID + subCommodityVO.getSubCommodityCd());
				subCommodityJsonObj.put(JSTreeVO.TEXT, subCommodityVO.getSubCommodityCd() + Constants.DASH_WHITE_SPACE + subCommodityVO.getSubCommodityDesc());
				subCommodityJsonObj.put(JSTreeVO.A_ATTR, aAttrObjCommodity);
				subCommodityJsonObj.put(JSTreeVO.STATE, state);
				commodityJsonObjList.add(subCommodityJsonObj);
			}
		}
		return commodityJsonObjList;
	}

	/**
	 * getCommodityListFromVendorHierarchyMapping.
	 * @param lstComHierachy
	 *            :List<CommodityVO>
	 * @param subCommodityMap
	 *            :Map<String, String>
	 * @return List<CommodityVO>
	 * @author duyen.le.
	 */
	private List<CommodityVO> getlstCommodityFromVendorHierarchyMapping(Map<String, String> subCommodityMap, List<CommodityVO> lstComHierachy) {
		List<CommodityVO> finalCom = new ArrayList<CommodityVO>();
		List<CommodityVO> lstComMapped = new ArrayList<CommodityVO>();
		// List<CommodityVO> lstComNoMapped = new ArrayList<CommodityVO>();
		boolean flagComIsMapped = false;
		boolean flagSubComisMapped = false;
		for (CommodityVO commodityVO : lstComHierachy) {
			List<SubCommodityVO> listSubCom = commodityVO.getSubCommodityVOs();
			for (SubCommodityVO subComVO : listSubCom) {
				for (Map.Entry<String, String> a : subCommodityMap.entrySet()) {
					if (subComVO.getSubCommodityCd() == (Integer.parseInt(a.getKey()))) {
						flagSubComisMapped = true;
						flagComIsMapped = true;
					}
				}
				if (flagSubComisMapped) {
					subComVO.setSort(1);
				} else {
					subComVO.setSort(0);
				}
				flagSubComisMapped = false;
			}

			if (flagComIsMapped) {
				commodityVO.setMarkMapped(1);
				lstComMapped.add(commodityVO);
			} else {
				commodityVO.setMarkMapped(0);
				// lstComNoMapped.add(commodityVO);
			}
			flagComIsMapped = false;
		}
		Collections.sort(lstComMapped, CommodityVO.CommodityVOComparator);
		// Collections.sort(lstComNoMapped, CommodityVO.CommodityVOComparator);
		finalCom.addAll(lstComMapped);
		// finalCom.addAll(lstComNoMapped);
		return finalCom;
	}

	/**
	 * convertHierarchyMapVOToSubCommodityMap.
	 * @param hierarchyMapList
	 *            :List<HierarchyMapVO>
	 * @return Map<String, String>
	 * @author anhtran.
	 */
	private Map<String, String> convertHierarchyMapVOToSubCommodityMap(List<HierarchyMapVO> hierarchyMapList) {
		Map<String, String> mapSubCommodity = new HashMap<String, String>();
		for (HierarchyMapVO hierarchyMapVO : hierarchyMapList) {
			mapSubCommodity.put(hierarchyMapVO.getSubCommodityCd(), hierarchyMapVO.getSubCommodity());
		}
		return mapSubCommodity;
	}

	/**
	 * show Global Rules Screen to manage configuration settings.
	 * @return String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@RequestMapping(value = { RESET_RULES_PATH }, method = { RequestMethod.POST })
	@ResponseBody
	public String resetRulesToHigherLevel(@RequestParam("vendorId") String vendorId, @RequestParam(value = "commodityId", required = false) String commodityId,
			@RequestParam(value = "subCommdId", required = false) String subCommdId) throws DSVException {
		LOG.info("Global: Reset Rules To Higher Level method.");
		boolean rs = this.vendorRuleService.resetRulesToHigherLevel(vendorId, commodityId, subCommdId);
		return String.valueOf(rs);
	}

	/**
	 * Activate Rules.
	 * @param vendorId
	 *            Vendor ID String
	 * @param type
	 *            Type are Commodity or SubCommodity :String
	 * @param value
	 *            Commodity ID or SubCommodity ID:String
	 * @return String
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@RequestMapping(value = { ACTIVATE_RULES_PATH }, method = RequestMethod.POST)
	@ResponseBody
	public String activateRules(@RequestParam("type") String type, @RequestParam("value") String value, @RequestParam("vendorId") String vendorId) throws DSVException {
		LOG.info("Activate Rules by DuyenLe with type: " + type + " value: " + value + " VendorId: " + vendorId);
		HebUserDetails user = this.commonService.getUserLogin();
		String userId = Constants.EMPTY_STRING;

		if (!Helper.isEmpty(user)) {
			userId = user.getUsername();
		}

		//userId = "vn44178";
		Map<String, String> subCommodityMap = this.convertHierarchyMapVOToSubCommodityMap(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId));
		boolean rs = this.vendorRuleService.activateRule(type, value, vendorId, userId, subCommodityMap);
		return String.valueOf(rs);
	}

	/**
	 * DeActivate Rules.
	 * @param vendorId
	 *            Vendor ID String
	 * @param type
	 *            Type are Commodity or SubCommodity :String
	 * @param value
	 *            Commodity ID or SubCommodity ID:String
	 * @return String
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@RequestMapping(value = { DEACTIVATE_RULES_PATH }, method = RequestMethod.POST)
	@ResponseBody
	public String deActivateRules(@RequestParam("type") String type, @RequestParam("value") String value, @RequestParam("vendorId") String vendorId) throws DSVException {
		LOG.info("DeActivate Rules by DuyenLe with type: " + type + " value: " + value + " VendorId: " + vendorId);
		Map<String, String> subCommodityMap = this.convertHierarchyMapVOToSubCommodityMap(this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId));
		boolean rs = this.vendorRuleService.deActivateRule(type, value, vendorId, subCommodityMap);
		return String.valueOf(rs);
	}
}
