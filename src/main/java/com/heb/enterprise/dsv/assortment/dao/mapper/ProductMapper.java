/*
 * $Id.: CommonMapper.java, Oct 28, 2014$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.heb.enterprise.dsv.assortment.utils.AConstants;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.ProductRiewVO;

/**
 * ProductMapper class.
 * @author thangdang
 */
public final class ProductMapper {
	private static final Logger LOG = Logger.getLogger(ProductMapper.class);

	/**
	 * constructor method.
	 */
	private ProductMapper() {

	}

	/**
	 * getProductBdmReview.
	 * @return RowMapper<ProductRiewVO>
	 * @author thangdang
	 */
	public static RowMapper<ProductRiewVO> getProductBdmReview() {
		RowMapper<ProductRiewVO> productRiewVOMapper = new RowMapper<ProductRiewVO>() {
			@Override
			public ProductRiewVO mapRow(ResultSet rs, int arg1) throws SQLException {
				ProductRiewVO productRiewVO = new ProductRiewVO();
				productRiewVO.setUpc(rs.getString(ProductRiewVO.PROD_SCN_CD));
				productRiewVO.setDescription(rs.getString(ProductRiewVO.PROD_DES));
				productRiewVO.setSize(String.valueOf(rs.getDouble(ProductRiewVO.SELL_UNT_SZ)));
				productRiewVO.setMap(String.valueOf(rs.getDouble(ProductRiewVO.MAP_AMT)));
				productRiewVO.setMsrp(String.valueOf(rs.getDouble(ProductRiewVO.MSRP_AMT)));
				productRiewVO.setPrePrice(String.valueOf(rs.getDouble(ProductRiewVO.PRE_PRC_AMT)));
				productRiewVO.setImageUri(rs.getString(ProductRiewVO.IMG_URL_TXT));
				productRiewVO.setCost(String.valueOf(rs.getDouble(ProductRiewVO.HEB_CST_AMT)));
				productRiewVO.setSrp(String.valueOf(rs.getDouble(ProductRiewVO.SRP_AMT)));
				productRiewVO.setReviewStatus(rs.getString(ProductRiewVO.REV_STAT_CD));
				String hebRetail = String.valueOf(rs.getDouble(ProductRiewVO.HEB_RETL_AMT));
				productRiewVO.setHebRetail(hebRetail != null ? hebRetail : AntiMagicNumber.STRING_ZERO);
				productRiewVO.setTotalRow(rs.getInt(ProductRiewVO.TOTALROWS));
				productRiewVO.setGpPercent(String.valueOf(rs.getDouble(ProductRiewVO.PER_GROSS_PROFIT)));
				productRiewVO.setPennyProfit(String.valueOf(rs.getDouble(ProductRiewVO.MARGIN)));
				productRiewVO.setFirstReceivedTS(rs.getString(ProductRiewVO.CRE8_TS));
				productRiewVO.setLastEditedTS(rs.getString(ProductRiewVO.LST_UPDT_TS));
				productRiewVO.setStatus("N/A");
				return productRiewVO;
			}
		};
		return productRiewVOMapper;
	}

	/**
	 * getProductBdmReview.
	 * @return RowMapper<ProductRiewVO>
	 * @author thangdang
	 */
	public static RowMapper<ProductRiewVO> getProductBdmCountReview() {
		RowMapper<ProductRiewVO> productRiewVOMapper = new RowMapper<ProductRiewVO>() {
			@Override
			public ProductRiewVO mapRow(ResultSet rs, int arg1) throws SQLException {
				ProductRiewVO productRiewVO = new ProductRiewVO();
				String revStatCd = rs.getString(ProductRiewVO.REV_STAT_CD);
				if (Helper.isEmpty(rs.getString(ProductRiewVO.REV_STAT_CD))) {
					productRiewVO.setReviewStatus(Constants.NULL_REVIEW_STATUS);
				} else {
					productRiewVO.setReviewStatus(rs.getString(ProductRiewVO.REV_STAT_CD));
				}
				productRiewVO.setTotalRow(rs.getInt(ProductRiewVO.TOTALROWS));
				return productRiewVO;
			}
		};
		return productRiewVOMapper;
	}

	/**
	 * getProductBdmReview.
	 * @return RowMapper<ProductRiewVO>
	 * @author thangdang
	 */
	public static RowMapper<ProductRiewVO> getProductBdmReviewFailReason() {
		RowMapper<ProductRiewVO> productRiewVOMapper = new RowMapper<ProductRiewVO>() {
			@Override
			public ProductRiewVO mapRow(ResultSet rs, int arg1) throws SQLException {
				ProductRiewVO productRiewVO = new ProductRiewVO();
				productRiewVO.setUpc(rs.getString(ProductRiewVO.PROD_SCN_CD));
				productRiewVO.setFailedReason(rs.getString(ProductRiewVO.ERR_TYP_DES));
				productRiewVO.setFailedReasonCode(rs.getString(ProductRiewVO.ERR_TYP_CD));
				return productRiewVO;
			}
		};
		return productRiewVOMapper;
	}

	/**
	 * Retrieves a Failed Reasons set by Rule Group.
	 * @return List<BaseVO> : List of Rules set are retrieved from Database.
	 * @author mrh.diemnguyen
	 */
	public static RowMapper<BaseVO> getAllFailedReasonsMapper() {
		RowMapper<BaseVO> failedReasonsMapper = new RowMapper<BaseVO>() {
			@Override
			public BaseVO mapRow(ResultSet rs, int arg1) throws SQLException {
				BaseVO baseVO = new BaseVO();
				baseVO.setId(rs.getString(AConstants.ERR_TYP_CD));
				baseVO.setName(rs.getString(AConstants.ERR_TYP_DES));
				return baseVO;
			}
		};
		return failedReasonsMapper;
	}
}
