/*
 * $Id: CompetitorServiceWSImpl.java,v 1.5 2013/10/29 13:58:01 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.webservice.CompetitorServiceWS;
import com.heb.enterprise.dsv.vo.CompetitiveRetails;

/**
 * CompetitorServiceWSImpl.
 * @author Duyen.le
 */
@Service
public class CompetitorServiceWSImpl implements CompetitorServiceWS {
	/**
	 * getOnlineCompetitors - get List of Competitors.
	 * @author Duyen.Le
	 * @return List<CompetitiveRetails>
	 */
	@Override
	public List<CompetitiveRetails> getOnlineCompetitors() {
		List<CompetitiveRetails> competitiveRetails = new ArrayList<CompetitiveRetails>();
		return competitiveRetails;
	}

	/**
	 * getCompetitorPricebyUPC - get List of Competitors Price by UPC.
	 * @author Duyen.Le
	 * @return List<CompetitiveRetails>
	 */
	@Override
	public List<CompetitiveRetails> getCompetitorPricebyUPC() {
		List<CompetitiveRetails> competitiveRetails = new ArrayList<CompetitiveRetails>();
		return competitiveRetails;
	}

}
