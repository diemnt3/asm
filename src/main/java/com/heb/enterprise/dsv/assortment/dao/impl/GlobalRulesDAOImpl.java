/*
 * $Id: GlobalRulesDAOImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.heb.enterprise.dsv.assortment.dao.GlobalRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.mapper.RuleMapper;
import com.heb.enterprise.dsv.assortment.utils.DAOHelper;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;

/**
 * GlobalManagementDAOImpl interface.
 * @author duyen.le
 */
@Repository
public class GlobalRulesDAOImpl extends BaseDAOImpl implements GlobalRulesDAO {
	private static final Logger LOG = Logger.getLogger(GlobalRulesDAOImpl.class);
	/**
	 * get Rule for SubDept.
	 */
	@Value("${coms.dsv.assortment.sql.getGlobalRules}")
	private String getRules;
	/**
	 * get Exist Rules.
	 */
	@Value("${coms.dsv.assortment.sql.getExistRules}")
	private String getExistRules;
	/**
	 * insert Rules into RuleDfns.
	 */
	@Value("${coms.dsv.assortment.sql.insertIntoRuleDfns}")
	private String insertRulesToDfns;
	/**
	 * Update Values of Rule into RuleDfns.
	 */
	@Value("${coms.dsv.assortment.sql.updateValuesOfRuleDfns}")
	private String updateRuleValues;

	@Value("${coms.dsv.assortment.sql.insertIntoLVL}")
	private String sqlInsertIntoLVL;

	/**
	 * get Exist Rules for SubDept.
	 */
	@Value("${coms.dsv.asm.globalDefault.getGlobalCommodityOverride}")
	private String getGlobalCommodityOverrideSql;

	/**
	 * removeRuleByLvlId.
	 */
	@Value("${coms.dsv.assortment.sql.removeRuleByLvlId}")
	private String sqlRemoveRuleByLvlId;
	/**
	 * removeLvl.
	 */
	@Value("${coms.dsv.assortment.sql.removeLvl}")
	private String sqlRemoveLvl;
	/**
	 * sqlGetLVLKeyByCommodityGlobal.
	 */
	@Value("${coms.dsv.assortment.sql.getLVLKeyByCommodityGlobal}")
	private String sqlGetLVLKeyByCommodityGlobal;
	/**
	 * sqlGetLVLKeyByCommodityGlobal.
	 */
	@Value("${coms.dsv.assortment.sql.getLVLKeyBySubDeptGlobal}")
	private String sqlGetLVLKeyBySubDeptGlobal;
	/**
	 * getMaxLVLID.
	 */
	@Value("${coms.dsv.asm.sql.getMaxLVLID}")
	private String getMaxLVLID;
	/**
	 * updateLVL.
	 */
	@Value("${coms.dsv.assortment.sql.updateLVLTable}")
	private String updateLVL;

	/**
	 * get Rule set by CommodityID.
	 * @param subDeptID
	 *            String
	 * @param commodityID
	 *            String
	 * @throws DSVException
	 *             DSVException
	 * @return List<RuleConfigVO>
	 * @author duyen.le
	 */
	@Override
	public Map<String, AssortmentRulesVO> getGlobalRuleSet(final String subDeptID, final String commodityID) throws DSVException {
		Map<String, AssortmentRulesVO> rulesRs = new HashMap<String, AssortmentRulesVO>();
		String subDept = Constants.EMPTY_STRING;
		if (!Helper.isEmpty(subDeptID)) {
			subDept = Helper.trim(subDeptID);
			final String deptId = subDept.substring(0, subDept.length() - 1);
			final String subDeptId = subDept.substring(subDept.length() - 1);
			LOG.info("getGlobalRuleSet deptId =" + deptId);
			LOG.info("getGlobalRuleSet subDeptId =" + subDeptId);
			List<AssortmentRulesVO> lstRules = null;
			try {
				lstRules = this.getJdbcTemplate().query(this.getRules, new PreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement arg0) throws SQLException {
						arg0.setString(AntiMagicNumber.ONE, commodityID);
						arg0.setString(AntiMagicNumber.TWO, deptId);
						arg0.setString(AntiMagicNumber.THREE, subDeptId);
						arg0.setString(AntiMagicNumber.FOUR, deptId);
						arg0.setString(AntiMagicNumber.FIVE, subDeptId);
					}
				}, RuleMapper.getGlobalRulesMapper());

			} catch (DataAccessException e) {
				LOG.error(e.getMessage(), e);
				throw new DSVException(e);
			}
			if (Helper.isNormalList(lstRules)) {
				rulesRs = DAOHelper.convertColectionRuleToMap(lstRules);
			}
		}

		return rulesRs;
	}

	@Override
	public String updateRuleValuesToDfns(final List<RuleDefinationVO> lstRulesDfns, final String userId) throws DSVException {
		String returnStr = Constants.SUCCESSFULLY;
		this.getJdbcTemplate().batchUpdate(this.updateRuleValues, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Date date = new Date();
				RuleDefinationVO rule = lstRulesDfns.get(i);
				ps.setString(AntiMagicNumber.ONE, rule.getVal());
				ps.setString(AntiMagicNumber.TWO, rule.getActSW());
				ps.setString(AntiMagicNumber.THREE, rule.getActiveType());
				ps.setString(AntiMagicNumber.FOUR, userId);
				ps.setTimestamp(AntiMagicNumber.FIVE, new Timestamp(date.getTime()));
				ps.setLong(AntiMagicNumber.SIX, rule.getRuleId());
				ps.setLong(AntiMagicNumber.SEVEN, rule.getLvlId());
				ps.setInt(AntiMagicNumber.EIGHT, rule.getSeq_nbr());
			}

			@Override
			public int getBatchSize() {
				return lstRulesDfns.size();
			}
		});
		return returnStr;
	}

	/**
	 * updateLvl.
	 * @param lvlId
	 *            long
	 * @param userId
	 *            String
	 * @return int
	 * @throws DSVException
	 *             DSVException
	 * @author Duyen.le.
	 */
	@Override
	public int updateLvl(final long lvlId, final String userId) throws DSVException {
		// String returnStr = Constants.SUCCESSFULLY;
		LOG.info("Sql Update LVL = " + this.updateLVL);
		int rs = this.getJdbcTemplate().update(this.updateLVL, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				Date date = new Date();
				LOG.info("Update LVL with Date = " + date + " &uid = " + userId + " &lvlID = " + lvlId);
				ps.setString(AntiMagicNumber.ONE, userId);
				ps.setTimestamp(AntiMagicNumber.TWO, new Timestamp(date.getTime()));
				ps.setLong(AntiMagicNumber.THREE, lvlId);
			}
		});
		return rs;
	}

	@Override
	public String insertRulesToDfns(final List<RuleDefinationVO> lstRulesDfns, final long lvlId, final String userId) throws DSVException {
		String returnStr = Constants.SUCCESSFULLY;
		this.getJdbcTemplate().batchUpdate(this.insertRulesToDfns, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				RuleDefinationVO rule = lstRulesDfns.get(i);
				Date date = new Date();
				ps.setLong(AntiMagicNumber.ONE, rule.getRuleId());
				ps.setInt(AntiMagicNumber.TWO, rule.getSeq_nbr());
				ps.setString(AntiMagicNumber.THREE, rule.getVal());
				ps.setString(AntiMagicNumber.FOUR, rule.getValType());
				ps.setLong(AntiMagicNumber.FIVE, lvlId);
				ps.setString(AntiMagicNumber.SIX, rule.getActSW());
				ps.setString(AntiMagicNumber.SEVEN, rule.getActiveType());
				ps.setLong(AntiMagicNumber.EIGHT, rule.getLogicOprtrId());
				ps.setLong(AntiMagicNumber.NINE, rule.getMathOprtrId());
				ps.setLong(AntiMagicNumber.TEN, rule.getRuleCat1Id());
				ps.setLong(AntiMagicNumber.ELEVEN, rule.getRuleCat2Id());
				ps.setString(AntiMagicNumber.TWELVE, userId);
				ps.setTimestamp(AntiMagicNumber.THIRTEEN, new Timestamp(date.getTime()));
				ps.setString(AntiMagicNumber.FOURTEEN, rule.getPctSW());
				ps.setTimestamp(AntiMagicNumber.FIFTEEN, new Timestamp(date.getTime()));
				ps.setString(AntiMagicNumber.SIXTEEN, userId);
			}

			@Override
			public int getBatchSize() {
				return lstRulesDfns.size();
			}
		});
		return returnStr;
	}

	@Override
	public long insertlvl(final long lvlId, final String userId, final String commodityId, final String deptId, final String subDeptId, final long itmCls, final String vendorId,
			final String subCommodity, final String lvlType) throws DSVException {
		LOG.info("Query insertlvl" + this.sqlInsertIntoLVL);
		LOG.info("lvlId = " + lvlId);
		LOG.info("userId = " + userId);
		LOG.info("commodityId = " + commodityId);
		LOG.info("deptId = " + deptId);
		LOG.info("subDeptId = " + subDeptId);
		LOG.info("vendorId = " + vendorId);
		LOG.info("subCommodity = " + subCommodity);
		LOG.info("lvlType = " + lvlType);

		this.getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
				Date date = new Date();
				PreparedStatement pst = null;
				pst = arg0.prepareStatement(sqlInsertIntoLVL, new String[] { "LVL_ID" });
				if (!Helper.isEmpty(lvlType)) {
					pst.setString(AntiMagicNumber.ONE, lvlType);
				} else {
					pst.setString(AntiMagicNumber.ONE, null);
				}
				if (!Helper.isEmpty(vendorId) && !AntiMagicNumber.STRING_ZERO.equals(vendorId)) {
					pst.setLong(AntiMagicNumber.TWO, NumberUtils.toLong(vendorId));
				} else {
					pst.setString(AntiMagicNumber.TWO, null);
				}
				if (!Helper.isEmpty(commodityId) && !AntiMagicNumber.STRING_ZERO.equals(commodityId)) {
					pst.setLong(AntiMagicNumber.THREE, NumberUtils.toLong(commodityId));
				} else {
					pst.setString(AntiMagicNumber.THREE, null);
				}
				if (!Helper.isEmpty(subCommodity) && !AntiMagicNumber.STRING_ZERO.equals(subCommodity)) {
					pst.setLong(AntiMagicNumber.FOUR, NumberUtils.toLong(subCommodity));
				} else {
					pst.setString(AntiMagicNumber.FOUR, null);
				}
				pst.setString(AntiMagicNumber.FIVE, deptId);
				pst.setString(AntiMagicNumber.SIX, subDeptId);
				pst.setLong(AntiMagicNumber.SEVEN, itmCls);
				pst.setString(AntiMagicNumber.EIGHT, userId);
				pst.setTimestamp(AntiMagicNumber.NINE, new Timestamp(date.getTime()));
				pst.setLong(AntiMagicNumber.TEN, lvlId);
				LOG.info("userId : " + userId);
				LOG.info("Update TimesTamp : " + new Timestamp(date.getTime()));
				pst.setString(AntiMagicNumber.ELEVEN, userId);
				pst.setTimestamp(AntiMagicNumber.TWELVE, new Timestamp(date.getTime()));
				return pst;
			}
		});

		return lvlId;
	}

	/**
	 * getGlobalCommodityOverride.
	 * @return Map<String, LevelVO>
	 * @author anhtran.
	 */
	@Override
	public Map<String, LevelVO> getGlobalCommodityOverride() {
		Map<String, LevelVO> globalCommodityMap = new HashMap<String, LevelVO>();
		List<LevelVO> lstRules = this.getJdbcTemplate().query(
				this.getGlobalCommodityOverrideSql, RuleMapper.getGlobalCommodityOverrideMapper()
				);
		for (LevelVO levelVO : lstRules) {
			globalCommodityMap.put(levelVO.getSubDeptId().trim().concat(String.valueOf(levelVO.getComdId())), levelVO);
		}
		return globalCommodityMap;
	}

	@Override
	public String insertlvlActivated(final String userId, final List<AssortmentRulesVO> lstAssortmentRulesVO, final String vendorId, final long lvlId, final String commodityId)
			throws DSVException {
		String messReturn = Constants.SUCCESSFULLY;
		try {
			this.getJdbcTemplate().batchUpdate(sqlInsertIntoLVL, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement pst, int i) throws SQLException {

					AssortmentRulesVO assortmentRulesVO = lstAssortmentRulesVO.get(i);
					Date date = new Date();
					pst.setString(AntiMagicNumber.ONE, "ACTV");
					pst.setLong(AntiMagicNumber.TWO, NumberUtils.toLong(vendorId));
					if (commodityId == null) {
						pst.setLong(AntiMagicNumber.THREE, assortmentRulesVO.getHebComdId());
					} else {
						pst.setLong(AntiMagicNumber.THREE, Long.parseLong(commodityId));
					}
					pst.setLong(AntiMagicNumber.FOUR, assortmentRulesVO.getHebSubComdId());
					pst.setString(AntiMagicNumber.FIVE, assortmentRulesVO.getDeptId());
					pst.setString(AntiMagicNumber.SIX, assortmentRulesVO.getSubDeptId());
					pst.setLong(AntiMagicNumber.SEVEN, assortmentRulesVO.getItmCls());
					pst.setString(AntiMagicNumber.EIGHT, userId);
					pst.setTimestamp(AntiMagicNumber.NINE, new Timestamp(date.getTime()));
					pst.setLong(AntiMagicNumber.TEN, lvlId + i);
					pst.setString(AntiMagicNumber.ELEVEN, userId);
					pst.setTimestamp(AntiMagicNumber.TWELVE, new Timestamp(date.getTime()));
				}

				@Override
				public int getBatchSize() {
					return lstAssortmentRulesVO.size();
				}
			});
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return messReturn;
	}

	@Override
	public boolean removeRule(final long lvlId) {
		boolean flag = false;
		try {
			this.getJdbcTemplate().update(this.sqlRemoveRuleByLvlId, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setLong(AntiMagicNumber.ONE, lvlId);

				}
			});
			flag = true;
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);

		}
		return flag;
	}

	@Override
	public boolean removeLvl(final long lvlId) {
		boolean flag = false;
		try {
			this.getJdbcTemplate().update(this.sqlRemoveLvl, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setLong(AntiMagicNumber.ONE, lvlId);
				}
			});
			flag = true;
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);

		}
		return flag;
	}

	@Override
	public long getLvlIdByCommodity(final String commodityId, final String deptId, final String subDeptId) {
		long lvlId = 0;
		try {
			List<Long> lstLvlId = this.getJdbcTemplate().query(this.sqlGetLVLKeyByCommodityGlobal,
					new PreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement arg0) throws SQLException {
							arg0.setString(AntiMagicNumber.ONE, commodityId);
							arg0.setString(AntiMagicNumber.TWO, deptId);
							arg0.setString(AntiMagicNumber.THREE, subDeptId);
						}
					}, RuleMapper.getLvlIdMapper());
			if (!Helper.isEmpty(lstLvlId)) {
				lvlId = lstLvlId.get(0);
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);

		}
		return lvlId;
	}

	@Override
	public long getLvlIdBysubDept(final String deptId, final String subDeptId) {
		long lvlId = 0;
		try {
			List<Long> lstLvlId = this.getJdbcTemplate().query(this.sqlGetLVLKeyBySubDeptGlobal,
					new PreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement arg0) throws SQLException {
							arg0.setString(AntiMagicNumber.ONE, deptId);
							arg0.setString(AntiMagicNumber.TWO, subDeptId);
						}
					}, RuleMapper.getLvlIdMapper());
			if (!Helper.isEmpty(lstLvlId)) {
				lvlId = lstLvlId.get(0);
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);

		}
		return lvlId;
	}

	@Override
	public boolean removeRules(final List<Long> lstLvlIds) {
		boolean flag = false;
		try {
			this.getJdbcTemplate().batchUpdate(this.sqlRemoveRuleByLvlId, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Long lvlId = lstLvlIds.get(i);
					ps.setLong(AntiMagicNumber.ONE, lvlId);
				}

				@Override
				public int getBatchSize() {
					return lstLvlIds.size();
				}
			});
			flag = true;
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);

		}
		return flag;
	}

	@Override
	public boolean removeLvls(final List<Long> lstLvlIds) {
		boolean flag = false;
		try {
			this.getJdbcTemplate().batchUpdate(this.sqlRemoveLvl, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Long lvlId = lstLvlIds.get(i);
					ps.setLong(AntiMagicNumber.ONE, lvlId);
				}

				@Override
				public int getBatchSize() {
					return lstLvlIds.size();
				}
			});
			flag = true;
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);

		}
		return flag;
	}

	/**
	 * Get max lvlID form LVL table.
	 * @return long
	 * @author Duyen.Le
	 */
	@Override
	public long getMaxLVLID() {
		long lvlId = 0;
		try {
			List<Long> lstLvlId = this.getJdbcTemplate().query(this.getMaxLVLID, RuleMapper.getLvlIdMapper());
			if (!Helper.isEmpty(lstLvlId)) {
				lvlId = lstLvlId.get(0);
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);

		}
		return lvlId;
	}
}
