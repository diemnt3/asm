/*
 * $Id: AssortmentRulesDAOImpl.java,v 1.10 2015/03/12 11:19:22 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.heb.enterprise.dsv.assortment.dao.AssortmentRulesDAO;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ProductVO;

@Repository
public class AssortmentRulesDAOImpl implements AssortmentRulesDAO {

	/**
	 * getRuleProduct.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>
	 * @author anhtran.
	 */
	@Override
	public Map<String, AssortmentRulesVO> getRuleProduct(ProductVO product) {
		Map<String, AssortmentRulesVO> map = new HashMap<String, AssortmentRulesVO>();
		this.getRuleByVendorSubCommodity(product);
		this.getRuleByVendorCommodity(product);
		this.getRuleByVendor(product);
		this.getRuleByGlobalSubDepartment(product);
		this.getRuleByGlobalCommodity(product);
		return map;
	}

	/**
	 * getRuleByVendorSubCommodity.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>
	 * @author anhtran.
	 */
	private Map<String, AssortmentRulesVO> getRuleByVendorSubCommodity(ProductVO product) {
		Map<String, AssortmentRulesVO> map = new HashMap<String, AssortmentRulesVO>();
		return map;
	}

	/**
	 * getRuleByVendorCommodity.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>
	 * @author anhtran.
	 */
	private Map<String, AssortmentRulesVO> getRuleByVendorCommodity(ProductVO product) {
		Map<String, AssortmentRulesVO> map = new HashMap<String, AssortmentRulesVO>();
		return map;
	}

	/**
	 * getRuleByVendor.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>
	 * @author anhtran.
	 */
	private Map<String, AssortmentRulesVO> getRuleByVendor(ProductVO product) {
		Map<String, AssortmentRulesVO> map = new HashMap<String, AssortmentRulesVO>();
		return map;
	}

	/**
	 * getRuleByGlobalCommodity.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>
	 * @author anhtran.
	 */
	private Map<String, AssortmentRulesVO> getRuleByGlobalCommodity(ProductVO product) {
		Map<String, AssortmentRulesVO> map = new HashMap<String, AssortmentRulesVO>();
		return map;
	}

	/**
	 * getRuleByGlobalSubDepartment.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>
	 * @author anhtran.
	 */
	private Map<String, AssortmentRulesVO> getRuleByGlobalSubDepartment(ProductVO product) {
		Map<String, AssortmentRulesVO> map = new HashMap<String, AssortmentRulesVO>();
		return map;
	}

}
