/*
 * $Id: AssortmentRulesDAO.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao;

import java.util.Map;

import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ProductVO;

/**
 * AssortmentRulesDAO.
 * @author anhtran.
 */
public interface AssortmentRulesDAO {

	/**
	 * getRuleProduct.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>
	 * @author anhtran.
	 */
	Map<String, AssortmentRulesVO> getRuleProduct(ProductVO product);

}
