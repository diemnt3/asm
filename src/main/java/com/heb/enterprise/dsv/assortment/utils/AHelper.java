/*
 * $Id: AHelper.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.utils;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;

/**
 * Common Function (Utility, parse JSON, convert..).
 */
public final class AHelper {
	private static final Logger LOG = Logger.getLogger(AHelper.class);

	/**
	 * Constructor method.
	 */
	public AHelper() {
	}

	/**
	 * parseJsonObjectToListHierarchyMapVO.
	 * @param orderJsonId
	 *            :String.
	 * @return The list HierarchyMapVO.
	 * @author anhtran
	 */
	// public static List<HierarchyMapVO> parseJsonObjectToListHierarchyMapVO(String orderJsonId) {
	// List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
	// return hierarchyMapVOs;
	// }

	/**
	 * parseListCommodityVOToJsonStr.
	 * @param commodityVOs
	 *            :List<CommodityVO>.
	 * @return String.
	 * @author anhtran
	 */
	// public static String parseListCommodityVOToJsonStr(List<CommodityVO> commodityVOs) {
	// return "";
	// }

	/**
	 * parseListHierarchyMapVOToJsonStr.
	 * @param hierarchyMapVOs
	 *            :List<HierarchyMapVO>.
	 * @return String.
	 * @author anhtran
	 */
	// public static String parseListHierarchyMapVOToJsonStr(List<HierarchyMapVO> hierarchyMapVOs) {
	// return "";
	// }

	/**
	 * parseListVendorsVOToJsonStr.
	 * @param jsonResult
	 *            :JSONObject
	 * @return String.
	 * @author anhtran
	 */
	// public static String parseListVendorsVOToJsonStr(JSONObject jsonResult) {
	// String json = Constants.JSON_EMPTY;
	// ObjectMapper mapper = new ObjectMapper();
	// try {
	// json = mapper.writeValueAsString(jsonResult);
	// } catch (JsonGenerationException e) {
	// LOG.error(e.getMessage(), e);
	// } catch (JsonMappingException e) {
	// LOG.error(e.getMessage(), e);
	// } catch (IOException e) {
	// LOG.error(e.getMessage(), e);
	// }
	// return json;
	// }

	/**
	 * parseFileToListHierarchyMapVO.
	 * @param file
	 *            :File.
	 * @return The list HierarchyMapVO.
	 * @author anhtran
	 */
	// public static List<HierarchyMapVO> parseFileToListHierarchyMapVO(File file) {
	// List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
	// return hierarchyMapVOs;
	// }

	/**
	 * parseJsonObjectToRetailCostVO.
	 * @param orderJsonId
	 *            :String.
	 * @return The list HierarchyMapVO.
	 * @author anhtran
	 */
	// public static List<RetailCostVO> parseJsonObjectToListRetailCostVO(String orderJsonId) {
	// List<RetailCostVO> retailCostVOs = new ArrayList<RetailCostVO>();
	// return retailCostVOs;
	// }

	/**
	 * parseJsonObjectToJsonStr.
	 * @param jsonResult
	 *            :JSONObject
	 * @return String.
	 * @author anhtran
	 */
	// public static String parseJsonObjectToJsonStr(JSONObject jsonResult) {
	// String json = Constants.JSON_EMPTY;
	// ObjectMapper mapper = new ObjectMapper();
	// try {
	// json = mapper.writeValueAsString(jsonResult);
	// } catch (JsonGenerationException e) {
	// LOG.error(e.getMessage(), e);
	// } catch (JsonMappingException e) {
	// LOG.error(e.getMessage(), e);
	// } catch (IOException e) {
	// LOG.error(e.getMessage(), e);
	// }
	// LOG.info("JSON string:" + json);
	// return json;
	// }

	/**
	 * convertListToString.
	 * @param lstParams
	 *            :List<String>
	 * @return String.
	 * @author thangdang
	 */
	public static String convertListToString(List<String> lstParams) {
		String strReturn = Constants.EMPTY_STRING;
		for (String str : lstParams) {
			strReturn = strReturn.concat(str).concat(",");
		}
		if (!Constants.EMPTY_STRING.equals(strReturn.trim())) {
			strReturn = strReturn.substring(0, strReturn.length() - 1);
		}
		return strReturn;
	}

	/**
	 * getSearchConditionBdmReview.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @return String.
	 * @author thangdang
	 */
	public static String getSearchConditionBdmReview(ProductSearchCriteriaVO productSearchCriteriaVO) {
		String condition = Constants.EMPTY_STRING;
		String filter = Constants.EMPTY_STRING;
		if (!Helper.isEmpty(productSearchCriteriaVO.getFailReason())) {
			condition = condition.concat(
					"INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD ").
					concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition = condition.concat(appendCriteriaString(ProductSearchCriteriaVO.ERR_TYP_CD,
					productSearchCriteriaVO.getFailReason())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		} else {
			condition = condition.concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getCommodittyId())) {
			condition = condition.concat(ProductSearchCriteriaVO.HEB_COM_ID).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getCommodittyId()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getBrand())) {
			condition = condition.concat(appendCriteriaLike(ProductSearchCriteriaVO.BRND_NM,
					productSearchCriteriaVO.getBrand())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getVendorId())) {
			condition = condition.concat(ProductSearchCriteriaVO.VEND_ID).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getVendorId()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getProdDes())) {
			condition = condition.concat(appendCriteriaLike(ProductSearchCriteriaVO.PROD_DES,
					productSearchCriteriaVO.getProdDes())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getDateBegin()) &&
				!Helper.isEmpty(productSearchCriteriaVO.getDateEnd())) {
			condition = condition.concat(searchDateCriteria(ProductSearchCriteriaVO.CRE8_TS,
					productSearchCriteriaVO.getDateBegin(), Constants.GREATER_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition = condition.concat(searchDateCriteria(ProductSearchCriteriaVO.CRE8_TS,
					productSearchCriteriaVO.getDateEnd(), Constants.LESS_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getUpc())) {
			condition = condition.concat(ProductSearchCriteriaVO.PROD_SCN_CD).concat(Constants.EQUAL_SYMBOL).
					concat(productSearchCriteriaVO.getUpc()).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}

		if (!Helper.isEmpty(productSearchCriteriaVO.getMarginBegin()) && !Helper.isEmpty(productSearchCriteriaVO.getMarginEnd())) {
			condition =
					condition.concat(ProductSearchCriteriaVO.MARGIN).concat(Constants.GREATER_THEN).concat(productSearchCriteriaVO.getMarginBegin()).concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition =
					condition.concat(ProductSearchCriteriaVO.MARGIN).concat(Constants.LESS_THEN).concat(productSearchCriteriaVO.getMarginEnd()).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}

		if (productSearchCriteriaVO.isAllMapProduct()) {
			condition = condition.concat(appendCriteriaString(ProductSearchCriteriaVO.MAP_SW,
					Constants.YES)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (productSearchCriteriaVO.isOnlyPrePriceProduct()) {
			condition = condition.concat(ProductSearchCriteriaVO.PRE_PRC_AMT).
					concat(Constants.GREATER_THAN_SIGN).concat(AntiMagicNumber.STRING_ZERO).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (AntiMagicNumber.STRING_ONE.equals(productSearchCriteriaVO.getStatus())) {
			String conditionPass = "(PRC.REV_STAT_CD IS NULL OR PRC.REV_STAT_CD ='APPRV') AND PRC.PROD_SCN_CD NOT IN "
					+ "(SELECT DISTINCT PS.PROD_SCN_CD FROM DSV_ASSRT.PROD_RULE_STAT PS WHERE PS.ERR_TYP_CD IN ('R0001', 'R0002', 'R0003', 'R0004', 'R0005', 'R0006', 'R0007', 'R0008', 'R0009'))";
			condition = condition.concat(conditionPass);
		} else if (AntiMagicNumber.STRING_TWO.equals(productSearchCriteriaVO.getStatus())) {
			String conditionFail = "(PRC.REV_STAT_CD IS NULL OR PRC.REV_STAT_CD ='APPRV') AND PRC.PROD_SCN_CD IN "
					+ "(SELECT DISTINCT PS.PROD_SCN_CD FROM DSV_ASSRT.PROD_RULE_STAT PS WHERE PS.ERR_TYP_CD IN ('R0001', 'R0002', 'R0003', 'R0004', 'R0005', 'R0006', 'R0007', 'R0008', 'R0009'))";
			condition = condition.concat(conditionFail);
		} else if (AntiMagicNumber.STRING_THREE.equals(productSearchCriteriaVO.getStatus())) {
			condition = condition.concat(appendCriteriaString(ProductSearchCriteriaVO.REV_STAT_CD, AConstants.RJECT));
		} else {
			condition = condition.concat(" (PRC.REV_STAT_CD IS NULL OR PRC.REV_STAT_CD ='RJECT' OR PRC.REV_STAT_CD ='APPRV')");
		}
		filter = AHelper.getSearchFilterBdmReview(productSearchCriteriaVO);
		if (!Helper.isEmpty(filter)) {
			if (filter.contains("INNER JOIN")) {
				condition = condition.concat(filter);
			} else {
				condition = condition.concat(Constants.AND_UPPERCASE_STR_BLANK).concat(filter);
			}
			condition = condition.substring(AntiMagicNumber.ZERO, condition.lastIndexOf(Constants.AND_UPPERCASE_STR));
		}

		return condition;
	}

	/**
	 * getSearchFitlerBdmReview.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @return String.
	 * @author thangdang
	 */
	private static String getSearchFilterBdmReview(ProductSearchCriteriaVO productSearchCriteriaVO) {
		String fitler = Constants.EMPTY_STRING;
		if (!Helper.isEmpty(productSearchCriteriaVO.getFailReasonFilter())) {
			if (!Helper.isEmpty(productSearchCriteriaVO.getFailReason())) {
				fitler = fitler.concat(appendCriteriaString(ProductSearchCriteriaVO.ERR_TYP_CD,
						productSearchCriteriaVO.getFailReasonFilter())).concat(Constants.AND_UPPERCASE_STR_BLANK);
			} else {
				fitler = fitler.concat(" INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD ").concat(Constants.AND_UPPERCASE_STR_BLANK);
				fitler = fitler.concat(appendCriteriaString(ProductSearchCriteriaVO.ERR_TYP_CD,
						productSearchCriteriaVO.getFailReasonFilter())).concat(Constants.AND_UPPERCASE_STR_BLANK);
			}
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getUpcIdFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.PROD_SCN_CD).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getUpcIdFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getProdDescFilter())) {
			fitler = fitler.concat(appendCriteriaLike(ProductSearchCriteriaVO.PROD_DES,
					productSearchCriteriaVO.getProdDescFilter())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getProdSizeFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.SELL_UNT_SZ).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getProdSizeFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getMapFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.MAP_AMT).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getMapFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getPrePriceFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.PRE_PRC_AMT).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getPrePriceFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getMsrpFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.MSRP_AMT).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getMsrpFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getSrpFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.SRP_AMT).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getSrpFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getCostFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.HEB_CST_AMT).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getCostFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getImageFilter())) {
			fitler = fitler.concat(appendCriteriaLike(ProductSearchCriteriaVO.IMG_URL_TXT,
					productSearchCriteriaVO.getImageFilter())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getFromReceivedTSFilter())) {
			fitler = fitler.concat(searchDateCriteria(ProductSearchCriteriaVO.CRE8_TS,
					productSearchCriteriaVO.getFromReceivedTSFilter(), Constants.GREATER_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getToReceivedTSFilter())) {
			fitler = fitler.concat(searchDateCriteria(ProductSearchCriteriaVO.CRE8_TS,
					productSearchCriteriaVO.getToReceivedTSFilter(), Constants.LESS_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getFromEditedTSFilter())) {
			fitler = fitler.concat(searchDateCriteria(ProductSearchCriteriaVO.LST_UPDT_TS,
					productSearchCriteriaVO.getFromEditedTSFilter(), Constants.GREATER_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getToEditedTSFilter())) {
			fitler = fitler.concat(searchDateCriteria(ProductSearchCriteriaVO.LST_UPDT_TS,
					productSearchCriteriaVO.getToEditedTSFilter(), Constants.LESS_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getPerGpFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.PER_GROSS_PROFIT).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getPerGpFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getPennyProfitFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.MARGIN).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getPennyProfitFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getHebRetailFilter())) {
			fitler = fitler.concat(ProductSearchCriteriaVO.HEB_RETL_AMT).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getHebRetailFilter()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		return fitler;
	}

	/**
	 * Append search equal.
	 * @param fieldName
	 *            String
	 * @param fieldValue
	 *            String
	 * @return String
	 * @author giaule
	 */
	public static String appendCriteriaString(String fieldName, String fieldValue) {
		String strQuery = Constants.EMPTY_STRING;
		String value = fieldValue;
		if (!Helper.isEmpty(fieldValue) && !Constants.PERCENT_STRING.equals(fieldValue)) {
			if (value.contains(Constants.QUOTESTRING)) {
				value = value.replaceAll(Constants.QUOTESTRING, Constants.DOUBLE_QUOTE);
			}
			strQuery = strQuery.concat(fieldName).concat(Constants.EQUAL_SYMBOL).
					concat(Constants.QUOTESTRING).concat(value.trim()).
					concat(Constants.QUOTESTRING);
		}

		return strQuery;
	}

	/**
	 * Append search equal.
	 * @param fieldName
	 *            String
	 * @param fieldValue
	 *            String
	 * @return String
	 * @author giaule
	 */
	public static String appendCriteriaStringIn(String fieldName, String fieldValue) {
		String strQuery = Constants.EMPTY_STRING;
		String value = fieldValue;
		if (!Helper.isEmpty(fieldValue) && !Constants.PERCENT_STRING.equals(fieldValue)) {
			if (value.contains(Constants.QUOTESTRING)) {
				value = value.replaceAll(Constants.QUOTESTRING, Constants.DOUBLE_QUOTE);
			}
			strQuery = strQuery.concat(fieldName).concat(Constants.EQUAL_SYMBOL).
					concat(Constants.QUOTESTRING).concat(value.trim()).
					concat(Constants.QUOTESTRING);
		}

		return strQuery;
	}

	/**
	 * Append value to search <b>LIKE</b> Criteria.
	 * @return generate the sql statement to search information from Database.
	 * @param fieldName
	 *            - The field have name same as field in Database.
	 * @param fieldValue
	 *            - The value will be fill when execute searching.
	 * @param operator
	 *            - The operator in sqlStatement.
	 * @author duyen.le
	 */
	public static String searchDateCriteria(final String fieldName, final String fieldValue, final String operator) {
		StringBuilder strQuery = new StringBuilder();
		if (!Helper.isEmpty(fieldValue)) {
			String format = "TRUNC(%s) " + operator + " TO_DATE('%s', 'mm/dd/YYYY')";
			strQuery.append(String.format(format, fieldName, fieldValue));
		}
		return strQuery.toString();
	}

	/**
	 * Append search equal.
	 * @param fieldName
	 *            String
	 * @param fieldValue
	 *            String
	 * @return String
	 * @author giaule
	 */
	public static String appendCriteriaLike(final String fieldName, final String fieldValue) {
		String strQuery = Constants.EMPTY_STRING;
		String fieldValueStr = fieldValue;

		if (!Helper.isEmpty(fieldValue) && !Constants.PERCENT_STRING.equals(fieldValue)) {
			if (fieldValueStr.contains(Constants.QUOTESTRING)) {
				fieldValueStr = fieldValueStr.replace(Constants.QUOTESTRING, Constants.DOUBLE_QUOTE);
			}
			fieldValueStr = fieldValueStr.toUpperCase();
			strQuery = strQuery.concat(" UPPER(" + fieldName + ") like '%");
			strQuery = strQuery.concat(fieldValueStr).concat("%'");
		}

		return strQuery;
	}

	/**
	 * get Excel FileName.
	 * @param defaultName
	 *            {@link String} default name
	 * @return
	 * @author ANNGUYEN
	 */
	public static String getDownloadFileName(String defaultName) {
		// get current date time with Date()
		Date date = new Date();
		String dateString = com.heb.enterprise.dsv.utils.Helper.parseDateToString(date, Constants.DATE_FORMAT_MMDDYYYY, Locale.US);
		dateString = dateString + Constants.UNDERSTROKE + defaultName;
		return dateString.replace(Constants.WHITE_SPACE, Constants.EMPTY_STRING);
	}

	/**
	 * Build excel file.
	 * @param response
	 *            The response of HttpServlet
	 * @param workbook
	 *            The workbook of POI library contain byte array will be exported to excel file
	 * @throws DSVException
	 *             If the workbook cannot export to excel file
	 * @author ThangHT
	 */
	public static void buildExcelFile(HttpServletResponse response, HSSFWorkbook workbook, String fileName) throws DSVException {
		try {
			response.setHeader(Constants.CONTENT_DISPOSITION, Constants.FILE_NAME_ATTACHMENT + fileName + Constants.EXCEL_EXTENSION);
			response.setContentType(Constants.EXCEL_CONTENT_TYPE);
			ServletOutputStream outputStream = response.getOutputStream();
			workbook.write(outputStream);
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			throw new DSVException(e);
		}
	}

	/**
	 * getSearchConditionBdmReview.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @return String.
	 * @author thangdang
	 */
	public static String getSearchConditionBdmReviewCount(ProductSearchCriteriaVO productSearchCriteriaVO) {
		String condition = Constants.EMPTY_STRING;
		String filter = Constants.EMPTY_STRING;
		if (!Helper.isEmpty(productSearchCriteriaVO.getFailReason())) {
			condition = condition.concat(
					"INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD ").
					concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition = condition.concat(appendCriteriaString(ProductSearchCriteriaVO.ERR_TYP_CD,
					productSearchCriteriaVO.getFailReason())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		} else {
			condition = condition.concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getCommodittyId())) {
			condition = condition.concat(ProductSearchCriteriaVO.HEB_COM_ID).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getCommodittyId()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getBrand())) {
			condition = condition.concat(appendCriteriaLike(ProductSearchCriteriaVO.BRND_NM,
					productSearchCriteriaVO.getBrand())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getVendorId())) {
			condition = condition.concat(ProductSearchCriteriaVO.VEND_ID).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getVendorId()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getProdDes())) {
			condition = condition.concat(appendCriteriaLike(ProductSearchCriteriaVO.PROD_DES,
					productSearchCriteriaVO.getProdDes())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getDateBegin()) &&
				!Helper.isEmpty(productSearchCriteriaVO.getDateEnd())) {
			condition = condition.concat(searchDateCriteria(ProductSearchCriteriaVO.CRE8_TS,
					productSearchCriteriaVO.getDateBegin(), Constants.GREATER_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition = condition.concat(searchDateCriteria(ProductSearchCriteriaVO.CRE8_TS,
					productSearchCriteriaVO.getDateEnd(), Constants.LESS_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getUpc())) {
			condition = condition.concat(ProductSearchCriteriaVO.PROD_SCN_CD).concat(Constants.EQUAL_SYMBOL).
					concat(productSearchCriteriaVO.getUpc()).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}

		if (!Helper.isEmpty(productSearchCriteriaVO.getMarginBegin()) && !Helper.isEmpty(productSearchCriteriaVO.getMarginEnd())) {
			condition =
					condition.concat(ProductSearchCriteriaVO.MARGIN).concat(Constants.GREATER_THEN).concat(productSearchCriteriaVO.getMarginBegin()).concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition =
					condition.concat(ProductSearchCriteriaVO.MARGIN).concat(Constants.LESS_THEN).concat(productSearchCriteriaVO.getMarginEnd()).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}

		if (productSearchCriteriaVO.isAllMapProduct()) {
			condition = condition.concat(appendCriteriaString(ProductSearchCriteriaVO.MAP_SW,
					Constants.YES)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (productSearchCriteriaVO.isOnlyPrePriceProduct()) {
			condition = condition.concat(ProductSearchCriteriaVO.PRE_PRC_AMT).
					concat(Constants.GREATER_THAN_SIGN).concat(AntiMagicNumber.STRING_ZERO).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		condition = condition.concat(" (PRC.REV_STAT_CD IS NULL OR PRC.REV_STAT_CD ='RJECT' OR PRC.REV_STAT_CD ='APPRV')");
		filter = AHelper.getSearchFilterBdmReview(productSearchCriteriaVO);
		if (!Helper.isEmpty(filter)) {
			if (filter.contains("INNER JOIN")) {
				condition = condition.concat(filter);
			} else {
				condition = condition.concat(Constants.AND_UPPERCASE_STR_BLANK).concat(filter);
			}
			condition = condition.substring(AntiMagicNumber.ZERO, condition.lastIndexOf(Constants.AND_UPPERCASE_STR));
		}

		return condition;
	}

	/**
	 * getSearchConditionBdmReview.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @return String.
	 * @author thangdang
	 */
	public static String getSearchConditionBdmReviewCountFail(ProductSearchCriteriaVO productSearchCriteriaVO) {
		String condition = Constants.EMPTY_STRING;
		String filter = Constants.EMPTY_STRING;
		if (!Helper.isEmpty(productSearchCriteriaVO.getFailReason())) {
			condition = condition.concat(
					"INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD ").
					concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition = condition.concat(appendCriteriaString(ProductSearchCriteriaVO.ERR_TYP_CD,
					productSearchCriteriaVO.getFailReason())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		} else {
			condition = condition.concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getCommodittyId())) {
			condition = condition.concat(ProductSearchCriteriaVO.HEB_COM_ID).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getCommodittyId()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getBrand())) {
			condition = condition.concat(appendCriteriaString(ProductSearchCriteriaVO.BRND_NM,
					productSearchCriteriaVO.getBrand())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getVendorId())) {
			condition = condition.concat(ProductSearchCriteriaVO.VEND_ID).
					concat(Constants.EQUAL_SYMBOL).concat(productSearchCriteriaVO.getVendorId()).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getProdDes())) {
			condition = condition.concat(appendCriteriaLike(ProductSearchCriteriaVO.PROD_DES,
					productSearchCriteriaVO.getProdDes())).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getDateBegin()) &&
				!Helper.isEmpty(productSearchCriteriaVO.getDateEnd())) {
			condition = condition.concat(searchDateCriteria(ProductSearchCriteriaVO.CRE8_TS,
					productSearchCriteriaVO.getDateBegin(), Constants.GREATER_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition = condition.concat(searchDateCriteria(ProductSearchCriteriaVO.CRE8_TS,
					productSearchCriteriaVO.getDateEnd(), Constants.LESS_THEN)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (!Helper.isEmpty(productSearchCriteriaVO.getUpc())) {
			condition = condition.concat(ProductSearchCriteriaVO.PROD_SCN_CD).concat(Constants.EQUAL_SYMBOL).
					concat(productSearchCriteriaVO.getUpc()).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}

		if (!Helper.isEmpty(productSearchCriteriaVO.getMarginBegin()) && !Helper.isEmpty(productSearchCriteriaVO.getMarginEnd())) {
			condition =
					condition.concat(ProductSearchCriteriaVO.MARGIN).concat(Constants.GREATER_THEN).concat(productSearchCriteriaVO.getMarginBegin()).concat(Constants.AND_UPPERCASE_STR_BLANK);
			condition =
					condition.concat(ProductSearchCriteriaVO.MARGIN).concat(Constants.LESS_THEN).concat(productSearchCriteriaVO.getMarginEnd()).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}

		if (productSearchCriteriaVO.isAllMapProduct()) {
			condition = condition.concat(appendCriteriaString(ProductSearchCriteriaVO.MAP_SW,
					Constants.YES)).concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		if (productSearchCriteriaVO.isOnlyPrePriceProduct()) {
			condition = condition.concat(ProductSearchCriteriaVO.PRE_PRC_AMT).
					concat(Constants.GREATER_THAN_SIGN).concat(AntiMagicNumber.STRING_ZERO).
					concat(Constants.AND_UPPERCASE_STR_BLANK);
		}
		condition = condition.concat("( PRC.REV_STAT_CD IS NULL  OR PRC.REV_STAT_CD ='APPRV')");
		filter = AHelper.getSearchFilterBdmReview(productSearchCriteriaVO);
		if (!Helper.isEmpty(filter)) {
			if (filter.contains("INNER JOIN")) {
				condition = condition.concat(filter);
			} else {
				condition = condition.concat(Constants.AND_UPPERCASE_STR_BLANK).concat(filter);
			}
			condition = condition.substring(AntiMagicNumber.ZERO, condition.lastIndexOf(Constants.AND_UPPERCASE_STR));
		}

		return condition;
	}

	/**
	 * getProductSearch.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @return String.
	 * @author thangdang
	 */
	// public static String getProductSearchColumn(ProductSearchCriteriaVO productSearchCriteriaVO) {
	// String column = "PRC.LST_UPDT_TS";
	// return column;
	// }
}
