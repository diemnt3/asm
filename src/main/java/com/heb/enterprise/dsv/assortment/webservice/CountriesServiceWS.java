/*
 * $Id: CountriesServiceWS.java,v 1.5 2013/10/29 13:58:01 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice;

import java.util.List;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.CountriesVO;

/**
 * CountriesServiceWS.
 * @author anhtran.
 */
public interface CountriesServiceWS {
	/**
	 * GetCountryCode.
	 * @return List<CountriesVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran
	 */
	List<CountriesVO> getCountryCode() throws DSVException;
}
