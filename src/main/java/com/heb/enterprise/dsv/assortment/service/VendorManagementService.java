/*
 * $Id: VendorManagementService.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.sql.SQLException;
import java.util.List;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;

/**
 * VendorManagementService.
 * @author anhtran
 */
public interface VendorManagementService {

	/**
	 * getVendorHierarchyMappingByVendor.
	 * @param vendorId
	 *            :String
	 * @return String
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @author anhtran
	 */
	List<HierarchyMapVO> getVendorHierarchyMappingByVendor(String vendorId) throws DSVException;

	/**
	 * uploadHierarchyMap.
	 * @return String
	 * @author anhtran
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 */
	// String uploadHierarchyMap(File file) throws DSVException;

	/**
	 * saveVendorHierarchyMapping.
	 * @param hierarchyMapVOs
	 *            :List<HierarchyMapVO>
	 * @param vendorId
	 *            :String
	 * @throws SQLException
	 *             : SQLException.
	 * @author anhtran
	 */
	void saveVendorHierarchyMapping(List<HierarchyMapVO> hierarchyMapVOs, String vendorId) throws SQLException;

	/**
	 * Get HEB Hierarchy.
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author anhtran
	 */
	// String getHEBHierarchy() throws DSVException;

	/**
	 * deleteEntyRlshp.
	 * @param vendorId
	 *            :String
	 * @param categoryIds
	 *            :String
	 * @param subCommodity
	 *            :String
	 * @throws DSVException
	 *             :DSVException
	 * @throws SQLException
	 *             :SQLException
	 * @author anhtran.
	 */
	void deleteEntyRlshp(String vendorId, String categoryIds, String subCommodity) throws DSVException, SQLException;

	/**
	 * Download template file.
	 * @throws DSVException
	 *             If could not get values from DAO Layer.
	 * @return String
	 * @author anhtran
	 */
	// String downloadTemplateFile();

}
