/*
 * $Id: VendorManagementDAO.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao;

import java.sql.SQLException;
import java.util.List;

import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;

/**
 * VendorManagementDAO.
 * @author anhtran
 */
public interface VendorManagementDAO {
	/**
	 * getVendorHierarchyMappingByVendor.
	 * @param vendorId
	 *            :String
	 * @return List<HierarchyMapVO>
	 * @throws DSVException
	 *             :If could not get values from Service Layer.
	 * @author anhtran
	 */
	List<HierarchyMapVO> getVendorHierarchyMappingByVendor(String vendorId) throws DSVException;

	/**
	 * saveVendorHierarchyMapping.
	 * @param hierarchyMapVOs
	 *            :List<HierarchyMapVO>
	 * @param vendorId
	 *            :String
	 * @throws SQLException
	 *             :If could not get values from Service Layer.
	 * @author anhtran
	 */
	void saveVendorHierarchyMapping(List<HierarchyMapVO> hierarchyMapVOs, String vendorId) throws SQLException;

	/**
	 * getHEBHierarchy.
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return List<CommodityVO>
	 * @author anhtran
	 */
	List<CommodityVO> getHEBHierarchy() throws DSVException;

	/**
	 * deleteEntyRlshp.
	 * @param vendorId
	 *            String
	 * @param subCommodity
	 *            :String
	 * @param categoryIds
	 *            String
	 * @throws DSVException
	 *             :DSVException
	 * @throws SQLException
	 *             SQLException.
	 * @author anhtran.
	 */
	void deleteEntyRlshp(String vendorId, String categoryIds, String subCommodity) throws DSVException, SQLException;

	/**
	 * setCommonService.
	 * @param commonService
	 * :CommonService
	 * @param commonService
	 */
	void setCommonService(CommonService commonService);

}
