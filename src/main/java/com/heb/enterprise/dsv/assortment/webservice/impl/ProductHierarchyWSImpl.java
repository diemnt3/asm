/*
 * $Id: ProductHierarchyWSImpl.java,v 1.5 2015/01/05 13:58:01 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.webservice.ProductHierarchyWS;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;
import com.heb.xmlns.ei.Authentication.Authentication;
import com.heb.xmlns.ei.ClsComm.Class_Commodity;
import com.heb.xmlns.ei.GetCommodityDetail_Request.CommodityDetail_Request;
import com.heb.xmlns.ei.GetMerchandisingHierarchyDetails_Reply.GetMerchandisingHierarchyDetails_ReplyDEPT;
import com.heb.xmlns.ei.GetMerchandisingHierarchyDetails_Reply.GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPT;
import com.heb.xmlns.ei.GetMerchandisingHierarchyDetails_Reply.GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASS;
import com.heb.xmlns.ei.GetMerchandisingHierarchyDetails_Reply.GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITY;
import com.heb.xmlns.ei.GetMerchandisingHierarchyDetails_Reply.GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITYSUB_COMMODITY;
import com.heb.xmlns.ei.GetMerchandisingHierarchyDetails_Request.GetMerchandisingHierarchyDetails_Request;
import com.heb.xmlns.ei.ProductHierarchyService.ProductHierarchyServiceServiceagentLocator;
import com.heb.xmlns.ei.ProductHierarchyService.ProductHierarchy_PortType_1;

/**
 * ProductHierarchyWSImpl.
 * @author anhtran
 */
@Service
@EnableCaching
public class ProductHierarchyWSImpl implements ProductHierarchyWS {
	private static final Logger LOG = Logger.getLogger(ProductHierarchyWSImpl.class);
	private static final String USER_ID = "aa";
	private static final String PWD = "bb";
	private static final String CLIEND_ID = "ww";
	private static final String NOT_USED = "NOT USED";

	@Value("${productHierarchyService.endpoint}")
	private String productHierarchyServiceEndpoint;

	/**
	 * getCommodityDetailList.
	 * @return Map<String, CommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	@Override
	@Cacheable("COMMODITY_DETAIL_LIST_INFO")
	public Map<Integer, CommodityVO> getCommodityDetailList() throws DSVException {
		ProductHierarchyServiceServiceagentLocator ls = new ProductHierarchyServiceServiceagentLocator();
		ProductHierarchy_PortType_1 portType;
		Map<Integer, CommodityVO> commodityMap = null;
		Authentication authentication = new Authentication(USER_ID, PWD, CLIEND_ID);
		CommodityDetail_Request detailListRequest = new CommodityDetail_Request();
		detailListRequest.setAuthentication(authentication);
		try {
			LOG.info("## getCommodityDetailList: " + this.productHierarchyServiceEndpoint);
			portType = ls.getProductHierarchyService_1(new URL(this.productHierarchyServiceEndpoint));
			commodityMap = this.getCommodityMap(portType.getCommodityDetailList(detailListRequest));
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (RemoteException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return commodityMap;
	}

	/**
	 * getCommodityMap.
	 * @param classCommodityArray
	 *            :Class_Commodity[]
	 * @return Map<String, CommodityVO>
	 * @author anhtran.
	 */
	private Map<Integer, CommodityVO> getCommodityMap(Class_Commodity[] classCommodityArray) {
		Map<Integer, CommodityVO> commodityMap = new HashMap<Integer, CommodityVO>();
		for (Class_Commodity classCommodity : classCommodityArray) {
			if (classCommodity.getOMI_COM_CLS_CD().intValue() > 0 && !NOT_USED.equals(classCommodity.getOMI_COM_DES())) {
				commodityMap.put(classCommodity.getOMI_COM_CD().intValue(), this.convertClassCommodityToCommodityVO(classCommodity));
			}
		}
		return commodityMap;
	}

	/**
	 * Convert Class_Commodity to CommodityVO.
	 * @param classCommodity
	 *            :Class_Commodity
	 * @return CommodityVO
	 * @author anhtran.
	 */
	private CommodityVO convertClassCommodityToCommodityVO(Class_Commodity classCommodity) {
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(classCommodity.getOMI_COM_CD().intValue());
		commodityVO.setCommodityDesc(Helper.trim(classCommodity.getOMI_COM_DES()));
		commodityVO.setBdmCd(classCommodity.getBDM_CD());
		return commodityVO;
	}

	/**
	 * getMerchandisingHierarchyDetails.
	 * @return List<DepartmentVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	@Override
	@Cacheable("HIERACHY_INFO")
	public List<DepartmentVO> getMerchandisingHierarchyDetails() throws DSVException {
		ProductHierarchyServiceServiceagentLocator ls = new ProductHierarchyServiceServiceagentLocator();
		ProductHierarchy_PortType_1 portType;
		List<DepartmentVO> departmentVOs = null;
		Authentication authentication = new Authentication(USER_ID, PWD, CLIEND_ID);
		GetMerchandisingHierarchyDetails_Request detailListRequest = new GetMerchandisingHierarchyDetails_Request();
		detailListRequest.setAuthentication(authentication);
		try {
			LOG.info("## getMerchandisingHierarchyDetails : " + this.productHierarchyServiceEndpoint);
			portType = ls.getProductHierarchyService_1(new URL(this.productHierarchyServiceEndpoint));
			departmentVOs = this.getDepartmentList(portType.getMerchandisingHierarchyDetails(detailListRequest));
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (RemoteException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return departmentVOs;
	}

	/**
	 * getDepartmentList.
	 * @param replyDept
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPT[]
	 * @throws DSVException
	 *             :DSVException
	 * @return List<DepartmentVO>
	 * @author anhtran.
	 */
	private List<DepartmentVO> getDepartmentList(GetMerchandisingHierarchyDetails_ReplyDEPT[] replyDept) throws DSVException {
		List<DepartmentVO> departmentList = new ArrayList<DepartmentVO>();
		for (GetMerchandisingHierarchyDetails_ReplyDEPT hierarchyDetailsReplyDept : replyDept) {
			departmentList.add(this.convertMerchandisingHierarchyDetailsReplyDeptToDepartmentVO(hierarchyDetailsReplyDept));
		}
		return departmentList;
	}

	/**
	 * convertMerchandisingHierarchyDetailsReplyDeptToDepartmentVO.
	 * @param hierarchyDetailsReplyDept
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPT
	 * @return DepartmentVO
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	private DepartmentVO convertMerchandisingHierarchyDetailsReplyDeptToDepartmentVO(GetMerchandisingHierarchyDetails_ReplyDEPT hierarchyDetailsReplyDept) throws DSVException {
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setDeptId(hierarchyDetailsReplyDept.getDEPT_ID());
		departmentVO.setDeptDesc(Helper.trim(hierarchyDetailsReplyDept.getDEPT_DESC()));
		departmentVO.setSubDepartmentVOs(this.getSubDepartmentList(departmentVO.getDeptId(), hierarchyDetailsReplyDept.getSUB_DEPT()));
		return departmentVO;
	}

	/**
	 * getSubDepartmentList.
	 * @param replyDeptSubDept
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPT[]
	 * @throws DSVException
	 *             :DSVException
	 * @return List<SubDepartmentVO>
	 * @author anhtran.
	 */
	private List<SubDepartmentVO> getSubDepartmentList(String deptID, GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPT[] replyDeptSubDept) throws DSVException {
		List<SubDepartmentVO> subDepartmentList = new ArrayList<SubDepartmentVO>();
		for (GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPT hierarchyDetailsReplyDeptSubDept : replyDeptSubDept) {
			subDepartmentList.add(this.convertMerchandisingHierarchyDetailsReplySubDeptToSubDepartmentVO(deptID, hierarchyDetailsReplyDeptSubDept));
		}
		return subDepartmentList;
	}

	/**
	 * convertMerchandisingHierarchyDetailsReplySubDeptToSubDepartmentVO.
	 * @param hierarchyDetailsReplyDeptSubDept
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPT
	 * @return SubDepartmentVO
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	private SubDepartmentVO convertMerchandisingHierarchyDetailsReplySubDeptToSubDepartmentVO(String deptID, GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPT hierarchyDetailsReplyDeptSubDept)
			throws DSVException {
		SubDepartmentVO subDepartmentVO = new SubDepartmentVO();
		subDepartmentVO.setSubDeptId(deptID + Helper.trim(hierarchyDetailsReplyDeptSubDept.getSUB_DEPT_ID()));
		subDepartmentVO.setSubDeptDesc(Helper.trim(hierarchyDetailsReplyDeptSubDept.getSUB_DEPT_DESC()));
		subDepartmentVO.setClassVOs(this.getClassList(hierarchyDetailsReplyDeptSubDept.getCLASS()));
		subDepartmentVO.setSubDeptIdOri(Helper.trim(hierarchyDetailsReplyDeptSubDept.getSUB_DEPT_ID()));
		return subDepartmentVO;
	}

	/**
	 * getClassList.
	 * @param replyDeptSubDeptClass
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASS[]
	 * @throws DSVException
	 *             :DSVException
	 * @return List<ClassVO>
	 * @author anhtran.
	 */
	private List<ClassVO> getClassList(GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASS[] replyDeptSubDeptClass) throws DSVException {
		List<ClassVO> classVOs = new ArrayList<ClassVO>();
		for (GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASS detailsReplyDeptSubDeptClass : replyDeptSubDeptClass) {
			classVOs.add(this.convertMerchandisingHierarchyDetailsReplyClassToClassVO(detailsReplyDeptSubDeptClass));
		}
		return classVOs;
	}

	/**
	 * convertMerchandisingHierarchyDetailsReplyClassToClassVO.
	 * @param detailsReplyDeptSubDeptClass
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASS
	 * @throws DSVException
	 *             :DSVException
	 * @return ClassVO
	 * @author anhtran.
	 */
	private ClassVO convertMerchandisingHierarchyDetailsReplyClassToClassVO(GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASS detailsReplyDeptSubDeptClass) throws DSVException {
		ClassVO classVO = new ClassVO();
		classVO.setClassCode(detailsReplyDeptSubDeptClass.getCLASS_CODE());
		classVO.setClassDesc(Helper.trim(detailsReplyDeptSubDeptClass.getCLASS_DESC()));
		classVO.setCommodityVOs(this.getCommodityList(detailsReplyDeptSubDeptClass.getCOMMODITY()));
		return classVO;
	}

	/**
	 * getCommodityList.
	 * @param replyDeptSubDeptClassCommodity
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITY[]
	 * @return List<CommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	private List<CommodityVO> getCommodityList(GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITY[] replyDeptSubDeptClassCommodity) throws DSVException {
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		for (GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITY detailsReplyDeptSubDeptClassCommodity : replyDeptSubDeptClassCommodity) {
			if (!NOT_USED.equalsIgnoreCase(detailsReplyDeptSubDeptClassCommodity.getCOMMODITY_DESC().trim())) {
				commodityVOs.add(this.convertMerchandisingHierarchyDetailsReplyCommodityToCommodityVO(detailsReplyDeptSubDeptClassCommodity));
			}
		}
		return commodityVOs;
	}

	/**
	 * convertMerchandisingHierarchyDetailsReplyCommodityToCommodityVO.
	 * @param detailsReplyDeptSubDeptClassCommodity
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITY
	 * @return CommodityVO
	 * @author anhtran.
	 */
	private CommodityVO convertMerchandisingHierarchyDetailsReplyCommodityToCommodityVO(GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITY detailsReplyDeptSubDeptClassCommodity) {
		CommodityVO commodityVO = new CommodityVO();
		commodityVO.setCommodityCd(detailsReplyDeptSubDeptClassCommodity.getCOMMODITY_CD().intValue());
		commodityVO.setCommodityDesc(Helper.trim(detailsReplyDeptSubDeptClassCommodity.getCOMMODITY_DESC()));// commodityVO.getCommodityCd() + Constants.DASH +
		commodityVO.setSubCommodityVOs(this.getSubCommodityList(detailsReplyDeptSubDeptClassCommodity.getSUB_COMMODITY()));
		return commodityVO;
	}

	/**
	 * getSubCommodityList.
	 * @return List<SubCommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	private List<SubCommodityVO> getSubCommodityList(GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITYSUB_COMMODITY[] replyDeptSubDeptClassSubCommodity) {
		List<SubCommodityVO> subCommodityVOs = new ArrayList<SubCommodityVO>();
		for (GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITYSUB_COMMODITY detailsReplyDeptSubDeptClassSubCommodity : replyDeptSubDeptClassSubCommodity) {
			if (!NOT_USED.equalsIgnoreCase(detailsReplyDeptSubDeptClassSubCommodity.getSUB_COM_DESC().trim())) {
				subCommodityVOs.add(this.convertMerchandisingHierarchyDetailsReplySubCommodityToSubCommodityVO(detailsReplyDeptSubDeptClassSubCommodity));
			}
		}
		return subCommodityVOs;
	}

	/**
	 * convertMerchandisingHierarchyDetailsReplySubCommodityToSubCommodityVO.
	 * @param detailsReplyDeptSubDeptClassSubCommodity
	 *            :GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITYSUB_COMMODITY
	 * @return SubCommodityVO
	 * @author anhtran.
	 */
	private SubCommodityVO convertMerchandisingHierarchyDetailsReplySubCommodityToSubCommodityVO(
			GetMerchandisingHierarchyDetails_ReplyDEPTSUB_DEPTCLASSCOMMODITYSUB_COMMODITY detailsReplyDeptSubDeptClassSubCommodity) {
		SubCommodityVO subCommodityVO = new SubCommodityVO();
		subCommodityVO.setSubCommodityCd(detailsReplyDeptSubDeptClassSubCommodity.getSUB_COM_CD().intValue());
		subCommodityVO.setSubCommodityDesc(Helper.trim(detailsReplyDeptSubDeptClassSubCommodity.getSUB_COM_DESC()));
		return subCommodityVO;
	}

	/**
	 * Get list SubDepartments.
	 * @return List<SubDepartmentVO>
	 * @throws DSVException
	 * @author duyen.le
	 */
	@Override
	public List<SubDepartmentVO> getHierachyForGlobalRules() {
		List<SubDepartmentVO> departmentVOs = new ArrayList<SubDepartmentVO>();
		return departmentVOs;
	}

}
