/*
 * $Id: VendorManagementServiceImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.dao.VendorManagementDAO;
import com.heb.enterprise.dsv.assortment.service.VendorManagementService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;

/**
 * VendorManagementServiceImpl.
 * @author anhtran.
 */
@Service
public class VendorManagementServiceImpl implements VendorManagementService {

	@Autowired
	private VendorManagementDAO vendorManagementDAO;

	/**
	 * getVendorHierarchyMappingByVendor.
	 * @param vendorId
	 *            :String
	 * @return List<HierarchyMapVO>
	 * @throws DSVException
	 *             :DSVException.
	 * @author anhtran
	 */
	@Override
	public List<HierarchyMapVO> getVendorHierarchyMappingByVendor(String vendorId) throws DSVException {
		return this.vendorManagementDAO.getVendorHierarchyMappingByVendor(vendorId);
	}

	/**
	 * Save vendor hierarchy.
	 * @param hierarchyMapVOs
	 *            :List<HierarchyMapVO>
	 * @param vendorId
	 *            :String
	 * @throws SQLException
	 *             :SQLException.
	 * @author anhtran
	 */
	@Override
	public void saveVendorHierarchyMapping(List<HierarchyMapVO> hierarchyMapVOs, String vendorId) throws SQLException {
		this.vendorManagementDAO.saveVendorHierarchyMapping(hierarchyMapVOs, vendorId);
	}

	/**
	 * Get HEB Hierarchy.
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author anhtran
	 */
	// @Override
	// public String getHEBHierarchy() throws DSVException {
	// return AHelper.parseListCommodityVOToJsonStr(this.vendorManagementDAO.getHEBHierarchy());
	// }

	/**
	 * deleteEntyRlshp.
	 * @param vendorId
	 *            :String
	 * @param categoryIds
	 *            :String
	 * @param subCommodity
	 *            :String
	 * @throws DSVException
	 *             :DSVException
	 * @throws SQLException
	 *             :SQLException
	 * @author anhtran.
	 */
	@Override
	public void deleteEntyRlshp(String vendorId, String categoryIds, String subCommodity) throws DSVException, SQLException {
		this.vendorManagementDAO.deleteEntyRlshp(vendorId, categoryIds, subCommodity);
	}

}
