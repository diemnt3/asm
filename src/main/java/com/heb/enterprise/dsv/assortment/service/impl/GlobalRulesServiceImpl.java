/*
 * $Id: GlobalRulesServiceImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.heb.enterprise.dsv.assortment.dao.CommonDAO;
import com.heb.enterprise.dsv.assortment.dao.GlobalRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.RulesDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.GlobalRulesService;
import com.heb.enterprise.dsv.assortment.utils.Convert;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;

/**
 * GlobalManagementServiceImpl interface.
 * @author duyen.le
 */
@Service
public class GlobalRulesServiceImpl implements GlobalRulesService {
	private static final Logger LOG = Logger.getLogger(GlobalRulesServiceImpl.class);
	/**
	 * Inject GlobalManagementDAO.
	 * @author duyen.le
	 */
	@Autowired
	private GlobalRulesDAO globalRulesDAO;
	/**
	 * Inject CacheService.
	 */
	@Autowired
	private CacheService cacheService;
	/**
	 * Inject CommonService.
	 */
	@Autowired
	private CommonDAO commonDao;
	@Autowired
	private RulesDAO rulesDAO;

	/**
	 * get Rule set by CommodityID or SubDeptID.
	 * @param subDeptID
	 *            String
	 * @param commodityID
	 *            String
	 * @throws DSVException
	 *             DSVException
	 * @return List<AssortmentRulesVO>
	 * @author duyen.le
	 */
	@Override
	public Map<String, AssortmentRulesVO> getGlobalRuleSet(String subDeptID, String commodityID) throws DSVException {
		return this.globalRulesDAO.getGlobalRuleSet(subDeptID, commodityID);
	}

	/**
	 * update Rules of Commodity.
	 * @param subDept
	 *            String
	 * @param commodityId
	 *            String
	 * @param lstRules
	 *            List<AssortmentRulesVO>
	 * @param userId
	 *            String
	 * @return boolean
	 * @author duyen.le
	 */
	@Override
	@Transactional("transactionManager")
	public boolean updateGlobalRulesSetting(String userId, String subDept, String commodityId, List<AssortmentRulesVO> lstRules) {
		LOG.info("updateGlobalRulesSetting subDeptId=" + subDept + " - commodityId =" + commodityId);
		boolean flag = false;
		long lvlID = 0;
		try {
			List<RuleDefinationVO> lstAddRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
			List<RuleDefinationVO> lstEditRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
			boolean flagInsert = false;
			for (AssortmentRulesVO assortmentRulesVO : lstRules) {
				if (Constants.STRING_A.equals(assortmentRulesVO.getActionCd())) {
					flagInsert = true;

					Convert.convertAssortmentRulesVO(assortmentRulesVO, lstAddRuleDefinationVOs);
				} else if (Constants.STRING_U.equals(assortmentRulesVO.getActionCd())) {

					Convert.convertAssortmentRulesVO(assortmentRulesVO, lstEditRuleDefinationVOs);
				}
			}
			if (flagInsert) {
				LOG.info("insert into LVl");
				List<DepartmentVO> lstDepartmentVOs = cacheService.getHierachyForGlobalRulesFromCache();
				String deptId = subDept.substring(0, subDept.length() - 1);
				String subDeptId = subDept.substring(subDept.length() - 1);
				AssortmentRulesVO assortmentRulesVOTmp = Convert.getHierachy(lstDepartmentVOs, subDept, commodityId, null);
				lvlID = this.globalRulesDAO.getMaxLVLID();
				lvlID = lvlID + 1;
				LOG.info("insert into LVl with LVL ID : " + lvlID);
				lvlID = this.globalRulesDAO.insertlvl(lvlID, userId, commodityId, deptId, subDeptId, assortmentRulesVOTmp.getItmCls(), null, null, null);
				this.globalRulesDAO.insertRulesToDfns(lstAddRuleDefinationVOs, lvlID, userId);
			}
			if (!Helper.isEmpty(lstEditRuleDefinationVOs)) {
				lvlID = lstEditRuleDefinationVOs.get(0).getLvlId();
				LOG.info("Update LVl Table");
				int rs = this.globalRulesDAO.updateLvl(lvlID, userId);
				LOG.info("Update LVL Ok with result = " + rs);
				LOG.info("Update ToDfns");
				this.globalRulesDAO.updateRuleValuesToDfns(lstEditRuleDefinationVOs, userId);
			}
			flag = true;
		} catch (DSVException e) {
			LOG.info("Error e: " + e.getMessage(), e);
		}
		return flag;
	}

	/**
	 * Get list SubDepartments.
	 * @return List<SubDepartmentVO>
	 * @author duyen.le
	 * @throws DSVException
	 */
	/**
	 * Get list Sub-Department from Cache - Demo service.
	 * @return List<SubDepartmentVO>
	 * @author Duyen.Le
	 * @throws DSVException
	 *             :DSVException
	 */
	@Override
	public List<DepartmentVO> getHierachyForGlobalRules() throws DSVException {
		return this.cacheService.getHierachyForGlobalRulesFromCache();
	}

	@Override
	public BaseVO getLogicRules() throws DSVException {
		List<BaseVO> lstRuleLogic = this.commonDao.findAllRuleLogicOprtr();
		BaseVO logicRule = new BaseVO();
		if (!Helper.isEmpty(lstRuleLogic)) {
			for (BaseVO baseVO : lstRuleLogic) {
				if ("AND".equalsIgnoreCase(baseVO.getName())) {
					logicRule.setId(baseVO.getId());
					logicRule.setName(baseVO.getName());
				}
			}
		}
		return logicRule;
	}

	/**
	 * Retrieves a Rules set by Rule Group.
	 * @return List<AssortmentRulesVO> : List of Rules set are retrieved from Database.
	 * @author Duyen.le
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	@Override
	public List<AssortmentRulesVO> getRulesByRuleGroup() throws DSVException {
		return this.commonDao.getRulesByRuleGroup();
	}

	/**
	 * Reset the Rules to Higher Level.
	 * @param subDeptId
	 *            String
	 * @param commodityId
	 *            String
	 * @return boolean : Result.
	 * @author Duyen.le
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	@Override
	@Transactional("transactionManager")
	public boolean resetRulesToHigherLevel(String subDeptId, String commodityId) throws DSVException {
		LOG.info("resetRulesToHigherLevel subDeptId = " + subDeptId + " &commodityId=" + commodityId);
		AssortmentRulesVO assortmentRulesVOTmp = null;
		long lvlId = 0;
		boolean flag = false;
		List<DepartmentVO> lstDepartmentVOs = this.cacheService.getHierachyForGlobalRulesFromCache();
		if (Helper.isEmpty(commodityId)) {
			assortmentRulesVOTmp = Convert.getHierachy(lstDepartmentVOs, subDeptId, null, null);
			LOG.info("reset to higher Level with commodity = " + commodityId + " and DeptID = " + assortmentRulesVOTmp.getDeptId() + " and SubDeptId = " + assortmentRulesVOTmp.getSubDeptId());
			lvlId = this.globalRulesDAO.getLvlIdBysubDept(assortmentRulesVOTmp.getDeptId(), assortmentRulesVOTmp.getSubDeptId());
		} else {
			assortmentRulesVOTmp = Convert.getHierachy(lstDepartmentVOs, null, commodityId, null);
			LOG.info("reset to higher Level with commodity = " + commodityId + " and DeptID = " + assortmentRulesVOTmp.getDeptId() + " and SubDeptId = " + assortmentRulesVOTmp.getSubDeptId());
			lvlId = this.globalRulesDAO.getLvlIdByCommodity(commodityId, assortmentRulesVOTmp.getDeptId(), assortmentRulesVOTmp.getSubDeptId());
		}

		LOG.info("getLvlIdByCommodity lvlId = " + lvlId);
		if (lvlId > 0) {
			this.globalRulesDAO.removeRule(lvlId);
			this.globalRulesDAO.removeLvl(lvlId);
		}
		flag = true;
		return flag;
	}

	/**
	 * getGlobalCommodityOverride.
	 * @return Map<String, LevelVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	@Override
	public Map<String, LevelVO> getGlobalCommodityOverride() throws DSVException {
		return this.globalRulesDAO.getGlobalCommodityOverride();
	}
}
