/*
 * $Id: GlobalRulesService.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.util.List;
import java.util.Map;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.LevelVO;

/**
 * GlobalService interface.
 * @author duyen.le
 */
public interface GlobalRulesService {
	/**
	 * Get list LogicRules.
	 * @return List<BaseVO>
	 * @author duyen.le
	 * @throws DSVException
	 *             :DSVException
	 */
	BaseVO getLogicRules() throws DSVException;

	/**
	 * Get list SubDepartments.
	 * @return List<SubDepartmentVO>
	 * @author duyen.le
	 * @throws DSVException
	 *             :DSVException
	 */
	List<DepartmentVO> getHierachyForGlobalRules() throws DSVException;

	/**
	 * get Rule set by CommodityID or SubDeptID.
	 * @param subDeptID
	 *            String
	 * @param commodityID
	 *            :String
	 * @throws DSVException
	 *             :DSVException
	 * @return List<RuleConfigVO>
	 * @author duyen.le
	 */
	Map<String, AssortmentRulesVO> getGlobalRuleSet(String subDeptID, String commodityID) throws DSVException;

	/**
	 * update Rules set for Global Rules Setting.
	 * @param subDeptId
	 *            String
	 * @param commodityId
	 *            String
	 * @param userId
	 *            String
	 * @param lstRules
	 *            List<AssortmentRulesVO>
	 * @return boolean
	 * @author duyen.le
	 */
	boolean updateGlobalRulesSetting(String userId, String subDeptId, String commodityId, List<AssortmentRulesVO> lstRules);

	/**
	 * Retrieves a Rules set by Rule Group.
	 * @return List<AssortmentRulesVO> : List of Rules set are retrieved from Database.
	 * @author Duyen.le
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	List<AssortmentRulesVO> getRulesByRuleGroup() throws DSVException;

	/**
	 * Reset the Rules to Higher Level.
	 * @param subDeptId
	 *            String
	 * @param commodityId
	 *            String
	 * @return boolean : Result.
	 * @author Duyen.le
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	boolean resetRulesToHigherLevel(String subDeptId, String commodityId) throws DSVException;

	/**
	 * getGlobalCommodityOverride.
	 * @return Map<String, LevelVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	Map<String, LevelVO> getGlobalCommodityOverride() throws DSVException;
}
