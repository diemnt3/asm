/*
 * $Id: BDMServiceWSImpl.java,v 1.5 2013/10/29 13:58:01 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.webservice.BDMServiceWS;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.BDMDetailVO;
import com.heb.xmlns.ei.Authentication.Authentication;
import com.heb.xmlns.ei.BDMService.BDMServiceServiceagentLocator;
import com.heb.xmlns.ei.BDMService.BDMService_PortType;
import com.heb.xmlns.ei.MD_BDM.MD_BDM;
import com.heb.xmlns.ei.getBDMDetailList_Request.GetBDMDetailList_Request;
import com.heb.xmlns.ei.getBDMDetails_Request.GetBDMDetails_Request;

/**
 * BDMServiceWSImpl.
 * @author anhtran.
 */
@Service
public class BDMServiceWSImpl implements BDMServiceWS {

	private static final Logger LOG = Logger.getLogger(BDMServiceWSImpl.class);
	private static final String USER_ID = "aa";
	private static final String PWD = "bb";
	private static final String CLIEND_ID = "ww";

	@Value("${bdmService.endpoint}")
	private String bdmServiceEndpoint;

	/**
	 * getBDMDetailList.
	 * @return Map<String, BDMDetailVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	@Override
	public Map<String, BDMDetailVO> getBDMDetailList() throws DSVException {
		BDMServiceServiceagentLocator ls = new BDMServiceServiceagentLocator();
		BDMService_PortType portType;
		Map<String, BDMDetailVO> detailVOs = null;
		Authentication authentication = new Authentication(USER_ID, PWD, CLIEND_ID);
		GetBDMDetailList_Request request = new GetBDMDetailList_Request();
		request.setAuthentication(authentication);
		try {
			portType = ls.getBDMService(new URL(this.bdmServiceEndpoint));
			detailVOs = this.getBDMDetailVOMap(portType.getBDMDetailList(request));
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (RemoteException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return detailVOs;
	}

	/**
	 * getBDMDetailVOList.
	 * @param bdms
	 *            :MD_BDM[]
	 * @return List<BDMDetailVO>
	 * @author anhtran.
	 */
	private List<BDMDetailVO> getBDMDetailVOList(MD_BDM[] bdms) {
		List<BDMDetailVO> detailVOs = new ArrayList<BDMDetailVO>();
		for (MD_BDM bdm : bdms) {
			detailVOs.add(this.convertToBDMDetailVO(bdm));
		}
		return detailVOs;
	}

	/**
	 * getBDMDetailVOMap.
	 * @param bdms
	 *            :MD_BDM[]
	 * @return Map<String, BDMDetailVO>
	 * @author anhtran.
	 */
	private Map<String, BDMDetailVO> getBDMDetailVOMap(MD_BDM[] bdms) {
		Map<String, BDMDetailVO> detailVOs = new HashMap<String, BDMDetailVO>();
		for (MD_BDM bdm : bdms) {
			BDMDetailVO bdmDetailVO = this.convertToBDMDetailVO(bdm);
			detailVOs.put(bdmDetailVO.getBdmCd(), bdmDetailVO);
		}
		return detailVOs;
	}

	/**
	 * convertToBDMDetailVO.
	 * @param bdm
	 *            :MD_BDM
	 * @return BDMDetailVO
	 * @author anhtran.
	 */
	private BDMDetailVO convertToBDMDetailVO(MD_BDM bdm) {
		BDMDetailVO bdmDetailVO = new BDMDetailVO();
		bdmDetailVO.setBdmCd(Helper.trim(bdm.getBDM_CD()));
		bdmDetailVO.setBdmFrstNm(Helper.trim(bdm.getBDM_FRST_NM()));
		bdmDetailVO.setDbmLstNm(Helper.trim(bdm.getDBM_LST_NM()));
		bdmDetailVO.setBdmFullNm(Helper.trim(bdm.getBDM_FULL_NM()));
		bdmDetailVO.setActvSw(Helper.trim(bdm.getACTV_SW()));
		return bdmDetailVO;
	}

	/**
	 * getBDMDetails.
	 * @param bdmCd
	 *            :String
	 * @return BDMDetailVO
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	@Override
	public BDMDetailVO getBDMDetails(String bdmCd) throws DSVException {
		BDMServiceServiceagentLocator ls = new BDMServiceServiceagentLocator();
		BDMService_PortType portType;
		BDMDetailVO detailVO = new BDMDetailVO();
		Authentication authentication = new Authentication(USER_ID, PWD, CLIEND_ID);
		GetBDMDetails_Request request = new GetBDMDetails_Request();
		request.setAuthentication(authentication);
		request.setBDM_CD(bdmCd);
		try {
			portType = ls.getBDMService(new URL(this.bdmServiceEndpoint));
			List<BDMDetailVO> detailVOs = this.getBDMDetailVOList(portType.getBDMDetails(request));
			if (!detailVOs.isEmpty()) {
				detailVO = detailVOs.get(AntiMagicNumber.ZERO);
			}
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (RemoteException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return detailVO;
	}
}
