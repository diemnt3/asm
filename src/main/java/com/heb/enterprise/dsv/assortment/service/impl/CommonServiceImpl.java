/*
 * $Id: CommonServiceImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.dao.CommonDAO;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * @author thangdang
 */
@Service
public class CommonServiceImpl implements CommonService {
	/**
	 * Inject CommonDAO.
	 */
	@Autowired
	private CommonDAO commonDao;

	/**
	 * CommonServiceImpl constructor method.
	 */
	private CommonServiceImpl() {
	}

	@Override
	public HebUserDetails getUserLogin() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return (HebUserDetails) authentication.getPrincipal();
	}

	/**
	 * getAllAssortmentRule.
	 * @throws DSVException
	 *             :DSVException
	 */
	@Override
	public List<BaseVO> getAllAssortmentRule() throws DSVException {
		return this.commonDao.getAllAssortmentRule();
	}

	/**
	 * Get all list vendors.
	 * @return List<VendorInfoVO>
	 * @author Duyen.le
	 */
	@Override
	public List<VendorInfoVO> getAllListVendors() throws DSVException {
		// TODO Auto-generated method stub
		return this.commonDao.getAllListVendors();
	}
}
