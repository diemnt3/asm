/*
 * $Id: ConstantsWeb.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.utils;

/**
 * Constants for web params.
 * @author binhht
 */
public final class ConstantsWeb {
	/**
	 * DASHBOARD_PAGE.
	 */
	public static final String GLOBAL_PAGE = "global-default";

	/**
	 * VENDOR_MANAGEMENT_PAGE.
	 */
	public static final String VENDOR_PAGE = "vendor-management";

	/**
	 * VENDOR_RULE_PAGE.
	 */
	public static final String VENDOR_RULE_PAGE = "assortment-rules";

	/**
	 * BDM_REVIEW_PATH.
	 */
	public static final String BDM_REVIEW_PATH = "product-review";

	/**
	 * START_CREATE_MANUAL_INVOICE.
	 */
	public static final String UPDATE_COMMODITY_RULES = "ajax/commodity-updateRule";
	/**
	 * START_CREATE_MANUAL_INVOICE.
	 */
	public static final String GET_ASSORTMENT_RULES = "/ajax/getAssortmentRules";
	/**
	 * START_CREATE_MANUAL_INVOICE.
	 */
	public static final String UPDATE_ASM_RULES = "/ajax/assortment-updateRules";

	/**
	 * SEARCH_ORDER.
	 */
	public static final String SEARCH_PRODS_FOR_REVIEW_PATH = "product-review/search-product";
	/**
	 * REJECT_PRODS_PATH.
	 */
	public static final String REJECT_PRODS_PATH = "product-review/rejectProduct";
	/**
	 * APPROVE_PRODS_PATH.
	 */
	public static final String APPROVE_PRODS_PATH = "product-review/approveProduct";
	/**
	 * APPROVE_PRODS_PATH.
	 */
	public static final String UPDATE_PRODS_PATH = "product-review/updateProduct";
	/**
	 * APPROVE_PRODS_PATH.
	 */
	public static final String UN_REJECT_PRODS_PATH = "product-review/un-rejectProduct";

	/**
	 * CREATE_INVOICE_LIST_EXCEL_FILE.
	 */
	public static final String AJAX_HIERACHY = "ajax/getHierachy";
	/**
	 * EXPORT_INVOICE_DETAILS_DIRECTLY.
	 */
	public static final String LOAD_COMMODITY_RULES = "ajax/commodity-loadRule";
	/**
	 * EXPORT_INVOICE_DETAILS_DIRECTLY.
	 */
	public static final String RESET_RULES = "ajax/resetRuleToHigherLevel";
	/**
	 * EXPORT_INVOICE_DETAILS_DIRECTLY.
	 */
	public static final String RESET_RULES_VENDOR = "ajax/resetRule";

	/**
	 * searchItems by Orders.
	 */
	public static final String AJAX_GET_COMMODITY_AND_SUB = "/ajax/getCommodityAndSub";
	/**
	 * Search items by orders id and date.
	 */
	public static final String AJAX_GET_RULESET_FOR_VENDOR = "/ajax/assortment-loadRule";

	/**
	 * go to setting configure pricing Rules.
	 */
	public static final String PRICING_RULES_PAGE = "pricing-rules";
	/**
	 * Search items by order information.
	 */
	public static final String GET_RULES_PAGE = "getPricingRules";
	/**
	 * searchInvoice.
	 */
	public static final String UPDATE_RULES = "updateRules";
	/**
	 * invoiceDetails.
	 */
	public static final String GET_COMPETITORS = "ajax/getOnlineCompetitors";

	/**
	 * RETAIL_PAGE.
	 */
	public static final String RETAIL_PAGE = "cost-review";
	/**
	 * SEARCH_RETAIL.
	 */
	public static final String SEARCH_RETAIL = "cost-review/search";

	/**
	 * AJAX_AUTOCOMPLETE_GET_COMMODITY.
	 */
	public static final String AJAX_AUTOCOMPLETE_GET_COMMODITY = "/ajax/get-autocomplete-commodities";

	/**
	 * AJAX_AUTOCOMPLETE_GET_SUBCOMM.
	 */
	public static final String AJAX_AUTOCOMPLETE_GET_SUBCOMM = "/ajax/get-autocomplete-subCommodities";

	/**
	 * SEARCH_FOR_VENDORS.
	 */
	public static final String SEARCH_FOR_VENDORS = "/vendor-management/search-for-vendors";
	/**
	 * UPLOAD_HIERARCHY_MAP.
	 */
	public static final String UPLOAD_HIERARCHY_MAP = "/vendor-management/upload-hierarchy-map";
	/**
	 * DOWNLOAD_TEMPLATE.
	 */
	public static final String DOWNLOAD_TEMPLATE = "/vendor-management/download-template-file";
	/**
	 * GET_VENDORS_HIERARCHY.
	 */
	public static final String GET_VENDORS_HIERARCHY = "/vendor-management/get-vendor-hierarchy";
	/**
	 * GET_HEB_HIERARCHY.
	 */
	public static final String GET_HEB_HIERARCHY = "/vendor-management/get-heb-hierarchy";
	/**
	 * SAVE_VENDOR_HIERARCHY.
	 */
	public static final String SAVE_VENDOR_HIERARCHY = "/vendor-management/save-vendor-hierarchy";
	/**
	 * DELETE_ENTYRLSHP.
	 */
	public static final String DELETE_ENTYRLSHP = "/vendor-management/delete-entyRlshp";

	/**
	 * GET_HEB_HIERARCHY_ROOT.
	 */
	public static final String GET_HEB_HIERARCHY_ROOT = "/vendor-management/get-heb-hierarchy-root";

	/**
	 * SEARCH_HEB_HIERARCHY.
	 */
	public static final String SEARCH_HEB_HIERARCHY = "/vendor-management/search-heb-hierarchy";

	/**
	 * RULE_ORI.
	 */
	public static final String RULE_ORI = "RULE_ORI";

	/**
	 * AJAX_AUTOCOMPLETE_GET_DATA_COMMODITY.
	 */
	public static final String AJAX_AUTOCOMPLETE_GET_DATA_COMMODITY = "/ajax/get-data-autocomplete-commodities";
	/**
	 * AJAX_AUTOCOMPLETE_GET_DATA_VENDOR.
	 */
	public static final String AJAX_AUTOCOMPLETE_GET_DATA_VENDOR = "/ajax/get-data-autocomplete-vendor";

	/**
	 * AJAX_AUTOCOMPLETE_GET_DATA_BRAND.
	 */
	public static final String AJAX_AUTOCOMPLETE_GET_DATA_BRAND = "/ajax/get-data-autocomplete-brand";
	/**
	 * AJAX_AUTOCOMPLETE_GET_DATA_PRODUCT.
	 */
	public static final String AJAX_AUTOCOMPLETE_GET_DATA_PRODUCT = "/ajax/get-data-autocomplete-product";

	/**
	 * AJAX_AUTOCOMPLETE_GET_DATA_BRAND.
	 */
	public static final String AJAX__GET_DATA_FAILED_REASON = "/ajax/get-data-failed-reason";
	/**
	 * EXPORT_PRODUCT_REVIEW_TO_EXCEL_DIRECTLY.
	 */
	public static final String EXPORT_PRODUCT_REVIEW_TO_EXCEL_DIRECTLY = "/new-product-review/export-to-excel-directly";

	/**
	 * ACTIVATE_RULES_VENDOR.
	 */
	public static final String ACTIVATE_RULES_VENDOR = "/ajax/activateRule";
	/**
	 * DEACTIVATE_RULES_VENDOR.
	 */
	public static final String DEACTIVATE_RULES_VENDOR = "/ajax/deActivateRule";

	/**
	 * EXPORT_COST_REVIEW_TO_EXCEL_DIRECTLY.
	 */
	public static final String EXPORT_COST_REVIEW_TO_EXCEL_DIRECTLY = "/cost-review/export-to-excel-directly";

	/**
	 * Constructor method.
	 */
	private ConstantsWeb() {
	}
}
