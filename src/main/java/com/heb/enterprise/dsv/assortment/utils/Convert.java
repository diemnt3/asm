/*
 * $Id: Convert.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;

import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;

/**
 * Common Function (Utility, parse JSON, convert..).
 * @author thangdang
 */
public final class Convert {
	/**
	 * Constructor method.
	 */
	private Convert() {
	}

	/**
	 * convertAssortmentRulesVO.
	 * @param assortmentRulesVO
	 *            AssortmentRulesVO
	 * @param lstRuleDefinationVOs
	 *            List<RuleDefinationVO>
	 * @author thangdang
	 */
	public static void convertAssortmentRulesVO(AssortmentRulesVO assortmentRulesVO, List<RuleDefinationVO> lstRuleDefinationVOs) {
		String activeType = Constants.EMPTY_STRING;
		RuleDefinationVO ruleDefinationVO;
		if (!Helper.isEmpty(assortmentRulesVO.getValues()) && assortmentRulesVO.getValues().size() > AntiMagicNumber.ONE) {
			for (String value : assortmentRulesVO.getValues()) {
				String[] valueArr = value.split(":");
				ruleDefinationVO = new RuleDefinationVO();
				ruleDefinationVO.setRuleId(NumberUtils.toInt(assortmentRulesVO.getId()));
				ruleDefinationVO.setVal(valueArr[AntiMagicNumber.ONE]);
				if ("Start".equals(valueArr[0].trim())) {
					activeType = "Start";
					ruleDefinationVO.setSeq_nbr(AntiMagicNumber.ONE);
					ruleDefinationVO.setMathOprtrId(AntiMagicNumber.ONE);
				} else {
					activeType = "End";
					ruleDefinationVO.setSeq_nbr(AntiMagicNumber.TWO);
					ruleDefinationVO.setMathOprtrId(AntiMagicNumber.TWO);
				}
				if (Helper.isEmpty(value)) {
					ruleDefinationVO.setVal(AntiMagicNumber.STRING_ZERO);//
				}
				ruleDefinationVO.setLogicOprtrId(AntiMagicNumber.ONE);
				ruleDefinationVO.setRuleCat1Id(AntiMagicNumber.ONE);
				ruleDefinationVO.setRuleCat2Id(AntiMagicNumber.TWO);
				ruleDefinationVO.setActSW(assortmentRulesVO.getActiveSw());
				ruleDefinationVO.setActiveType(activeType);
				ruleDefinationVO.setValType(assortmentRulesVO.getValType());
				ruleDefinationVO.setLvlId(assortmentRulesVO.getLvlId());
				ruleDefinationVO.setPctSW(assortmentRulesVO.getPctSW());
				lstRuleDefinationVOs.add(ruleDefinationVO);
			}
		} else {
			ruleDefinationVO = new RuleDefinationVO();
			ruleDefinationVO.setRuleId(NumberUtils.toInt(assortmentRulesVO.getId()));
			ruleDefinationVO.setSeq_nbr(AntiMagicNumber.ONE);
			ruleDefinationVO.setActiveType(activeType);
			ruleDefinationVO.setValType(assortmentRulesVO.getValType());
			ruleDefinationVO.setActSW(assortmentRulesVO.getActiveSw());
			if (Constants.STRING_Y.equals(assortmentRulesVO.getActiveSw())) {//
				// if (Integer.parseInt(assortmentRulesVO.getId()) == AntiMagicNumber.TEN) {
				// ruleDefinationVO.setVal(assortmentRulesVO.getValues().get(0));
				// } else {
				ruleDefinationVO.setVal(AntiMagicNumber.STRING_ONE);
				// }
			} else {
				ruleDefinationVO.setVal(AntiMagicNumber.STRING_ZERO);
			}
			ruleDefinationVO.setLvlId(assortmentRulesVO.getLvlId());
			ruleDefinationVO.setLogicOprtrId(AntiMagicNumber.ONE);
			ruleDefinationVO.setRuleCat1Id(AntiMagicNumber.ONE);
			ruleDefinationVO.setRuleCat2Id(AntiMagicNumber.TWO);
			ruleDefinationVO.setMathOprtrId(AntiMagicNumber.FIVE);
			ruleDefinationVO.setPctSW(assortmentRulesVO.getPctSW());
			lstRuleDefinationVOs.add(ruleDefinationVO);
		}
	}

	/**
	 * getHierachy.
	 * @param lstDepartmentVOs
	 *            List<DepartmentVO>
	 * @param subdept
	 *            String
	 * @param commodityId
	 *            String
	 * @param subComId
	 *            String
	 * @return assortmentRulesVO
	 * @author thangdang
	 */
	public static AssortmentRulesVO getHierachy(List<DepartmentVO> lstDepartmentVOs,
			String subdept, String commodityId, String subComId) {
		AssortmentRulesVO assortmentRulesVO = null;
		if (!com.heb.enterprise.dsv.utils.Helper.isEmpty(subComId)) {
			assortmentRulesVO = Convert.getHierachySubComm(lstDepartmentVOs, subComId);
		} else if (!com.heb.enterprise.dsv.utils.Helper.isEmpty(commodityId)) {
			assortmentRulesVO = Convert.getHierachyComm(lstDepartmentVOs, commodityId);
		} else {
			assortmentRulesVO = Convert.getHierachyDept(lstDepartmentVOs, subdept);
		}
		return assortmentRulesVO;
	}

	/**
	 * getHierachySubComm.
	 * @param lstDepartmentVOs
	 *            List<DepartmentVO>
	 * @param subComId
	 *            String
	 * @return assortmentRulesVO
	 * @author thangdang
	 */
	private static AssortmentRulesVO getHierachySubComm(List<DepartmentVO> lstDepartmentVOs, String subComId) {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setItmCls(AntiMagicNumber.ZERO);
		assortmentRulesVO.setSubDeptId(Constants.EMPTY_STRING);
		assortmentRulesVO.setDeptId(Constants.EMPTY_STRING);
		for (DepartmentVO departmentVO : lstDepartmentVOs) {
			for (SubDepartmentVO subDepartmentVO : departmentVO.getSubDepartmentVOs()) {
				for (ClassVO classVO : subDepartmentVO.getClassVOs()) {
					for (CommodityVO commodityVO : classVO.getCommodityVOs()) {
						for (SubCommodityVO subCommodityVO : commodityVO.getSubCommodityVOs()) {
							if (subComId.equals(String.valueOf(subCommodityVO.getSubCommodityCd()))) {
								assortmentRulesVO.setHebComdId(commodityVO.getCommodityCd());
								assortmentRulesVO.setHebSubComdId(Long.valueOf(subComId));
								assortmentRulesVO.setItmCls(classVO.getClassCode().longValue());
								assortmentRulesVO.setSubDeptId(subDepartmentVO.getSubDeptIdOri());
								assortmentRulesVO.setDeptId(departmentVO.getDeptId());
								break;
							}
						}
					}
				}
			}
		}
		return assortmentRulesVO;
	}

	/**
	 * getHierachyComm.
	 * @param lstDepartmentVOs
	 *            List<DepartmentVO>
	 * @param commodityId
	 *            String
	 * @return assortmentRulesVO
	 * @author thangdang
	 */
	private static AssortmentRulesVO getHierachyComm(List<DepartmentVO> lstDepartmentVOs, String commodityId) {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setItmCls(AntiMagicNumber.ZERO);
		assortmentRulesVO.setSubDeptId(Constants.EMPTY_STRING);
		assortmentRulesVO.setDeptId(Constants.EMPTY_STRING);
		for (DepartmentVO departmentVO : lstDepartmentVOs) {
			for (SubDepartmentVO subDepartmentVO : departmentVO.getSubDepartmentVOs()) {
				for (ClassVO classVO : subDepartmentVO.getClassVOs()) {
					for (CommodityVO commodityVO : classVO.getCommodityVOs()) {
						if (commodityId.equals(String.valueOf(commodityVO.getCommodityCd()))) {
							assortmentRulesVO.setHebComdId(commodityVO.getCommodityCd());
							assortmentRulesVO.setItmCls(classVO.getClassCode().longValue());
							assortmentRulesVO.setSubDeptId(subDepartmentVO.getSubDeptIdOri());
							assortmentRulesVO.setDeptId(departmentVO.getDeptId());
							break;
						}
					}
				}
			}
		}
		return assortmentRulesVO;
	}

	/**
	 * getHierachyDept.
	 * @param lstDepartmentVOs
	 *            List<DepartmentVO>
	 * @param subdept
	 *            String
	 * @return assortmentRulesVO
	 * @author thangdang
	 */
	private static AssortmentRulesVO getHierachyDept(List<DepartmentVO> lstDepartmentVOs, String subdept) {
		AssortmentRulesVO assortmentRulesVO = new AssortmentRulesVO();
		assortmentRulesVO.setItmCls(AntiMagicNumber.ZERO);
		assortmentRulesVO.setSubDeptId(Constants.EMPTY_STRING);
		assortmentRulesVO.setDeptId(Constants.EMPTY_STRING);
		for (DepartmentVO departmentVO : lstDepartmentVOs) {
			for (SubDepartmentVO subDepartmentVO : departmentVO.getSubDepartmentVOs()) {
				for (ClassVO classVO : subDepartmentVO.getClassVOs()) {
					if (subdept.equals(subDepartmentVO.getSubDeptId())) {
						assortmentRulesVO.setItmCls(classVO.getClassCode().longValue());
						assortmentRulesVO.setSubDeptId(subDepartmentVO.getSubDeptIdOri());
						assortmentRulesVO.setDeptId(departmentVO.getDeptId());
						break;
					}
				}
			}
		}
		return assortmentRulesVO;
	}

	/**
	 * getPathSubComm.
	 * @param lstDepartmentVOs
	 *            List<DepartmentVO>
	 * @param lstsubComs
	 *            List<String>
	 * @return List<AssortmentRulesVO>
	 * @author thangdang
	 */
	public static List<AssortmentRulesVO> getPathSubComm(List<DepartmentVO> lstDepartmentVOs, List<String> lstsubComs) {
		List<AssortmentRulesVO> lstAssortmentRulesVOs = new ArrayList<AssortmentRulesVO>();
		AssortmentRulesVO assortmentRulesVO = null;
		for (String subComId : lstsubComs) {
			for (DepartmentVO departmentVO : lstDepartmentVOs) {
				for (SubDepartmentVO subDepartmentVO : departmentVO.getSubDepartmentVOs()) {
					for (ClassVO classVO : subDepartmentVO.getClassVOs()) {
						for (CommodityVO commodityVO : classVO.getCommodityVOs()) {
							for (SubCommodityVO subCommodityVO : commodityVO.getSubCommodityVOs()) {
								if (subComId.equals(String.valueOf(subCommodityVO.getSubCommodityCd()))) {
									assortmentRulesVO = new AssortmentRulesVO();
									assortmentRulesVO.setHebComdId(commodityVO.getCommodityCd());
									assortmentRulesVO.setHebSubComdId(NumberUtils.toLong(subComId));
									assortmentRulesVO.setItmCls(classVO.getClassCode().longValue());
									assortmentRulesVO.setSubDeptId(subDepartmentVO.getSubDeptIdOri());
									assortmentRulesVO.setDeptId(departmentVO.getDeptId());
									if (!lstAssortmentRulesVOs.contains(assortmentRulesVO)) {
										lstAssortmentRulesVOs.add(assortmentRulesVO);
									}
									break;
								}
							}
						}
					}
				}
			}
		}
		return lstAssortmentRulesVOs;
	}

	/**
	 * convertAssortmentRulesVO.
	 * @param assortmentRulesVO
	 *            AssortmentRulesVO
	 * @param lstRuleDefinationVOs
	 *            List<RuleDefinationVO>
	 * @author thangdang
	 */
	public static void convertPricingRulesVO(AssortmentRulesVO assortmentRulesVO, List<RuleDefinationVO> lstRuleDefinationVOs) {
		// String activeType = Constants.EMPTY_STRING;
		RuleDefinationVO ruleDefinationVO;
		if (!Helper.isEmpty(assortmentRulesVO.getValues()) && assortmentRulesVO.getValues().size() > AntiMagicNumber.ONE) {
			int seq = 1;
			for (String value : assortmentRulesVO.getValues()) {
				// String[] valueArr = value.split("-");
				ruleDefinationVO = new RuleDefinationVO();
				ruleDefinationVO.setRuleId(NumberUtils.toInt(assortmentRulesVO.getId()));
				if (AntiMagicNumber.STRING_FOURTEEN.equals(assortmentRulesVO.getId()) && seq == 1) {
					ruleDefinationVO.setVal(AntiMagicNumber.STRING_ZERO + Constants.DOT + value);
				} else {
					ruleDefinationVO.setVal(value);
				}
				ruleDefinationVO.setSeq_nbr(seq);
				ruleDefinationVO.setMathOprtrId(seq);
				if (Helper.isEmpty(value)) {
					ruleDefinationVO.setVal(AntiMagicNumber.STRING_ZERO);
				}
				ruleDefinationVO.setLogicOprtrId(AntiMagicNumber.ONE);
				ruleDefinationVO.setRuleCat1Id(AntiMagicNumber.ONE);
				ruleDefinationVO.setRuleCat2Id(AntiMagicNumber.TWO);
				// ruleDefinationVO.setActSW(Constants.STRING_Y);
				if (Constants.EMPTY_STRING.equals(value)) {
					ruleDefinationVO.setActSW(Constants.STRING_N);
				} else {
					ruleDefinationVO.setActSW(Constants.STRING_Y);
				}
				ruleDefinationVO.setActiveType(assortmentRulesVO.getActTypCd());
				// ruleDefinationVO.setValType(assortmentRulesVO.getValType());
				ruleDefinationVO.setValType(AntiMagicNumber.STRING_TWO);
				ruleDefinationVO.setLvlId(assortmentRulesVO.getLvlId());
				if (Constants.STRING_Y.equals(assortmentRulesVO.getPctSW())) {
					if (AntiMagicNumber.STRING_SEVENTEEN.equals(assortmentRulesVO.getId())) {
						ruleDefinationVO.setPctSW(Constants.STRING_Y);
					} else {
						if (seq == 1) {
							ruleDefinationVO.setPctSW(Constants.STRING_N);
						} else {
							ruleDefinationVO.setPctSW(Constants.STRING_Y);
						}
					}
				} else {
					ruleDefinationVO.setPctSW(Constants.STRING_N);
				}
				lstRuleDefinationVOs.add(ruleDefinationVO);
				seq++;
			}
		} else {
			ruleDefinationVO = new RuleDefinationVO();
			ruleDefinationVO.setRuleId(NumberUtils.toInt(assortmentRulesVO.getId()));
			ruleDefinationVO.setSeq_nbr(AntiMagicNumber.ONE);
			ruleDefinationVO.setActiveType(assortmentRulesVO.getActTypCd());
			ruleDefinationVO.setValType(AntiMagicNumber.STRING_TWO);
			ruleDefinationVO.setActSW(Constants.STRING_Y);
			if (!Helper.isEmpty(assortmentRulesVO.getValues())) {
				// ruleDefinationVO.setVal(assortmentRulesVO.getValues().get(0));
				if (Helper.isEmpty(assortmentRulesVO.getValues().get(0))) {
					ruleDefinationVO.setVal(AntiMagicNumber.STRING_ZERO);
				} else {
					ruleDefinationVO.setVal(assortmentRulesVO.getValues().get(0));
				}
			}
			ruleDefinationVO.setLvlId(assortmentRulesVO.getLvlId());
			ruleDefinationVO.setLogicOprtrId(AntiMagicNumber.ONE);
			ruleDefinationVO.setRuleCat1Id(AntiMagicNumber.ONE);
			ruleDefinationVO.setRuleCat2Id(AntiMagicNumber.TWO);
			ruleDefinationVO.setMathOprtrId(AntiMagicNumber.FIVE);
			ruleDefinationVO.setPctSW(assortmentRulesVO.getPctSW());
			lstRuleDefinationVOs.add(ruleDefinationVO);
		}
	}

	/**
	 * setFailReasonProductReview.
	 * @param lstProductRiewVOs
	 *            List<ProductRiewVO>
	 * @param lstProductRiewVOFails
	 *            Map<String, ProductRiewVO>
	 * @author thangdang
	 */
	public static void setFailReasonProductReview(List<ProductRiewVO> lstProductRiewVOs, Map<String, ProductRiewVO> listProductRiewVOFails) {
		for (ProductRiewVO productRiewVO : lstProductRiewVOs) {
			if (listProductRiewVOFails.containsKey(productRiewVO.getUpc())) {
				productRiewVO.setFailedReason(listProductRiewVOFails.get(productRiewVO.getUpc()).getFailedReason());
				productRiewVO.setFailedReasonCode(listProductRiewVOFails.get(productRiewVO.getUpc()).getFailedReasonCode());
				if (Helper.isEmpty(productRiewVO.getReviewStatus()) || !AConstants.RJECT.equals(productRiewVO.getReviewStatus())) {
					productRiewVO.setReviewStatus(Constants.STRING_F);
				}
			}
		}

	}
}
