/*
 * $Id: RulesDAOImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.heb.enterprise.dsv.assortment.dao.GlobalRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.RulesDAO;
import com.heb.enterprise.dsv.assortment.dao.mapper.RuleMapper;
import com.heb.enterprise.dsv.assortment.utils.AHelper;
import com.heb.enterprise.dsv.assortment.utils.DAOHelper;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.AttrAndRuleVendorVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.VendorAttributeVO;

/**
 * PricingRulesDAOImpl.
 * @author Duyen.le.
 */
@Repository
public class RulesDAOImpl extends BaseDAOImpl implements RulesDAO {
	private static final Logger LOG = Logger.getLogger(RulesDAOImpl.class);
	/**
	 * Inject GlobalRulesDAO.
	 */
	@Autowired
	private GlobalRulesDAO globalRule;
	/**
	 * get list Attributes.
	 */
	@Value("${coms.dsv.assortment.sql.getVendorAttr}")
	private String getLstAttr;

	/**
	 * get Rule for Vendor.
	 */
	@Value("${coms.dsv.asm.assortment.getRuleByVendor}")
	private String getRuleforVendorByVendID;
	/**
	 * get Rule for Vendor.
	 */
	@Value("${coms.dsv.assortment.sql.getActiveFlagSubComd}")
	private String getActiveFlagSubComd;
	/**
	 * get Rule for Vendor.
	 */
	@Value("${coms.dsv.assortment.sql.getRuleByCommodity}")
	private String getRuleByCommd;
	/**
	 * get Rule for Vendor.
	 */
	@Value("${coms.dsv.assortment.sql.getRulebySubCommodity}")
	private String getRuleBySubCommd;
	/**
	 * insertIntoVendAttr.
	 */
	@Value("${coms.dsv.assortment.sql.insertIntoVendorAttr}")
	private String insertIntoVendAttr;
	/**
	 * insertIntoVendAttr.
	 */
	@Value("${coms.dsv.assortment.sql.UpdateIntoVendorAttr}")
	private String updateVendAttr;

	@Value("${coms.dsv.assortment.sql.getLVLKeyByVendor}")
	private String getLVLKey;
	@Value("${coms.dsv.assortment.sql.getLVLKeyByCommodity}")
	private String getLVLKeyByCommodity;
	@Value("${coms.dsv.assortment.sql.getLVLKeyBySubComd}")
	private String getLVLKeyBySubComd;
	/**
	 * sqlRemoveSubCommodityActivated.
	 */
	@Value("${coms.dsv.assortment.sql.removeSubCommodityActivated}")
	private String sqlRemoveSubCommodityActivated;
	/**
	 * Update Values of Rule into RuleDfns.
	 */
	@Value("${coms.dsv.assortment.sql.updateValuesOfRuleDfns}")
	private String updateRuleValues;
	@Value("${coms.dsv.assortment.sql.updateValuesOfRuleDfnsPricingRules}")
	private String updateRuleValuesPricingRules;
	@Value("${coms.dsv.assortment.sql.getLVLKeyByCommodityArray}")
	private String sqlGetLVLKeyByCommodityArray;
	@Value("${coms.dsv.assortment.sql.getLVLKeyBySubComdArray}")
	private String sqlGetLVLKeyBySubComdArray;
	@Value("${coms.dsv.assortment.sql.getHebSuBCommodityActivated}")
	private String sqlGetHebSuBCommodityActivated;
	@Value("${coms.dsv.assortment.sql.removeListSubCommodityActivated}")
	private String sqlListRemoveSubCommodityActivated;
	@Value("${coms.dsv.assortment.sql.getAsmSubCommOverrideSql}")
	private String getAsmSubCommOverrideSql;
	/**
	 * get Exist Rules for SubDept.
	 */
	@Value("${coms.dsv.asm.VendorRule.getGlobalCommodityOverride}")
	private String getVendorCommodityOverrideSql;

	/**
	 * coms.dsv.asm.assortment.updateLvlType.
	 */
	@Value("${coms.dsv.asm.assortment.updateLvlType}")
	private String sqlUpdateLvlType;
	/**
	 * coms.dsv.asm.vendorManagement.countActiveSubComToSetFlagForCom.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countActiveSubComToSetFlagForCom}")
	private String sqlGetActiveSubCom;

	@Override
	public Map<String, AssortmentRulesVO> getRuleSettings(final String vendor, final String commod, final String subCommd, final String deptId, final String subDeptId) throws DSVException {
		Map<String, AssortmentRulesVO> rulesRs = new HashMap<String, AssortmentRulesVO>();

		List<AssortmentRulesVO> lstRule = null;
		if (!Helper.isEmpty(commod)) {
			if (Helper.isEmpty(subCommd)) {
				LOG.info("GET RULE FOR COMMODITY");
				lstRule = this.getRulesbyCommodity(vendor, commod, deptId, subDeptId);
			} else {
				LOG.info("GET RULE FOR SUB-COMMODITY");
				lstRule = this.getRulesbySubCommodity(vendor, commod, subCommd, deptId, subDeptId);
			}
		}
		if (Helper.isNormalList(lstRule)) {
			rulesRs = DAOHelper.convertColectionRuleToMap(lstRule);
		}

		return rulesRs;
	}

	/**
	 * get Rule for Vendor Settings Rule by vendorID.
	 * @param vendorID
	 *            String
	 * @return Map<String, List<AssortmentRulesVO>>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	private List<AssortmentRulesVO> getRulesbyVendor(final String vendorID) throws DSVException {
		LOG.info("getRulesbyVendor " + getRuleforVendorByVendID + " vendorID = " + vendorID);

		Map<String, AssortmentRulesVO> rulesRs = new HashMap<String, AssortmentRulesVO>();
		List<AssortmentRulesVO> lstRules = null;
		try {
			lstRules = this.getJdbcTemplate().query(this.getRuleforVendorByVendID, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement arg0) throws SQLException {
					arg0.setString(1, vendorID);
				}
			}, RuleMapper.getVendorRulesMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		if (Helper.isNormalList(lstRules)) {
			rulesRs = DAOHelper.convertColectionRuleToMap(lstRules);
		}
		lstRules = new ArrayList<AssortmentRulesVO>();
		lstRules.addAll(rulesRs.values());
		return lstRules;
	}

	/**
	 * get Rule for Vendor Settings Rule by vendorID.
	 * @param vendorID
	 *            String
	 * @return Map<String, List<AssortmentRulesVO>>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	private List<AssortmentRulesVO> getRulesbyCommodity(final String vendorID, final String commodity, final String deptId, final String subDeptId) throws DSVException {
		LOG.info("getRulesbyCommodity" + getRuleByCommd + " vendorID = " + vendorID + " commodity= " + commodity + " deptId= " + deptId + " subDeptId= " + subDeptId);
		Map<String, AssortmentRulesVO> rulesRs = new HashMap<String, AssortmentRulesVO>();
		List<AssortmentRulesVO> lstRules = null;
		try {
			lstRules = this.getJdbcTemplate().query(this.getRuleByCommd, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement arg0) throws SQLException {
					arg0.setString(AntiMagicNumber.ONE, vendorID);
					arg0.setString(AntiMagicNumber.TWO, commodity);
					arg0.setString(AntiMagicNumber.THREE, vendorID);
					arg0.setString(AntiMagicNumber.FOUR, commodity);
					arg0.setString(AntiMagicNumber.FIVE, deptId);
					arg0.setString(AntiMagicNumber.SIX, subDeptId);
					arg0.setString(AntiMagicNumber.SEVEN, deptId);
					arg0.setString(AntiMagicNumber.EIGHT, subDeptId);
				}
			}, RuleMapper.getVendorRulesMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		if (Helper.isNormalList(lstRules)) {
			rulesRs = DAOHelper.convertColectionRuleToMap(lstRules);
		}
		lstRules = new ArrayList<AssortmentRulesVO>();
		lstRules.addAll(rulesRs.values());
		return lstRules;
	}

	/**
	 * get Rule for Vendor Settings Rule by vendorID.
	 * @param vendorID
	 *            String
	 * @return Map<String, List<AssortmentRulesVO>>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	private List<AssortmentRulesVO> getRulesbySubCommodity(final String vendorID, final String commodity, final String subCommodity, final String deptId, final String subDeptId) throws DSVException {
		LOG.info("getRulesbySubCommodity" + getRuleBySubCommd + " vendorID = " + vendorID + " subCommodity= " + subCommodity + " commodity= " + commodity + " deptId= " + deptId + " subDeptId= "
				+ subDeptId);

		Map<String, AssortmentRulesVO> rulesRs = new HashMap<String, AssortmentRulesVO>();
		List<AssortmentRulesVO> lstRules = null;
		try {
			lstRules = this.getJdbcTemplate().query(this.getRuleBySubCommd, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement arg0) throws SQLException {
					arg0.setString(AntiMagicNumber.ONE, vendorID);
					arg0.setString(AntiMagicNumber.TWO, subCommodity);
					arg0.setString(AntiMagicNumber.THREE, vendorID);
					arg0.setString(AntiMagicNumber.FOUR, commodity);
					arg0.setString(AntiMagicNumber.FIVE, vendorID);
					arg0.setString(AntiMagicNumber.SIX, commodity);
					arg0.setString(AntiMagicNumber.SEVEN, deptId);
					arg0.setString(AntiMagicNumber.EIGHT, subDeptId);
					arg0.setString(AntiMagicNumber.NINE, deptId);
					arg0.setString(AntiMagicNumber.TEN, subDeptId);
				}
			}, RuleMapper.getVendorRulesBySubComdMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		if (Helper.isNormalList(lstRules)) {
			rulesRs = DAOHelper.convertColectionRuleToMap(lstRules);
		}
		lstRules = new ArrayList<AssortmentRulesVO>();
		lstRules.addAll(rulesRs.values());
		return lstRules;
	}

	/**
	 * get list of Vendor Attributes and Rule for Vendor rule by VendorID.
	 * @param vendorID
	 *            String
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	@Override
	public AttrAndRuleVendorVO getRuleSettingsByVendor(final String vendorID) throws DSVException {
		AttrAndRuleVendorVO attr = new AttrAndRuleVendorVO();
		// get attr
		List<VendorAttributeVO> lstAttr = new ArrayList<VendorAttributeVO>();
		// try {
		// lstAttr = this.getJdbcTemplate().query(this.getLstAttr, new PreparedStatementSetter() {
		// @Override
		// public void setValues(PreparedStatement arg0) throws SQLException {
		// arg0.setString(1, vendorID);
		// }
		// }, RuleMapper.getVendorAttrMapper());
		// } catch (DataAccessException e) {
		// LOG.error(e.getMessage(), e);
		// throw new DSVException(e);
		// }
		// get rules
		List<AssortmentRulesVO> lstRules = this.getRulesbyVendor(vendorID);
		attr.setLstVendorAttrs(lstAttr);
		attr.setLstVendorRules(lstRules);
		return attr;
	}

	@Override
	public long checkLVL(final String vendorId, final String commodityId, final String subCommodityId) throws DSVException {
		long lvlId = 0;
		List<Long> lstLvls = null;
		try {
			if (!Helper.isEmpty(vendorId)) {
				if (Helper.isEmpty(commodityId)) {
					if (Helper.isEmpty(subCommodityId)) {
						LOG.info("Save Rule for Vendor!!!!");
						lstLvls = this.getJdbcTemplate().query(this.getLVLKey, new PreparedStatementSetter() {
							@Override
							public void setValues(PreparedStatement ps) throws SQLException {
								ps.setLong(1, Long.parseLong(vendorId.trim()));
							}

						}, RuleMapper.getLvlIdMapper());
					} else {
						LOG.info("Save Rule for Sub-Commodity!!!!");
						lstLvls = this.getJdbcTemplate().query(this.getLVLKeyBySubComd, new PreparedStatementSetter() {
							@Override
							public void setValues(PreparedStatement ps) throws SQLException {
								ps.setLong(1, Long.parseLong(vendorId.trim()));
								ps.setLong(2, Long.parseLong(subCommodityId.trim()));
							}
						}, RuleMapper.getLvlIdMapper());
					}

				} else {
					LOG.info("Save Rule for Commodity!!!!");
					lstLvls = this.getJdbcTemplate().query(this.getLVLKeyByCommodity, new PreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement ps) throws SQLException {
							ps.setLong(1, Long.parseLong(vendorId.trim()));
							ps.setLong(2, Long.parseLong(commodityId.trim()));
						}

					}, RuleMapper.getLvlIdMapper());

				}
			}
			if (!Helper.isEmpty(lstLvls)) {
				lvlId = lstLvls.get(0);
			}
			LOG.info("checkLVL method have lvlID :" + lvlId);
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}

		return lvlId;
	}

	/**
	 * Update Rule to Dfns Table.
	 * @param lstRulesDfns
	 *            List<RuleDefinationVO>
	 * @param lvlID
	 *            long
	 * @param vendorAsmVo
	 *            AttrAndRuleVendorVO
	 * @return boolean
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	@Override
	public String updateRuleValuesToDfns(final List<RuleDefinationVO> lstRulesDfns, final long lvlId, final String userId) throws DSVException {
		LOG.info("########## updateRuleValuesToDfns ##");
		for (RuleDefinationVO r : lstRulesDfns) {
			LOG.info(r.toString());
		}
		String returnStr = Constants.SUCCESSFULLY;
		this.getJdbcTemplate().batchUpdate(this.updateRuleValues, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Date date = new Date();
				RuleDefinationVO rule = lstRulesDfns.get(i);
				ps.setString(AntiMagicNumber.ONE, rule.getVal());
				ps.setString(AntiMagicNumber.TWO, rule.getActSW());
				ps.setString(AntiMagicNumber.THREE, rule.getActiveType());
				ps.setString(AntiMagicNumber.FOUR, userId);
				ps.setTimestamp(AntiMagicNumber.FIVE, new Timestamp(date.getTime()));
				ps.setLong(AntiMagicNumber.SIX, rule.getRuleId());
				ps.setLong(AntiMagicNumber.SEVEN, lvlId);
				ps.setInt(AntiMagicNumber.EIGHT, rule.getSeq_nbr());
			}

			@Override
			public int getBatchSize() {
				return lstRulesDfns.size();
			}
		});
		return returnStr;
	}

	@Override
	public String updateRuleValuesToDfnsPricingRules(final List<RuleDefinationVO> lstRulesDfns, final long lvlId, final String userId) throws DSVException {
		LOG.info("########## updateRuleValuesToDfns ##");
		for (RuleDefinationVO r : lstRulesDfns) {
			LOG.info(r.toString());
		}
		String returnStr = Constants.SUCCESSFULLY;
		this.getJdbcTemplate().batchUpdate(this.updateRuleValuesPricingRules, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Date date = new Date();
				RuleDefinationVO rule = lstRulesDfns.get(i);
				ps.setString(AntiMagicNumber.ONE, rule.getVal());
				ps.setString(AntiMagicNumber.TWO, rule.getActSW());
				ps.setString(AntiMagicNumber.THREE, rule.getActiveType());
				ps.setString(AntiMagicNumber.FOUR, userId);
				ps.setTimestamp(AntiMagicNumber.FIVE, new Timestamp(date.getTime()));
				ps.setString(AntiMagicNumber.SIX, rule.getPctSW());
				ps.setLong(AntiMagicNumber.SEVEN, rule.getRuleId());
				ps.setLong(AntiMagicNumber.EIGHT, lvlId);
				ps.setInt(AntiMagicNumber.NINE, rule.getSeq_nbr());
			}

			@Override
			public int getBatchSize() {
				return lstRulesDfns.size();
			}
		});
		return returnStr;
	}

	/**
	 * get list of Vendor Attributes.
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	@Override
	public List<VendorAttributeVO> getVendorAttr() throws DSVException {
		List<VendorAttributeVO> lstRs = null;
		try {
			lstRs = this.getJdbcTemplate().query(this.getLstAttr, RuleMapper.getAttrMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstRs;
	}

	/**
	 * get Active Switch for subComd.
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	@Override
	public List<Integer> getActiveSw(final String vendorId) throws DSVException {
		List<Integer> lstHebComd = null;
		try {
			lstHebComd = this.getJdbcTemplate().query(this.getActiveFlagSubComd, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement arg0) throws SQLException {
					arg0.setString(1, vendorId);
				}
			}, RuleMapper.getHebComdMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstHebComd;
	}

	/**
	 * Update Vendor Attrs.
	 * @param lstVendorAttrs
	 *            List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	@Override
	public boolean updateVendorAttr(final List<VendorAttributeVO> lstVendorAttrs, final String vendor) throws DSVException {
		List<VendorAttributeVO> lstUpdate = new ArrayList<VendorAttributeVO>();
		List<VendorAttributeVO> lstNew = new ArrayList<VendorAttributeVO>();
		String sql = "SELECT Attr_Id FROM Vend_Attr  Where Vend_Id = ? and Attr_Id in (9,10,11,12,13)";
		List<Integer> lstAttr = null;
		try {
			lstAttr = this.getJdbcTemplate().query(sql, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement arg0) throws SQLException {
					arg0.setString(1, vendor);
				}
			}, RuleMapper.getListExistAttrMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		if (Helper.isNormalList(lstAttr)) {
			for (VendorAttributeVO vo : lstVendorAttrs) {
				if (lstAttr.contains(vo.getId())) {
					lstUpdate.add(vo);
				} else {
					lstNew.add(vo);
				}
			}
			if (Helper.isNormalList(lstNew)) {
				this.insertIntoVendAttr(lstNew, vendor);
			}
			if (Helper.isNormalList(lstUpdate)) {
				this.updateVendAttr(lstUpdate, vendor);
			}
		} else {
			this.insertIntoVendAttr(lstVendorAttrs, vendor);
		}
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * insert Vendor Attrs.
	 * @param vendor
	 *            String
	 * @param lstVendorAttrs
	 *            List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	private void insertIntoVendAttr(final List<VendorAttributeVO> lstVendorAttrs, final String vendor) throws DSVException {
		LOG.info("Insert Into Vendor Attribute!!!!!!!");
		this.getJdbcTemplate().batchUpdate(this.insertIntoVendAttr, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				VendorAttributeVO rule = lstVendorAttrs.get(i);
				ps.setString(1, vendor);
				ps.setInt(2, rule.getId());
				ps.setString(3, rule.getValue());
			}

			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return lstVendorAttrs.size();

			}

		});
	}

	/**
	 * Update Vendor Attrs.
	 * @param vendor
	 *            String
	 * @param lstVendorAttrs
	 *            List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	private void updateVendAttr(final List<VendorAttributeVO> lstVendorAttrs, final String vendor) throws DSVException {
		LOG.info("Update Into Vendor Attribute!!!!!!!");
		this.getJdbcTemplate().batchUpdate(this.updateVendAttr, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				VendorAttributeVO rule = lstVendorAttrs.get(i);
				ps.setString(AntiMagicNumber.ONE, rule.getValue());
				ps.setInt(AntiMagicNumber.TWO, rule.getId());
				ps.setString(AntiMagicNumber.THREE, vendor);
			}

			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return lstVendorAttrs.size();

			}

		});
	}

	@Override
	public boolean removeSubCommodityActivated(final String vendorId) {
		LOG.info("removeSubCommodityActivated vendorId=" + vendorId + " - " + this.sqlRemoveSubCommodityActivated);
		boolean flag = false;
		try {
			this.getJdbcTemplate().update(this.sqlRemoveSubCommodityActivated, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(AntiMagicNumber.ONE, vendorId);

				}
			});
			flag = true;
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return flag;
	}

	@Override
	public List<Long> getLvlByCommodityVendor(final String vendorId, final List<String> lstCommoditys) {
		LOG.info("getLvlByCommodityVendor vendorId=" + vendorId + "- lstCommoditys=" + lstCommoditys);
		List<Long> lstLvls = null;
		String query = this.sqlGetLVLKeyByCommodityArray;
		query = String.format(query, AHelper.convertListToString(lstCommoditys));
		LOG.info(query);
		try {
			lstLvls = this.getJdbcTemplate().query(query, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(AntiMagicNumber.ONE, vendorId);
				}

			}, RuleMapper.getLvlIdMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lstLvls;
	}

	@Override
	public List<Long> getLvlBySubCommodityVendor(final String vendorId, final List<String> lstSubCommoditys) {
		LOG.info("getLvlBySubCommodityVendor vendorId=" + vendorId + "- lstCommoditys=" + lstSubCommoditys);
		List<Long> lstLvls = null;
		String query = this.sqlGetLVLKeyBySubComdArray;
		query = String.format(query, AHelper.convertListToString(lstSubCommoditys));
		LOG.info(query);
		try {
			lstLvls = this.getJdbcTemplate().query(query, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(AntiMagicNumber.ONE, vendorId);
				}

			}, RuleMapper.getLvlIdMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		LOG.info("########### lstLvls =" + lstLvls);
		return lstLvls;
	}

	@Override
	public List<String> getSubCommodityActivated(final String vendorId) {
		LOG.info("getSubCommodityActivated vendorId=" + vendorId);
		List<String> lstHebSubCommodity = null;
		try {
			lstHebSubCommodity = this.getJdbcTemplate().query(sqlGetHebSuBCommodityActivated, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(AntiMagicNumber.ONE, vendorId);
				}

			}, new RowMapper<String>() {
				@Override
				public String mapRow(ResultSet rs, int arg1) throws SQLException {
					String hebSubComm = rs.getString("HEB_SUB_COM_ID");
					return hebSubComm;
				}
			});
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		LOG.info("lstHebSubCommodity lstHebSubCommodity=" + lstHebSubCommodity);
		return lstHebSubCommodity;
	}

	@Override
	public boolean removeSubCommodityActivated(final String vendorId, final List<String> lstSubComms) {
		LOG.info("removeSubCommodityActivated vendorId=" + vendorId + " -- lstSubComms =" + lstSubComms);
		boolean flag = false;
		String query = this.sqlListRemoveSubCommodityActivated;
		query = String.format(query, AHelper.convertListToString(lstSubComms));
		LOG.info(query);
		try {
			this.getJdbcTemplate().update(query, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(AntiMagicNumber.ONE, vendorId);
				}
			});
			flag = true;
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return flag;
	}

	/**
	 * getASMCommodityOverride.
	 * @param vendorId
	 *            String
	 * @param lvlType
	 *            String
	 * @return List<LevelVO>
	 * @author duyen.le.
	 */
	@Override
	public List<Integer> getAsmLVLModified(String lvlType, final String vendorId) {
		List<Integer> lstLVL = null;
		if ("comd".equals(lvlType)) {
			lstLVL = this.getJdbcTemplate().query(
					this.getVendorCommodityOverrideSql, new PreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement ps) throws SQLException {
							ps.setString(AntiMagicNumber.ONE, vendorId);
						}
					}, RuleMapper.getCommodityModifiedMapper()
					);
		} else {
			lstLVL = this.getJdbcTemplate().query(
					this.getAsmSubCommOverrideSql, new PreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement ps) throws SQLException {
							ps.setString(AntiMagicNumber.ONE, vendorId);
						}
					}, RuleMapper.getHebComdMapper()
					);
		}
		return lstLVL;
	}

	@Override
	public String updatelvlTyp(final long lvlId, final String lvlTyp) throws DSVException {
		this.LOG.info("updatelvlTyp lvlId=" + lvlId + "- lvlTyp= " + lvlTyp + " -- " + this.sqlUpdateLvlType);
		String mess = Constants.SUCCESSFULLY;
		this.getJdbcTemplate().update(this.sqlUpdateLvlType, new PreparedStatementSetter() {
			Date date = new Date();

			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(AntiMagicNumber.ONE, lvlTyp);
				ps.setTimestamp(AntiMagicNumber.TWO, new Timestamp(date.getTime()));
				ps.setLong(AntiMagicNumber.THREE, lvlId);
			}
		});
		return mess;
	}

	/**
	 * getLstSubActivateBySub.
	 * @return List<LevelVO>
	 * @author Duyen.le
	 */

	@Override
	public List<LevelVO> getLstSubActivateBySub() throws DSVException {
		// TODO Auto-generated method stub
		LOG.info("get list activate SubCom of Com");
		List<LevelVO> lstSubCom = null;
		try {
			lstSubCom = this.getJdbcTemplate().query(this.sqlGetActiveSubCom, RuleMapper.getLstActivateSubComMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		LOG.info("leave method rs: " + lstSubCom.size());
		return lstSubCom;
	}
}
