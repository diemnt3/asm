/*
 * $Id: ReviewNewProductsDAO.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao;

import java.util.List;
import java.util.Map;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;

/**
 * @author Duyen.le
 */
public interface ProductsDAO {
	/**
	 * search product for review.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author thangdang
	 * @return Page<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 */
	Page<ProductRiewVO> searchForProducts(ProductSearchCriteriaVO productSearchCriteriaVO) throws DSVException;

	/**
	 * Reject Products after reviewing.
	 * @param lstUpcs
	 *            List<String>
	 * @param userId
	 *            String
	 * @author Duyen.Le
	 * @return String
	 */
	String rejectProducts(List<String> lstUpcs, String userId);

	/**
	 * approveProducts.
	 * @param lstUpcs
	 *            List<String>
	 * @param userId
	 *            String
	 * @author Duyen.Le
	 * @return String
	 */
	String approveProducts(List<String> lstUpcs, String userId);

	/**
	 * approveProducts.
	 * @param lstUpcs
	 *            List<String>
	 * @param userId
	 *            String
	 * @author Duyen.Le
	 * @return String
	 */
	String updateProducts(List<ProductRiewVO> lstUpcs);

	/**
	 * countProudctRev.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author thangdang
	 * @return Page<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 */
	List<ProductRiewVO> countProudctRev(ProductSearchCriteriaVO productSearchCriteriaVO) throws DSVException;

	/**
	 * countProudctFail.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author thangdang
	 * @return int
	 */
	int countProudctFail(ProductSearchCriteriaVO productSearchCriteriaVO);

	/**
	 * getFailReason.
	 * @param lstProdScn
	 *            List<String>
	 * @author thangdang
	 * @return Map<String, ProductRiewVO>
	 */

	Map<String, ProductRiewVO> getFailReason(List<String> lstProdScn);

	/**
	 * UnReject Products after reviewing.
	 * @param lstUpcs
	 *            List<String>
	 * @param userId
	 *            String
	 * @author Duyen.Le
	 * @return String
	 */
	String unRejectProducts(List<String> lstUpcs, String userId);

	/**
	 * checkApproveReject.
	 * @param lstUpcs
	 *            List<String>
	 * @author thangdang
	 * @return boolean
	 */
	boolean checkApproveReject(List<String> lstUpcs);

	/**
	 * checkUnReject.
	 * @param lstUpcs
	 *            List<String>
	 * @author thangdang
	 * @return boolean
	 */
	boolean checkUnReject(List<String> lstUpcs);

	/**
	 * export products to Excel.
	 * @param prdSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author Duyen.le
	 * @return List<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 */
	List<ProductRiewVO> exportExcelForProducts(ProductSearchCriteriaVO prdSearchCriteriaVO) throws DSVException;

	/**
	 * getAllProductInfo.
	 * @return List<ProductRiewVO>
	 */
	List<ProductRiewVO> getAllProductInfo();

	/**
	 * getListFailedReasons - get List of Failed Reason.
	 * @author mrh.diemnguyen
	 * @return List<BaseVO>
	 * @throws DSVException
	 */
	List<BaseVO> getListFailedReasons() throws DSVException;
}
