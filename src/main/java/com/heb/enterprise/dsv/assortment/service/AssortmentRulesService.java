/*
 * $Id: AssortmentRulesService.java,v 1.0 2014/03/13 11:08:03 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.util.Map;

import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ProductVO;

/**
 * @author anhtran.
 */
public interface AssortmentRulesService {
	/**
	 * getRuleProduct.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>.
	 * @author anhtran.
	 */
	Map<String, AssortmentRulesVO> getRuleProduct(ProductVO product);
}
