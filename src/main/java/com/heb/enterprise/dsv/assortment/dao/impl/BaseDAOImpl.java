/*
 * $Id: BaseDAOImpl.java,v 1.11 2015/03/12 11:19:22 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

/**
 * BaseDAOImpl class.
 * @author duyen.le
 */
public class BaseDAOImpl {
	/**
	 * jdbcTemplate.
	 */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private DataSourceTransactionManager transactionManager;

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}

	@Autowired
	public void setJdbcTemplate(@Qualifier("jdbcTemplate") JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * @return the transactionManager
	 */
	public DataSourceTransactionManager getTransactionManager() {
		return this.transactionManager;
	}

	@Autowired
	public void setTransactionManager(@Qualifier("transactionManager") DataSourceTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}
}