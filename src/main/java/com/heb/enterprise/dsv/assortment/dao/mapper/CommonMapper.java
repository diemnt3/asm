/*
 * $Id.: CommonMapper.java, Oct 28, 2014$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.heb.enterprise.dsv.assortment.utils.AConstants;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * CommonMapper class.
 * @author Duyen.le
 */
public final class CommonMapper {
	/**
	 * constructor method.
	 */
	private CommonMapper() {

	}

	/**
	 * getInvoiceStatusMapper - find all Invoice Status.
	 * @author Duyen.le
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<BaseVO> getLogicRuleMapper() {
		RowMapper<BaseVO> logicRulesMapper = new RowMapper<BaseVO>() {
			@Override
			public BaseVO mapRow(ResultSet rs, int arg1) throws SQLException {
				BaseVO logicRule = new BaseVO();
				logicRule.setId(rs.getString("LOGIC_OPRTR_ID"));
				logicRule.setName(rs.getString("LOGIC_RULE_OPRTR_TXT"));
				return logicRule;
			}
		};
		return logicRulesMapper;
	}

	/**
	 * Retrieves a Rules set by Rule Group.
	 * @return List<AssortmentRulesVO> : List of Rules set are retrieved from Database.
	 * @author Duyen.le
	 */
	public static RowMapper<AssortmentRulesVO> getRuleByRuleGroupMapper() {
		RowMapper<AssortmentRulesVO> rulesMapper = new RowMapper<AssortmentRulesVO>() {
			@Override
			public AssortmentRulesVO mapRow(ResultSet rs, int arg1) throws SQLException {
				AssortmentRulesVO rule = new AssortmentRulesVO();
				rule.setId(rs.getString(AConstants.RULE_ID));
				rule.setName(rs.getString(AConstants.RULE_NM));
				rule.setDesc(rs.getString("RULE_DES"));
				return rule;
			}
		};
		return rulesMapper;
	}

	/**
	 * Retrieves a Rules set by Rule Group.
	 * @return List<AssortmentRulesVO> : List of Rules set are retrieved from Database.
	 * @author thangdang
	 */
	public static RowMapper<BaseVO> getAllRuleMapper() {
		RowMapper<BaseVO> rulesMapper = new RowMapper<BaseVO>() {
			@Override
			public BaseVO mapRow(ResultSet rs, int arg1) throws SQLException {
				BaseVO rule = new BaseVO();
				rule.setId(rs.getString(AConstants.RULE_ID));
				rule.setName(rs.getString(AConstants.RULE_NM));
				return rule;
			}
		};
		return rulesMapper;
	}

	/**
	 * Retrieves a Failed Reasons set by Rule Group.
	 * @return List<BaseVO> : List of Failed Reasons set are retrieved from Database.
	 * @author mrh.diemnguyen
	 */
	public static RowMapper<BaseVO> getAllFailedReasonsMapper() {
		RowMapper<BaseVO> rulesMapper = new RowMapper<BaseVO>() {
			@Override
			public BaseVO mapRow(ResultSet rs, int arg1) throws SQLException {
				BaseVO rule = new BaseVO();
				rule.setId(rs.getString(AConstants.ERR_TYP_CD));
				rule.setName(rs.getString(AConstants.ERR_TYP_DES));
				return rule;
			}
		};
		return rulesMapper;
	}

	/**
	 * Retrieves a Rules set by Rule Group.
	 * @return List<VendorInfoVO> : List of Vendors set are retrieved from Database.
	 * @author Duyen.le
	 */
	public static RowMapper<VendorInfoVO> getAllVendorMapper() {
		RowMapper<VendorInfoVO> vendorMapper = new RowMapper<VendorInfoVO>() {
			@Override
			public VendorInfoVO mapRow(ResultSet rs, int arg1) throws SQLException {
				VendorInfoVO vendor = new VendorInfoVO();
				vendor.setVendorId(rs.getString(AConstants.VEND_ID));
				vendor.setVendorName(rs.getString(AConstants.VEND_NM));
				return vendor;
			}
		};
		return vendorMapper;
	}
}
