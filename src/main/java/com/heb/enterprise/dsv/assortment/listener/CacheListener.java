/*
 * $Id: CacheListener.java,v 1.11 2015/03/21 09:10:11 vn44178 Exp $.
 *
 * Copyright (c) 2012 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.exception.DSVException;

/**
 * @author duyen.le
 */
public class CacheListener implements HttpSessionListener, ServletContextListener {
	private static final Logger LOG = Logger.getLogger(CacheListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}

	/**
	 * cache data when start tomcat server.
	 * @param servletContextEvent
	 *            ServletContextEvent
	 * @author thangdang
	 */
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(
				servletContextEvent.getServletContext());
		CacheService cacheService = (CacheService) ctx.getBean("cacheService");
		try {
			cacheService.refreshCaches();
		} catch (DSVException e) {
			// TODO Auto-generated catch block
			LOG.error(e.getMessage(), e);
		}
	}
}
