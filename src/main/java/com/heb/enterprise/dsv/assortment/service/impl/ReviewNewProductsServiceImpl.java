/*
 * $Id: NewProductsReviewServiceImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.dao.ProductsDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.ReviewNewProductsService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * NewProductsReviewServiceImpl interface.
 * @author duyen.le
 */
@Service
public class ReviewNewProductsServiceImpl implements ReviewNewProductsService {
	private static final Logger LOG = Logger.getLogger(ReviewNewProductsServiceImpl.class);
	/**
	 * inject CacheService.
	 */
	@Autowired
	private CacheService cacheService;
	/**
	 * inject ReviewNewProductsDAO.
	 */
	@Autowired
	private ProductsDAO productsDAO;

	/**
	 * getBrands - get List of Brands.
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	@Override
	public List<BaseVO> getListBrands() {
		return this.cacheService.getBrandsFromCache();
	}

	/**
	 * getListCommodities - get List of Commodities.
	 * @author Duyen.Le
	 * @return List<CommodityVO>
	 * @throws DSVException
	 *             DSVException.
	 */
	@Override
	public List<CommodityVO> getListCommodities() throws DSVException {
		return this.cacheService.getCommodityList();
	}

	/**
	 * getListVendors - get List of Vendors.
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	@Override
	public List<VendorInfoVO> getListVendors() {
		return this.cacheService.getAllListVendors();
	}

	@Override
	public Page<ProductRiewVO> searchForProducts(ProductSearchCriteriaVO productSearchCriteriaVO) throws DSVException {
		return this.productsDAO.searchForProducts(productSearchCriteriaVO);
	}

	@Override
	public List<ProductRiewVO> countProudctRev(ProductSearchCriteriaVO productSearchCriteriaVO) throws DSVException {
		List<ProductRiewVO> lstProductRiewVOs = this.productsDAO.countProudctRev(productSearchCriteriaVO);
		List<ProductRiewVO> lstProductRiewVOReturns = new ArrayList<ProductRiewVO>();
		ProductRiewVO productRiewVO = null;
		if (!Helper.isEmpty(lstProductRiewVOs)) {
			int countProductFails = 0;
			int countProductPass = 0;
			for (ProductRiewVO productRiewVOTmp : lstProductRiewVOs) {
				if (Constants.ALL_UPPERCASE_STR.equals(productRiewVOTmp.getReviewStatus()) && productRiewVOTmp.getTotalRow() > 0) {
					countProductFails = this.productsDAO.countProudctFail(productSearchCriteriaVO);
					if (productRiewVOTmp.getTotalRow() > countProductFails) {
						countProductPass = productRiewVOTmp.getTotalRow() - countProductFails;
					}

				} else {
					lstProductRiewVOReturns.add(productRiewVOTmp);
				}
			}
			productRiewVO = new ProductRiewVO();
			productRiewVO.setReviewStatus(Constants.STRING_P);
			productRiewVO.setTotalRow(countProductPass);
			lstProductRiewVOReturns.add(productRiewVO);
			productRiewVO = new ProductRiewVO();
			productRiewVO.setReviewStatus(Constants.STRING_F);
			productRiewVO.setTotalRow(countProductFails);
			lstProductRiewVOReturns.add(productRiewVO);
		}
		return lstProductRiewVOReturns;
	}

	@Override
	public String rejectProducts(List<String> lstUpcs, String userId) {
		return this.productsDAO.rejectProducts(lstUpcs, userId);
	}

	@Override
	public String approveProducts(List<String> lstUpcs, String userId) {
		return this.productsDAO.approveProducts(lstUpcs, userId);
	}

	@Override
	public String updateProducts(List<ProductRiewVO> lstProds) {
		return this.productsDAO.updateProducts(lstProds);
	}

	@Override
	public String unRejectProducts(List<String> lstUpcs, String userId) {
		return this.productsDAO.unRejectProducts(lstUpcs, userId);
	}

	@Override
	public boolean checkApproveReject(List<String> lstUpcs) {
		return this.productsDAO.checkApproveReject(lstUpcs);
	}

	@Override
	public boolean checkUnReject(List<String> lstUpcs) {
		return this.productsDAO.checkUnReject(lstUpcs);
	}

	/**
	 * search product for review.
	 * @param prdSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author Duyen.le
	 * @return List<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 */
	@Override
	public List<ProductRiewVO> exportExcelForProducts(ProductSearchCriteriaVO prdSearchCriteriaVO) throws DSVException {
		return this.productsDAO.exportExcelForProducts(prdSearchCriteriaVO);
	}

	/**
	 * getListFailedReasons - get List of Failed Reason.
	 * @author mrh.diemnguyen
	 * @return List<BaseVO>
	 * @throws DSVException
	 */
	@Override
	public List<BaseVO> getListFailedReasons() throws DSVException {
		return this.productsDAO.getListFailedReasons();
	}

}
