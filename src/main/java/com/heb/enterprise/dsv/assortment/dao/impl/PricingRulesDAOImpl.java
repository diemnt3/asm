/*
 * $Id: PricingRulesDAOImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.heb.enterprise.dsv.assortment.dao.PricingRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.mapper.RuleMapper;
import com.heb.enterprise.dsv.assortment.utils.AConstants;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;

/**
 * PricingRulesDAOImpl.
 * @author quangmai
 */
@Repository
@EnableTransactionManagement
public class PricingRulesDAOImpl extends BaseDAOImpl implements PricingRulesDAO {
	private static final Logger LOG = Logger.getLogger(GlobalRulesDAOImpl.class);
	/**
	 * getRulesByVendor.
	 */
	@Value("${coms.dsv.asm.pricing.getRulesByVendor}")
	private String sqlGetRulesByVendor;
	/**
	 * getRulesByCommodity.
	 */
	@Value("${coms.dsv.asm.pricing.getRulesByCommodity}")
	private String sqlGetRulesByCommodity;

	/**
	 * getRulesBySubCommodity.
	 */
	@Value("${coms.dsv.asm.pricing.getRulesBySubCommodity}")
	private String sqlGetRulesBySubCommodity;

	/**
	 * sqlGetCmptrsByVendor.
	 */
	@Value("${coms.dsv.asm.pricing.getCmptrsByVendor}")
	private String sqlGetCmptrsByVendor;
	/**
	 * sqlGetCmptrsByCommodity.
	 */
	@Value("${coms.dsv.asm.pricing.getCmptrsByCommodity}")
	private String sqlGetCmptrsByCommodity;

	/**
	 * sqlGetCmptrsBySubCommodity.
	 */
	@Value("${coms.dsv.asm.pricing.getCmptrsBySubCommodity}")
	private String sqlGetCmptrsBySubCommodity;

	/**
	 * Delete CMPTR_LVL table.
	 */
	@Value("${coms.dsv.asm.pricing.deleteCmptrsByLvl}")
	private String sqlDeleteCmptrsByLvl;

	/**
	 * Insert new row to CMPTR_LVL table.
	 */
	@Value("${coms.dsv.asm.pricing.inserCmptrs}")
	private String sqlInserCmptrs;
	/**
	 * sqlGetLVLKeyByVendor.
	 */
	@Value("${coms.dsv.asm.pricing.getLVLKeyByVendor}")
	private String sqlGetLVLKeyByVendor;

	/**
	 * sqlGetLVLKeyByCommodity.
	 */
	@Value("${coms.dsv.asm.pricing.getLVLKeyByCommodity}")
	private String sqlGetLVLKeyByCommodity;

	/**
	 * sqlGetLVLKeyByCommoditySubCommodity.
	 */
	@Value("${coms.dsv.asm.pricing.getLVLKeyByCommoditySubCommodity}")
	private String sqlGetLVLKeyByCommoditySubCommodity;

	/**
	 * sqlGetActTyp.
	 */
	@Value("${coms.dsv.asm.pricing.getActTyp}")
	private String sqlGetActTyp;

	@Override
	public List<AssortmentRulesVO> getPricingRuleByVendor(final String vendorId) {
		List<AssortmentRulesVO> lstRules = null;
		try {
			lstRules = this.getJdbcTemplate().query(this.sqlGetRulesByVendor, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement arg0) throws SQLException {
					arg0.setString(AntiMagicNumber.ONE, vendorId);
				}
			}, RuleMapper.getPricingRulesMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lstRules;

	}

	/**
	 * getPricingRuleByCommodity.
	 * @param commodity
	 *            :String
	 * @return List<AssortmentRulesVO>
	 */
	@Override
	public List<AssortmentRulesVO> getPricingRuleByCommodity(final String commodity) {
		LOG.info("getPricingRuleByCommoditySubCommodity - Just only for commodity");
		List<AssortmentRulesVO> lstRules = null;
		try {
			lstRules = this.getJdbcTemplate().query(this.sqlGetRulesByCommodity, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement arg0) throws SQLException {
					arg0.setString(AntiMagicNumber.ONE, commodity);
				}
			}, RuleMapper.getPricingRulesMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lstRules;
	}

	/**
	 * getPricingRuleByCommoditySubCommodity.
	 * @param commodity
	 *            :String
	 * @param subComodity
	 *            :String
	 * @return List<AssortmentRulesVO>
	 */
	@Override
	public List<AssortmentRulesVO> getPricingRuleByCommoditySubCommodity(final String commodity, final String subComodity) {
		List<AssortmentRulesVO> lstRules = null;
		try {
			lstRules = this.getJdbcTemplate().query(this.sqlGetRulesBySubCommodity,
					new PreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement arg0) throws SQLException {
							arg0.setString(AntiMagicNumber.ONE, commodity);
							arg0.setString(AntiMagicNumber.TWO, subComodity);
							arg0.setString(AntiMagicNumber.THREE, commodity);
						}
					}, RuleMapper.getPricingRulesMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		if (Helper.isEmpty(lstRules)) {
			LOG.info("Search based on commodity + subcommodity is not found");
			lstRules = this.getPricingRuleByCommodity(commodity);
		}
		return lstRules;
	}

	/**
	 * getPricingCompetitorByVendor.
	 * @param vendorId
	 *            :String
	 * @return List<String>
	 */
	@Override
	public List<String> getPricingCompetitorByVendor(final String vendorId) {
		List<String> lstCompetitors = null;
		try {
			lstCompetitors = this.getJdbcTemplate().query(this.sqlGetCmptrsByVendor, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement arg0) throws SQLException {
					arg0.setString(AntiMagicNumber.ONE, vendorId);
				}
			}, new RowMapper<String>() {
				@Override
				public String mapRow(ResultSet rs, int arg1) throws SQLException {
					String compareId = rs.getString(AConstants.CMPTR_ID);
					return compareId;
				}
			});
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lstCompetitors;
	}

	/**
	 * getPricingCompetitorByCommodity.
	 * @param commodity
	 *            :String
	 * @return List<String>
	 */
	@Override
	public List<String> getPricingCompetitorByCommodity(final String commodity) {
		List<String> lstCompetitors = null;
		try {
			lstCompetitors = this.getJdbcTemplate().query(this.sqlGetCmptrsByCommodity,
					new PreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement arg0) throws SQLException {
							arg0.setString(AntiMagicNumber.ONE, commodity);
						}
					}, new RowMapper<String>() {
						@Override
						public String mapRow(ResultSet rs, int arg1) throws SQLException {
							String compareId = rs.getString(AConstants.CMPTR_ID);
							return compareId;
						}
					});
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lstCompetitors;
	}

	/**
	 * getPricingCompetitorByCommoditySubCommodity.
	 * @param commodity
	 *            :String
	 * @param subComodity
	 *            :String
	 * @return List<String>
	 */
	@Override
	public List<String> getPricingCompetitorByCommoditySubCommodity(final String commodity, final String subComodity) {
		LOG.info("## getPricingCompetitorByCommoditySubCommodity commodity ##" + commodity + " subComodity--" + subComodity + " -- " + sqlGetCmptrsBySubCommodity);
		List<String> lstCompetitors = null;
		try {
			lstCompetitors = this.getJdbcTemplate().query(this.sqlGetCmptrsBySubCommodity,
					new PreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement arg0) throws SQLException {
							arg0.setString(AntiMagicNumber.ONE, commodity);
							arg0.setString(AntiMagicNumber.TWO, subComodity);
						}
					}, new RowMapper<String>() {
						@Override
						public String mapRow(ResultSet rs, int arg1) throws SQLException {
							String compareId = rs.getString(AConstants.CMPTR_ID);
							return compareId;
						}
					});
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lstCompetitors;
	}

	/**
	 * removeComparetorInfo.
	 * @param lvlId
	 *            :long
	 * @throws DSVException
	 *             :DSVException
	 * @return List<String>
	 */
	@Override
	public String removeComparetorInfo(final long lvlId) throws DSVException {
		LOG.info("## removeComparetorInfo ##" + lvlId + "--" + this.sqlDeleteCmptrsByLvl);
		String mess = Constants.ERROR_ATTRIBUTE;
		this.getJdbcTemplate().update(this.sqlDeleteCmptrsByLvl,
				new PreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement arg0) throws SQLException {
						arg0.setLong(AntiMagicNumber.ONE, lvlId);
					}
				});
		mess = Constants.SUCCESSFULLY;
		return mess;
	}

	/**
	 * insertComparetorInfo.
	 * @param lvlId
	 *            :long
	 * @param lstComparetorIds
	 *            :List<String>
	 * @param userId
	 *            :String
	 * @throws DSVException
	 *             :DSVException
	 * @return List<String>
	 */
	@Override
	public String insertComparetorInfo(final long lvlId, final List<String> lstComparetorIds, final String userId) throws DSVException {
		LOG.info("## insertComparetorInfo ##" + lvlId + "--" + this.sqlInserCmptrs + "--" + lstComparetorIds);
		String messReturn = Constants.ERROR_ATTRIBUTE;
		this.getJdbcTemplate().batchUpdate(this.sqlInserCmptrs, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Date date = new Date();
				String cmptrId = lstComparetorIds.get(i);
				ps.setString(AntiMagicNumber.ONE, cmptrId);
				ps.setLong(AntiMagicNumber.TWO, lvlId);
				ps.setString(AntiMagicNumber.THREE, userId);
				ps.setTimestamp(AntiMagicNumber.FOUR, new Timestamp(date.getTime()));
				ps.setString(AntiMagicNumber.FIVE, userId);
				ps.setTimestamp(AntiMagicNumber.SIX, new Timestamp(date.getTime()));
			}

			@Override
			public int getBatchSize() {
				return lstComparetorIds.size();
			}
		});
		messReturn = Constants.SUCCESSFULLY;
		return messReturn;
	}

	/**
	 * getLvlByVendor.
	 * @param vendorId
	 *            :String
	 */
	@Override
	public long getLvlByVendor(final String vendorId) {
		LOG.info("## getLvlByVendor ##" + vendorId + "--" + this.sqlGetLVLKeyByVendor);
		long lvlId = 0;
		try {
			List<Long> lstLVlIds = this.getJdbcTemplate().query(this.sqlGetLVLKeyByVendor, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(AntiMagicNumber.ONE, vendorId);
				}
			}, RuleMapper.getLvlIdMapper());
			if (!Helper.isEmpty(lstLVlIds)) {
				lvlId = lstLVlIds.get(0);
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lvlId;
	}

	/**
	 * getLvlByCommodity.
	 * @param commodity
	 *            :String
	 * @return long
	 */
	@Override
	public long getLvlByCommodity(final String commodity) {
		LOG.info("## getLvlByCommodity ##" + commodity + "--" + this.sqlGetLVLKeyByCommodity);
		long lvlId = 0;
		try {
			List<Long> lstLVlIds = this.getJdbcTemplate().query(this.sqlGetLVLKeyByCommodity, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(AntiMagicNumber.ONE, commodity);
				}
			}, RuleMapper.getLvlIdMapper());
			if (!Helper.isEmpty(lstLVlIds)) {
				lvlId = lstLVlIds.get(0);
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lvlId;
	}

	/**
	 * getLvlByCommoditySubCommodity.
	 * @param commodity
	 *            :String
	 * @param subComodity
	 *            :String
	 * @return long
	 */
	@Override
	public long getLvlByCommoditySubCommodity(final String commodity, final String subComodity) {
		LOG.info("## getLvlByCommoditySubCommodity ##" + commodity + "--" + subComodity + "--" + this.sqlGetLVLKeyByCommodity);

		long lvlId = 0;
		try {
			List<Long> lstLVlIds = this.getJdbcTemplate().query(this.sqlGetLVLKeyByCommoditySubCommodity, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(AntiMagicNumber.ONE, commodity);
					ps.setString(AntiMagicNumber.TWO, subComodity);
				}
			}, RuleMapper.getLvlIdMapper());
			if (!Helper.isEmpty(lstLVlIds)) {
				lvlId = lstLVlIds.get(0);
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lvlId;
	}

	/**
	 * getActTyp.
	 * @return List<BaseVO>
	 */
	@Override
	public List<BaseVO> getActTyp() {
		List<BaseVO> lstActTyp = null;
		try {
			lstActTyp = this.getJdbcTemplate().query(this.sqlGetActTyp, RuleMapper.getActTypMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return lstActTyp;
	}

}
