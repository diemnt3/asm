/*
 * $Id: CustomAuthenticationEntryPoint.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.utils;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

import com.heb.enterprise.dsv.utils.AntiMagicNumber;

/**
 * CustomAuthenticationEntryPoint.
 * @author anhtran
 */
@Component(value = "customAuthenticationEntryPoint")
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {
	private String loginPageUrl;
	private boolean returnParameterEnabled;
	private String returnParameterName;

	/**
	 * getLoginPageUrl.
	 * @return String
	 */
	public String getLoginPageUrl() {
		return this.loginPageUrl;
	}

	public void setLoginPageUrl(String loginPageUrl) {
		this.loginPageUrl = loginPageUrl;
	}

	/**
	 * isReturnParameterEnabled.
	 * @return boolean
	 */
	public boolean isReturnParameterEnabled() {
		return this.returnParameterEnabled;
	}

	public void setReturnParameterEnabled(boolean returnParameterEnabled) {
		this.returnParameterEnabled = returnParameterEnabled;
	}

	public String getReturnParameterName() {
		return this.returnParameterName;
	}

	public void setReturnParameterName(String returnParameterName) {
		this.returnParameterName = returnParameterName;
	}

	/**
	 * commence.
	 * @param request
	 *            :HttpServletRequest
	 * @param response
	 *            :HttpServletResponse
	 * @param reason
	 *            :AuthenticationException
	 * @throws IOException
	 *             :IOException
	 * @throws ServletException
	 *             :ServletException
	 * @author anhtran.
	 */
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException reason) throws IOException, ServletException {
		String ajaxHeader = ((HttpServletRequest) request).getHeader("X-Requested-With");
		if ("XMLHttpRequest".equals(ajaxHeader)) {
			response.sendError(AntiMagicNumber.NINE_HUNDRED_AND_ONE, "Ajax Request Denied (Session Expired)");
			// response.setStatus(AntiMagicNumber.NINE_HUNDRED_AND_ONE);
		} else {
			if (this.loginPageUrl == null || com.heb.enterprise.dsv.utils.Constants.EMPTY_STRING.equals(this.loginPageUrl)) {
				throw new RuntimeException("loginPageUrl has not been defined");
			}
			String redirectUrl = this.loginPageUrl;
			if (this.isReturnParameterEnabled()) {
				String redirectUrlReturnParameterName = this.getReturnParameterName();
				if (redirectUrlReturnParameterName == null ||
						com.heb.enterprise.dsv.utils.Constants.EMPTY_STRING.equals(redirectUrlReturnParameterName)) {
					throw new RuntimeException("redirectUrlReturnParameterName has not been defined");
				}
				redirectUrl += "?" + redirectUrlReturnParameterName + "=" + request.getServletPath();
			}
			RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
			redirectStrategy.sendRedirect(request, response, redirectUrl);
		}
	}
}
