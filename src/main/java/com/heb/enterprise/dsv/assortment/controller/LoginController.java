/*
 * $Id: LoginController.java,v 1.10 2015/03/12 11:19:22 vn44178 Exp $
 *
 * Copyright (c) 2013 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.heb.enterprise.dsv.utils.Helper;

/**
 * Controller for the Login screen.
 * @author duyen.le
 */
@Controller
// @RequestMapping("/login")
public class LoginController {

	private static final String LOGIN_TILE = "login.tile";

	private static final String ACCESS_DENIED = "403.tile";

	private static final String SESSION = "session";

	@Autowired
	private StandardEnvironment environment;

	/**
	 * Handles any login case.
	 * @param logout
	 *            boolean whether the user has just logged out
	 * @param fail
	 *            boolean whether the user has failed a login attempt
	 * @param model
	 *            ModelMap Spring's model to which to add the logout and failure flags
	 * @return the tile for the login page
	 */
	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String showLogin(@RequestParam(required = false) boolean logout, @RequestParam(required = false) boolean fail, ModelMap model) {
		String[] profs = this.environment.getActiveProfiles();
		if (profs.length > 0)
			model.addAttribute("env", profs[0]);
		model.addAttribute("showLogout", logout);
		model.addAttribute("showFailure", fail);
		return LOGIN_TILE;
	}

	/**
	 * showAccessDeniedScreen.
	 * @return the tile for the access denied page
	 */
	@RequestMapping(value = { "/403" }, method = RequestMethod.GET)
	public String showAccessDeniedScreen() {
		return ACCESS_DENIED;
	}

	/**
	 * checkSessionTimeOut.
	 * @param request
	 *            :HttpServletRequest
	 * @return String
	 * @author anhtran.
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = { "/checkSessionTimeOut" }, method = RequestMethod.GET)
	public String checkSessionTimeOut(HttpServletRequest request) {
		HttpSession session = request.getSession();
		JSONObject jsonResult = new JSONObject();
		if (Helper.isEmpty(session)) {
			jsonResult.put(SESSION, false);
		} else {
			jsonResult.put(SESSION, true);
		}
		return jsonResult.toString();
	}

}
