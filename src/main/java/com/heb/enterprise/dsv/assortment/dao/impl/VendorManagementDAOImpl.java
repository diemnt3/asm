/*
 * $Id: VendorManagementDAOImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.heb.enterprise.dsv.assortment.dao.VendorManagementDAO;
import com.heb.enterprise.dsv.assortment.dao.mapper.VendorManagementMapper;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.VendorRulesService;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.EntyDesVO;
import com.heb.enterprise.dsv.vo.EntyRlshpVO;
import com.heb.enterprise.dsv.vo.EntyVO;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;

/**
 * VendorManagementDAOImpl.
 * @author anhtran.
 */
@Repository
@EnableTransactionManagement
public class VendorManagementDAOImpl extends BaseDAOImpl implements VendorManagementDAO {

	private static final Logger LOG = Logger.getLogger(VendorManagementDAOImpl.class);

	private static final String SUB = "SUB";

	private static final String CAT = "CAT";

	private static final String ROOT = "ROOT";

	/**
	 * getVendorHierarchyMapping SqlString.
	 */
	@Value("${coms.dsv.asm.getVendorHierarchyMapping}")
	private String getVendorHierarchyMappingSql;

	/**
	 * getMaxEntyId SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.getMaxEntyId}")
	private String getMaxEntyIdSql;

	/**
	 * getEntyIdByEntyAbb SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.getEntyIdByEntyAbb}")
	private String getEntyIdByEntyAbbSql;

	/**
	 * insertEnty SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.insertEnty}")
	private String insertEntySql;

	/**
	 * insertEntyRlshp SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.insertEntyRlshp}")
	private String insertEntyRlshpSql;

	/**
	 * countEntyRlshp SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyRlshp}")
	private String countEntyRlshpSql;

	/**
	 * updateEntyRlshp SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.updateEntyRlshp}")
	private String updateEntyRlshpSql;

	/**
	 * countEntyRlshpByChildEntyIdHierCntxtCd SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyRlshpByChildEntyIdHierCntxtCd}")
	private String countEntyRlshpByChildEntyIdHierCntxtCdSql;

	/**
	 * deleteEnty SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.deleteEnty}")
	private String deleteEntySql;

	/**
	 * countEntyByEntyAbb SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyByEntyAbb}")
	private String countEntyByEntyAbbSql;

	/**
	 * deleteRlshp SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.deleteEntyRlshp}")
	private String deleteRlshpSql;

	/**
	 * insertEntyDes SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.insertEntyDes}")
	private String insertEntyDesSql;

	/**
	 * countEntyRlshp SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyDesById}")
	private String countEntyDesByIdSql;

	/**
	 * countEntyRlshp SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.deleteEntyDes}")
	private String deleteEntyDesSql;

	/**
	 * countEntyRoot SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyRoot}")
	private String countEntyRootSql;

	/**
	 * countEntyRoot SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyRlshpByEntyId}")
	private String countEntyRlshpByEntyIdSql;

	/**
	 * countEntyRoot SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyRlshpForDeleteRoot}")
	private String countEntyRlshpForDeleteRootSql;

	/**
	 * countEntyRoot SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyDes}")
	private String countEntyDesSql;

	/**
	 * countEntyRoot SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.getEntyByDsplyNbr}")
	private String getEntyByDsplyNbSql;

	/**
	 * countEntyByChildIdAndHierarCntxtCd SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyByChildIdAndHierarCntxtCd}")
	private String countEntyByChildIdAndHierarCntxtCdSql;

	/**
	 * getEntyRlshpByParntEntyAndHierarchyCntxt SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyRlshpByParntEntyAndHierarchyCntxt}")
	private String countEntyRlshpByParntEntyAndHierarchyCntxtSql;

	/**
	 * getEntyRlshpByChildEntyAndHierarchyCntxt SqlString.
	 */
	@Value("${coms.dsv.asm.vendorManagement.countEntyRlshpByChildEntyAndHierarchyCntxt}")
	private String countEntyRlshpByChildEntyAndHierarchyCntxtSql;

	@Autowired
	private CommonService commonService;

	@Autowired
	private VendorRulesService vendorRulesService;

	/**
	 * getCommonService.
	 * @return CommonService
	 */
	public CommonService getCommonService() {
		return this.commonService;
	}

	/**
	 * setCommonService.
	 * @param commonService
	 * :CommonService
	 * @author anhtran.
	 */
	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	/**
	 * getVendorHierarchyMappingByVendor.
	 * @param vendorId
	 *            :String
	 * @return List<HierarchyMapVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran
	 */
	@Override
	public List<HierarchyMapVO> getVendorHierarchyMappingByVendor(String vendorId) throws DSVException {
		List<HierarchyMapVO> hierarchyMapVOs = null;
		List<EntyRlshpVO> entyRlshpVOs = null;
		try {
			String hierCntxtCd1 = Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ZERO);
			String hierCntxtCd2 = Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE);
			entyRlshpVOs = this.getJdbcTemplate().query(this.getVendorHierarchyMappingSql, new Object[] { hierCntxtCd1, hierCntxtCd2 }, VendorManagementMapper.getEntyRlshpMapper());// this.getVendorHierarchyMappingSql
			hierarchyMapVOs = this.getListHierarchyMapVO(entyRlshpVOs, vendorId);
		} catch (DataAccessException e) {
			LOG.info(e.getMessage(), e);
			throw new DSVException(e);
		}
		return hierarchyMapVOs;
	}

	/**
	 * getEntyRlshpMapByHierCntxt.
	 * @param entyRlshpVOs
	 *            :List<EntyRlshpVO>
	 * @param hierCntxtCd
	 *            :String
	 * @return Map<String, EntyVO>
	 * @author anhtran.
	 */
	private Map<String, EntyRlshpVO> getEntyRlshpMapByHierCntxt(List<EntyRlshpVO> entyRlshpVOs, String hierCntxtCd) {
		Map<String, EntyRlshpVO> map = new HashMap<String, EntyRlshpVO>();
		for (EntyRlshpVO entyRlshpVO : entyRlshpVOs) {
			if (entyRlshpVO.getHierCntxtCd().equals(hierCntxtCd)) {
				map.put(entyRlshpVO.getChildEntyId(), entyRlshpVO);
			}
		}
		return map;
	}

	/**
	 * getEntyRlshpVOListByHierCntxt.
	 * @param entyRlshpVOs
	 *            :List<EntyRlshpVO>
	 * @param hierCntxtCd
	 *            :String
	 * @return List<EntyVO>
	 * @author anhtran.
	 */
	private List<EntyRlshpVO> getEntyRlshpVOListByHierCntxt(List<EntyRlshpVO> entyRlshpVOs, String hierCntxtCd) {
		List<EntyRlshpVO> entyRlshpVOs2 = new ArrayList<EntyRlshpVO>();
		for (EntyRlshpVO entyRlshpVO : entyRlshpVOs) {
			if (entyRlshpVO.getHierCntxtCd().equals(hierCntxtCd)) {
				entyRlshpVOs2.add(entyRlshpVO);
			}
		}
		return entyRlshpVOs2;
	}

	/**
	 * getListHierarchyMapVO.
	 * @param entyRlshpVOs
	 *            :List<EntyRlshpVO>
	 * @return List<HierarchyMapVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	private List<HierarchyMapVO> getListHierarchyMapVO(List<EntyRlshpVO> entyRlshpVOs, String vendorId) throws DSVException {
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		Map<String, EntyRlshpVO> mapKehe = this.getEntyRlshpMapByHierCntxt(entyRlshpVOs, Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ZERO));
		List<EntyRlshpVO> listHebKehe = this.getEntyRlshpVOListByHierCntxt(entyRlshpVOs, Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE));
		for (EntyRlshpVO entyVO : listHebKehe) {
			this.getHierarchyPath(entyVO, mapKehe);
		}
		for (int i = 1; i <= listHebKehe.size(); i++) {
			String[] catIds = listHebKehe.get(i - 1).getFullPath().split(Constants.GREATER_THAN_SIGN);
			String[] catAbbs = listHebKehe.get(i - 1).getFullPathAbb().split(Constants.GREATER_THAN_SIGN);
			HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
			try {
				for (int j = 1; j <= catIds.length; j++) {
					if (j == catIds.length) {
						BeanUtils.setProperty(hierarchyMapVO, "subCommodity", catAbbs[j - 1]);
					} else {
						BeanUtils.setProperty(hierarchyMapVO, "category" + j, catIds[j - 1]);
						BeanUtils.setProperty(hierarchyMapVO, "categoryAbb" + j, catAbbs[j - 1]);
					}
				}
				if (SUB.equals(listHebKehe.get(i - 1).getEntyTypCd())) {
					hierarchyMapVO.setEntyIdSubCommodity(listHebKehe.get(i - 1).getChildEntyId());
				} else {
					hierarchyMapVO.setEntyIdSubCommodity(Constants.EMPTY_STRING);
				}
			} catch (IllegalAccessException e) {
				LOG.fatal(e.getMessage(), e);
				throw new DSVException(e);
			} catch (InvocationTargetException e) {
				LOG.fatal(e.getMessage(), e);
				throw new DSVException(e);
			}

			hierarchyMapVO.setLowestCatId(catIds[catIds.length - 2]);
			hierarchyMapVO.setVendorId(vendorId);
			hierarchyMapVO.setStatusNew(Constants.NO);
			hierarchyMapVO.setStatusChange(Constants.NO);
			hierarchyMapVO.setSubCommodityCd(listHebKehe.get(i - 1).getDsplyNbr());
			hierarchyMapVOs.add(hierarchyMapVO);
		}
		return hierarchyMapVOs;
	}

	/**
	 * getHierarchyPath.
	 * @param entyVO
	 *            :entyVO
	 * @param mapKehe
	 *            :Map<String, EntyVO>
	 * @return EntyVO
	 * @author anhtran.
	 */
	private EntyRlshpVO getHierarchyPath(EntyRlshpVO entyVO, Map<String, EntyRlshpVO> mapKehe) {
		if (mapKehe.containsKey(entyVO.getParentTemp())) {
			if (Helper.isEmpty(entyVO.getFullPathAbb())) {
				entyVO.setFullPathAbb(mapKehe.get(entyVO.getParentTemp()).getEntyAbb());
			} else {
				entyVO.setFullPathAbb(mapKehe.get(entyVO.getParentTemp()).getEntyAbb() + Constants.GREATER_THAN_SIGN + entyVO.getFullPathAbb());
			}
			entyVO.setParentTemp(mapKehe.get(entyVO.getParentTemp()).getParntEntyId());
			if (!entyVO.getParentTemp().equals(Constants.ZERO)) {
				entyVO.setFullPath(entyVO.getParentTemp() + Constants.GREATER_THAN_SIGN + entyVO.getFullPath());
			}
			this.getHierarchyPath(entyVO, mapKehe);
		}
		return entyVO;
	}

	/**
	 * Save vendor hierarchy mapping.
	 * @param hierarchyMapVOs
	 *            :List<HierarchyMapVO>
	 * @param vendorId
	 *            :String
	 * @throws SQLException
	 *             :SQLException.
	 * @author anhtran
	 */
	@Override
	@Transactional("transactionManager")
	public void saveVendorHierarchyMapping(List<HierarchyMapVO> hierarchyMapVOs, String vendorId) throws SQLException {
		this.insertEntyRoot();
		String userName = this.commonService.getUserLogin().getUsername();
		List<EntyRlshpVO> updateEntyRlshpList = new ArrayList<EntyRlshpVO>();
		for (HierarchyMapVO hierarchyMapVO : hierarchyMapVOs) {
			List<EntyRlshpVO> rlshpVOs = new ArrayList<EntyRlshpVO>();
			List<EntyVO> entyVOs = new ArrayList<EntyVO>();
			List<EntyDesVO> entyDesVOs = new ArrayList<EntyDesVO>();
			String[] strArr = hierarchyMapVO.getCats().split(Constants.GREATER_THAN_SIGN);
			if (Constants.STRING_Y.equals(hierarchyMapVO.getStatusNew())) {
				for (int i = 0; i < strArr.length; i++) {
					EntyVO entyVO = new EntyVO();
					entyVO.setEntyAbb(strArr[i]);
					entyVO.setCre8Uid(userName);
					entyVO.setLstUpdtUid(userName);
					if (i < strArr.length - 1) {
						entyVO.setEntyDsplyNbr(Constants.ZERO);
						entyVO.setEntyTypCd(CAT);
					} else {
						entyVO.setEntyDsplyNbr(hierarchyMapVO.getSubCommodityCd());
						entyVO.setEntyTypCd(SUB);
					}
					if (i == 0) {
						entyVO.setParntEntyId(Constants.ZERO);
					} else {
						entyVO.setParntEntyId(entyVOs.get(i - 1).getEntyId());
					}
					String entyId = this.insertEnty(entyVO);
					entyVO.setEntyId(entyId);
					entyVOs.add(entyVO);
					LOG.info("saveVendorHierarchyMapping - entyId new: " + entyId);
					// create enty_des
					EntyDesVO entyDesVO = new EntyDesVO();
					entyDesVO.setEntyId(Long.valueOf(entyVO.getEntyId()));
					entyDesVO.setEntyShrtDes(entyVO.getEntyAbb().length() <= AntiMagicNumber.FIFTY ? entyVO.getEntyAbb() : entyVO.getEntyAbb().substring(AntiMagicNumber.ZERO, AntiMagicNumber.FIFTY));
					entyDesVO.setEntyLongDes(entyVO.getEntyAbb());
					entyDesVO.setCre8Uid(userName);
					entyDesVO.setLstUpdtUid(userName);

					EntyRlshpVO rlshpVO = new EntyRlshpVO();
					if (i == 0) {
						rlshpVO.setParntEntyId(Constants.ZERO);
					} else {
						rlshpVO.setParntEntyId(entyVOs.get(i - 1).getEntyId());
					}

					rlshpVO.setChildEntyId(entyVO.getEntyId());
					if (CAT.equals(entyVO.getEntyTypCd())) {
						rlshpVO.setHierCntxtCd(Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ZERO));
						entyDesVO.setHierCntxtCd(Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ZERO));
					} else {
						rlshpVO.setHierCntxtCd(Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE));
						entyDesVO.setHierCntxtCd(Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE));
					}
					// set value EntyRlshpVO
					rlshpVO.setDfltParntSw(Constants.STRING_Y);
					rlshpVO.setDsplySw(Constants.STRING_Y);
					rlshpVO.setSeqNbr(String.valueOf(AntiMagicNumber.ONE));
					rlshpVO.setCre8UId(userName);
					rlshpVO.setLstUpdtUid(userName);
					rlshpVOs.add(rlshpVO);
					entyDesVOs.add(entyDesVO);
				}
				this.insertEntyRlshp(rlshpVOs);
				this.insertEntyDes(entyDesVOs);
			} else {
				EntyRlshpVO rlshpVO = new EntyRlshpVO();
				rlshpVO.setChildEntyId(hierarchyMapVO.getEntyIdSubCommodity());
				rlshpVO.setParntEntyId(hierarchyMapVO.getLowestCatId());
				rlshpVO.setEntyAbb(strArr[strArr.length - 1]);
				rlshpVO.setHierCntxtCd(Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE));
				rlshpVO.setDsplyNbr(hierarchyMapVO.getSubCommodityCd());
				updateEntyRlshpList.add(rlshpVO);
			}
		}
		this.updateEntyRlshp(updateEntyRlshpList, vendorId);
	}

	/**
	 * getMaxEntyId.
	 * @return int
	 * @author anhtran.
	 */
	private int getMaxEntyId() {
		return this.getJdbcTemplate().queryForInt(this.getMaxEntyIdSql);
	}

	/**
	 * countEntyRlshp.
	 * @param childEntyId
	 *            :String
	 * @param hierCntxtCd
	 *            :String
	 * @param parntEntyId
	 *            :String
	 * @return int
	 * @author anhtran.
	 */
	private int countEntyRlshp(String parntEntyId, String childEntyId, String hierCntxtCd) {
		return this.getJdbcTemplate().queryForInt(this.countEntyRlshpSql, Integer.parseInt(parntEntyId), Integer.parseInt(childEntyId), hierCntxtCd);
	}

	/**
	 * insertEnty.
	 * @param entyVO
	 *            :EntyVO
	 * @return String
	 * @throws SQLException
	 *             :SQLException
	 * @author anhtran.
	 */
	public String insertEnty(final EntyVO entyVO) throws SQLException {
		List<EntyVO> entyVOs = null;
		if (SUB.equals(entyVO.getEntyTypCd())) {
			entyVOs = this.getJdbcTemplate().query(this.getEntyByDsplyNbSql, new Object[] { entyVO.getEntyDsplyNbr() }, VendorManagementMapper.getEntyMapper());
		} else {
			entyVOs = this.getJdbcTemplate().query(this.getEntyIdByEntyAbbSql, VendorManagementMapper.getPreparedStatementSetterToGetEntyId(entyVO), VendorManagementMapper.getEntyMapper());
		}

		if (entyVOs.isEmpty()) {
			entyVO.setEntyId(String.valueOf(this.getMaxEntyId() + 1));
			this.getJdbcTemplate().update(this.insertEntySql, VendorManagementMapper.getPreparedStatementSetterToInsertEnty(entyVO));
		} else {
			entyVO.setEntyId(String.valueOf(entyVOs.get(0).getEntyId()));
		}
		return entyVO.getEntyId();
	}

	/**
	 * Get HEB Hierarchy.
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author anhtran
	 */
	@Override
	public List<CommodityVO> getHEBHierarchy() throws DSVException {
		return new ArrayList<CommodityVO>();
	}

	/**
	 * insertEntyRlshp.
	 * @param entyRlshpVOs
	 *            :List<EntyRlshpVO>
	 * @throws SQLException
	 *             :DSVException
	 * @author anhtran.
	 */
	private void insertEntyRlshp(final List<EntyRlshpVO> entyRlshpVOs) throws SQLException {
		for (EntyRlshpVO entyRlshpVO : entyRlshpVOs) {
			if (this.countEntyRlshp(entyRlshpVO.getParntEntyId(), entyRlshpVO.getChildEntyId(), entyRlshpVO.getHierCntxtCd()) == 0) {
				this.getJdbcTemplate().update(this.insertEntyRlshpSql, VendorManagementMapper.getPreparedStatementSetterToInsertEntyRlshp(entyRlshpVO));
			}
		}
	}

	/**
	 * updateEntyRlshp.
	 * @param entyRlshpVOs
	 *            :List<EntyRlshpVO>
	 * @param vendorId
	 *            :String
	 * @throws SQLException
	 *             :SQLException
	 * @author anhtran.
	 */
	private void updateEntyRlshp(final List<EntyRlshpVO> entyRlshpVOs, String vendorId) throws SQLException {
		String userName = this.commonService.getUserLogin().getUsername();
		List<String> deleteEntyLst = new ArrayList<String>();
		List<EntyDesVO> deleteLst = new ArrayList<EntyDesVO>();
		List<EntyDesVO> entyDesVOs = new ArrayList<EntyDesVO>();
		for (EntyRlshpVO entyRlshpVO : entyRlshpVOs) {
			// create enty
			EntyVO entyVO = new EntyVO();
			entyVO.setEntyTypCd(SUB);
			entyVO.setEntyAbb(entyRlshpVO.getEntyAbb());
			entyVO.setEntyDsplyNbr(entyRlshpVO.getDsplyNbr());
			entyVO.setCre8Uid(userName);
			entyVO.setLstUpdtUid(userName);
			String entyId = this.insertEnty(entyVO);
			entyRlshpVO.setChildEntyIdTemp(entyId);
			// create enty_des
			EntyDesVO entyDesVO = new EntyDesVO();
			entyDesVO.setEntyId(Long.valueOf(entyId));
			entyDesVO.setHierCntxtCd(Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE));
			entyDesVO.setEntyShrtDes(entyRlshpVO.getEntyAbb());
			entyDesVO.setEntyLongDes(entyRlshpVO.getEntyAbb());
			entyDesVO.setCre8Uid(userName);
			entyDesVO.setLstUpdtUid(userName);
			entyDesVOs.add(entyDesVO);
		}
		this.insertEntyDes(entyDesVOs);
		this.getJdbcTemplate().batchUpdate(this.updateEntyRlshpSql, VendorManagementMapper.getPreparedStatementToUpdateEntyRlshp(entyRlshpVOs));
		for (EntyRlshpVO entyRlshpVO : entyRlshpVOs) {
			if (this.getJdbcTemplate().queryForInt(this.countEntyRlshpByChildEntyIdHierCntxtCdSql, entyRlshpVO.getChildEntyId(), entyRlshpVO.getHierCntxtCd()) == AntiMagicNumber.ZERO) {
				EntyDesVO entyDesVO = new EntyDesVO();
				entyDesVO.setEntyId(Long.valueOf(entyRlshpVO.getChildEntyId()));
				entyDesVO.setHierCntxtCd(entyRlshpVO.getHierCntxtCd());
				deleteLst.add(entyDesVO);
				deleteEntyLst.add(entyRlshpVO.getChildEntyId());
			}
		}
		this.deleteEntyDes(deleteLst);
		this.deleteEnty(deleteEntyLst);
	}

	/**
	 * deleteEntyRlshp.
	 * @param vendorId
	 *            :String
	 * @param categoryIds
	 *            :String
	 * @param subCommodity
	 *            :String
	 * @throws DSVException
	 *             :DSVException
	 * @throws SQLException
	 *             :SQLException
	 * @author anhtran.
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional("transactionManager")
	public void deleteEntyRlshp(String vendorId, String categoryIds, String subCommodity) throws DSVException, SQLException {
		String[] categoryIdArr = categoryIds.split(Constants.GREATER_THAN_SIGN);
		List<String> entyIds = new ArrayList<String>();
		List<EntyDesVO> entyDesVOs = new ArrayList<EntyDesVO>();
		for (int i = categoryIdArr.length - 1; i >= 0; i--) {
			EntyRlshpVO rlshpVO = new EntyRlshpVO();
			rlshpVO.setChildEntyId(categoryIdArr[i]);
			if (i == categoryIdArr.length - 1) {
				rlshpVO.setEntyTypCd(SUB);
			} else {
				rlshpVO.setEntyTypCd(CAT);
			}
			if (i == AntiMagicNumber.ZERO) {
				rlshpVO.setParntEntyId(Constants.ZERO);
			} else {
				rlshpVO.setParntEntyId(categoryIdArr[i - 1]);
			}
			if (CAT.equals(rlshpVO.getEntyTypCd())) {
				rlshpVO.setHierCntxtCd(Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ZERO));
			} else {
				rlshpVO.setHierCntxtCd(Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE));
			}

			if (SUB.equals(rlshpVO.getEntyTypCd())) {
				List<Object> objects = this.getEntyIdsAndEntyDesVOsForSubEnty(vendorId, subCommodity, rlshpVO, entyDesVOs, entyIds);
				entyIds.addAll((List<String>) objects.get(AntiMagicNumber.ZERO));
				entyDesVOs.addAll((List<EntyDesVO>) objects.get(AntiMagicNumber.ONE));
			} else {
				if (Constants.ZERO.equals(rlshpVO.getParntEntyId())) {
					List<Object> objects = this.getEntyIdsAndEntyDesVOsForParntEntyZero(vendorId, subCommodity, rlshpVO, entyDesVOs, entyIds);
					entyIds.addAll((List<String>) objects.get(AntiMagicNumber.ZERO));
					entyDesVOs.addAll((List<EntyDesVO>) objects.get(AntiMagicNumber.ONE));
				} else {
					List<Object> objects = this.getEntyIdsAndEntyDesVOsForParntEnty(vendorId, subCommodity, rlshpVO, entyDesVOs, entyIds);
					entyIds.addAll((List<String>) objects.get(AntiMagicNumber.ZERO));
					entyDesVOs.addAll((List<EntyDesVO>) objects.get(AntiMagicNumber.ONE));
				}
			}
		}
		this.deleteEntyDes(entyDesVOs);
		this.deleteEnty(entyIds);
		this.deleteEntyRoot();
	}

	/**
	 * getEntyIdsAndEntyDesVOsForSubEnty.
	 * @param vendorId
	 *            :String
	 * @param subCommodity
	 *            :String
	 * @param rlshpVO
	 *            :String
	 * @param entyDesVOs
	 *            :String
	 * @param entyIds
	 *            :String
	 * @return List<Object>
	 * @author anhtran.
	 */
	private List<Object> getEntyIdsAndEntyDesVOsForSubEnty(String vendorId, String subCommodity, EntyRlshpVO rlshpVO, List<EntyDesVO> entyDesVOs, List<String> entyIds) {
		List<Object> objects = new ArrayList<Object>();
		this.getJdbcTemplate().update(this.deleteRlshpSql, Integer.parseInt(rlshpVO.getParntEntyId()), Integer.parseInt(rlshpVO.getChildEntyId()), rlshpVO.getHierCntxtCd());
		if (this.getJdbcTemplate().queryForInt(this.countEntyByChildIdAndHierarCntxtCdSql, rlshpVO.getChildEntyId(), rlshpVO.getHierCntxtCd()) == 0) {
			EntyDesVO desVO = new EntyDesVO();
			desVO.setEntyId(Long.valueOf(rlshpVO.getChildEntyId()));
			desVO.setHierCntxtCd(rlshpVO.getHierCntxtCd());
			entyDesVOs.add(desVO);
			if (this.getJdbcTemplate().queryForInt(this.countEntyDesByIdSql, rlshpVO.getChildEntyId()) == AntiMagicNumber.ONE) {
				entyIds.add(rlshpVO.getChildEntyId());
			}
			this.vendorRulesService.resetRulesToHigherLevel(vendorId, null, subCommodity);
		}
		objects.add(entyIds);
		objects.add(entyDesVOs);
		return objects;
	}

	/**
	 * getEntyIdsAndEntyDesVOsForParntEntyZero.
	 * @param vendorId
	 *            :String
	 * @param subCommodity
	 *            :String
	 * @param rlshpVO
	 *            :String
	 * @param entyDesVOs
	 *            :String
	 * @param entyIds
	 *            :String
	 * @return List<Object>
	 * @author anhtran.
	 */
	private List<Object> getEntyIdsAndEntyDesVOsForParntEntyZero(String vendorId, String subCommodity, EntyRlshpVO rlshpVO, List<EntyDesVO> entyDesVOs, List<String> entyIds) {
		List<Object> objects = new ArrayList<Object>();
		if (this.getJdbcTemplate().queryForInt(this.countEntyRlshpByParntEntyAndHierarchyCntxtSql, rlshpVO.getChildEntyId(),
				Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ZERO), Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE)) == 0) {
			this.getJdbcTemplate().update(this.deleteRlshpSql, Integer.parseInt(rlshpVO.getParntEntyId()), Integer.parseInt(rlshpVO.getChildEntyId()), rlshpVO.getHierCntxtCd());
			EntyDesVO desVO = new EntyDesVO();
			desVO.setEntyId(Long.valueOf(rlshpVO.getChildEntyId()));
			desVO.setHierCntxtCd(rlshpVO.getHierCntxtCd());
			entyDesVOs.add(desVO);
			if (this.getJdbcTemplate().queryForInt(this.countEntyDesByIdSql, rlshpVO.getChildEntyId()) == AntiMagicNumber.ONE) {
				entyIds.add(rlshpVO.getChildEntyId());
			}
		}
		objects.add(entyIds);
		objects.add(entyDesVOs);
		return objects;
	}

	/**
	 * getEntyIdsAndEntyDesVOsForParntEnty.
	 * @param vendorId
	 *            :String
	 * @param subCommodity
	 *            :String
	 * @param rlshpVO
	 *            :String
	 * @param entyDesVOs
	 *            :String
	 * @param entyIds
	 *            :String
	 * @return List<Object>
	 * @author anhtran.
	 */
	private List<Object> getEntyIdsAndEntyDesVOsForParntEnty(String vendorId, String subCommodity, EntyRlshpVO rlshpVO, List<EntyDesVO> entyDesVOs, List<String> entyIds) {
		List<Object> objects = new ArrayList<Object>();
		if (this.getJdbcTemplate().queryForInt(this.countEntyRlshpByParntEntyAndHierarchyCntxtSql, rlshpVO.getChildEntyId(),
				Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ZERO), Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE)) == 0
				&&
				this.getJdbcTemplate().queryForInt(this.countEntyRlshpByChildEntyAndHierarchyCntxtSql, rlshpVO.getParntEntyId(),
						Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ZERO), Helper.getHierCntxtCdByVendorId(vendorId).get(AntiMagicNumber.ONE)) > 0) {
			this.getJdbcTemplate().update(this.deleteRlshpSql, Integer.parseInt(rlshpVO.getParntEntyId()), Integer.parseInt(rlshpVO.getChildEntyId()), rlshpVO.getHierCntxtCd());
			EntyDesVO desVO = new EntyDesVO();
			desVO.setEntyId(Long.valueOf(rlshpVO.getChildEntyId()));
			desVO.setHierCntxtCd(rlshpVO.getHierCntxtCd());
			entyDesVOs.add(desVO);
			if (this.getJdbcTemplate().queryForInt(this.countEntyDesByIdSql, rlshpVO.getChildEntyId()) == AntiMagicNumber.ONE) {
				entyIds.add(rlshpVO.getChildEntyId());
			}
		}
		objects.add(entyIds);
		objects.add(entyDesVOs);
		return objects;
	}

	/**
	 * insertEntyDes.
	 * @param entyDesVOs
	 *            :List<EntyRlshpVO>
	 * @throws SQLException
	 *             :DSVException
	 * @author anhtran.
	 */
	private void insertEntyDes(final List<EntyDesVO> entyDesVOs) throws SQLException {
		for (EntyDesVO entyDesVO : entyDesVOs) {
			if (this.countEntyDes(entyDesVO.getEntyId(), entyDesVO.getHierCntxtCd()) == 0) {
				this.getJdbcTemplate().update(this.insertEntyDesSql, VendorManagementMapper.getPreparedStatementSetterToInsertEntyDes(entyDesVO));
			}
		}
	}

	/**
	 * countEntyDes.
	 * @param entyId
	 *            :long
	 * @param hierCntxtCd
	 *            :String
	 * @return int
	 * @author anhtran.
	 */
	private int countEntyDes(long entyId, String hierCntxtCd) {
		return this.getJdbcTemplate().queryForInt(this.countEntyDesSql, entyId, hierCntxtCd);
	}

	/**
	 * deleteEntyDes.
	 * @param entyDesVOs
	 *            :List<EntyDesVO>
	 * @throws SQLException
	 *             :DSVException
	 * @author anhtran.
	 */
	private void deleteEntyDes(final List<EntyDesVO> entyDesVOs) throws SQLException {
		this.getJdbcTemplate().batchUpdate(this.deleteEntyDesSql, VendorManagementMapper.getPreparedStatementToDeleteEntyDes(entyDesVOs));
	}

	/**
	 * deleteEnty.
	 * @param entyIdList
	 *            :List<String>
	 * @throws SQLException
	 *             :DSVException
	 * @author anhtran.
	 */
	private void deleteEnty(final List<String> entyIdList) throws SQLException {
		this.getJdbcTemplate().batchUpdate(this.deleteEntySql, VendorManagementMapper.getPreparedStatementToDeleteEnty(entyIdList));
	}

	/**
	 * insertEntyRoot.
	 * @author anhtran.
	 */
	private void insertEntyRoot() {
		if (this.getJdbcTemplate().queryForInt(this.countEntyRootSql, AntiMagicNumber.ZERO) == 0) {
			String userName = this.commonService.getUserLogin().getUsername();
			EntyVO entyVO = new EntyVO();
			entyVO.setEntyId(Constants.ZERO);
			entyVO.setEntyTypCd(CAT);
			entyVO.setEntyAbb(ROOT);
			entyVO.setEntyDsplyNbr(Constants.ZERO);
			entyVO.setCre8Uid(userName);
			entyVO.setLstUpdtUid(userName);
			this.getJdbcTemplate().update(this.insertEntySql, VendorManagementMapper.getPreparedStatementSetterToInsertEnty(entyVO));
		}
	}

	/**
	 * deleteEntyRoot.
	 * @author anhtran.
	 */
	private void deleteEntyRoot() {
		if (this.getJdbcTemplate().queryForInt(this.countEntyRlshpForDeleteRootSql) == AntiMagicNumber.ZERO) {
			this.getJdbcTemplate().update(this.deleteEntySql, Constants.ZERO);
		}
	}

}
