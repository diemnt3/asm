/*
 * $Id: RetailCostService.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.util.List;
import java.util.Map;

/**
 * RetailCostService interface.
 * @author diemnguyen
 */
public interface CostReviewService {
	/**
	 * find Retail and Cost by searching criteria based on the RetailCostSearchModel.
	 * @param model
	 *            RetailCostSearchModel
	 * @param tabActive
	 *            - There are 2 types : Pass/Failed
	 * @return List<RetailCostModel> List of ProductSearchModel is retrieved
	 * @throws DSVException
	 *             - If could not connect to database
	 */
	List<RetailCostVO> getProductWithCostChanges(RetailCostSearchVO model, String tabActive) throws DSVException;

	/**
	 * total Of Result ForEach Status (passed or failed).
	 * @param model
	 *            RetailCostSearchModel
	 * @param tabActive
	 *            String
	 * @return Map<String, Integer> result - Each of status has matches result is retrieved
	 * @throws DSVException
	 *             - If could not connect to database
	 * @author diemnguyen
	 */
	Map<String, Integer> getTotalPassedFailed(RetailCostSearchVO model, String tabActive) throws DSVException;

	/**
	 * Get competitor list.
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return List<CompetitiveRetails>
	 * @author diemnguyen.
	 */
	List<CompetitiveRetails> getCompetitorList() throws DSVException;

	/**
	 * Get Future Cost.
	 * @throws DSVException
	 *             :If could not get values from Service Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	List<RetailCostVO> getFutureSegments() throws DSVException;

	/**
	 * getBoundaryRules.
	 * @throws DSVException
	 *             :DSVException.
	 * @return List<BaseVO>
	 * @author diemnguyen.
	 */
	List<BaseVO> getBoundaryRules() throws DSVException;

	/**
	 * Reject product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	String rejectProducts(List<RetailCostVO> retailCostVOs) throws DSVException;

	/**
	 * Remove product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	String removeProducts(List<RetailCostVO> retailCostVOs) throws DSVException;

	/**
	 * Save product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	String saveProducts(List<RetailCostVO> retailCostVOs) throws DSVException;
}
