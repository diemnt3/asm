/*
 * $Id.: $GlobalRulesController.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.GlobalRulesService;
import com.heb.enterprise.dsv.assortment.utils.ConstantsWeb;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.JSTreeVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;

/**
 * Controller for the Dashboard screen.
 * @author Duyen.le
 */

@Controller
public class GlobalRulesController {
	private static final Logger LOG = Logger.getLogger(GlobalRulesController.class);
	/**
	 * Path here.
	 */
	private static final String GLOBAL_RULES_PATH = Constants.SLASH + ConstantsWeb.GLOBAL_PAGE;
	private static final String AJAX_HIERACHY_PATH = Constants.SLASH + ConstantsWeb.AJAX_HIERACHY;
	private static final String UPDATE_COMMODITY_RULES_PATH = Constants.SLASH + ConstantsWeb.UPDATE_COMMODITY_RULES;
	private static final String LOAD_COMMODITY_RULES_PATH = Constants.SLASH + ConstantsWeb.LOAD_COMMODITY_RULES;
	private static final String RESET_RULES_PATH = Constants.SLASH + ConstantsWeb.RESET_RULES;

	/**
	 * Tiles here.
	 */
	private static final String GLOBAL_RULES_TILE = "global.tile";
	private static final String SUBDEPT_ID = "subDeptId";
	/**
	 * Inject GlobalManagementService.
	 */
	@Autowired
	private GlobalRulesService globalRulesService;
	@Autowired
	private CommonService commonService;

	/**
	 * show Global Rules Screen to manage configuration settings.
	 * @param model
	 *            ModelMap
	 * @return String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@RequestMapping(value = { Constants.SLASH, GLOBAL_RULES_PATH }, method = { RequestMethod.GET })
	public String showGlobalRulesScreen(ModelMap model) throws DSVException {
		LOG.info("Show Global Rules Screen");
		// model.addAttribute("logicRule", this.globalRulesService.getLogicRules());
		List<AssortmentRulesVO> lstRules = this.globalRulesService.getRulesByRuleGroup();
		if (Helper.isNormalList(lstRules)) {
			for (AssortmentRulesVO assortmentRulesVO : lstRules) {
				model.addAttribute("rule" + assortmentRulesVO.getId(), assortmentRulesVO);
			}
		}
		return GLOBAL_RULES_TILE;
	}

	/**
	 * get Commodity Rules have existed from Database.
	 * @return String JSON String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { AJAX_HIERACHY_PATH }, method = { RequestMethod.POST })
	@ResponseBody
	public String ajaxGetHierachy() throws DSVException {
		LOG.info("ajax to Get Hierachy info");
		List<DepartmentVO> lstDept = this.globalRulesService.getHierachyForGlobalRules();
		List<JSONObject> lstSubDeptJson = new ArrayList<JSONObject>();
		Map<String, LevelVO> globalCommodityMap = this.globalRulesService.getGlobalCommodityOverride();
		List<SubDepartmentVO> lstSubDeptSorted = new ArrayList<SubDepartmentVO>();

		for (DepartmentVO departmentVO : lstDept) {
			List<SubDepartmentVO> lstSubDept = departmentVO.getSubDepartmentVOs();
			lstSubDeptSorted.addAll(lstSubDept);
		}
		Collections.sort(lstSubDeptSorted, SubDepartmentVO.SubDeptVOComparator);
		if (Helper.isNormalList(lstSubDeptSorted)) {
			for (SubDepartmentVO subDeptVO : lstSubDeptSorted) {
				JSONObject subDeptJsonObj = new JSONObject();
				subDeptJsonObj.put(JSTreeVO.TEXT, subDeptVO.getSubDeptDesc());
				subDeptJsonObj.put(JSTreeVO.ID, subDeptVO.getSubDeptId().trim());
				List<ClassVO> classVOs = subDeptVO.getClassVOs();
				List<CommodityVO> lstCommoditySorted = new ArrayList<CommodityVO>();
				List<JSONObject> lstCommodityChild = new ArrayList<JSONObject>();
				if (Helper.isNormalList(classVOs)) {
					for (ClassVO classVO : classVOs) {
						List<CommodityVO> lstCommodity = classVO.getCommodityVOs();
						lstCommoditySorted.addAll(lstCommodity);
					}
					Collections.sort(lstCommoditySorted, CommodityVO.CommodityVOComparator);
					if (Helper.isNormalList(lstCommoditySorted)) {
						for (CommodityVO commodityVO : lstCommoditySorted) {
							JSONObject commodityJsonObj = new JSONObject();
							JSONObject aAttrObjSubCommodity = new JSONObject();
							commodityJsonObj.put(JSTreeVO.TEXT, commodityVO.getCommodityCd() + Constants.DASH_WHITE_SPACE + commodityVO.getCommodityDesc());
							commodityJsonObj.put(JSTreeVO.ID, commodityVO.getCommodityCd());

							if (!Helper.isEmpty(globalCommodityMap.get(subDeptVO.getSubDeptId().trim().concat(String.valueOf(commodityVO.getCommodityCd()).trim())))) {
								aAttrObjSubCommodity.put(Constants.CLASS_CONS, "rule-flag");
								commodityJsonObj.put(JSTreeVO.A_ATTR, aAttrObjSubCommodity);
								commodityJsonObj.put(JSTreeVO.RULES_CUSTOMIZE, Constants.YES);
							} else {
								commodityJsonObj.put(JSTreeVO.RULES_CUSTOMIZE, Constants.NO);
							}
							lstCommodityChild.add(commodityJsonObj);
						}
					}
				}

				subDeptJsonObj.put(JSTreeVO.CHILDREN, lstCommodityChild);
				lstSubDeptJson.add(subDeptJsonObj);
			}
		}
		return Helper.parseToJsonStringWithListObj(lstSubDeptJson);
	}

	/**
	 * Get Global Rules Set.
	 * @param subDeptId
	 *            String
	 * @param commodityId
	 *            String
	 * @param session
	 *            String
	 * @param model
	 *            ModelMap
	 * @return String JSON String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { LOAD_COMMODITY_RULES_PATH }, method = { RequestMethod.POST })
	@ResponseBody
	public String getGlobalRuleSet(@RequestParam("subDeptId") String subDeptId, @RequestParam("commodityID") String commodityId,
			ModelMap model, HttpSession session) throws DSVException {
		this.LOG.info("GET RULE SETTINGS: Sub DeptId : " + subDeptId + " and CommodityID: " + commodityId);
		JSONObject jsonResult = new JSONObject();
		Map<String, AssortmentRulesVO> lstRules = this.globalRulesService.getGlobalRuleSet(subDeptId, commodityId);
		List<AssortmentRulesVO> lstRuleOri = null;
		if (!Helper.isEmpty(lstRules)) {
			lstRuleOri = new ArrayList<AssortmentRulesVO>(lstRules.values());

			if (!Helper.isEmpty(commodityId)) {
				for (AssortmentRulesVO assortmentRulesVO : lstRuleOri) {
					if (assortmentRulesVO.getHebComdId() == 0) {
						assortmentRulesVO.setLvlId(Long.valueOf(AntiMagicNumber.ZERO));
					}
				}
			}
		}

		session.setAttribute(ConstantsWeb.RULE_ORI, lstRuleOri);
		List<JSONObject> lstJson = new ArrayList<JSONObject>();
		for (Map.Entry<String, AssortmentRulesVO> entry : lstRules.entrySet()) {
			AssortmentRulesVO asm = entry.getValue();
			JSONObject json = new JSONObject();
			json.put("id", asm.getId());
			json.put("name", asm.getName());
			json.put("values", asm.getValues());
			json.put("seqNbr", asm.getLstSeqNbrs());
			json.put("activeSw", asm.getActiveSw());
			json.put("HebComdId", asm.getHebComdId());
			json.put(SUBDEPT_ID, asm.getDeptId() + asm.getSubDeptId());
			json.put("lvlId", asm.getLvlId());
			json.put("lstUpdateUid", asm.getLstUpdateUid());
			json.put("lstUpdateTs", asm.getLstUpdateTs());
			lstJson.add(json);
		}
		jsonResult.put("data", lstJson);
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * save Commodity Rules to Database.
	 * @param request
	 *            HttpServletRequest
	 * @param lstRules
	 *            List<AssortmentRulesVO>
	 * @return String JSON String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@RequestMapping(value = { UPDATE_COMMODITY_RULES_PATH }, method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
	@ResponseBody
	public String updateRuleSettings(@RequestBody List<AssortmentRulesVO> lstRules, HttpServletRequest request, HttpSession session)
			throws DSVException {
		LOG.info("Enter update Rule Settings function");
		String subDept = request.getParameter("subDeptId");
		String commodity = request.getParameter("commodityID");
		HebUserDetails user = this.commonService.getUserLogin();
		String userId = Constants.EMPTY_STRING;
		if (!Helper.isEmpty(user)) {
			userId = user.getUsername();
			LOG.info(user.getUsername());
		}
		JSONObject jsonResult = new JSONObject();
		List<AssortmentRulesVO> lstAssortmentRulesVOOris = (List<AssortmentRulesVO>) session.getAttribute(ConstantsWeb.RULE_ORI);
		this.updateAssortmentRulesVOInfoStatus(lstRules, lstAssortmentRulesVOOris);
		boolean flag = this.globalRulesService.updateGlobalRulesSetting(userId, subDept, commodity, lstRules);
		if (flag) {
			Map<String, AssortmentRulesVO> lstRulesORiGets = this.globalRulesService.getGlobalRuleSet(subDept, commodity);
			List<AssortmentRulesVO> lstRuleOri = null;
			if (!Helper.isEmpty(lstRulesORiGets)) {
				lstRuleOri = new ArrayList<AssortmentRulesVO>(lstRulesORiGets.values());
			}
			session.setAttribute(ConstantsWeb.RULE_ORI, lstRuleOri);
		}
		jsonResult.put("rs", flag);
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * show Global Rules Screen to manage configuration settings.
	 * @param subDeptId
	 *            String
	 * @param commodityId
	 *            String
	 * @return String
	 * @throws DSVException
	 *             throws Exception if have any errors from Service Layer
	 * @author Duyen.le
	 */
	@RequestMapping(value = { RESET_RULES_PATH }, method = { RequestMethod.POST })
	@ResponseBody
	public String resetRulesToHigherLevel(@RequestParam("subDeptId") String subDeptId, @RequestParam(value = "commodityId", required = false) String commodityId) throws DSVException {
		LOG.info("Reset Rules To Higher Level method.");
		boolean rs = this.globalRulesService.resetRulesToHigherLevel(subDeptId, commodityId);
		return String.valueOf(rs);
	}

	/**
	 * updateAssortmentRulesVOInfoStatus.
	 * @param lstRules
	 *            List<AssortmentRulesVO>
	 * @param lstAssortmentRulesVOOris
	 *            List<AssortmentRulesVO>
	 * @author Duyen.le
	 */
	private void updateAssortmentRulesVOInfoStatus(List<AssortmentRulesVO> lstRules, List<AssortmentRulesVO> lstAssortmentRulesVOOris) {
		if (Helper.isNormalList(lstRules)) {
			if (Helper.isEmpty(lstAssortmentRulesVOOris)) {
				for (AssortmentRulesVO assortmentRulesVO : lstRules) {
					assortmentRulesVO.setActionCd(Constants.STRING_A);
				}
			} else {
				boolean flag = false;
				for (AssortmentRulesVO assortmentRulesVO : lstRules) {
					flag = false;
					for (AssortmentRulesVO assortmentRulesVOOri : lstAssortmentRulesVOOris) {
						if (assortmentRulesVO.getId().equals(assortmentRulesVOOri.getId())) {
							flag = true;
							if (assortmentRulesVOOri.getLvlId() == 0 || assortmentRulesVOOri.isRoot()) {
								assortmentRulesVO.setActionCd(Constants.STRING_A);
							} else {
								assortmentRulesVO.setLvlId(assortmentRulesVOOri.getLvlId());
								assortmentRulesVO.setActionCd(Constants.STRING_U);
							}
							break;
						}
					}
					if (!flag) {
						assortmentRulesVO.setActionCd(Constants.STRING_A);
					}
				}
			}
		}
	}
}
