/*
 * $Id.: $SessionListener.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.utils;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

/**
 * SessionListener Class.
 * @author hai.ngo
 */
public class SessionListener implements HttpSessionListener {
	private static final Logger LOG = Logger.getLogger(SessionListener.class);

	/**
	 * sessionCreated.
	 * @param arg0
	 *            :HttpSessionEvent
	 * @author hai.ngo
	 */
	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		LOG.info("sessionCreated - add one session into counter");
	}

	/**
	 * sessionDestroyed.
	 * @param arg0
	 *            :HttpSessionEvent
	 * @author hai.ngo
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		LOG.info("sessionDestroyed - deduct one session from counter");
		// HttpServletResponse httpServletResponse
		this.handleCustomException();
	}

	/**
	 * handleCustomException.
	 * @return ModelAndView
	 * @author hai.ngo
	 */
	public ModelAndView handleCustomException() {

		// ModelAndView model = new ModelAndView("common/generic_error");
		ModelAndView model = new ModelAndView("login.tile");

		// model.addObject("errMsg", ex.getMessage());
		//
		// StringWriter sw = new StringWriter();
		// ex.printStackTrace(new PrintWriter(sw));
		// model.addObject("errDetal", sw.toString());

		// LOG.error(ex.getMessage(), ex);

		return model;

	}
}
