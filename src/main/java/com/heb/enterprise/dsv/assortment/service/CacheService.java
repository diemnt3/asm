/*
 * $Id: CacheService.java,v 1.0 2014/03/13 11:08:03 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.util.List;
import java.util.Map;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * CacheService interface.
 * @author Duyen.Le
 */
public interface CacheService {
	/**
	 * cache all data into memory.
	 * @author Duyen.Le
	 * @throws DSVException
	 *             :DSVException
	 */
	void refreshCaches() throws DSVException;

	/**
	 * getListSubDepartmentFromCache - get List Sub-Department From cache.
	 * @author Duyen.Le
	 * @return List<SubDepartmentVO>
	 * @throws DSVException
	 *             :DSVException
	 */
	List<DepartmentVO> getHierachyForGlobalRulesFromCache() throws DSVException;

	/**
	 * refreshSubDepartmentCache : cache All info of vendor from Vendor WS into memory.
	 * @author Duyen.Le
	 * @throws DSVException
	 *             :DSVException
	 */
	void refreshHierachyForGlobalFromCache() throws DSVException;

	/**
	 * Get list Commodity.
	 * @author anhtran.
	 * @throws DSVException
	 *             :DSVException
	 * @return List<CommodityVO>
	 */
	List<CommodityVO> getCommodityList() throws DSVException;

	/**
	 * Get all list vendors.
	 * @author anhtran.
	 * @return List<VendorInfoVO>
	 */
	List<VendorInfoVO> getAllListVendors();

	/**
	 * remove all Vendors from Cache.
	 * @author duyen.le
	 */
	void refreshVendorCache();

	/**
	 * getBrandsFromCache - get List of Brands From cache.
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	List<BaseVO> getBrandsFromCache();

	/**
	 * getBrandsFromCacheByBrand - get List of Brands From cache by brand's name.
	 * @param brandName
	 *            String
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	List<BaseVO> getBrandsFromCacheByBrand(String brandName);

	/**
	 * refreshBrandsFromCache : cache All info of Brand from WS into memory.
	 * @author Duyen.Le
	 */
	void refreshBrandsFromCache();

	/**
	 * getProductsFromCacheByProduct - get List of Brands From cache by Product's name.
	 * @param productName
	 *            String
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	List<BaseVO> getCommoditiesFromCacheByCommodity(String productName);

	/**
	 * refreshProductsFromCache : cache All info of Products from Products WS into memory.
	 * @author Duyen.Le
	 */
	void refreshCommoditiesFromCache();

	/**
	 * Get list sub-commodities by CommodityId.
	 * @param commodityId
	 *            String
	 * @author Duyen.Le
	 * @return List<SubCommodityVO>
	 * @throws DSVException
	 *             if have any errors when getting data from Cache
	 */
	List<SubCommodityVO> getSubCommByCommodityId(String commodityId) throws DSVException;

	/**
	 * getSubCommodityList.
	 * @return Map<String, SubCommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	Map<String, SubCommodityVO> getSubCommodityList() throws DSVException;

	/**
	 * getCommodityDetailList.
	 * @return Map<String, SubCommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	Map<Integer, CommodityVO> getCommodityDetailMap() throws DSVException;

	/**
	 * Get Commodity Map.
	 * @author anhtran.
	 * @throws DSVException
	 *             :DSVException
	 * @return Map<Integer, CommodityVO>
	 */
	Map<Integer, CommodityVO> getCommodityMap() throws DSVException;

	/**
	 * getCommodityDetailList.
	 * @throws DSVException
	 *             :DSVException
	 * @return List<CommodityVO>
	 * @author anhtran.
	 */
	List<CommodityVO> getCommodityDetailList() throws DSVException;

	/**
	 * searchCommodityList.
	 * @param query
	 *            String
	 * @return List<CommodityVO>
	 * @author thangdang.
	 */
	List<CommodityVO> searchCommodityList(String query);

	/**
	 * searchbrand.
	 * @param query
	 *            String
	 * @return List<BaseVO>
	 * @author thangdang.
	 */
	List<BaseVO> searchbrand(String query);

	/**
	 * remove all Brand from Cache.
	 * @author thangdang
	 */
	void refreshBrandCache();

	/**
	 * getAllBrandFromCache.
	 * @return List<BaseVO>
	 * @author thangdang.
	 */
	List<BaseVO> getAllBrandFromCache();

	/**
	 * searchProductDesc.
	 * @param query
	 *            String
	 * @return List<String>
	 * @author thangdang.
	 */
	List<String> searchProductDesc(String query);

	/**
	 * refreshProdDescCache.
	 * @author thangdang.
	 */
	void refreshProdDescCache();

	/**
	 * getAllProductDesc.
	 * @author thangdang.
	 */
	List<ProductRiewVO> getAllProductDesc();
}
