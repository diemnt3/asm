/*
 * $Id: VendorRulesServiceImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.heb.enterprise.dsv.assortment.dao.GlobalRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.RulesDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.VendorRulesService;
import com.heb.enterprise.dsv.assortment.utils.Convert;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.AttrAndRuleVendorVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.VendorAttributeVO;

/**
 * VendorRulesDAO interface.
 * @author duyen.le
 */
@Service
public class VendorRulesServiceImpl implements VendorRulesService {
	private static final Logger LOG = Logger.getLogger(VendorRulesServiceImpl.class);
	@Autowired
	private CacheService cacheService;
	/**
	 * inject RulesDao.
	 */
	@Autowired
	private RulesDAO ruleDao;
	/**
	 * inject RulesDao.
	 */
	@Autowired
	private GlobalRulesDAO globalDao;

	@Override
	public Map<String, AssortmentRulesVO> getRuleSettingsForVendor(String vendorId, String commodityId,
			String subCommodityId) throws DSVException {
		String subDeptId = Constants.EMPTY_STRING;
		String deptId = Constants.EMPTY_STRING;
		List<DepartmentVO> lstDepartmentVOs = this.cacheService.getHierachyForGlobalRulesFromCache();
		AssortmentRulesVO asmVO = Convert.getHierachy(lstDepartmentVOs, null, commodityId, subCommodityId);

		deptId = asmVO.getDeptId();
		subDeptId = asmVO.getSubDeptId();

		return this.ruleDao.getRuleSettings(vendorId, commodityId, subCommodityId, deptId, subDeptId);
	}

	/**
	 * updateRuleSettingsForVendor method.
	 * @param vendorAsmVo
	 *            AttrAndRuleVendorVO
	 * @return boolean
	 * @author duyen.le
	 */
	@Override
	@Transactional("transactionManager")
	public boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO vendorAsmVo) {
		boolean success = false;
		if (vendorAsmVo != null) {
			try {
				List<DepartmentVO> lstDepartmentVOs = this.cacheService.getHierachyForGlobalRulesFromCache();
				this.insertRuleDefine(vendorAsmVo, lstDepartmentVOs);

				success = true;
			} catch (DSVException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return success;
	}

	/**
	 * insertRuleDefine method.
	 * @param vendorAsmVo
	 *            AttrAndRuleVendorVO
	 * @param lstDepartmentVOs
	 *            List<DepartmentVO> @throws DSVException DSVException
	 * @author duyen.le
	 */
	private void insertRuleDefine(AttrAndRuleVendorVO vendorAsmVo, List<DepartmentVO> lstDepartmentVOs) throws DSVException {
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		if (Helper.isNormalList(vendorAsmVo.getLstVendorRules())) {
			for (AssortmentRulesVO asm : vendorAsmVo.getLstVendorRules()) {
				Convert.convertAssortmentRulesVO(asm, lstRuleDefinationVOs);
			}
			long lvlID = this.ruleDao.checkLVL(vendorAsmVo.getVendorId(), vendorAsmVo.getCommodityId(), vendorAsmVo.getSubCommdId());
			if (lvlID > 0) {
				// lvlID
				this.ruleDao.updateRuleValuesToDfns(lstRuleDefinationVOs, lvlID, vendorAsmVo.getUserId());
			} else {
				String deptId = Constants.WHITE_SPACE;
				String subDeptId = Constants.WHITE_SPACE;
				long itmCls = 0;
				if (!Helper.isEmpty(vendorAsmVo.getCommodityId()) || !Helper.isEmpty(vendorAsmVo.getSubCommdId())) {
					AssortmentRulesVO assortmentRulesVOTmp = Convert.getHierachy(lstDepartmentVOs, null,
							vendorAsmVo.getCommodityId(), vendorAsmVo.getSubCommdId());
					deptId = assortmentRulesVOTmp.getDeptId();
					subDeptId = assortmentRulesVOTmp.getSubDeptId();
					itmCls = assortmentRulesVOTmp.getItmCls();
				}
				lvlID = this.globalDao.getMaxLVLID();
				lvlID = lvlID + 1;
				lvlID = this.globalDao
						.insertlvl(lvlID, vendorAsmVo.getUserId(), vendorAsmVo.getCommodityId(), deptId, subDeptId, itmCls, vendorAsmVo.getVendorId(), vendorAsmVo.getSubCommdId(), null);
				this.globalDao.insertRulesToDfns(lstRuleDefinationVOs, lvlID, vendorAsmVo.getUserId());
			}
		}
	}

	/**
	 * ajaxGetCommodityandSub method.
	 * @return List<CommodityVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	@Override
	public List<CommodityVO> ajaxGetCommodityandSub() throws DSVException {
		// hard code
		// TODO Auto-generated method stub
		List<CommodityVO> lstCommodity = new ArrayList<CommodityVO>();
		CommodityVO com1 = new CommodityVO();
		com1.setCommodityCd(1);
		com1.setCommodityDesc("Commodity 1");
		SubCommodityVO sub1 = new SubCommodityVO();
		sub1.setSubCommodityCd(5833);
		sub1.setSubCommodityDesc("5833 CELL PHONE ACC-EXPERIENCE");

		SubCommodityVO sub2 = new SubCommodityVO();
		sub2.setSubCommodityCd(5832);
		sub2.setSubCommodityDesc("5832 CELL PHONE ACC-ANDROID");

		SubCommodityVO sub3 = new SubCommodityVO();
		sub3.setSubCommodityCd(5831);
		sub3.setSubCommodityDesc("5831 CELL PHONE ACC-IPHONE");

		List<SubCommodityVO> lstSubCom = new ArrayList<SubCommodityVO>();
		lstSubCom.add(sub1);
		lstSubCom.add(sub2);
		lstSubCom.add(sub3);
		com1.setSubCommodityVOs(lstSubCom);
		lstCommodity.add(com1);
		return lstCommodity;
	}

	/**
	 * get list of Vendor Attributes.
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	@Override
	public List<VendorAttributeVO> getVendorAttr() throws DSVException {
		return this.ruleDao.getVendorAttr();
	}

	/**
	 * get list of Vendor Attributes and Rule for Vendor rule by VendorID.
	 * @param vendorID
	 *            String
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	@Override
	public AttrAndRuleVendorVO getRuleSettingsByVendor(String vendorID) throws DSVException {
		return this.ruleDao.getRuleSettingsByVendor(vendorID);
	}

	/**
	 * get Active Switch for subComd.
	 * @param vendorId
	 *            String
	 * @return List<String>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */

	@Override
	public List<Integer> getActiveSw(String vendorId) throws DSVException {
		return this.ruleDao.getActiveSw(vendorId);
	}

	@Override
	@Transactional("transactionManager")
	public boolean resetRulesToHigherLevel(String vendorId, String commodityId, String subCommodityId) {
		boolean rs = false;
		try {
			long lvlId = this.ruleDao.checkLVL(vendorId, commodityId, subCommodityId);

			if (lvlId > 0) {
				this.globalDao.removeRule(lvlId);
				this.globalDao.removeLvl(lvlId);
			}
			rs = true;
		} catch (DSVException e) {
			LOG.error(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * get subCommodity override for Assortment Rule.
	 * @param vendorId
	 *            String
	 * @param lvlType
	 *            String
	 * @return List<String>
	 * @author duyen.le
	 */
	@Override
	public List<Integer> getAsmLVLModified(String lvlType, String vendorId) {
		// TODO Auto-generated method stub
		return this.ruleDao.getAsmLVLModified(lvlType, vendorId);
	}

	/**
	 * activateRule.
	 * @param subComMapped
	 *            Map<String, String>
	 * @param userId
	 *            String
	 * @param type
	 *            Type are Commodity or SubCommodity :String
	 * @param value
	 *            Commodity ID or SubCommodity ID:String
	 * @return boolean
	 * @author Duyen.le
	 */
	@Override
	public boolean activateRule(String type, String value, String vendorId, String userId, Map<String, String> subComMapped) {
		boolean flag = false;
		if ("Commodity".equals(type)) {
			try {
				List<DepartmentVO> lstDepartmentVOs = this.cacheService.getHierachyForGlobalRulesFromCache();
				List<SubCommodityVO> lstSubCommodityVOs = this.cacheService.getSubCommByCommodityId(value);
				if (!Helper.isEmpty(lstSubCommodityVOs)) {
					List<String> lstHebSubComActivated = this.ruleDao.getSubCommodityActivated(vendorId);
					List<String> lstSubcommoditys = new ArrayList<String>();
					for (SubCommodityVO subCommodityVO : lstSubCommodityVOs) {
						for (Map.Entry<String, String> a : subComMapped.entrySet()) {
							if (subCommodityVO.getSubCommodityCd() == (Integer.parseInt(a.getKey()))) {
								if (!Helper.isEmpty(lstHebSubComActivated) && !lstHebSubComActivated.contains(String.valueOf(subCommodityVO.getSubCommodityCd()))
										|| Helper.isEmpty(lstHebSubComActivated)) {
									lstSubcommoditys.add(String.valueOf(subCommodityVO.getSubCommodityCd()));
								}
							}
						}
					}
					List<AssortmentRulesVO> lstAssortmentRulesVOs = Convert.getPathSubComm(lstDepartmentVOs, lstSubcommoditys);
					long lvlIdMax = this.globalDao.getMaxLVLID();
					lvlIdMax = lvlIdMax + 1;
					this.globalDao.insertlvlActivated(userId, lstAssortmentRulesVOs, vendorId, lvlIdMax, value);
					flag = true;
				}
			} catch (DSVException e) {
				LOG.error(e.getMessage(), e);
			}
		} else {
			try {
				List<DepartmentVO> lstDepartmentVOs = this.cacheService.getHierachyForGlobalRulesFromCache();
				List<String> lstHebSubComActivated = this.ruleDao.getSubCommodityActivated(vendorId);
				List<String> lstSubcommoditys = new ArrayList<String>();
				if (!Helper.isEmpty(lstHebSubComActivated) && !lstHebSubComActivated.contains(value) || Helper.isEmpty(lstHebSubComActivated)) {
					lstSubcommoditys.add(value);
				}
				List<AssortmentRulesVO> lstAssortmentRulesVOs = Convert.getPathSubComm(lstDepartmentVOs, lstSubcommoditys);
				long lvlIdMax = this.globalDao.getMaxLVLID();
				lvlIdMax = lvlIdMax + 1;
				this.globalDao.insertlvlActivated(userId, lstAssortmentRulesVOs, vendorId, lvlIdMax, null);
				flag = true;
			} catch (DSVException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return flag;
	}

	/**
	 * deActivateRule.
	 * @param vendor
	 *            String
	 * @param subCommodityMap
	 *            Map<String, String>
	 * @param type
	 *            Type are Commodity or SubCommodity :String
	 * @param value
	 *            Commodity ID or SubCommodity ID:String
	 * @return boolean
	 * @author Duyen.le
	 */
	@Override
	public boolean deActivateRule(String type, String value, String vendorId, Map<String, String> subComMapped) {
		boolean flag = false;
		List<String> lstSubcommoditys = new ArrayList<String>();
		if ("Commodity".equals(type)) {
			try {
				List<SubCommodityVO> lstSubCommodityVOs = this.cacheService.getSubCommByCommodityId(value);
				if (!Helper.isEmpty(lstSubCommodityVOs)) {
					for (SubCommodityVO subCommodityVO : lstSubCommodityVOs) {
						for (Map.Entry<String, String> a : subComMapped.entrySet()) {
							if (subCommodityVO.getSubCommodityCd() == (Integer.parseInt(a.getKey()))) {
								lstSubcommoditys.add(String.valueOf(subCommodityVO.getSubCommodityCd()));
							}
						}
					}
					this.ruleDao.removeSubCommodityActivated(vendorId, lstSubcommoditys);
					flag = true;
				}

			} catch (DSVException e) {
				LOG.error(e.getMessage(), e);
			}
		} else {
			lstSubcommoditys.add(value);
			this.ruleDao.removeSubCommodityActivated(vendorId, lstSubcommoditys);
			flag = true;
		}
		return flag;
	}

	/**
	 * getLstSubActivateBySub.
	 * @return List<LevelVO>
	 * @author Duyen.le
	 */

	@Override
	public List<LevelVO> getLstSubActivateBySub() throws DSVException {
		// TODO Auto-generated method stub
		return this.ruleDao.getLstSubActivateBySub();
	}
}
