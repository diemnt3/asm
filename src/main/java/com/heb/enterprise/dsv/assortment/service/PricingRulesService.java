/*
 * $Id: PricingRulesService.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.util.List;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.PricingRuleVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;

/**
 * PricingRulesService.
 * @author Duyen.le.
 */
public interface PricingRulesService {
	/**
	 * getListCommodities - get List of Commodities.
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 * @throws DSVException
	 *             DSVException
	 */
	List<CommodityVO> getListCommodities() throws DSVException;

	/**
	 * getList Sub-Commodities by commodityId.
	 * @param commodityId
	 *            String
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 * @throws DSVException
	 *             - if have any errors from getting data.
	 */
	List<SubCommodityVO> getListSubCommByCommodityId(String commodityId) throws DSVException;

	/**
	 * getOnlineCompetitors - get List of Competitors.
	 * @author Duyen.Le
	 * @return List<CompetitiveRetails>
	 */
	List<BaseVO> getOnlineCompetitors();

	/**
	 * Gets the pricing rule defns.
	 * @param vendorId
	 *            String
	 * @param commodityId
	 *            String
	 * @param subCommodityId
	 *            String
	 * @return List<AssortmentRulesVO>
	 * @author thangdang
	 */
	PricingRuleVO getPricingRules(String vendorId, String commodityId, String subCommodityId);

	/**
	 * Update pricing rules.
	 * @param userId
	 *            String
	 * @param pricingRuleVO
	 *            PricingRuleVO
	 * @return String
	 * @author thangdang
	 * @throws DSVException
	 *             DSVException
	 */
	String updatePricingRules(String userId, PricingRuleVO pricingRuleVO) throws DSVException;

	/**
	 * getLvlId.
	 * @param vendorId
	 *            String
	 * @param commodity
	 *            String
	 * @param subCommodity
	 *            String
	 * @return long
	 * @author thangdang
	 */
	long getLvlId(String vendorId, String commodity, String subCommodity);

	/**
	 * getActTyp.
	 * @return List<BaseVO>
	 * @author thangdang
	 */
	List<BaseVO> getActTyp();

}
