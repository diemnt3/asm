/*
 * $Id: PricingRulesDAO.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao;

import java.util.List;
import java.util.Map;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.AttrAndRuleVendorVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.VendorAttributeVO;

/**
 * PricingRulesDAO.
 * @author Duyen.le.
 */
public interface RulesDAO {
	/**
	 * getRuleSettings method.
	 * @param commodityId
	 *            String
	 * @param subCommodityId
	 *            String
	 * @param vendorID
	 *            String
	 * @param deptId
	 *            String
	 * @param subDeptId
	 *            String
	 * @return Map<String, List<AssortmentRulesVO>>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	Map<String, AssortmentRulesVO> getRuleSettings(String vendorID, String commodityId, String subCommodityId,
			String deptId, String subDeptId) throws DSVException;

	/**
	 * get list of Vendor Attributes.
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	List<VendorAttributeVO> getVendorAttr() throws DSVException;

	/**
	 * get list of Vendor Attributes and Rule for Vendor rule by VendorID.
	 * @param vendorID
	 *            String
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	AttrAndRuleVendorVO getRuleSettingsByVendor(String vendorID) throws DSVException;

	/**
	 * get Active Switch for subComd.
	 * @param vendorId
	 *            String
	 * @return List<String>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	List<Integer> getActiveSw(String vendorId) throws DSVException;

	/**
	 * Update Vendor Attrs.
	 * @param vendor
	 *            String
	 * @param lstVendorAttrs
	 *            List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @return boolean
	 * @author duyen.le
	 */
	boolean updateVendorAttr(List<VendorAttributeVO> lstVendorAttrs, String vendor) throws DSVException;

	/**
	 * updateRuleValuesToDfns method.
	 * @param lvlID
	 *            long
	 * @param lstRulesDfns
	 *            List<RuleDefinationVO>
	 * @param userId
	 *            String
	 * @return boolean
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	String updateRuleValuesToDfns(List<RuleDefinationVO> lstRulesDfns, long lvlID, String userId) throws DSVException;

	/**
	 * get LVl method.
	 * @param vendorId
	 *            String
	 * @param commodityId
	 *            String
	 * @param subCommodityId
	 *            String
	 * @return long
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	long checkLVL(String vendorId, String commodityId, String subCommodityId) throws DSVException;

	/**
	 * removeSubCommodityActivated.
	 * @param vendorId
	 *            String
	 * @return boolean
	 * @author thangdang
	 */
	boolean removeSubCommodityActivated(String vendorId);

	/**
	 * removeSubCommodityActivated.
	 * @param vendorId
	 *            String
	 * @param lstSubComms
	 *            List<String>
	 * @return boolean
	 * @author thangdang
	 */
	boolean removeSubCommodityActivated(String vendorId, List<String> lstSubComms);

	/**
	 * getLvlByCommodityVendor.
	 * @param vendorId
	 *            String
	 * @param lstCommoditys
	 *            List<String>
	 * @return List<Long>
	 * @author thangdang
	 */
	List<Long> getLvlByCommodityVendor(String vendorId, List<String> lstCommoditys);

	/**
	 * lstSubCommoditys.
	 * @param vendorId
	 *            String
	 * @param lstSubCommoditys
	 *            List<String>
	 * @return List<Long>
	 * @author thangdang
	 */
	List<Long> getLvlBySubCommodityVendor(String vendorId, List<String> lstSubCommoditys);

	/**
	 * getSubCommodityActivated.
	 * @param vendorId
	 *            String
	 * @return List<String>
	 * @author thangdang
	 */
	List<String> getSubCommodityActivated(String vendorId);

	/**
	 * get subCommodity override for Assortment Rule.
	 * @param vendorId
	 *            String
	 * @param lvlType
	 *            String
	 * @return List<Integer>
	 * @author duyen.le
	 */
	List<Integer> getAsmLVLModified(String lvlType, String vendorId);

	/**
	 * updatelvlTyp.
	 * @param lvlId
	 *            long
	 * @param lvlTyp
	 *            String
	 * @return String
	 * @author thangdang
	 * @throws DSVException
	 *             DSVException
	 */
	String updatelvlTyp(long lvlId, String lvlTyp) throws DSVException;

	/**
	 * Update rule values to dfns pricing rules.
	 * @param lstRulesDfns
	 *            the lst rules dfns
	 * @param lvlId
	 *            the lvl id
	 * @param userId
	 *            the user id
	 * @return the string
	 * @throws DSVException
	 *             the DSV exception
	 * @author quang.phan
	 */
	String updateRuleValuesToDfnsPricingRules(final List<RuleDefinationVO> lstRulesDfns, final long lvlId, final String userId) throws DSVException;

	/**
	 * getLstSubActivateBySub.
	 * @return List<LevelVO>
	 * @author Duyen.le
	 */
	List<LevelVO> getLstSubActivateBySub() throws DSVException;
}
