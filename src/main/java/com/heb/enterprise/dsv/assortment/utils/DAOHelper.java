/*
 * $Id: DAOHelper.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;

/**
 * Helper for DAO layers.
 * @author duyen.le
 */
public final class DAOHelper {
	/**
	 * Private constructor.
	 */
	private DAOHelper() {
	}

	/**
	 * Convert Collections Rule To Map.
	 * @param lstRuleVOs
	 *            List<AssortmentRulesVO>
	 * @author duyen.le
	 * @return Map<String, AssortmentRulesVO>
	 */
	public static Map<String, AssortmentRulesVO> convertColectionRuleToMap(List<AssortmentRulesVO> lstRuleVOs) {
		Map<String, AssortmentRulesVO> ruleAsHashMap = new HashMap<String, AssortmentRulesVO>();
		List<String> lstRuleNames = new ArrayList<String>();
		AssortmentRulesVO ruleVO;
		String key = Constants.EMPTY_STRING;
		List<Integer> lstSq = null;
		for (AssortmentRulesVO ruleVOTmp : lstRuleVOs) {
			if (Helper.isEmpty(ruleVOTmp.getSubDeptId()) && Helper.isEmpty(ruleVOTmp.getDeptId())) {
				ruleVOTmp.setRoot(true);
			} else {
				ruleVOTmp.setRoot(false);
			}
			// System.out.println("DAO Root : " + ruleVOTmp.isRoot());
			key = ruleVOTmp.getId();
			if (!lstRuleNames.contains(key)) {
				lstSq = new ArrayList<Integer>();
				lstSq.add(ruleVOTmp.getSeqNbr());
				ruleVO = ruleVOTmp;
				lstRuleNames.add(key);
				ruleAsHashMap.put(ruleVOTmp.getId(), ruleVO);
			} else {
				if (!lstSq.contains(ruleVOTmp.getSeqNbr())) {
					lstSq.add(ruleVOTmp.getSeqNbr());
					ruleAsHashMap.get(ruleVOTmp.getId()).getValues().add(ruleVOTmp.getValue());
					ruleAsHashMap.get(ruleVOTmp.getId()).setLstSeqNbrs(lstSq);
				}
			}
		}
		return ruleAsHashMap;
	}
}
