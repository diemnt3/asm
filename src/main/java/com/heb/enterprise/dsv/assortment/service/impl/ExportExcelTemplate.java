/*
 * $Id.: $ExportExcelTemplate.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.stereotype.Component;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.InvoiceVO;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.RetailCostVO;

/**
 * ExportExcelTemplate.
 * @author Duyen.le
 */
@Component
public class ExportExcelTemplate {
	public static final String FONT_CALIBRI = "Calibri";
	private static final Logger LOG = Logger.getLogger(ExportExcelTemplate.class);
	// private HSSFHyperlink emailLink;
	// private HSSFCellStyle emailCellStyle;
	private HSSFCellStyle currencyCellStyle;
	private HSSFCellStyle currencyNoBorderCellStyle;
	private HSSFCellStyle currencyCellStyle4;
	private HSSFCellStyle currencyCellStyle4NoBorderBot;
	private HSSFCellStyle normalCellStyle;
	private HSSFCellStyle numberCellStyle;
	private HSSFCellStyle numberCellStyleNoBorderBot;
	private HSSFCellStyle centerCellStyleTitle;
	private HSSFCellStyle normalNoBorderCellStyle;
	private HSSFCellStyle normalNoBorderCellStyleBigSize;
	private HSSFCellStyle lableCellStyleCenter;
	private HSSFCellStyle normalLableNoBorderCellStyle;
	private HSSFCellStyle normalLableNoBorderCellStyleBigSize;
	private HSSFCellStyle normalCellStyleNoBorderBot;
	private HSSFCellStyle emptyCellBorderTop;
	private HSSFCellStyle normalCellStyleRightNoBorderBot;
	private HSSFCellStyle centerCellStyle;

	/**
	 * Get excel document.
	 * @param lstProdt
	 *            {@link List}<{@link ProductRiewVO}> for Review
	 * @return The workbook of POI library have byte array of excel file
	 * @throws DSVException
	 *             If cannot get excel template
	 * @author Duyen.le
	 */
	public HSSFWorkbook getExcelProduct(List<ProductRiewVO> lstProdt) throws DSVException {
		HSSFWorkbook workbook = null;
		try {
			InputStream is = this.getClass().getResourceAsStream("/excel_list_product_review_template.xls");
			workbook = new HSSFWorkbook(is);
			HSSFSheet excelSheet = workbook.getSheetAt(AntiMagicNumber.ZERO);
			excelSheet.setActive(true);
			excelSheet.showInPane(AntiMagicNumber.ZERO, AntiMagicNumber.ZERO);
			this.currencyCellStyle = this.getCurrencyCellStyle(workbook);
			this.currencyCellStyle4 = this.getCurrencyCellStyle(workbook, AntiMagicNumber.FOUR);
			this.normalCellStyle = this.getCellStyle(workbook, HSSFCellStyle.ALIGN_LEFT);
			this.numberCellStyle = this.getCellStyle(workbook, HSSFCellStyle.ALIGN_RIGHT);
			this.setDataForProduct(workbook, excelSheet, lstProdt);

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return workbook;
	}

	/**
	 * Set data into existing file.
	 * @param workbook
	 *            HSSFWorkbook
	 * @param excelSheet
	 *            HSSFSheet
	 * @param listProdt
	 *            List<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 * @author Duyen.le
	 */
	private void setDataForProduct(HSSFWorkbook workbook, HSSFSheet excelSheet,
			List<ProductRiewVO> listProdt) throws DSVException {
		int rowNum = AntiMagicNumber.ONE;
		if (Helper.isNormalList(listProdt)) {
			for (ProductRiewVO model : listProdt) {
				HSSFRow excelRow = excelSheet.createRow(rowNum++);
				this.setProductDataRow(model, excelRow, rowNum);
			}
		}
	}

	/**
	 * Set data row of invoice.
	 * @param model
	 *            {@link InvoiceVO}
	 * @param excelRow
	 *            {@link HSSFRow}
	 * @author Duyen.le
	 */
	private void setProductDataRow(ProductRiewVO model, HSSFRow excelRow, int rowNum) {
		this.createCell(String.valueOf(rowNum - 1), excelRow, AntiMagicNumber.ZERO, this.numberCellStyle);
		this.createCell(model.getUpc(), excelRow, AntiMagicNumber.ONE, this.numberCellStyle);
		this.createCell(model.getDescription(), excelRow, AntiMagicNumber.TWO, this.normalCellStyle);
		this.createCell(model.getSize(), excelRow, AntiMagicNumber.THREE, this.numberCellStyle);
		this.createCell(model.getMap(), excelRow, AntiMagicNumber.FOUR, this.numberCellStyle);
		this.createCell(model.getPrePrice(), excelRow, AntiMagicNumber.FIVE, this.numberCellStyle);
		this.createCell(model.getMsrp(), excelRow, AntiMagicNumber.SIX, this.numberCellStyle);

		this.createCell(model.getSrp(), excelRow, AntiMagicNumber.SEVEN, this.numberCellStyle);
		this.createCell(model.getCost(), excelRow, AntiMagicNumber.EIGHT, this.numberCellStyle);
		this.createCell(model.getGpPercent(), excelRow, AntiMagicNumber.NINE, this.numberCellStyle);
		this.createCell(model.getPennyProfit(), excelRow, AntiMagicNumber.TEN, this.numberCellStyle);
		this.createCell(model.getHebRetail(), excelRow, AntiMagicNumber.ELEVEN, this.numberCellStyle);
		this.createCell(model.getImageUri(), excelRow, AntiMagicNumber.TWELVE, this.normalCellStyle);
		this.createCell(model.getFailedReason(), excelRow, AntiMagicNumber.THIRTEEN, this.normalCellStyle);
		this.createCell(model.getStatus(), excelRow, AntiMagicNumber.FOURTEEN, this.normalCellStyle);
		this.createCell(model.getFirstReceivedTS(), excelRow, AntiMagicNumber.FIFTEEN, this.normalCellStyle);
		this.createCell(model.getLastEditedTS(), excelRow, AntiMagicNumber.SIXTEEN, this.normalCellStyle);
	}

	/**
	 * Create string cell.
	 * @param cellValue
	 *            The string was added into cell
	 * @param row
	 *            The row will be create cell
	 * @param column
	 *            The column will be create cell
	 * @param cellStyle
	 *            The cell style
	 * @author anhtran
	 */
	private void createCell(String cellValue, HSSFRow row, int column, HSSFCellStyle cellStyle) {
		HSSFCell cell = row.createCell(column);
		cell.setCellValue(cellValue);
		cell.setCellStyle(cellStyle);
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
	}

	/**
	 * Get currency cell style.
	 * @param workbook
	 *            The workbook of POI library
	 * @return The currency cell style of POI library
	 * @author ANNGUYEN
	 */
	private HSSFCellStyle getCurrencyCellStyle(HSSFWorkbook workbook) {
		return this.getCurrencyCellStyle(workbook, AntiMagicNumber.TWO);
	}

	/**
	 * Get currency cell style.
	 * @param workbook
	 *            The workbook of POI library
	 * @return The currency cell style of POI library
	 * @author ANNGUYEN
	 */
	private HSSFCellStyle getCurrencyCellStyle(HSSFWorkbook workbook, int frac) {
		HSSFCellStyle curCellStyle = null;
		curCellStyle = this.getCellStyle(workbook, HSSFCellStyle.ALIGN_RIGHT);
		short type = (short) AntiMagicNumber.EIGHT;
		if (frac == AntiMagicNumber.FOUR) {
			type = workbook.getCreationHelper().createDataFormat().getFormat(Constants.DECIMAL_FORMAT_EXCEL_FOUR);
		}
		curCellStyle.setDataFormat(type);
		return curCellStyle;
	}

	/**
	 * Get cell style.
	 * @param workbook
	 *            The workbook of POI library
	 * @param align
	 *            The value define cell align
	 * @return The cell style of POI library
	 * @author anhtran
	 */
	private HSSFCellStyle getCellStyle(HSSFWorkbook workbook, short align) {
		HSSFFont fontCell = workbook.createFont();
		fontCell.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		fontCell.setFontName(FONT_CALIBRI);
		HSSFCellStyle cellStyle = null;
		cellStyle = workbook.createCellStyle();
		cellStyle.setWrapText(false);
		cellStyle.setFont(fontCell);
		cellStyle.setAlignment(align);
		cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		return cellStyle;
	}

	// -----------------------------

	/**
	 * Get excel document.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @return The workbook of POI library have byte array of excel file
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran
	 */
	public HSSFWorkbook getExcelCostReview(List<RetailCostVO> retailCostVOs) throws DSVException {
		HSSFWorkbook workbook = null;
		try {
			InputStream is = this.getClass().getResourceAsStream("/excel_list_cost_review_template.xls");
			workbook = new HSSFWorkbook(is);
			HSSFSheet excelSheet = workbook.getSheetAt(AntiMagicNumber.ZERO);
			excelSheet.setActive(true);
			excelSheet.showInPane(AntiMagicNumber.ZERO, AntiMagicNumber.ZERO);
			this.currencyCellStyle = this.getCurrencyCellStyle(workbook);
			this.currencyCellStyle4 = this.getCurrencyCellStyle(workbook, AntiMagicNumber.FOUR);
			this.normalCellStyle = this.getCellStyle(workbook, HSSFCellStyle.ALIGN_LEFT);
			this.numberCellStyle = this.getCellStyle(workbook, HSSFCellStyle.ALIGN_RIGHT);
			this.centerCellStyle = this.getCellStyle(workbook, HSSFCellStyle.ALIGN_CENTER);
			this.setDataForCostReview(workbook, excelSheet, retailCostVOs);

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return workbook;
	}

	/**
	 * Set data into existing file.
	 * @param workbook
	 *            HSSFWorkbook
	 * @param excelSheet
	 *            HSSFSheet
	 * @param listProdt
	 *            List<RetailCostVO>
	 * @throws DSVException
	 *             DSVException
	 * @author anhtran
	 */
	private void setDataForCostReview(HSSFWorkbook workbook, HSSFSheet excelSheet,
			List<RetailCostVO> retailCostVOs) throws DSVException {
		int rowNum = AntiMagicNumber.TWO;
		if (Helper.isNormalList(retailCostVOs)) {
			for (RetailCostVO model : retailCostVOs) {
				HSSFRow excelRow = excelSheet.createRow(rowNum++);
				this.setCostReviewDataRow(model, excelRow, rowNum);
			}
		}
	}

	/**
	 * setCostReviewDataRow.
	 * @param model
	 *            :RetailCostVO
	 * @param excelRow
	 *            :HSSFRow
	 * @param rowNum
	 *            :int
	 * @author anhtran.
	 */
	private void setCostReviewDataRow(RetailCostVO model, HSSFRow excelRow, int rowNum) {
		this.createCell(String.valueOf(rowNum - 2), excelRow, AntiMagicNumber.ZERO, this.numberCellStyle);
		this.createCell(String.valueOf(model.getProdScnCd()), excelRow, AntiMagicNumber.ONE, this.numberCellStyle);
		this.createCell(model.getProdDesc(), excelRow, AntiMagicNumber.TWO, this.normalCellStyle);
		this.createCell(model.getPackSize(), excelRow, AntiMagicNumber.THREE, this.normalCellStyle);
		this.createCell(String.valueOf(model.getCurrentCost()), excelRow, AntiMagicNumber.FOUR, this.numberCellStyle);
		this.createCell(String.valueOf(model.getCurrentRetail()), excelRow, AntiMagicNumber.FIVE, this.numberCellStyle);
		this.createCell(model.getCurrentMap(), excelRow, AntiMagicNumber.SIX, this.centerCellStyle);
		this.createCell(model.getCurrentComp(), excelRow, AntiMagicNumber.SEVEN, this.centerCellStyle);
		this.createCell(String.valueOf(model.getCurrentGP()), excelRow, AntiMagicNumber.EIGHT, this.numberCellStyle);
		this.createCell(model.getNextPackSize(), excelRow, AntiMagicNumber.NINE, this.numberCellStyle);
		this.createCell(String.valueOf(model.getNextCost()), excelRow, AntiMagicNumber.TEN, this.numberCellStyle);
		this.createCell(String.valueOf(model.getNextRetail()), excelRow, AntiMagicNumber.ELEVEN, this.numberCellStyle);
		this.createCell(model.getNextMap(), excelRow, AntiMagicNumber.TWELVE, this.centerCellStyle);
		this.createCell(String.valueOf(model.getNextGP()), excelRow, AntiMagicNumber.THIRTEEN, this.numberCellStyle);
		this.createCell(model.getStatus(), excelRow, AntiMagicNumber.FOURTEEN, this.centerCellStyle);
		this.createCell(String.valueOf(model.getNextEffectiveDate()), excelRow, AntiMagicNumber.FIFTEEN, this.normalCellStyle);
		this.createCell(model.getErrTypDes(), excelRow, AntiMagicNumber.SIXTEEN, this.normalCellStyle);
	}

}
