/*
 * $Id: ProductsDAOImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.dao.ProductsDAO;
import com.heb.enterprise.dsv.assortment.dao.mapper.ProductMapper;
import com.heb.enterprise.dsv.assortment.utils.AConstants;
import com.heb.enterprise.dsv.assortment.utils.AHelper;
import com.heb.enterprise.dsv.assortment.utils.Convert;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;

/**
 * @author Duyen.le
 */
@Service
@EnableCaching
public class ProductsDAOImpl extends BaseDAOImpl implements ProductsDAO {
	private static final Logger LOG = Logger.getLogger(ProductsDAOImpl.class);
	@Value("${coms.dsv.asm.bdmReview.searchProduct}")
	private String sqlSearchProduct;
	@Value("${coms.dsv.asm.bdmReview.countProduct}")
	private String sqlCountProduct;
	@Value("${coms.dsv.asm.bdmReview.countProductFail}")
	private String sqlCountProductFail;
	@Value("${coms.dsv.asm.bdmReview.getFailReason}")
	private String sqlGetFailReason;
	@Value("${coms.dsv.asm.bdmReview.updateReviewStatus}")
	private String sqlUpdateReviewStatus;
	@Value("${coms.dsv.asm.bdmReview.updateHebRetail}")
	private String sqlUpdateHebRetail;
	@Value("${coms.dsv.asm.bdmReview.getStatusProduct}")
	private String sqlGetStatusProduct;
	@Value("${coms.dsv.asm.bdmReview.exportToExcel}")
	private String sqlExportToExcel;
	@Value("${coms.dsv.asm.bdmReview.getProductDesc}")
	private String sqlGetProductDesc;
	@Value("${coms.dsv.asm.bdmReview.getAllFailedReasons}")
	private String sqlGetAllFailedReasons;

	/**
	 * searchForProducts.
	 * @param productSearchCriteriaVO
	 *            :ProductSearchCriteriaVO
	 * @throws DSVException
	 *             :DSVException
	 * @return Page<ProductRiewVO>
	 */
	@Override
	public Page<ProductRiewVO> searchForProducts(ProductSearchCriteriaVO productSearchCriteriaVO) throws DSVException {
		Page<ProductRiewVO> page = new Page<ProductRiewVO>();
		List<ProductRiewVO> listProductRiewVOs = null;
		String condition = AHelper.getSearchConditionBdmReview(productSearchCriteriaVO);
		int start = Integer.parseInt(productSearchCriteriaVO.getStart());
		start = (start == 0) ? start : (start + 1);
		int length = Integer.parseInt(productSearchCriteriaVO.getLength());
		String end = String.valueOf(start + length);
		String sortDir = productSearchCriteriaVO.getsSortDir();
		if (Helper.isEmpty(sortDir)) {
			sortDir = AConstants.DESC;
		}
		condition = condition.concat(Constants.ORDER_BY).concat(productSearchCriteriaVO.getiSortColumn())
				.concat(Constants.WHITE_SPACE).concat(sortDir);
		String sqlSearch = String.format(this.sqlSearchProduct, productSearchCriteriaVO.getiSortColumn(),
				sortDir, condition, start, end);
		LOG.info("## searchForProducts query = " + sqlSearch);
		try {
			listProductRiewVOs = this.getJdbcTemplate().query(sqlSearch, ProductMapper.getProductBdmReview());
			page.setPageItems(listProductRiewVOs);
			List<String> lstUpcs = new ArrayList<String>();
			if (!Helper.isEmpty(listProductRiewVOs)) {
				for (ProductRiewVO productRiewVO : listProductRiewVOs) {
					if (!lstUpcs.contains(productRiewVO.getUpc())) {
						if (Helper.isEmpty(productRiewVO.getReviewStatus())) {
							productRiewVO.setReviewStatus(Constants.STRING_P);
						}
						lstUpcs.add(productRiewVO.getUpc());
					}
				}
				if (!Helper.isEmpty(lstUpcs)) {
					Map<String, ProductRiewVO> listProductRiewVOFails = this.getFailReason(lstUpcs);
					if (!Helper.isEmpty(listProductRiewVOFails)) {
						Convert.setFailReasonProductReview(listProductRiewVOs, listProductRiewVOFails);
					}
				}
				page.setRowCount(listProductRiewVOs.get(0).getTotalRow());
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return page;
	}

	/**
	 * Export product to Excel.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author duyen.le
	 * @return List<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 */
	@Override
	public List<ProductRiewVO> exportExcelForProducts(ProductSearchCriteriaVO prdSearchCriteriaVO) throws DSVException {
		LOG.info("Export To Excel Dao.");
		List<ProductRiewVO> listProductRiewVOs = null;
		String sortDir = prdSearchCriteriaVO.getsSortDir();
		if (Helper.isEmpty(sortDir)) {
			sortDir = AConstants.DESC;
		}
		String condition = AHelper.getSearchConditionBdmReview(prdSearchCriteriaVO);
		condition = condition.concat(Constants.ORDER_BY).concat(prdSearchCriteriaVO.getiSortColumn())
				.concat(Constants.WHITE_SPACE).concat(sortDir).concat(")");

		String sqlSearch = String.format(this.sqlExportToExcel, prdSearchCriteriaVO.getiSortColumn(),
				sortDir, condition);
		LOG.info("## exportExcel BDM Review query = " + sqlSearch);
		try {
			listProductRiewVOs = this.getJdbcTemplate().query(sqlSearch, ProductMapper.getProductBdmReview());
			List<String> lstUpcs = new ArrayList<String>();
			if (!Helper.isEmpty(listProductRiewVOs)) {
				for (ProductRiewVO productRiewVO : listProductRiewVOs) {
					if (!lstUpcs.contains(productRiewVO.getUpc())) {
						if (Helper.isEmpty(productRiewVO.getReviewStatus())) {
							productRiewVO.setReviewStatus(Constants.STRING_P);
						}
						lstUpcs.add(productRiewVO.getUpc());
					}
				}
				Map<String, ProductRiewVO> listProductRiewVOFails = this.getFailReason(lstUpcs);
				if (!Helper.isEmpty(listProductRiewVOFails)) {
					Convert.setFailReasonProductReview(listProductRiewVOs, listProductRiewVOFails);
				}
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return listProductRiewVOs;
	}

	/**
	 * countProudctRev.
	 * @param productSearchCriteriaVO
	 *            :ProductSearchCriteriaVO
	 * @throws DSVException
	 *             :DSVException
	 * @return List<ProductRiewVO>
	 */
	@Override
	public List<ProductRiewVO> countProudctRev(ProductSearchCriteriaVO productSearchCriteriaVO) throws DSVException {
		List<ProductRiewVO> listProductRiewVOs = null;
		List<ProductRiewVO> lstProductRiewVOReturns = null;
		String condition = AHelper.getSearchConditionBdmReviewCount(productSearchCriteriaVO);
		String sqlSearch = String.format(this.sqlCountProduct, condition);
		LOG.info("## countProudctRev sqlSearch = " + sqlSearch);
		int countProductAll = 0;
		try {
			listProductRiewVOs = this.getJdbcTemplate().query(sqlSearch, ProductMapper.getProductBdmCountReview());
			if (!Helper.isEmpty(listProductRiewVOs)) {
				lstProductRiewVOReturns = new ArrayList<ProductRiewVO>();
				for (ProductRiewVO productRiewVOTmp : listProductRiewVOs) {
					if ((Constants.NULL_REVIEW_STATUS.equals(productRiewVOTmp.getReviewStatus()) || AConstants.APPRV.equals(productRiewVOTmp.getReviewStatus()))
							&& productRiewVOTmp.getTotalRow() > 0) {
						countProductAll += productRiewVOTmp.getTotalRow();
					} else {
						lstProductRiewVOReturns.add(productRiewVOTmp);
					}
				}
				ProductRiewVO productRiewVO = new ProductRiewVO();
				productRiewVO.setReviewStatus(Constants.ALL_UPPERCASE_STR);
				productRiewVO.setTotalRow(countProductAll);
				lstProductRiewVOReturns.add(productRiewVO);
			}

		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstProductRiewVOReturns;
	}

	/**
	 * countProudctFail.
	 * @param productSearchCriteriaVO
	 *            :ProductSearchCriteriaVO
	 * @return int
	 */
	@Override
	public int countProudctFail(ProductSearchCriteriaVO productSearchCriteriaVO) {
		int count = 0;
		String condition = Constants.EMPTY_STRING;
		if (Helper.isEmpty(productSearchCriteriaVO.getFailReason())) {
			condition = condition.concat("INNER JOIN DSV_ASSRT.PROD_RULE_STAT PRS ON PRC.PROD_SCN_CD = PRS.PROD_SCN_CD AND PRC.VEND_ID = PRS.VEND_ID");
		}
		condition = condition.concat(AHelper.getSearchConditionBdmReviewCountFail(productSearchCriteriaVO));
		condition = condition.concat(" AND PRS.ERR_TYP_CD IN('R0001', 'R0002', 'R0003', 'R0004', 'R0005', 'R0006', 'R0007', 'R0008', 'R0009')");
		String sqlCount = String.format(this.sqlCountProductFail, condition);
		LOG.info("## countProudctFail sqlCount = " + sqlCount);
		try {
			count = this.getJdbcTemplate().queryForInt(sqlCount);

		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return count;
	}

	/**
	 * getFailReason.
	 * @param lstProdScn
	 *            :List<String>
	 * @return Map<String, ProductRiewVO>
	 */
	@Override
	public Map<String, ProductRiewVO> getFailReason(List<String> lstProdScn) {
		List<ProductRiewVO> listProductRiewVOs = null;
		Map<String, ProductRiewVO> listProductRiewVOResults = new HashMap<String, ProductRiewVO>();
		String prodScn = AHelper.convertListToString(lstProdScn);
		String sqlQuery = String.format(this.sqlGetFailReason, prodScn);
		LOG.info("## getFailReason sqlQuery = " + sqlQuery);
		ProductRiewVO productRiewVO;
		try {
			listProductRiewVOs = this.getJdbcTemplate().query(sqlQuery, ProductMapper.getProductBdmReviewFailReason());
			if (!Helper.isEmpty(listProductRiewVOs)) {
				for (ProductRiewVO productRiewVOTmp : listProductRiewVOs) {
					if (listProductRiewVOResults.containsKey(productRiewVOTmp.getUpc())) {
						productRiewVO = listProductRiewVOResults.get(productRiewVOTmp.getUpc());
						if (productRiewVO != null) {
							if (Helper.isEmpty(productRiewVO.getFailedReason())) {
								productRiewVO.setFailedReason(productRiewVOTmp.getFailedReason());
								productRiewVO.setFailedReasonCode(productRiewVOTmp.getFailedReasonCode());
							} else {
								productRiewVO.setFailedReason(productRiewVO.getFailedReason().concat(Constants.COMMAS).
										concat(productRiewVOTmp.getFailedReason()));
								productRiewVO.setFailedReasonCode(productRiewVO.getFailedReasonCode().concat(Constants.COMMAS).
										concat(productRiewVOTmp.getFailedReasonCode()));
							}
						}

					} else {
						listProductRiewVOResults.put(productRiewVOTmp.getUpc(), productRiewVOTmp);
					}

				}
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);

		}
		return listProductRiewVOResults;
	}

	@Override
	public String rejectProducts(final List<String> lstUpcs, final String userId) {
		String mess = Constants.SUCCESSFULLY;
		try {
			this.getJdbcTemplate().batchUpdate(this.sqlUpdateReviewStatus, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					String upc = lstUpcs.get(i);
					Date date = new Date();
					ps.setString(AntiMagicNumber.ONE, AConstants.RJECT);
					ps.setString(AntiMagicNumber.TWO, userId);
					ps.setTimestamp(AntiMagicNumber.THREE, new Timestamp(date.getTime()));
					ps.setString(AntiMagicNumber.FOUR, upc);
				}

				@Override
				public int getBatchSize() {
					return lstUpcs.size();
				}
			});
		} catch (DataAccessException e) {
			mess = Constants.ERROR_ATTRIBUTE;
			LOG.error(e.getMessage(), e);
		}
		return mess;
	}

	@Override
	public String approveProducts(final List<String> lstUpcs, final String userId) {
		String mess = Constants.SUCCESSFULLY;
		try {
			this.getJdbcTemplate().batchUpdate(this.sqlUpdateReviewStatus, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					String upc = lstUpcs.get(i);
					Date date = new Date();
					ps.setString(AntiMagicNumber.ONE, AConstants.APPRV);
					ps.setString(AntiMagicNumber.TWO, userId);
					ps.setTimestamp(AntiMagicNumber.THREE, new Timestamp(date.getTime()));
					ps.setString(AntiMagicNumber.FOUR, upc);
				}

				@Override
				public int getBatchSize() {
					return lstUpcs.size();
				}
			});
		} catch (DataAccessException e) {
			mess = Constants.ERROR_ATTRIBUTE;
			LOG.error(e.getMessage(), e);
		}
		return mess;
	}

	@Override
	public String updateProducts(final List<ProductRiewVO> lstProds) {
		String mess = Constants.SUCCESSFULLY;
		try {
			this.getJdbcTemplate().batchUpdate(this.sqlUpdateHebRetail, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ProductRiewVO prod = lstProds.get(i);
					Date date = new Date();
					ps.setString(AntiMagicNumber.ONE, prod.getHebRetail());
					ps.setString(AntiMagicNumber.TWO, prod.getUpc());
				}

				@Override
				public int getBatchSize() {
					return lstProds.size();
				}
			});
		} catch (DataAccessException e) {
			mess = Constants.ERROR_ATTRIBUTE;
			LOG.error(e.getMessage(), e);
		}
		return mess;
	}

	@Override
	public String unRejectProducts(final List<String> lstUpcs, final String userId) {
		String mess = Constants.SUCCESSFULLY;
		try {
			this.getJdbcTemplate().batchUpdate(this.sqlUpdateReviewStatus, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					String upc = lstUpcs.get(i);
					Date date = new Date();
					ps.setNull(AntiMagicNumber.ONE, java.sql.Types.NULL);
					ps.setString(AntiMagicNumber.TWO, userId);
					ps.setTimestamp(AntiMagicNumber.THREE, new Timestamp(date.getTime()));
					ps.setString(AntiMagicNumber.FOUR, upc);
				}

				@Override
				public int getBatchSize() {
					return lstUpcs.size();
				}
			});
		} catch (DataAccessException e) {
			mess = Constants.ERROR_ATTRIBUTE;
			LOG.error(e.getMessage(), e);
		}
		return mess;
	}

	@Override
	public boolean checkApproveReject(List<String> lstUpcs) {
		String prodScn = AHelper.convertListToString(lstUpcs);
		boolean flag = true;
		String sqlSearch = String.format(this.sqlGetStatusProduct, prodScn);
		List<String> lstStatus = null;
		try {
			lstStatus = this.getJdbcTemplate().query(sqlSearch, new RowMapper<String>() {
				@Override
				public String mapRow(ResultSet rs, int arg1) throws SQLException {
					String status = rs.getString(AConstants.REV_STAT_CD);
					return status;
				}
			});
			if (!Helper.isEmpty(lstStatus)) {
				for (String status : lstStatus) {
					if (!Helper.isEmpty(status) && AConstants.RJECT.equals(status.trim())) {
						flag = false;
						break;
					}
				}
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return flag;
	}

	@Override
	public boolean checkUnReject(List<String> lstUpcs) {
		String prodScn = AHelper.convertListToString(lstUpcs);
		boolean flag = true;
		String sqlSearch = String.format(this.sqlGetStatusProduct, prodScn);
		List<String> lstStatus = null;
		try {
			lstStatus = this.getJdbcTemplate().query(sqlSearch, new RowMapper<String>() {
				@Override
				public String mapRow(ResultSet rs, int arg1) throws SQLException {
					String status = rs.getString(AConstants.REV_STAT_CD);
					return status;
				}
			});
			if (!Helper.isEmpty(lstStatus)) {
				for (String status : lstStatus) {
					if (Helper.isEmpty(status)) {
						flag = false;
						break;
					}
				}
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}
		return flag;
	}

	@Override
	@Cacheable("PRODUCT_DESC")
	public List<ProductRiewVO> getAllProductInfo() {
		LOG.info("getAllProductInfo");
		String query = String.format(this.sqlGetProductDesc, AntiMagicNumber.ONE, Constants.MAX_ROW_QUERY);
		LOG.info("query = " + query);
		List<ProductRiewVO> lstProductRiewVOs = null;
		List<ProductRiewVO> lstProductRiewVOTmp = null;
		lstProductRiewVOs = this.getJdbcTemplate().query(query, new RowMapper<ProductRiewVO>() {
			@Override
			public ProductRiewVO mapRow(ResultSet rs, int arg1) throws SQLException {
				ProductRiewVO productRiewVO = new ProductRiewVO();
				productRiewVO.setDescription(rs.getString(ProductRiewVO.PROD_DES));
				productRiewVO.setTotalRow(rs.getInt(ProductRiewVO.TOTALROWS));
				return productRiewVO;
			}
		});
		if (!Helper.isEmpty(lstProductRiewVOs)) {
			int pageTotal = lstProductRiewVOs.get(0).getTotalRow() / Constants.MAX_ROW_QUERY;
			int startIndex = 0;
			int endIndex = 0;
			if (pageTotal > 1) {
				for (int page = 1; page < pageTotal; page++) {
					startIndex = page * Constants.MAX_ROW_QUERY;
					endIndex = startIndex + Constants.MAX_ROW_QUERY;
					startIndex = startIndex + 1;
					if (endIndex > lstProductRiewVOs.get(0).getTotalRow()) {
						endIndex = lstProductRiewVOs.get(0).getTotalRow();
					}
					lstProductRiewVOTmp = this.getAllProductInfolimit(startIndex, endIndex);
					if (!Helper.isEmpty(lstProductRiewVOTmp)) {
						lstProductRiewVOs.addAll(lstProductRiewVOTmp);
					}
				}
			}

		}
		return lstProductRiewVOs;
	}

	/**
	 * getAllProductInfolimit - get All Product Desc From DSV_PROD_CTLG Table int limit from startIndex to endIndex.
	 * @param startIndex
	 *            int
	 * @param endIndex
	 *            int
	 * @author thangdang
	 * @return List<ProductRiewVO>
	 */
	public List<ProductRiewVO> getAllProductInfolimit(int startIndex, int endIndex) {
		String query = String.format(this.sqlGetProductDesc, startIndex, endIndex);
		LOG.info("getAllProductInfolimit " + query);
		List<ProductRiewVO> lstProductRiewVOs = this.getJdbcTemplate().query(query, new RowMapper<ProductRiewVO>() {
			@Override
			public ProductRiewVO mapRow(ResultSet rs, int arg1) throws SQLException {
				ProductRiewVO productRiewVO = new ProductRiewVO();
				productRiewVO.setDescription(rs.getString(ProductRiewVO.PROD_DES));
				productRiewVO.setTotalRow(rs.getInt(ProductRiewVO.TOTALROWS));
				return productRiewVO;
			}
		});
		return lstProductRiewVOs;
	}

	/**
	 * getListFailedReasons - get List of Failed Reason.
	 * @author mrh.diemnguyen
	 * @return List<BaseVO>
	 * @throws DSVException
	 */
	@Override
	public List<BaseVO> getListFailedReasons() throws DSVException {
		LOG.info("get all Failed Reason ");
		List<BaseVO> lstFailedReasons = null;
		try {
			lstFailedReasons = this.getJdbcTemplate().query(this.sqlGetAllFailedReasons, ProductMapper.getAllFailedReasonsMapper());
		} catch (DataAccessException e) {
			LOG.fatal(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstFailedReasons;
	}
}
