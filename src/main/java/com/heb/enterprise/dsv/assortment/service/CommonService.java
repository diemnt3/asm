/*
 * $Id: CommonService.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.util.List;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * CommonService interface.
 * @author duyen.le
 */
public interface CommonService {

	/**
	 * get User login.
	 * @return HebUserDetails
	 * @author thangdang
	 */
	HebUserDetails getUserLogin();

	/**
	 * Retrieves a Rules set by Rule Group.
	 * @return List<BaseVO> : List of Rules set are retrieved from Database.
	 * @author thangdang
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	List<BaseVO> getAllAssortmentRule() throws DSVException;

	/**
	 * Get all list vendors.
	 * @return List<VendorInfoVO>
	 * @author Duyen.le
	 */
	List<VendorInfoVO> getAllListVendors() throws DSVException;
}
