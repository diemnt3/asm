package com.heb.enterprise.dsv.assortment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.heb.enterprise.dsv.exception.DSVException;

/**
 * The Class GlobalExceptionController.
 * @author quang.phan
 */
@ControllerAdvice
@Controller
public class GlobalExceptionController {

	private static final Logger LOG = Logger.getLogger(GlobalExceptionController.class);

	/**
	 * Handle custom exception.
	 * @param e
	 *            :Exception
	 * @param response
	 *            :HttpServletResponse
	 * @param request
	 *            :HttpServletRequest
	 * @return the model and view
	 * @author quang.phan
	 */
	@ExceptionHandler(Exception.class)
	public ModelAndView handleCustomException(Exception e, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = null;
		if (this.isAjax(request)) {
			model = new ModelAndView("genericError.tile");
			model.addObject("errDetal", StringEscapeUtils.escapeJavaScript(e.getMessage().replaceAll("\\r|\\n", "")));
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} else {
			model = new ModelAndView("unhandled.error.tile");
			model.addObject("errorMessage", StringEscapeUtils.escapeJavaScript(e.getMessage().replaceAll("\\r|\\n", "")));
		}
		LOG.info("handleCustomException !!!!!!");
		LOG.info("Message: " + e.getMessage());
		return model;
	}

	/**
	 * Handle custom exception.
	 * @author quang.phan
	 * @param e
	 *            the e
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the model and view
	 */
	@ExceptionHandler(DSVException.class)
	public ModelAndView handleCustomException(DSVException e, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = null;
		if (this.isAjax(request)) {
			model = new ModelAndView("genericError.tile");
			model.addObject("errDetal", StringEscapeUtils.escapeJavaScript(e.getMessage().replaceAll("\\r|\\n", "")));
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} else {
			model = new ModelAndView("unhandled.error.tile");
			model.addObject("errorMessage", StringEscapeUtils.escapeJavaScript(e.getMessage().replaceAll("\\r|\\n", "")));
		}
		return model;
	}

	/**
	 * isAjax.
	 * @param request
	 *            :HttpServletRequest
	 * @param request
	 * @return boolean
	 * @author anhtran.
	 */
	private boolean isAjax(HttpServletRequest request) {
		return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
	}

	/**
	 * showInternalServerError.
	 * @param model
	 *            :ModelMap
	 * @return String
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	@RequestMapping(value = { "/internal-server-error" }, method = { RequestMethod.GET })
	public String showInternalServerError(ModelMap model, @RequestParam(value = "message", required = false) String message) throws DSVException {
		LOG.info("Show showInternalServerError Screen");
		LOG.info("test: " + message);
		model.addAttribute("errorMessage", message);
		return "unhandled.error.tile";
	}

}
