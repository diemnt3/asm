/*
 * $Id: NewProductReviewService.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.util.List;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * NewProductReviewService interface.
 * @author duyen.le
 */
public interface ReviewNewProductsService {
	/**
	 * getBrands - get List of Brands.
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	List<BaseVO> getListBrands();

	/**
	 * getListCommodities - get List of Commodities.
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 * @throws DSVException
	 *             DSVException
	 */
	List<CommodityVO> getListCommodities() throws DSVException;

	/**
	 * getListVendors - get List of Vendors.
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	List<VendorInfoVO> getListVendors();

	/**
	 * search product for review.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author thangdang
	 * @return Page<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 */
	Page<ProductRiewVO> searchForProducts(ProductSearchCriteriaVO productSearchCriteriaVO) throws DSVException;

	/**
	 * Reject Products after reviewing.
	 * @param lstUpcs
	 *            List<String>
	 * @param userId
	 *            String
	 * @author Duyen.Le
	 * @return String
	 */
	String rejectProducts(List<String> lstUpcs, String userId);

	/**
	 * Approve Products after reviewing.
	 * @param lstUpcs
	 *            List<String>
	 * @param userId
	 *            String
	 * @author Duyen.Le
	 * @return String
	 */
	String approveProducts(List<String> lstUpcs, String userId);

	/**
	 * Approve Products after reviewing.
	 * @param lstUpcs
	 *            List<ProductRiewVO>
	 * @param userId
	 *            String
	 * @author mrh.diemnguyen
	 * @return String
	 */
	String updateProducts(List<ProductRiewVO> lstProd);

	/**
	 * UnReject Products after reviewing.
	 * @param lstUpcs
	 *            List<String>
	 * @param userId
	 *            String
	 * @author Duyen.Le
	 * @return String
	 */
	String unRejectProducts(List<String> lstUpcs, String userId);

	/**
	 * countProudctRev.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author thangdang
	 * @return Page<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 */
	List<ProductRiewVO> countProudctRev(ProductSearchCriteriaVO productSearchCriteriaVO)
			throws DSVException;

	/**
	 * checkApproveReject.
	 * @param lstUpcs
	 *            List<String>
	 * @author thangdang
	 * @return boolean
	 */
	boolean checkApproveReject(List<String> lstUpcs);

	/**
	 * checkUnReject.
	 * @param lstUpcs
	 *            List<String>
	 * @author thangdang
	 * @return boolean
	 */
	boolean checkUnReject(List<String> lstUpcs);

	/**
	 * search product for review.
	 * @param prdSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @author Duyen.le
	 * @return List<ProductRiewVO>
	 * @throws DSVException
	 *             DSVException
	 */
	List<ProductRiewVO> exportExcelForProducts(ProductSearchCriteriaVO prdSearchCriteriaVO) throws DSVException;

	/**
	 * getListFailedReasons - get List of Failed Reason.
	 * @author mrh.diemnguyen
	 * @return List<BaseVO>
	 * @throws DSVException
	 */
	List<BaseVO> getListFailedReasons() throws DSVException;

}
