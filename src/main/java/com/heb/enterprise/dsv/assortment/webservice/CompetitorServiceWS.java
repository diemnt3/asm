/*
 * $Id: CompetitorServiceWS.java,v 1.5 2013/10/29 13:58:01 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice;

import java.util.List;

import com.heb.enterprise.dsv.vo.CompetitiveRetails;

/**
 * CompetitorServiceWS.
 * @author Duyen.le
 */
public interface CompetitorServiceWS {
	/**
	 * getOnlineCompetitors - get List of Competitors.
	 * @author Duyen.Le
	 * @return List<CompetitiveRetails>
	 */
	List<CompetitiveRetails> getOnlineCompetitors();

	/**
	 * getCompetitorPricebyUPC - get List of Competitors Price by UPC.
	 * @author Duyen.Le
	 * @return List<CompetitiveRetails>
	 */
	List<CompetitiveRetails> getCompetitorPricebyUPC();
}
