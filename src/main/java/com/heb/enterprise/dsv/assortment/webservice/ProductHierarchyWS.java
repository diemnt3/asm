/*
 * $Id: ProductHierarchyWS.java,v 1.5 2013/10/29 13:58:01 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice;

import java.util.List;
import java.util.Map;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;

/**
 * ProductHierarchyWS.
 * @author anhtran
 */
public interface ProductHierarchyWS {

	/**
	 * getCommodityDetailList.
	 * @return Map<Integer, CommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	Map<Integer, CommodityVO> getCommodityDetailList() throws DSVException;

	/**
	 * getMerchandisingHierarchyDetails.
	 * @return List<DepartmentVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	List<DepartmentVO> getMerchandisingHierarchyDetails() throws DSVException;

	/**
	 * getHierachyForGlobalRules.
	 * @return List<SubDepartmentVO>
	 * @throws DSVException
	 * @author duyen.le
	 */
	List<SubDepartmentVO> getHierachyForGlobalRules();
}
