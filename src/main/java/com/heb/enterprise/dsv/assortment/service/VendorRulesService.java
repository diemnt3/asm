/*
 * $Id: VendorRulesService.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service;

import java.util.List;
import java.util.Map;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.AttrAndRuleVendorVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.VendorAttributeVO;

/**
 * VendorRulesService interface.
 * @author duyen.le
 */
public interface VendorRulesService {
	/**
	 * getRuleSettingsForVendor method.
	 * @param commodityId
	 *            String
	 * @param subCommodityId
	 *            String
	 * @param vendorId
	 *            String
	 * @return List<RuleConfigVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	Map<String, AssortmentRulesVO> getRuleSettingsForVendor(String vendorId, String commodityId, String subCommodityId) throws DSVException;

	/**
	 * updateRuleSettingsForVendor method.
	 * @param vendorAsmVo
	 *            AttrAndRuleVendorVO
	 * @return boolean
	 * @author duyen.le
	 */
	boolean updateRuleSettingsForVendor(AttrAndRuleVendorVO vendorAsmVo);

	/**
	 * ajaxGetCommodityandSub method.
	 * @return List<CommodityVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	List<CommodityVO> ajaxGetCommodityandSub() throws DSVException;

	/**
	 * get list of Vendor Attributes.
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	List<VendorAttributeVO> getVendorAttr() throws DSVException;

	/**
	 * get list of Vendor Attributes and Rule for Vendor rule by VendorID.
	 * @param vendorID
	 *            String
	 * @return List<VendorAttributeVO>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	AttrAndRuleVendorVO getRuleSettingsByVendor(String vendorID) throws DSVException;

	/**
	 * get Active Switch for subComd.
	 * @param vendorId
	 *            String
	 * @return List<String>
	 * @throws DSVException
	 *             DSVException
	 * @author duyen.le
	 */
	List<Integer> getActiveSw(String vendorId) throws DSVException;

	/**
	 * resetRulesToHigherLevel.
	 * @param vendorId
	 *            String
	 * @param commodityId
	 *            String
	 * @param subCommodityId
	 *            String
	 * @return boolean
	 * @author thangdang
	 */
	boolean resetRulesToHigherLevel(String vendorId, String commodityId, String subCommodityId);

	/**
	 * get subCommodity override for Assortment Rule.
	 * @param vendorId
	 *            String
	 * @param lvlType
	 *            String
	 * @return List<String>
	 * @author duyen.le
	 */
	List<Integer> getAsmLVLModified(String lvlType, String vendorId);

	/**
	 * activateRule.
	 * @param subCommodityMap
	 *            Map<String, String>
	 * @param userId
	 *            String
	 * @param vendor
	 *            String
	 * @param type
	 *            Type are Commodity or SubCommodity :String
	 * @param value
	 *            Commodity ID or SubCommodity ID:String
	 * @return boolean
	 * @author Duyen.le
	 */
	boolean activateRule(String type, String value, String vendor, String userId, Map<String, String> subCommodityMap);

	/**
	 * deActivateRule.
	 * @param vendor
	 *            String
	 * @param subCommodityMap
	 *            Map<String, String>
	 * @param type
	 *            Type are Commodity or SubCommodity :String
	 * @param value
	 *            Commodity ID or SubCommodity ID:String
	 * @return boolean
	 * @author Duyen.le
	 */
	boolean deActivateRule(String type, String value, String vendor, Map<String, String> subCommodityMap);

	/**
	 * getLstSubActivateBySub.
	 * @return List<LevelVO>
	 * @author Duyen.le
	 */
	List<LevelVO> getLstSubActivateBySub() throws DSVException;

}
