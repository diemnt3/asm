/*
 * $Id: CountriesServiceWSImpl.java,v 1.5 2013/10/29 13:58:01 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.xmlns._1342700025447.CodeTableServiceServiceagentLocator;
import com.example.xmlns._1342700025447.PortType;
import com.heb.enterprise.dsv.assortment.webservice.CountriesServiceWS;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.CountriesVO;
import com.heb.xmlns.ei.Authentication.Authentication;
import com.heb.xmlns.ei.CNTRY_CD.CNTRY_CD;
import com.heb.xmlns.ei.CodeTableManagement.GetCountryCode_Request.GetCountryCode_Request;
import com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;

/**
 * CountriesServiceWSImpl.
 * @author anhtran.
 */
@Service
public class CountriesServiceWSImpl implements CountriesServiceWS {

	private static final Logger LOG = Logger.getLogger(CountriesServiceWSImpl.class);
	private static final String USER_ID = "aa";
	private static final String PWD = "bb";
	private static final String CLIEND_ID = "ww";

	@Value("${countriesService.endpoint}")
	private String countriesServiceEndpoint;

	/**
	 * CountriesServiceWS.
	 * @return List<CountriesVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	@Override
	public List<CountriesVO> getCountryCode() throws DSVException {
		CodeTableServiceServiceagentLocator ls = new CodeTableServiceServiceagentLocator();
		PortType portType;
		List<CountriesVO> countriesVOs = null;
		Authentication authentication = new Authentication(USER_ID, PWD, CLIEND_ID);
		GetCountryCode_Request request = new GetCountryCode_Request();
		request.setAuthentication(authentication);
		try {
			portType = ls.getCodeTableService(new URL(this.countriesServiceEndpoint));
			countriesVOs = this.getCountriesVOList(portType.getCountryCode(request));
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (ServiceException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (ProviderSOAPFault e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		} catch (RemoteException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return countriesVOs;
	}

	/**
	 * getCountriesVOList.
	 * @param cntryCDs
	 *            :CNTRY_CD[]
	 * @return Map<String, CommodityVO>
	 * @author anhtran.
	 */
	private List<CountriesVO> getCountriesVOList(CNTRY_CD[] cntryCDs) {
		List<CountriesVO> countriesVOs = new ArrayList<CountriesVO>();
		for (CNTRY_CD cntry : cntryCDs) {
			countriesVOs.add(this.convertToCountriesVO(cntry));
		}
		return countriesVOs;
	}

	/**
	 * convertToCountriesVO.
	 * @param cntry
	 *            :CNTRY_CD
	 * @return CountriesVO
	 * @author anhtran.
	 */
	private CountriesVO convertToCountriesVO(CNTRY_CD cntry) {
		CountriesVO countriesVO = new CountriesVO();
		countriesVO.setCntryId(cntry.getCNTRY_ID());
		countriesVO.setCountryName(Helper.trim(cntry.getCNTRY_NM()));
		return countriesVO;
	}
}
