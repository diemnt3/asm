/*
 * $Id: VendorManagementController.java,v 1.15 2015/03/12 11:19:22 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.VendorManagementService;
import com.heb.enterprise.dsv.assortment.utils.ConstantsWeb;
import com.heb.enterprise.dsv.assortment.webservice.BDMServiceWS;
import com.heb.enterprise.dsv.assortment.webservice.ProductHierarchyWS;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.BDMDetailVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.HierarchyMapVO;
import com.heb.enterprise.dsv.vo.JSTreeVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * VendorManagementController.
 * @author anhtran.
 */
@Controller
public class VendorManagementController {

	// private static final Detector DETECTOR = new DefaultDetector(MimeTypes.getDefaultMimeTypes());

	private static final Logger LOG = Logger.getLogger(VendorManagementController.class);

	private static final String VENDOR_PATH = Constants.SLASH + ConstantsWeb.VENDOR_PAGE;

	private static final String GET_VENDORS_LIST_PATH = ConstantsWeb.SEARCH_FOR_VENDORS;

	private static final String GET_VENDORS_HIERARCHY_PATH = ConstantsWeb.GET_VENDORS_HIERARCHY;

	// private static final String UPLOAD_HIERARCHY_MAP_PATH = ConstantsWeb.UPLOAD_HIERARCHY_MAP;

	// private static final String DOWNLOAD_TEMPLATE_PATH = ConstantsWeb.DOWNLOAD_TEMPLATE;

	// private static final String GET_HEB_HIERARCHY_PATH = ConstantsWeb.GET_HEB_HIERARCHY;

	private static final String SAVE_VENDOR_HIERARCHY_PATH = ConstantsWeb.SAVE_VENDOR_HIERARCHY;

	private static final String DELETE_ENTYRLSHP_PATH = ConstantsWeb.DELETE_ENTYRLSHP;

	private static final String GET_HEB_HIERARCHY_ROOT = ConstantsWeb.GET_HEB_HIERARCHY_ROOT;

	private static final String SEARCH_HEB_HIERARCHY = ConstantsWeb.SEARCH_HEB_HIERARCHY;

	private static final String VENDOR_TILE = "vendor.tile";

	// private static final String FILE_NAME = "Vendor_Hierarchy_Mapping_Template";

	private static final String CLASS_CONS = "class";

	private static final String ERROR_MESSAGE_CONS = "errorMessage";

	private static final String BDM = "BDM";

	private static final String CLASS_A_ATTR_COMMODITY = "font-bold commodity-a";

	private static final String SPAN_SUBCOMMODITY_CLASS = "<span class='subCommodityCd-cls'>";

	private static final String SPAN_SUBCOMMODITY_DESC_CLASS = "<span class='subCommodityDesc-cls'>";

	private static final String END_SPAN = "</span>";

	private static final String ERROR = "error";

	private static final String DISABLED = "disabled";

	// private static final String CONNECTION_DOWN = "Sorry, connection is down";

	private static final String COMMODITY_SUFFIXES = "_com";

	@Autowired
	private VendorManagementService vendorManagementService;
	@Autowired
	private CommonService commonService;

	@Autowired
	private CacheService cacheService;

	@Autowired
	private BDMServiceWS bdmServiceWS;

	@Autowired
	private ProductHierarchyWS productHierarchyWS;

	@Value("${coms.dsv.asm.vendorManagement.cat1Index}")
	private int cat1Index;

	@Value("${coms.dsv.asm.vendorManagement.cat2Index}")
	private int cat2Index;

	@Value("${coms.dsv.asm.vendorManagement.cat3Index}")
	private int cat3Index;

	@Value("${coms.dsv.asm.vendorManagement.cat4Index}")
	private int cat4Index;

	@Value("${coms.dsv.asm.vendorManagement.subCommodityIndex}")
	private int subCommodityIndex;

	/**
	 * showPricingRulesScreen.
	 * @return String
	 * @author anhtran
	 * @throws DSVException
	 *             :If could not get values from Service Layer.
	 */
	@RequestMapping(value = { VENDOR_PATH }, method = RequestMethod.GET)
	public String showPricingRulesScreen()
			throws DSVException {
		return VENDOR_TILE;
	}

	/**
	 * getAllVendorsList.
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author anhtran
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = { GET_VENDORS_LIST_PATH }, method = { RequestMethod.GET, RequestMethod.POST })
	public String getAllVendorsList()
			throws DSVException {
		JSONObject jsonResult = new JSONObject();
		List<Object> lstData = new ArrayList<Object>();
		List<VendorInfoVO> listFilterVendors = this.commonService.getAllListVendors();
		for (VendorInfoVO ven : listFilterVendors) {
			lstData.add(this.mapObjVendor(ven));
		}
		jsonResult.put(Constants.AA_DATA, lstData);
		LOG.info("JSON STR: " + jsonResult.toString());
		return jsonResult.toString();
	}

	/**
	 * mapObjVendor.
	 * @param vendor
	 *            :VendorInfoVO
	 * @return Map<String, Object>
	 * @author anhtran.
	 */
	private Map<String, Object> mapObjVendor(VendorInfoVO vendor) {
		Map<String, Object> lstOrderJson = new HashMap<String, Object>();
		lstOrderJson.put(Constants.AUTO_VALUE, vendor.getVendorId());
		lstOrderJson.put(Constants.AUTO_NAME, vendor.getVendorName());
		lstOrderJson.put(Constants.AUTO_LABEL, vendor.getVendorId() + " - " + vendor.getVendorName());
		return lstOrderJson;
	}

	/**
	 * getVendorHierarchyMappingByVendor.
	 * @param vendorId
	 *            :String
	 * @return String
	 * @author anhtran
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = { GET_VENDORS_HIERARCHY_PATH }, method = { RequestMethod.GET, RequestMethod.POST })
	public String getVendorHierarchyMappingByVendor(@RequestParam(value = "vendorId", required = false) String vendorId, ModelMap modelMap) {
		LOG.info("getVendorHierarchyMappingByVendor: " + vendorId);
		int rowID = AntiMagicNumber.ZERO;
		String errorMessage = Constants.EMPTY_STRING;
		JSONObject jsonResult = new JSONObject();
		List<Object> lstData = new ArrayList<Object>();
		List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
		Map<String, SubCommodityVO> subCommodityMap = new HashMap<String, SubCommodityVO>();
		try {
			subCommodityMap = this.cacheService.getSubCommodityList();
			hierarchyMapVOs = this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId);
		} catch (DSVException e) {
			LOG.info("getVendorHierarchyMappingByVendor DSVException !!!");
			LOG.info(e.getMessage(), e);
			errorMessage = e.getMessage();
		}
		Collections.sort(hierarchyMapVOs, HierarchyMapVO.HierarchyMapVOComparator);
		for (HierarchyMapVO hierarchyMapVO : hierarchyMapVOs) {
			hierarchyMapVO.setSubCommodity(subCommodityMap.get(hierarchyMapVO.getSubCommodityCd()).getSubCommodityDesc());
			lstData.add(this.mapObjFromHierarchyMapVO(hierarchyMapVO, ++rowID));
		}
		jsonResult.put(Constants.AA_DATA, lstData);
		jsonResult.put(ERROR_MESSAGE_CONS, errorMessage);
		return jsonResult.toString();
	}

	/**
	 * mapObjFromHierarchyMapVO.
	 * @param hierarchyMapVO
	 *            :HierarchyMapVO
	 * @param rowID
	 *            :int
	 * @return Map<String, Object>
	 * @author anhtran.
	 */
	private Map<String, Object> mapObjFromHierarchyMapVO(HierarchyMapVO hierarchyMapVO, int rowID) {
		Map<String, Object> lstJson = new HashMap<String, Object>();
		StringBuilder builder = new StringBuilder();
		lstJson.put("DT_RowId", rowID);
		lstJson.put("vendorId", hierarchyMapVO.getVendorId());
		lstJson.put("entyIdSubCommodity", hierarchyMapVO.getEntyIdSubCommodity());
		lstJson.put("category1", Helper.trim(hierarchyMapVO.getCategoryAbb1()));
		lstJson.put("categoryId1", Helper.trim(hierarchyMapVO.getCategory1()));
		lstJson.put("category1Input", Helper.trim(hierarchyMapVO.getCategoryAbb1()));
		lstJson.put("category2", Helper.trim(hierarchyMapVO.getCategoryAbb2()));
		lstJson.put("categoryId2", Helper.trim(hierarchyMapVO.getCategory2()));
		lstJson.put("category2Input", Helper.trim(hierarchyMapVO.getCategoryAbb2()));
		lstJson.put("category3", Helper.trim(hierarchyMapVO.getCategoryAbb3()));
		lstJson.put("categoryId3", Helper.trim(hierarchyMapVO.getCategory3()));
		lstJson.put("category3Input", Helper.trim(hierarchyMapVO.getCategoryAbb3()));
		lstJson.put("category4", Helper.trim(hierarchyMapVO.getCategoryAbb4()));
		lstJson.put("categoryId4", Helper.trim(hierarchyMapVO.getCategory4()));
		lstJson.put("category4Input", Helper.trim(hierarchyMapVO.getCategoryAbb4()));
		lstJson.put("subCommodity", Helper.trim(hierarchyMapVO.getSubCommodity()));
		lstJson.put("subCommodityCd", Helper.trim(hierarchyMapVO.getSubCommodityCd()));
		lstJson.put("lowestCatId", hierarchyMapVO.getLowestCatId());
		lstJson.put("statusNew", hierarchyMapVO.getStatusNew());
		lstJson.put("statusChange", hierarchyMapVO.getStatusChange());

		builder.append(Helper.isEmpty(Helper.trim(hierarchyMapVO.getCategory1())) ? Constants.EMPTY_STRING : Helper.trim(hierarchyMapVO.getCategory1()));
		builder.append(Helper.isEmpty(Helper.trim(hierarchyMapVO.getCategory2())) ? Constants.EMPTY_STRING : Constants.GREATER_THAN_SIGN.concat(Helper.trim(hierarchyMapVO.getCategory2())));
		builder.append(Helper.isEmpty(Helper.trim(hierarchyMapVO.getCategory3())) ? Constants.EMPTY_STRING : Constants.GREATER_THAN_SIGN.concat(Helper.trim(hierarchyMapVO.getCategory3())));
		builder.append(Helper.isEmpty(Helper.trim(hierarchyMapVO.getCategory4())) ? Constants.EMPTY_STRING : Constants.GREATER_THAN_SIGN.concat(Helper.trim(hierarchyMapVO.getCategory4())));
		builder.append(Helper.isEmpty(Helper.trim(hierarchyMapVO.getEntyIdSubCommodity())) ? Constants.EMPTY_STRING : Constants.GREATER_THAN_SIGN.concat(Helper.trim(hierarchyMapVO
				.getEntyIdSubCommodity())));
		lstJson.put("categoryIds", builder.toString());

		return lstJson;
	}

	/**
	 * Upload hierarchy map.
	 * @param file
	 *            :MultipartFile
	 * @param vendorId
	 *            :String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author anhtran
	 * @throws IOException
	 */
	// @SuppressWarnings("unchecked")
	// @ResponseBody
	// @RequestMapping(value = { UPLOAD_HIERARCHY_MAP_PATH }, method = RequestMethod.POST)
	// public String uploadHierarchyMap(HttpServletRequest request, @RequestParam("file") MultipartFile file, @RequestParam("vendorId") String vendorId) throws DSVException, IOException {
	// String type = detectMimeType(file.getInputStream());
	// LOG.info("DETECTOR: " + type);
	// LOG.info("uploadHierarchyMap");
	// LOG.info("description: " + request.getParameter("description"));
	// LOG.info("Saving file: " + file.getOriginalFilename());
	// LOG.info("containt file: " + file.getContentType());
	// LOG.info("containt name: " + file.getName());
	// LOG.info("Vendor ID: " + vendorId);
	//
	// List<HierarchyMapVO> hierarchyMapDBVOs = new ArrayList<HierarchyMapVO>();
	// List<HierarchyMapVO> hierarchyMapUploadVOs = new ArrayList<HierarchyMapVO>();
	// JSONObject jsonResult = new JSONObject();
	// List<Object> lstData = new ArrayList<Object>();
	// int rowID = 0;
	// try {
	// hierarchyMapUploadVOs = this.convertExcelFileToHierarchyMapVO(type, file);
	// hierarchyMapDBVOs = this.vendorManagementService.getVendorHierarchyMappingByVendor(vendorId);
	// for (HierarchyMapVO hierarchyMapVO : this.mergeHierarchyMapVOList(hierarchyMapUploadVOs, hierarchyMapDBVOs)) {
	// lstData.add(this.mapObjFromHierarchyMapVO(hierarchyMapVO, ++rowID));
	// }
	// } catch (DSVException e) {
	// LOG.info(e.getMessage(), e);
	// throw new DSVException(e);
	// }
	//
	// jsonResult.put(Constants.AA_DATA, lstData);
	// return jsonResult.toJSONString();
	// }

	/**
	 * mergeHierarchyMapVOList.
	 * @param listUpload
	 *            :List<HierarchyMapVO>
	 * @param listDB
	 *            :List<HierarchyMapVO>
	 * @return List<HierarchyMapVO>
	 * @author anhtran.
	 */
	// private List<HierarchyMapVO> mergeHierarchyMapVOList(List<HierarchyMapVO> listUpload, List<HierarchyMapVO> listDB) {
	// Map<String, HierarchyMapVO> map = this.convertListHierarchyMapVOToMap(listDB);
	// StringBuilder stringBuilder = new StringBuilder();
	// for (HierarchyMapVO hierarchyMapVO : listUpload) {
	// stringBuilder.append(hierarchyMapVO.getCategoryAbb1());
	// stringBuilder.append(hierarchyMapVO.getCategoryAbb2());
	// stringBuilder.append(hierarchyMapVO.getCategoryAbb3());
	// stringBuilder.append(hierarchyMapVO.getCategoryAbb4());
	// if (map.containsKey(stringBuilder.toString())) {
	// HierarchyMapVO vo = map.get(stringBuilder.toString());
	// if (!vo.getCategoryAbb1().equals(hierarchyMapVO.getCategoryAbb1())) {
	// map.get(stringBuilder.toString()).setCategoryAbb1(hierarchyMapVO.getCategoryAbb1());
	// map.get(stringBuilder.toString()).setStatusChange(Constants.YES);
	// }
	// if (!vo.getCategoryAbb2().equals(hierarchyMapVO.getCategoryAbb2())) {
	// map.get(stringBuilder.toString()).setCategoryAbb2(hierarchyMapVO.getCategoryAbb2());
	// map.get(stringBuilder.toString()).setStatusChange(Constants.YES);
	// }
	// if (!vo.getCategoryAbb3().equals(hierarchyMapVO.getCategoryAbb3())) {
	// map.get(stringBuilder.toString()).setCategoryAbb3(hierarchyMapVO.getCategoryAbb3());
	// map.get(stringBuilder.toString()).setStatusChange(Constants.YES);
	// }
	// if (!vo.getCategoryAbb4().equals(hierarchyMapVO.getCategoryAbb4())) {
	// map.get(stringBuilder.toString()).setCategoryAbb4(hierarchyMapVO.getCategoryAbb4());
	// map.get(stringBuilder.toString()).setStatusChange(Constants.YES);
	// }
	// if (!vo.getSubCommodity().equals(hierarchyMapVO.getSubCommodity())) {
	// map.get(stringBuilder.toString()).setSubCommodity(hierarchyMapVO.getSubCommodity());
	// map.get(stringBuilder.toString()).setStatusChange(Constants.YES);
	// }
	// } else {
	// map.put(stringBuilder.toString(), hierarchyMapVO);
	// }
	// stringBuilder.delete(0, stringBuilder.length());
	// }
	// return this.convertHierarchyMapVOMapToList(map);
	// }

	/**
	 * convertExcelFileToHierarchyMapVO.
	 * @param type
	 *            :String
	 * @param file
	 *            :MultipartFile
	 * @return List<HierarchyMapVO>
	 * @author anhtran.
	 */
	// private List<HierarchyMapVO> convertExcelFileToHierarchyMapVO(String type, MultipartFile file) {
	// Workbook workbook;
	// Sheet sheet;
	// List<HierarchyMapVO> hierarchyMapUploadVOs = new ArrayList<HierarchyMapVO>();
	// if ("application/vnd.ms-excel".equalsIgnoreCase(type)) {
	// try {
	// workbook = new HSSFWorkbook(file.getInputStream());
	// LOG.info("sheet name 2003  = " + workbook.getSheetName(0));
	// sheet = (HSSFSheet) workbook.getSheetAt(0);
	// hierarchyMapUploadVOs = this.getListHierarchyMapVOFromSheet(sheet);
	// } catch (IOException e) {
	// LOG.info("ERROR :!!!!");
	// LOG.info(e.getMessage());
	// }
	// } else if ("application/x-tika-ooxml".equalsIgnoreCase(type)) {
	// try {
	// workbook = new XSSFWorkbook(file.getInputStream());
	// LOG.info("sheet name 2007 = " + workbook.getSheetName(0));
	// sheet = (XSSFSheet) workbook.getSheetAt(0);
	// hierarchyMapUploadVOs = this.getListHierarchyMapVOFromSheet(sheet);
	// } catch (IOException e) {
	// LOG.info("ERROR:!!!!");
	// LOG.info(e.getMessage());
	// }
	//
	// } else {
	// LOG.info("Received file does not have a standard excel extension !!!!!");
	// throw new IllegalArgumentException("Received file does not have a standard excel extension.");
	// }
	// return hierarchyMapUploadVOs;
	// }

	/**
	 * getListHierarchyMapVOFromSheet.
	 * @param sheet
	 *            :Sheet
	 * @return List<HierarchyMapVO>
	 * @author anhtran.
	 */
	// private List<HierarchyMapVO> getListHierarchyMapVOFromSheet(Sheet sheet) {
	// List<HierarchyMapVO> hierarchyMapVOs = new ArrayList<HierarchyMapVO>();
	// for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getLastRowNum(); i++) {
	// Row row = sheet.getRow(i);
	// HierarchyMapVO hierarchyMapVO = new HierarchyMapVO();
	// hierarchyMapVO.setCategoryAbb1(row.getCell(this.cat1Index) == null ? Constants.EMPTY_STRING : row.getCell(this.cat1Index).getStringCellValue());
	// if (Helper.isEmpty(hierarchyMapVO.getCategoryAbb1())) {
	// break;
	// }
	// hierarchyMapVO.setCategoryAbb2(row.getCell(this.cat2Index) == null ? Constants.EMPTY_STRING : row.getCell(this.cat2Index).getStringCellValue());
	// hierarchyMapVO.setCategoryAbb3(row.getCell(this.cat3Index) == null ? Constants.EMPTY_STRING : row.getCell(this.cat3Index).getStringCellValue());
	// hierarchyMapVO.setCategoryAbb4(row.getCell(this.cat4Index) == null ? Constants.EMPTY_STRING : row.getCell(this.cat4Index).getStringCellValue());
	// hierarchyMapVO.setSubCommodity(row.getCell(this.subCommodityIndex) == null ? Constants.EMPTY_STRING : row.getCell(this.subCommodityIndex).getStringCellValue());
	// hierarchyMapVO.setStatusNew(Constants.YES);
	// hierarchyMapVO.setStatusChange(Constants.NO);
	// hierarchyMapVOs.add(hierarchyMapVO);
	// }
	// LOG.info("last row = " + sheet.getLastRowNum());
	// return hierarchyMapVOs;
	// }

	/**
	 * Download template file.
	 * @param request
	 *            :HttpServletRequest
	 * @param response
	 *            :HttpServletResponse
	 * @throws IOException
	 *             :IOException
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @author anhtran
	 */
	// @ResponseBody
	// @RequestMapping(value = { DOWNLOAD_TEMPLATE_PATH }, method = { RequestMethod.GET, RequestMethod.POST })
	// public void downloadTemplateFile(HttpServletRequest request,
	// HttpServletResponse response)
	// throws DSVException, IOException {
	// HSSFWorkbook workbook = this.getExcelTemplate();
	// this.buildExcelFile(response, workbook, FILE_NAME);
	// LOG.info("downloadTemplateFile");
	// }

	/**
	 * Save vendor hierarchy.
	 * @param vendorId
	 *            :String
	 * @param strJson
	 *            :String
	 * @return String
	 * @author anhtran
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = { SAVE_VENDOR_HIERARCHY_PATH }, method = RequestMethod.POST)
	public String saveVendorHierarchyMapping(@RequestParam("vendorId") String vendorId, @RequestParam("strJson") String strJson) {
		LOG.info("saveVendorHierarchyMapping");
		LOG.info("strJson: " + strJson);
		String errorMessage = Constants.EMPTY_STRING;
		JSONObject jsonResult = new JSONObject();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<HierarchyMapVO> list = null;
		try {
			list = objectMapper.readValue(strJson, objectMapper.getTypeFactory().constructCollectionType(List.class, HierarchyMapVO.class));
			this.vendorManagementService.saveVendorHierarchyMapping(list, vendorId);
			LOG.info("LIST: " + list.size());
		} catch (JsonParseException e) {
			LOG.info(e.getMessage(), e);
			errorMessage = e.getMessage();
		} catch (JsonMappingException e) {
			LOG.info(e.getMessage(), e);
			errorMessage = e.getMessage();
		} catch (IOException e) {
			LOG.info(e.getMessage(), e);
			errorMessage = e.getMessage();
		} catch (SQLException e) {
			LOG.info(e.getMessage(), e);
			errorMessage = e.getMessage();
		}
		jsonResult.put(ERROR_MESSAGE_CONS, errorMessage);
		return jsonResult.toString();

	}

	/**
	 * Get HEB Hierarchy.
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author anhtran
	 */
	// @SuppressWarnings("unchecked")
	// @ResponseBody
	// @RequestMapping(value = { GET_HEB_HIERARCHY_PATH }, method = RequestMethod.GET)
	// public List<JSONObject> getHEBHierarchy()
	// throws DSVException {
	// List<JSONObject> lstSubDeptJson = new ArrayList<JSONObject>();
	// List<CommodityVO> lst = this.cacheService.getCommodityList();
	// Map<String, CommodityVO> mapCommodityVO = this.cacheService.getCommodityDetailList();
	// Map<String, BDMDetailVO> bdmDetailMap = this.bdmServiceWS.getBDMDetailList();
	// StringBuilder subCommodityTextBuilder = new StringBuilder();
	// StringBuilder commodityTextBuilder = new StringBuilder();
	// for (CommodityVO commodityVO : lst) {
	// JSONObject commodityJsonObj = new JSONObject();
	// JSONObject liAttrObjCommodity = new JSONObject();
	// liAttrObjCommodity.put(CLASS_CONS, Constants.EMPTY_STRING);
	// JSONObject aAttrObjCommodity = new JSONObject();
	// aAttrObjCommodity.put(CLASS_CONS, CLASS_A_ATTR_COMMODITY);
	// commodityJsonObj.put(JSTreeVO.ID, commodityVO.getCommodityCd());
	// CommodityVO cvo = mapCommodityVO.get(String.valueOf(commodityVO.getCommodityCd()));
	//
	// commodityTextBuilder.append(commodityVO.getCommodityCd());
	// commodityTextBuilder.append(Constants.DASH_WHITE_SPACE);
	// commodityTextBuilder.append(commodityVO.getCommodityDesc());
	// commodityTextBuilder.append(Constants.DASH_WHITE_SPACE);
	// commodityTextBuilder.append(com.heb.enterprise.dsv.utils.Helper.isEmpty(cvo) ? BDM : bdmDetailMap.get(cvo.getBdmCd()).getBdmFullNm());
	// commodityJsonObj.put(JSTreeVO.TEXT, commodityTextBuilder.toString());
	// commodityJsonObj.put(JSTreeVO.LI_ATTR, liAttrObjCommodity);
	// commodityJsonObj.put(JSTreeVO.A_ATTR, aAttrObjCommodity);
	// commodityTextBuilder.delete(0, commodityTextBuilder.length());
	// List<JSONObject> lstSubCommodityChild = new ArrayList<JSONObject>();
	// for (SubCommodityVO subCommodityVO : commodityVO.getSubCommodityVOs()) {
	// JSONObject subCommodityJsonObj = new JSONObject();
	// JSONObject liAttrObjSubCommodity = new JSONObject();
	// liAttrObjCommodity.put(CLASS_CONS, Constants.EMPTY_STRING);
	// JSONObject aAttrObjSubCommodity = new JSONObject();
	// aAttrObjCommodity.put(CLASS_CONS, CLASS_A_ATTR_SUBCOMMODITY);
	// subCommodityJsonObj.put(JSTreeVO.ID, subCommodityVO.getSubCommodityCd());
	// subCommodityTextBuilder.append(SPAN_SUBCOMMODITY_CLASS);
	// subCommodityTextBuilder.append(subCommodityVO.getSubCommodityCd());
	// subCommodityTextBuilder.append(END_SPAN);
	// subCommodityTextBuilder.append(Constants.WHITE_SPACE);
	// subCommodityTextBuilder.append(SPAN_SUBCOMMODITY_DESC_CLASS);
	// subCommodityTextBuilder.append(subCommodityVO.getSubCommodityDesc());
	// subCommodityTextBuilder.append(END_SPAN);
	//
	// subCommodityJsonObj.put(JSTreeVO.TEXT, subCommodityTextBuilder.toString());
	// subCommodityJsonObj.put(JSTreeVO.LI_ATTR, liAttrObjSubCommodity);
	// subCommodityJsonObj.put(JSTreeVO.A_ATTR, aAttrObjSubCommodity);
	// lstSubCommodityChild.add(subCommodityJsonObj);
	// subCommodityTextBuilder.delete(0, subCommodityTextBuilder.length());
	// }
	// commodityJsonObj.put(JSTreeVO.CHILDREN, lstSubCommodityChild);
	// lstSubDeptJson.add(commodityJsonObj);
	// }
	// return lstSubDeptJson;
	// }
	/**
	 * Get excel document.
	 * @return The workbook of POI library have byte array of excel file
	 * @throws DSVException
	 *             If cannot get excel template
	 * @author anhtran.
	 */
	// private HSSFWorkbook getExcelTemplate() throws DSVException {
	// HSSFWorkbook workbook = null;
	// try {
	// InputStream is = this.getClass().getResourceAsStream("/excel_vendor_hierarchy_mapping_template.xls");
	// workbook = new HSSFWorkbook(is);
	// } catch (IOException e) {
	// LOG.error(e.getMessage(), e);
	// throw new DSVException(e);
	// }
	// return workbook;
	// }
	/**
	 * Build excel file.
	 * @param response
	 *            The response of HttpServlet
	 * @param workbook
	 *            The workbook of POI library contain byte array will be exported to excel file
	 * @throws DSVException
	 *             If the workbook cannot export to excel file
	 * @author anhtran
	 */
	// private void buildExcelFile(HttpServletResponse response, HSSFWorkbook workbook, String fileName) throws DSVException {
	// try {
	// response.setHeader(Constants.CONTENT_DISPOSITION, Constants.FILE_NAME_ATTACHMENT + fileName + Constants.EXCEL_EXTENSION);
	// response.setContentType(Constants.EXCEL_CONTENT_TYPE);
	// ServletOutputStream outputStream = response.getOutputStream();
	// workbook.write(outputStream);
	// outputStream.flush();
	// outputStream.close();
	// } catch (IOException e) {
	// throw new DSVException(e);
	// }
	// }
	/**
	 * detectMimeType.
	 * @param inputStream
	 *            :InputStream
	 * @return String
	 * @throws IOException
	 *             :IOException
	 * @author anhtran.
	 */
	// public String detectMimeType(final InputStream inputStream) throws IOException {
	// TikaInputStream tikaIS = null;
	// try {
	// Tika tika = new Tika();
	// String mediaType = tika.detect(inputStream);
	// LOG.info("CHECKKKKKKK: " + mediaType);
	// tikaIS = TikaInputStream.get(inputStream);
	// final Metadata metadata = new Metadata();
	// // metadata.set(Metadata.RESOURCE_NAME_KEY, file.getName());
	// LOG.info("CHECKKKKKKK1: " + DETECTOR.detect(tikaIS, metadata).toString());
	// return DETECTOR.detect(tikaIS, metadata).toString();
	// } finally {
	// if (tikaIS != null) {
	// tikaIS.close();
	// }
	// }
	// }
	/**
	 * convertListHierarchyMapVOToMap.
	 * @param list
	 *            :List<HierarchyMapVO>
	 * @return Map<String, HierarchyMapVO>
	 * @author anhtran.
	 */
	// private Map<String, HierarchyMapVO> convertListHierarchyMapVOToMap(List<HierarchyMapVO> list) {
	// Map<String, HierarchyMapVO> map = new HashMap<String, HierarchyMapVO>();
	// StringBuilder stringBuilder = new StringBuilder();
	// for (HierarchyMapVO hierarchyMapVO : list) {
	// stringBuilder.append(hierarchyMapVO.getCategoryAbb1());
	// stringBuilder.append(hierarchyMapVO.getCategoryAbb2());
	// stringBuilder.append(hierarchyMapVO.getCategoryAbb3());
	// stringBuilder.append(hierarchyMapVO.getCategoryAbb4());
	// map.put(stringBuilder.toString(), hierarchyMapVO);
	// stringBuilder.delete(0, stringBuilder.length());
	// }
	// return map;
	// }
	/**
	 * convertHierarchyMapVOMapToList.
	 * @param map
	 *            :Map<String, HierarchyMapVO>
	 * @return List<HierarchyMapVO>
	 * @author anhtran.
	 */
	// private List<HierarchyMapVO> convertHierarchyMapVOMapToList(Map<String, HierarchyMapVO> map) {
	// List<HierarchyMapVO> list = new ArrayList<HierarchyMapVO>();
	// for (Entry<String, HierarchyMapVO> entry : map.entrySet()) {
	// list.add(entry.getValue());
	// }
	// return list;
	// }
	/**
	 * deleteEntyRlshp.
	 * @param strJson
	 *            :String
	 * @param vendorId
	 *            :String
	 * @return int
	 * @author anhtran.
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = { DELETE_ENTYRLSHP_PATH }, method = RequestMethod.POST)
	public String deleteEntyRlshp(@RequestParam("vendorId") String vendorId, @RequestParam("strJson") String strJson) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<HierarchyMapVO> list = null;
		String errMessage = Constants.EMPTY_STRING;
		JSONObject jsonResult = new JSONObject();
		try {
			list = objectMapper.readValue(strJson, objectMapper.getTypeFactory().constructCollectionType(List.class, HierarchyMapVO.class));
			LOG.info("deleteEntyRlshp !!!");
			LOG.info("categorys: " + list.get(AntiMagicNumber.ZERO).getCategoryIds());
			this.vendorManagementService.deleteEntyRlshp(vendorId, list.get(AntiMagicNumber.ZERO).getCategoryIds(), list.get(AntiMagicNumber.ZERO).getSubCommodityCd());
		} catch (JsonParseException e) {
			errMessage = e.getMessage();
			LOG.error(e.getMessage(), e);
		} catch (JsonMappingException e) {
			errMessage = e.getMessage();
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			errMessage = e.getMessage();
			LOG.error(e.getMessage(), e);
		} catch (DSVException e) {
			errMessage = e.getMessage();
			LOG.error(e.getMessage(), e);
		} catch (SQLException e) {
			errMessage = e.getMessage();
			LOG.error(e.getMessage(), e);
		}
		jsonResult.put(ERROR_MESSAGE_CONS, errMessage);
		return jsonResult.toString();

	}

	/**
	 * Get HEB Hierarchy.
	 * @param commodityCd
	 *            :String
	 * @return List<JSONObject>
	 * @author anhtran
	 * @throws DSVException
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = { GET_HEB_HIERARCHY_ROOT }, method = RequestMethod.GET)
	public List<JSONObject> getHEBHierarchyRoot(@RequestParam("id") String commodityCd, HttpSession httpSession) throws DSVException {
		LOG.info("getHEBHierarchyRoot");
		LOG.info("commodityCd : " + commodityCd);
		List<JSONObject> lstSubDeptJson = null;
		// try {
		lstSubDeptJson = new ArrayList<JSONObject>();
		if (Constants.NUMBER_SIGN.equals(commodityCd)) {
			List<CommodityVO> lst = this.cacheService.getCommodityDetailList();
			Collections.sort(lst, CommodityVO.CommodityVOComparator);
			Map<String, BDMDetailVO> bdmDetailMap = this.bdmServiceWS.getBDMDetailList();
			StringBuilder commodityTextBuilder = new StringBuilder();
			for (CommodityVO commodityVO : lst) {
				if (!commodityVO.getSubCommodityVOs().isEmpty()) {
					JSONObject commodityJsonObj = new JSONObject();
					JSONObject liAttrObjCommodity = new JSONObject();
					liAttrObjCommodity.put(CLASS_CONS, Constants.EMPTY_STRING);
					JSONObject aAttrObjCommodity = new JSONObject();
					aAttrObjCommodity.put(CLASS_CONS, CLASS_A_ATTR_COMMODITY);
					JSONObject stateObjCommodity = new JSONObject();
					stateObjCommodity.put(DISABLED, true);
					commodityJsonObj.put(JSTreeVO.ID, commodityVO.getCommodityCd() + COMMODITY_SUFFIXES);
					commodityTextBuilder.append(commodityVO.getCommodityCd());
					commodityTextBuilder.append(Constants.DASH_WHITE_SPACE);
					commodityTextBuilder.append(commodityVO.getCommodityDesc());
					commodityTextBuilder.append(Constants.DASH_WHITE_SPACE);
					commodityTextBuilder.append(com.heb.enterprise.dsv.utils.Helper.isEmpty(commodityVO.getBdmCd()) ? BDM : bdmDetailMap.get(commodityVO.getBdmCd()).getBdmFullNm());
					commodityJsonObj.put(JSTreeVO.TEXT, commodityTextBuilder.toString());
					commodityJsonObj.put(JSTreeVO.LI_ATTR, liAttrObjCommodity);
					commodityJsonObj.put(JSTreeVO.A_ATTR, aAttrObjCommodity);
					commodityJsonObj.put(JSTreeVO.STATE, stateObjCommodity);
					commodityTextBuilder.delete(0, commodityTextBuilder.length());
					if (commodityVO.getSubCommodityVOs().size() > AntiMagicNumber.ZERO) {
						commodityJsonObj.put(JSTreeVO.CHILDREN, true);
					} else {
						commodityJsonObj.put(JSTreeVO.CHILDREN, false);
					}
					lstSubDeptJson.add(commodityJsonObj);
				}
			}
		} else {
			lstSubDeptJson = this.getHEBHierarchyChild(commodityCd);
		}
		// }
		// catch (DSVException e) {
		// lstSubDeptJson = new ArrayList<JSONObject>();
		// LOG.info(e.getMessage(), e);
		// JSONObject jsonObject = new JSONObject();
		// jsonObject.put(ERROR, CONNECTION_DOWN);
		// lstSubDeptJson.add(jsonObject);
		// }
		return lstSubDeptJson;
	}

	/**
	 * Get HEB Hierarchy.
	 * @param commodityCd
	 *            :String
	 * @throws DSVException
	 *             :DSVException
	 * @return String
	 * @author anhtran
	 */
	@SuppressWarnings("unchecked")
	private List<JSONObject> getHEBHierarchyChild(String commodityCd) throws DSVException {
		LOG.info("commodityCd: " + commodityCd);
		List<JSONObject> lstSubCommodityJson = new ArrayList<JSONObject>();
		List<SubCommodityVO> subCommodityList;
		subCommodityList = this.cacheService.getSubCommByCommodityId(commodityCd);
		StringBuilder subCommodityTextBuilder = new StringBuilder();
		for (SubCommodityVO subCommodityVO : subCommodityList) {
			JSONObject subCommodityJsonObj = new JSONObject();
			JSONObject liAttrObjSubCommodity = new JSONObject();
			JSONObject aAttrObjSubCommodity = new JSONObject();
			subCommodityTextBuilder.append(SPAN_SUBCOMMODITY_CLASS);
			subCommodityTextBuilder.append(subCommodityVO.getSubCommodityCd());
			subCommodityTextBuilder.append(END_SPAN);
			subCommodityTextBuilder.append(Constants.WHITE_SPACE);
			subCommodityTextBuilder.append(SPAN_SUBCOMMODITY_DESC_CLASS);
			subCommodityTextBuilder.append(subCommodityVO.getSubCommodityDesc());
			subCommodityTextBuilder.append(END_SPAN);
			subCommodityJsonObj.put(JSTreeVO.ID, subCommodityVO.getSubCommodityCd());
			subCommodityJsonObj.put(JSTreeVO.TEXT, subCommodityTextBuilder.toString());
			subCommodityJsonObj.put(JSTreeVO.LI_ATTR, liAttrObjSubCommodity);
			subCommodityJsonObj.put(JSTreeVO.A_ATTR, aAttrObjSubCommodity);
			subCommodityTextBuilder.delete(0, subCommodityTextBuilder.length());
			lstSubCommodityJson.add(subCommodityJsonObj);
		}

		return lstSubCommodityJson;
	}

	/**
	 * searchHebHierarchy.
	 * @param str
	 *            :String
	 * @return String
	 * @author anhtran
	 */
	@ResponseBody
	@RequestMapping(value = { SEARCH_HEB_HIERARCHY }, method = RequestMethod.GET)
	public List<String> searchHebHierarchy(String str) {
		List<String> list = new ArrayList<String>();
		Map<String, BDMDetailVO> bdmDetailMap = null;
		try {
			bdmDetailMap = this.bdmServiceWS.getBDMDetailList();
			Map<Integer, CommodityVO> mapCommodityVO = this.cacheService.getCommodityDetailMap();
			StringBuilder commodityTextBuilder = new StringBuilder();
			StringBuilder subCommodityTextBuilder = new StringBuilder();
			for (Entry<Integer, CommodityVO> entry : mapCommodityVO.entrySet()) {
				commodityTextBuilder.append(entry.getValue().getCommodityCd());
				commodityTextBuilder.append(Constants.DASH_WHITE_SPACE);
				commodityTextBuilder.append(entry.getValue().getCommodityDesc());
				commodityTextBuilder.append(Constants.DASH_WHITE_SPACE);
				commodityTextBuilder.append(bdmDetailMap.get(entry.getValue().getBdmCd()).getBdmFullNm());
				if (commodityTextBuilder.toString().toUpperCase().contains(Helper.trim(str).toUpperCase())) {
					list.add(String.valueOf(entry.getValue().getCommodityCd()) + COMMODITY_SUFFIXES);
				} else {
					if (!Helper.isEmpty(entry.getValue().getSubCommodityVOs())) {
						for (SubCommodityVO subCommodityVO : entry.getValue().getSubCommodityVOs()) {
							subCommodityTextBuilder.append(subCommodityVO.getSubCommodityCd());
							subCommodityTextBuilder.append(Constants.WHITE_SPACE);
							subCommodityTextBuilder.append(subCommodityVO.getSubCommodityDesc());
							if (subCommodityTextBuilder.toString().toUpperCase().contains(Helper.trim(str).toUpperCase())) {
								list.add(String.valueOf(entry.getValue().getCommodityCd()) + COMMODITY_SUFFIXES);
								subCommodityTextBuilder.delete(0, subCommodityTextBuilder.length());
								break;
							}
							subCommodityTextBuilder.delete(0, subCommodityTextBuilder.length());
						}
					}
				}
				commodityTextBuilder.delete(0, commodityTextBuilder.length());
			}
		} catch (DSVException e) {
			LOG.info(e.getMessage(), e);
			list = new ArrayList<String>();
			list.add(ERROR);
			list.add(e.getMessage());
		}
		return list;
	}
}
