/*
 * $Id: RetailCostServiceImpl.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.taglibs.standard.lang.jstl.Constants;

import com.heb.enterprise.dsv.assortment.dao.CostReviewDAO;
import com.heb.enterprise.dsv.assortment.service.CostReviewService;

/**
 * RetailCostServiceImpl interface.
 * @author diemnguyen
 */
@Service
public class CostReviewServiceImpl implements CostReviewService {
	/**
	 * RetailCostDAO.
	 */
	@Autowired
	private CostReviewDAO retailDao;

	/**
	 * find Retail and Cost by searching criteria based on the RetailCostSearchModel.
	 * @param model
	 *            RetailCostSearchModel
	 * @param tabActive
	 *            :There are 2 types : Pass/Failed
	 * @return List<RetailCostModel> List of ProductSearchModel is retrieved
	 * @throws DSVException
	 *             - If could not connect to database
	 * @author diemnguyen.
	 */
	@Override
	public List<RetailCostVO> getProductWithCostChanges(RetailCostSearchVO model, String tabActive)
			throws DSVException {
		List<RetailCostVO> lstResult = this.retailDao.getProductWithCostChanges(model, tabActive);
		for (RetailCostVO retailCostModel : lstResult) {
			retailCostModel.setLstCompetitor(this.getCompetitorList());
			retailCostModel.setCurrentComp(Constants.YES);
		}
		return lstResult;
	}

	/**
	 * total Of Result ForEach Status (passed or failed).
	 * @param model
	 *            RetailCostSearchModel
	 * @param tabActive
	 *            String
	 * @return Map<String, Integer> result - Each of status has matches result is retrieved
	 * @throws DSVException
	 *             - If could not connect to database
	 * @author diemnguyen
	 */
	@Override
	public Map<String, Integer> getTotalPassedFailed(RetailCostSearchVO model,
			String tabActive) throws DSVException {
		return this.retailDao.getTotalPassedFailed(model, tabActive);
	}

	/**
	 * Reject product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public String rejectProducts(List<RetailCostVO> retailCostVOs) throws DSVException {
		return this.retailDao.rejectProducts(retailCostVOs);
	}

	/**
	 * Remove product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public String removeProducts(List<RetailCostVO> retailCostVOs) throws DSVException {
		return this.retailDao.removeProducts(retailCostVOs);
	}

	/**
	 * Save product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public String saveProducts(List<RetailCostVO> retailCostVOs) throws DSVException {
		return this.retailDao.saveProducts(retailCostVOs);
	}

	/**
	 * Get competitor list.
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public List<CompetitiveRetails> getCompetitorList() throws DSVException {
		NumberFormat formatter = new DecimalFormat("#0.00");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		List<CompetitiveRetails> lstCompetitor = new ArrayList<CompetitiveRetails>();
		List<String> competitor = new ArrayList<String>();
		competitor.add("Wal-Mart");
		competitor.add("Amazon");
		competitor.add("Target");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		Calendar c = Calendar.getInstance();
		String nextDate = dateFormat.format(c.getTime());
		c.setTime(date); // Now use today date.
		c.add(Calendar.DATE, 10);
		for (String comp : competitor) {
			Random r = new Random();
			double d = 1.0 + r.nextDouble() * 1.0;
			double d2 = 1.1 + r.nextDouble() * 1.0;
			CompetitiveRetails obj = new CompetitiveRetails();
			obj.setCompetitor(comp);
			obj.setOldRetail(Double.parseDouble(formatter.format(d)));
			obj.setOldDate(currentDate);
			obj.setNewDate(nextDate);
			obj.setNewRetail(Double.parseDouble(formatter.format(d2)));
			lstCompetitor.add(obj);
		}
		return lstCompetitor;
	}

	/**
	 * Get Future Cost.
	 * @throws DSVException
	 *             :If could not get values from Service Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public List<RetailCostVO> getFutureSegments() throws DSVException {
		List<RetailCostVO> costVOs = new ArrayList<RetailCostVO>();
		return costVOs;
	}

	/**
	 * getBoundaryRules.
	 * @throws DSVException
	 *             :DSVException.
	 * @return List<BaseVO>
	 * @author diemnguyen.
	 */
	@Override
	public List<BaseVO> getBoundaryRules() throws DSVException {
		return this.retailDao.getBoundaryRules();
	}

}
