/*
 * $Id.: VendorManagementMapper.java, Oct 28, 2014$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.EntyDesVO;
import com.heb.enterprise.dsv.vo.EntyRlshpVO;
import com.heb.enterprise.dsv.vo.EntyVO;

/**
 * VendorManagementMapper.
 * @author anhtran.
 */
public final class VendorManagementMapper {

	private static final Logger LOG = Logger.getLogger(VendorManagementMapper.class);

	/**
	 * VendorManagementMapper.
	 */
	private VendorManagementMapper() {

	}

	/**
	 * getEntyRlshpMapper.
	 * @return RowMapper<EntyRlshpVO>
	 * @author anhtran.
	 */
	public static RowMapper<EntyRlshpVO> getEntyRlshpMapper() {
		RowMapper<EntyRlshpVO> entyMapper = new RowMapper<EntyRlshpVO>() {
			@Override
			public EntyRlshpVO mapRow(ResultSet rs, int arg1) throws SQLException {
				EntyRlshpVO entyVO = new EntyRlshpVO();
				entyVO.setParntEntyId(rs.getString("PARNT_ENTY_ID"));
				entyVO.setParentTemp(rs.getString("PARNT_ENTY_ID"));
				entyVO.setChildEntyId(rs.getString("CHILD_ENTY_ID"));
				entyVO.setHierCntxtCd(Helper.trim(rs.getString("HIER_CNTXT_CD")));
				entyVO.setEntyTypCd(Helper.trim(rs.getString("ENTY_TYP_CD")));
				entyVO.setEntyAbb(Helper.trim(rs.getString("ENTY_LONG_DES")));
				entyVO.setDsplyNbr(Helper.trim(rs.getString("ENTY_DSPLY_NBR")));
				entyVO.setFullPath(entyVO.getParntEntyId() + Constants.GREATER_THAN_SIGN + entyVO.getChildEntyId());
				entyVO.setFullPathAbb(entyVO.getEntyAbb());
				return entyVO;
			}
		};
		return entyMapper;
	}

	/**
	 * getPreparedStatementSetterToInsertEnty.
	 * @param entyVO
	 *            :EntyVO
	 * @return PreparedStatementSetter.
	 * @author anhtran.
	 */
	public static PreparedStatementSetter getPreparedStatementSetterToInsertEnty(final EntyVO entyVO) {
		PreparedStatementSetter preparedStatementSetter = new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				Date date = new Date();
				ps.setInt(AntiMagicNumber.ONE, Integer.parseInt(entyVO.getEntyId()));
				ps.setString(AntiMagicNumber.TWO, entyVO.getEntyTypCd());
				ps.setString(AntiMagicNumber.THREE, Constants.EMPTY_STRING);
				ps.setString(AntiMagicNumber.FOUR, entyVO.getEntyDsplyNbr());
				ps.setString(AntiMagicNumber.FIVE, entyVO.getCre8Uid());
				ps.setTimestamp(AntiMagicNumber.SIX, new Timestamp(date.getTime()));
				ps.setString(AntiMagicNumber.SEVEN, entyVO.getLstUpdtUid());
				ps.setTimestamp(AntiMagicNumber.EIGHT, new Timestamp(date.getTime()));
			}
		};
		return preparedStatementSetter;
	}

	/**
	 * getEntyMapper.
	 * @return RowMapper<EntyVO>
	 * @author anhtran.
	 */
	public static RowMapper<EntyVO> getEntyMapper() {
		RowMapper<EntyVO> entyMapper = new RowMapper<EntyVO>() {
			@Override
			public EntyVO mapRow(ResultSet rs, int arg1) throws SQLException {
				EntyVO entyVO = new EntyVO();
				entyVO.setEntyId(String.valueOf(rs.getInt("ENTY_ID")));
				return entyVO;
			}
		};
		return entyMapper;
	}

	/**
	 * getPreparedStatementToInsertEntyRlshp.
	 * @param entyRlshpVOs
	 *            :List<EntyRlshpVO>
	 * @return BatchPreparedStatementSetter.
	 * @author anhtran
	 */
	public static BatchPreparedStatementSetter getPreparedStatementToInsertEntyRlshp(final List<EntyRlshpVO> entyRlshpVOs) {
		BatchPreparedStatementSetter preparedStatementSetter = new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Date date = new Date();
				EntyRlshpVO entyRlshpVO = entyRlshpVOs.get(i);
				// Column: PARNT_ENTY_ID
				ps.setInt(AntiMagicNumber.ONE, Integer.parseInt(entyRlshpVO.getParntEntyId()));
				// Column: CHILD_ENTY_ID
				ps.setInt(AntiMagicNumber.TWO, Integer.parseInt(entyRlshpVO.getChildEntyId()));
				// Column: HIER_CNTXT_CD
				ps.setString(AntiMagicNumber.THREE, entyRlshpVO.getHierCntxtCd());
				// DFLT_PARNT_SW
				ps.setString(AntiMagicNumber.FOUR, entyRlshpVO.getDfltParntSw());
				// Column: DSPLY_SW
				ps.setString(AntiMagicNumber.FIVE, entyRlshpVO.getDsplySw());
				// Column: SEQ_NBR
				ps.setString(AntiMagicNumber.SIX, entyRlshpVO.getSeqNbr());
				// Column: CRE8_UID
				ps.setString(AntiMagicNumber.SEVEN, entyRlshpVO.getCre8UId());
				// Column: CRE8_TS
				ps.setTimestamp(AntiMagicNumber.EIGHT, new Timestamp(date.getTime()));
				// Column: LST_UPDT_UID
				ps.setString(AntiMagicNumber.NINE, entyRlshpVO.getLstUpdtUid());
				// Column: LST_UPDT_TS
				ps.setTimestamp(AntiMagicNumber.TEN, new Timestamp(date.getTime()));
				// Column: EFF_DT
				ps.setTimestamp(AntiMagicNumber.ELEVEN, new Timestamp(date.getTime()));
				// Column: EXPRN_DT
				ps.setTimestamp(AntiMagicNumber.TWELVE, new Timestamp(date.getTime()));
			}

			@Override
			public int getBatchSize() {
				return entyRlshpVOs.size();
			}
		};
		return preparedStatementSetter;
	}

	/**
	 * getPreparedStatementSetterToGetEntyId.
	 * @param entyVO
	 *            :EntyVO
	 * @return PreparedStatementSetter.
	 * @author anhtran.
	 */
	public static PreparedStatementSetter getPreparedStatementSetterToGetEntyId(final EntyVO entyVO) {
		PreparedStatementSetter preparedStatementSetter = new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				LOG.info("entyVO.getEntyAbb(): " + entyVO.getEntyAbb());
				ps.setString(AntiMagicNumber.ONE, entyVO.getParntEntyId());
				ps.setString(AntiMagicNumber.TWO, entyVO.getEntyAbb());
			}
		};
		return preparedStatementSetter;
	}

	/**
	 * getPreparedStatementSetterToInsertEntyRlshp.
	 * @param entyRlshpVO
	 *            :EntyRlshpVO
	 * @return PreparedStatementSetter.
	 * @author anhtran.
	 */
	public static PreparedStatementSetter getPreparedStatementSetterToInsertEntyRlshp(final EntyRlshpVO entyRlshpVO) {
		PreparedStatementSetter preparedStatementSetter = new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				Date date = new Date();
				// Column: PARNT_ENTY_ID
				ps.setInt(AntiMagicNumber.ONE, Integer.parseInt(entyRlshpVO.getParntEntyId()));
				// Column: CHILD_ENTY_ID
				ps.setInt(AntiMagicNumber.TWO, Integer.parseInt(entyRlshpVO.getChildEntyId()));
				// Column: HIER_CNTXT_CD
				ps.setString(AntiMagicNumber.THREE, entyRlshpVO.getHierCntxtCd());
				// DFLT_PARNT_SW
				ps.setString(AntiMagicNumber.FOUR, entyRlshpVO.getDfltParntSw());
				// Column: DSPLY_SW
				ps.setString(AntiMagicNumber.FIVE, entyRlshpVO.getDsplySw());
				// Column: SEQ_NBR
				ps.setString(AntiMagicNumber.SIX, entyRlshpVO.getSeqNbr());
				// Column: CRE8_UID
				ps.setString(AntiMagicNumber.SEVEN, entyRlshpVO.getCre8UId());
				// Column: CRE8_TS
				ps.setTimestamp(AntiMagicNumber.EIGHT, new Timestamp(date.getTime()));
				// Column: LST_UPDT_UID
				ps.setString(AntiMagicNumber.NINE, entyRlshpVO.getLstUpdtUid());
				// Column: LST_UPDT_TS
				ps.setTimestamp(AntiMagicNumber.TEN, new Timestamp(date.getTime()));
				// Column: EFF_DT
				ps.setTimestamp(AntiMagicNumber.ELEVEN, new Timestamp(date.getTime()));
				// Column: EXPRN_DT
				ps.setTimestamp(AntiMagicNumber.TWELVE, new Timestamp(date.getTime()));
			}
		};
		return preparedStatementSetter;
	}

	/**
	 * getPreparedStatementToInsertEntyRlshp.
	 * @param entyRlshpVOs
	 *            :List<EntyRlshpVO>
	 * @return BatchPreparedStatementSetter.
	 * @author anhtran
	 */
	public static BatchPreparedStatementSetter getPreparedStatementToUpdateEntyRlshp(final List<EntyRlshpVO> entyRlshpVOs) {
		BatchPreparedStatementSetter preparedStatementSetter = new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				EntyRlshpVO entyRlshpVO = entyRlshpVOs.get(i);
				// Column: CHILD_ENTY_ID
				ps.setInt(AntiMagicNumber.ONE, Integer.parseInt(entyRlshpVO.getChildEntyIdTemp()));
				// Column: PARNT_ENTY_ID
				ps.setInt(AntiMagicNumber.TWO, Integer.parseInt(entyRlshpVO.getParntEntyId()));
				LOG.info("entyRlshpVO.getParntEntyId(): " + entyRlshpVO.getParntEntyId());
				// Column: CHILD_ENTY_ID
				ps.setInt(AntiMagicNumber.THREE, Integer.parseInt(entyRlshpVO.getChildEntyId()));
				// Column: HIER_CNTXT_CD
				ps.setString(AntiMagicNumber.FOUR, entyRlshpVO.getHierCntxtCd());
			}

			@Override
			public int getBatchSize() {
				return entyRlshpVOs.size();
			}
		};
		return preparedStatementSetter;
	}

	/**
	 * getPreparedStatementSetterToInsertEntyDes.
	 * @param entyDesVO
	 *            :EntyDesVO
	 * @return PreparedStatementSetter.
	 * @author anhtran
	 */
	public static PreparedStatementSetter getPreparedStatementSetterToInsertEntyDes(final EntyDesVO entyDesVO) {
		PreparedStatementSetter preparedStatementSetter = new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				Date date = new Date();
				// Column: ENTY_ID
				ps.setLong(AntiMagicNumber.ONE, entyDesVO.getEntyId());
				// Column: HIER_CNTXT_CD
				ps.setString(AntiMagicNumber.TWO, entyDesVO.getHierCntxtCd());
				// Column: ENTY_SHRT_DES
				ps.setString(AntiMagicNumber.THREE, Helper.trim(entyDesVO.getEntyShrtDes()));
				// ENTY_LONG_DES
				ps.setString(AntiMagicNumber.FOUR, Helper.trim(entyDesVO.getEntyLongDes()));
				// Column: CRE8_UID
				ps.setString(AntiMagicNumber.FIVE, entyDesVO.getCre8Uid());
				// Column: CRE8_TS
				ps.setTimestamp(AntiMagicNumber.SIX, new Timestamp(date.getTime()));
				// Column: LST_UPDT_UID
				ps.setString(AntiMagicNumber.SEVEN, entyDesVO.getLstUpdtUid());
				// Column: LST_UPDT_TS
				ps.setTimestamp(AntiMagicNumber.EIGHT, new Timestamp(date.getTime()));
			}
		};
		return preparedStatementSetter;
	}

	/**
	 * getPreparedStatementToDeleteEntyDes.
	 * @param entyDesVOs
	 *            :List<EntyDesVO>
	 * @return BatchPreparedStatementSetter.
	 * @author anhtran
	 */
	public static BatchPreparedStatementSetter getPreparedStatementToDeleteEntyDes(final List<EntyDesVO> entyDesVOs) {
		BatchPreparedStatementSetter preparedStatementSetter = new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				EntyDesVO desVO = entyDesVOs.get(i);
				// Column: ENTY_ID
				ps.setLong(AntiMagicNumber.ONE, desVO.getEntyId());
				// Column: HIER_CNTXT_CD
				ps.setString(AntiMagicNumber.TWO, desVO.getHierCntxtCd());
			}

			@Override
			public int getBatchSize() {
				return entyDesVOs.size();
			}
		};
		return preparedStatementSetter;
	}

	/**
	 * getPreparedStatementToDeleteEnty.
	 * @param entyIdList
	 *            :List<String>
	 * @return BatchPreparedStatementSetter
	 */
	public static BatchPreparedStatementSetter getPreparedStatementToDeleteEnty(final List<String> entyIdList) {
		BatchPreparedStatementSetter preparedStatementSetter = new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				// Column: ENTY_ID
				ps.setLong(AntiMagicNumber.ONE, Long.valueOf(entyIdList.get(i)));
				LOG.info("ENTYID: " + entyIdList.get(i));
			}

			@Override
			public int getBatchSize() {
				return entyIdList.size();
			}
		};
		return preparedStatementSetter;
	}

}
