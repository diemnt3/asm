/*
 * $Id: GlobalRulesDAO.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao;

import java.util.List;
import java.util.Map;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;

/**
 * GlobalManagementDAO interface.
 * @author duyen.le
 */
public interface GlobalRulesDAO {
	/**
	 * get Rule set by CommodityID.
	 * @param commodityID
	 *            String
	 * @param subDeptID
	 *            String
	 * @throws DSVException
	 *             DSVException
	 * @return List<RuleConfigVO>
	 * @author duyen.le
	 */
	Map<String, AssortmentRulesVO> getGlobalRuleSet(String subDeptID, String commodityID) throws DSVException;

	/**
	 * updateRuleValuesToDfns.
	 * @param lstRulesDfns
	 *            List<RuleDefinationVO>
	 * @param userId
	 *            String
	 * @return String
	 * @throws DSVException
	 *             DSVException
	 * @author thangdang.
	 */
	String updateRuleValuesToDfns(List<RuleDefinationVO> lstRulesDfns, String userId) throws DSVException;

	/**
	 * updateLvl.
	 * @param lvlId
	 *            long
	 * @param userId
	 *            String
	 * @return String
	 * @throws DSVException
	 *             DSVException
	 * @author Duyen.le.
	 */
	int updateLvl(long lvlId, String userId) throws DSVException;

	/**
	 * updateRuleValuesToDfns.
	 * @param lstRulesDfns
	 *            List<RuleDefinationVO>
	 * @param lvlId
	 *            int
	 * @param userId
	 *            String
	 * @return String
	 * @throws DSVException
	 *             DSVException
	 * @author thangdang.
	 */
	String insertRulesToDfns(List<RuleDefinationVO> lstRulesDfns, long lvlId, String userId) throws DSVException;

	/**
	 * insertlvl.
	 * @param lvlID
	 *            long
	 * @param commodityId
	 *            String
	 * @param deptId
	 *            String
	 * @param subDeptId
	 *            String
	 * @param itmCls
	 *            long
	 * @param vendorId
	 *            String
	 * @param subCommodity
	 *            String
	 * @param lvlType
	 *            String
	 * @param userId
	 *            String
	 * @return long
	 * @throws DSVException
	 *             DSVException
	 * @author thangdang.
	 */
	long insertlvl(long lvlID, String userId, String commodityId, String deptId, String subDeptId, long itmCls, String vendorId, String subCommodity, String lvlType)
			throws DSVException;

	/**
	 * getGlobalCommodityOverride.
	 * @return Map<String, LevelVO>
	 * @author anhtran.
	 */
	Map<String, LevelVO> getGlobalCommodityOverride();

	/**
	 * insertlvlActivated.
	 * @param commodityID
	 *            String
	 * @param lstAssortmentRulesVO
	 *            List<AssortmentRulesVO>
	 * @param userId
	 *            String
	 * @param vendorId
	 *            String
	 * @param lvlId
	 *            long
	 * @return String
	 * @throws DSVException
	 *             DSVException
	 * @author thangdang
	 */
	String insertlvlActivated(String userId, List<AssortmentRulesVO> lstAssortmentRulesVO,
			String vendorId, long lvlId, String commodityID) throws DSVException;

	/**
	 * removeRule.
	 * @param lvlId
	 *            long
	 * @return boolean
	 * @author thangdang
	 */
	boolean removeRule(long lvlId);

	/**
	 * removeRules.
	 * @param lstLvlIds
	 *            List<Long>
	 * @return boolean
	 * @author thangdang
	 */
	boolean removeRules(List<Long> lstLvlIds);

	/**
	 * removeLvl.
	 * @param lvlId
	 *            long
	 * @return boolean
	 * @author thangdang
	 */
	boolean removeLvl(long lvlId);

	/**
	 * removeLvls.
	 * @param lstLvlIds
	 *            List<Long>
	 * @return boolean
	 * @author thangdang
	 */
	boolean removeLvls(List<Long> lstLvlIds);

	/**
	 * getLvlIdByCommodity.
	 * @param commodityId
	 *            String
	 * @param deptId
	 *            String
	 * @param subDeptId
	 *            String
	 * @return long
	 * @author thangdang
	 */
	long getLvlIdByCommodity(String commodityId, String deptId, String subDeptId);

	/**
	 * getLvlIdBysubDept.
	 * @param deptId
	 *            String
	 * @param subDeptId
	 *            String
	 * @return long
	 * @author thangdang
	 */
	long getLvlIdBysubDept(String deptId, String subDeptId);

	/**
	 * Get max lvlID form LVL table.
	 * @return long
	 * @author Duyen.Le
	 */
	long getMaxLVLID();
}
