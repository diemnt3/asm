/*
 * $Id: CommonDaoImpl.java,v 1.12 2015/03/16 09:36:22 vn55228 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.heb.enterprise.dsv.assortment.dao.CommonDAO;
import com.heb.enterprise.dsv.assortment.dao.mapper.CommonMapper;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * @author thangdang
 */
@Repository
public class CommonDaoImpl extends BaseDAOImpl implements CommonDAO {
	private static final Logger LOG = Logger.getLogger(CommonDaoImpl.class);
	/**
	 * findAllLogicRuleSql query Sql.
	 */
	@Value("${coms.dsv.assortment.sql.getAllLogicRule}")
	private String findAllLogicRuleSql;
	/**
	 * findRuleByRuleGroup query Sql.
	 */
	@Value("${coms.dsv.assortment.sql.getRuleByRuleGruop}")
	private String findRuleByRuleGroup;
	/**
	 * getAllRule query Sql.
	 */
	@Value("${coms.dsv.asm.getAllRule}")
	private String sqlGetAllRule;

	/**
	 * findAllVendor query Sql.
	 */
	@Value("${coms.dsv.asm.vendorManagement.findAllVendor}")
	private String sqlFindAllVendor;

	/**
	 * CommonDaoImpl constructor method.
	 */
	private CommonDaoImpl() {
	}

	/**
	 * Retrieves a list of Logic Rule.
	 * @return List<BaseVO> : List of Logic Rule is retrieved from Database.
	 * @author Duyen.le
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	@Override
	public List<BaseVO> findAllRuleLogicOprtr() throws DSVException {
		List<BaseVO> lstLogicRule = null;
		try {
			lstLogicRule = this.getJdbcTemplate().query(this.findAllLogicRuleSql, CommonMapper.getLogicRuleMapper());
		} catch (DataAccessException e) {
			LOG.fatal(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstLogicRule;
	}

	/**
	 * Retrieves a Rules set by Rule Group.
	 * @return List<AssortmentRulesVO> : List of Rules set are retrieved from Database.
	 * @author Duyen.le
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	@Override
	public List<AssortmentRulesVO> getRulesByRuleGroup() throws DSVException {
		LOG.info("SQL String is : " + this.findRuleByRuleGroup);
		List<AssortmentRulesVO> lstRules = null;
		try {
			lstRules = this.getJdbcTemplate().query(this.findRuleByRuleGroup, CommonMapper.getRuleByRuleGroupMapper());
		} catch (DataAccessException e) {
			LOG.fatal(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstRules;
	}

	@Override
	public List<BaseVO> getAllAssortmentRule() throws DSVException {
		List<BaseVO> lstRules = null;
		try {
			lstRules = this.getJdbcTemplate().query(this.sqlGetAllRule, CommonMapper.getAllRuleMapper());
		} catch (DataAccessException e) {
			LOG.fatal(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstRules;
	}

	/**
	 * Get all list vendors.
	 * @return List<VendorInfoVO>
	 * @author Duyen.le
	 */
	@Override
	public List<VendorInfoVO> getAllListVendors() throws DSVException {
		List<VendorInfoVO> lstVendor = null;
		try {
			lstVendor = this.getJdbcTemplate().query(this.sqlFindAllVendor, CommonMapper.getAllVendorMapper());
		} catch (DataAccessException e) {
			LOG.fatal(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstVendor;
	}

}