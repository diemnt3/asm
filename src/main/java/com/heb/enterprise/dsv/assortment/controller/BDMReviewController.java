/*
 * $Id.: $BDMReviewController.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.ReviewNewProductsService;
import com.heb.enterprise.dsv.assortment.service.impl.ExportExcelTemplate;
import com.heb.enterprise.dsv.assortment.utils.AConstants;
import com.heb.enterprise.dsv.assortment.utils.AHelper;
import com.heb.enterprise.dsv.assortment.utils.ConstantsWeb;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.jaf.security.HebUserDetails;
import com.heb.enterprise.dsv.utils.AntiMagicNumber;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.Page;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.ProductSearchCriteriaVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * BDM Review screen.
 * @author duyen.le
 */
@Controller
@SessionAttributes({ Constants.SESSION_PASSED, Constants.SESSION_FAILED, Constants.SESSION_REJECTED })
public class BDMReviewController {
	private static final Logger LOG = Logger.getLogger(DashboardController.class);
	// this name show in address bar
	private static final String BDM_REVIEW_PATH = Constants.SLASH + ConstantsWeb.BDM_REVIEW_PATH;
	private static final String SEARCH_PROD_FOR_REVIEW_PATH = Constants.SLASH + ConstantsWeb.SEARCH_PRODS_FOR_REVIEW_PATH;
	private static final String REJECT_PROD_PATH = Constants.SLASH + ConstantsWeb.REJECT_PRODS_PATH;
	private static final String APPROVE_PRODS_PATH = Constants.SLASH + ConstantsWeb.APPROVE_PRODS_PATH;
	private static final String UPDATE_PRODS_PATH = Constants.SLASH + ConstantsWeb.UPDATE_PRODS_PATH;
	private static final String UN_REJECT_PRODS_PATH = Constants.SLASH + ConstantsWeb.UN_REJECT_PRODS_PATH;
	private static final String GET_COMMODITY_AUTOCOMPLETE_PATH = ConstantsWeb.AJAX_AUTOCOMPLETE_GET_DATA_COMMODITY;
	private static final String GET_VENDOR_AUTOCOMPLETE_PATH = ConstantsWeb.AJAX_AUTOCOMPLETE_GET_DATA_VENDOR;
	private static final String GET_BRAND_AUTOCOMPLETE_PATH = ConstantsWeb.AJAX_AUTOCOMPLETE_GET_DATA_BRAND;
	private static final String GET_PRODUCT_AUTOCOMPLETE_PATH = ConstantsWeb.AJAX_AUTOCOMPLETE_GET_DATA_PRODUCT;
	private static final String GET_DATA_FAILED_REASON_PATH = ConstantsWeb.AJAX__GET_DATA_FAILED_REASON;
	private static final String EXPORT_PRODUCT_REVIEW_TO_EXCEL_DIRECTLY = ConstantsWeb.EXPORT_PRODUCT_REVIEW_TO_EXCEL_DIRECTLY;

	// TILE
	private static final String BDM_REVIEW_TILE = "BDMReview.tile";

	/**
	 * Inject NewProductsReviewService.
	 */
	@Autowired
	private ReviewNewProductsService productForReviewService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private CacheService cacheService;
	@Autowired
	private CommonService commonService;
	@Autowired
	private ExportExcelTemplate excelTemplate;

	/**
	 * Display The BDM Review screen.
	 * @return String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @author Duyen.le
	 */
	@RequestMapping(value = { BDM_REVIEW_PATH }, method = RequestMethod.GET)
	public final String showNewProductForReviewScreen() throws DSVException {
		LOG.info("show BDM Review screen");
		return BDM_REVIEW_TILE;
	}

	/**
	 * searchForProduct.
	 * @param productSearchCriteriaVO
	 *            ProductSearchCriteriaVO
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * @return String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @author Duyen.le
	 */
	@RequestMapping(value = { SEARCH_PROD_FOR_REVIEW_PATH }, method = RequestMethod.POST)
	@ResponseBody
	public final String searchForProduct(@ModelAttribute("productSearchCriteriaVO") ProductSearchCriteriaVO productSearchCriteriaVO,
			HttpServletRequest request, final HttpServletResponse response) throws DSVException {

		List<ProductRiewVO> lstResults = null;
		JSONObject jsonResult = new JSONObject();
		int rowID = Integer.parseInt(productSearchCriteriaVO.getStart());
		// rowID = (rowID == 0) ? rowID : (rowID + 1);
		productSearchCriteriaVO.setsSortDir(request.getParameter(Constants.SORT_DIR));
		String columnNumber = request.getParameter(Constants.SORT_COLUMN);
		productSearchCriteriaVO.setiSortColumn(this.getColumnSortTable(columnNumber));
		Map<String, String> mapErrors = this.validateSearchCriteria(productSearchCriteriaVO);
		int totalPass = 0;
		int totalFail = 0;
		int totalReject = 0;
		if (mapErrors != null && !mapErrors.isEmpty()) {
			jsonResult.put(Constants.AA_DATA, mapErrors);
			jsonResult.put(Constants.ITOTAL_DISPLAY_RECORDS, 0);
			jsonResult.put(Constants.ITOTAL_RECORDS, 0);
			jsonResult.put(Constants.ITOTAL_RECORD_PASS, totalPass);
			jsonResult.put(Constants.ITOTAL_RECORD_FAIL, totalFail);
			jsonResult.put(Constants.ITOTAL_RECORD_REJECT, totalReject);
		} else {
			List<Object> lstData = new ArrayList<Object>();
			List<ProductRiewVO> lstCountProductRiewVO;
			Page<ProductRiewVO> page = null;
			try {
				page = this.productForReviewService.searchForProducts(productSearchCriteriaVO);
				lstCountProductRiewVO = this.productForReviewService.countProudctRev(productSearchCriteriaVO);
			} catch (DSVException e) {
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				throw new DSVException(e);
			}
			lstResults = page.getPageItems();
			if (Helper.isNormalList(lstResults)) {
				LOG.info("LIST SIZE : " + lstResults.size());
				for (ProductRiewVO productRiewVO : lstResults) {
					rowID++;
					lstData.add(this.mapObjProductRiewVO(productRiewVO, rowID));
				}
			}
			if (!Helper.isEmpty(lstCountProductRiewVO)) {
				for (ProductRiewVO productRiewVO : lstCountProductRiewVO) {
					if (Constants.STRING_P.equals(Helper.trim(productRiewVO.getReviewStatus()))) {
						totalPass = productRiewVO.getTotalRow();
					} else if (Constants.STRING_F.equals(Helper.trim(productRiewVO.getReviewStatus()))) {
						totalFail = productRiewVO.getTotalRow();
					} else if (AConstants.RJECT.equals(Helper.trim(productRiewVO.getReviewStatus()))) {
						totalReject = productRiewVO.getTotalRow();
					}
				}
			}
			jsonResult.put(Constants.ITOTAL_DISPLAY_RECORDS, page.getRowCount());
			jsonResult.put(Constants.ITOTAL_RECORDS, page.getRowCount());
			jsonResult.put(Constants.ITOTAL_RECORD_PASS, totalPass);
			jsonResult.put(Constants.ITOTAL_RECORD_FAIL, totalFail);
			jsonResult.put(Constants.ITOTAL_RECORD_REJECT, totalReject);
			jsonResult.put(Constants.AA_DATA, lstData);

		}
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Approve Products after reviewing.
	 * @param lstProds
	 *            :List<ProductSearchVO>
	 * @param request
	 *            :HttpServletRequest
	 * @return String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @author Duyen.le
	 */
	@RequestMapping(value = { APPROVE_PRODS_PATH }, method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
	@ResponseBody
	public final String approveProducts(@RequestBody List<ProductRiewVO> lstProds, HttpServletRequest request)
			throws DSVException {
		LOG.info("Approve Products after reviewing.");
		JSONObject jsonResult = new JSONObject();
		LOG.info("lstProds : " + lstProds);
		List<String> lstUpcs = new ArrayList<String>();
		if (!Helper.isEmpty(lstProds)) {
			for (ProductRiewVO prod : lstProds) {
				lstUpcs.add(prod.getUpc());
			}
		}
		if (this.productForReviewService.checkApproveReject(lstUpcs)) {
			HebUserDetails user = this.commonService.getUserLogin();
			String userId = Constants.EMPTY_STRING;
			if (!Helper.isEmpty(user)) {
				userId = user.getUsername();
				LOG.info(user.getUsername());
			}
			this.productForReviewService.approveProducts(lstUpcs, userId);
			this.productForReviewService.updateProducts(lstProds);
		} else {
			jsonResult.put(Constants.ERROR_ATTRIBUTE, this.messageSource.getMessage("error.aproved.bdmreview", null, Locale.ENGLISH));
		}
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Update HEB Retail of Products after reviewing.
	 * @param lstProdModel
	 *            List<ProductSearchVO>
	 * @return String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @author Duyen.le
	 */
	@RequestMapping(value = { UPDATE_PRODS_PATH }, method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
	@ResponseBody
	public final String updateProducts(@RequestBody List<ProductRiewVO> lstProds, HttpServletRequest request)
			throws DSVException {
		LOG.info("Update Products after reviewing.");
		JSONObject jsonResult = new JSONObject();
		LOG.info("lstProds : " + lstProds);
		this.productForReviewService.updateProducts(lstProds);
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Approve Products after reviewing.
	 * @param lstProdModel
	 *            List<ProductSearchVO>
	 * @return String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @author Duyen.le
	 */
	@RequestMapping(value = { REJECT_PROD_PATH }, method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
	@ResponseBody
	public final String rejectProducts(@RequestBody List<String> lstProds, HttpServletRequest request)
			throws DSVException {
		LOG.info("Reject Products after reviewing.");
		JSONObject jsonResult = new JSONObject();
		LOG.info("lstProds : " + lstProds);
		if (this.productForReviewService.checkApproveReject(lstProds)) {
			HebUserDetails user = this.commonService.getUserLogin();
			String userId = Constants.EMPTY_STRING;
			if (!Helper.isEmpty(user)) {
				userId = user.getUsername();
				LOG.info(user.getUsername());
			}
			this.productForReviewService.rejectProducts(lstProds, userId);
		} else {
			jsonResult.put(Constants.ERROR_ATTRIBUTE, this.messageSource.getMessage("error.reject.bdmreview", null, Locale.ENGLISH));
		}
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Approve Products after reviewing.
	 * @param lstProdModel
	 *            List<ProductSearchVO>
	 * @return String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @author Duyen.le
	 */
	@RequestMapping(value = { UN_REJECT_PRODS_PATH }, method = { RequestMethod.POST }, headers = { "Content-type=application/json" })
	@ResponseBody
	public final String unrejectProducts(@RequestBody List<String> lstProds, HttpServletRequest request)
			throws DSVException {
		LOG.info("Un-Reject Products after reviewing.");
		JSONObject jsonResult = new JSONObject();
		LOG.info("lstProds : " + lstProds);
		if (this.productForReviewService.checkUnReject(lstProds)) {
			HebUserDetails user = this.commonService.getUserLogin();
			String userId = Constants.EMPTY_STRING;
			if (!Helper.isEmpty(user)) {
				userId = user.getUsername();
				LOG.info(user.getUsername());
			}
			this.productForReviewService.unRejectProducts(lstProds, userId);
		} else {
			jsonResult.put(Constants.ERROR_ATTRIBUTE, this.messageSource.getMessage("error.unreject.bdmreview", null, Locale.ENGLISH));
		}
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Get Map of Invoice.
	 * @param invoiceModel
	 *            InvoiceVO
	 * @return map invoice Map<String, Object>
	 * @author thangdang
	 */
	private Map<String, Object> mapObjProductRiewVO(ProductRiewVO productRiewVO, int rowId) {
		Map<String, Object> lstInvoiceJson = new HashMap<String, Object>();
		/* lstInvoiceJson.put(AConstants.DT_RowId, rowId); */
		lstInvoiceJson.put(AConstants.NO, rowId);
		lstInvoiceJson.put(AConstants.BDMRIVEW_UPC, productRiewVO.getUpc());
		lstInvoiceJson.put(AConstants.BDMRIVEW_DESCRIPTION, productRiewVO.getDescription());
		lstInvoiceJson.put(AConstants.BDMRIVEW_SIZE, productRiewVO.getSize());
		lstInvoiceJson.put(AConstants.BDMRIVEW_MAP, productRiewVO.getMap());
		lstInvoiceJson.put(AConstants.BDMRIVEW_PREPRICE, productRiewVO.getPrePrice());
		lstInvoiceJson.put(AConstants.BDMRIVEW_MSRP, productRiewVO.getMsrp());
		lstInvoiceJson.put(AConstants.BDMRIVEW_SRP, productRiewVO.getSrp());
		lstInvoiceJson.put(AConstants.BDMRIVEW_COST, productRiewVO.getCost());
		lstInvoiceJson.put(AConstants.BDMRIVEW_GPPERCENT, productRiewVO.getGpPercent());// productRiewVO.getGpPercent()
		lstInvoiceJson.put(AConstants.BDMRIVEW_PENNYPROFIT, productRiewVO.getPennyProfit());// productRiewVO.getPennyProfit()
		lstInvoiceJson.put(AConstants.BDMRIVEW_IMAGE, productRiewVO.getImageUri());
		lstInvoiceJson.put(AConstants.BDMRIVEW_FAILEDREASON, productRiewVO.getFailedReason());
		lstInvoiceJson.put(AConstants.BDMRIVEW_TOTAL_ROWS, productRiewVO.getTotalRow());
		lstInvoiceJson.put(AConstants.BDMRIVEW_REVIEW_STATUS, productRiewVO.getReviewStatus());
		lstInvoiceJson.put(AConstants.BDMRIVEW_REVIEW_HEB_RETAIL, productRiewVO.getHebRetail());// HEB RETAIl
		lstInvoiceJson.put(AConstants.BDMRIVEW_STATUS, productRiewVO.getStatus());
		lstInvoiceJson.put(AConstants.BDMRIVEW_FIRST_RECEIVED_TS, productRiewVO.getFirstReceivedTS());
		lstInvoiceJson.put(AConstants.BDMRIVEW_LAST_EDITED_TS, productRiewVO.getLastEditedTS());
		lstInvoiceJson.put(AConstants.BDMRIVEW_FAILEDREASONCODE, productRiewVO.getFailedReasonCode());
		return lstInvoiceJson;
	}

	/**
	 * Validate model to search.
	 * @param productSearchCriteriaVO
	 *            :The RetailInquirySearchModel contain searching information.
	 * @return Map<String, Object>
	 * @author thangdang
	 */
	private Map<String, String> validateSearchCriteria(ProductSearchCriteriaVO productSearchCriteriaVO) {
		Map<String, String> mapErrors = new HashMap<String, String>();
		this.validateTypeDouble(mapErrors, "Margin begin", productSearchCriteriaVO.getMarginBegin());
		this.validateTypeDouble(mapErrors, "Margin end", productSearchCriteriaVO.getMarginEnd());
		this.validateTypeDate(mapErrors, "Receive Date Begin", productSearchCriteriaVO.getDateBegin());
		this.validateTypeDate(mapErrors, "Receive Date End", productSearchCriteriaVO.getDateEnd());
		this.validateDateRange(mapErrors, "Receive Date Range", productSearchCriteriaVO.getDateBegin(), productSearchCriteriaVO.getDateEnd());
		this.validateTypeDouble(mapErrors, "Map Filter", productSearchCriteriaVO.getMapFilter());
		this.validateTypeDouble(mapErrors, "Pre Price Filter", productSearchCriteriaVO.getPrePriceFilter());
		this.validateTypeDouble(mapErrors, "Msrp Filter", productSearchCriteriaVO.getMsrpFilter());
		this.validateTypeDouble(mapErrors, "SRP Filter", productSearchCriteriaVO.getSrpFilter());
		this.validateTypeDouble(mapErrors, "Cost Filter", productSearchCriteriaVO.getCostFilter());
		this.validateTypeLong(mapErrors, "UPC Filter", productSearchCriteriaVO.getUpcIdFilter());
		this.validateTypeLong(mapErrors, "Size Filter", productSearchCriteriaVO.getProdSizeFilter());
		LOG.info("error =================== " + mapErrors);
		return mapErrors;
	}

	/**
	 * Validate InputString with Long type.
	 * @param inputName
	 *            String
	 * @param inputValue
	 *            String
	 * @return Map<String, String> map contains errors.
	 * @author thangdang
	 */
	private Map<String, String> validateTypeDouble(Map<String, String> mapErrors, String inputName, String inputValue) {
		if (!Helper.isEmpty(inputValue) && !Helper.isDouble(inputValue)) {
			mapErrors.put(inputName, this.messageSource.getMessage("error.input.invalid", null, Locale.ENGLISH));
		}
		return mapErrors;
	}

	/**
	 * Validate InputString with Long type.
	 * @param inputName
	 *            String
	 * @param inputValue
	 *            String
	 * @return Map<String, String> map contains errors.
	 * @author thangdang
	 */
	private Map<String, String> validateTypeLong(Map<String, String> mapErrors, String inputName, String inputValue) {
		if (!Helper.isEmpty(inputValue) && !Helper.isLong(inputValue)) {
			mapErrors.put(inputName, this.messageSource.getMessage("error.input.invalid", null, Locale.ENGLISH));
		}
		return mapErrors;
	}

	/**
	 * Validate InputString with Date type.
	 * @param inputName
	 *            String
	 * @param inputValue
	 *            String
	 * @return Map<String, String> map contains errors.
	 * @author thangdang
	 */
	private Map<String, String> validateTypeDate(Map<String, String> mapErrors, String inputName, String inputValue) {
		if (!Helper.isEmpty(inputValue) && !Helper.isValidDate(inputValue)) {
			mapErrors.put(inputName, this.messageSource.getMessage("error.input.invalid", null, Locale.ENGLISH));
		}
		return mapErrors;
	}

	/**
	 * Validate Date Range with Date type.
	 * @param inputName
	 *            String
	 * @param inputValue
	 *            String
	 * @return Map<String, String> map contains errors.
	 * @author ANNGUYEN
	 */
	private Map<String, String> validateDateRange(Map<String, String> mapErrors, String inputName, String date1, String date2) {
		if ((Helper.isEmpty(date1) && !Helper.isEmpty(date2)) || (Helper.isEmpty(date2) && !Helper.isEmpty(date1))) {
			mapErrors.put(inputName, this.messageSource.getMessage("error.input.invalid", null, Locale.ENGLISH));
		}
		return mapErrors;
	}

	/**
	 * Ajax Get Data Commondity.
	 * @param termSearch
	 *            String
	 * @return String
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_COMMODITY_AUTOCOMPLETE_PATH }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String ajaxGetDataCommodity() throws DSVException {
		LOG.info("Start get autocomplete  for commodity.");
		JSONObject jsonResult = new JSONObject();
		List<CommodityVO> lstCommodities = this.cacheService.getCommodityList();
		List<JSONObject> lstCommoditiesJson = new ArrayList<JSONObject>();
		if (Helper.isNormalList(lstCommodities)) {
			for (CommodityVO com : lstCommodities) {
				JSONObject commodityJson = new JSONObject();
				commodityJson.put(Constants.AUTO_VALUE, com.getCommodityCd());
				commodityJson.put(Constants.AUTO_NAME, com.getCommodityDesc());
				commodityJson.put(Constants.AUTO_LABEL, com.getCommodityCd() + Constants.DASH + com.getCommodityDesc());
				lstCommoditiesJson.add(commodityJson);
			}
		}
		jsonResult.put(Constants.AA_DATA, lstCommoditiesJson);
		LOG.info("End get autocomplete  for commodity.");
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Ajax Get Data Vendor.
	 * @return String
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_VENDOR_AUTOCOMPLETE_PATH }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String ajaxGetDataVendor() throws DSVException {
		LOG.info("Start get autocomplete  for vendor.");
		JSONObject jsonResult = new JSONObject();
		List<VendorInfoVO> lstVendorInfoVO = this.commonService.getAllListVendors();
		List<JSONObject> lstVendorInfoVOJson = new ArrayList<JSONObject>();
		if (Helper.isNormalList(lstVendorInfoVO)) {
			for (VendorInfoVO vendor : lstVendorInfoVO) {
				JSONObject vendorJson = new JSONObject();
				vendorJson.put(Constants.AUTO_VALUE, vendor.getVendorId());
				vendorJson.put(Constants.AUTO_NAME, vendor.getVendorName());
				vendorJson.put(Constants.AUTO_LABEL, vendor.getVendorId() + Constants.DASH + vendor.getVendorName());
				lstVendorInfoVOJson.add(vendorJson);
			}
		}
		jsonResult.put(Constants.AA_DATA, lstVendorInfoVOJson);
		LOG.info("End get autocomplete  for vendor.");
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Ajax Get Data Brand.
	 * @param termSearch
	 *            String
	 * @return String
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_BRAND_AUTOCOMPLETE_PATH }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String ajaxGetDataBrand(@RequestParam(value = "termSearch", required = false) String termSearch)
			throws DSVException {
		LOG.info("Start get autocomplete  for brand. :Iterm Search" + termSearch);
		JSONObject jsonResult = new JSONObject();
		List<BaseVO> lstBrand = this.cacheService.searchbrand(termSearch);
		List<JSONObject> lstBrandJson = new ArrayList<JSONObject>();
		if (Helper.isNormalList(lstBrand)) {
			for (BaseVO brand : lstBrand) {
				JSONObject brandJson = new JSONObject();
				// brandJson.put(Constants.AUTO_VALUE, brand.getId());
				// brandJson.put(Constants.AUTO_NAME, brand.getName());
				brandJson.put(Constants.AUTO_LABEL, brand.getName());
				lstBrandJson.add(brandJson);
			}
		}
		jsonResult.put(Constants.AA_DATA, lstBrandJson);
		LOG.info("End get autocomplete  for brand.");
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Ajax Get Data Brand.
	 * @param termSearch
	 *            String
	 * @return String
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_PRODUCT_AUTOCOMPLETE_PATH }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String ajaxGetDataProduct(@RequestParam(value = "termSearch", required = false) String termSearch)
			throws DSVException {
		LOG.info("Start get autocomplete  for product. :Iterm Search" + termSearch);
		JSONObject jsonResult = new JSONObject();
		List<String> lstProduct = this.cacheService.searchProductDesc(termSearch);
		List<JSONObject> lstProductJson = new ArrayList<JSONObject>();
		if (Helper.isNormalList(lstProduct)) {
			for (String str : lstProduct) {
				JSONObject productJson = new JSONObject();
				productJson.put(Constants.AUTO_LABEL, str);
				lstProductJson.add(productJson);
			}
		}
		jsonResult.put(Constants.AA_DATA, lstProductJson);
		LOG.info("End get autocomplete  for product.");
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Ajax Get Data Brand.
	 * @param termSearch
	 *            String
	 * @return String
	 * @throws DSVException
	 * @author mrh.diemnguyen
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_DATA_FAILED_REASON_PATH }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String ajaxGetDataFailedReason() throws DSVException {
		LOG.info("Start get data for failed reason.");
		JSONObject jsonResult = new JSONObject();
		List<BaseVO> lstFailedReason = this.productForReviewService.getListFailedReasons();
		List<JSONObject> lstFailedReasonJson = new ArrayList<JSONObject>();
		if (Helper.isNormalList(lstFailedReason)) {
			for (BaseVO failedReasion : lstFailedReason) {
				JSONObject failedReasonJson = new JSONObject();
				failedReasonJson.put(Constants.AUTO_VALUE, failedReasion.getId());
				failedReasonJson.put(Constants.AUTO_NAME, failedReasion.getName());
				failedReasonJson.put(Constants.AUTO_LABEL, failedReasion.getId() + Constants.DASH + failedReasion.getName());
				lstFailedReasonJson.add(failedReasonJson);
			}
		}
		jsonResult.put(Constants.AA_DATA, lstFailedReasonJson);
		LOG.info("End get data for failed reason.");
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Export search result to excel file.
	 * @param request
	 *            The request of HttpServlet
	 * @param response
	 *            The response of HttpServlet
	 * @param prodSearchModel
	 *            ProductSearchModel
	 * @throws DSVException
	 *             If cannot get list order form service layer or get excel template
	 * @author Duyen.Le
	 */
	@RequestMapping(value = { EXPORT_PRODUCT_REVIEW_TO_EXCEL_DIRECTLY }, method = RequestMethod.POST)
	public void exportToExcel(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("prodSearchModel") ProductSearchCriteriaVO prodSearchModel) throws DSVException {
		LOG.info("Export excel !!!!!!!!!!");
		String columnNumber = prodSearchModel.getiSortColumn();
		prodSearchModel.setiSortColumn(this.getColumnSortTable(columnNumber));
		List<ProductRiewVO> lstPrdsToExport = this.productForReviewService.exportExcelForProducts(prodSearchModel);

		LOG.info("LIST Products To Export SIZE : " + lstPrdsToExport.size());
		if (!Helper.isEmpty(lstPrdsToExport) && Helper.isNormalList(lstPrdsToExport)) {
			// export
			HSSFWorkbook workbook = this.excelTemplate.getExcelProduct(lstPrdsToExport);
			AHelper.buildExcelFile(response, workbook, AHelper.getDownloadFileName(Constants.LST_PRODUCT_EXCEL_FILE_NAME));
		}
	}

	/**
	 * Gets the column sort table.
	 * @param columnNumber
	 *            the column number
	 * @return the column sort table
	 */
	private String getColumnSortTable(String columnNumber) {
		String result = Constants.EMPTY_STRING;
		if (AntiMagicNumber.STRING_TWO.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.PROD_SCN_CD;
		} else if (AntiMagicNumber.STRING_FOUR.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.SELL_UNT_SZ;
		} else if (AntiMagicNumber.STRING_FIVE.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.MAP_AMT;
		} else if (AntiMagicNumber.STRING_SIX.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.PRE_PRC_AMT;
		} else if (AntiMagicNumber.STRING_SEVEN.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.MSRP_AMT;
		} else if (AntiMagicNumber.STRING_EIGHT.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.SRP_AMT;
		} else if (AntiMagicNumber.STRING_NINE.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.HEB_CST_AMT;
		} else if (AntiMagicNumber.STRING_EIGHTTEEN.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.LST_UPDT_TS;
		} else if (AntiMagicNumber.STRING_TEN.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.PER_GROSS_PROFIT;
		} else if (AntiMagicNumber.STRING_ELEVEN.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.MARGIN;
		} else if (AntiMagicNumber.STRING_TWELVE.equals(columnNumber)) {
			result = ProductSearchCriteriaVO.HEB_RETL_AMT;
		} else {
			result = ProductSearchCriteriaVO.CRE8_TS;
		}
		return result;
	}
}
