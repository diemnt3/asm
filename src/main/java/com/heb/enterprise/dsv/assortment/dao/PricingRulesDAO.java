/*
 * $Id: PricingRulesDAO.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao;

import java.util.List;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;

/**
 * PricingRulesDAO.
 * @author quangmai
 */
public interface PricingRulesDAO {

	/**
	 * Gets the pricing rule defns by vendor.
	 * @param vendorId
	 *            String
	 * @return List<AssortmentRulesVO>
	 * @author thangdang
	 */
	List<AssortmentRulesVO> getPricingRuleByVendor(String vendorId);

	/**
	 * Gets the pricing rule defns by vendor.
	 * @param commodity
	 *            String
	 * @return List<AssortmentRulesVO>
	 * @author thangdang
	 */
	List<AssortmentRulesVO> getPricingRuleByCommodity(String commodity);

	/**
	 * getPricingRuleByCommoditySubCommodity.
	 * @param commodity
	 *            String
	 * @param subComodity
	 *            subComodity
	 * @return List<AssortmentRulesVO>
	 * @author thangdang
	 */
	List<AssortmentRulesVO> getPricingRuleByCommoditySubCommodity(String commodity, String subComodity);

	/**
	 * getPricingCompetitorByVendor.
	 * @param vendorId
	 *            String
	 * @return List<AssortmentRulesVO>
	 * @author thangdang
	 */
	List<String> getPricingCompetitorByVendor(String vendorId);

	/**
	 * getPricingCompetitorByCommodity.
	 * @param commodity
	 *            String
	 * @return List<AssortmentRulesVO>
	 * @author thangdang
	 */
	List<String> getPricingCompetitorByCommodity(String commodity);

	/**
	 * getPricingPricingCompetitorByCommoditySubCommodity.
	 * @param commodity
	 *            String
	 * @param subComodity
	 *            subComodity
	 * @return List<AssortmentRulesVO>
	 * @author thangdang
	 */
	List<String> getPricingCompetitorByCommoditySubCommodity(String commodity, String subComodity);

	/**
	 * removeComparetorInfo.
	 * @param lvlId
	 *            long
	 * @return String
	 * @author thangdang
	 * @throws DSVException
	 *             the DSV exception
	 */
	String removeComparetorInfo(long lvlId) throws DSVException;

	/**
	 * getCompareInfo.
	 * @param lvlId
	 *            long
	 * @param lstComparetorIds
	 *            List<String>
	 * @param userId
	 *            String
	 * @return List<long>
	 * @author thangdang
	 * @throws DSVException
	 *             the DSV exception
	 */
	String insertComparetorInfo(long lvlId, List<String> lstComparetorIds, String userId) throws DSVException;

	/**
	 * getLvlByVendor.
	 * @param vendorId
	 *            String
	 * @return long
	 * @author thangdang
	 */
	long getLvlByVendor(String vendorId);

	/**
	 * getLvlByVendor.
	 * @param commodity
	 *            String
	 * @return long
	 * @author thangdang
	 */
	long getLvlByCommodity(String commodity);

	/**
	 * getLvlByCommoditySubCommodity.
	 * @param commodity
	 *            String
	 * @param subComodity
	 *            String
	 * @return long
	 * @author thangdang
	 */
	long getLvlByCommoditySubCommodity(String commodity, String subComodity);

	/**
	 * getActTyp.
	 * @return List<BaseVO>
	 * @author thangdang
	 */
	List<BaseVO> getActTyp();

}
