/*
 * $Id: BDMServiceWS.java,v 1.5 2013/10/29 13:58:01 vn44178 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.webservice;

import java.util.Map;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.BDMDetailVO;

/**
 * BDMServiceWS.
 * @author anhtran
 */
public interface BDMServiceWS {
	/**
	 * getBDMDetailList.
	 * @return Map<String, BDMDetailVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	Map<String, BDMDetailVO> getBDMDetailList() throws DSVException;

	/**
	 * getBDMDetails.
	 * @param bdmCd
	 *            :String
	 * @return BDMDetailVO
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	BDMDetailVO getBDMDetails(String bdmCd) throws DSVException;
}
