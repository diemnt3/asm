/*
 * $Id.: CostReviewMapper.java, Oct 28, 2014$
 *
 * Copyright (c) 2015 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.tree.RowMapper;

import org.apache.taglibs.standard.lang.jstl.Constants;

/**
 * CostReviewMapper class.
 * @author diemnguyen
 */
public final class CostReviewMapper {

	private static final Logger LOG = Logger.getLogger(CostReviewMapper.class);

	/**
	 * constructor method.
	 */
	private CostReviewMapper() {

	}

	/**
	 * getProductWithCostChangesMapper.
	 * @return RowMapper<RetailCostVO>
	 * @author diemnguyen.
	 */
	public static RowMapper<RetailCostVO> getProductWithCostChangesMapper() {
		RowMapper<RetailCostVO> entyMapper = new RowMapper<RetailCostVO>() {
			@Override
			public RetailCostVO mapRow(ResultSet rs, int arg1) throws SQLException {
				RetailCostVO entyVO = new RetailCostVO();
				StringBuilder nextPackSizeBuilder = new StringBuilder();
				nextPackSizeBuilder.append(rs.getString("NEW_PK_QTY"));
				nextPackSizeBuilder.append(Constants.SLASH_WHITE_SPACE);
				nextPackSizeBuilder.append(rs.getString("SELL_UNT_SZ"));
				nextPackSizeBuilder.append(Constants.WHITE_SPACE);
				nextPackSizeBuilder.append(rs.getString("UOM_TXT"));

				entyVO.setVendorId(rs.getLong("VEND_ID"));
				entyVO.setProdScnCd(rs.getLong("PROD_SCN_CD"));
				entyVO.setProdDesc(rs.getString("PROD_DES"));

				entyVO.setPackSize(rs.getString("PACK_SIZE"));

				entyVO.setCurrentCost(rs.getBigDecimal("HEB_CST_AMT"));
				entyVO.setCurrentRetail(rs.getBigDecimal("HEB_RETL_AMT"));
				entyVO.setCurrentMap(rs.getString("MAP_SW"));
				entyVO.setCurrentGP(rs.getBigDecimal("GP"));
				entyVO.setNextCost(rs.getBigDecimal("NEXT_SCT"));
				entyVO.setNextRetail(rs.getBigDecimal("NEXT_RETL"));
				entyVO.setNextMap(rs.getString("MAP_SW"));
				entyVO.setNextGP(rs.getBigDecimal("NEXT_GP"));
				entyVO.setNextEffectiveDate(rs.getTimestamp("EFF_TS"));
				entyVO.setStatus(rs.getString("STATUS"));
				entyVO.setNewPkQty(rs.getString("NEW_PK_QTY"));
				entyVO.setNextPackSize(nextPackSizeBuilder.toString());
				entyVO.setErrTypDes(rs.getString("ERR_TYP_DES"));
				return entyVO;
			}
		};
		return entyMapper;
	}

	/**
	 * productWithCostChangesSetter.
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return PreparedStatementSetter.
	 * @author diemnguyen.
	 */
	public static PreparedStatementSetter productWithCostChangesSetter(final RetailCostSearchVO retailCostSearchVO) {
		PreparedStatementSetter preparedStatementSetter = new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(AntiMagicNumber.ONE, 0);
				ps.setInt(AntiMagicNumber.TWO, 10);
			}
		};
		return preparedStatementSetter;
	}

	/**
	 * getProductWithCostChangesMapper.
	 * @return RowMapper<RetailCostVO>
	 * @author diemnguyen.
	 */
	public static RowMapper<Integer> getTotalPassedFailedMapper() {
		RowMapper<Integer> entyMapper = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getInt("TOTAL");
			}
		};
		return entyMapper;
	}

	/**
	 * getBoundaryRulesMapper.
	 * @return RowMapper<BaseVO>
	 * @author diemnguyen.
	 */
	public static RowMapper<BaseVO> getBoundaryRulesMapper() {
		RowMapper<BaseVO> entyMapper = new RowMapper<BaseVO>() {
			@Override
			public BaseVO mapRow(ResultSet rs, int arg1) throws SQLException {
				BaseVO baseVO = new BaseVO();
				baseVO.setId(rs.getString("ERR_TYP_CD"));
				baseVO.setName(rs.getString("ERR_TYP_DES"));
				return baseVO;
			}
		};
		return entyMapper;
	}

}
