/*
 * $Id: CacheServiceImpl.java,v 1.0 2014/03/13 11:08:03 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.dao.ProductsDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.webservice.ProductHierarchyWS;
import com.heb.enterprise.dsv.common.service.DMDService;
import com.heb.enterprise.dsv.common.webservice.VendorWsClient;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.ClassVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.ProductRiewVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;
import com.heb.enterprise.dsv.vo.SubDepartmentVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * CacheService interface.
 * @author Duyen.Le
 */
@Service
public class CacheServiceImpl implements CacheService {
	private static final Logger LOG = Logger.getLogger(CacheServiceImpl.class);
	@Autowired
	@Qualifier("vendorWsClient")
	private VendorWsClient vendorWsClient;

	@Autowired
	private ProductHierarchyWS productHierachyWsService;
	@Autowired
	private DMDService dmdService;
	/**
	 * inject ProductsDAO.
	 */
	@Autowired
	private ProductsDAO productsDAO;

	/**
	 * Constructor method.
	 */
	public CacheServiceImpl() {
	}

	/**
	 * cache all data into memory.
	 * @author Duyen.Le
	 * @throws DSVException
	 *             :DSVException
	 */
	@Override
	public void refreshCaches() throws DSVException {
		this.refreshVendorCache();
		this.refreshHierachyForGlobalFromCache();
		this.getCommodityDetailMap();
		this.refreshBrandCache();
		this.refreshProdDescCache();
	}

	/**
	 * getListSubDepartmentFromCache - get List Sub-Department From cache.
	 * @author Duyen.Le
	 * @return List<SubDepartmentVO>
	 * @throws DSVException
	 *             :DSVException
	 */
	@Override
	public List<DepartmentVO> getHierachyForGlobalRulesFromCache() throws DSVException {
		return this.productHierachyWsService.getMerchandisingHierarchyDetails();
	}

	/**
	 * remove all HIERACHY_INFO from cache.
	 * @author duyen.le
	 */
	@CacheEvict(value = "HIERACHY_INFO", allEntries = true)
	private void removeHierachyInfoCache() {

	}

	/**
	 * refreshHierachyForGlobalFromCache : cache All info of Hierachy from Hierachy WS into memory.
	 * @author Duyen.Le
	 * @throws DSVException
	 *             :DSVException
	 */
	@Override
	public void refreshHierachyForGlobalFromCache() throws DSVException {
		this.removeHierachyInfoCache();
		this.getHierachyForGlobalRulesFromCache();
	}

	/**
	 * Get list Commodity.
	 * @author anhtran.
	 * @throws DSVException
	 *             :DSVException
	 */
	@Override
	public List<CommodityVO> getCommodityList() throws DSVException {
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		for (DepartmentVO departmentVO : this.getHierachyForGlobalRulesFromCache()) {
			for (SubDepartmentVO subDepartmentVO : departmentVO.getSubDepartmentVOs()) {
				commodityVOs.addAll(this.getListCommodityFromSubDepartmentVO(subDepartmentVO));
			}
		}
		return commodityVOs;
	}

	/**
	 * Get Commodity Map.
	 * @author anhtran.
	 * @throws DSVException
	 *             :DSVException
	 */
	@Override
	public Map<Integer, CommodityVO> getCommodityMap() throws DSVException {
		Map<Integer, CommodityVO> commodityVOMap = new HashMap<Integer, CommodityVO>();
		for (CommodityVO commodityVO : this.getCommodityList()) {
			commodityVOMap.put(commodityVO.getCommodityCd(), commodityVO);
		}
		return commodityVOMap;
	}

	/**
	 * getListCommodityFromSubDepartmentVO.
	 * @param subDepartmentVO
	 *            :SubDepartmentVO
	 * @return List<CommodityVO>
	 * @author anhtran.
	 */
	private List<CommodityVO> getListCommodityFromSubDepartmentVO(SubDepartmentVO subDepartmentVO) {
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		for (ClassVO classVO : subDepartmentVO.getClassVOs()) {
			commodityVOs.addAll(classVO.getCommodityVOs());
		}
		return commodityVOs;
	}

	/**
	 * refresh vendor from cache.
	 * @author duyen.le
	 */
	@Override
	public void refreshVendorCache() {
		this.removeVendorInfoCache();
		this.getAllListVendors();
	}

	/**
	 * Get all list vendors.
	 * @return List<VendorInfoVO>
	 * @author anhtran.
	 */
	@Override
	public List<VendorInfoVO> getAllListVendors() {
		List<VendorInfoVO> lstVendors = this.vendorWsClient.getDSVendors();
		if (Helper.isEmpty(lstVendors)) {
			lstVendors = this.vendorWsClient.getFreightCarrierVendors();
		} else {
			lstVendors.addAll(this.vendorWsClient.getFreightCarrierVendors());
		}
		return lstVendors;
	}

	/**
	 * remove all vendor data from cache.
	 * @author duyen.le
	 */
	@CacheEvict(value = "VENDOR_LIST_INFO", allEntries = true)
	private void removeVendorInfoCache() {

	}

	/**
	 * getProductsFromCacheByProduct - get List of Brands From cache by Product's name.
	 * @param productName
	 *            String
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	@Override
	public List<BaseVO> getCommoditiesFromCacheByCommodity(String productName) {
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		return baseVOs;
	}

	/**
	 * refreshProductsFromCache : cache All info of Products from Products WS into memory.
	 * @author Duyen.Le
	 */
	@Override
	public void refreshCommoditiesFromCache() {
		// TODO Auto-generated method stub
	}

	/**
	 * getBrandsFromCache - get List of Brands From cache.
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	@Override
	public List<BaseVO> getBrandsFromCache() {
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		return baseVOs;
	}

	/**
	 * refreshBrandsFromCache : cache All info of Brand from Vendor WS into memory.
	 * @author Duyen.Le
	 */
	@Override
	public void refreshBrandsFromCache() {
		// TODO Auto-generated method stub
	}

	/**
	 * getBrandsFromCacheByBrand - get List of Brands From cache by brand's name.
	 * @param brandName
	 *            String
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 */
	@Override
	public List<BaseVO> getBrandsFromCacheByBrand(String brandName) {
		List<BaseVO> baseVOs = new ArrayList<BaseVO>();
		return baseVOs;
	}

	/**
	 * Get list sub-commodities by CommodityId.
	 * @param commodityId
	 *            String
	 * @author Duyen.Le
	 * @return List<SubCommodityVO>
	 * @throws DSVException
	 *             if have any errors when getting data from Cache
	 */
	@Override
	public List<SubCommodityVO> getSubCommByCommodityId(String commodityId) throws DSVException {
		List<SubCommodityVO> lstSubCommodity = null;
		List<DepartmentVO> lstDept = this.getHierachyForGlobalRulesFromCache();
		for (DepartmentVO departmentVO : lstDept) {
			List<SubDepartmentVO> subDept = departmentVO.getSubDepartmentVOs();
			for (SubDepartmentVO subDepartmentVO : subDept) {
				List<ClassVO> lstClss = subDepartmentVO.getClassVOs();
				for (ClassVO classVO : lstClss) {
					List<CommodityVO> lstCom = classVO.getCommodityVOs();
					for (CommodityVO commodityVO : lstCom) {
						if (commodityVO.getCommodityCd() == Integer.parseInt(commodityId)) {
							lstSubCommodity = commodityVO.getSubCommodityVOs();
						}
					}
				}
			}
		}
		return lstSubCommodity;
	}

	/**
	 * getSubCommodityList.
	 * @return Map<String, SubCommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	public Map<String, SubCommodityVO> getSubCommodityList() throws DSVException {
		Map<String, SubCommodityVO> subCommodityVOs = new HashMap<String, SubCommodityVO>();
		for (CommodityVO commodityVO : this.getCommodityList()) {
			subCommodityVOs.putAll(this.getSubCommodityMapFromCommodityVO(commodityVO));
		}
		return subCommodityVOs;
	}

	/**
	 * getSubCommodityMapFromCommodityVO.
	 * @param commodityVO
	 *            :CommodityVO
	 * @return Map<String, SubCommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	private Map<String, SubCommodityVO> getSubCommodityMapFromCommodityVO(CommodityVO commodityVO) throws DSVException {
		Map<String, SubCommodityVO> subCommodityVOs = new HashMap<String, SubCommodityVO>();
		for (SubCommodityVO subCommodityVO : commodityVO.getSubCommodityVOs()) {
			subCommodityVOs.put(String.valueOf(subCommodityVO.getSubCommodityCd()), subCommodityVO);
		}
		return subCommodityVOs;
	}

	/**
	 * getSubCommodityMap.
	 * @return Map<String, SubCommodityVO>
	 * @throws DSVException
	 *             :DSVException
	 * @author anhtran.
	 */
	public Map<Integer, CommodityVO> getCommodityDetailMap() throws DSVException {
		return this.productHierachyWsService.getCommodityDetailList();
	}

	/**
	 * getCommodityDetailList.
	 * @throws DSVException
	 *             :DSVException
	 * @return List<CommodityVO>
	 * @author anhtran.
	 */
	public List<CommodityVO> getCommodityDetailList() throws DSVException {
		List<CommodityVO> commodityVOs = new ArrayList<CommodityVO>();
		Map<Integer, CommodityVO> commodityVOMap = this.getCommodityMap();
		for (Entry<Integer, CommodityVO> entry : this.productHierachyWsService.getCommodityDetailList().entrySet()) {
			if (!Helper.isEmpty(commodityVOMap.get(entry.getKey()))) {
				entry.getValue().setSubCommodityVOs(commodityVOMap.get(entry.getKey()).getSubCommodityVOs());
				commodityVOs.add(entry.getValue());
			}
		}
		return commodityVOs;
	}

	@Override
	public List<CommodityVO> searchCommodityList(String query) {
		List<CommodityVO> lstCommodityReturn = new ArrayList<CommodityVO>();
		try {
			List<CommodityVO> lstCommodityParents = this.getCommodityList();
			if (!Helper.isEmpty(query)) {
				String commodityId;
				String commodityDesc;
				for (CommodityVO commodityVOTmp : lstCommodityParents) {
					commodityId = String.valueOf(commodityVOTmp.getCommodityCd());
					commodityDesc = Constants.EMPTY_STRING;
					if (!Helper.isEmpty(commodityVOTmp.getCommodityDesc())) {
						commodityDesc = commodityVOTmp.getCommodityDesc().toUpperCase();
					}
					if (commodityId.contains(query) || commodityDesc.contains(query.toUpperCase())) {
						lstCommodityReturn.add(commodityVOTmp);
					}
				}
			} else {
				lstCommodityReturn = lstCommodityParents;
			}
		} catch (DSVException e) {
			LOG.info(e.getMessage(), e);
		}
		return lstCommodityReturn;
	}

	@Override
	public List<BaseVO> searchbrand(String query) {
		List<BaseVO> lstBrandReturn = new ArrayList<BaseVO>();
		try {
			List<BaseVO> lstBrands = this.dmdService.getBrandLists();
			String brandName;
			if (!Helper.isEmpty(query)) {
				for (BaseVO baseVO : lstBrands) {
					brandName = Constants.EMPTY_STRING;
					if (!Helper.isEmpty(baseVO.getName())) {
						brandName = baseVO.getName().toUpperCase();
					}
					if (baseVO.getId().contains(query) || brandName.contains(query.toUpperCase())) {
						lstBrandReturn.add(baseVO);
					}
				}
			} else {
				lstBrandReturn = lstBrands;
			}
		} catch (DSVException e) {
			LOG.info(e.getMessage(), e);
		}
		return lstBrandReturn;
	}

	@Override
	public void refreshBrandCache() {
		this.removeBrandCache();
		this.getAllBrandFromCache();

	}

	/**
	 * remove all vendor data from cache.
	 * @author duyen.le
	 */
	@CacheEvict(value = "BRAND_LIST", allEntries = true)
	private void removeBrandCache() {

	}

	@Override
	public List<BaseVO> getAllBrandFromCache() {
		List<BaseVO> lstBrand = null;
		try {
			lstBrand = this.dmdService.getBrandLists();
		} catch (DSVException e) {
			LOG.info(e.getMessage(), e);
		}
		return lstBrand;
	}

	@Override
	public List<String> searchProductDesc(String query) {
		List<String> lstProductDes = new ArrayList<String>();
		List<ProductRiewVO> lstProductRiewVOs = this.productsDAO.getAllProductInfo();
		for (ProductRiewVO productRiewVO : lstProductRiewVOs) {
			if (!Helper.isEmpty(productRiewVO.getDescription()) && productRiewVO.getDescription().toUpperCase().contains(query.toUpperCase())) {
				lstProductDes.add(productRiewVO.getDescription());
			}

		}
		return lstProductDes;
	}

	@Override
	public void refreshProdDescCache() {
		this.getAllProductDesc();
		this.removeProductDescCache();
	}

	@Override
	public List<ProductRiewVO> getAllProductDesc() {
		return this.productsDAO.getAllProductInfo();
	}

	/**
	 * remove all vendor data from cache.
	 * @author duyen.le
	 */
	@CacheEvict(value = "PRODUCT_DESC", allEntries = true)
	private void removeProductDescCache() {

	}
}
