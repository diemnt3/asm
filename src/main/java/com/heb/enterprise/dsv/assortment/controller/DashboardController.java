/*
 * $Id.: $DashboardController.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Controller for the Dashboard screen.
 * @author anhtran
 */
@Controller
@Scope("session")
public class DashboardController {

	private static final Logger LOG = Logger.getLogger(DashboardController.class);
	// this name show in address bar
	// private static final String GLOBAL_PATH = Constants.SLASH + ConstantsWeb.GLOBAL_PAGE;
	// TILE
	private static final String GLOBAL_TILE = "global.tile";

	/**
	 * Display The Dashboard screen.
	 * @param locale
	 *            locale
	 * @param session
	 *            HttpSession
	 * @return The tile for the dashboard page
	 * @author anhtran
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */

	// @RequestMapping(value = { GLOBAL_PATH }, method = RequestMethod.GET)
	// public String showDashboardScreen(Locale locale, HttpSession session) throws DSVException {
	// return GLOBAL_TILE;
	// }

}
