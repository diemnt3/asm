/*
 * $Id: AssortmentRulesServiceImpl.java,v 1.0 2014/03/13 11:08:03 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.heb.enterprise.dsv.assortment.service.AssortmentRulesService;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.ProductVO;

/**
 * AssortmentRulesServiceImpl.
 * @author anhtran.
 */
@Service
public class AssortmentRulesServiceImpl implements AssortmentRulesService {

	/**
	 * getRuleProduct.
	 * @param product
	 *            :ProductVO
	 * @return Map<String, AssortmentRulesVO>.
	 * @author anhtran.
	 */
	public Map<String, AssortmentRulesVO> getRuleProduct(ProductVO product) {
		Map<String, AssortmentRulesVO> map = new HashMap<String, AssortmentRulesVO>();
		return map;
	}

}
