/*
 * $Id.: $RetailCostController.java
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.taglibs.standard.lang.jstl.Constants;

import com.heb.enterprise.dsv.assortment.service.CostReviewService;
import com.heb.enterprise.dsv.assortment.service.impl.ExportExcelTemplate;
import com.heb.enterprise.dsv.assortment.utils.AHelper;
import com.heb.enterprise.dsv.assortment.utils.ConstantsWeb;

/**
 * RetailCostController.
 * @author diemnguyen
 */
@Controller
public class CostReviewController {

	private static final Logger LOG = Logger.getLogger(CostReviewController.class);

	private static final String RETAIL_PATH = Constants.SLASH + ConstantsWeb.RETAIL_PAGE;
	private static final String RETAIL_AJAX_SEARCH = Constants.SLASH + ConstantsWeb.SEARCH_RETAIL;
	private static final String EXPORT_COST_REVIEW_TO_EXCEL_DIRECTLY = ConstantsWeb.EXPORT_COST_REVIEW_TO_EXCEL_DIRECTLY;
	// TILE
	private static final String RETAIL_TILE = "retail.tile";

	// @Value("${coms.dsv.asm.costReview.getProductWithCostChanges}")
	// private String getProductWithCostChangesSql;

	@Autowired
	private CostReviewService retailCostService;

	@Autowired
	private ExportExcelTemplate excelTemplate;

	/**
	 * showPricingRulesScreen.
	 * @param modelMap
	 *            :ModelMap
	 * @return String - page name
	 * @author diemnguyen
	 * @throws DSVException
	 *             :If could not get values from Service Layer.
	 */
	@RequestMapping(value = { RETAIL_PATH }, method = RequestMethod.GET)
	public String showPricingRulesScreen() throws DSVException {
		LOG.info("showprincingRulesScreen");
		return RETAIL_TILE;
	}

	/**
	 * getBoundaryRules.
	 * @throws DSVException
	 *             :DSVException.
	 * @return List<BaseVO>
	 * @author diemnguyen.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/get-boundary-rule" }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String getBoundaryRules() throws DSVException {
		LOG.info("Start get data for failed reason.");
		JSONObject jsonResult = new JSONObject();
		List<BaseVO> boundaryRules = this.retailCostService.getBoundaryRules();
		List<JSONObject> boundaryRulesJson = new ArrayList<JSONObject>();
		if (Helper.isNormalList(boundaryRules)) {
			for (BaseVO baseVO : boundaryRules) {
				JSONObject object = new JSONObject();
				object.put(Constants.AUTO_VALUE, baseVO.getId());
				object.put(Constants.AUTO_NAME, baseVO.getId());
				object.put(Constants.AUTO_LABEL, baseVO.getId() + Constants.DASH_WHITE_SPACE + baseVO.getName());
				boundaryRulesJson.add(object);
			}
		}
		jsonResult.put(Constants.AA_DATA, boundaryRulesJson);
		LOG.info("End get data for failed reason.");
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Call ajax to search Retail and Cost.
	 * @param retailCostModel
	 *            :RetailCostSearchVO
	 * @param request
	 *            HttpServletRequest
	 * @param session
	 *            HttpSession
	 * @param model
	 *            :ModelMap
	 * @return String - JsonString
	 * @author diemnguyen
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { RETAIL_AJAX_SEARCH }, method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String getProductWithCostChanges(@ModelAttribute("retailCostModel") RetailCostSearchVO retailCostModel, @RequestParam("tabActive") String tabActive, HttpServletRequest request,
			HttpSession session, ModelMap model) throws DSVException {
		int displayRecords = AntiMagicNumber.ZERO;
		LOG.info("getProductWithCostChanges");
		// ObjectMapper objectMapper = new ObjectMapper();
		// objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		// List<DataTablesOrderVO> ordersDT = null;
		// try {
		// ordersDT = objectMapper.readValue(retailCostModel.getOrderJsonStr(), objectMapper.getTypeFactory().constructCollectionType(List.class, DataTablesOrderVO.class));
		// } catch (JsonParseException e) {
		// LOG.info(e.getMessage(), e);
		// } catch (JsonMappingException e) {
		// LOG.info(e.getMessage(), e);
		// } catch (IOException e) {
		// LOG.info(e.getMessage(), e);
		// }
		//
		// int start = Integer.parseInt(retailCostModel.getStart());
		// retailCostModel.setBegin(String.valueOf((start == 0) ? start : (start + 1)));
		// int length = Integer.parseInt(retailCostModel.getLength());
		// retailCostModel.setEnd(String.valueOf(start + length));
		//
		// retailCostModel.setsSortDir(ordersDT.get(AntiMagicNumber.ZERO).getDir());
		// retailCostModel.setiSortColumn(this.getMappingTableColumnName().get(ordersDT.get(AntiMagicNumber.ZERO).getColumn().toString()));

		List<RetailCostVO> lstResult = this.retailCostService.getProductWithCostChanges(this.setDataForRetailCostModel(retailCostModel, "get"), tabActive);
		Map<String, Integer> mapTotal = this.retailCostService.getTotalPassedFailed(retailCostModel, tabActive);
		JSONObject jsonResult = new JSONObject();
		if (Constants.ALL_LOWERCASE_STR.equals(tabActive)) {
			displayRecords = mapTotal.get(Constants.ALL_LOWERCASE_STR);
		} else if (Constants.PASSED.equals(tabActive)) {
			displayRecords = mapTotal.get(Constants.PASSED);
		} else {
			displayRecords = mapTotal.get(Constants.FAILED);
		}

		jsonResult.put(Constants.PASSED, mapTotal.get(Constants.PASSED));
		jsonResult.put(Constants.FAILED, mapTotal.get(Constants.FAILED));
		jsonResult.put(Constants.ALL_LOWERCASE_STR, mapTotal.get(Constants.ALL_LOWERCASE_STR));
		jsonResult.put(Constants.ITOTAL_DISPLAY_RECORDS, displayRecords);
		jsonResult.put(Constants.ITOTAL_RECORDS, displayRecords);
		jsonResult.put(Constants.AA_DATA, this.getObjectListFromRetailCostVOList(lstResult, tabActive));
		jsonResult.put(Constants.S_ECHO, retailCostModel.getDraw());
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * setDataForRetailCostModel.
	 * @param retailCostModel
	 * @return RetailCostSearchVO
	 * @author diemnguyen.
	 */
	private RetailCostSearchVO setDataForRetailCostModel(RetailCostSearchVO retailCostModel, String action) {
		if ("exportExcel".equals(action)) {
			retailCostModel.setiSortColumn(this.getMappingTableColumnName().get(retailCostModel.getiSortColumn()));
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			List<DataTablesOrderVO> ordersDT = null;
			try {
				ordersDT = objectMapper.readValue(retailCostModel.getOrderJsonStr(), objectMapper.getTypeFactory().constructCollectionType(List.class, DataTablesOrderVO.class));
			} catch (JsonParseException e) {
				LOG.info(e.getMessage(), e);
			} catch (JsonMappingException e) {
				LOG.info(e.getMessage(), e);
			} catch (IOException e) {
				LOG.info(e.getMessage(), e);
			}

			int start = Integer.parseInt(retailCostModel.getStart());
			retailCostModel.setBegin(String.valueOf((start == 0) ? start : (start + 1)));
			int length = Integer.parseInt(retailCostModel.getLength());
			retailCostModel.setEnd(String.valueOf(start + length));
			retailCostModel.setsSortDir(ordersDT.get(AntiMagicNumber.ZERO).getDir());
			retailCostModel.setiSortColumn(this.getMappingTableColumnName().get(ordersDT.get(AntiMagicNumber.ZERO).getColumn().toString()));
		}

		return retailCostModel;
	}

	/**
	 * getObjectListFromRetailCostVOList.
	 * @param lstResult
	 *            :List<RetailCostVO>
	 * @return List<Object>
	 * @author diemnguyen.
	 */
	private List<Object> getObjectListFromRetailCostVOList(List<RetailCostVO> lstResult, String tabActive) {
		List<Object> lstData = new ArrayList<Object>();
		if (Helper.isNormalList(lstResult)) {
			for (int i = 0; i < lstResult.size(); i++) {
				RetailCostVO retail = lstResult.get(i);
				lstData.add(this.getRetailMapFromRetailCostVO(retail, tabActive));
			}
		}
		return lstData;
	}

	/**
	 * getRetailMapFromRetailCostVO.
	 * @param retail
	 *            :RetailCostVO
	 * @return Map<String, Object>
	 * @author diemnguyen.
	 */
	private Map<String, Object> getRetailMapFromRetailCostVO(RetailCostVO retail, String tabActive) {
		Map<String, Object> retailMap = new HashMap<String, Object>();
		StringBuilder rowIdBuilder = new StringBuilder();
		rowIdBuilder.append(retail.getVendorId());
		rowIdBuilder.append(Constants.DASH);
		rowIdBuilder.append(retail.getProdScnCd());
		rowIdBuilder.append(Constants.DASH);
		rowIdBuilder.append(new SimpleDateFormat(Constants.DATE_FORMAT_MMDDYYYY).format(retail.getNextEffectiveDate()));
		rowIdBuilder.append(Constants.DASH);
		rowIdBuilder.append(tabActive);
		retailMap.put("DT_RowId", rowIdBuilder.toString());
		retailMap.put("no", Constants.EMPTY_STRING);
		retailMap.put("checkBox", Constants.EMPTY_STRING);
		retailMap.put("upc", retail.getProdScnCd());
		retailMap.put("desc", retail.getProdDesc());
		retailMap.put("packSize", retail.getPackSize());
		retailMap.put("currentCost", retail.getCurrentCost());
		retailMap.put("currentRetail", retail.getCurrentRetail());
		retailMap.put("currentMap", retail.getCurrentMap());
		retailMap.put("currentComp", retail.getCurrentComp());
		retailMap.put("currentGP", retail.getCurrentGP());
		retailMap.put("nextCost", retail.getNextCost());
		retailMap.put("nextRetail", retail.getNextRetail());
		retailMap.put("nextMap", retail.getNextMap());
		retailMap.put("nextGP", retail.getNextGP());
		retailMap.put("nextPackSize", retail.getNextPackSize());
		retailMap.put("effectDate", new SimpleDateFormat(Constants.DATE_FORMAT_MM_DD_YYYY).format(retail.getNextEffectiveDate()));
		retailMap.put("status", retail.getStatus());
		retailMap.put("errTypDes", retail.getErrTypDes());
		retailMap.put("changeReason", "PACK");
		retailMap.put("competitors", retail.getLstCompetitor());
		return retailMap;
	}

	private Map<String, String> getMappingTableColumnName() {
		Map<String, String> map = new HashMap<String, String>();

		map.put("2", "FTC.PROD_SCN_CD");
		map.put("3", "DPC.PROD_DES");
		map.put("4", "PACKSIZE");
		map.put("5", "HEB_CST_AMT");
		map.put("6", "NEXT_RETL");

		map.put("7", "MAP");
		map.put("8", "COMP");
		map.put("9", "GP");
		map.put("10", "PACK/SIZE");
		map.put("11", "NextCost");
		map.put("12", "NextRetail");
		map.put("13", "MAP");

		map.put("14", "NEXT_GP");
		// map.put("15", "GP");
		map.put("15", "STATUS");
		map.put("16", "FTC.EFF_TS");
		map.put("17", "PRC_CHG_RSN_TXT");
		return map;
	}

	// /**
	// * get TabAtive From Session.
	// * @param session
	// * The HttpSession of HttpServlet
	// * @return Map<String, Integer> mapErrors
	// * @author diemnguyen
	// */
	// private Map<String, Integer> getTabAtiveFromSession(HttpSession session) {
	// Map<String, Integer> dataFromSession = new HashMap<String, Integer>();
	//
	// if (session.getAttribute(Constants.SESSION_PASSED_RETAILCOST) != null) {
	// int totalOfPass = (Integer) session.getAttribute(Constants.SESSION_PASSED_RETAILCOST);
	// dataFromSession.put(Constants.SESSION_PASSED_RETAILCOST, totalOfPass);
	// }
	// if (session.getAttribute(Constants.SESSION_FAILED_RETAILCOST) != null) {
	// int totalOfFailed = (Integer) session.getAttribute(Constants.SESSION_FAILED_RETAILCOST);
	// dataFromSession.put(Constants.SESSION_FAILED_RETAILCOST, totalOfFailed);
	// }
	// return dataFromSession;
	// }

	/**
	 * Reject product.
	 * @param jsonObject
	 *            :String
	 * @return String
	 * @throws DSVException
	 *             :DSVException
	 * @author diemnguyen.
	 */
	@ResponseBody
	@RequestMapping(value = { "/approve-all" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String approveAllProducts(String jsonObject) throws DSVException {
		// String orderJsonId = Constants.EMPTY_STRING;
		// this.retailCostService.rejectProducts(AHelper.parseJsonObjectToListRetailCostVO(orderJsonId));
		return Constants.EMPTY_STRING;
	}

	/**
	 * Remove product.
	 * @param jsonObject
	 *            :String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	// @ResponseBody
	// @RequestMapping(value = { "/remove" }, method = { RequestMethod.GET, RequestMethod.POST })
	// public String removeProduct(String jsonObject) throws DSVException {
	// LOG.info("removeProduct");
	// String orderJsonId = Constants.EMPTY_STRING;
	// this.retailCostService.removeProducts(AHelper.parseJsonObjectToListRetailCostVO(orderJsonId));
	// return Constants.EMPTY_STRING;
	// }

	/**
	 * Save product.
	 * @param jsonObject
	 *            :String
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	// @ResponseBody
	// @RequestMapping(value = { "/save" }, method = { RequestMethod.GET, RequestMethod.POST })
	// public String saveProduct(String jsonObject) throws DSVException {
	// LOG.info("saveProduct");
	// String orderJsonId = Constants.EMPTY_STRING;
	// this.retailCostService.saveProducts(AHelper.parseJsonObjectToListRetailCostVO(orderJsonId));
	// return Constants.EMPTY_STRING;
	// }

	/**
	 * Get Future Cost.
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	// @ResponseBody
	// @RequestMapping(value = { "/get-future-cost" }, method = { RequestMethod.GET, RequestMethod.POST })
	// public String getFutureSegments() throws DSVException {
	// LOG.info("getFutureSegments");
	// return Constants.EMPTY_STRING;
	// }

	/**
	 * Export search result to excel file.
	 * @param request
	 *            The request of HttpServlet
	 * @param response
	 *            The response of HttpServlet
	 * @param prodSearchModel
	 *            :ProductSearchModel
	 * @throws DSVException
	 *             :DSVException
	 * @author diemnguyen.
	 */
	@RequestMapping(value = { EXPORT_COST_REVIEW_TO_EXCEL_DIRECTLY }, method = RequestMethod.POST)
	public void exportToExcel(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("retailCostModel") RetailCostSearchVO retailCostModel, @RequestParam("tabActive") String tabActive) throws DSVException {
		LOG.info("Export excel !!!!!!!!!!");
		List<RetailCostVO> lstResult = this.retailCostService.getProductWithCostChanges(this.setDataForRetailCostModel(retailCostModel, "exportExcel"), tabActive);

		LOG.info("LIST SIZE : " + lstResult.size());
		if (!Helper.isEmpty(lstResult) && Helper.isNormalList(lstResult)) {
			// export
			HSSFWorkbook workbook = this.excelTemplate.getExcelCostReview(lstResult);
			AHelper.buildExcelFile(response, workbook, AHelper.getDownloadFileName(Constants.LST_COST_REVIEW_EXCEL_FILE_NAME));
		}
	}

}
