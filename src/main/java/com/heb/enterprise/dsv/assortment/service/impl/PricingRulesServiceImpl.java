/*
 * $Id: PricingRulesServiceImpl.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.heb.enterprise.dsv.assortment.dao.GlobalRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.PricingRulesDAO;
import com.heb.enterprise.dsv.assortment.dao.RulesDAO;
import com.heb.enterprise.dsv.assortment.service.CacheService;
import com.heb.enterprise.dsv.assortment.service.PricingRulesService;
import com.heb.enterprise.dsv.assortment.utils.Convert;
import com.heb.enterprise.dsv.assortment.utils.DAOHelper;
import com.heb.enterprise.dsv.assortment.webservice.CompetitorServiceWS;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.DepartmentVO;
import com.heb.enterprise.dsv.vo.PricingRuleVO;
import com.heb.enterprise.dsv.vo.RuleDefinationVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;

/**
 * PricingRulesServiceImpl.
 * @author Duyen.le.
 */
@Service
public class PricingRulesServiceImpl implements PricingRulesService {
	private static final Logger LOG = Logger.getLogger(PricingRulesServiceImpl.class);
	/**
	 * inject CacheService.
	 */
	@Autowired
	private CacheService cacheService;
	/**
	 * inject CacheService.
	 */
	@Autowired
	private RulesDAO rulesDAO;
	/**
	 * inject CacheService.
	 */
	@Autowired
	private CompetitorServiceWS competitorService;

	/**
	 * inject PricingRulesDAO.
	 */
	@Autowired
	private PricingRulesDAO pricingRulesDAO;

	/**
	 * inject PricingRulesDAO.
	 */
	@Autowired
	private GlobalRulesDAO globalRulesDAO;

	/**
	 * getListCommodities - get List of Commodities.
	 * @author Duyen.Le
	 * @return List<CommodityVO>
	 * @throws DSVException
	 *             DSVException
	 */
	@Override
	public List<CommodityVO> getListCommodities() throws DSVException {
		return this.cacheService.getCommodityDetailList();
	}

	/**
	 * getOnlineCompetitors - get List of Competitors.
	 * @author Duyen.Le
	 * @return List<CompetitiveRetails>
	 */
	@Override
	public List<BaseVO> getOnlineCompetitors() {
		// this method call to WEbService.
		// hard code
		List<BaseVO> lstCompetitors = new ArrayList<BaseVO>();
		// lstCompetitors = this.competitorService.getOnlineCompetitors();
		BaseVO competitor = new BaseVO();
		competitor.setId("1");
		competitor.setName("Amazon");
		lstCompetitors.add(competitor);
		competitor = new BaseVO();
		competitor.setId("2");
		competitor.setName("WalMart");
		lstCompetitors.add(competitor);
		competitor = new BaseVO();
		competitor.setId("3");
		competitor.setName("BestBuy");
		lstCompetitors.add(competitor);
		competitor = new BaseVO();
		competitor.setId("4");
		competitor.setName("WayFair");
		lstCompetitors.add(competitor);
		return lstCompetitors;
	}

	/**
	 * getList Sub-Commodities by commodityId.
	 * @param commodityId
	 *            String
	 * @author Duyen.Le
	 * @return List<BaseVO>
	 * @throws DSVException
	 *             - if have any errors from getting data.
	 */
	@Override
	public List<SubCommodityVO> getListSubCommByCommodityId(String commodityId) throws DSVException {
		return this.cacheService.getSubCommByCommodityId(commodityId);
	}

	/**
	 * @author Duyen.Le
	 * @param vendorId
	 *            the vendor id
	 * @param commodityId
	 *            String
	 * @param subCommodityId
	 *            the sub commodity id
	 * @return PricingRuleVO
	 */
	@Override
	public PricingRuleVO getPricingRules(String vendorId, String commodityId, String subCommodityId) {
		LOG.info("getPricingRules vendorId = " + vendorId + " commodityId =" + commodityId + " subCommodityId =" + subCommodityId);
		PricingRuleVO pricingRuleVO = new PricingRuleVO();
		Map<String, AssortmentRulesVO> assortmentRulesVOAsHashmap = null;
		List<AssortmentRulesVO> lstAssortmentRulesVOs = null;
		List<String> lstCompetitors = null;

		if (!Helper.isEmpty(vendorId)) {
			lstAssortmentRulesVOs = this.pricingRulesDAO.getPricingRuleByVendor(vendorId);
			lstCompetitors = this.pricingRulesDAO.getPricingCompetitorByVendor(vendorId);
		} else if (Helper.isEmpty(subCommodityId)) {
			lstAssortmentRulesVOs = this.pricingRulesDAO.getPricingRuleByCommodity(commodityId);
			lstCompetitors = this.pricingRulesDAO.getPricingCompetitorByCommodity(commodityId);
		} else {
			// Search Commodity + SubCommodity
			lstAssortmentRulesVOs = this.pricingRulesDAO.getPricingRuleByCommoditySubCommodity(commodityId, subCommodityId);
			lstCompetitors = this.pricingRulesDAO.getPricingCompetitorByCommoditySubCommodity(commodityId, subCommodityId);
			if (Helper.isEmpty(lstCompetitors)) {
				lstCompetitors = this.pricingRulesDAO.getPricingCompetitorByCommodity(commodityId);
			}
		}
		if (Helper.isNormalList(lstAssortmentRulesVOs)) {
			assortmentRulesVOAsHashmap = DAOHelper.convertColectionRuleToMap(lstAssortmentRulesVOs);
			pricingRuleVO.setLvlType(lstAssortmentRulesVOs.get(0).getType());
		}
		pricingRuleVO.setLstAssortmentRulesVODisplays(assortmentRulesVOAsHashmap);
		pricingRuleVO.setLstCompetitors(lstCompetitors);
		return pricingRuleVO;
	}

	/**
	 * Update pricing rules.
	 * @author Duyen.Le
	 * @param userId
	 *            the user id
	 * @param pricingRuleVO
	 *            the pricing rule vo
	 * @return String
	 * @throws DSVException
	 */
	@Override
	@Transactional("transactionManager")
	public String updatePricingRules(String userId, PricingRuleVO pricingRuleVO) throws DSVException {
		LOG.info("## updatePricingRules ##" + userId);
		String mess = Constants.SUCCESSFULLY;

		long lvlId = this.getLvlId(pricingRuleVO.getVendorId(), pricingRuleVO.getCommodityId(), pricingRuleVO.getSubCommodityId());
		LOG.info("## lvlId ##" + lvlId);
		List<RuleDefinationVO> lstRuleDefinationVOs = new ArrayList<RuleDefinationVO>();
		List<DepartmentVO> lstDepartmentVOs = this.cacheService.getHierachyForGlobalRulesFromCache();
		for (AssortmentRulesVO asm : pricingRuleVO.getLstAssortmentRulesVOs()) {
			Convert.convertPricingRulesVO(asm, lstRuleDefinationVOs);
		}
		if (lvlId > 0) {
			// lvlID
			if (!Helper.isEmpty(pricingRuleVO.getLvlType())) {
				this.rulesDAO.updatelvlTyp(lvlId, pricingRuleVO.getLvlType());
			}
			this.rulesDAO.updateRuleValuesToDfnsPricingRules(lstRuleDefinationVOs, lvlId, userId);
			this.pricingRulesDAO.removeComparetorInfo(lvlId);
			if (!Helper.isEmpty(pricingRuleVO.getLstCompetitors())) {
				this.pricingRulesDAO.insertComparetorInfo(lvlId, pricingRuleVO.getLstCompetitors(), userId);
			}
		} else {
			String deptId = Constants.WHITE_SPACE;
			String subDeptId = Constants.WHITE_SPACE;
			long itmCls = 0;
			if (!Helper.isEmpty(pricingRuleVO.getCommodityId()) || !Helper.isEmpty(pricingRuleVO.getSubCommodityId())) {
				AssortmentRulesVO assortmentRulesVOTmp = Convert.getHierachy(lstDepartmentVOs, null,
						pricingRuleVO.getCommodityId(), pricingRuleVO.getSubCommodityId());
				deptId = assortmentRulesVOTmp.getDeptId();
				subDeptId = assortmentRulesVOTmp.getSubDeptId();
				itmCls = assortmentRulesVOTmp.getItmCls();
			}
			lvlId = this.globalRulesDAO.getMaxLVLID() + 1;
			LOG.info("## lvlId ##" + lvlId + "deptId=" + deptId + "-" + subDeptId + "-" + itmCls);
			this.globalRulesDAO
					.insertlvl(lvlId, userId, pricingRuleVO.getCommodityId(), deptId, subDeptId, itmCls, pricingRuleVO.getVendorId(), pricingRuleVO.getSubCommodityId(), pricingRuleVO.getLvlType());
			this.globalRulesDAO.insertRulesToDfns(lstRuleDefinationVOs, lvlId, userId);
			this.pricingRulesDAO.removeComparetorInfo(lvlId);
			if (!Helper.isEmpty(pricingRuleVO.getLstCompetitors())) {
				this.pricingRulesDAO.insertComparetorInfo(lvlId, pricingRuleVO.getLstCompetitors(), userId);
			}
		}

		return mess;
	}

	/**
	 * Getting level Id.
	 * @author Duyen.Le
	 * @param vendorId
	 *            the vendor id
	 * @param commodity
	 *            the commodity
	 * @param subCommodity
	 *            the sub commodity
	 * @return long
	 */
	@Override
	public long getLvlId(String vendorId, String commodity, String subCommodity) {
		long lvlId = 0;
		if (!Helper.isEmpty(vendorId)) {
			lvlId = this.pricingRulesDAO.getLvlByVendor(vendorId);
		} else if (!Helper.isEmpty(subCommodity)) {
			lvlId = this.pricingRulesDAO.getLvlByCommoditySubCommodity(commodity, subCommodity);
		} else {
			lvlId = this.pricingRulesDAO.getLvlByCommodity(commodity);
		}
		return lvlId;
	}

	@Override
	public List<BaseVO> getActTyp() {
		return new ArrayList<BaseVO>();
	}
}
