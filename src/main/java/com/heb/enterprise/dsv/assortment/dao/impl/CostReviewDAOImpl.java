/*
 * $Id: RetailCostDAOImpl.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.taglibs.standard.lang.jstl.Constants;

import com.heb.enterprise.dsv.assortment.dao.CostReviewDAO;
import com.heb.enterprise.dsv.assortment.dao.mapper.CostReviewMapper;

/**
 * CostReviewDAOImpl interface.
 * @author diemnguyen
 */
@Repository
public class CostReviewDAOImpl extends BaseDAOImpl implements CostReviewDAO {
	private static final Logger LOG = Logger.getLogger(CostReviewDAOImpl.class);
	/**
	 * find Retail and code SqlString.
	 */
	@Value("${coms.dsv.asm.costReview.getProductWithCostChangesPassed}")
	private String getProductWithCostChangesPassedSql;
	@Value("${coms.dsv.asm.costReview.getProductWithCostChangesFailed}")
	private String getProductWithCostChangesFailedSql;
	@Value("${coms.dsv.asm.costReview.getProductWithCostChangesAll}")
	private String getProductWithCostChangesAllSql;
	@Value("${coms.dsv.asm.costReview.getTotalPassedFailed}")
	private String getTotalPassedFailedSql;
	@Value("${coms.dsv.asm.costReview.getTotalPassed}")
	private String getTotalPassedSql;
	@Value("${coms.dsv.asm.costReview.getTotalFailed}")
	private String getTotalFailedSql;
	@Value("${coms.dsv.asm.costReview.getBoundaryRules}")
	private String getBoundaryRulesSql;

	/**
	 * find Retail and Cost by searching criteria.
	 * @param model
	 *            RetailCostSearchModel
	 * @param tabActive
	 *            String
	 * @return List<RetailCostModel> result
	 * @throws DSVException
	 *             - If could not connect to database
	 * @author diemnguyen
	 */
	@Override
	public List<RetailCostVO> getProductWithCostChanges(RetailCostSearchVO retailCostSearchVO, String tabActive) throws DSVException {
		List<RetailCostVO> lstData = new ArrayList<RetailCostVO>();
		String sqlString = Constants.EMPTY_STRING;
		if (Constants.ALL_LOWERCASE_STR.equals(tabActive)) {
			sqlString = this.generateStringSqlForTabAll(retailCostSearchVO);
		} else if (Constants.PASSED.equals(tabActive)) {
			sqlString = this.generateStringSqlForTabPassed(retailCostSearchVO);
		} else if (Constants.FAILED.equals(tabActive)) {
			sqlString = this.generateStringSqlForTabFailed(retailCostSearchVO);
		}
		try {
			lstData = this.getJdbcTemplate().query(sqlString, CostReviewMapper.getProductWithCostChangesMapper());// CostReviewMapper.productWithCostChangesSetter(retailCostSearchVO)
			LOG.info("Select SQL : " + sqlString);
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}

		return lstData;
	}

	/**
	 * generate SqlString.
	 * @param retailCostSearchVO
	 *            :retailCostSearchVO
	 * @param tabActive
	 *            :String
	 * @param type
	 *            :String
	 * @return String
	 * @author diemnguyen
	 */
	private String generateStringSql(RetailCostSearchVO retailCostSearchVO, String tabActive, String type) {
		StringBuilder strQuery = null;
		if (Constants.PASSED.equalsIgnoreCase(tabActive) && "select".equals(type)) {
			strQuery = new StringBuilder(this.getStringForFilter(retailCostSearchVO));
			strQuery.append(this.getProductWithCostChangesPassedSql);
		} else if (Constants.PASSED.equalsIgnoreCase(tabActive) && "count".equals(type)) {
			strQuery = new StringBuilder(this.getTotalPassedSql);
		} else if (Constants.FAILED.equalsIgnoreCase(tabActive) && "select".equals(type)) {
			strQuery = new StringBuilder(this.getStringForFilter(retailCostSearchVO));
			strQuery.append(this.getProductWithCostChangesFailedSql);
		} else if (Constants.FAILED.equalsIgnoreCase(tabActive) && "count".equals(type)) {
			strQuery = new StringBuilder(this.getTotalFailedSql);
		}

		strQuery.append(this.subGenerateStringSql1(retailCostSearchVO, tabActive));
		strQuery.append(this.subGenerateStringSql2(retailCostSearchVO, tabActive));
		strQuery.append(this.subGenerateStringSql3(retailCostSearchVO, tabActive));
		strQuery.append(this.subGenerateStringSql4(retailCostSearchVO, tabActive));
		strQuery.append(this.subGenerateStringSql5(retailCostSearchVO, tabActive));

		// /* filter */
		// if (!Helper.isEmpty(retailCostSearchVO.getProdDescFilter())) {
		// strQuery.append("AND DPC.PROD_DES LIKE '%" + retailCostSearchVO.getProdDescFilter().toUpperCase() + "%'");
		// }
		//
		// if (!Helper.isEmpty(retailCostSearchVO.getUpcIdFilter())) {
		// strQuery.append("AND FTC.PROD_SCN_CD LIKE '%" + retailCostSearchVO.getUpcIdFilter().toUpperCase() + "%'");
		// }
		//
		// // --
		// // mapFilter
		// if (!Helper.isEmpty(retailCostSearchVO.getMapFilter())) {
		// strQuery.append("AND RC.MAP_SW = " + retailCostSearchVO.getMapFilter().toUpperCase());
		// }

		// compFilter
		// if (!Helper.isEmpty(retailCostSearchVO.getUpcIdFilter())) {
		// strQuery.append("AND DPC.PROD_DES LIKE '%" + retailCostSearchVO.getUpcIdFilter() + "%'");
		// }

		// // nextMapFilter
		// if (!Helper.isEmpty(retailCostSearchVO.getNextMapFilter())) {
		// strQuery.append("AND FTC.MAP_SW = '" + retailCostSearchVO.getNextMapFilter() + "'");
		// }
		//
		// // changeReasonFilter
		// if (!Helper.isEmpty(retailCostSearchVO.getChangeReasonFilter())) {
		// strQuery.append("AND FTC.PRC_CHG_RSN_TXT = '" + retailCostSearchVO.getChangeReasonFilter() + "'");
		// }

		return strQuery.toString();
	}

	/**
	 * getStringForFilter.
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return String
	 * @author diemnguyen.
	 */
	private String getStringForFilter(RetailCostSearchVO retailCostSearchVO) {
		StringBuilder strQuery = new StringBuilder("SELECT ROW_NUMBER () OVER (ORDER BY ");
		strQuery.append(retailCostSearchVO.getiSortColumn());
		strQuery.append(Constants.WHITE_SPACE);
		strQuery.append(retailCostSearchVO.getsSortDir());
		strQuery.append(" ) ");
		return strQuery.toString();
	}

	/**
	 * subGenerateStringSql1.
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @param tabActive
	 *            :String
	 * @return String
	 * @author diemnguyen.
	 */
	private String subGenerateStringSql1(RetailCostSearchVO retailCostSearchVO, String tabActive) {
		StringBuilder strQuery = new StringBuilder();
		if (!Helper.isEmpty(retailCostSearchVO.getCommodittyId())) {
			strQuery.append(" AND DPC.HEB_COM_ID = " + retailCostSearchVO.getCommodittyId());
		}
		if (!Helper.isEmpty(retailCostSearchVO.getBrand())) {
			strQuery.append(" AND DPC.BRND_NM LIKE '%" + retailCostSearchVO.getBrand() + "%' ");
		}
		if (!Helper.isEmpty(retailCostSearchVO.getReceiveDateBegin()) && !Helper.isEmpty(retailCostSearchVO.getReceiveDateEnd())) {
			strQuery.append(" AND TRUNC(DPC.CRE8_TS) BETWEEN TO_DATE('" + retailCostSearchVO.getReceiveDateBegin() + "', 'mm/dd/YYYY') AND TO_DATE('" + retailCostSearchVO.getReceiveDateEnd()
					+ "', 'mm/dd/YYYY') ");
		}
		return strQuery.toString();
	}

	/**
	 * subGenerateStringSql2.
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @param tabActive
	 *            :String
	 * @return String
	 * @author diemnguyen.
	 */
	private String subGenerateStringSql2(RetailCostSearchVO retailCostSearchVO, String tabActive) {
		StringBuilder strQuery = new StringBuilder();
		if (!Helper.isEmpty(retailCostSearchVO.getMarginBegin()) && !Helper.isEmpty(retailCostSearchVO.getMarginEnd())) {
			strQuery.append(" AND FTC.HEB_RETL_AMT - FTC.HEB_CST_AMT BETWEEN " + retailCostSearchVO.getMarginBegin() + " AND " + retailCostSearchVO.getMarginEnd() + " ");
		}
		if (!Helper.isEmpty(retailCostSearchVO.getFailReason()) && Constants.FAILED.equals(tabActive)) {
			strQuery.append(" AND PRS.ERR_TYP_CD = '" + retailCostSearchVO.getFailReason() + "'");
		}
		if (!Helper.isEmpty(retailCostSearchVO.getVendorId())) {
			strQuery.append(" AND FTC.VEND_ID = " + retailCostSearchVO.getVendorId());
		}
		return strQuery.toString();
	}

	/**
	 * subGenerateStringSql3.
	 * @param tabActive
	 *            :String
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return String
	 * @author diemnguyen.
	 */
	private String subGenerateStringSql3(RetailCostSearchVO retailCostSearchVO, String tabActive) {
		StringBuilder strQuery = new StringBuilder();
		if (!Helper.isEmpty(retailCostSearchVO.getProdDes())) {
			strQuery.append(" AND DPC.PROD_DES LIKE '%" + retailCostSearchVO.getProdDes() + "%' ");
		}
		if (retailCostSearchVO.isAllMapProduct()) {
			strQuery.append(" AND RC.MAP_SW = 'Y'");
		}

		if (retailCostSearchVO.isOnlyPrePriceProduct()) {
			strQuery.append(" AND PRE_PRC_AMT > 0");
		}

		return strQuery.toString();
	}

	/**
	 * subGenerateStringSql4.
	 * @param tabActive
	 *            :String
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return String
	 * @author diemnguyen.
	 */
	private String subGenerateStringSql4(RetailCostSearchVO retailCostSearchVO, String tabActive) {
		StringBuilder strQuery = new StringBuilder();
		if (!Helper.isEmpty(retailCostSearchVO.getProdDescFilter())) {
			strQuery.append("AND DPC.PROD_DES LIKE '%" + retailCostSearchVO.getProdDescFilter().toUpperCase() + "%'");
		}

		if (!Helper.isEmpty(retailCostSearchVO.getUpcIdFilter())) {
			strQuery.append("AND FTC.PROD_SCN_CD LIKE '%" + retailCostSearchVO.getUpcIdFilter().toUpperCase() + "%'");
		}

		if (!Helper.isEmpty(retailCostSearchVO.getMapFilter())) {
			strQuery.append("AND RC.MAP_SW = " + retailCostSearchVO.getMapFilter().toUpperCase());
		}
		return strQuery.toString();
	}

	/**
	 * subGenerateStringSql5.
	 * @param tabActive
	 *            :String
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return String
	 * @author diemnguyen.
	 */
	private String subGenerateStringSql5(RetailCostSearchVO retailCostSearchVO, String tabActive) {
		StringBuilder strQuery = new StringBuilder();
		// compFilter
		// if (!Helper.isEmpty(retailCostSearchVO.getUpcIdFilter())) {
		// strQuery.append("AND DPC.PROD_DES LIKE '%" + retailCostSearchVO.getUpcIdFilter() + "%'");
		// }

		// nextMapFilter
		if (!Helper.isEmpty(retailCostSearchVO.getNextMapFilter())) {
			strQuery.append("AND FTC.MAP_SW = '" + retailCostSearchVO.getNextMapFilter() + "'");
		}

		// changeReasonFilter
		if (!Helper.isEmpty(retailCostSearchVO.getChangeReasonFilter())) {
			strQuery.append("AND FTC.PRC_CHG_RSN_TXT = '" + retailCostSearchVO.getChangeReasonFilter() + "'");
		}
		return strQuery.toString();
	}

	/**
	 * generateStringSqlForTabPassed.
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return String
	 * @author diemnguyen.
	 */
	private String generateStringSqlForTabPassed(RetailCostSearchVO retailCostSearchVO) {
		StringBuilder sqlBuilder = new StringBuilder(" SELECT * FROM ( ");
		sqlBuilder.append(this.generateStringSql(retailCostSearchVO, Constants.PASSED, "select"));
		sqlBuilder.append(")");
		if (!"exportExcel".equals(retailCostSearchVO.getAction())) {
			sqlBuilder.append(" WHERE RN BETWEEN " + retailCostSearchVO.getBegin() + " AND " + retailCostSearchVO.getEnd() + "");
		}

		return sqlBuilder.toString();
	}

	/**
	 * generateStringSqlForTabFailed.
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return String
	 * @author diemnguyen.
	 */
	private String generateStringSqlForTabFailed(RetailCostSearchVO retailCostSearchVO) {
		StringBuilder sqlBuilder = new StringBuilder(" SELECT * FROM ( ");
		sqlBuilder.append(this.generateStringSql(retailCostSearchVO, Constants.FAILED, "select"));
		sqlBuilder.append(")");
		if (!"exportExcel".equals(retailCostSearchVO.getAction())) {
			sqlBuilder.append(" WHERE RN BETWEEN " + retailCostSearchVO.getBegin() + " AND " + retailCostSearchVO.getEnd() + "");
		}
		return sqlBuilder.toString();
	}

	/**
	 * generateStringSqlForTabAll.
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return String
	 * @author diemnguyen.
	 */
	private String generateStringSqlForTabAll(RetailCostSearchVO retailCostSearchVO) {
		StringBuilder strQuery = new StringBuilder("SELECT * FROM ( SELECT ROW_NUMBER () OVER ( ORDER BY ");
		strQuery.append("tabAll.");
		strQuery.append(retailCostSearchVO.getiSortColumn().split("\\.")[AntiMagicNumber.ONE]);
		strQuery.append(" ");
		strQuery.append(retailCostSearchVO.getsSortDir());
		strQuery.append(" ) ");
		strQuery.append(this.getProductWithCostChangesAllSql);// EFF_TS ASC )
		strQuery.append(" ( ");
		strQuery.append(this.generateStringSql(retailCostSearchVO, Constants.PASSED, "select"));
		strQuery.append(" UNION ALL ");
		strQuery.append(this.generateStringSql(retailCostSearchVO, Constants.FAILED, "select"));
		strQuery.append(" ) ");
		strQuery.append(" tabAll " + " )");

		if (!"exportExcel".equals(retailCostSearchVO.getAction())) {
			strQuery.append(" WHERE RN BETWEEN " + retailCostSearchVO.getBegin() + " AND " + retailCostSearchVO.getEnd() + "");
		}

		// status filter
		if (!Helper.isEmpty(retailCostSearchVO.getStatus())) {
			strQuery.append(" AND STATUS = '" + retailCostSearchVO.getStatus() + "'");
		}
		return strQuery.toString();
	}

	/**
	 * generateStringSqlTotalAllTypes.
	 * @param retailCostSearchVO
	 *            :RetailCostSearchVO
	 * @return String
	 * @author diemnguyen.
	 */
	private String generateStringSqlTotalAllTypes(RetailCostSearchVO retailCostSearchVO) {
		StringBuilder strQuery = new StringBuilder(" SELECT * FROM ( SELECT * FROM ( ");
		strQuery.append(this.generateStringSql(retailCostSearchVO, "passed", "count"));
		strQuery.append(" UNION ALL ");
		strQuery.append(this.generateStringSql(retailCostSearchVO, "failed", "count"));
		strQuery.append(" ) tabAll ) ");
		return strQuery.toString();
	}

	/**
	 * check Status Of TabActive.
	 * @return int - Type Of Tab.
	 * @param tabActive
	 *            - There are 3 types : Pass/Failed/Rejected
	 * @author diemnguyen
	 */
	// private int checkStatusOfTabActive(String tabActive) {
	// int typeStatus = 0;
	// if ("passed".equalsIgnoreCase(tabActive)) {
	// typeStatus = 0;
	// } else if ("failed".equalsIgnoreCase(tabActive)) {
	// typeStatus = 1;
	// }
	// return typeStatus;
	// }

	/**
	 * totalOfResultForEachStatus.
	 * @param model
	 *            :RetailCostSearchVO
	 * @param tabActive
	 *            :String
	 * @return Map<String, Integer>
	 * @throws DSVException
	 *             :DSVException
	 * @author diemnguyen.
	 */
	@Override
	public Map<String, Integer> getTotalPassedFailed(RetailCostSearchVO retailCostSearchVO, String tabActive) throws DSVException {
		Map<String, Integer> lstStatusTab = new HashMap<String, Integer>();
		String sql = this.generateStringSqlTotalAllTypes(retailCostSearchVO);
		List<Integer> result = this.getJdbcTemplate().query(sql, CostReviewMapper.getTotalPassedFailedMapper());
		lstStatusTab.put(Constants.ALL_LOWERCASE_STR, result.get(AntiMagicNumber.ZERO) + result.get(AntiMagicNumber.ONE));
		lstStatusTab.put(Constants.PASSED, result.get(AntiMagicNumber.ZERO));
		lstStatusTab.put(Constants.FAILED, result.get(AntiMagicNumber.ONE));
		return lstStatusTab;
	}

	/**
	 * Reject product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public String rejectProducts(List<RetailCostVO> retailCostVOs) throws DSVException {
		// TODO Auto-generated method stub
		return Constants.EMPTY_STRING;
	}

	/**
	 * Remove product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public String removeProducts(List<RetailCostVO> retailCostVOs) throws DSVException {
		// TODO Auto-generated method stub
		return Constants.EMPTY_STRING;
	}

	/**
	 * Save product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public String saveProducts(List<RetailCostVO> retailCostVOs) throws DSVException {
		// TODO Auto-generated method stub
		return Constants.EMPTY_STRING;
	}

	/**
	 * Get Future Cost.
	 * @throws DSVException
	 *             If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	@Override
	public List<RetailCostVO> getFutureSegments() throws DSVException {
		// TODO Auto-generated method stub
		return new ArrayList<RetailCostVO>();
	}

	/**
	 * getBoundaryRules.
	 * @throws DSVException
	 *             :DSVException.
	 * @return List<BaseVO>
	 * @author diemnguyen.
	 */
	@Override
	public List<BaseVO> getBoundaryRules() throws DSVException {
		List<BaseVO> lstData = new ArrayList<BaseVO>();
		try {
			lstData = this.getJdbcTemplate().query(this.getBoundaryRulesSql, CostReviewMapper.getBoundaryRulesMapper());
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
			throw new DSVException(e);
		}
		return lstData;
	}

}
