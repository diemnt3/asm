/*
 * $Id: CommonDAO.java, 5:38:05 PM$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao;

import java.util.List;

import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.VendorInfoVO;

/**
 * @author Duyen.le
 */
public interface CommonDAO {

	/**
	 * findAllRuleLogicOprtr.
	 * @return List<BaseModel> : List of Rules Logic is retrieved from Database.
	 * @throws DSVException
	 *             :if could not connect to Database.
	 * @author Duyen.le
	 */
	List<BaseVO> findAllRuleLogicOprtr() throws DSVException;

	/**
	 * getRulesByRuleGroup.
	 * @return List<AssortmentRulesVO> : List of Rules set are retrieved from Database.
	 * @author Duyen.le
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	List<AssortmentRulesVO> getRulesByRuleGroup() throws DSVException;

	/**
	 * getAllAssortmentRule.
	 * @return List<BaseVO> : List of Rules set are retrieved from Database.
	 * @author thangdang
	 * @throws DSVException
	 *             if could not connect to Database.
	 */
	List<BaseVO> getAllAssortmentRule() throws DSVException;

	/**
	 * Get all list vendors.
	 * @return List<VendorInfoVO>
	 * @author Duyen.le
	 */
	List<VendorInfoVO> getAllListVendors() throws DSVException;

}