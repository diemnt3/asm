/*
 * $Id: AConstants.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.utils;

/**
 * Constants for bussiness.
 * @author binhht
 */
public final class AConstants {

	/**
	 * SLASH.
	 */
	public static final String SLASH = "/";
	/**
	 * RULE_ID.
	 */
	public static final String RULE_ID = "RULE_ID";
	/**
	 * RULE_NM.
	 */
	public static final String RULE_NM = "RULE_NM";
	/**
	 * VAL.
	 */
	public static final String VAL = "VAL";
	/**
	 * SEQ_NBR.
	 */
	public static final String SEQ_NBR = "SEQ_NBR";
	/**
	 * ACTV_SW.
	 */
	public static final String ACTV_SW = "ACTV_SW";
	/**
	 * ACT_TYP_CD.
	 */
	public static final String ACT_TYP_CD = "ACT_TYP_CD";
	/**
	 * HEB_COM_ID.
	 */
	public static final String HEB_COM_ID = "HEB_COM_ID";
	/**
	 * STR_SUB_DEPT_ID.
	 */
	public static final String STR_SUB_DEPT_ID = "STR_SUB_DEPT_ID";
	/**
	 * STR_DEPT_NBR.
	 */
	public static final String STR_DEPT_NBR = "STR_DEPT_NBR";
	/**
	 * LVL_ID.
	 */
	public static final String LVL_ID = "LVL_ID";
	/**
	 * LVL_TYP_CD.
	 */
	public static final String LVL_TYP_CD = "LVL_TYP_CD";

	/**
	 * VEND_ID.
	 */
	public static final String VEND_ID = "VEND_ID";
	/**
	 * VEND_NM.
	 */
	public static final String VEND_NM = "VEND_NM";
	/**
	 * HEB_SUB_COM_ID.
	 */
	public static final String HEB_SUB_COM_ID = "HEB_SUB_COM_ID";
	/**
	 * CMPTR_ID.
	 */
	public static final String CMPTR_ID = "CMPTR_ID";

	/**
	 * DT_RowId.
	 */
	public static final String DT_RowId = "DT_RowId";
	/**
	 * BDMRIVEW_UPC.
	 */
	public static final String BDMRIVEW_UPC = "upc";

	/**
	 * BDMRIVEW_DESCRIPTION.
	 */
	public static final String BDMRIVEW_DESCRIPTION = "description";
	/**
	 * BDMRIVEW_SIZE.
	 */
	public static final String BDMRIVEW_SIZE = "size";
	/**
	 * BDMRIVEW_MAP.
	 */
	public static final String BDMRIVEW_MAP = "map";
	/**
	 * BDMRIVEW_PREPRICE.
	 */
	public static final String BDMRIVEW_PREPRICE = "prePrice";
	/**
	 * BDMRIVEW_MSRP.
	 */
	public static final String BDMRIVEW_MSRP = "msrp";
	/**
	 * BDMRIVEW_SRP.
	 */
	public static final String BDMRIVEW_SRP = "srp";
	/**
	 * BDMRIVEW_COST.
	 */
	public static final String BDMRIVEW_COST = "cost";
	/**
	 * BDMRIVEW_GPPERCENT.
	 */
	public static final String BDMRIVEW_GPPERCENT = "gppercent";
	/**
	 * BDMRIVEW_PENNYPROFIT.
	 */
	public static final String BDMRIVEW_PENNYPROFIT = "pennyProfit";
	/**
	 * BDMRIVEW_IMAGE.
	 */
	public static final String BDMRIVEW_IMAGE = "image";
	/**
	 * BDMRIVEW_FAILEDREASON.
	 */
	public static final String BDMRIVEW_FAILEDREASON = "failedReason";
	/**
	 * BDMRIVEW_FAILEDREASONCODE.
	 */
	public static final String BDMRIVEW_FAILEDREASONCODE = "failedReasonCode";
	/**
	 * TOTALROWS.
	 */
	public static final String BDMRIVEW_STATUS = "status";
	/**
	 * TOTALROWS.
	 */
	public static final String BDMRIVEW_FIRST_RECEIVED_TS = "firstReceivedTS";
	/**
	 * TOTALROWS.
	 */
	public static final String BDMRIVEW_LAST_EDITED_TS = "lastEditedTS";
	/**
	 * TOTALROWS.
	 */
	public static final String BDMRIVEW_TOTAL_ROWS = "totalRow";
	/**
	 * ACT_TYP_DES.
	 */
	public static final String ACT_TYP_DES = "ACT_TYP_DES";
	/**
	 * BDMRIVEW_UPC.
	 */
	public static final String NO = "no";

	/**
	 * BDMRIVEW_UPC.
	 */
	public static final String BDMRIVEW_REVIEW_STATUS = "reviewStatus";
	/**
	 * HEB_COM_ID.
	 */
	public static final String BDMRIVEW_REVIEW_HEB_RETAIL = "hebRetail";
	/**
	 * RJECT.
	 */
	public static final String RJECT = "RJECT";
	/**
	 * APPRV.
	 */
	public static final String APPRV = "APPRV";
	/**
	 * REV_STAT_CD.
	 */
	public static final String REV_STAT_CD = "REV_STAT_CD";
	/**
	 * DESC.
	 */
	public static final String DESC = "DESC";
	/**
	 * LST_UPDT_TS.
	 */
	public static final String LST_UPDT_TS = "LST_UPDT_TS";
	/**
	 * LST_UPDT_UID.
	 */
	public static final String LST_UPDT_UID = "LST_UPDT_UID";
	/**
	 * ERR_TYP_CD.
	 */
	public static final String ERR_TYP_CD = "ERR_TYP_CD";
	/**
	 * ERR_TYP_DES.
	 */
	public static final String ERR_TYP_DES = "ERR_TYP_DES";

	/**
	 * Constructor method.
	 */
	private AConstants() {
	}
}
