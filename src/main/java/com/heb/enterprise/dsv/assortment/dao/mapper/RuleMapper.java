/*
 * $Id.: RuleMapper.java, Oct 28, 2014$
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.heb.enterprise.dsv.assortment.utils.AConstants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.AssortmentRulesVO;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.LevelVO;
import com.heb.enterprise.dsv.vo.VendorAttributeVO;

/**
 * RuleMapper class.
 * @author Duyen.le
 */
public final class RuleMapper {
	private static final Logger LOG = Logger.getLogger(RuleMapper.class);

	/**
	 * RuleMapper - Constructor method.
	 */
	private RuleMapper() {
	}

	/**
	 * getGlobalRulesMapper - find all Invoice Status.
	 * @author Duyen.le
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<AssortmentRulesVO> getGlobalRulesMapper() {
		RowMapper<AssortmentRulesVO> globalRulesMapper = new RowMapper<AssortmentRulesVO>() {
			@Override
			public AssortmentRulesVO mapRow(ResultSet rs, int arg1) throws SQLException {
				AssortmentRulesVO rule = new AssortmentRulesVO();
				List<String> lstVal = new ArrayList<String>();
				rule.setId(rs.getString(AConstants.RULE_ID));
				rule.setName(rs.getString(AConstants.RULE_NM));
				rule.setValue(String.valueOf(rs.getFloat(AConstants.VAL)));
				lstVal.add(rule.getValue());
				rule.setValues(lstVal);
				rule.setSeqNbr(rs.getInt(AConstants.SEQ_NBR));
				rule.setActiveSw(rs.getString(AConstants.ACTV_SW));
				rule.setHebComdId(rs.getLong(AConstants.HEB_COM_ID));
				rule.setSubDeptId(Helper.trim(rs.getString(AConstants.STR_SUB_DEPT_ID)));
				rule.setDeptId(Helper.trim(rs.getString(AConstants.STR_DEPT_NBR)));
				rule.setLvlId(rs.getLong(AConstants.LVL_ID));
				rule.setLstUpdateTs(rs.getString(AConstants.LST_UPDT_TS));
				rule.setLstUpdateUid(rs.getString(AConstants.LST_UPDT_UID));
				return rule;
			}
		};
		return globalRulesMapper;
	}

	/**
	 * getInvoiceStatusMapper - find all Invoice Status.
	 * @author Duyen.le
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<AssortmentRulesVO> getGlobalRuleMapper() {
		RowMapper<AssortmentRulesVO> rulesMapper = new RowMapper<AssortmentRulesVO>() {
			@Override
			public AssortmentRulesVO mapRow(ResultSet rs, int arg1) throws SQLException {
				AssortmentRulesVO rule = new AssortmentRulesVO();
				rule.setId(rs.getString(AConstants.RULE_ID));
				rule.setName(rs.getString(AConstants.RULE_NM));
				rule.setType(rs.getString(AConstants.ACT_TYP_CD));
				rule.setValue(rs.getString(AConstants.VAL));
				rule.setActiveSw(rs.getString(AConstants.ACTV_SW));

				return rule;
			}
		};
		return rulesMapper;
	}

	/**
	 * getInvoiceStatusMapper - find all Invoice Status.
	 * @author Duyen.le
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<String> getExistRuleMapper() {
		RowMapper<String> rulesMapper = new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int arg1) throws SQLException {
				String rslt = rs.getString(AConstants.RULE_ID);

				return rslt;
			}
		};
		return rulesMapper;
	}

	/**
	 * getAttrMapper - find all Attributes.
	 * @author Duyen.le
	 * @return RowMapper<LevelVO>
	 */
	public static RowMapper<VendorAttributeVO> getAttrMapper() {
		RowMapper<VendorAttributeVO> attrMapper = new RowMapper<VendorAttributeVO>() {
			@Override
			public VendorAttributeVO mapRow(ResultSet rs, int arg1) throws SQLException {
				VendorAttributeVO attr = new VendorAttributeVO();
				attr.setId(rs.getInt("ATTR_ID"));
				attr.setValue(rs.getString("ATTR_NM"));
				return attr;
			}
		};
		return attrMapper;
	}

	/**
	 * getAttrMapper - find all Attributes.
	 * @author Duyen.le
	 * @return RowMapper<LevelVO>
	 */
	public static RowMapper<VendorAttributeVO> getVendorAttrMapper() {
		RowMapper<VendorAttributeVO> attrMapper = new RowMapper<VendorAttributeVO>() {
			@Override
			public VendorAttributeVO mapRow(ResultSet rs, int arg1) throws SQLException {
				VendorAttributeVO attr = new VendorAttributeVO();
				attr.setId(rs.getInt("ATTR_ID"));
				attr.setValue(rs.getString("ATTR_VAL"));
				return attr;
			}
		};
		return attrMapper;
	}

	/**
	 * getVendorRuleMapper.
	 * @author Duyen.le
	 * @return RowMapper<AssortmentRulesVO>
	 */
	public static RowMapper<AssortmentRulesVO> getVendorRuleMapper() {
		RowMapper<AssortmentRulesVO> rulesMapper = new RowMapper<AssortmentRulesVO>() {
			@Override
			public AssortmentRulesVO mapRow(ResultSet rs, int arg1) throws SQLException {
				AssortmentRulesVO rule = new AssortmentRulesVO();
				rule.setId(rs.getString(AConstants.RULE_ID));
				rule.setLvlId(rs.getLong(AConstants.LVL_ID));
				rule.setType(rs.getString(AConstants.ACT_TYP_CD));
				rule.setValue(rs.getString(AConstants.VAL));
				rule.setActiveSw(rs.getString(AConstants.ACTV_SW));
				return rule;
			}
		};
		return rulesMapper;
	}

	/**
	 * getAttrMapper - find all Attributes.
	 * @author Duyen.le
	 * @return RowMapper<LevelVO>
	 */
	public static RowMapper<Long> getLvlIdMapper() {
		RowMapper<Long> lvlId = new RowMapper<Long>() {
			@Override
			public Long mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getLong(AConstants.LVL_ID);
			}
		};
		return lvlId;
	}

	/**
	 * getGlobalCommodityOverrideMapper.
	 * @author anhtran.
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<LevelVO> getGlobalCommodityOverrideMapper() {
		RowMapper<LevelVO> mapper = new RowMapper<LevelVO>() {
			@Override
			public LevelVO mapRow(ResultSet rs, int arg1) throws SQLException {
				LevelVO levelVO = new LevelVO();
				levelVO.setSubDeptId(Helper.trim(rs.getString(AConstants.STR_DEPT_NBR)) +
						Helper.trim(rs.getString(AConstants.STR_SUB_DEPT_ID)));
				levelVO.setComdId(rs.getLong(AConstants.HEB_COM_ID));
				levelVO.setSubComdId(rs.getLong(AConstants.HEB_SUB_COM_ID));
				return levelVO;
			}
		};
		return mapper;
	}

	/**
	 * getGlobalCommodityOverrideMapper.
	 * @author anhtran.
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<Integer> getCommodityModifiedMapper() {
		RowMapper<Integer> comd = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getInt(AConstants.HEB_COM_ID);
			}
		};
		return comd;
	}

	/**
	 * getInvoiceStatusMapper - find all Invoice Status.
	 * @author Duyen.le
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<AssortmentRulesVO> getVendorRulesMapper() {
		RowMapper<AssortmentRulesVO> vendorRulesMapper = new RowMapper<AssortmentRulesVO>() {
			@Override
			public AssortmentRulesVO mapRow(ResultSet rs, int arg1) throws SQLException {
				AssortmentRulesVO rule = new AssortmentRulesVO();
				List<String> lstVal = new ArrayList<String>();
				rule.setId(rs.getString(AConstants.RULE_ID));
				rule.setName(rs.getString(AConstants.RULE_NM));
				rule.setValue(String.valueOf(rs.getFloat(AConstants.VAL)));
				lstVal.add(rule.getValue());
				rule.setValues(lstVal);
				rule.setSeqNbr(rs.getInt(AConstants.SEQ_NBR));
				rule.setActiveSw(rs.getString(AConstants.ACTV_SW));
				rule.setLvlId(rs.getLong(AConstants.LVL_ID));
				rule.setHebComdId(rs.getLong(AConstants.HEB_COM_ID));
				rule.setSubDeptId(Helper.trim(rs.getString(AConstants.STR_DEPT_NBR)) +
						Helper.trim(rs.getString(AConstants.STR_SUB_DEPT_ID)));
				rule.setVendorId(rs.getString(AConstants.VEND_ID));
				rule.setLstUpdateTs(rs.getString(AConstants.LST_UPDT_TS));
				rule.setLstUpdateUid(rs.getString(AConstants.LST_UPDT_UID));
				return rule;
			}
		};
		return vendorRulesMapper;
	}

	/**
	 * getInvoiceStatusMapper - find all Invoice Status.
	 * @author Duyen.le
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<AssortmentRulesVO> getVendorRulesBySubComdMapper() {
		RowMapper<AssortmentRulesVO> vendorRulesBySubComdMapper = new RowMapper<AssortmentRulesVO>() {
			@Override
			public AssortmentRulesVO mapRow(ResultSet rs, int arg1) throws SQLException {
				AssortmentRulesVO rule = new AssortmentRulesVO();
				List<String> lstVal = new ArrayList<String>();
				rule.setId(rs.getString(AConstants.RULE_ID));
				rule.setName(rs.getString(AConstants.RULE_NM));
				rule.setValue(String.valueOf(rs.getFloat(AConstants.VAL)));
				lstVal.add(rule.getValue());
				rule.setValues(lstVal);
				rule.setSeqNbr(rs.getInt(AConstants.SEQ_NBR));
				rule.setActiveSw(rs.getString(AConstants.ACTV_SW));
				rule.setHebSubComdId(rs.getLong(AConstants.HEB_SUB_COM_ID));
				rule.setHebComdId(rs.getLong(AConstants.HEB_COM_ID));
				rule.setSubDeptId(Helper.trim(rs.getString(AConstants.STR_DEPT_NBR)) +
						Helper.trim(rs.getString(AConstants.STR_SUB_DEPT_ID)));
				// rule.setDeptId(rs.getString("STR_DEPT_NBR"));
				rule.setVendorId(rs.getString(AConstants.VEND_ID));
				rule.setLvlId(rs.getLong(AConstants.LVL_ID));
				rule.setLstUpdateTs(rs.getString(AConstants.LST_UPDT_TS));
				rule.setLstUpdateUid(rs.getString(AConstants.LST_UPDT_UID));
				return rule;
			}
		};
		return vendorRulesBySubComdMapper;
	}

	/**
	 * getAttrMapper - find all Attributes.
	 * @author Duyen.le
	 * @return RowMapper<LevelVO>
	 */
	public static RowMapper<Integer> getHebComdMapper() {
		RowMapper<Integer> comd = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getInt(AConstants.HEB_SUB_COM_ID);
			}
		};
		return comd;
	}

	/**
	 * getAttrMapper - find all Attributes.
	 * @author Duyen.le
	 * @return RowMapper<LevelVO>
	 */
	public static RowMapper<Integer> getListExistAttrMapper() {
		RowMapper<Integer> attr = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
				return rs.getInt("Attr_Id");
			}
		};
		return attr;
	}

	/**
	 * getPricingRulesMapper.
	 * @author thangdang
	 * @return RowMapper<AssortmentRulesVO>
	 */
	public static RowMapper<AssortmentRulesVO> getPricingRulesMapper() {
		LOG.info("## getPricingRulesMapper ##");
		RowMapper<AssortmentRulesVO> pricingRulesMapper = new RowMapper<AssortmentRulesVO>() {
			@Override
			public AssortmentRulesVO mapRow(ResultSet rs, int arg1) throws SQLException {
				AssortmentRulesVO rule = new AssortmentRulesVO();
				List<String> lstVal = new ArrayList<String>();
				rule.setId(rs.getString(AConstants.RULE_ID));
				rule.setName(rs.getString(AConstants.RULE_NM));
				rule.setValue(rs.getString(AConstants.VAL));
				lstVal.add(rule.getValue());
				rule.setValues(lstVal);
				rule.setSeqNbr(rs.getInt(AConstants.SEQ_NBR));
				rule.setActiveSw(rs.getString(AConstants.ACTV_SW));
				rule.setType(rs.getString(AConstants.LVL_TYP_CD));
				rule.setActTypCd(rs.getString(AConstants.ACT_TYP_CD));
				rule.setLstUpdateTs(rs.getString(AConstants.LST_UPDT_TS));
				rule.setLstUpdateUid(rs.getString(AConstants.LST_UPDT_UID));
				LOG.info(rule.getId() + " -- " + rule.getName() + " -- " + rule.getValue() + " -- " + rule.getSeqNbr() + " -- " + rule.getActTypCd());
				return rule;
			}
		};
		return pricingRulesMapper;
	}

	/**
	 * getActTypMapper - find all ACT_TYP.
	 * @author thangdang
	 * @return RowMapper<LevelVO>
	 */
	public static RowMapper<BaseVO> getActTypMapper() {
		RowMapper<BaseVO> baseVOMapper = new RowMapper<BaseVO>() {
			@Override
			public BaseVO mapRow(ResultSet rs, int arg1) throws SQLException {
				BaseVO baseVO = new BaseVO();
				baseVO.setId(rs.getString(AConstants.ACT_TYP_CD));
				baseVO.setName(rs.getString(AConstants.ACT_TYP_DES));
				return baseVO;
			}
		};
		return baseVOMapper;
	}

	/**
	 * getLstActivateSubComMapper.
	 * @author Duyen.le
	 * @return RowMapper<BaseModel>
	 */
	public static RowMapper<LevelVO> getLstActivateSubComMapper() {
		RowMapper<LevelVO> mapper = new RowMapper<LevelVO>() {
			@Override
			public LevelVO mapRow(ResultSet rs, int arg1) throws SQLException {
				LevelVO levelVO = new LevelVO();
				levelVO.setComdId(rs.getLong(AConstants.HEB_COM_ID));
				levelVO.setNumberOfActiveSubCom(rs.getInt("NUMBER_OF_SUB_COM"));
				return levelVO;
			}
		};
		return mapper;
	}
}
