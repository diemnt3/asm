/*
 * $Id: RetailCostDAO.java,v 1 2014/03/24 13:58:00 vn75430 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.dao;

import java.util.List;
import java.util.Map;

/**
 * RetailCostDAO interface.
 * @author diemnguyen
 */
public interface CostReviewDAO {
	/**
	 * find Retail and Cost by searching criteria.
	 * @param retailCostModel
	 *            RetailCostSearchModel
	 * @param tabActive
	 *            String
	 * @return List<RetailCostModel> result
	 * @throws DSVException
	 *             - If could not connect to database
	 * @author diemnguyen
	 */
	List<RetailCostVO> getProductWithCostChanges(RetailCostSearchVO retailCostModel, String tabActive) throws DSVException;

	/**
	 * total Of Result ForEach Status (passed or failed).
	 * @param model
	 *            RetailCostSearchModel
	 * @param tabActive
	 *            String
	 * @return Map<String, Integer> result - Each of status has matches result
	 * @throws DSVException
	 *             - If could not connect to database
	 * @author diemnguyen
	 */
	Map<String, Integer> getTotalPassedFailed(RetailCostSearchVO model, String tabActive) throws DSVException;

	/**
	 * Reject product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	String rejectProducts(List<RetailCostVO> retailCostVOs) throws DSVException;

	/**
	 * Remove product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	String removeProducts(List<RetailCostVO> retailCostVOs) throws DSVException;

	/**
	 * Save product.
	 * @param retailCostVOs
	 *            :List<RetailCostVO>
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	String saveProducts(List<RetailCostVO> retailCostVOs) throws DSVException;

	/**
	 * Get Future Cost.
	 * @throws DSVException
	 *             :If could not get values from DAO Layer.
	 * @return String
	 * @author diemnguyen.
	 */
	List<RetailCostVO> getFutureSegments() throws DSVException;

	/**
	 * getBoundaryRules.
	 * @throws DSVException
	 *             :DSVException.
	 * @return List<BaseVO>
	 * @author diemnguyen.
	 */
	List<BaseVO> getBoundaryRules() throws DSVException;
}
