/*
 * $Id: PricingRulesController.java,v 1.13 2015/03/19 04:14:38 vn55228 Exp $
 *
 * Copyright (c) 2014 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.heb.enterprise.dsv.assortment.service.CommonService;
import com.heb.enterprise.dsv.assortment.service.PricingRulesService;
import com.heb.enterprise.dsv.assortment.utils.ConstantsWeb;
import com.heb.enterprise.dsv.exception.DSVException;
import com.heb.enterprise.dsv.utils.Constants;
import com.heb.enterprise.dsv.utils.Helper;
import com.heb.enterprise.dsv.vo.BaseVO;
import com.heb.enterprise.dsv.vo.CommodityVO;
import com.heb.enterprise.dsv.vo.JSTreeVO;
import com.heb.enterprise.dsv.vo.PricingRuleVO;
import com.heb.enterprise.dsv.vo.SubCommodityVO;

/**
 * PricingRulesController.
 * @author Duyen.le.
 */
@Controller
public class PricingRulesController {
	private static final Logger LOG = Logger.getLogger(PricingRulesController.class);
	// this name show in address bar
	private static final String PRICING_RULES_PATH = Constants.SLASH + ConstantsWeb.PRICING_RULES_PAGE;
	private static final String GET_RULES_PATH = Constants.SLASH + ConstantsWeb.GET_RULES_PAGE;
	private static final String UPDATE_RULES_PATH = Constants.SLASH + ConstantsWeb.UPDATE_RULES;
	private static final String GET_ONLINE_COMPETITORS = Constants.SLASH + ConstantsWeb.GET_COMPETITORS;
	private static final String GET_COMMODITY_AUTOCOMPLETE_PATH = ConstantsWeb.AJAX_AUTOCOMPLETE_GET_COMMODITY;
	private static final String GET_SUBCOMM_AUTOCOMPLETE_PATH = ConstantsWeb.AJAX_AUTOCOMPLETE_GET_SUBCOMM;
	private static final String ERROR_MESSAGE_CONS = "errorMessage";
	private static final String PRICING_RULES_MODEL = "model";
	// TILE
	private static final String PRICING_TILE = "pricingRules.tile";

	/**
	 * Inject VendorRulesService.
	 */
	@Autowired
	private PricingRulesService pricingRulesService;

	@Autowired
	private CommonService commonService;

	/**
	 * Display The Setting Pricing Rules screen.
	 * @param model
	 *            ModelMap
	 * @return String
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@RequestMapping(value = { PRICING_RULES_PATH }, method = RequestMethod.GET)
	public String showPricingRulesScreen(ModelMap model)
			throws DSVException {
		LOG.info("show PRICING RULES SETTING screen");
		return PRICING_TILE;
	}

	/**
	 * Display List of Commodities by autocomplete.
	 * @return String
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_COMMODITY_AUTOCOMPLETE_PATH }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String ajaxGetCommodity()
			throws DSVException {
		LOG.info("Get autocomplete for commodity is processing ...");
		JSONObject jsonResult = new JSONObject();
		List<CommodityVO> lstCommodities = this.pricingRulesService.getListCommodities();
		Collections.sort(lstCommodities, CommodityVO.CommodityVOComparator);
		List<JSONObject> lstCommoditiesJson = new ArrayList<JSONObject>();
		if (Helper.isNormalList(lstCommodities)) {
			for (CommodityVO com : lstCommodities) {
				JSONObject commodity = new JSONObject();
				commodity.put(Constants.AUTO_VALUE, com.getCommodityCd());
				commodity.put(Constants.AUTO_NAME, com.getCommodityDesc());
				commodity.put(Constants.AUTO_LABEL, com.getCommodityCd() + Constants.WHITE_SPACE + Constants.DASH + Constants.WHITE_SPACE + com.getCommodityDesc());
				lstCommoditiesJson.add(commodity);
			}
		}
		jsonResult.put(Constants.AA_DATA, lstCommoditiesJson);
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * Display List of Sub-Commodities by autocomplete.
	 * @param commodityId
	 *            String
	 * @return String
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_SUBCOMM_AUTOCOMPLETE_PATH }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String ajaxGetSubCommodityByCommodityId(@RequestParam("commodityId") String commodityId) throws DSVException {
		LOG.info("Get autocomplete for sub-commodity is processing ...");
		JSONObject jsonResult = new JSONObject();
		List<SubCommodityVO> lstCommodities = this.pricingRulesService.getListSubCommByCommodityId(commodityId);
		List<JSONObject> lstSubCommJson = new ArrayList<JSONObject>();
		if (Helper.isNormalList(lstCommodities)) {
			JSONObject jsObj = new JSONObject();
			jsObj.put(Constants.AUTO_VALUE, Constants.EMPTY_STRING);
			jsObj.put(Constants.AUTO_NAME, Constants.EMPTY_STRING);
			jsObj.put(Constants.AUTO_LABEL, Constants.ALL_UPPERCASE_STR);
			lstSubCommJson.add(jsObj);
			for (SubCommodityVO subCom : lstCommodities) {
				JSONObject subComm = new JSONObject();
				subComm.put(Constants.AUTO_VALUE, subCom.getSubCommodityCd());
				subComm.put(Constants.AUTO_NAME, subCom.getSubCommodityDesc());
				subComm.put(Constants.AUTO_LABEL, subCom.getSubCommodityCd() + Constants.WHITE_SPACE + Constants.DASH + Constants.WHITE_SPACE + subCom.getSubCommodityDesc());
				lstSubCommJson.add(subComm);
			}
		}
		jsonResult.put(Constants.AA_DATA, lstSubCommJson);
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * ajaxGetOnlineCompetitors.
	 * @return String
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { GET_ONLINE_COMPETITORS }, method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String ajaxGetOnlineCompetitors()
			throws DSVException {
		LOG.info("ajaxGetOnlineCompetitors");
		JSONObject jsonResult = new JSONObject();
		List<BaseVO> lstCompetitors = this.pricingRulesService.getOnlineCompetitors();
		jsonResult.put(JSTreeVO.TEXT, "Select All");
		jsonResult.put(JSTreeVO.ID, "0_root");
		jsonResult.put(JSTreeVO.STATE, JSTreeVO.STATE_OPEN);
		List<JSONObject> lstCompetitor = new ArrayList<JSONObject>();
		for (BaseVO baseVO : lstCompetitors) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(JSTreeVO.ID, baseVO.getId());
			jsonObject.put(JSTreeVO.TEXT, baseVO.getName());
			lstCompetitor.add(jsonObject);
		}
		jsonResult.put(JSTreeVO.CHILDREN, lstCompetitor);
		return Helper.parseToJsonString(jsonResult);
	}

	/**
	 * getPricingSettingRules.
	 * @param vendorId
	 *            :String
	 * @param commodityId
	 *            :String
	 * @param subCommodityId
	 *            :String
	 * @return PricingRuleVO
	 * @throws DSVException
	 *             :DSVException
	 */
	@RequestMapping(value = { GET_RULES_PATH }, method = RequestMethod.GET)
	@ResponseBody
	public PricingRuleVO getPricingSettingRules(@RequestParam(value = "vendorId", required = false) String vendorId, @RequestParam(value = "commodityId", required = false) String commodityId,
			@RequestParam(value = "subCommodityId", required = false) String subCommodityId)
			throws DSVException {
		LOG.info("Get PRICING SETTING RULES");
		LOG.info("commodityId: " + commodityId);
		LOG.info("subCommodityId: " + subCommodityId);
		LOG.info("vendorId: " + vendorId);
		return this.pricingRulesService.getPricingRules(vendorId, commodityId, subCommodityId);
	}

	/**
	 * Display The Setting Pricing Rules screen.
	 * @param strJson
	 *            :String
	 * @return String
	 * @author duyen.le
	 * @throws DSVException
	 *             If could not get values from Service Layer.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { UPDATE_RULES_PATH }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updatePricingSettingRules(@RequestParam(value = "strJson") String strJson) throws DSVException {
		LOG.info("Update PRICING SETTING RULES");
		LOG.info("strJson: " + strJson);
		String errorMessage = Constants.EMPTY_STRING;
		Map<String, Object> data = new HashMap<String, Object>();
		// JSONObject jsonResult = new JSONObject();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String userName = this.commonService.getUserLogin().getUsername();
		try {
			PricingRuleVO pricingRuleVO = objectMapper.readValue(strJson, PricingRuleVO.class);
			this.pricingRulesService.updatePricingRules(userName, pricingRuleVO);
			PricingRuleVO ruleVO = this.getPricingSettingRules(pricingRuleVO.getVendorId(), pricingRuleVO.getCommodityId(), pricingRuleVO.getSubCommodityId());
			data.put(PRICING_RULES_MODEL, ruleVO);
		} catch (JsonParseException e) {
			LOG.info(e.getMessage(), e);
			errorMessage = e.getMessage();
		} catch (JsonMappingException e) {
			LOG.info(e.getMessage(), e);
			errorMessage = e.getMessage();
		} catch (IOException e) {
			LOG.info(e.getMessage(), e);
			errorMessage = e.getMessage();
		}
		// jsonResult.put(ERROR_MESSAGE_CONS, errorMessage);
		// return jsonResult.toString();
		data.put(ERROR_MESSAGE_CONS, errorMessage);
		return data;
	}
}
