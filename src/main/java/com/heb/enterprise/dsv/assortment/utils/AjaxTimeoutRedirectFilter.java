/*
 * $Id: AjaxTimeoutRedirectFilter.java,v 1.16 2013/10/29 13:58:02 vn44178 Exp $
 *
 * Copyright (c) 2012 HEB
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of HEB.
 */
package com.heb.enterprise.dsv.assortment.utils;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.ThrowableAnalyzer;
import org.springframework.security.web.util.ThrowableCauseExtractor;
import org.springframework.web.filter.GenericFilterBean;

import com.heb.enterprise.dsv.utils.AntiMagicNumber;

/**
 * @author hainn
 */
public class AjaxTimeoutRedirectFilter extends GenericFilterBean {

	private static final Logger LOG = Logger.getLogger(AjaxTimeoutRedirectFilter.class);

	private ThrowableAnalyzer throwableAnalyzer = new DefaultThrowableAnalyzer();
	private AuthenticationTrustResolver authenticationTrustResolver = new AuthenticationTrustResolverImpl();

	private int customSessionExpiredErrorCode = AntiMagicNumber.NINE_HUNDRED_AND_ONE;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			chain.doFilter(request, response);
		} catch (IOException ex) {
			LOG.error(ex.getMessage(), ex);

		} catch (ServletException ex) {
			LOG.error(ex.getMessage(), ex);
			Throwable[] causeChain = this.throwableAnalyzer.determineCauseChain(ex);
			RuntimeException ase = (AuthenticationException) this.throwableAnalyzer.getFirstThrowableOfType(
					AuthenticationException.class, causeChain);
			if (ase == null) {
				ase = (AccessDeniedException) this.throwableAnalyzer.getFirstThrowableOfType(
						AccessDeniedException.class, causeChain);
			}

			if (ase != null) {
				if (ase instanceof AuthenticationException) {
					throw ase;
				} else if (ase instanceof AccessDeniedException) {

					if (this.authenticationTrustResolver.isAnonymous(
							SecurityContextHolder.getContext().getAuthentication())) {
						String ajaxHeader = ((HttpServletRequest) request).getHeader("X-Requested-With");
						if ("XMLHttpRequest".equals(ajaxHeader)) {
							HttpServletResponse resp = (HttpServletResponse) response;
							resp.sendError(this.customSessionExpiredErrorCode);
						} else {
							throw ase;
						}
					} else {
						throw ase;
					}
				}
			}

		}
	}

	/**
	 * @author hainn
	 */
	private static final class DefaultThrowableAnalyzer extends ThrowableAnalyzer {
		/**
		 * @author hainn
		 */
		protected void initExtractorMap() {
			super.initExtractorMap();

			registerExtractor(ServletException.class, new ThrowableCauseExtractor() {
				public Throwable extractCause(Throwable throwable) {
					ThrowableAnalyzer.verifyThrowableHierarchy(throwable, ServletException.class);
					Throwable throwableReturn = ((ServletException) throwable).getRootCause();
					return throwableReturn;
				}
			});
		}
	}

	public void setCustomSessionExpiredErrorCode(int customSessionExpiredErrorCode) {
		this.customSessionExpiredErrorCode = customSessionExpiredErrorCode;
	}
}
